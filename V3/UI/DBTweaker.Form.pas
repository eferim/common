﻿unit DBTweaker.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation,
  FMX.StdCtrls, FMX.ListBox, FMX.Edit, mormot, syncommons, FMX.Common.Utils, IOUtils,
  UI.StateManagement, MormotMods, MormotTypes, FMX.Models, FMX.GridLookupEdit, ObservableController;

type

  TfrmDBTweaker = class(TForm, IAppStateObserver, IServerObserver, IControllerTaskObserver, IServerStateObserver,
                               IMultipleTaskInitiator)
    lab1: TLabel;
    Label1: TLabel;
    cbxTable: TComboBox;
    Label2: TLabel;
    edtValidRecord: TEdit;
    edtInvalidRecord: TEdit;
    labDup: TLabel;
    btnRedirectAndDelete: TButton;
    labValidRefCount: TLabel;
    labInvalidRefCount: TLabel;
    btnRedirect: TButton;
    procedure cbxTableChange(Sender: TObject);
    procedure btnRedirectAndDeleteClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    gleValid, gleInvalid: TGridLookupEdit;
    FController: TObservableController;
    class var FInstance: TfrmDBTweaker;
    procedure SetController(const Value: TObservableController);
    procedure UpdateTables;
    function CurrentTable: TSqlRecordClass;
    procedure AppStateNotification(const State: TAppState);
    //II
    procedure ObserveTask (Task: TObservableTask);
    function GetName: string;
    function GetModel: TSqlModelEx;
    procedure ObserveState (State: TServerState; var Outcome: TOutcome);
  public
    class function ShowInstance (const AController: TObservableController): TfrmDBTweaker;
    class property Instance: TfrmDBTweaker read FInstance;
    property Model: TSqlModelEx read GetModel;
    property Controller: TObservableController read FController write SetController;
  end;

implementation

{$R *.fmx}

{ TfrmDBTweaker }

function TfrmDBTweaker.CurrentTable: TSqlRecordClass;
begin
  //result := model.Table[StringToUTF8(cbxTable.Selected.Text)];
  result := model.Tables [integer (cbxTable.Items.Objects[cbxTable.ItemIndex])];
end;

procedure TfrmDBTweaker.FormDestroy(Sender: TObject);
begin
  if FController <> nil
     then FController.Unsubscribe(self);
end;

function TfrmDBTweaker.GetModel: TSqlModelEx;
begin
  result := FController.Model;
end;

function TfrmDBTweaker.GetName: string;
begin
  result := Name;
end;

procedure TfrmDBTweaker.ObserveState(State: TServerState; var Outcome: TOutcome);
begin
  if State = TServerState.BeforeLogout
    then begin
           //Controller.PersistentUserSettings.WriteFormPosition(self);
           Close;
         end;
end;

procedure TfrmDBTweaker.ObserveTask(Task: TObservableTask);
begin
  if Task is TDBMaintenanceTask then
     begin
       var t := TDBMaintenanceTask(Task);

       case t.Kind of
         TDBMaintenanceKind.GetFKInfo:
           if t.ValidID = gleValid.SelectedID
              then labValidRefCount.Text := 'Referenci: ' + t.Outcome.RecordCount.ToString
              else if t.ValidID = gleInvalid.SelectedID
                   then labInvalidRefCount.Text := 'Referenci: ' + t.Outcome.RecordCount.ToString
              else // neither, must have changed the selected id while waiting for task
              ;
         TDBMaintenanceKind.RedirectFKAndDelete:
           if Task.Success then
             begin
               begin
                 gleInvalid.ClearSelection;
                 ShowMessage('OK');
               end;
             end;
       end;
     end;
end;

procedure TfrmDBTweaker.AppStateNotification(const State: TAppState);
begin
  if State = gleValid.apsSelectedRecordChangedDelayed
    then if gleValid.SelectedID.IsNotNULL
            then FController.GetFKInfo (CurrentTable, gleValid.SelectedID, self)
            else labValidRefCount.Text := ''
    else if gleInvalid.SelectedID.IsNotNULL
            then FController.GetFKInfo (CurrentTable, gleInvalid.SelectedID, self)
            else labInvalidRefCount.Text := ''
end;

procedure TfrmDBTweaker.btnRedirectAndDeleteClick(Sender: TObject);
begin
  FController.RedirectFKs (CurrentTable, gleValid.SelectedID, gleInvalid.SelectedID, Sender = btnRedirectAndDelete, self);
  CurrentTable.AddSubstitute (gleValid.SelectedRecord, gleInvalid.SelectedRecord);
  FController.GetFKInfo (CurrentTable, gleValid.SelectedID, self);
  if Sender <> btnRedirectAndDelete then
     FController.GetFKInfo (CurrentTable, gleInvalid.SelectedID, self);
end;

procedure TfrmDBTweaker.cbxTableChange(Sender: TObject);
begin
  if gleValid <> nil then gleValid.FreeGrid;
  gleValid.Free;

  if gleInvalid <> nil then gleInvalid.FreeGrid;
  gleInvalid.Free;

  if not CurrentTable.FieldInfoList.HasBriefFields
     then ShowErrorAndAbort('Tabela nema polja za sažeti prikaz!');

  gleValid := TGridLookupEdit.Create (CurrentTable, edtValidRecord, FController, nil);
  gleValid.FetchIfEmpty := true;

  gleInvalid := TGridLookupEdit.Create (CurrentTable, edtInvalidRecord, FController, nil);
  gleInvalid.FetchIfEmpty := true;

  RegisterStateObserver(self, TAppStateAction.Notification, [gleValid.apsSelectedRecordChangedDelayed,
                                                             gleInValid.apsSelectedRecordChangedDelayed], []);
  UnRegisterStateObserver(btnRedirectAndDelete);
  UnRegisterStateObserver(btnRedirect);
  RegisterStateObserver (btnRedirectAndDelete, TAppStateAction.Enabled,
                        [gleValid.apsHasSelectedRecord, gleInValid.apsHasSelectedRecord], []);
  RegisterStateObserver (btnRedirect, TAppStateAction.Enabled,
                        [gleValid.apsHasSelectedRecord, gleInValid.apsHasSelectedRecord], []);
end;

procedure TfrmDBTweaker.SetController(const Value: TObservableController);
begin
  if FController <> nil
     then FController.Unsubscribe(self);
  FController := Value;
  UpdateTables;
  FController.Subscribe(self);
end;

procedure TfrmDBTweaker.UpdateTables;
var
  index: integer;
begin
  cbxTable.Items.Clear;
  var sl := TStringList.Create;
  sl.Sorted := true;
  index := 0;
  for var table in Model.Tables do
      begin
        sl.AddObject (FController.Model.GetTableDisplayName (table), pointer(index));
        inc (index);
      end;
  cbxTable.Items.AddStrings(sl);
  sl.Free;
end;

class function TfrmDBTweaker.ShowInstance (const AController: TObservableController): TfrmDBTweaker;
begin
  if FInstance = nil then
    begin
      Application.CreateForm(TfrmDBTweaker, FInstance);
      FInstance.Controller := AController;
    end;
  FInstance.Show;
  result := FInstance;
end;

end.
