unit SingleGrid.Frame;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, System.Rtti, FMX.Grid.Style,
  FMX.ScrollBox, FMX.Grid, FMX.Edit, FMX.ComboEdit, FMX.Layouts, FMX.Controls.Presentation,
  FMX.GridModel, SynCommons, Mormot, MormotMods, ObservableController, FMX.Objects, FMX.BaseModel, FMX.TabControl,
  StylesModule, FMX.Common.Utils;

type
  TfraSingleGrid = class(TFrame)
    stg: TStringGrid;
    tobMain: TToolBar;
    spbCreate: TSpeedButton;
    spbDelete: TSpeedButton;
    spbUpdate: TSpeedButton;
    spbRead: TSpeedButton;
    spbFirst: TSpeedButton;
    spbLast: TSpeedButton;
    spbNext: TSpeedButton;
    spbPrevious: TSpeedButton;
    layDiv1: TLayout;
    layDiv2: TLayout;
    spbPrint: TSpeedButton;
    layFilter: TLayout;
    rctFilterIndicator: TRectangle;
    coeFilter: TComboEdit;
    Layout1: TLayout;
    layMain: TLayout;
    layGrid: TLayout;
    spbShow: TSpeedButton;
    procedure coeFilterChangeTracking(Sender: TObject);
  private
  public
    tac: TTabControl;
    spl: TSplitter;
    grm: TGridModel;
    procedure CreateDetails (CreateSubDetails: boolean);
    procedure AddDetail (SqlRecordClass: TSqlRecordClass; FieldInfo: TFieldInfo;
                         DetailLinkKeyName: string; CreateSubDetails: boolean);

    function CreateGridModel<T: TSqlRecord> (AController: TObservableController): TMormotGridModel<T>; overload;
    function CreateGridModel (SqlRecordClass: TSqlRecordClass; AController: TObservableController;
                              Master: TSqlRecordModel = nil; DetailLinkKeyName: string = ''): TGridModel; overload;
  end;

implementation

{$R *.fmx}

uses
  Common.Utils, BusinessLogic, MormotTypes;

{ TfraSingleGrid }

function TfraSingleGrid.CreateGridModel (SqlRecordClass: TSqlRecordClass; AController: TObservableController;
                                         Master: TSqlRecordModel = nil; DetailLinkKeyName: string = ''): TGridModel;
begin
  result := TGridModel.Create (SqlRecordClass, '', AController);
  result.AttachButtons(tobMain);
  result.Master := Master;
  if DetailLinkKeyName <> ''
     then result.DetailLinkKeyName.AsString := DetailLinkKeyName;
  result.Grid := stg;
  result.Fetch;//Records;
  grm := result;
end;

function TfraSingleGrid.CreateGridModel<T> (AController: TObservableController): TMormotGridModel<T>;
begin
  result := TMormotGridModel<T>.Create ('', AController);
  result.Grid := stg;
  result.AttachButtons(tobMain);
  result.Fetch;//Records;
//  result.SetFilterUI (coeFilter, nil, nil);
  grm := result;
end;

procedure TfraSingleGrid.coeFilterChangeTracking(Sender: TObject);
begin
  if coeFilter.Text = ''
     then rctFilterIndicator.Fill.Color := TAlphaColorRec.Null
     else rctFilterIndicator.Fill.Color := TAlphaColorRec.Red;
end;

procedure TfraSingleGrid.AddDetail (SqlRecordClass: TSqlRecordClass; FieldInfo: TFieldInfo;
                                    DetailLinkKeyName: string; CreateSubDetails: boolean);
var
  tai: TTabItem;
  fra: TfraSingleGrid;
begin
  if tac.IsNotAssigned then
    begin
      tac := TTabControl.Create(self);
      tac.Align := TAlignLayout.Bottom;
      tac.Parent := self;
      tac.Height := 300;//GetParentForm(self).Height / 4;

      spl := TSplitter.Create(self);
      spl.Align := TAlignLayout.Bottom;
      spl.Height := 3;
      spl.Parent := self;
      spl.Position.Y := 10;

      if CreateSubDetails then tac.Height := tac.Height * 2;
    end;

  tai := tac.Add();

  tai.Text := BL.Controller.Model.GetTableDisplayName(SqlRecordClass);

  if FieldInfo.Name <> grm.SqlRecordClass.CrudSQLTableName.AsString + 'ID'
    then tai.Text := tai.Text + format ('(%s)', [FieldInfo.Name]);

  fra := TfraSingleGrid.Create (self);
  fra.Name := 'fra' + FieldInfo.Name + '_' + SqlRecordClass.SQLTableName.AsString;
  fra.Parent := tai;
  fra.Align := TAlignLayout.Client;
  fra.CreateGridModel(SqlRecordClass, BL.Controller, grm, DetailLinkKeyName);

  if CreateSubDetails then fra.CreateDetails(false);
end;

procedure TfraSingleGrid.CreateDetails (CreateSubDetails: boolean);
var
  fi: TFieldInfo;
begin
  for var tab in BL.Controller.Model.Tables do
    for fi in tab.FieldInfoList do
      if fi.IsForeignKey and
         ( (fi.ForeignTableName = grm.SqlRecordClass.SQLTableName.AsString) or
           (fi.ForeignTableName = grm.SqlRecordClass.crudSQLTableName.AsString) )
        then begin
               if not tab.IsView then // if a view exists, skip the table because view will be added later
                 if BL.Controller.Model.Table[tab.SQLTableName + 'View'] <> nil then continue;

               //if tab2 <> nil
                  //then AddDetail (tab2, fi)
                  //else
               AddDetail (tab, fi, fi.Name, CreateSubDetails);
             end;
end;

end.
