unit Arrow.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects, FMX.Common.Utils,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo, math, FMX.Layouts;

type
  TfrmArrow = class(TForm)
    memDescription: TMemo;
    Layout1: TLayout;
    Image: TImage;
  private
    function GetDescription: string;
    procedure SetDescription(const Value: string);
    procedure AfterDescriptionChange;
    { Private declarations }
  public
    procedure AddToDescription(const Value: string);
    procedure PointTo (AControl: TControl);
    property Description: string read GetDescription write SetDescription;
  end;

var
  frmArrow: TfrmArrow;

implementation

{$R *.fmx}

{ TfrmArrow }

procedure TfrmArrow.AddToDescription(const Value: string);
begin
  memDescription.Lines.Add (Value);
  AfterDescriptionChange;
end;

procedure TfrmArrow.AfterDescriptionChange;
begin
  ClientHeight := max (round(Image.Height), memDescription.GetAutoHeight);
  memDescription.Visible := not Description.IsEmpty;
end;

function TfrmArrow.GetDescription: string;
begin
  result := memDescription.Text;
end;

procedure TfrmArrow.PointTo(AControl: TControl);
var
  ControlPos: TPointF;
begin
//  Width := 80;
  Height := 80;
  if AControl = nil then Exit;

  // Get the position of the control on the screen
  ControlPos := AControl.LocalToAbsolute(TPointF.Create(AControl.Width / 2, AControl.Height));
  var frm := GetParentForm(AControl);
  ControlPos.X := ControlPos.X + frm.Left + frm.Width - frm.ClientWidth;
  ControlPos.Y := ControlPos.Y + frm.Top + frm.Height - frm.ClientHeight;

  if (AControl.Height > 200)
     then ControlPos.Y := ControlPos.Y - AControl.Height / 2;

  if image.Align = TAlignLayout.Left
    then
      begin
        Left := Round(ControlPos.X - Image.Width / 2);
        Top := Round(ControlPos.Y);
      end
    else
      begin
        Left := Round(ControlPos.X - Image.Width / 2);
        Top := Round(ControlPos.Y);
      end;

  if not Visible then Show;
  BringToFront;
end;

procedure TfrmArrow.SetDescription(const Value: string);
begin
  memDescription.Text := Value;
  AfterDescriptionChange;
end;

end.
