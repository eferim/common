unit FMX.StateManagement.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Rtti, FMX.Grid.Style, FMX.Grid, FMX.Controls.Presentation,
  FMX.ScrollBox, math, UI.StateManagement, FMX.Edit, FMX.StdCtrls,
  FMX.Layouts;

type
  TfrmAppStates = class(TForm)
    grid: TStringGrid;
    StringColumn1: TStringColumn;
    stcElements: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    tim: TTimer;
    Layout1: TLayout;
    Label1: TLabel;
    edtObserver: TEdit;
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
    procedure gridDrawColumnCell(Sender: TObject; const Canvas: TCanvas; const Column: TColumn; const Bounds: TRectF; const Row: Integer;
      const Value: TValue; const State: TGridDrawStates);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure timTimer(Sender: TObject);
    procedure gridCellDblClick(const Column: TColumn; const Row: Integer);
    procedure edtObserverChange(Sender: TObject);
  private
    CellBrush : TBrush;
    procedure DoPopulate;
  public
    procedure Populate;
    class procedure ShowForm;
  end;

var
  frmAppStates: TfrmAppStates;

implementation

{$R *.fmx}

procedure TfrmAppStates.edtObserverChange(Sender: TObject);
begin
  Populate;
end;

procedure TfrmAppStates.FormCreate(Sender: TObject);
begin
  CellBrush := TBrush.Create(TBrushKind.Solid, TAlphaColors.Alpha);
end;

procedure TfrmAppStates.FormDestroy(Sender: TObject);
begin
  CellBrush.Free;
end;

procedure TfrmAppStates.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkF5
    then Populate;
end;

procedure TfrmAppStates.gridCellDblClick(const Column: TColumn; const Row: Integer);
begin
  if Column = stcElements then
    begin
      var Observer := UIStateRegistry.RegisteredObservers[row];
      UIStateRegistry.UpdateObserver (Observer);
    end;
end;

procedure TfrmAppStates.gridDrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const Bounds: TRectF;
  const Row: Integer; const Value: TValue; const State: TGridDrawStates);
begin
  if (TGridDrawState.Selected in State) or
      (TGridDrawState.Focused in State) then
    begin
      Column.DefaultDrawCell(Canvas, Bounds, Row, Value, State);
      Exit;
    end;

  case Column.Index of
    0 : begin
          if UIStateRegistry.RegisteredStates.Count <= Row
            then begin
                   Column.DefaultDrawCell(Canvas, Bounds, Row, Value, State);
                   exit;
                 end;

          var AppState := UIStateRegistry.RegisteredStates[Row];
          if AppState.Name <> grid.Cells[0, row]
            then begin Populate; exit; end;

          var b := Bounds;
          InflateRect(b, 2.0, 2.0);
          if AppState.Active
            then CellBrush.Color := TAlphaColorRec.Lightgreen
            else CellBrush.Color := TAlphaColorRec.Coral;
          Canvas.FillRect (b, 0, 0, [], 1, CellBrush);
          Column.DefaultDrawCell(Canvas, Bounds, Row, Value, State);
        end;
    2 : begin
          if UIStateRegistry.RegisteredObservers.Count <= Row
            then begin
                   Column.DefaultDrawCell(Canvas, Bounds, Row, Value, State);
                   exit;
                 end;

          var Observer := UIStateRegistry.RegisteredObservers[row];
          if Observer.Name <> grid.Cells[1, row]
            then begin Populate; exit; end;

          var b := Bounds;

          for var AppState in Observer.Required do
            begin
              if AppState.Active
                then Canvas.Fill.Color := TAlphaColors.Green
                else Canvas.Fill.Color := TAlphaColors.Red;

              Canvas.FillText(b, AppState.Name, false, 1, [], TTextAlign.Leading, TTextAlign.Leading);
              b.Left := b.Left + Canvas.TextWidth(AppState.Name + '  ');
            end;
        end;
    3 : begin
          if UIStateRegistry.RegisteredObservers.Count <= Row
            then begin
                   Column.DefaultDrawCell(Canvas, Bounds, Row, Value, State);
                   exit;
                 end;

          var Observer := UIStateRegistry.RegisteredObservers[row];
          if Observer.Name <> grid.Cells[1, row]
            then begin Populate; exit; end;

          var b := Bounds;

          for var AppState in Observer.Forbidden do
            begin
              if AppState.Active
                then Canvas.Fill.Color := TAlphaColors.Green
                else Canvas.Fill.Color := TAlphaColors.Red;

              Canvas.FillText(b, AppState.Name, false, 1, [], TTextAlign.Leading, TTextAlign.Leading);
              b.Left := b.Left + Canvas.TextWidth(AppState.Name + '  ');
            end;
        end;
    else Column.DefaultDrawCell(Canvas, Bounds, Row, Value, State);
  end;


//  grid.DefaultDrawing := False;
//https://stackoverflow.com/questions/43624260/how-to-change-color-of-a-cell-in-a-delphi-tgrid-firemonkey-component


end;

procedure TfrmAppStates.Populate;
begin
  tim.Enabled := false;
  tim.Enabled := true;
end;

procedure TfrmAppStates.DoPopulate;
var
  row: integer;
  ObserverFilter: string;
begin
  grid.BeginUpdate;

  grid.RowCount := max (UIStateRegistry.RegisteredStates.Count, UIStateRegistry.RegisteredObservers.Count);

  for row := 0 to grid.RowCount - 1 do
    begin
      grid.Cells [0, row] := '';
      grid.Cells [1, row] := '';
      grid.Cells [2, row] := '';
      grid.Cells [3, row] := '';
    end;

  row := 0;

  for var State in UIStateRegistry.RegisteredStates do
    begin
      grid.Cells [0, row] := State.Name;
      inc (row);
    end;

  row := 0;
  ObserverFilter := lowercase (edtObserver.Text);
  for var Observer in UIStateRegistry.RegisteredObservers do
    if (ObserverFilter = '') or (pos (ObserverFilter, lowercase (Observer.Name)) > 0) then
    begin
      grid.Cells [1, row] := Observer.Name;
      inc (row);
    end;

  grid.EndUpdate;
end;

class procedure TfrmAppStates.ShowForm;
begin
  if frmAppStates = nil
     then Application.CreateForm(TfrmAppStates, frmAppStates);
  frmAppStates.Show;
end;

procedure TfrmAppStates.timTimer(Sender: TObject);
begin
  DoPopulate;
  tim.Enabled := false;
end;

end.
