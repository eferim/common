unit ChangeLogViewer;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation,
  FMX.StdCtrls, FMX.ListBox, FMX.Edit, mormot, syncommons, FMX.Common.Utils, IOUtils,
  UI.StateManagement, MormotMods, MormotTypes, FMX.Models, FMX.GridLookupEdit, ObservableController, System.Rtti,
  FMX.Grid.Style, FMX.ScrollBox, FMX.Grid, Generics.Collections, Common.Utils;

type

  TfrmChangeLogViewer = class(TForm, IAppStateObserver, IServerObserver, IControllerTaskObserver, IMultipleTaskInitiator)
    cbxTable: TComboBox;
    Label1: TLabel;
    edtCurrent: TEdit;
    Label2: TLabel;
    stg: TStringGrid;
    edtGeneralFilter: TEdit;
    Label4: TLabel;
    edtHistory: TEdit;
    Label6: TLabel;
    procedure cbxTableChange(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
    procedure edtGeneralFilterChangeTracking(Sender: TObject);
  private
    gleCurrent: TGridLookupEdit;
    gleHistory: TGridLookupEdit<TSqlChangeLog>;
    FController: TObservableController;
    class var FInstance: TfrmChangeLogViewer;
    function GetModel: TSqlModelEx;
    procedure SetController(const Value: TObservableController);
    procedure AppStateNotification(const State: TAppState);
    procedure UpdateTables;
    function GetName: string;
    procedure ObserveTask (Task: TObservableTask);
    procedure Fetch;
  public
    class function ShowInstance (const AController: TObservableController): TfrmChangeLogViewer;
    class property Instance: TfrmChangeLogViewer read FInstance;

    property Model: TSqlModelEx read GetModel;
    property Controller: TObservableController read FController write SetController;
  end;

implementation

{$R *.fmx}

uses Controller.Tasks;

procedure TfrmChangeLogViewer.AppStateNotification(const State: TAppState);
begin
  Fetch;
end;

procedure TfrmChangeLogViewer.Fetch;
var
  qp: TQueryParameters;
begin
  qp := default (TQueryParameters);
  qp.OrderBy := 'ModifiedAt DESC';
  qp.Page.Limit := 100;
  qp.GeneralFilter.AsString := edtGeneralFilter.Text;
  if gleCurrent.SelectedID.IsNotNULL
     then qp.ID := gleCurrent.SelectedID
     else if gleHistory.SelectedID.IsNotNULL
          then qp.ID := gleHistory.SelectedRecord.RecordID;
  FController.GetTableHistory (gleCurrent.SqlRecordClass, qp, self);
end;

procedure TfrmChangeLogViewer.cbxTableChange(Sender: TObject);
begin
  if gleCurrent <> nil then gleCurrent.FreeGrid;
  gleCurrent.Free;

  var table := model.Table[StringToUTF8(cbxTable.Selected.Text)];
  if not table.FieldInfoList.HasBriefFields
     then ShowErrorAndAbort('Tabela nema polja za sa�eti prikaz!');

  gleCurrent := TGridLookupEdit.Create (table, edtCurrent, FController, nil);
  gleCurrent.CreateClearButton;
  RegisterStateObserver(self, TAppStateAction.Notification, [gleCurrent.apsSelectedRecordChangedDelayed], []);

  Fetch;
end;

procedure TfrmChangeLogViewer.edtGeneralFilterChangeTracking(Sender: TObject);
begin
  PostponeOnce (Fetch, 200);
end;

procedure TfrmChangeLogViewer.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkF5 then if cbxTable.ItemIndex > -1 then Fetch;
end;

function TfrmChangeLogViewer.GetModel: TSqlModelEx;
begin
  result := FController.Model;
end;

function TfrmChangeLogViewer.GetName: string;
begin
  result := Name;
end;

procedure TfrmChangeLogViewer.ObserveTask(Task: TObservableTask);
var
  tghTask: TGetTableHistoryTask;
  col: TColumn;
  iRow, iCol, iCurrent, iHistory: integer;
  rec: TSqlRecord;
  ttlCurrent, ttlHistory: TTimeLog;
  lstIDs: TList<TID>;
begin
  tghTask := TGetTableHistoryTask (Task);
  while stg.ColumnCount > 0 do
    stg.Columns[0].Free;

  PrepareGrid (FController.Model, gleCurrent.SqlRecordClass, '', stg, false, false);

  col := stg.AddColumn(TStringColumn, 'Vreme');
  col.Index := 0;

  col := stg.AddColumn(TStringColumn, 'Stanje/operacija');
  col.Index := 1;

  col := stg.AddColumn(TStringColumn, 'Korisnik');
  col.Index := 2;
  col.TagObject := gleCurrent.SqlRecordClass.FieldInfoList.ItemsByName['ModifiedByUserID'];

  col := stg.AddColumn(TStringColumn, 'ID');
  col.Index := 3;
  col.TagObject := gleCurrent.SqlRecordClass.FieldInfoList.ItemsByName['ID'];

  iCurrent := 0;
  iHistory := 0;

  lstIDs := TList<TID>.Create;
  iRow := 0;
  stg.BeginUpdate;
  stg.RowCount := tghTask.CurrentRecords.Count + tghTask.HistoryRecords.Count;

  while (iCurrent < tghTask.CurrentRecords.Count) or
        (iHistory < tghTask.HistoryRecords.Count)
        do
        begin
          if (iCurrent = tghTask.CurrentRecords.Count)
            then ttlCurrent := 0//High(Int64)
            else ttlCurrent := tghTask.CurrentRecords.Records[iCurrent].GetFieldVariant('ModifiedAt');

          if (iHistory = tghTask.HistoryRecords.Count)
            then ttlHistory := 0//High(Int64)
            else ttlHistory := tghTask.HistoryRecords.Records[iHistory].GetFieldVariant('ModifiedAt');

          if ttlHistory > ttlCurrent
            then
               begin
                 rec := tghTask.HistoryRecords.Records[iHistory];
                 stg.Cells[0, iRow] := DateTimeToStr (TimeLogToDateTime(ttlHistory));
                 if lstIDs.IndexOf (rec.IDValue) = -1
                    then stg.Cells[1, iRow] := 'Obrisano'
                    else stg.Cells[1, iRow] := 'Izmenjeno';
                 inc (iHistory);
               end
            else
               begin
                 rec := tghTask.CurrentRecords.Records[iCurrent];
                 stg.Cells[0, iRow] := DateTimeToStr (TimeLogToDateTime(ttlCurrent));
                 stg.Cells[1, iRow] := 'Aktuelno';
                 inc (iCurrent);
               end;
          lstIDs.Add (rec.IDValue);
          stg.Cells[2, iRow] := Controller.GetFieldDisplayValue(rec, TFieldInfo(stg.Columns[2].TagObject), nil); //lookups
          stg.Cells[3, iRow] := rec.IDValue.ToString;
          for iCol := 4 to stg.ColumnCount - 1 do
             stg.Cells[iCol, iRow] := Controller.GetFieldDisplayValue(rec, TFieldInfo(stg.Columns[iCol].TagObject), nil); //lookups
          inc (iRow);
        end;
  lstIDs.Free;
  stg.EndUpdate;
  stg.AutoColumnWidths;
end;

procedure TfrmChangeLogViewer.SetController(const Value: TObservableController);
begin
  if FController = Value then exit;
  if FController <> nil
     then FController.Unsubscribe(self);
  FController := Value;
  UpdateTables;
  FController.GetLookup(FController.Model.AuthUserClass, default(TQueryParameters));

  if gleHistory = nil then
    begin
      gleHistory := TGridLookupEdit<TSqlChangeLog>.Create (edtHistory, FController, nil);
      gleHistory.CreateClearButton;
      RegisterStateObserver(self, TAppStateAction.Notification, [gleHistory.apsSelectedRecordChangedDelayed], []);
    end;
end;

class function TfrmChangeLogViewer.ShowInstance(const AController: TObservableController): TfrmChangeLogViewer;
begin
  if FInstance = nil then
    begin
      Application.CreateForm(TfrmChangeLogViewer, FInstance);
      FInstance.Controller := AController;
    end;
  FInstance.Show;
  result := FInstance;
end;

procedure TfrmChangeLogViewer.UpdateTables;
begin
  cbxTable.Items.Clear;
  var sl := TStringList.Create;
  sl.Sorted := true;
  for var table in Model.Tables do
      sl.Add (table.SQLTableName.AsString);
  cbxTable.Items.AddStrings(sl);
  sl.Free;
end;

end.
