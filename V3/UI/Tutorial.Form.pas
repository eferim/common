﻿unit Tutorial.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts, FMX.StdCtrls, FMX.Controls.Presentation,
  FMX.ScrollBox, FMX.Memo, Arrow.Form, ObservableController, Generics.Collections, FMX.Edit, FMX.ListBox,
  UI.StateManagement, SynCommons, mormot, Common.Utils, FMX.Common.Utils, IOUtils, FMX.Menus, FMX.TabControl,
  FMX.BaseModel, FMX.GridLookupEdit, FMX.GridModel,
  FMX.DateTimeCtrls, FMX.Memo.Types, FMX.ComboEdit, MormotTypes;

const
  SLOW_TYPE_DELAY = 3;

type

  TfrmTutorial = class(TForm)
    spbPrev: TSpeedButton;
    spbNext: TSpeedButton;
    labNavigation: TLabel;
    layNavigation: TLayout;
    spbNextChapter: TSpeedButton;
    SpeedButton1: TSpeedButton;
    memCode: TMemo;
    pbr: TProgressBar;
    mnm: TMainMenu;
    mitSave: TMenuItem;
    mitCopyRecord: TMenuItem;
    mitRestart: TMenuItem;
    mitRunFromHere: TMenuItem;
    aiBusy: TAniIndicator;
    Timer1: TTimer;
    cbxChapter: TComboBox;
    spbRestartChapter: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
    procedure spbNextClick(Sender: TObject);
    procedure mitCopyRecordClick(Sender: TObject);
    procedure mitRestartClick(Sender: TObject);
    procedure mitRunFromHereClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure memCodeChangeTracking(Sender: TObject);
    procedure cbxChapterChange(Sender: TObject);
    procedure spbRestartChapterClick(Sender: TObject);
  private
    procedure PreParse;
    procedure SetController(const Value: TObservableController);
    function IsComment(const Line: string): boolean;
    procedure SelectFirstInModel;
    procedure SetLineIndex(const Value: integer);
    procedure Save;
    function IsChapter(const Line: string): boolean;
    procedure RunFromCursor;
    type
      TCommandMethod = procedure (Line: string) of object;
    class var
      Instance: TfrmTutorial;
    var
    FLineIndex, CommandIndex, TotalCommands: integer;
    FController: TObservableController;
    slCommands: TStringList;
    dicCommandMethods: TDictionary<string, TCommandMethod>;
    CurrentForm: TCommonCustomForm;
    CurrentComponent: TComponent;
    CurrentModel: TSqlRecordModel;
    WaitForStateDeactivation, apsWaitingForUser, apsProcessing: TAppState;
    CurrentLine, SlowTypeText: string;
    ArrowHidden, IgnoreModification, IgnoreChapterChange, Modified: boolean;
    function Code: TStrings;
    procedure Execute;
    function CanContinue: boolean;
    function IsCommand(const Line: string): boolean;
    function ExecuteCommand (Line: string): boolean; // returns false if it was async and the execution should not immediately continue
    procedure Load;

    function ExtractLineSegment(var Line: string): string;
    procedure GetCurrentComponent (var Line: string);

    procedure metShowForm (Line: string);
    procedure metSetForm (Line: string);
    procedure metClick (Line: string);
    procedure metType (Line: string);
    procedure metDropDown (Line: string);
    procedure metChapter (Line: string);
    procedure metPointTo (Line: string);
    procedure metPointAndClick (Line: string);
    procedure metCheck (Line: string);
    procedure metSlowType (Line: string);
    procedure metSetDate (Line: string);
    procedure metSetTime (Line: string);
    procedure metSelect (Line: string);
    procedure metSelectTab (Line: string);
    procedure metHideArrow (Line: string);
    procedure ContinueSlowType;
  public
    class procedure ShowInstance (AController: TObservableController);
    class function Active: boolean;
    var frmArrow: TfrmArrow;
    procedure Restart;

    property Controller: TObservableController read FController write SetController;
    property LineIndex: integer read FLineIndex write SetLineIndex;
  end;


implementation

{$R *.fmx}

{ TfrmTutorial }

procedure TfrmTutorial.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CancelPostponedCalls(self);
  apsProcessing.Deactivate;
  frmArrow.Close;
end;

procedure TfrmTutorial.FormCreate(Sender: TObject);
begin
  Application.CreateForm(TfrmArrow, frmArrow);
  Load;
  slCommands := TStringList.Create;
  dicCommandMethods := TDictionary<string, TCommandMethod>.Create;
  dicCommandMethods.Add('setform', metSetForm);
  dicCommandMethods.Add('showform', metShowForm);
  dicCommandMethods.Add('click', metClick);
  dicCommandMethods.Add('pointto', metPointTo);
  dicCommandMethods.Add('PointAndClick'.ToLower, metPointAndClick);
  dicCommandMethods.Add('type', metType);
  dicCommandMethods.Add('dropdown', metDropDown);
  dicCommandMethods.Add('chapter', metChapter);
  dicCommandMethods.Add('subchapter', metChapter);
  dicCommandMethods.Add('check', metCheck);
  dicCommandMethods.Add('SlowType'.ToLower, metSlowType);
  dicCommandMethods.Add('Select'.ToLower, metSelect);
  dicCommandMethods.Add('SelectTab'.ToLower, metSelectTab);
  dicCommandMethods.Add('SetDate'.ToLower, metSetDate);
  dicCommandMethods.Add('SetTime'.ToLower, metSetTime);
  dicCommandMethods.Add('HideArrow'.ToLower, metHideArrow);

  apsWaitingForUser := TAppState.CreateAsSingle ('Waiting');
  apsProcessing     := TAppState.CreateAsSingle ('Processing');

  Left := Screen.WorkAreaWidth - Width;
  Top := 0;
//  frmArrow.Left := 100;
//  frmArrow.top := 100;
//  frmArrow.Show;
end;

procedure TfrmTutorial.FormDestroy(Sender: TObject);
begin
  slCommands.Free;
  apsWaitingForUser.Free;
  apsProcessing.Free;
  dicCommandMethods.Free;
end;

procedure TfrmTutorial.Restart;
begin
  PreParse;
  CommandIndex := 0;
  LineIndex := 0;
  Execute;
end;

procedure TfrmTutorial.Save;
begin
  Code.SaveToFile(TPath.Combine (FindProjectFileFolder, 'Tutorial.txt'), TEncoding.UTF8);
  Modified := false;
end;

procedure TfrmTutorial.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
  case Key of
    vkF8: Execute;
    vkF5: Restart;
    {$IFDEF DEBUG}
    ord ('S'): if Shift = [ssCtrl]
      then Save;
    {$ENDIF}
  end;
end;

procedure TfrmTutorial.SetController(const Value: TObservableController);
begin
  FController := Value;
  RegisterStateObserver(spbNext, TAppStateAction.Enabled, [apsWaitingForUser], [apsProcessing, Controller.apsTaskRunning]);
end;

procedure TfrmTutorial.SetLineIndex(const Value: integer);
begin
  if (Value < memCode.Lines.Count - 1) and memCode.Lines[Value].Trim.IsEmpty then
    begin
      //skip empty line
      LineIndex := Value + 1;
      exit;
    end;

  FLineIndex := Value; // allow setting line index to count, i.e. out of bounds
  if FLineIndex >= memCode.Lines.Count then exit;

  memCode.SelStart  := memCode.PosToTextPos(TCaretPosition.Create (LineIndex, 0));
  memCode.SelLength := memCode.PosToTextPos(TCaretPosition.Create (LineIndex, memCode.Lines[LineIndex].Length)) - memCode.SelStart;
end;

class procedure TfrmTutorial.ShowInstance (AController: TObservableController);
begin
  if Instance = nil
     then begin
            Application.CreateForm(TfrmTutorial, Instance);
            Instance.Controller := AController;
          end;
  Instance.Show;
  Instance.Restart;
end;

procedure TfrmTutorial.spbNextClick(Sender: TObject);
begin
  Execute;
end;

procedure TfrmTutorial.spbRestartChapterClick(Sender: TObject);
begin
  SetLineIndex(integer (cbxChapter.SelectedObject));
  RunFromCursor;
end;

procedure TfrmTutorial.Timer1Timer(Sender: TObject);
begin
  if Modified then Save;
end;

function TfrmTutorial.Code: TStrings;
begin
  result := Instance.memCode.Lines;
end;

class function TfrmTutorial.Active: boolean;
begin
  result := Instance.IsAssigned and Instance.Visible;
end;

function TfrmTutorial.CanContinue: boolean;
begin
  if WaitForStateDeactivation.IsAssigned then
    if WaitForStateDeactivation.Active
      then exit (false)
      else WaitForStateDeactivation := nil;

  result := not Controller.apsTaskRunning.Active
            and not apsWaitingForUser.Active
            and not apsProcessing.Active;
end;

procedure TfrmTutorial.RunFromCursor;
begin
  LineIndex := memCode.CaretPosition.Line;
  Execute;
end;

procedure TfrmTutorial.cbxChapterChange(Sender: TObject);
begin
  if IgnoreChapterChange then exit;
  SetLineIndex(integer (cbxChapter.SelectedObject));
  RunFromCursor;
end;

function TfrmTutorial.IsCommand (const Line: string): boolean;
begin
  result := Line.Trim.StartsWith('!');
end;

function TfrmTutorial.IsChapter (const Line: string): boolean;
begin
  result := Line.Trim.ToLower.StartsWith('!chapter') or Line.Trim.ToLower.StartsWith('!subchapter');
end;

function TfrmTutorial.IsComment (const Line: string): boolean;
begin
  result := Line.Trim.StartsWith('//');
end;

procedure TfrmTutorial.Load;
begin
  IgnoreModification := true;
  memCode.Lines.Text := GetResourceString('Tutorial');
  IgnoreModification := false;
  PreParse;
end;

procedure TfrmTutorial.PreParse;
var
  i, Level, SubLevel: integer;
  cmd: string;
begin
  TotalCommands := 0;
  cbxChapter.Items.Clear;
  Level := 1;
  SubLevel := 0;
  for i := 0 to Code.Count - 1 do
    begin
      if IsCommand (Code[i])
        then inc (TotalCommands);
      if IsChapter (Code[i])
        then begin
               var Line := Code[i];
               cmd := ExtractLineSegment(Line);
               if cmd.ToLower = '!subchapter'
                 then inc (SubLevel)
                 else begin
                        inc (Level);
                        SubLevel := 0;
                      end;
               if SubLevel > 0
                 then Line := Level.ToString + '.' + SubLevel.ToString + ' ' + Line
                 else Line := Level.ToString + ' ' + Line;
               cbxChapter.Items.AddObject(Line, pointer(i));
             end;
    end;
  pbr.Max := TotalCommands;
  pbr.Value := 0;
end;

procedure TfrmTutorial.memCodeChangeTracking(Sender: TObject);
begin
  if not IgnoreModification then Modified := true;
end;

procedure TfrmTutorial.metChapter(Line: string);
begin
  labNavigation.Text := Line;
  begin
    IgnoreChapterChange := true;
    cbxChapter.SelectedObject := pointer (LineIndex);// Text := Line;
    IgnoreChapterChange := false;
  end;
end;

procedure TfrmTutorial.GetCurrentComponent (var Line: string);
var
  ComponentPath: TArray<string>;
  i: integer;
  ComponentName: string;
  c: TComponent;

  procedure SetCurrent (Component: TComponent; Segment: string);
  begin
    if Component = nil then ShowErrorAndAbort('Component not found. Path:'#13 + ComponentName + #13'Segment: '+Segment);
    CurrentComponent := Component;
  end;
begin
  if CurrentForm.IsNotAssigned then ShowErrorAndAbort('Current form not assigned!');

  ComponentName := ExtractLineSegment(Line);
  ComponentPath := ComponentName.Split(['.']);
  c := CurrentForm.FindComponent(ComponentPath[0]);
  if c = nil
     then c := Screen.ActiveForm.FindComponent(ComponentPath[0]);

  SetCurrent (c, ComponentPath[0]);
  for i := 1 to High (ComponentPath) do
    SetCurrent (CurrentComponent.FindComponent(ComponentPath[i]), ComponentPath[i]);

  if (CurrentComponent is TControl) and not ArrowHidden
     then frmArrow.PointTo(TControl(CurrentComponent));

  if CurrentComponent is TFmxObject
     then TSqlRecordModel.dicControlToModel.TryGetValue(TFmxObject(CurrentComponent), CurrentModel);
end;

procedure TfrmTutorial.metCheck(Line: string);
begin
  GetCurrentComponent (Line);
  if CurrentComponent is TCheckBox then
     TCheckBox(CurrentComponent).IsChecked := Line.Trim.ToLower = 'true';
end;

procedure TfrmTutorial.metClick(Line: string);
begin
  GetCurrentComponent (Line);
  if CurrentComponent is TCustomButton then
    begin
      var btn := TCustomButton(CurrentComponent);
      btn.SimulateClick;
//      if Assigned (btn.Action) and Assigned (btn.Action.OnExecute)
//         then btn.Action.OnExecute (btn.Action)
//         else if Assigned (btn.OnClick)
//              then btn.OnClick (btn);
      BringToFront;
    end;
end;

procedure TfrmTutorial.metDropDown(Line: string);
begin
  GetCurrentComponent (Line);

  if CurrentComponent is TCustomComboBox then
    begin
      var com := TCustomComboBox(CurrentComponent);
      com.DropDown;
      frmArrow.PointTo(TControl(CurrentComponent));
    end;

  if CurrentModel is TGridLookupEdit then
    TGridLookupEdit(CurrentModel).ShowGrid;

//  if not apsWaitingForUser.Active then
    for var i := 0 to 100 do
      begin
        sleep (1);
        Application.ProcessMessages;
      end;
end;

procedure TfrmTutorial.metHideArrow(Line: string);
begin
  ArrowHidden := true;
  frmArrow.Description := '';
  frmArrow.Hide;
end;

procedure TfrmTutorial.metPointAndClick(Line: string);
begin
  ArrowHidden := false;
  GetCurrentComponent (Line);
  if CurrentComponent is TCustomButton then
    begin
      var btn := TCustomButton(CurrentComponent);
      btn.SimulateClick;
      frmArrow.PointTo(btn);
    end;
end;

procedure TfrmTutorial.metPointTo(Line: string);
begin
  ArrowHidden := false;
  GetCurrentComponent (Line);
  if CurrentComponent is TControl
     then frmArrow.PointTo(TControl(CurrentComponent));
  BringToFront;
end;

procedure TfrmTutorial.SelectFirstInModel;
begin
  if CurrentModel.apsFetching.Active then
    begin
      Postpone (SelectFirstInModel, 1000);
      apsProcessing.Activate;
      exit;
    end;
  if CurrentModel is TGridLookupEdit then
     begin
       var gle := TGridLookupEdit(CurrentModel);
       if gle.GridModel.RecordCount <> 1 then
          ShowErrorAndAbort('Tutorijal očekuje da postoji samo jedan slog koji odgovara ovim parametrima, ali postoji '
                                       + gle.GridModel.RecordCount.ToString);
       gle.SelectedID := gle.GridModel.Records.Records[0].IDValue;
       gle.HideGrid;
      apsProcessing.Deactivate;
     end;
end;

procedure TfrmTutorial.metSelect(Line: string);
begin
  GetCurrentComponent (Line);

  if CurrentComponent is TCustomComboBox
     then TCustomComboBox(CurrentComponent).ItemIndex := ExtractLineSegment(Line).ToInteger;

  if CurrentComponent is TListBox then
    begin
      var Value := ExtractLineSegment(Line);
      if not TListBox(CurrentComponent).SelectText (Value)
         then ShowErrorAndAbort(format ('List box %s does not contain %s', [CurrentComponent.Name, Value]));
    end;

  if CurrentModel is TGridLookupEdit then
     begin
       SelectFirstInModel;
       CurrentLine := Line;
     end;

  if CurrentModel is TGridModel then
    begin
      var gm := TGridModel(CurrentModel);
      var FieldName := ExtractLineSegment(Line);
      var FieldValue := ExtractLineSegment(Line);
      if (gm.Records = nil) or gm.apsFetching.Active or gm.Records.Find(FieldName, FieldValue).IsNotAssigned
       then
        begin
          if not gm.apsFetching.Active
             then gm.Fetch;
          WaitForStateDeactivation := gm.apsFetching;
          LineIndex := LineIndex - 1;
          Postpone (Execute, 1000);
          exit;
        end;

      gm.SelectedRecord := gm.Records.Find(FieldName, FieldValue);
      if not gm.SelectedRecord.IsAssigned
         then ShowErrorAndAbort(format ('Cant find record with %s = %s', [FieldName, FieldValue]));
    end;
end;

procedure TfrmTutorial.metSelectTab(Line: string);
begin
  GetCurrentComponent (Line);
  if CurrentComponent is TTabItem
     then TTabItem(CurrentComponent).TabControl.ActiveTab := TTabItem(CurrentComponent)
     else if CurrentComponent is TTabControl
     then TTabControl(CurrentComponent).Index := Line.Trim.ToInteger
     else ShowErrorDialogue(CurrentComponent.Name + ' is not a tab item nor tab control!');
end;

procedure TfrmTutorial.metSetDate(Line: string);
begin
  GetCurrentComponent (Line);
  if CurrentComponent is TDateEdit then
    begin
      var s := ExtractLineSegment(Line);
      if s.ToLower = 'today'
        then TDateEdit(CurrentComponent).SetSmartDate (trunc(now))
        else TDateEdit(CurrentComponent).SetSmartDate (s.ToInteger);
    end;
end;

procedure TfrmTutorial.metSetForm(Line: string);
begin
  var FormName := ExtractLineSegment(Line);

  if FormName.ToLower = 'active'
    then CurrentForm := Screen.ActiveForm // doesnt catch the frx form for some reason, called too soon?
    else
      begin
        var form := FindForm (FormName);
        if form.IsAssigned
           then begin
                  CurrentForm := form;
                  CurrentForm.BringToFront;
                  BringToFront;
                end
           else ShowErrorAndAbort ('Form not found: ' + FormName);
      end;
end;

procedure TfrmTutorial.metSetTime(Line: string);
begin
  GetCurrentComponent (Line);
  if CurrentComponent is TTimeEdit
     then begin
            var s := ExtractLineSegment(Line);
            if s.IndexOf (':') > 0
               then TTimeEdit(CurrentComponent).Time := strtotime (s)
               else TTimeEdit(CurrentComponent).Time := (s.ToInteger * DelphiSecond);
          end;
end;

procedure TfrmTutorial.metShowForm(Line: string);
begin
  var form := FindForm (ExtractLineSegment(Line));
  if form.IsAssigned
     then begin
            CurrentForm := form;
            CurrentForm.Show;
            CurrentForm.BringToFront;
            BringToFront;
          end;
end;

procedure TfrmTutorial.metSlowType(Line: string);
begin
  var OriginalLine := Line;

  GetCurrentComponent(Line);
  SlowTypeText := ExtractLineSegment(Line);

  if Length(SlowTypeText) < 2 then
    begin
      SlowTypeText := '';
      metType (OriginalLine);
      exit;
    end;

  CurrentLine := Line;
  TCustomEdit (CurrentComponent).Text := '';

  // if value is empty, execute will move on to next command if continue is set
  if SlowTypeText.IsEmpty then exit;

  if not SlowTypeText.IsEmpty
     then ContinueSlowType;
  if not SlowTypeText.IsEmpty
     then PostponeOnce (ContinueSlowType, SLOW_TYPE_DELAY);
end;

procedure TfrmTutorial.ContinueSlowType;
begin
  TCustomEdit (CurrentComponent).Text := TCustomEdit (CurrentComponent).Text + SlowTypeText[1];
  SlowTypeText := copy (SlowTypeText, 2);
  apsProcessing.SetActiveIf(not SlowTypeText.IsEmpty);
  if not SlowTypeText.IsEmpty
     then PostponeOnce (ContinueSlowType, SLOW_TYPE_DELAY)
     else begin
            if assigned (TCustomEdit (CurrentComponent).OnTyping)
               then TCustomEdit (CurrentComponent).OnTyping (CurrentComponent);
            if assigned (CurrentModel) then CurrentModel.Fetch;

            if frmArrow.Description.IsEmpty
               then Execute;
          end;
end;

procedure TfrmTutorial.metType(Line: string);
begin
  GetCurrentComponent(Line);
  if CurrentComponent is TCustomEdit then
    begin
      var edt := TCustomEdit(CurrentComponent);
      edt.Text := ExtractLineSegment(Line);
      if assigned (edt.OnTyping)
         then edt.OnTyping (edt);
    end;
end;

procedure TfrmTutorial.mitCopyRecordClick(Sender: TObject);
var
  sl: TStringList;
  mf: TModelForm;
  s: string;
begin
  if CurrentForm is TModelForm then
    begin
      LineIndex := memCode.TextPosToPos(memCode.SelStart).Line;
      code.Insert (LineIndex, '');
      mf := TModelForm(CurrentForm);
      if not mf.Model.IsAssigned
         then ShowErrorAndAbort ('This form does not have a model assigned!');
      s := format ('// === Begin: %s ===', [mf.Model.SqlRecordClass.SQLTableName]);
      code.Insert (LineIndex + 1, s);
      s :=     format ('// === End: %s ===',   [mf.Model.SqlRecordClass.SQLTableName]);
      code.Insert (LineIndex + 2, s);
      code.Insert (LineIndex + 3, '');
      sl := TStringList.Create;
      sl.Text := TModelForm(CurrentForm).Model.CopyForTutorial;
      while sl.Count > 0 do
        begin
          code.Insert (LineIndex + 2, sl[sl.Count - 1]);
          sl.Delete (sl.Count - 1);
        end;
      sl.Free;
    end;
end;

procedure TfrmTutorial.mitRestartClick(Sender: TObject);
begin
  Restart;
end;

procedure TfrmTutorial.mitRunFromHereClick(Sender: TObject);
begin
  RunFromCursor;
end;

function TfrmTutorial.ExtractLineSegment(var Line: string): string;
var
  DelimPos: Integer;
  QuoteEnd: Integer;
begin
  Line := Line.Trim;
  if Line = '' then exit ('');

  if Line[1] = '"'
    then
      begin
        QuoteEnd := Pos('"', Line, 2);
        if QuoteEnd > 0 then
        begin
          Result := Copy(Line, 2, QuoteEnd - 2);
          Delete(Line, 1, QuoteEnd);
        end
        else
        begin
          Result := Copy(Line, 2, MaxInt);
          Line := '';
        end;
      end
    else
      begin
        DelimPos := Pos(' ', Line);
        if DelimPos = 0 then
          DelimPos := Pos(#9, Line);
        if DelimPos = 0 then
          DelimPos := Pos(#13, Line);

        if DelimPos > 0 then
        begin
          Result := Copy(Line, 1, DelimPos - 1);
          Delete(Line, 1, DelimPos);
        end
        else
        begin
          Result := Line;
          Line := '';
        end;
      end;

  Line := Line.Trim;
end;

function TfrmTutorial.ExecuteCommand (Line: string): boolean;
var
  command: string;
  method: TCommandMethod;
begin
  command := ExtractLineSegment(Line).ToLower.Substring (1);
  result := command.ToLower <> 'async';
  if not result then
    begin
      PostponeOnce (Execute, 1);
      command := ExtractLineSegment(Line).ToLower.Substring (1);
    end;

  if dicCommandMethods.TryGetValue(command, method)
     then method (Line)
     else raise Exception.Create('Unknown command: ' + command);
end;

procedure TfrmTutorial.Execute;
var
  line: string;
  Postponed: boolean;
begin
  apsWaitingForUser.Deactivate;
  frmArrow.Description := '';
  try
    while (LineIndex < Code.Count) do
      begin
        line := code[LineIndex];
        if IsCommand (line)
          then if CanContinue
               then begin
                      inc (CommandIndex);
                      pbr.Value := CommandIndex;
                      frmArrow.Description := '';
                      // if it returns false, it means Execute is postponed and will be called later
                      Postponed := not ExecuteCommand (line);
                      LineIndex := LineIndex + 1;
                      if Postponed then exit;
                    end
               else break
          else begin
                 if not IsComment (line) and not line.Trim.IsEmpty
                   then begin
                          frmArrow.AddToDescription (line);
                          var i := LineIndex + 1;
                          while (i < code.Count) and not IsCommand (code[i]) do
                            begin
                              if not IsComment (code[i])
                                 then frmArrow.AddToDescription (Code[i]);
                              inc (i);
                            end;
                          apsWaitingForUser.Activate; //any prompt causes tutorial to stop running commands
                          LineIndex := i;
                          exit;
                        end;
                 LineIndex := LineIndex + 1;
               end;
      end;
  finally
    BringToFront;
    Activate;
    spbNext.SetFocus;
  end;
end;

end.
