unit FMX.CreateUpdateModel;

interface

uses Mormot, MormotMods, System.Generics.Collections;

type

  TCreateUpdateModel = class
  private
    FCrudRecord: TSqlRecord;
    FSqlRecordClass: TSqlRecordClass;
    procedure SetCrudRecord(const Value: TSqlRecord);
  public
    constructor Create;
    destructor Destroy; override;

    procedure CreateRecord; overload;
    procedure CreateRecord (rec: TSqlRecord); overload; virtual;
    procedure LockForUpdate(ID: TID);

    property SqlRecordClass: TSqlRecordClass read FSqlRecordClass write FSqlRecordClass;
    property CrudRecord: TSqlRecord read FCrudRecord write SetCrudRecord;
  end;

implementation

{ TCreateUpdateModel }

constructor TCreateUpdateModel.Create;
begin
  inherited;
end;

procedure TCreateUpdateModel.CreateRecord;
begin

end;

procedure TCreateUpdateModel.CreateRecord(rec: TSqlRecord);
begin

end;

destructor TCreateUpdateModel.Destroy;
begin
  inherited;
end;

procedure TCreateUpdateModel.LockForUpdate(ID: TID);
begin

end;

procedure TCreateUpdateModel.SetCrudRecord(const Value: TSqlRecord);
begin
  FCrudRecord := Value;
end;

end.
