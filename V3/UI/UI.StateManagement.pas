unit UI.StateManagement;

interface

uses SysUtils, Classes, System.Generics.Collections, Common.Utils, System.Actions, Types, System.UITypes

     {$IFDEF FIREMONKEY}
     , FMX.Types, FMX.Controls, FMX.Edit, FMX.Common.Utils, FMX.ActnList, FMX.Forms
     {$ELSE}

       {$IFDEF VCL}
       , VCL.Forms, VCL.Controls, VCL.ActnList, VCL.ExtCtrls, VCL.LBUtils, VCL.StdCtrls

       {$ELSE}
       This unit should not be used in console projects.
       {$ENDIF}

     {$ENDIF}
     ;

{$SCOPEDENUMS ON}

type

  TAppStateObserver = TObject;
  {TODO -oOwner -cGeneral : set akcija}
  TAppStateAction = (Visible, VisibleAndFocus, Enabled, EnabledAndFocus, Clear, Notification);

  TAppStateTiming = (Immediate, OnIdle, Delayed);

  TAppStateObserverKind = (Action, CustomEdit, Control, Callback);

  TRequiredType = (rtAND, rtOR);
  TForbiddenType = (ftAND, ftOR);

  TRegisteredObserver = class;

  TAppState = class (TComponent)
  private
    function GetActive: boolean;
  protected
    FSuspendCount, FActiveCount: integer;
  public
    Name: string;
    AllowMultiple: boolean;
    Timing: TAppStateTiming;
    Observers: TList <TRegisteredObserver>;
    Child: TAppState;
    DelayedUntil: TDateTime;
    constructor CreateAsSingle   (const AName: string; ATiming: TAppStateTiming = TAppStateTiming.Immediate; AChildState: TAppState = nil);
    constructor CreateAsMultiple (const AName: string; AChildState: TAppState = nil);
    destructor Destroy; override;

    procedure Activate;
    procedure Deactivate;
    procedure SetActiveIf (Condition: boolean);

    procedure Add;
    procedure Remove;

    procedure CycleOnOff;

    procedure Notification (AComponent: TComponent; Operation: TOperation); override;
    procedure CheckIfMultiple;
    procedure CheckIfSingle;

    procedure Suspend;// temporarily disabled
    procedure Resume;

    property Active: boolean read GetActive;
  end;

  TAppStateArray = TArray<TAppState>;

  TNotifyEventAppState = class (TAppState)
  protected
    FComponent: TComponent;
  public
    procedure Notification (AComponent: TComponent; Operation: TOperation); override;
  end;

  TFormClosedAppState = class (TNotifyEventAppState)
  protected
    FOriginal: TCloseEvent;
    class var
    FDictionary: TDictionary <TForm, TFormClosedAppState>;
    procedure FHandler (Sender: TObject; var Action: TCloseAction);
  public
    constructor Create (AOwner: TComponent; AForm: TForm); reintroduce;
    destructor Destroy; override;
    class function FindOrCreateFor (AForm: TForm): TFormClosedAppState; overload;
    class function FindOrCreateFor (AControl: TControl): TFormClosedAppState; overload;
    class constructor Create;
    class destructor Destroy;
  end;

  TAppStates    = TArray<TAppState>;
  TAppStateList = TList <TAppState>;
  TAppStateDictionary = TObjectDictionary <string, TAppState>;

  TRegisteredObserver = class
    Observer: TAppStateObserver;
    Action: TAppStateAction;
    Required, Forbidden: TAppStates;
    RequiredType: TRequiredType;
    ForbiddenType: TForbiddenType;
    Kind: TAppStateObserverKind;
    Name: string;
    constructor Create (AObserver: TAppStateObserver; AAction: TAppStateAction; const ARequired, AForbidden: TAppStates; const AName: string = '');
    destructor Destroy; override;

    procedure StateDestroyed (st: TAppState);
  end;

  IAppStateObserver = interface
  ['{B059D9A0-496E-4BFC-B6F2-EDF482CB5C9A}']
    procedure AppStateNotification (const State: TAppState);
  end;

  TUIStateRegistry = class (TComponent) //(TNonARCInterfacedObject, IUIStateRegistry)
  private
    Timer: TTimer;
    FRegisteredStates: TList <TAppState>;
    FActiveStates: TList <TAppState>;
    FRegisteredObservers: TObjectList <TRegisteredObserver>;
    FDelayedStates: TList <TAppState>;
    procedure Ontimer (Sender: TObject);
    procedure FAdd (State: TAppState);
    procedure FRemove (State: TAppState);
    procedure UpdateUI (State: TAppState);
    procedure AddDelay (State: TAppState);
    procedure RegisterState  (AppState: TAppState);
    procedure UnregisterState  (AppState: TAppState);
    function TryFindRegisteredObserver(Element: TAppStateObserver; Action: TAppStateAction; var regel: TRegisteredObserver): boolean;
  public
    DelayInterval: double;
    InTimer: boolean;
    procedure Activate    (State: TAppState; ASet: boolean = true);
    procedure Deactivate  (State: TAppState); // i.e. UnsetState (uisCreatingRecord)

    procedure Add         (State: TAppState);
    procedure Remove      (State: TAppState);

    procedure RegisterObserver  (Element: TAppStateObserver; Action: TAppStateAction; Required, Forbidden: TAppStates; RequiredType: TRequiredType = TRequiredType.rtAND; ForbiddenType: TForbiddenType = TForbiddenType.ftOR);
    procedure RegisterObservers (const Elements: TArray<TAppStateObserver>; Action: TAppStateAction; const Required, Forbidden: TAppStates);
    procedure UnregisterObserver (Element: TAppStateObserver);
    procedure UpdateObserver (RegElement: TRegisteredObserver);

    function AsString: string;

    procedure Notification (AComponent: TComponent; Operation: TOperation); override;

    property RegisteredStates: TList <TAppState> read FRegisteredStates;
    property RegisteredObservers: TObjectList <TRegisteredObserver> read FRegisteredObservers;

    constructor Create; reintroduce;
    destructor Destroy; override;
  end;

  procedure RegisterStateObserver  (Element: TAppStateObserver; Action: TAppStateAction; Required, Forbidden: TAppStates);
  procedure RegisterStateObservers (const Elements: TArray<TAppStateObserver>; Action: TAppStateAction; const Required, Forbidden: TAppStates);
  procedure UnregisterStateObserver (Element: TAppStateObserver);

var
  UIStateRegistry: TUIStateRegistry;

implementation

{ TUIStateRegistry }

constructor TUIStateRegistry.Create;
begin
  inherited Create (nil);
  DelayInterval := 250 * 1/MSecsPerDay;

  FRegisteredStates := TList <TAppState>.Create;
  FActiveStates := TList <TAppState>.Create;
  FRegisteredObservers := TObjectList <TRegisteredObserver>.Create (true);
  FDelayedStates := TList <TAppState>.Create;

  Timer := TTimer.Create (nil);
  Timer.Interval := 20;
  Timer.OnTimer := Ontimer;
end;

destructor TUIStateRegistry.Destroy;
begin
  Timer.Free;
  FRegisteredStates.Free;
  FActiveStates.Free;
  FRegisteredObservers.Free;
  FDelayedStates.Free;
  inherited;
end;

procedure TUIStateRegistry.RegisterObservers(const Elements: TArray<TAppStateObserver>; Action: TAppStateAction;
                                                      const Required, Forbidden: TAppStates);
begin
  for var Element in Elements do
     if Element <> nil then RegisterObserver (Element, Action, Required, Forbidden);
end;

procedure TUIStateRegistry.RegisterState(AppState: TAppState);
begin
  FRegisteredStates.Add (AppState);
end;

function TUIStateRegistry.TryFindRegisteredObserver(Element: TAppStateObserver; Action: TAppStateAction; var regel: TRegisteredObserver): boolean;
begin
  for var i := 0 to FRegisteredObservers.Count - 1 do
    if (FRegisteredObservers[i].Observer = Element) and (FRegisteredObservers[i].Action = Action) then
       begin
         regel := FRegisteredObservers[i];
         exit (true);
       end;
  result := false;
end;

procedure TUIStateRegistry.RegisterObserver  (Element: TAppStateObserver; Action: TAppStateAction; Required, Forbidden: TAppStates; RequiredType: TRequiredType = TRequiredType.rtAND; ForbiddenType: TForbiddenType = TForbiddenType.ftOR);
var
  State: TAppState;
  NewRegEl: boolean;
  regel : TRegisteredObserver;
begin
  if (Element = nil) then exit;

  for var i := high (Required) downto low (Required) do
    if Required[i] = nil then System.Delete (Required, i, 1);

  for var i := high (Forbidden) downto low (Forbidden) do
    if Forbidden[i] = nil then System.Delete (Forbidden, i, 1);

  if length (Required) + length (Forbidden) = 0 then exit;

  NewRegEl := not TryFindRegisteredObserver(Element, Action, regel);
  if NewRegEl
     then regel := TRegisteredObserver.Create(Element, Action, Required, Forbidden)
     else begin
            regel.Required := regel.Required + Required;
            regel.Forbidden := regel.Forbidden + Forbidden;
          end;

  regel.RequiredType := RequiredType;
  regel.ForbiddenType := ForbiddenType;

  for State in Required do
    begin
      State.Observers.Add(regel);
      if Element is TComponent
         then TComponent(Element).FreeNotification(State);
    end;

  for State in Forbidden do
    begin
      if State <> nil then
        begin
          State.Observers.Add(regel);
          if Element is TComponent
             then TComponent(Element).FreeNotification(State);
        end;
    end;

  if NewRegEl then
    begin
      FRegisteredObservers.Add(regel);
      if Element is TComponent then
        TComponent(Element).FreeNotification(self);
    end;

  if Action <> TAppStateAction.Notification
     then UpdateObserver(regel);
end;

procedure TUIStateRegistry.Activate(State: TAppState; ASet: boolean = true);
begin
  if not ASet
    then Deactivate(State)
    else
      begin
        State.CheckIfSingle;
        if not FActiveStates.Contains (State) //allow multiple setting but only set once
           then FAdd (State);
      end;
end;

procedure TUIStateRegistry.FAdd(State: TAppState);
begin
  {$IFDEF DEBUG}
//  if State.Name <> 'TaskRunning' then
//    if now=0 then exit;
  {$ENDIF}
  if State.Name = 'RealizationView_CU' then
    if now = 0 then exit;

  FActiveStates.Add(State);
  inc (State.FActiveCount);

  if State.Timing = TAppStateTiming.Immediate
    then UpdateUI(State)
    else AddDelay (State);

  if State.Child <> nil
     then FAdd (State.Child);
end;

procedure TUIStateRegistry.FRemove(State: TAppState);
begin
  if (FActiveStates.Remove(State) = -1)
     then if State.AllowMultiple
          then raise Exception.Create(format ('State %s not set', [State.Name]))
          else exit; // dont raise exception for single instance states

  dec (State.FActiveCount);
  if State.Timing = TAppStateTiming.Immediate
    then UpdateUI(State);

  if State.Child <> nil
     then FRemove (State.Child);
end;


procedure TUIStateRegistry.Deactivate(State: TAppState);
begin
  State.CheckIfSingle;
  FRemove (State);
end;

procedure TUIStateRegistry.Remove(State: TAppState);
begin
  State.CheckIfMultiple;
  FRemove (State);
end;

procedure TUIStateRegistry.Add(State: TAppState);
begin
  State.CheckIfMultiple;
  FAdd (State);
end;


procedure TUIStateRegistry.AddDelay(State: TAppState);
begin
  var index := FDelayedStates.IndexOf (State);
  if index >= 0
    then FDelayedStates.Move (index, FDelayedStates.Count-1)
    else FDelayedStates.Add (State);
  State.DelayedUntil := now + DelayInterval;
end;

function TUIStateRegistry.AsString: string;
begin
  result := 'Active States:'+ sLineBreak;
  for var st in FActiveStates do
    result := result + format ('[%s] ', [st.Name]);
  result := result + sLineBreak + sLineBreak + 'Registered elements:' + sLineBreak;
  for var regel in FRegisteredObservers do
    result := result + format ('[%s] ', [regel.Name]);
end;

procedure TUIStateRegistry.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if Operation = TOperation.opRemove
    then UnregisterObserver (AComponent);
end;

procedure TUIStateRegistry.Ontimer(Sender: TObject);
begin
//  var count := 0;
  var dt := now;

  if InTimer then exit;
  InTimer := true;

  try
    while (FDelayedStates.Count > 0) and (FDelayedStates[0].DelayedUntil < dt) do
      begin
        UpdateUI(FDelayedStates[0]);
        FDelayedStates.Delete (0);
      end;
  finally
    InTimer := false;
  end;

{  for var State in FDelayedStates do
    if State.DelayedUntil < now then
      begin
        UpdateUI(State);
        inc (count);
      end;
  FDelayedStates.DeleteRange(0, count);}
end;

procedure TUIStateRegistry.UpdateUI(State: TAppState);
begin
  for var RegEl in State.Observers do
    begin
      if RegEl.Action = TAppStateAction.Notification
        then
          begin
            var intf : IAppStateObserver;
            if Supports (RegEl.Observer, IAppStateObserver, intf)
              then intf.AppStateNotification(State); //todo hmph
          end
      else UpdateObserver (RegEl);
    end;
end;

procedure TUIStateRegistry.UnregisterObserver(Element: TAppStateObserver);
//var
//  regel: TRegisteredObserver;
begin
//  if FObserverToRegisteredObserver.TryGetValue (Element, regel) then
//    begin
//      FRegisteredObservers.Remove(regel);
//      FObserverToRegisteredObserver.Remove (Element);
//    end;

  for var i := FRegisteredObservers.Count - 1 downto 0 do
    if FRegisteredObservers[i].Observer = Element then
       FRegisteredObservers.Delete (i);
end;

procedure TUIStateRegistry.UnregisterState(AppState: TAppState);
begin
  FActiveStates.Remove(AppState);
  FRegisteredStates.Remove(AppState);
  FDelayedStates.Remove(AppState);
end;

procedure TUIStateRegistry.UpdateObserver (RegElement: TRegisteredObserver);
var
  outcome: boolean;
  state: TAppState;
begin
  if pos ('btnAddItem', RegElement.Name) > 0  then
    if now=0 then Exit;


  outcome := true;

  if RegElement.ForbiddenType = TForbiddenType.ftOR
    then for state in RegElement.Forbidden do
      begin
        if FActiveStates.Contains (state) then
          begin
            outcome := false;
            break;
          end
      end
    else for state in RegElement.Forbidden do // all must be present so if any is missing just break
      if not FActiveStates.Contains (state) then
         break;

  if outcome then
    if RegElement.RequiredType = TRequiredType.rtAND
      then for state in RegElement.Required do
        begin // outcome starts as true, if any state is not present switch to false and break
          if not FActiveStates.Contains (state) then
            begin
              outcome := false;
              break;
            end
        end
      else
        begin // one of required states must be true so set outcome to false until you find one
          outcome := false;
          for state in RegElement.Required do
            begin
              if FActiveStates.Contains (state) then
                begin
                  outcome := true;
                  break;
                end
            end
        end;

  case RegElement.Action of
    TAppStateAction.Visible, TAppStateAction.VisibleAndFocus:
      case RegElement.Kind of
        TAppStateObserverKind.Action: TAction(RegElement.Observer).Visible := outcome;
        TAppStateObserverKind.Control,
        TAppStateObserverKind.CustomEdit:
                                      begin
                                        TControl(RegElement.Observer).Visible := outcome;
                                        if outcome and (RegElement.Action = TAppStateAction.VisibleAndFocus)
                                          then FocusControlOrChild (TControl(RegElement.Observer));
                                      end;
        end;
    TAppStateAction.Enabled, TAppStateAction.EnabledAndFocus:
      case RegElement.Kind of
        TAppStateObserverKind.Action:     TAction(RegElement.Observer).Enabled := outcome;
        TAppStateObserverKind.Control,
        TAppStateObserverKind.CustomEdit:
                                      begin
                                        TControl(RegElement.Observer).Enabled := outcome;
                                        if outcome and (RegElement.Action = TAppStateAction.EnabledAndFocus)
                                           then FocusControlOrChild (TControl(RegElement.Observer));
                                      end;
      end;
    TAppStateAction.Clear:   if outcome then TCustomEdit(RegElement.Observer).Text := '';
    TAppStateAction.Notification: ; //done in the caller
  end;
end;

{ TAppState }

procedure TAppState.Activate;
begin
  if FSuspendCount > 0 then exit;

  UIStateRegistry.Activate (self);
end;

procedure TAppState.Deactivate;
begin
  if FSuspendCount > 0 then exit;

  UIStateRegistry.Deactivate(self);
end;

procedure TAppState.Add;
begin
  UIStateRegistry.Add(self);
end;

procedure TAppState.CheckIfMultiple;
begin
  if not AllowMultiple
    then raise Exception.Create(format ('State %s must be treated as single', [Name]));
end;

procedure TAppState.CheckIfSingle;
begin
  if AllowMultiple
    then raise Exception.Create(format ('State %s must be treated as multiple', [Name]));
end;

constructor TAppState.CreateAsSingle (const AName: string; ATiming: TAppStateTiming = TAppStateTiming.Immediate; AChildState: TAppState = nil);
begin
  inherited Create(nil);
  Name := AName;
  Timing := ATiming;
  Child := AChildState;
  Observers := TList <TRegisteredObserver>.Create;
  UIStateRegistry.RegisterState (self);
end;

constructor TAppState.CreateAsMultiple (const AName: string; AChildState: TAppState = nil);
begin
  inherited Create(nil);
  Name := AName;
  AllowMultiple := true;
  Child := AChildState;
  Observers := TList <TRegisteredObserver>.Create;
  UIStateRegistry.RegisterState (self);
end;

procedure TAppState.CycleOnOff;
begin
  Activate;
  Deactivate;
end;

destructor TAppState.Destroy;
begin
  for var RegElement in Observers do
    if RegElement.Observer is TComponent then TComponent(RegElement.Observer).RemoveFreeNotification(self);
  for var obs in Observers do
    obs.StateDestroyed (self);

  Observers.Free;
  UIStateRegistry.UnregisterState (self);
  inherited;
end;

function TAppState.GetActive: boolean;
begin
  result := FActiveCount > 0;
end;

procedure TAppState.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if Operation = TOperation.opRemove then
    if AComponent is TAppStateObserver then
      for var i := Observers.Count - 1 downto 0 do
        if Observers[i].Observer = AComponent then Observers.Delete (i);//.Remove(TAppStateObserver (AComponent)) do ;
end;

procedure TAppState.Remove;
begin
  UIStateRegistry.Remove(self);
end;

procedure TAppState.Resume;
begin
  dec (FSuspendCount);
end;

procedure TAppState.SetActiveIf(Condition: boolean);
begin
  if Condition
    then Activate
    else Deactivate;
end;

procedure TAppState.Suspend;
begin
  inc (FSuspendCount);
end;

{ TAppStateActionDetails }

constructor TRegisteredObserver.Create (AObserver: TAppStateObserver; AAction: TAppStateAction; const ARequired, AForbidden: TAppStates; const AName: string = '');
var
  intf: INamed;
begin
  inherited Create;
  if AName <> ''
    then Name := AName
    else if Supports (Observer, IAppStateObserver, intf)
      then Name := intf.GetName
    else if (AObserver is TComponent) and (TComponent (AObserver).Name <> '')
      then Name := TComponent (AObserver).Name
      else Name := TObject (AObserver).ClassName;

  Observer := AObserver;
  Action := AAction;
  Required := ARequired;
  Forbidden := AForbidden;

  if Observer is TAction
   then Kind := TAppStateObserverKind.Action
   else if Observer is TCustomEdit
   then Kind := TAppStateObserverKind.CustomEdit
   else if Observer is TControl
   then Kind := TAppStateObserverKind.Control
   else if Supports (Observer, IAppStateObserver)
   then Kind := TAppStateObserverKind.Callback
   else raise Exception.Create('Unexpected Observer type');
end;

destructor TRegisteredObserver.Destroy;
begin
  for var State in Required do
    if State <> nil then State.Observers.Remove(self);
  for var State in Forbidden do
    if State <> nil then State.Observers.Remove(self);

  inherited;
end;

procedure TRegisteredObserver.StateDestroyed(st: TAppState);
var
  i : integer;
begin
  for i := high (Required) downto low (Required) do
    if Required[i] = st then System.Delete(Required, i, 1);

  for i := high (Forbidden) downto low (Forbidden) do
    if Forbidden[i] = st then System.Delete(Forbidden, i, 1);
end;

procedure RegisterStateObserver  (Element: TAppStateObserver; Action: TAppStateAction; Required, Forbidden: TAppStates);
begin
  UIStateRegistry.RegisterObserver  (Element, Action, Required, Forbidden);
end;

procedure RegisterStateObservers (const Elements: TArray<TAppStateObserver>; Action: TAppStateAction; const Required, Forbidden: TAppStates);
begin
  UIStateRegistry.RegisterObservers (Elements, Action, Required, Forbidden);
end;

procedure UnregisterStateObserver (Element: TAppStateObserver);
begin
  UIStateRegistry.UnregisterObserver (Element);
end;

{ TNotifyEventAppState }

procedure TNotifyEventAppState.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if (AComponent = FComponent) and (Operation = TOperation.opRemove) then
      Free;
end;

{ TFormClosedAppState }

class constructor TFormClosedAppState.Create;
begin
  inherited;
  FDictionary := TDictionary <TForm, TFormClosedAppState>.Create;
end;

class destructor TFormClosedAppState.Destroy;
begin
  FDictionary.Free;
  inherited;
end;

class function TFormClosedAppState.FindOrCreateFor(AForm: TForm): TFormClosedAppState;
begin
  if not FDictionary.TryGetValue (AForm, result) then
    begin
      result := TFormClosedAppState.Create(AForm, AForm);
      FDictionary.Add(AForm, result);
    end;
end;

constructor TFormClosedAppState.Create(AOwner: TComponent; AForm: TForm);
begin
  inherited CreateAsSingle (AOwner.Name+'.OnClose');
  FComponent := AForm;
  FComponent.FreeNotification(self);

  FOriginal := AForm.OnClose;
  AForm.OnClose := FHandler;
end;

destructor TFormClosedAppState.Destroy;
begin
  FDictionary.Remove (TForm(FComponent));
  if FComponent <> nil
     then FComponent.RemoveFreeNotification(self);
  inherited;
end;

procedure TFormClosedAppState.FHandler(Sender: TObject; var Action: TCloseAction);
begin
  if Assigned (FOriginal)
     then FOriginal (Sender, Action);
  if Action in [TCloseAction.caHide, TCloseAction.caFree] then
     CycleOnOff;
end;

class function TFormClosedAppState.FindOrCreateFor(AControl: TControl): TFormClosedAppState;
begin
  {$IFDEF FIREMONKEY}
  result := FindOrCreateFor(TForm (AControl.Root.GetObject));
  {$ELSE}
  result := FindOrCreateFor(GetParentForm(AControl));
  {$ENDIF}
end;

initialization
  UIStateRegistry := TUIStateRegistry.Create;

finalization
  FreeAndNil (UIStateRegistry);

end.
