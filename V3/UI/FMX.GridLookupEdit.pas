unit FMX.GridLookupEdit;

{TODO -oOwner -cGeneral : model has Fetching but lookup edit grid has child model which can also have fetching active,
in theory parent is also fetching is child is fetching?}

interface

uses FMX.Edit, FMX.Grid, FMX.Models, FMX.Edit.Win, System.Classes, System.UITypes, System.Types,
     SysUtils, FMX.Types, FMX.Controls, FMX.Forms, FMX.StdCtrls, FMX.ImgList, FMX.GridModel, math, System.Messaging,
     syncommons, mORMot,
     MormotTypes, MormotMods, SqlRecords, Common.Utils,
     FMX.Common.Utils,
     UI.StateManagement, ObservableController, FMX.BaseModel;

type

  TGridLookupEditOption = (ShowGridOnEnter, FormAsGridParent);
  TGridLookupEditOptions = set of TGridLookupEditOption;

  TGridLookupEdit = class (TLookupEditModel, IRecordsObserver, IFreeNotification)
  private
    class var
      MessageManager: TMessageManager;
//    procedure HideGridIfNeeded(const Sender: TObject; const M: TMessage);
    procedure HideGridIfNeeded;
    function CreateButton: TSpeedButton;
    procedure SetContent(const s: string);
    procedure ForceRefresh;
    function GetSelectedIDIfVisible: TID;
    var
    FGridAlignment: TAlignLayout;
    FGridHeight: integer;
    IgnoreEditEnter: boolean;
    procedure SetGridBounds;
  protected
    FClearButton, FAddButton : TSpeedButton;
    FGrid: TStringGrid;
    FGridModel: TGridModel;
    FMasterSqlRecordClass: TSQLRecordClass;
    FMasterForeignKeyName: string;
    OriginalOnEditKeyUp: TKeyEvent;
    IgnoreMouseUp: TDateTime;
    function GetSelectedRecord: TSqlRecord; override;
    procedure ClearList; override;
    procedure OnChange (Sender: TObject); override;
    procedure OnEditResize (Sender: TObject);
    procedure OnEditExit (Sender: TObject);
    procedure OnEditClick (Sender: TObject);
    procedure OnEditMouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
//    procedure OnEditMouseDown (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure OnEditKeyUp (Sender: TObject; var Key: word; var KeyChar: char; Shift: TShiftState);

    procedure OnGridCellClick (const Column: TColumn; const Row: Integer);
    procedure OnGridEnter (Sender: TObject);
    procedure OnGridExit (Sender: TObject);
    procedure OnGridKeyUp (Sender: TObject; var Key: word; var KeyChar: char; Shift: TShiftState);
    procedure OnGridKeyDown (Sender: TObject; var Key: word; var KeyChar: char; Shift: TShiftState);

    procedure OnBtnClearClick (Sender: TObject);
    procedure OnBtnAddClick (Sender: TObject);

    procedure DoOnEnter; override;

    function GeneralFilter: string; override;
    procedure SetImageList(const Value: TImageList); override;
    procedure AfterSetSelectedID; override;
  public
    Options: TGridLookupEditOptions;
    apsContentChanged: TAppState; // state is toggled when the edit text is changed to reflect a record or nil, typing doesnt matter

    class constructor Create;
    class destructor Destroy;
    constructor Create (ASqlRecordClass: TSQLRecordClass; AEdit: TCustomEdit; AController: TObservableController;
                        AMasterSqlRecordClass: TSQLRecordClass; AMasterForeignKeyName: string = ''); reintroduce;
    destructor Destroy; override;

    procedure Populate; override;
    procedure ObserveRecords(Task: TRecordsTask);
    procedure AppStateNotification (const State: TAppState); override;
    procedure FreeNotification(AObject: TObject); override;
    procedure CreateAddButton;
    procedure CreateClearButton;
    procedure ClearSelection; override;
    procedure FreeGrid;
    procedure ShowGrid;
    procedure HideGrid;

    property GridAlignment: TAlignLayout read FGridAlignment write FGridAlignment;
    property GridModel: TGridModel read FGridModel;
    property ClearButton : TSpeedButton read FClearButton;
    property AddButton : TSpeedButton read FAddButton;
    property SelectedIDIfVisible: TID read GetSelectedIDIfVisible; // return SelectedID if associated Edit is visible otherwise NULL
  end;

  TGridLookupEdit<T: TSqlRecord> = class (TGridLookupEdit)
  protected
    function GetSelectedRecord: T; reintroduce;
  public
    constructor Create (AEdit: TCustomEdit; AController: TObservableController;
                        AMasterSqlRecordClass: TSQLRecordClass; AMasterForeignKeyName: string = ''); reintroduce;

    property SelectedRecord: T read GetSelectedRecord;
  end;

implementation

uses Validation;

{ TGridLookupEdit<T> }

procedure TGridLookupEdit.AppStateNotification(const State: TAppState);
begin
  inherited;
  if (State = FGridModel.apsSelectedIDChanged) then
    begin
      {if not FGridModel.Fetching and (SelectedID <> FGridModel.SelectedID)
               then }SelectedID := FGridModel.SelectedID;
    end;
//  if FGrid.IsFocused
//     then FCustomEdit.SetFocus;
//  if FGridModel.SelectedID.IsNotNull
//     then HideGrid;
end;

procedure TGridLookupEdit.ClearList;
begin
  inherited;
  FGrid.RowCount := 0;
  HideGrid;
end;

class constructor TGridLookupEdit.Create;
begin
  MessageManager := TMessageManager.Create;
end;

//procedure TGridLookupEdit.HideGridIfNeeded (const Sender: TObject; const M: TMessage);
//begin
//  //ShowMessage((M as TMessage<UnicodeString>).Value);
//  if (sender = self) and not FGrid.IsFocused and not FCustomEdit.IsFocused
//     then HideGrid;
//  if (sender = self) and not FGrid.IsFocused and not FCustomEdit.IsFocused and (FCustomEdit.Text <> '')
//     and SelectedID.IsNULL
//     then SetContent ('');
//end;

procedure TGridLookupEdit.HideGridIfNeeded;
begin
  if not FGrid.IsFocused and not FCustomEdit.IsFocused
     then HideGrid;
  if not FGrid.IsFocused and not FCustomEdit.IsFocused and (FCustomEdit.Text <> '')
     and SelectedID.IsNULL
     then SetContent ('');
end;


const Button_Size = 30;

function TGridLookupEdit.CreateButton: TSpeedButton;
begin
  FCustomEdit.Margins.Right := FCustomEdit.Margins.Right + Button_Size;
  result := TSpeedButton.Create(FCustomEdit);
  result.Parent := FCustomEdit{.Parent};
  result.Images := ImageList;
  result.Cursor := crArrow;
  result.StyleLookup := 'stoptoolbutton';
  result.BringToFront;
//  result.SetBounds(0, 0, Button_Size, Button_Size);
//  result.SetBounds(FCustomEdit.Position.X + FCustomEdit.Size.Width + 3, FCustomEdit.Position.Y, Button_Size, Button_Size);
  result.SetBounds(FCustomEdit.Width + FCustomEdit.Margins.Left, 0, Button_Size, Button_Size);
  result.Anchors := [TAnchorKind.akRight, TAnchorKind.akTop];
//  result.Align := TAlignLayout.Right;
end;

procedure TGridLookupEdit.CreateClearButton;
begin
  FClearButton := CreateButton;
//  FClearButton.Margins.Right := - FCustomEdit.Margins.Right;
  FClearButton.ImageIndex := Controller.dicImageIndexes['clear'];
  FClearButton.OnClick := OnBtnClearClick;
  FClearButton.ShowHint := true;
  FClearButton.Hint := 'Poni�ti izbor';
end;

procedure TGridLookupEdit.CreateAddButton;
begin
  FAddButton := CreateButton;
//  FAddButton.Margins.Right := - FCustomEdit.Margins.Right;
  FAddButton.ImageIndex := Controller.dicImageIndexes['create'];
  FAddButton.OnClick := OnBtnAddClick;
  FAddButton.ShowHint := true;
  FAddButton.Hint := 'Dodaj';
  if FClearButton <> nil
    then FAddButton.Position.X := FAddButton.Position.X + Button_Size;
end;

constructor TGridLookupEdit.Create (ASqlRecordClass: TSQLRecordClass; AEdit: TCustomEdit; AController: TObservableController;
                                    AMasterSqlRecordClass: TSQLRecordClass; AMasterForeignKeyName: string = '');
begin
  AEdit.Text := '';
  AEdit.StyleLookup := 'comboeditstyle';
  inherited Create (ASqlRecordClass, 'gle' + AEdit.Owner.Name + '.' + AEdit.Name, AController);
  apsContentChanged := TAppState.CreateAsSingle (name+'.apsContentChanged');

  FMasterSqlRecordClass := AMasterSqlRecordClass;
  if AMasterForeignKeyName = ''
     then FMasterForeignKeyName := SqlRecordClass.crudSQLTableName.AsString + 'ID'
     else FMasterForeignKeyName := AMasterForeignKeyName;
  TargetFieldName := StringToUTF8 (FMasterForeignKeyName);
  Options := [ShowGridOnEnter, FormAsGridParent];
  FGridHeight := 300;
  FGridAlignment := TAlignLayout.Client;
  AEdit.OnResize := OnEditResize;
  AEdit.OnExit := OnEditExit;
  OriginalOnEditKeyUp := AEdit.OnKeyUp;
  AEdit.OnKeyUp := OnEditKeyUp;
  AEdit.OnClick := OnEditClick;
  AEdit.OnMouseUp := OnEditMouseUp;
//  AEdit.OnMouseDown := OnEditMouseDown;
  AEdit.AddFreeNotify(self);
  CustomEdit := AEdit;
  FExpLookup := true;

  FGrid := TStringGrid{TGLEGrid}.Create (AEdit.Owner);
  FGrid.Name := 'grd' + AEdit.Name;
  FGridModel := TGridModel.Create (ASqlRecordClass, 'gleg' + ASqlRecordClass.ClassName, AController);
  FGridModel.Brief := true;
  FGridModel.Options := FGridModel.Options - [TGridModelOption.Statistics];
  FGridModel.Grid := FGrid;
//  Exclude (FGridModel.Options, TGridModelOption.RestoreSelected);

  HideGrid;
  FGrid.TabOrder := AEdit.TabOrder + 1;
  FGrid.ReadOnly := true;
  FGrid.Options := FGrid.Options + [TGridOption.RowSelect] - [TGridOption.Tabs];

  FGrid.OnCellClick := OnGridCellClick;
  FGrid.OnKeyDown := OnGridKeyDown;
  FGrid.OnKeyUp := OnGridKeyUp;
  FGrid.OnEnter := OnGridEnter;
  FGrid.OnExit := OnGridExit;
  FGrid.TabStop := false;
  //SetGridBounds;

  RegisterStateObserver (self, TAppStateAction.Notification, [FGridModel.apsSelectedIDChanged], []);

//  {SubscriptionId := }MessageManager.SubscribeToMessage(TMessage<string>, HideGridIfNeeded);

  if (FMasterSqlRecordClass <> nil)
     //and not PropertyHasAttribute (FMasterSqlRecordClass, FMasterForeignKeyName, NotNullAttribute)
     then begin
            CreateClearButton;
//            if (FMasterSqlRecordClass <> SqlRecordClass)
//               then CreateAddButton;
          end;
end;

procedure TGridLookupEdit.FreeGrid;
begin
  FGrid.Free;
  FGrid := nil;
end;

destructor TGridLookupEdit.Destroy;
begin
  CustomEdit := nil;
  apsContentChanged.Free;
  // edit owns grid, it will free it and then the free notification will kill the grid model
  // edit owns grid so that saving grid settings has a naming hierarchy
  // FGrid.Free;
  // FGridModel.Free;
  inherited;
end;

procedure TGridLookupEdit.SetGridBounds;
var
  AvailableWidth: single;
begin
  var ParentForm := GetParentForm(FCustomEdit);
  var AbsoluteTopLeft := CustomEdit.LocalToAbsolute(Point(0, 0));
  var TopLeft := CustomEdit.Position;

  if TGridLookupEditOption.FormAsGridParent in Options
    then FGrid.Position.Y := AbsoluteTopLeft.Y + CustomEdit.Height + 5
    else FGrid.Position.Y := CustomEdit.Position.Y + CustomEdit.Height + 5;
  //FGrid.Height := min (FGridHeight, FGrid.Parent.Height - FGrid.Position.Y);

  FGrid.Height := min (
                            max (100, ParentForm{FGrid.ParentControl}.Height - FGrid.Position.Y - 50),
                            (FGrid.RowCount + 3) * FGrid.Model.EffectiveRowHeight
                      );

  case GridAlignment of
    TAlignLayout.Client:
      begin
        AvailableWidth := ParentForm.ClientWidth - 40;
        FGrid.Position.X := - AbsoluteTopLeft.X + 20;
        FGrid.Width := AvailableWidth;
      end;
    TAlignLayout.Fit:
      begin
        FGrid.Position.X := CustomEdit.Position.X;
        FGrid.Width := CustomEdit.Width;
      end;
    TAlignLayout.Left:
      begin
        AvailableWidth := ParentForm.ClientWidth - 20 - round(ParentForm.ClientWidth - AbsoluteTopLeft.X + CustomEdit.Width);
        FGrid.Position.X := CustomEdit.Position.X + CustomEdit.Width - AvailableWidth + 20;
        FGrid.Width := AvailableWidth;
      end;
    TAlignLayout.Right:
      begin
        //var AvailableWidth := ParentForm.ClientWidth -  AbsoluteTopLeft.X - 20;
        if TGridLookupEditOption.FormAsGridParent in Options
          then begin
                 FGrid.Position.X := AbsoluteTopLeft.X + 2;
                 if CustomEdit.Parent is TControl
                    then AvailableWidth := TControl(CustomEdit.Parent).Width
                    else if CustomEdit.Parent is TCustomForm
                    then AvailableWidth := TCustomForm(CustomEdit.Parent).ClientWidth
                    else AvailableWidth := 0;

                 AvailableWidth :=  AvailableWidth - ({FGrid.Position.X - } {Absolute} TopLeft.X) - 10;
               end
          else begin
                 FGrid.Position.X := CustomEdit.Position.X + 2;
                 AvailableWidth :=  TControl(CustomEdit.Parent).Width - FGrid.Position.X;
               end;

        FGrid.Width := AvailableWidth;
      end;
  end;
//  SetBounds(0, CustomEdit.Height, CustomEdit.Width,  FGrid.DefaultSize Canvas. RowHeight * 20);
end;

procedure TGridLookupEdit.SetImageList(const Value: TImageList);
begin
  inherited;
  if FClearButton <> nil then FClearButton.Images := Value;
  if FAddButton   <> nil then FAddButton.Images := Value;
end;

class destructor TGridLookupEdit.Destroy;
begin
  MessageManager.Free;
end;

procedure TGridLookupEdit.DoOnEnter;
begin
  inherited;

  if (SelectedID = NULL_ID) and not FetchIfEmpty
     then FGridModel.Records := nil;

  SetGridBounds;
  if not IgnoreEditEnter and (FGrid.RowCount > 0) and SelectedID.IsNull
     and (ShowGridOnEnter in Options)
     then begin
            ShowGrid;
            if IsLeftMouseButtonDown {IsMouseDown }
               then IgnoreMouseUp := now;
          end;
  IgnoreEditEnter := false;
end;

procedure TGridLookupEdit.FreeNotification(AObject: TObject);
begin
  if AObject = CustomEdit
     then begin
            CustomEdit := nil;
            Free;
          end;
end;

function TGridLookupEdit.GeneralFilter: string;
var
  p: integer;
begin
  result := FCustomEdit.Text;
  // we are not doing simple replace because delimiter is 3 characters not 1
  // and there is no builting function to replace substring
  while true do
    begin
      p := pos (BRIEF_FIELD_DELIMITER, result);
      if p = 0 then break;
      system.Delete (result, p, 3);
    end;
end;

function TGridLookupEdit.GetSelectedIDIfVisible: TID;
begin
  if FCustomEdit.VisibleIncludingParents
     then result := SelectedID
     else result := NULL_ID;
end;

function TGridLookupEdit.GetSelectedRecord: TSqlRecord;
begin
  result := GridModel.SelectedRecord;
end;

procedure TGridLookupEdit.ObserveRecords(Task: TRecordsTask);
begin
  ApsFetching.Deactivate;

  if name = 'glefraInputControl.edtContent'
     then if now = 0 then exit;

  NonFilteredFetchWasEmpty := (Task.QueryParameters.Filter.Count = 0)
                              {and  FetchingBecauseEmpty }
                              and (Task.QueryParameters.GeneralFilter = '')
                              and FGridModel.Records.IsEmpty;

  FGridModel.Records := Task.SqlRecords.CreateCopy;
  if SelectedID.IsNotNull and FGridModel.SelectedID.IsNull
     then begin
            FGridModel.SelectedID := SelectedID;
          end;

  if SelectedID.IsNull and FGridModel.SelectedID.IsNotNull
    then SelectedID := FGridModel.SelectedID;

  if FGridModel.SelectedRecord.IsAssigned
     then SetContent (FGridModel.SelectedRecord.AsBriefString)
     else if SelectedID.IsNotNull
          then SetContent ('');

  SetGridBounds;
  if not FGrid.Visible and CustomEdit.IsFocused and SelectedID.IsNull
     then ShowGrid;
end;

procedure TGridLookupEdit.OnChange(Sender: TObject);
begin
  if Assigned (OriginalOnChange) then
     OriginalOnChange (FCustomEdit);
end;

procedure TGridLookupEdit.OnBtnAddClick(Sender: TObject);
begin
  GridModel.CreateRecord;
end;

procedure TGridLookupEdit.OnBtnClearClick(Sender: TObject);
begin
  ClearSelection;
  FCustomEdit.SetFocus;
//  IgnoreEditEnter := false;
end;

procedure TGridLookupEdit.ClearSelection;
begin
  inherited;
  FGridModel.SelectedID := NULL_ID;
  PostponeFetch;
end;

procedure TGridLookupEdit.OnEditClick(Sender: TObject);
begin

end;

procedure TGridLookupEdit.OnEditExit(Sender: TObject);
begin
//  FGrid.HideIfNotFocused;
  // sadly we dont know what will get focused next...
  //FGrid.Visible := FGrid.IsFocused;
//  if SelectedID.IsNull and (FCustomEdit.Text <> '')
//     then FCustomEdit.Text := '';

  PostponeOnce (HideGridIfNeeded, 100);
//  aa
//  var mes := TMessage<string>.Create ('');
//  MessageManager.PostMessage(self, mes);
end;

procedure TGridLookupEdit.OnEditKeyUp(Sender: TObject; var Key: word; var KeyChar: char; Shift: TShiftState);
begin
  case Key of
    vkDown: begin
              ShowGrid;
              FGrid.SetFocus;
              if FGrid.Row < FGrid.RowCount - 1
                 then FGrid.Row := FGrid.Row + 1;
            end;
    vkLeft: if ssCtrl in Shift then
            begin
              FGridAlignment := TAlignLayout.Left;
              SetGridBounds;
              ShowGrid;
            end;
    vkRight: if ssCtrl in Shift then
            begin
              FGridAlignment := TAlignLayout.Right;
              SetGridBounds;
              ShowGrid;
            end;
    vkUp:   if ssCtrl in Shift then
            begin
              FGridAlignment := TAlignLayout.Client;
              SetGridBounds;
              ShowGrid;
            end;
    vkEscape:
            begin
              HideGrid;
            end;
    vkF5: begin
           if Shift = [ssCtrl] then
              GridModel.SaveAndRePrepareGrid;

            ForceRefresh;
          end;
    vkF6: FGrid.AutoColumnWidths;
    vkReturn:if SelectedID.IsNull and GridModel.SelectedID.IsNotNULL
               then begin
                      SelectedID := GridModel.SelectedID;
                      HideGrid;
                    end
    else if Shift = [ssCtrl, ssShift] then
      begin
        if (Key >= ord ('1')) and (Key <= ord ('5'))
           then SelectedID := Key - ord ('0');
      end;
  end;

  if assigned (OriginalOnEditKeyUp)
    then OriginalOnEditKeyUp (Sender, Key, KeyChar, Shift);
end;

procedure TGridLookupEdit.ForceRefresh;
begin
  NonFilteredFetchWasEmpty := false;
  Fetch;
end;

//procedure TGridLookupEdit.OnEditMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
//begin
//  if not FCustomEdit.IsFocused and (X > FCustomEdit.Width - 25)
//     then IgnoreEditEnter := true;
//end;

procedure TGridLookupEdit.OnEditMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  //if MillisecondsSince (IgnoreMouseUp) < 100 then exit;
  if IgnoreMouseUp > 0 then
    begin
      IgnoreMouseUp := 0;
      exit;
    end;

  if (X > FCustomEdit.Width - 25) then
     if FGrid.Visible
        then HideGrid
        else if (FGrid.RowCount > 0)
             then ShowGrid;
end;

procedure TGridLookupEdit.OnEditResize(Sender: TObject);
begin
  SetGridBounds;
end;

procedure TGridLookupEdit.OnGridCellClick(const Column: TColumn; const Row: Integer);
begin
  if FGridModel.SelectedRecord <> nil then
    begin
      SelectedID := FGridModel.SelectedID;
      HideGrid;
      IgnoreEditEnter := true;
      FCustomEdit.SetFocus;
    end;
end;

procedure TGridLookupEdit.OnGridEnter(Sender: TObject);
begin
  ShowGrid;
end;

procedure TGridLookupEdit.OnGridExit(Sender: TObject);
begin
  var mes := TMessage<string>.Create ('');
  MessageManager.SendMessage(self, mes);
end;

procedure TGridLookupEdit.OnGridKeyDown(Sender: TObject; var Key: word; var KeyChar: char; Shift: TShiftState);
begin
  if not (Key in [vkReturn, vkDown, vkUp])
     then FCustomEdit.SetFocus;
  case Key of
    vkEscape : FGridModel.SelectedID := NULL_ID;
    vktab:    begin
                key := 0;
                FocusNextControl (FCustomEdit);
              end;
  end;
end;

procedure TGridLookupEdit.OnGridKeyUp(Sender: TObject; var Key: word; var KeyChar: char; Shift: TShiftState);
begin
  case Key of
    vkReturn: begin
                SelectedID := FGridModel.SelectedID;
                HideGrid;
                IgnoreEditEnter := true;
                FCustomEdit.SetFocus;
              end;
  end;
end;

procedure TGridLookupEdit.Populate;
begin
  inherited;
end;

procedure TGridLookupEdit.SetContent (const s: string);
begin
  if FCustomEdit.Text = s then exit;
  FCustomEdit.Text := s;
  apsContentChanged.CycleOnOff;
end;

//function TGridLookupEdit.WaitingToFetch: boolean;
//begin
//  result := FGridModel.SelectionCandidates.Count > 0;
//end;

procedure TGridLookupEdit.AfterSetSelectedID;
begin
  if FGridModel.ContainsID (SelectedID) or SelectedID.IsNull
    then FGridModel.SelectedID := SelectedID; // dont set an ID that is missing because it will cause a fetch by grid model, mb grid model should not autofetch...

  inherited; // will fetch if ID is not null and that record is not already selected

  if FTyping
     then PostponeFetch
     else if SelectedRecord.IsAssigned
          then SetContent (SelectedRecord.AsBriefString)
          else SetContent ('');

(*
  if not FTyping then
    if (PreviousSelectedID <> SelectedID) or (SelectedID.IsNull and not FCustomEdit.IsFocused)
     then SetContent ('');
  if SelectedID.IsNotNull or (FetchIfEmpty and not NonFilteredFetchWasEmpty)
     then begin
            FetchingBecauseEmpty := {ID.IsNull and} (FCustomEdit.Text = '');
            if SelectedID.IsNotNull
               then FGridModel.SelectionCandidates.Add(SelectedID);
            if (FMaster <> nil) and (FMaster.apsFetching.Active)
               then
               else PostponedFetch;
          end
     else if (PreviousSelectedID <> SelectedID)
          then GridModel.Records := nil;
*)
end;

procedure TGridLookupEdit.HideGrid;
begin
  FGrid.Visible := false;
end;

procedure TGridLookupEdit.ShowGrid;
begin
  if FCustomEdit.ReadOnly then exit;

  SetGridBounds;
  if FGrid.Columns[0].Width = 0
     then FGrid.AutoColumnWidths;

  if TGridLookupEditOption.FormAsGridParent in Options
     then FGrid.Parent := GetParentForm(FCustomEdit)
     else FGrid.Parent := FCustomEdit.Parent;

  FGrid.BringToFront;
  FGrid.Parent.BringToFront;
  FGrid.Visible := true;
end;

//procedure TGridLookupEdit.ToggleGrid;
//begin
//  if FGrid.Visible then HideGrid else ShowGrid;
//end;

{ TGridLookupEdit<T> }

constructor TGridLookupEdit<T>.Create(AEdit: TCustomEdit; AController: TObservableController;
            AMasterSqlRecordClass: TSQLRecordClass; AMasterForeignKeyName: string);
begin
  inherited Create (T, AEdit, AController, AMasterSqlRecordClass, AMasterForeignKeyName);
end;

function TGridLookupEdit<T>.GetSelectedRecord: T;
begin
  result := T(inherited GetSelectedRecord);
end;

end.
