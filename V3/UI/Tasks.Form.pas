unit Tasks.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Generics.Collections, FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  SyncObjs, System.Rtti, FMX.Grid.Style, FMX.TabControl, FMX.Objects, FMX.Memo.Types,
  FMX.StdCtrls, FMX.Memo, FMX.Grid, FMX.Controls.Presentation, FMX.ScrollBox,
  mormotMods, MormotTypes, SynCommons,
  UI.StateManagement, PersistentUserSettings, Common.Utils,
  ObservableController, Controller.Tasks, LBLogger, TypInfo, FMX.Edit;

type

  TTaskLogger = class (TInterfacedObject, IServerObserver, ITaskStateObserver)
    FLogFile: TLogFile;
    procedure Write (const s: string);

    //procedure LogTask (Task: TObservableTask; const Action: string; const Content: string = ''); overload;

    procedure ObserveTaskState (Task: TObservableTask);
    function GetName: string;
//    function ObservesOwnTasksOnly: boolean;

    procedure Log (const Content: string); overload;
    procedure SetIdle (Idle: boolean);
    function FileName: string;
    procedure Clear;
    constructor Create;
    destructor Destroy; override;
  end;

  TfrmTasks = class (TForm, IServerObserver, ITaskStateObserver, IActivityObserver, IForeignTaskObserver,
                            IControllerTaskObserver)
    tacMain: TTabControl;
    taiTasks: TTabItem;
    sgr: TStringGrid;
    stcTime: TTimeColumn;
    stcID: TIntegerColumn;
    stcClass: TStringColumn;
    StringColumn2: TStringColumn;
    stcParameters: TStringColumn;
    stcOutcome: TStringColumn;
    memData: TMemo;
    recIdle: TRectangle;
    Timer1: TTimer;
    TabItem2: TTabItem;
    memUserSettings: TMemo;
    Timer2: TTimer;
    chbAutoScroll: TCheckBox;
    stcSubject: TStringColumn;
    chbIgnorePing: TCheckBox;
    TabItem3: TTabItem;
    memObservers: TMemo;
    Button1: TButton;
    StringColumn1: TStringColumn;
    taiCustomRequests: TTabItem;
    memCustomResponse: TMemo;
    Label1: TLabel;
    edtCustomRequest: TEdit;
    Label2: TLabel;
    procedure sgrSelChanged(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure edtCustomRequestKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
  private
    FForeignTaskObserver: IForeignTaskObserver;

    type
      TLogItem = record
        TaskID: cardinal;
        State: TObservableTask.TState;
        Content, Params, ClassName, Subject, Operation: string;
        Outcome: TOutcome;
      end;

    var
      cs: TCriticalSection;
      FController: TObservableController;

    //intf impl
    procedure ObserveTaskState (Task: TObservableTask);
    procedure ObserveTask (Task: TObservableTask);
    procedure ObserveActivity (const Busy: boolean);
    function GetName: string;
    // end

    function FindRow (TaskID: cardinal): integer;
    //procedure DoLog(TaskID: cardinal; const ClassName, Action, Content, Parameters: string; const Outcome: TOutcome);
    procedure DoLog(const LogItem: TLogItem);
    //procedure AppStateCallback (const State: TAppState);

    property ForeignTaskObserver: IForeignTaskObserver read FForeignTaskObserver implements IForeignTaskObserver;
  public
    LogItems: TList<TLogItem>;
    procedure SetController (AController: TObservableController);
    class procedure CreateInstance;
  end;

var
  frmTasks: TfrmTasks;
  TaskLogger : TTaskLogger;

implementation

{$R *.fmx}

const
  sgc_Time = 0;
  sgc_ID = 1;
  sgc_Class = 2;
  sgc_Subject = 3;
  sgc_Operation = 4;
  sgc_Life = 5;
  sgc_Parameters = 6;
  sgc_Outcome = 7;

procedure TfrmTasks.FormCreate(Sender: TObject);
begin
  FForeignTaskObserver := TForeignTaskObserver.Create (true);
  LogItems := TList<TLogItem>.Create;
  cs := TCriticalSection.Create;
  SetBounds(0, round(Screen.WorkAreaHeight) - Height, round(Screen.Width), Height);
  tacMain.ActiveTab := taiTasks;
end;

procedure TfrmTasks.FormDestroy(Sender: TObject);
begin
  frmTasks := nil;
  cs.Free;
  LogItems.Free;

  if FController <> nil
     then FController.Unsubscribe(self);
end;

function TfrmTasks.GetName: string;
begin
  result := Name;
end;

function TfrmTasks.FindRow (TaskID: cardinal): integer;
var
  r: integer;
  c: cardinal;
begin
  for r := sgr.RowCount-1 downto 0 do
    if  TryStrToUInt (sgr.Cells[stcID.Index, r], c)
        and (c = TaskID) then exit (r);
  result := -1;
end;

procedure TfrmTasks.Button1Click(Sender: TObject);
begin
//  memObservers.ClearContent;
//  for var i := 0 to FController.Observers.Count - 1 do
//    memObservers.Lines.Add(FController.Observers[i].Name);
end;

class procedure TfrmTasks.CreateInstance;
begin
  if frmTasks = nil
    then Application.CreateForm(TfrmTasks, frmTasks);
end;

procedure TfrmTasks.DoLog(const LogItem: TLogItem);
begin
  if frmTasks = nil then exit;

  var row := FindRow (LogItem.TaskID);
  if row < 0 then
    begin
      row := sgr.RowCount;
      sgr.RowCount := row + 1;
      sgr.Cells[sgc_Time, row] := timetostr (now);
      sgr.Cells[sgc_ID, row] := inttostr (LogItem.TaskID);
    end;

  if sgr.Cells[sgc_Class, row] = ''
     then sgr.Cells[sgc_Class, row] := LogItem.ClassName;
  if sgr.Cells[sgc_Subject, row] = ''
     then sgr.Cells[sgc_Subject, row] := LogItem.Subject;
  if sgr.Cells[sgc_Operation, row] = ''
     then sgr.Cells[sgc_Operation, row] := LogItem.Operation;

//  com := sgr.FindComponent(LogItem.Action);
//  if com = nil then
//    begin
//      colAction := TStringColumn.Create(self);
//      colAction.Header := LogItem.Action;
//      colAction.Name := LogItem.Action;
//      sgr.InsertObject(sgr.ColumnCount-2, colAction);
//    end
//    else colAction := TStringColumn (com);

//  sgr.Cells[colAction.Index, row] := LogItem.Action;
//  if LogItem.Content <> '' then
//    sgr.Cells[colAction.Index, row] := sgr.Cells[colAction.Index, row] + '[' + LogItem.Content + ']';

  sgr.Cells[sgc_Life, row] := sgr.Cells[sgc_Life, row] + copy (TRttiEnumerationType.GetName (LogItem.State), 1, 3) + ' ';
  if LogItem.Content <> '' then
    sgr.Cells[sgc_Life, row] := sgr.Cells[sgc_Life, row] + '[' + LogItem.Content + '] ';

  if LogItem.State = TObservableTask.TState.Executing then
    if LogItem.Params <> ''
       then sgr.Cells[sgc_Parameters, row] := LogItem.Params;
  if LogItem.State = TObservableTask.TState.Finished then
    sgr.Cells[sgc_Outcome, row] := LogItem.Outcome.ToString;
  if LogItem.State = TObservableTask.TState.Destroying then
    sgr.Cells[sgc_ID, row] := '('+sgr.Cells[sgc_ID, row]+')';

  if chbAutoScroll.IsChecked then
    begin
      sgr.SelectCell(sgc_ID, row);
      sgr.ScrollToSelectedCell;
    end;
end;

procedure TfrmTasks.edtCustomRequestKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
  if Key = vkReturn then
    begin
      FController.CustomRequest(edtCustomRequest.Text, varNull, '', self);
      memCustomResponse.Lines.Clear;
      memCustomResponse.Lines.Add ('> ' + edtCustomRequest.Text);
      edtCustomRequest.Text := '';
    end;
end;

procedure TfrmTasks.ObserveTask(Task: TObservableTask);
begin
  if (Task.Initiator = self as IServerObserver) and (Task is TCustomRequestTask) then
    begin
      var t := TCustomRequestTask(Task);
      memCustomResponse.Text := t.ResponseContent.AsString + #13#13 + t.Outcome.ToString;
      t.Outcome.ErrorHandled := true;
    end;
end;

procedure TfrmTasks.ObserveTaskState (Task: TObservableTask);//LogTask (Task: TebTask; const Action: string; const Content: string = '');
var
  li: TLogItem;
begin
  if self = nil then exit;
  if Task is TPingTask and chbIgnorePing.IsChecked then exit;

  li.params := Task.VerboseDescription.Replace(#13, ' ').Replace(#10, ' ').Trim;

  li.ClassName := Task.ClassName;
  li.Subject := Task.Subject;
  li.Operation := System.TypInfo.GetEnumName(System.TypeInfo(TCRUD), integer(Task.Operation));

  li.taskid := Task.ID;
  li.outcome := Task.Outcome;
  li.State := Task.State;
//  li.Content := Content;

  cs.Acquire;
  LogItems.Add(li);
  cs.Release;
//  TThread.Queue (TThread.Current, procedure begin DoLog (taskid, cn, Action, Content, params, Outcome) end);
end;

procedure TfrmTasks.SetController(AController: TObservableController);
begin
  FController := AController;
  AController.Subscribe(self);
end;

//procedure TfrmTasks.OnMyUserData(AEvent: IUserTask);
//begin
//  memUserSettings.Text := AEvent.User.Settings.ToVariant;
//end;

procedure TfrmTasks.ObserveActivity (const Busy: boolean);
begin
  if not Busy
    then recIdle.Fill.Color := TAlphaColorRec.Green
    else recIdle.Fill.Color := TAlphaColorRec.Red;
end;

procedure TfrmTasks.sgrSelChanged(Sender: TObject);
begin
  memData.Lines.Text := 'Parameters: ' + sgr.Cells[4, sgr.Selected] + sLineBreak + '-----' + sLineBreak + 'Outcome: ' + sgr.Cells[5, sgr.Row];
end;

procedure TfrmTasks.Timer2Timer(Sender: TObject);
begin
  cs.Acquire;
  if LogItems.Count > 0 then
    begin
      sgr.BeginUpdate;
      while LogItems.Count > 0 do
        begin
          DoLog(LogItems[0]);
          LogItems.Delete(0)
        end;
      sgr.EndUpdate;
    end;
  cs.Release;
end;

{ TTaskLogger }

procedure TTaskLogger.Clear;
begin
  FLogFile.Clear;
end;

constructor TTaskLogger.Create;
begin
  inherited;
  FLogFile := TLogFile.Create(ApplicationExeName+'.log', 'dd.mmm ' + FormatSettings.LongTimeFormat, 100*1024, 200*1024);
end;

destructor TTaskLogger.Destroy;
begin
  FLogFile.Free;
  inherited;
end;

function TTaskLogger.FileName: string;
begin
  result := FLogFile.FileName;
end;

function TTaskLogger.GetName: string;
begin
  result := ClassName;
end;

procedure TTaskLogger.Log(const Content: string);
begin
  Write (format ('Custom log:%s', [Content]));
end;

//function TTaskLogger.ObservesOwnTasksOnly: boolean;
//begin
//  result := false;
//end;

procedure TTaskLogger.ObserveTaskState (Task: TObservableTask);//LogTask(Task: TebTask; const Action, Content: string);
var
  params: string;
//  p: integer;
begin
  params := Task.VerboseDescription.Replace(#13, ' ').Replace(#10, ' ').Trim;

  Write (format ('Task:%s[%d] %s | %s | %s | %s', [Task.ClassName, Task.ID, TRttiEnumerationType.GetName (Task.State), '', params, task.Outcome.ToString]));
end;

procedure TTaskLogger.SetIdle(Idle: boolean);
begin
  Write ('Idle');
end;

procedure TTaskLogger.Write(const s: string);
begin
  FLogFile.Write(s);
end;


//  if State = TFunController.apsTaskRunning then
//    SetIdle(TFunController.apsTaskRunning.Active);

var
  ITaskLogger: IInterface;

initialization
  if not FindCmdLineSwitch ('nolog')
     then TaskLogger := TTaskLogger.Create;
  ITaskLogger := TaskLogger;

finalization
  ITaskLogger := nil;

end.
