unit FMX.HistoryEdit;

interface

uses
  Classes, FMX.ComboEdit, FMX.Types, FMX.Edit, mormot, SynCommons,
  mormotMods, MormotTypes, FMX.Models, Controller.Tasks, ObservableController,
  FMX.Common.Utils, FMX.BaseModel, FMX.Graphics, Types, System.UITypes;

type

  THistoryEditModel = class (TBaseModel, IHistoryObserver, IFreeNotification)
  private
    procedure DoOnEnter;
    procedure DropDown;
  protected
    OriginalOnChange: TNotifyEvent;
    FEdit: TComboEdit;
    IgnoreEvent: boolean;
    OriginalOnChangeTracking: TNotifyEvent;
    function GetName: string;
    procedure ObserveHistory (Task: THistoryTask);
    procedure OnEditChangeTracking (Sender: TObject);
    procedure OnEditEnter(Sender: TObject);
    procedure OnEditPaint (Sender: TObject; Canvas: TCanvas; const ARect: TRectF);
    procedure DoFetch; override;
    procedure OnEditKeyUp (Sender: TObject; var Key: word; var KeyChar: char; Shift: TShiftState);
  public
    FieldName: string;
    CustomLookup: boolean;
    SqlRecordClass: TSqlRecordClass;
    QueryParameters: TQueryParameters;
    FetchIfEmpty: boolean;
    constructor Create (ASqlRecordClass: TSqlRecordClass; const AFieldName: string; AController: TObservableController; AEdit: TComboEdit); reintroduce;
    destructor Destroy; override;

    procedure FreeNotification(AObject: TObject); override;
  end;

implementation

uses FMX.ListBox, SysUtils;

{ THistoryEditModel }

constructor THistoryEditModel.Create(ASqlRecordClass: TSqlRecordClass; const AFieldName: string;
            AController: TObservableController; AEdit: TComboEdit);
begin
  inherited Create ('hem' + ASqlRecordClass.SQLTableName.AsString + '.' + AFieldName, AController);
  SqlRecordClass := ASqlRecordClass;
  FieldName := AFieldName;
  FEdit := AEdit;
  OriginalOnChangeTracking := FEdit.OnChangeTracking;
  FEdit.OnChangeTracking := OnEditChangeTracking;
  FEdit.OnEnter := OnEditEnter;
  FEdit.AddFreeNotify(self);
  FEdit.OnPaint := OnEditPaint;
  FEdit.OnKeyUp := OnEditKeyUp;
//  FEdit.OnSelect... kad bi bilo
  FetchIfEmpty := true;
end;

destructor THistoryEditModel.Destroy;
begin
  if FEdit <> nil
     then FEdit.RemoveFreeNotify(self);
  inherited;
end;

procedure THistoryEditModel.DoFetch;
begin
  Controller.FetchHistory (SqlRecordClass, FieldName, FEdit.Text, QueryParameters, self);
end;

procedure THistoryEditModel.FreeNotification(AObject: TObject);
begin
  if AObject = FEdit
     then begin
            Fedit := nil;
            Free;
          end;
end;

function THistoryEditModel.GetName: string;
begin
  result := FName;
end;

procedure THistoryEditModel.DropDown;
begin
  FEdit.DropDown;
  FEdit.BringToFront;
//  if FEdit is TComboEdit then
//  tcomboBox(FEdit).li
//     TComboEdit (FEdit).
end;

procedure THistoryEditModel.ObserveHistory(Task: THistoryTask);
begin
  if name = 'hemVehicle.Manufacturer'
    then if now = 0 then exit;
  
  IgnoreEvent := true;
  FEdit.Items.Clear;
  FEdit.ItemIndex := -1;

  for var s in Task.Values do
    if (s.AsString <> FController.Model.ImportUnknownFieldContent) and (s <> '')
       then FEdit.Items.Add(s.AsString);

  FEdit.Width := FEdit.Width + 1;
  FEdit.Width := FEdit.Width - 1;

  if FEdit.IsFocused and (FEdit.Items.Count > 0)
     //and ( (FEdit.Items.Count > 1) or (FEdit.Items[0] <> FEdit.Text) )
     and (FEdit.Items.IndexOf (FEdit.Text) = -1)
     and not FEdit.DroppedDown
          then DropDown;

  IgnoreEvent := false;
end;

procedure THistoryEditModel.DoOnEnter;
begin
  if not FEdit.DroppedDown and (FEdit.Items.Count > 0)
     then DropDown;
end;

procedure THistoryEditModel.OnEditChangeTracking(Sender: TObject);
begin
  if not IgnoreEvent and FEdit.IsFocused {and  FEdit.Items.IndexOf (FEdit.Text)} then
     PostponeFetch;
  if Assigned (OriginalOnChangeTracking)
     then OriginalOnChangeTracking (Sender);
  FEdit.BringToFront;
end;

procedure THistoryEditModel.OnEditEnter(Sender: TObject);
begin
  DoOnEnter;
end;

procedure THistoryEditModel.OnEditKeyUp(Sender: TObject; var Key: word; var KeyChar: char; Shift: TShiftState);
begin
  if Key = vkF5 then Fetch;
end;

procedure THistoryEditModel.OnEditPaint(Sender: TObject; Canvas: TCanvas; const ARect: TRectF);
begin
  if FetchIfEmpty then
    begin
      FetchIfEmpty := false;
      PostponeFetch;
    end;
end;

end.
