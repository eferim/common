unit FMX.GridModel;

interface

uses
  System.SysUtils, System.UITypes, System.Classes, System.Generics.Collections,
  System.Types, System.Rtti, System.Actions, math, StrUtils, variants,
  FMX.Grid,  FMX.ActnList,  FMX.Forms, FMX.StdCtrls, FMX.Controls, FMX.ComboEdit,
  FMX.Types, FMX.Graphics, FMX.ImgList, FMX.Edit, FMX.ListBox, FMX.DateTimeCtrls, FMX.Layouts,
  FMX.Header, FMX.Styles.Objects, IOUtils, FMX.Grid.Style,
  syncommons,  mORMot,
  PersistentUserSettings, MormotMods, MormotTypes, SqlRecords, Common.Utils, FMX.Models, FMX.Common.Utils,
  UI.StateManagement, ObservableController, Controller.Tasks, FMX.Platform, FMX.Clipboard, FMX.Clipboard.Win,
  Client.MormotMods, MormotAttributes, FMX.BaseModel, FMX.Menus;

const
  Max_Records_Without_Navigation = -1;
  {TODO -oOwner -cGeneral : dodaj opciju da li da auto sklanja navigaciju}

type
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])} // disables RTTI generation but doesnt seem to help with generic bloat

  TIDList = TList<TID>;

  TMultipleSqlRecordsModel = class (TCrudSqlRecordModel, IRecordObserver, IRecordsObserver, ILookupObserver, IServerStateObserver)
  private
    FPage: TQueryPage;
    function PageLimited: boolean;
    function GetPageLimit: integer;
    procedure SetPageLimit(const Value: integer);
    procedure AddAlternativeSelectionCandidate;
    function GetRecordCount: integer;
  protected
    FSqlRecords : TSqlRecords;
    FSortFieldIndex: integer;
    FSortAscending: boolean;
    FLookups: TLookups;
    FetchDurationMS, UITime, UIDurationMS: int64;
    function GetRecords: TSqlRecords; virtual;
    procedure SetRecords(const Value: TSqlRecords); virtual;
    function CurrentRow: integer; virtual;

    procedure SetSortAscending(const Value: boolean);
    procedure SetSortFieldIndex(const Value: integer); virtual;
    function GetSortFieldName: string; virtual;
    procedure PrepareQueryParameters; virtual;
    function OrderBySQL: string; virtual;
    function FieldDisplayName(FieldName: string): string;
  public
    AllowSort {todo razne stvari ne rade ako je ovo true}: boolean;
    SelectionCandidates: TIDList;

    function RecordInRow (index: integer): TSqlRecord;
    function VisibleRecordCount: integer; virtual; abstract;
    //intf impl
    procedure ObserveRecord (Task: TSingleRecordTask); virtual;
    procedure ObserveRecords(Task: TRecordsTask); virtual;
    procedure ObserveLookup(Task: TObservableLookupTask); virtual;

    procedure AttachButtons (Buttons: TArray<TCustomButton>; ModelActions: TArray<TModelAction>); override;

    constructor Create (ASqlRecordClass: TSqlRecordClass; const AName: string;
                        AController: TObservableController; AApsOwnerCU: TAppState = nil;
                        AMaster: TSqlRecordModel = nil; ADetailLinkKeyName: RawUTF8 = '';
                        AMasterLinkKeyName: RawUTF8 = 'ID'); reintroduce; virtual;
    destructor Destroy; override;

    procedure Populate; virtual; abstract;
    procedure AutoDisplayNavigation;
    function ContainsID (const ID: TID): boolean;
    procedure CopyToClipboardAsText (const EmptyClipboard, IncludeFieldNames: boolean; Fields: TStringList = nil); virtual; abstract;
    procedure CopyToClipboardAsJSON (const EmptyClipboard: boolean; Fields: TStringList = nil); virtual; abstract;

    property SortFieldIndex : integer read FSortFieldIndex write SetSortFieldIndex;
    property SortAscending : boolean read FSortAscending write SetSortAscending;
    property SortFieldName : string read GetSortFieldName;

    property Records : TSqlRecords read GetRecords write SetRecords;
    property RecordCount: integer read GetRecordCount;
    property PageLimit: integer read GetPageLimit write SetPageLimit;
  end;

  TGridModelOption = (RestoreSelected, FreeOnGridDestroy, Statistics, FetchTimeInStatistics,
                      FetchOnFirstDraw, FetchAfterCU, FetchAfterDelete, OrderByServer, Charts,
                      DontFetchIfNotVisible);
  TGridModelOptions = set of TGridModelOption;

  TGridModel = class (TMultipleSqlRecordsModel)
  private
    FGrid : TStringGrid;
    FFirstDataColumn, FPrevSelectedRow: integer;
    FDetailIDs: TIDDictionary;
    FShowRowNumbers: boolean;
    OriginalGridOnKeyUp: TKeyEvent;
    labTotal: TLabel;
    spbCharts: TSpeedButton;
    dicMenuToChart: TDictionary <TObject, TChartAttributeSettings>;
    // UI event handlers
    //procedure OnTimer (Sender: TObject);
    procedure DataHeaderClick(Column: TColumn);

    procedure GridOnColumnMoved(Column: TColumn; FromIndex, ToIndex: Integer);
    procedure GridOnSelChanged (Sender: TObject);
    procedure GridOnKeyUp (Sender: TObject; var Key: Word; var KeyChar: WideChar; Shift: TShiftState);
    procedure GridOnDblClick (Sender: TObject);
    procedure GridOnApplyStyleLookup (Sender: TObject);
    procedure GridOnMouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure GridHeaderMouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure GridOnResize (Sender: TObject);
    procedure GridOnPaint(Sender: TObject; Canvas: TCanvas; const ARect: TRectF);
    procedure GridOnDrawColumnCell (Sender: TObject; const Canvas: TCanvas; const Column: TColumn; const Bounds: TRectF;
                                const Row: Integer; const Value: TValue; const State: TGridDrawStates);
    procedure ChartMenuItemOnClick (Sender: TObject);
    procedure spbChartsOnClick (Sender: TObject);

//    function MasterLinkKeyValue: TID;
    procedure SetShowRowNumbers(const Value: boolean);
    procedure OnStatusBarLabelClick(Sender: TObject);
    procedure AttachGridHeaderMouseUp;
    procedure ShowFilterUI(Column: TColumn);
    procedure UpdateHeaders;

    function FieldInfo(const Column: integer): TFieldInfo;
    procedure DoPrepareGrid;
    procedure SetOptions(const Value: TGridModelOptions);
    procedure RecalculateStatistics;
    function StatusBarNeeded: boolean;
    procedure UpdateStatisticsLabels;
    procedure CreateStatusBar;
//    procedure OnFilterButtonClick(Sender: TObject);
  protected
    FStatusBar: TStatusBar;
    FStatusBarLayout : TLayout;
    CellBrush: TBrush;
    FApplyAutoColumnWidths, IgnoreContentChangeSelect: boolean;
    GridFilter: TQueryFilter;
    FOptions: TGridModelOptions;
    GridRecords: TList<TSqlRecord>;
    function GetStatisticsLabelText(StatisticsIndex: integer): string; virtual;
    function GetStatisticsTotalLabelText: string; virtual;
    procedure UpdateStatusBar; virtual;
    procedure UpdateFilterHint; override;
//    function GetSelectedID: TID; override;
    function GetSelectedRecord: TSqlRecord; override;
    procedure SetSelectedRecord(const Value: TSqlRecord); override;
    procedure BusyChanged (Sender: TObject); override;
    //function GetGrid: TStringGrid; virtual;
    procedure SetGrid(const Value: TStringGrid); virtual;
    procedure PopulateSelected(lind : integer); virtual;

    procedure SetGeneralFilter(const Value: string); override;
    function GetFieldDisplayValue (rec: TSqlRecord; const FieldName: string): variant; overload; virtual;
    function GetFieldDisplayValue (rec: TSqlRecord; const FieldInfo: TFieldInfo): variant; overload; virtual;
    function CustomCellBrush(const ARow: integer; const FieldName: string): boolean; virtual;
    procedure SetDetailIDs(const Value: TIDDictionary); //6
    property DetailIDs : TIDDictionary read FDetailIDs write SetDetailIDs;
    procedure RestoreSelected;

    procedure AppStateNotification (const State: TAppState); override;
    procedure ExecuteAction(ModelAction: TModelAction); override;

    function CurrentRow: integer; override;
    procedure SetSortFieldIndex(const Value: integer); override;
    function GetSortFieldName: string; override;
    procedure SetRecords(const Value: TSqlRecords); override;
    procedure PrepareQueryParameters; override;
    function OrderBySQL: string; override;
    function IDToRow (const ID: TID): integer;
    procedure AfterSetSelectedID; override;
    procedure DoFetch; override;
  public
    RequiredLookups: integer;

    CustomGridDrawing: boolean;
    Brief, Silent: boolean;
    IgnoreFields: TStringList;
    constructor Create (ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController;
                        AApsOwnerCU: TAppState = nil; AMaster: TSqlRecordModel = nil; ADetailLinkKeyName: RawUTF8 = '';
                        AMasterLinkKeyName: RawUTF8 = 'ID'); override;

    destructor Destroy; override;

    procedure GoFirst;
    procedure GoPrev;
    procedure GoNext;
    procedure GoLast;

    procedure Populate; override;
    procedure UIFillTestData; override;
    function VisibleRecordCount: integer; override;
    procedure CloseUIDialogue; override;

    function SelectedIDAsPointer: pointer;

    procedure AttachButton (Button: TCustomButton; ModelAction: TModelAction); override;
    procedure DetachButton (Button: TCustomButton); override;

    procedure FreeNotification(AObject: TObject); override;

    procedure ObserveRecord (Task: TSingleRecordTask); override;
    procedure ObserveRecords(Task: TRecordsTask); override;
    procedure ObserveLookup(Task: TObservableLookupTask); override;
    procedure ObserveState(State: TServerState; var Outcome: TOutcome); override;
    procedure CopyToClipboardAsRTF (const EmptyClipboard: boolean);
    procedure CopyToClipboardAsText (const EmptyClipboard, IncludeFieldNames: boolean; slFields: TStringList = nil); override;
    procedure CopyToClipboardAsJSON (const EmptyClipboard: boolean; Fields: TStringList = nil); override;
    procedure SetSort (const AFieldName: string; Ascending: boolean);
    procedure SaveAndRePrepareGrid;
//    property SelectedID : TID read GetSelectedID write SetSelectedID;
    property Grid : TStringGrid read FGrid write SetGrid;
    property ShowRowNumbers: boolean read FShowRowNumbers write SetShowRowNumbers;
    property Options: TGridModelOptions read FOptions write SetOptions;
  end;

  TMormotGridModel<T : TSQLRecord> = class (TGridModel)
  private
    function GetSqlRecord(index: integer): T;
  protected
    function GetCrudRecord: T; reintroduce;
    function GetSelectedRecord: T; reintroduce;
    procedure SetSelectedRecord(const Value: T); reintroduce;
  public
    constructor Create (const AName: string; AController: TObservableController; AApsOwnerCU: TAppState = nil;
                        AMaster: TSqlRecordModel = nil; ADetailLinkKeyName: RawUTF8 = '';
                        AMasterLinkKeyName: RawUTF8 = 'ID'); reintroduce; virtual;
    property SelectedRecord: T read GetSelectedRecord write SetSelectedRecord;
    property CrudRecord: T read GetCrudRecord;
    property SqlRecords [index: integer]: T read GetSqlRecord;
  end;


//function FAddStatusBarLabel (parent: TFmxObject): TLabel;

implementation

uses
  FieldFilter.Form, MultiRecordClipboardDialog.Form, Chart.Form;

type

  TStatisticsLabel = class (TLabel)
  public
    StatisticsIndex: integer;
  end;

function FAddStatusBarLabel (parent: TFmxObject; Index: integer): TStatisticsLabel;
begin
  result := TStatisticsLabel.Create(parent);
  result.Parent := parent;
  result.Align := TAlignLayout.Left;
  result.AutoSize := true;
  result.Margins.Left := 20;
  result.Margins.Right := 20;
  result.TextSettings.WordWrap := false;
  result.Position.X := 999;
  result.HitTest := true;
  result.StatisticsIndex := Index;
end;

function TMultipleSqlRecordsModel.ContainsID(const ID: TID): boolean;
begin
  result := (Records <> nil) and (Records.Contains(ID));
end;

constructor TMultipleSqlRecordsModel.Create(ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController; AApsOwnerCU: TAppState;
                                                     AMaster: TSqlRecordModel; ADetailLinkKeyName, AMasterLinkKeyName: RawUTF8);
begin
  inherited Create (ASqlRecordClass, AName, AController);
  SelectionCandidates := TIDList.Create;
end;

destructor TMultipleSqlRecordsModel.Destroy;
begin
  FreeAndNil (SelectionCandidates);
  inherited;
end;

procedure TMultipleSqlRecordsModel.PrepareQueryParameters;
begin
  QueryParameters.Filter.Clear;
  QueryParameters.MasterID := MasterID;
  if HasMaster then
     begin
       QueryParameters.Filter.Add(DetailLinkKeyName, TFilterOperator.Equal, MasterID);
       QueryParameters.MasterTableName.AsString := MasterTableName;
     end;
  QueryParameters.Filter.AddFrom(AdditionalFilter);
  if PageLimited
     then QueryParameters.GeneralFilter.AsString := GeneralFilter;
  QueryParameters.Page := FPage;
  QueryParameters.OrderBy.AsString := OrderBySQL;
end;

function TMultipleSqlRecordsModel.FieldDisplayName(FieldName: string): string;
begin
  result := Controller.Model.GetFieldDisplayName(SqlRecordClass, FieldName);
end;

function TMultipleSqlRecordsModel.GetPageLimit: integer;
begin
  result := FPage.Limit;
end;

function TMultipleSqlRecordsModel.GetRecordCount: integer;
begin
  if FSqlRecords.IsAssigned
     then result := FSqlRecords.Count
     else result := 0;
end;

function TMultipleSqlRecordsModel.GetRecords: TSqlRecords;
begin
  Result := FSqlRecords;
end;

function TMultipleSqlRecordsModel.CurrentRow: integer;
begin
  result := 0;
end;

procedure TMultipleSqlRecordsModel.SetPageLimit(const Value: integer);
begin
  FPage.Limit := Value;
end;

procedure TMultipleSqlRecordsModel.AddAlternativeSelectionCandidate;
begin
  var rAlternate := CurrentRow;
  if rAlternate = 0
     then rAlternate := 1
     else rAlternate := rAlternate - 1;

  if (rAlternate < FSqlRecords.Count) and (rAlternate >= 0)
    then SelectionCandidates.Add (RecordInRow (rAlternate).IDValue);
end;

procedure TMultipleSqlRecordsModel.SetRecords(const Value: TSqlRecords);
begin
  if Assigned (FSqlRecords) and Assigned (Value) and Assigned (Value.FiltersAndStats)
     then begin
            Value.FiltersAndStats.Assign(FSqlRecords.FiltersAndStats);
            if SelectedID.IsNotNULL
               then SelectionCandidates.Add (SelectedID);

            AddAlternativeSelectionCandidate;
          end;

  FSqlRecords.Free;
  FSqlRecords := Value;

  if FLookups <> nil
     then FLookups.Dictionary.Clear;
  Populate;
  ApsHasData.SetActiveIf (Records.IsAssigned and not Records.IsEmpty);
  AutoDisplayNavigation;
end;

function TMultipleSqlRecordsModel.PageLimited: boolean;
begin
  result := FPage.Limit > 0
end;

function TMultipleSqlRecordsModel.RecordInRow(index: integer): TSqlRecord;
begin
  if not PageLimited  // if local sort then get from local index otherwise use sql records initial order
    then result := FSqlRecords.Indexes [SortFieldName].Records [index, SortAscending]
    else result := FSqlRecords.Records[index];
end;

procedure TMultipleSqlRecordsModel.SetSortAscending(const Value: boolean);
begin
  FSortAscending := Value;
  if PageLimited then {FetchRecords (FMasterID) }else Populate;
end;

procedure TMultipleSqlRecordsModel.SetSortFieldIndex(const Value: integer);
begin
  FSortFieldIndex := Value;
  if PageLimited then {FetchRecords (FMasterID) }else Populate;
end;

function TMultipleSqlRecordsModel.GetSortFieldName: string;
begin
  result := '';
end;

procedure TMultipleSqlRecordsModel.AttachButtons(Buttons: TArray<TCustomButton>; ModelActions: TArray<TModelAction>);
begin
  inherited;
  AutoDisplayNavigation;
end;

procedure TMultipleSqlRecordsModel.AutoDisplayNavigation;
var
  Action: TAction;
  Button1, Button2, Button3, Button4: TCustomButton;
begin
  inherited;

  var ManyRecords := VisibleRecordCount > Max_Records_Without_Navigation;
  // uvek umece na drugo mesto

  if FModelActionsToActions.TryGetValue (TModelAction.First, Action)
     then begin
            Action.Visible := ManyRecords;
            FActionToButton.TryGetValue(Action, Button1);
          end;
  if FModelActionsToActions.TryGetValue (TModelAction.Previous, Action)
     then begin
            Action.Visible := ManyRecords;
            FActionToButton.TryGetValue(Action, Button2);
            if Button1.IsAssigned and Button2.IsAssigned
              then Button2.Position.X := Button1.Position.X + Button1.Width + 1;
          end;
  if FModelActionsToActions.TryGetValue (TModelAction.Next, Action)
     then begin
            Action.Visible := ManyRecords;
            FActionToButton.TryGetValue(Action, Button3);
            if Button2.IsAssigned and Button3.IsAssigned
              then Button3.Position.X := Button2.Position.X + Button2.Width + 1;
          end;
  if FModelActionsToActions.TryGetValue (TModelAction.Last, Action)
     then begin
            Action.Visible := ManyRecords;
            FActionToButton.TryGetValue(Action, Button4);
            if Button3.IsAssigned and Button4.IsAssigned
              then Button4.Position.X := Button3.Position.X + Button3.Width + 1;
          end;
end;

function TGridModel.FieldInfo (const Column: integer): TFieldInfo;
begin
  result := TFieldInfo (Grid.Columns[Column].TagObject);
  if result = nil then
     begin
       result := SqlRecordClass.FieldInfoList.ItemsByName[Grid.Columns[Column].TagString];
       Grid.Columns[Column].TagObject := result;
     end;
end;

procedure TGridModel.UpdateFilterHint;
begin
  inherited;
  if FFilterEdit = nil then exit;

  if PageLimited
     then FFilterEdit.Hint := FFilterEdit.Hint + #13'Filtrirano na serveru'
     else FFilterEdit.Hint := FFilterEdit.Hint + #13'Filtrirano lokalno';
end;

procedure TGridModel.UpdateHeaders;
const
  OrderChars: array [false .. true] of string = (chrSortDescending, chrSortAscending);
var
  fi: TFieldInfo;
  i: integer;
  DisplayName: string;
begin
  for i := 0 to Grid.ColumnCount - 1 do
    begin
      fi := FieldInfo(i);
      DisplayName := Controller.Model.GetFieldDisplayName (SqlRecordClass, fi.DisplayName);
      if GridFilter.ContainsField (StringToUTF8(fi.NameForFilter))
        then DisplayName := chrFilterActive + ' ' + DisplayName;

      if FSortFieldIndex = i then
        DisplayName := OrderChars[SortAscending] + ' ' + DisplayName;

      Grid.Columns[i].Header := DisplayName;
    end;
end;

procedure TGridModel.SetSort(const AFieldName: string; Ascending: boolean);
var
  i: integer;
begin
  for i := 0 to Grid.ColumnCount - 1 do
    if Grid.Columns[i].TagString = AFieldName then
       begin
         FSortAscending := Ascending;
         SetSortFieldIndex(i);
         break;
       end;
end;

procedure TGridModel.SetSortFieldIndex(const Value: integer);
begin
  if Grid.Columns[Value].TagString <> '' then
  begin
    inherited;
    UpdateHeaders;
  end;
end;

procedure TGridModel.UIFillTestData;
var
  propname: string;
begin
  inc (TestID);

  if UIDialogueForm <> nil then
    begin
      for var i := 0 to UIDialogueForm.ComponentCount - 1 do
        begin
          var com := UIDialogueForm.Components[i];
          propname := copy (com.Name, 4);
          var P := SqlRecordClass.RecordProps.Fields.ByRawUTF8Name(StringToUTF8(PropName));
          if P <> nil then
            if (com is TCustomEdit) and (TCustomEdit(com).Text = '') // dont overwrite what is already there
               then TCustomEdit(com).Text := crudRecord.GenerateRandomValue (propname, TestID)
            else if com is TCustomComboBox
               then TCustomComboBox(com).ItemIndex := 1// + Random (TCustomComboBox(com).Items.Count-1)
            else if com is TDateEdit
               then TDateEdit(com).Date := now - 1 - Random (10);
        end;
    end;
end;

function TGridModel.GetStatisticsLabelText (StatisticsIndex: integer): string;
var
  sval: string;
begin
  sval := VarToFormattedStr (FSqlRecords.FiltersAndStats[StatisticsIndex].StatisticValue);
  if sval = '' then sval := '0';
  result := FSqlRecords.FiltersAndStats[StatisticsIndex].StatisticName + ': ' + sval;
end;

function TGridModel.GetStatisticsTotalLabelText: string;
begin
  result := 'Ukupno: ' + Records.Count.ToString;
  if PageLimited and (Records.Count = PageLimit) then result := result + ' (+?)';
end;

function TGridModel.StatusBarNeeded: boolean;
begin
  result := [TGridModelOption.Statistics, TGridModelOption.Charts] * FOptions <> [];
end;

procedure TGridModel.UpdateStatisticsLabels;
var
  com: TComponent;
  lab: TStatisticsLabel;
begin
  labTotal.Text := GetStatisticsTotalLabelText;

  if (TGridModelOption.FetchTimeInStatistics in Options)
     then begin
            var sval := format ('DB: %dms UI: %dms', [FetchDurationMS, UIDurationMS]);
            TLabel (FStatusBar.Components[FStatusBar.ComponentCount-1]).Text := sval;
          end;

  for var i := 0 to FStatusBar.ComponentCount - 1 do
    begin
      com := FStatusBar.Components[i];
      if com is TStatisticsLabel then
        begin
          lab := TStatisticsLabel (FStatusBar.Components[i]);
          if lab.StatisticsIndex >= 0
             then lab.Text := GetStatisticsLabelText (lab.StatisticsIndex);
        end;
    end;
end;

procedure TGridModel.CreateStatusBar;
var
  lab: TStatisticsLabel;
begin
  begin
    FStatusBar := TStatusBar.Create(FGrid);
    FStatusBar.Margins.Bottom := 1;
    FStatusBar.Margins.Left := 1;
    FStatusBar.ShowSizeGrip := false;
    FStatusBar.Align := TAlignLayout.MostBottom;

    if (FGrid.Parent is TLayout) and (FGrid.Align = TAlignLayout.Client)
      then FStatusBar.Parent := FGrid.Parent
      else
        begin
          FStatusBarLayout := TLayout.Create (FGrid.Parent);
          FStatusBarLayout.Align := FGrid.Align;
          FStatusBarLayout.Size := FGrid.Size;
          FStatusBarLayout.Parent := FGrid.Parent;
          FStatusBarLayout.Position.Y := FGrid.Position.Y;
          FGrid.Parent := FStatusBarLayout;
          FGrid.Align := TAlignLayout.Client;
        end;

    if TGridModelOption.Statistics in Options then
      begin
        labTotal := FAddStatusBarLabel (FStatusBar, -1); // for total

        for var fs in FSqlRecords.FiltersAndStats do
          begin
            if fs.Visible then
              begin
                lab := FAddStatusBarLabel (FStatusBar, fs.index);
                if fs.Name <> '' then
                  begin
                    lab.OnClick := OnStatusBarLabelClick;
                    lab.Cursor := crHandPoint;
                  end;
              end;
          end;

        lab := FAddStatusBarLabel (FStatusBar, -2); // for time stats
        lab.Align := TAlignLayout.Right;
        lab.AutoSize := true;
        lab.Margins.Right := 10;
      end;

    if ClassHasAttribute (SqlRecordClass, ChartAttribute) then
      begin
        spbCharts := TSpeedButton.Create (FStatusBar);
        spbCharts.PopupMenu := TPopupMenu.Create (spbCharts);
        spbCharts.Align := TAlignLayout.Right;
        spbCharts.Margins.Right := 10;
        spbCharts.Margins.Top := 3;
        spbCharts.Margins.Bottom := 3;
        spbCharts.OnClick := spbChartsOnClick;
        spbCharts.Text := 'C';
        spbCharts.Images := FController.ImageList;
        spbCharts.ImageIndex := FController.dicImageIndexes['chart'];
        spbCharts.Width := 32;
        spbCharts.Parent := FStatusBar;
        var ctx := TRttiContext.Create;
        var t := ctx.GetType(SqlRecordClass);
        for var a in t.GetAttributes do
          if a is ChartAttribute then
             begin
               var ca := ChartAttribute (a);
               var mi := TMenuItem.Create(spbCharts.PopupMenu);
               mi.Text := ca.Settings.Title;
               mi.OnClick := ChartMenuItemOnClick;
               dicMenuToChart.Add (mi, ca.Settings);
               spbCharts.PopupMenu.AddObject(mi);
             end;
      end;
  end;
end;

procedure TGridModel.UpdateStatusBar;
begin
  if not StatusBarNeeded
     or FSqlRecords.IsNotAssigned or FSqlRecords.FiltersAndStats.IsNotAssigned then
    begin
      if FStatusBar.IsAssigned then
        begin
          FStatusBar.Visible := false;
          if FStatusBarLayout.IsAssigned then FStatusBarLayout.Visible := false;
        end;
      exit;
    end;

  if FStatusBar.IsNotAssigned
     then CreateStatusBar;

  if TGridModelOption.Statistics in Options
     then UpdateStatisticsLabels;

  FStatusBar.Visible := true;
  if FStatusBarLayout.IsAssigned
     then FStatusBarLayout.Visible := true;
end;

function TGridModel.VisibleRecordCount: integer;
begin
  if FGrid = nil
     then result := 0
     else result := FGrid.RowCount;
end;

function TGridModel.GetSortFieldName: string;
var
  fi: TFieldInfo;
begin
  if AllowSort
     then
       begin
          fi := FieldInfo(SortFieldIndex);
          if fi = nil
            then result := ''
            else if fi.SubstituteSortField <> '' //Result := fi.NameForFilter //Result := Grid.Columns[SortFieldIndex].TagString;
                 then result := fi.SubstituteSortField
                 else result := fi.Name;
       end
     else Result := '';
end;

procedure TGridModel.GoFirst;
begin
  if Grid.RowCount > 0 then
    Grid.Selected := 0;
end;

procedure TGridModel.GoLast;
begin
  if (Grid.RowCount > 0) then
    Grid.Selected := Grid.RowCount - 1;
end;

procedure TGridModel.GoNext;
begin
  if (Grid.RowCount > 0) and (Grid.Selected < Grid.RowCount - 1) then
    Grid.Selected := Grid.Selected + 1;
end;

procedure TGridModel.GoPrev;
begin
  if (Grid.RowCount > 0) and (Grid.Selected > 0) then
    Grid.Selected := Grid.Selected - 1;
end;

procedure TGridModel.ShowFilterUI(Column: TColumn);
begin
  var FieldInfo := TFieldInfo (Column.TagObject);

  if TfrmFieldFilter.ShowFor (FController, FieldInfo, GridFilter) <> mrCancel
     then begin
            UpdateHeaders;
            Fetch;
          end;
end;

procedure TGridModel.GridHeaderMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  if Button = TMouseButton.mbRight then
    begin
      var ghi := TGridHeaderItem (Sender);
      ShowFilterUI (ghi.Column);
    end;
end;

procedure TGridModel.GridOnApplyStyleLookup(Sender: TObject);
begin
  AttachGridHeaderMouseUp;
end;

procedure TGridModel.GridOnDblClick(Sender: TObject);
var
  Action: TAction;
begin
  if SelectedID.IsNotNULL
     then begin
            FModelActionsToActions.TryGetValue (TModelAction.Update, Action);
            if Action <> nil
               then Action.Execute;
          end
     else begin
            FModelActionsToActions.TryGetValue (TModelAction.Create, Action);
            if Action <> nil
               then Action.Execute;
          end;
end;

procedure TGridModel.DoPrepareGrid;
begin
  PrepareGrid (Controller.Model, SqlRecordClass, {Master, } DetailLinkKeyName.AsString, grid, ShowRowNumbers,
               Brief, IgnoreFields.CommaText);
end;

procedure TGridModel.SaveAndRePrepareGrid;
begin
  Controller.PersistentUserSettings.WriteGridState(Grid);
  while grid.ColumnCount > 0 do grid.Columns[0].Free;
  DoPrepareGrid;
  Controller.PersistentUserSettings.ReadGridState(Grid);
end;

procedure TGridModel.GridOnKeyUp(Sender: TObject; var Key: Word; var KeyChar: WideChar; Shift: TShiftState);
begin
  if (Key = vkDelete) and SelectedID.IsNotNULL then
    begin
      {$IFDEF DEBUG}
      if (Shift = [ssCtrl, ssShift]) then
        begin
           if ConfirmationDialog('Da li ste sigurni da zelite da obrisete ovaj slog i SVE NJEGOVE DETALJE?')
           and ConfirmationDialog('Da li ste APSOLUTNO sigurni da zelite da obrisete ovaj slog i SVE NJEGOVE DETALJE?')
           then
               Controller.CustomRequest ('Delete Recursively',
                  DocVariant (['TableName', SqlRecordClass.crudSQLTableName, 'IDValue', SelectedID]), '', self);
           exit;
        end;
      {$ENDIF}
      ExecuteAction(TModelAction.Delete);
    end;
  if (Key = vkF6) //and (Shift = [ssCtrl])
    then FGrid.AutoColumnWidths;
  {$IFDEF DEBUG}
  if (Key = vkF7)
    then if (Shift = [ssCtrl, ssShift])
      then Records := nil
      else AttachGridHeaderMouseUp;
  {$ENDIF}
  if (Key = vkF2) and SelectedID.IsNotNULL then
    ExecuteAction(TModelAction.Update);


  if Key = vkF5
    then begin
           if Shift = [ssCtrl] then
              SaveAndRePrepareGrid;
           Fetch;
         end;
  if (Key = ord ('C')) and (Shift = [ssCtrl])
    then begin
           TfrmMultiRecordClipboardDialog.ShowInstanceFor (self); //FController, FSqlRecordClass);
           //CopyToClipboardAsText (true);
         end;
  if (Key = ord ('R')) and (Shift = [ssCtrl])
    then begin
           CopyToClipboardAsRTF (false);
         end;
  if (Key = ord ('M')) and (Shift = [ssCtrl])
    then begin
           //FGrid.Options := FGrid.Options + TGridOption.
         end;

  if Assigned (OriginalGridOnKeyUp) then OriginalGridOnKeyUp (Sender, Key, KeyChar, Shift);
end;

procedure TGridModel.GridOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var
  Col, Row: integer;
begin
//  DumpControls (FGrid);

  if FGrid.CellByPoint (X, Y, Col, Row)
    then begin
         end
    else SelectedID := NULL_ID;
end;

procedure TGridModel.GridOnSelChanged(Sender: TObject);
begin
  if IgnoreContentChangeSelect then
    begin
      IgnoreContentChangeSelect := true;
      SelectedID := NULL_ID;
      exit;
    end;
  if SelectedRecord.IsAssigned
     then SelectedID := SelectedRecord.IDValue
     else SelectedID := NULL_ID;
  if FPrevSelectedRow = Grid.Row then exit;
  FPrevSelectedRow := Grid.Row;
  CheckSelectedRecord;
end;

procedure TGridModel.ObserveState(State: TServerState; var Outcome: TOutcome);
begin
  inherited;

  case State of
    TServerState.BeforeSaveSettings: if grid <> nil
                                  then Controller.PersistentUserSettings.WriteGridState(grid);
    TServerState.AfterLogin:   if grid <> nil
                                  then Controller.PersistentUserSettings.ReadGridState(Grid);
    end;
end;

procedure TGridModel.ObserveLookup(Task: TObservableLookupTask);
begin
//  if Task.NamedInitiator = self as INamed
    //then
    if Task.Success
      then
        begin
          FLookups.Dictionary.AddOrSetValue(Task.SQLRecordClass, Task.Lookup);
          Populate;
        end
    else Task.ShowError;
end;

procedure TGridModel.ExecuteAction(ModelAction: TModelAction);
begin
  case ModelAction of
    TModelAction.First:     GoFirst;
    TModelAction.Previous:  GoPrev;
    TModelAction.Next:      GoNext;
    TModelAction.Last:      GoLast;

    TModelAction.Read:      Fetch;
    TModelAction.Update:    begin
                              if TGridOption.RowSelect in grid.Options
                                 then grid.Col := 1;
                              Edit;// (SelectedRecord.IDValue);
                            end;
    TModelAction.Create,
    TModelAction.Delete,
    TModelAction.Post,
    TModelAction.Cancel,
    TModelAction.Show:      inherited;
  end; //case
end;

//function TGridModel.MasterLinkKeyValue: TID;
//begin
//  if MasterLinkKeyName = 'ID'
//     then result := Master.SelectedID
//     else result := Master.SelectedSqlRecord.GetFieldVariant (MasterLinkKeyName.AsString);
//end;

procedure TGridModel.DoFetch;
begin
  if (TGridModelOption.DontFetchIfNotVisible in Options) and FGrid.IsAssigned and not FGrid.Visible
    then
      begin
        Options := Options + [TGridModelOption.FetchOnFirstDraw];
        exit;
      end;
  Options := Options - [TGridModelOption.FetchOnFirstDraw];

  if PageLimited and (SortFieldName = '')
    then SetSortFieldIndex(FFirstDataColumn);

  if Master.IsAssigned
     then if Master.SelectedID.IsNull
          then begin
                 Records := nil; // no master record
                 FMasterID := NULL_ID;
                 exit;
               end;
  inherited;

  PrepareQueryParameters;
  Controller.RecordsRead (SqlRecordClass, QueryParameters, self);
end;

procedure TGridModel.FreeNotification(AObject: TObject);
begin
  inherited;

  if AObject = FGrid then
    begin
      Grid := nil;
      if TGridModelOption.FreeOnGridDestroy in Options
         then Free;
    end;
end;

procedure TGridModel.spbChartsOnClick (Sender: TObject);
begin
  var p := spbCharts.LocalToAbsolute(PointF(0, 0));
  p.x := p.x + GetParentForm(spbCharts).Left;
  p.y := p.y + GetParentForm(spbCharts).Top;
  spbCharts.PopupMenu.Popup(p.x, p.y);
end;

procedure TGridModel.ChartMenuItemOnClick(Sender: TObject);
begin
  TfrmChart.Show (dicMenuToChart[Sender], records);
end;

procedure TGridModel.CloseUIDialogue;
begin
  inherited;
  FGrid.SetFocus;
end;

procedure TGridModel.ObserveRecord (Task: TSingleRecordTask);
begin
  if Task.Outcome.Success
    then begin
           case Task.Operation of
              TCRUD.Create:    begin
                                 FLastCreatedID := Task.Outcome.ID;
                                 apsCreate.Deactivate;
                                 if StickyCreate
                                    then CreateRecord;

                                 SelectionCandidates.Add(Task.Outcome.ID);
                                 if (TGridModelOption.FetchAfterCU in Options) or (Task.SqlRecord = nil) or FSqlRecords.IsNotAssigned
                                    then Fetch
                                    else
                                      begin
                                        FSqlRecords.AddOne(Task.SqlRecord.CreateCopy);
                                        Populate;
                                        UpdateStatusBar;
                                        SelectedID := Task.Outcome.ID;
                                      end;

                                 if TUIDialogueOption.CloseOnSuccess in UIDialogueOptions
                                    then CloseUIDialogue;
                               end;
              TCRUD.ReadLock : begin
                                 crudRecord := Task.SqlRecord.CreateCopy;
                                 apsUpdate.Activate;
                                 ShowUIDialogue;
                               end;
              TCRUD.Update   : begin
                                 crudRecord := nil;
                                 apsUpdate.Deactivate;

                                 if (TGridModelOption.FetchAfterCU in Options) or (Task.SqlRecord = nil)
                                    then Fetch
                                    else
                                      begin
                                        FSqlRecords.UpdateOne(Task.SqlRecord.CreateCopy);
                                        Populate;
                                        UpdateStatusBar;
                                        SelectedID := Task.SqlRecord.ID;
                                      end;

                                 if TUIDialogueOption.CloseOnSuccess in UIDialogueOptions
                                    then begin
                                           CloseUIDialogue;
                                           grid.SetFocus;
                                         end;
                               end;
              TCRUD.Delete   : begin
                                 if TGridModelOption.FetchAfterDelete in Options
                                   then Fetch
                                   else
                                     begin
                                       AddAlternativeSelectionCandidate;
                                       FSqlRecords.FreeAndDeleteOne(Task.Outcome.ID);
                                       Populate;
                                       UpdateStatusBar;
                                     end;
                               end;
              TCRUD.Unlock   : ProcessUnlock (Task);
            end; // case

            //if Task.Operation in [TCRUD.Delete, TCRUD.Update, TCRUD.Create]
          end
  else Task.ShowError;
end;

procedure TGridModel.ObserveRecords(Task: TRecordsTask);
begin
  inherited;
  FetchDurationMS := GetTickCount64 - FetchTime;
  if Task.Success
    then Records := Task.SqlRecords.CreateCopy
    else Task.ShowError;

  if HasMaster
     then FMasterID := task.QueryParameters.MasterID;

  Activity.EndActivity;
end;

function TGridModel.IDToRow (const ID: TID): integer;
begin
  if Records = nil then exit (-1);

  if GeneralFilter = ''
    then if PageLimited
         then exit (Records.dicIDToIndex[ID])
         else exit (Records.Indexes[SortFieldName].IndexOfRecordID(ID, FSortAscending))
    else
      begin
        result := 0;
        for var rec in GridRecords do
          begin
            if rec.IDValue = ID then
               exit (result);
            inc (result);
          end;
      end;
  result := -1;
end;

// RestoreSelected is not going to be called if there are no records,
// in order to preserve the candidates for potential later non-empty fetch that may include the candidates
procedure TGridModel.RestoreSelected;
begin
  if FLastCreatedID.IsNotNull then
    begin
      SelectionCandidates.Insert (0, FLastCreatedID);
      FLastCreatedID.Clear;
    end;

  for var ID in SelectionCandidates do
    if Records.Contains (ID) then
      begin
        // SelectedID := ID; doing this would cycle selection state because it would think current selected is null because grid was just populated so no row is selected
        var Row := IDToRow (ID);
        if Row = -1 then continue;
        Grid.Selected := Row;
        CheckSelectedRecord;
        break;
      end;

  SelectionCandidates.Clear;
end;

function TGridModel.SelectedIDAsPointer: pointer;
begin
  result := pointer (SelectedID);
end;

procedure TGridModel.BusyChanged;
begin
  inherited;
  if Activity.Busy
     then FGrid.Opacity := 0.5
     else FGrid.Opacity := 1;
end;

procedure TGridModel.SetDetailIDs(const Value: TIDDictionary);
begin
  FDetailIDs.Free;
  FDetailIDs := Value;
end;

procedure TGridModel.SetGeneralFilter(const Value: string);
begin
  inherited;

  if PageLimited
    then Fetch
    else begin
           Populate;
           AutoDisplayNavigation;
         end;
end;

procedure TGridModel.GridOnPaint(Sender: TObject; Canvas: TCanvas; const ARect: TRectF);
begin
  if (TGridModelOption.FetchOnFirstDraw in Options) and Controller.LoggedIn then
    begin
      FGrid.OnPaint := nil;
      Exclude (FOptions, TGridModelOption.FetchOnFirstDraw);
      if RecordCount = 0
        then Fetch;
    end;
end;

procedure TGridModel.GridOnResize(Sender: TObject);
begin
  Activity.Indicator.SetBounds(FGrid.Position.X, FGrid.Position.Y, FGrid.Width, FGrid.Height);
end;

procedure TGridModel.SetGrid(const Value: TStringGrid);
begin
  if FGrid <> nil
     then begin
            dicControlToModel.Remove(FGrid);
            FGrid.RemoveFreeNotify(self);
          end;

  FGrid := Value;
  if Value = nil then exit;

  FGrid.ReadOnly := true;
  FGrid.RowCount := 0;
  FGrid.Row := -1;

  // events
  FGrid.OnHeaderClick := DataHeaderClick;
  FGrid.OnSelChanged := GridOnSelChanged;
  FGrid.OnColumnMoved := GridOnColumnMoved;
  FGrid.OnApplyStyleLookup := GridOnApplyStyleLookup;
  if CustomGridDrawing
     then FGrid.OnDrawColumnCell := GridOnDrawColumnCell;
  FGrid.OnDblClick := GridOnDblClick;
  OriginalGridOnKeyUp := FGrid.OnKeyUp;
  FGrid.OnKeyUp := GridOnKeyUp;
  FGrid.OnMouseUp := GridOnMouseUp;
  FGrid.OnPaint := GridOnPaint;
  FGrid.OnResize := GridOnResize;

  FGrid.Options := FGrid.Options + [TGridOption.AlternatingRowBackground, TGridOption.RowSelect,
                                    TGridOption.AlwaysShowSelection];

  RegisterStateObserver(FGrid, TAppStateAction.Enabled, [], [apsOwnerCU, apsCU]);

  DoPrepareGrid;
  if FGrid.ColumnCount = 0
     then begin
            DoPrepareGrid;
            //raise Exception.Create('No columns in grid ' + Value.Name);
          end;

  if FGrid.ColumnCount = 0
     then raise Exception.Create('No columns in grid ' + Value.Name);

  FGrid.Columns[0].DragMode := TDragMode.dmManual;
  if not Controller.PersistentUserSettings.ReadGridState(Grid)
    then FApplyAutoColumnWidths := true;
  SortFieldIndex := FFirstDataColumn;
  FGrid.AddFreeNotify(self);
  dicControlToModel.Add(FGrid, self);

  Activity.Indicator.Parent := FGrid.Parent;
  Activity.Indicator.BringToFront;
  Activity.Indicator.SetBounds(FGrid.Position.X, FGrid.Position.Y, FGrid.Width, FGrid.Height);

//  if FGrid.Margins.Bottom = 0 then FGrid.Margins.Bottom := 10; zastoooooooooo
end;

function TGridModel.CurrentRow: integer;
begin
  result := FGrid.Row;
end;

function TGridModel.CustomCellBrush(const ARow: integer; const FieldName: string): boolean;
begin
  result := false;
end;

procedure TGridModel.GridOnDrawColumnCell(Sender: TObject; const Canvas: TCanvas; const Column: TColumn; const Bounds: TRectF;
                         const Row: Integer; const Value: TValue; const State: TGridDrawStates);
begin
  if (TGridDrawState.Selected in State) or
      (TGridDrawState.Focused in State) then
    begin
      Column.DefaultDrawCell(Canvas, Bounds, Row, Value, State);
      Exit;
    end;

  if CustomCellBrush (Row, Column.TagString) then
     begin
       var b := Bounds;
       InflateRect(b, 2.0, 2.0);
       Canvas.FillRect (b, 0, 0, [], 1, CellBrush);

       //Canvas.Font.Assign (FGrid.Parent.Font);
       Canvas.Fill.Color := Column.DefaultTextSettings.FontColor;

       if (Column is TCustomNumberColumn)
         then Canvas.FillText(Bounds, Value.AsString, false, 1, [], TTextAlign.Trailing, TTextAlign.Center)
         else Canvas.FillText(Bounds, Value.AsString, false, 1, [], Column.DefaultTextSettings.HorzAlign, Column.DefaultTextSettings.VertAlign);

       exit;
     end;

//  grid.DefaultDrawing := False;
//https://stackoverflow.com/questions/43624260/how-to-change-color-of-a-cell-in-a-delphi-tgrid-firemonkey-component

  Column.DefaultDrawCell(Canvas, Bounds, Row, Value, State);
end;

//procedure TGridModel.OnFilterButtonClick(Sender: TObject);
//begin
//  if FilterEnabled
//    then FilterEnabled := false
//    else if (FFilterEdit <> nil) and not FFilterEdit.Text.IsEmpty
//      then FilterEnabled := true;
//  FFilterTimer.Restart;
//end;

procedure TGridModel.OnStatusBarLabelClick(Sender: TObject);
var
  lab: TLabel;
begin
  lab := TLabel(Sender);
  var FilterIndex := lab.ComponentIndex-1;
  if FSqlRecords.FiltersAndStats.Toggle(FilterIndex)
     then begin
            //lab.Font.Style := lab.Font.Style + [TFontStyle.fsBold];
            lab.FontColor := TAlphaColorRec.Red;
            lab.StyledSettings := lab.StyledSettings - [{TStyledSetting.Style, }TStyledSetting.FontColor];
          end
     else begin
            //lab.Font.Style := lab.Font.Style - [TFontStyle.fsBold];
            lab.StyledSettings := lab.StyledSettings + [{TStyledSetting.Style, }TStyledSetting.FontColor];
          end;
  Populate;
end;

function TGridModel.OrderBySQL: string;
begin
  if not PageLimited and not (OrderByServer in Options)
     then exit ('');

  result := GetSortFieldName + ' ';
//  result := result + ' collate WIN32NOCASE ';
//  result := result + ' collate SYSTEMNOCASE ';

  if SortAscending
    then result := result + 'ASC'
    else result := result + 'DESC';
end;

constructor TGridModel.Create (ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController; AApsOwnerCU: TAppState = nil;
                                  AMaster: TSqlRecordModel = nil; ADetailLinkKeyName: RawUTF8 = ''; AMasterLinkKeyName: RawUTF8 = 'ID');
begin
  inherited Create (ASqlRecordClass, AName, AController);
  dicMenuToChart := TDictionary <TObject, TChartAttributeSettings>.Create;
  GridRecords := TList<TSqlRecord>.Create;
  IgnoreFields := TStringList.Create;
  GridFilter := default (TQueryFilter);
  Master := AMaster;
  if AMaster.IsAssigned and (ADetailLinkKeyName = '')
    then DetailLinkKeyName := Amaster.SqlRecordClass.crudSQLTableName + 'ID'
    else DetailLinkKeyName := ADetailLinkKeyName;
    //raise Exception.Create('Master field must be specified for ' + AName);
  MasterLinkKeyName := AMasterLinkKeyName;

  AllowSort := true;
  QueryParameters := default (TQueryParameters);

  CellBrush := TBrush.Create(TBrushKind.Solid, TAlphaColors.Alpha);
  FSortAscending := true;
  FLookups := TLookups.Create ([], []);

  ApsOwnerCU := AApsOwnerCU;
  Options := [TGridModelOption.RestoreSelected, TGridModelOption.FreeOnGridDestroy,
              TGridModelOption.FetchTimeInStatistics, TGridModelOption.FetchOnFirstDraw,
              TGridModelOption.DontFetchIfNotVisible];
  if ClassHasAttribute (SqlRecordClass, StatisticsAttribute)
     then Options := Options + [TGridModelOption.Statistics];
  if ClassHasAttribute (SqlRecordClass, ChartAttribute)
     then Options := Options + [TGridModelOption.Charts];
  if SqlRecordClass.IsView
     then Options := Options + [TGridModelOption.FetchAfterCU];

  Activity.Indicator := TAniIndicator.Create(nil);

  if ClassHasAttribute (SqlRecordClass, LargeTableAttribute) then
     PageLimit := 1000;
end;

destructor TGridModel.Destroy;
begin
  if FGrid.IsAssigned
     then FGrid.RemoveFreeNotify(self);

  FreeAndNil (FLookups);
  FreeAndNil (FSqlRecords);
  FreeAndNil (CellBrush);
  if FGrid.IsAssigned and FController.IsAssigned and FController.PersistentUserSettings.IsAssigned then
    begin
      Controller.PersistentUserSettings.WriteGridState (grid);
      Grid := nil;
    end;
  IgnoreFields.Free;
  GridRecords.Free;
  dicMenuToChart.Free;
  inherited;
end;

procedure TGridModel.DetachButton(Button: TCustomButton);
begin
  inherited;
end;

procedure TGridModel.DataHeaderClick(Column: TColumn);
begin
  if not AllowSort then exit;
  GetSelectedRecord;
  if SortFieldIndex = Column.Index then
    FSortAscending := not SortAscending
  else
    FSortAscending := true;
  SortFieldIndex := Column.Index;

  if PageLimited then Fetch;
end;

procedure TGridModel.AppStateNotification(const State: TAppState);
begin
  inherited;
  if (Master <> nil) then
    if (State = Master.apsSelectedRecordChangedDelayed) and (FMasterID <> Master.SelectedID)
      then begin
             Fetch;
             FMasterID := Master.SelectedID;
             //apsSelectionChanging.CycleOnOff;
           end
    else if (State = Master.apsSelectedIDChanged)
      then begin
             Records := nil;
             FMasterID := NULL_ID;
           end;
end;

procedure TGridModel.AttachButton(Button: TCustomButton; ModelAction: TModelAction);
begin
  inherited;
end;

//function FindChildByClass (c: TControl; cl: TClass): TControl;
//begin
//  result := nil;
//  if c is cl then exit (THeader(c));
//  for var c2 in c.Controls do
//    begin
//      result := FindChildByClass (c2, cl);
//      if result.IsAssigned
//        then break;
//    end;
//end;

procedure TGridModel.AttachGridHeaderMouseUp;
begin
  var list := FGrid.FindChildrenByClass<TGridHeaderItem>;
  for var c in list do
      c.OnMouseUp := GridHeaderMouseUp;
  list.Free;
end;

procedure TGridModel.GridOnColumnMoved(Column: TColumn; FromIndex, ToIndex: Integer);
begin
  Grid.Columns[ToIndex].Header := Grid.Columns[ToIndex].Header.TrimLeft([chrSortAscending, chrSortDescending, ' ']);
  Grid.Columns[FromIndex].Header := Grid.Columns[FromIndex].Header.TrimLeft([chrSortAscending, chrSortDescending, ' ']);
  SortFieldIndex := ToIndex;
end;

function EncodeStringForRTF(const Input: string): string;
var
  i: Integer;
  UnicodeChar: Word;
begin
  Result := '';

  for i := 1 to Length(Input) do
  begin
    UnicodeChar := Ord(Input[i]);

    // Check if the character is a "safe" ASCII character (0 to 127)
    if (UnicodeChar <= 127) then
    begin
      // Safe character, just add it directly
      Result := Result + Input[i];
    end
    else
    begin
      // Encode non-ASCII characters to RTF format
      Result := Result + '\u' + IntToStr(UnicodeChar) + '?';
    end;
  end;
end;

(*
  '{\rtf1\ansi\deff0' +
  '\pard' +
  '{\colortbl ;\red192\green192\blue192;\red255\green255\blue255;\red240\green240\blue240;\red255\green255\blue0;\red173\green216\blue230;\red144\green238\blue144;}' +
  '\trowd' +
  '\clbrdrt\brdrs\brdrw20\clbrdrl\brdrs\brdrw20\clbrdrb\brdrs\brdrw20\clbrdrr\brdrs\brdrw20\clcbpat1' +
  '\cellx2000' +
  '\clbrdrt\brdrs\brdrw20\clbrdrl\brdrs\brdrw20\clbrdrb\brdrs\brdrw20\clbrdrr\brdrs\brdrw20\clcbpat1' +
  '\cellx4000' +
  '\clbrdrt\brdrs\brdrw20\clbrdrl\brdrs\brdrw20\clbrdrb\brdrs\brdrw20\clbrdrr\brdrs\brdrw20\clcbpat1' +
  '\cellx6000' +
  '\qc\sl240\slmult1 A1\cell B1\cell C1\cell\row' +
//  'A1\cell B1\cell C1\cell\row' +
  '\trowd' +
  '\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\clcbpat2' +
  '\cellx2000' +
  '\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\clcbpat2' +
  '\cellx4000' +
  '\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\clcbpat2' +
  '\cellx6000' +
  '\ql\li50 A2\cell B2\cell\qr\ri50 C2\cell\row' +
  '\trowd' +
  '\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\clcbpat3' +
  '\cellx2000' +
  '\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\clcbpat3' +
  '\cellx4000' +
  '\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\clcbpat3' +
  '\cellx6000' +
//  'A1\cell B1\cell C1\cell\row' +
  '\ql A3\cell B3\cell\qr C3\cell\row' +
  '}';
*)

procedure TGridModel.CopyToClipboardAsJSON (const EmptyClipboard: boolean; Fields: TStringList = nil);
var
  Builder: TStringBuilder;
  iRow: integer;
  rec: TSqlRecord;
begin
  Builder := TStringBuilder.Create;
  try
    for iRow := 0 to FGrid.RowCount - 1 do
      begin
        rec := RecordInRow(iRow);
        if Fields = nil
           then Builder.Append (rec.GetJSONValues (true, rec.IDValue.IsNotNULL,
                rec.RecordProps.CopiableFieldsBits {rec.GetNonVoidFields}, []).AsString)
           else Builder.Append (rec.GetJSONValues (true, rec.IDValue.IsNotNULL, StringToUTF8 (Fields.CommaText), []).AsString);
        if iRow < FGrid.RowCount - 1 then Builder.Append (#13);
      end;

    WindowsClipboard.AddText (Builder.ToString, EmptyClipboard);
  finally
    Builder.Free;
  end;
end;

procedure TGridModel.CopyToClipboardAsText(const EmptyClipboard, IncludeFieldNames: boolean; slFields: TStringList = nil);
var
  Builder: TStringBuilder;
  iCol, iRow: integer;
  RowCount, ColumnCount: integer;
  NeedsTab : boolean;
  DisplayName: string;
begin
  Builder := TStringBuilder.Create;
  try
    ColumnCount := FGrid.ColumnCount;
    RowCount := FGrid.RowCount;

    if IncludeFieldNames then
      begin
        NeedsTab := false;
        for iCol := 0 to ColumnCount - 1 do
          begin
            var fi := FieldInfo(iCol);
            if (fi = nil) or
               ( (slFields <> nil) and (slFields.IndexOf (fi.Name) = -1) ) then continue;

            DisplayName := Controller.Model.GetFieldDisplayName (SqlRecordClass, fi.DisplayName);
            if NeedsTab
               then Builder.Append (#9);
            Builder.Append(DisplayName);
            NeedsTab := true;
          end;

        Builder.Append (#13);
      end;

    for iRow := 0 to RowCount - 1 do
      begin
        NeedsTab := false;
        for iCol := 0 to ColumnCount - 1 do
          begin
            var fi := FieldInfo(iCol);
            if (fi = nil) or
               ( (slFields <> nil) and (slFields.IndexOf (fi.Name) = -1) ) then continue;

            if NeedsTab
               then Builder.Append (#9);
            Builder.Append(strRemoveCharacters (FGrid.Cells[iCol, iRow], [#13, #10, #9]));
            NeedsTab := true;
          end;
        Builder.Append(#13);
      end;
//    var ss := TStringStream.Create (Builder.ToString);
//    WindowsClipboard.AddCustomFormat('Text', ss, EmptyClipboard);
    WindowsClipboard.AddText (Builder.ToString, EmptyClipboard);

//    var intf: IFMXExtendedClipboardService;
//    if TPlatformServices.Current.SupportsPlatformService(IFMXExtendedClipboardService , intf) then
//       intf.SetText(Builder.ToString);

//    ss.Free;
  finally
    Builder.Free;
  end;
end;

procedure TGridModel.CopyToClipboardAsRTF (const EmptyClipboard: boolean);
var
  Builder: TStringBuilder;
  ss: TStringStream;
  iCol, iRow, Twips: integer;
  CellTwips: TArray<string>;
  CurrentAlignment: TAlignLayout;
  RowCount, ColumnCount: integer;
begin
  Builder := TStringBuilder.Create;
  try
    Builder.Append('{\rtf1\ansi\deff0\pard'#13);
    // color table
    Builder.Append('{\colortbl ;\red192\green192\blue192;\red255\green255\blue255;\red240\green240\blue240;\red255\green255\blue0;\red173\green216\blue230;\red144\green238\blue144;}'#13);
    Builder.Append('\trowd'#13); // start header row
    Twips := 0;
    setlength (CellTwips, FGrid.ColumnCount);

    ColumnCount := FGrid.ColumnCount;
    RowCount := FGrid.RowCount;

    for iCol := 0 to ColumnCount - 1 do
      begin
        Builder.Append('\clbrdrt\brdrs\brdrw20\clbrdrl\brdrs\brdrw20\clbrdrb\brdrs\brdrw20\clbrdrr\brdrs\brdrw20\clcbpat1'#13);
        Twips := Twips + round (FGrid.Columns[iCol].Width * 15);
        CellTwips[iCol] := '\cellx' + Twips.ToString;
        Builder.Append(CellTwips[iCol] + #13);
      end;

    Builder.Append('\qc\sl240\slmult1 ');
    for iCol := 0 to ColumnCount - 1 do
      begin
        var fi := FieldInfo(iCol);
        var DisplayName := Controller.Model.GetFieldDisplayName (SqlRecordClass, fi.DisplayName);
        Builder.Append(' ' + EncodeStringForRTF (DisplayName) + '\cell');
      end;
    Builder.Append('\row' + #13);

    CurrentAlignment := TAlignLayout.None;

    for iRow := 0 to RowCount - 1 do
      begin
        Builder.Append('\trowd'#13);

        for iCol := 0 to ColumnCount - 1 do
          begin
            Builder.Append('\clbrdrt\brdrs\brdrw10\clbrdrl\brdrs\brdrw10\clbrdrb\brdrs\brdrw10\clbrdrr\brdrs\brdrw10\');
            if iRow mod 2 = 0
               then Builder.Append('clcbpat2'#13)
               else Builder.Append('clcbpat3'#13);

            Builder.Append(CellTwips[iCol] + #13);
          end;

        for iCol := 0 to ColumnCount - 1 do
          begin
            var col := FGrid.Columns[iCol];
            if CurrentAlignment <> col.Align then
              begin
                CurrentAlignment := col.Align;
                case CurrentAlignment of
                      TAlignLayout.Left : Builder.Append('\ql');
                      TAlignLayout.Right : Builder.Append('\qr');
                      TAlignLayout.Center : Builder.Append('\qc');
                    end;
              end;
            Builder.Append(EncodeStringForRTF (' ' + FGrid.Cells[iCol, iRow]) + '\cell ');
          end;
        Builder.Append('\row' + #13);
      end;

    Builder.Append('}');

    ss := TStringStream.Create(Builder.ToString);
    //ss.SaveToFile(ApplicationExeFolder + 'dump.rtf');
    try
      WindowsClipboard.AddCustomFormat('Rich Text Format', ss, EmptyClipboard);
//      var intf: IFMXExtendedClipboardService;
//      if TPlatformServices.Current.SupportsPlatformService(IFMXExtendedClipboardService , intf) then
//         begin
//           if not intf.IsCustomFormatRegistered('Rich Text Format')
//              then intf.RegisterCustomFormat('Rich Text Format');
//           intf.SetCustomFormat('Rich Text Format', ss);
//         end;
    finally
      ss.Free;
    end;
  finally
    Builder.Free;
  end;
end;

function TGridModel.GetFieldDisplayValue (rec: TSqlRecord; const FieldName: string): variant;
begin
  if FieldName <> ''
     then result := Controller.GetFieldDisplayValue(rec, FieldName, FLookups)
     else result := '';
end;

function TGridModel.GetFieldDisplayValue (rec: TSqlRecord; const FieldInfo: TFieldInfo): variant;
begin
  if FieldInfo.IsBoolean // for boolean column we dont want any translations
     then result := rec.GetFieldVariant(FieldInfo.Name)
     else result := Controller.GetFieldDisplayValue(rec, FieldInfo, FLookups);
end;

//function TGridModel.GetGrid: TStringGrid;
//begin
//  Result := FGrid;
//end;
{
function TGridModel.GetSelectedID: TID;
begin
  var rec := GetSelectedRecord;
  if rec = nil
    then result := Null_ID
    else result := rec.IDValue;

//  if Grid.Selected = -1
//     then result := Null_ID
//     else if PageLimited then
//          result := Records.Indexes[SortFieldName].Records[Grid.Selected, SortAscending].IDValue;
end;
}

function TGridModel.GetSelectedRecord: TSqlRecord;
begin
  Result := nil;

  if Assigned(Grid) and (Grid.Selected > -1) and (Grid.Selected < Records.Count)
    then result := GridRecords[Grid.Selected];
//
//    then
//      if PageLimited
//         then Result := Records.Records[Grid.Selected]
//         else Result := Records.Indexes[SortFieldName].Records[Grid.Selected, SortAscending];
end;

procedure TGridModel.RecalculateStatistics;
begin
  if Records.IsAssigned
     then Records.CalculateStatistics;
  UpdateStatusBar;
end;

procedure TGridModel.SetOptions(const Value: TGridModelOptions);
begin
  var StatusBarUpdateNeeded := TGridModelOption.Statistics in ( (Value - FOptions) + (FOptions - Value) );
  FOptions := Value;
  if StatusBarUpdateNeeded then
     RecalculateStatistics;
end;

procedure TGridModel.SetRecords(const Value: TSqlRecords);
begin
  inherited;
  if (TGridModelOption.Statistics in Options)
    then RecalculateStatistics
    else UpdateStatusBar;
end;

procedure TGridModel.Populate;
//var
//  OldSelectedID: TID;
begin
//  OldSelectedID := SelectedID;

  if (SelectionCandidates.Count = 0) and (SelectedID <> NULL_ID)
   then SelectionCandidates.Add (SelectedID);

  FPrevSelectedRow := -2;

  if AllowSort and (SortFieldName = '') then // this can happen if there is no page limit and sort order is not set yet
    begin
      SetSortFieldIndex(FFirstDataColumn);
      exit;
    end;

  if RequiredLookups > FLookups.Dictionary.Count then
    begin
      Grid.RowCount := 0;
      exit;
    end;

  Activity.BeginActivity;

  UITime := GetTickCount64;
  Grid.BeginUpdate;
  Grid.Selected := -1;
//  ApsRecordSelected.Deactivate;
  Grid.RowCount := Records.Count;
  GridRecords.Clear;

  if Records.IsAssigned and not Records.IsEmpty then
    begin
      var i := 0;

      if AllowSort and not PageLimited then // client side sort scenario
        //todo nekako ovo objediniti...
        begin
          var Filtering := not GeneralFilter.IsEmpty and SqlRecordClass.InheritsFrom(TSqlRecordEx);
          Records.SetFiltered (Filtering);

          for var rec in Records.EnumByField (SortFieldName, FSortAscending, '', GeneralFilter) do
            begin
              if Filtering
                then TSqlRecordEx(rec).Filtered := false;
              GridRecords.Add(rec);
              if ShowRowNumbers
                 then Grid.Cells[0, i] := (i + 1).ToString;
              for var col := FFirstDataColumn to Grid.ColumnCount - 1 do
                  Grid.Cells[col, i] := GetFieldDisplayValue (rec, FieldInfo(col)); {TODO -oOwner -cGeneral : ovde verovatno ima previse konverzija variant -> string -> float etc...}
              inc(i);
            end;
          Grid.RowCount := i;
          if Filtering then RecalculateStatistics;
        end

        else

        for var rec in Records.Records do
          begin
            Records.SetFiltered (false);
            GridRecords.Add(rec);
            if ShowRowNumbers
               then Grid.Cells[0, i] := (i + 1).ToString;
            for var col := FFirstDataColumn to Grid.ColumnCount - 1 do
                Grid.Cells[col, i] := GetFieldDisplayValue (rec, FieldInfo(col));
            inc(i);
          end;

      if TGridModelOption.RestoreSelected in Options
         then RestoreSelected;
//      SetSelectedID (SelectedID);// to select correct row
    end
    else
      begin
        SelectedID := NULL_ID;
        //if SelectionCandidates.Count > 0
        //   then CheckSelectedRecord; // force trigger apsSelected to inform detail models
      end;
  if SelectedID = NULL_ID then IgnoreContentChangeSelect := true;
  grid.EndUpdate;
  if IgnoreContentChangeSelect then FGrid.Row := -1;

  UIDurationMS := GetTickCount64 - UITime;
  IgnoreContentChangeSelect := false;

  if FApplyAutoColumnWidths and (FGrid.RowCount > 0) then
    begin
      FGrid.AutoColumnWidths;
      FApplyAutoColumnWidths := false;
    end;

  Activity.EndActivity;

  CheckSelectedRecord;
end;

procedure TGridModel.PopulateSelected(lind: integer);
begin
  Grid.Selected := lind;
end;

procedure TGridModel.PrepareQueryParameters;
begin
  inherited;

  QueryParameters.Filter.AddFrom(GridFilter);
end;

procedure TGridModel.AfterSetSelectedID;
var
  RowIndex : integer;
begin
  if Name = 'MainPlanItems' then
    if now = 0 then exit;

  inherited;

  if SelectedID = SelectedRecordID then exit;// we are here probably due to user clicks row -> SelectedID is changed

  if SelectedID.IsNull
    then Grid.Selected := -1
    else begin
           RowIndex := IDToRow (SelectedID);
           if RowIndex = -1 then
             begin
               SelectionCandidates.Add(SelectedID);
               Fetch;
               //Grid.Selected := -1; would cause loop?
             end
             else
               begin
                 Grid.Selected := RowIndex;
                 grid.ScrollToSelectedCell;
               end;
         end;
  CheckSelectedRecord;
end;

procedure TGridModel.SetSelectedRecord(const Value: TSqlRecord);
begin
  if Value.IsAssigned
    then Grid.Row := IDToRow (Value.IDValue)
    else Grid.Row := -1;
end;

procedure TGridModel.SetShowRowNumbers(const Value: boolean);
begin
  FShowRowNumbers := Value;
  if ShowRowNumbers
     then FFirstDataColumn := 1
     else FFirstDataColumn := 0;
end;

{ TMultipleSqlRecordsModel }

procedure TMultipleSqlRecordsModel.ObserveLookup(Task: TObservableLookupTask);
begin

end;

procedure TMultipleSqlRecordsModel.ObserveRecord(Task: TSingleRecordTask);
begin
end;

procedure TMultipleSqlRecordsModel.ObserveRecords(Task: TRecordsTask);
begin
  ApsFetching.Deactivate;
end;

function TMultipleSqlRecordsModel.OrderBySQL: string;
begin
  result := '';
end;

{ TGridModel<T> }

constructor TMormotGridModel<T>.Create(const AName: string; AController: TObservableController; AApsOwnerCU: TAppState;
  AMaster: TSqlRecordModel; ADetailLinkKeyName, AMasterLinkKeyName: RawUTF8);
begin
  inherited Create (T, AName, AController, AApsOwnerCU, AMaster, ADetailLinkKeyName, AMasterLinkKeyName);
end;

function TMormotGridModel<T>.GetCrudRecord: T;
begin
  result := T(inherited GetCrudRecord);
end;

function TMormotGridModel<T>.GetSelectedRecord: T;
begin
  result := T(inherited GetSelectedRecord);
end;

function TMormotGridModel<T>.GetSqlRecord(index: integer): T;
begin
  result := T (Records.Records[index]);
end;

procedure TMormotGridModel<T>.SetSelectedRecord(const Value: T);
begin
  inherited SetSelectedRecord(Value);
end;

end.
