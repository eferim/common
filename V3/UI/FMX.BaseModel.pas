﻿unit FMX.BaseModel;

interface

uses
  System.SysUtils, System.UITypes, System.Classes, System.Generics.Collections,
  System.Types, System.TypInfo, System.Rtti, System.Actions, math,
  FMX.Grid,  FMX.ActnList,  FMX.TabControl,  FMX.Forms,  FMX.StdCtrls,  FMX.Edit,  FMX.Controls, FMX.ListBox,
  FMX.ComboEdit, FMX.Types, FMX.Graphics, FMX.ImgList, FMX.Layouts, FMX.DateTimeCtrls, FMX.Memo,

  syncommons,  mORMot, IniFiles, IOUtils,
  MormotMods, MormotTypes, MormotAttributes, SqlRecords, Common.Utils,
  FMX.Common.Utils, UI.StateManagement, ObservableController, Controller.Tasks, Validation, StrUtils;

type
  TMormotAppState = class (TAppState)
    constructor Create (T: TSQLRecordClass; const Context: string; const ATiming: TAppStateTiming = TAppStateTiming.Immediate; AChildState: TAppState = nil); reintroduce;
  end;

  {$SCOPEDENUMS ON}
  TUIDialogueOption = (Enabled, Modal, AutoUI, AutoTabOrder, CloseOnSuccess, CloseOnCancelUpdate,
                       CloseOnCancelCreate, CustomCollect);
  TUIDialogueOptions = set of TUIDialogueOption;

  TModelAction = (First, Previous, Next, Last, Create, Read, Show, Update, Delete, Post, Cancel);
  TModelActions = set of TModelAction;

  TCustomSqlRecordStater = class
    function RequiredStates (Action: TModelAction): TAppStateArray; virtual; abstract;
    function ForbiddenStates (Action: TModelAction): TAppStateArray; virtual; abstract;
    function RequiredDetailStates (cl: TSqlRecordClass; Action: TModelAction): TAppStateArray; virtual; abstract;
    function ForbiddenDetailStates (cl: TSqlRecordClass; Action: TModelAction): TAppStateArray; virtual; abstract;
  end;

  TActivityElement = class (TNonARCInterfacedObject, IFreeNotification)
  private
   var
    FBusy: integer;
    FIndicator : TAniIndicator;
    FOnBysy: TNotifyEvent;
    function GetBusy: boolean;
    procedure SetIndicator(const Value: TAniIndicator);
    procedure FreeNotification(AObject: TObject); virtual;
  public
    procedure BeginActivity;
    procedure EndActivity;

    constructor Create (AOnBysy: TNotifyEvent);
    destructor Destroy; override;

    property Indicator : TAniIndicator read FIndicator write SetIndicator;
    property Busy: boolean read GetBusy;
  end;

  TUIContainerInfo = class
  protected
    FUpdatingCount: integer;
  public
    FieldInfoLabels:   TDictionary<TFieldInfo, TLabel>;
    FieldInfoControls: TDictionary<TFieldInfo, TControl>;
    ControlFieldInfos: TDictionary<TControl, TFieldInfo>;
    OriginalEvents:    TDictionary<TControl, TNotifyEvent>;
    TabOrder:          TList<TControl>;
    ShowMandatory: boolean;
    constructor Create;
    destructor Destroy; override;
    procedure BeginUpdate;
    procedure EndUpdate;
    function Updating: boolean;
  end;

  TUIContainerInfos = TObjectDictionary <TFmxObject, TUIContainerInfo>;

  TRecordHistory = record
    ID: TID;
    Brief: string;
    DateTime: TDateTime;
    end;

  TRecordHistoryList = TList<TRecordHistory>;

  TBaseModel = class (TNonARCInterfacedObject, IServerObserver, IServerStateObserver, IAppStateObserver,
                      IForeignTaskObserver, IFreeNotification)
  private
    FForeignTaskObserver: IForeignTaskObserver;
    function GetName: string;
  protected
    FName: string;
    FController: TObservableController;
    FetchDateTime: TDateTime;
    FetchTime: Int64;
    FStates: TAppStateDictionary;
    procedure DoFetch; virtual;
    procedure FPostponedFetch;
    //intf
    procedure ObserveState (State: TServerState; var Outcome: TOutcome); virtual;
    procedure AppStateNotification (const State: TAppState); virtual;
    procedure FreeNotification(AObject: TObject); virtual;
    //

    procedure BusyChanged (Sender: TObject); virtual;
    procedure SetController(const Value: TObservableController);
  public
    apsFetching: TAppState;
    Activity: TActivityElement;

    constructor Create (const AName: string; AController: TObservableController);
    destructor Destroy; override;

    procedure Fetch;
    procedure PostponeFetch;
    procedure Reload;

    property ForeignTaskObserver: IForeignTaskObserver read FForeignTaskObserver implements IForeignTaskObserver;
    property Controller: TObservableController read FController write SetController;
    property Name: string read GetName;
    property States: TAppStateDictionary read FStates;
  end;

  TSqlRecordModel = class;

  TModelForm = class;
  TModelFormClass = class of TModelForm;
  TModelForm = class (TRestrictedSizeForm)
  protected
    FModel: TSqlRecordModel;
    procedure BeforePopulate; virtual;
    procedure AfterPopulate; virtual;
    procedure AfterCollect; virtual;
  public
    procedure SetModel (Model: TSqlRecordModel); virtual;
    property Model: TSqlRecordModel read FModel;
  end;

  TModelForms = class (TNonARCInterfacedObject, IFreeNotification)
    dic: TDictionary<TSqlRecordClass, TModelForm>;
    constructor Create;
    destructor Destroy; override;

    procedure FreeNotification(AObject: TObject);
    function GetInstance(Controller: TObservableController; ASqlRecordClass: TSqlRecordClass; ForceNew: boolean): TModelForm;
  end;


  TSqlRecordModel = class (TBaseModel)
  private
    FAllowedModelActions: TModelActions;
    FTargetFieldName: RawUTF8;
    OriginalFilterEditOnChangeTracking: TNotifyEvent;

    procedure SetFilterEnabled(AValue: boolean);
    procedure FilterEditOnChangeTracking(Sender: TObject);
    procedure FilterEditOnEnter(Sender: TObject);
    procedure OnFilterButtonOnClick (Sender: TObject);
    procedure OnFilterButtonOffClick (Sender: TObject);

    function ControlNameMatches (const ControlName, TableName: string; FieldName: string): boolean;
    function FieldOfControl (Control: TFmxObject): string;
    function FieldInfoOfControl (Control: TFmxObject): TFieldInfo;

    procedure SetUIDialogueForm(const Value: TModelForm);

//    function CollectFieldValueFrom (rec: TSqlRecord; ParentControl: TControl; const FieldName: string): boolean; overload;
//    function CollectFieldValueFrom (rec: TSqlRecord; ParentForm: TForm; const FieldName: string): boolean; overload;

    //function PopulateUI (rec: TSqlRecord; ParentControl: TControl; const FieldName: string): boolean; overload;
    //function PopulateUI (rec: TSqlRecord; ParentForm: TForm; const FieldName: string): boolean; overload;
//    function PopulateUI(rec: TSqlRecord; Parent: TFmxObject; const FieldName: string): boolean; overload;
    procedure UIDialogueFormOnShow (Sender: TObject);
    function GetTargetFieldName: RawUTF8;
    procedure SetUIStates(const Value: word);
    procedure UIStatesChanged;
    function CheckVisibleMandatoryStatus: integer; overload;
    function CheckVisibleMandatoryStatus (const UIContainerInfo: TUIContainerInfo; Control: TControl): integer; overload;
    procedure CheckAllDataEntered;
    procedure SetCrudRecordWithDetails(const Value: TRecordWithDetails);
    procedure CollectGeneralFilter;
    type
      TInternalAction = class (TAction);
    var
      FActive: boolean;
    procedure SetActive(const Value: boolean);
  protected

    FUIStates: word;
    FFilterEdit: TCustomEdit;
//    FFilterButton: TCustomButton;
    FFilterEnabled: boolean;
    FilterChangeTime: TDateTime;
    FFilterButtonOn, FFilterButtonOff: TCustomButton;
    FCrudRecordWithDetails: TRecordWithDetails;
    TestID: integer; // for automatic generation of test data
    FModelActions: TList<TModelAction>;
    uiActions: TList<TAction>;
    FButtons: TList<TCustomButton>;

    FMaster: TSqlRecordModel;

    FModelActionsToActions: TDictionary<TModelAction, TAction>;
    FActionToButton: TDictionary<TBasicAction, TCustomButton>;
    FOriginalEvents: TDictionary<TModelAction, TNotifyEvent>;
    FapsOwnerCU: TAppState;
    FSqlRecordClass: TSqlRecordClass;
    FActionList: TActionList;
    FUIDialogueForm: TModelForm;
    FUILayout: TLayout;
    FUIDialogueOptions: TUIDialogueOptions;
    FLastCreatedID: TID;
    OriginalFilterHint, FGeneralFilter, MasterTableName: string;
    FImageList: TImageList;
    UseRecordWithDetails, Locked: boolean;
    UIContainerInfos: TUIContainerInfos;

    SelectedIDHistory, SelectedRecordHistory: TRecordHistoryList;

    FPreviousSelectedID, FSelectedID, FMasterID,
    FPreviousMasterID: TID; // NB MEANING IS MasterID at time of last fetch

    procedure CheckSelectedRecord; virtual;
    function GetSelectedRecord: TSqlRecord; virtual; abstract;
    procedure SetSelectedRecord(const Value: TSqlRecord); virtual;
    function GetSelectedRecordID: TID;

    procedure CollectUIData (rec: TSqlRecord);
    procedure PopulateUI; overload; virtual;
    procedure PopulateUI(rec: TSqlRecord); overload;

    function GetCrudRecord: TSqlRecord; virtual;
    procedure SetCrudRecord(const Value: TSqlRecord); virtual;
    procedure SetApsOwerCU(const Value: TAppState);
    procedure SetMaster (const Value: TSqlRecordModel); virtual;
    function CreateState (const NameSuffix: string;
                          Timing: TAppStateTiming = TAppStateTiming.Immediate; ChildState: TAppState = nil): TAppState;

    function GetHasMaster: boolean;
    function GetMasterID: TID;
    procedure SetMasterID(const Value: TID);
    procedure MasterIDChanged;
    procedure DoMasterIDChanged; virtual;
    procedure SetGeneralFilter(const Value: string); virtual;
    procedure CreateStates; virtual;

//    function GetSelectedID: TID; virtual; abstract;
    procedure SetSelectedID(const Value: TID);
    procedure DoSetSelectedID(const Value: TID);
    procedure AfterSetSelectedID; virtual;

    procedure OnModelAction(Sender: TObject);
    procedure ExecuteAction(ModelAction: TModelAction); virtual;

    function StatesForbiddenForCreate: TAppStateArray;
    function StatesRequiredForCreate: TAppStateArray;
    function StatesForbiddenForDelete: TAppStateArray;
    function StatesForbiddenForUpdate: TAppStateArray;
    function StatesRequiredForDelete: TAppStateArray;
    function StatesRequiredForUpdate: TAppStateArray;

    procedure SetUILayout(const Value: TLayout);
    function GetActionList: TActionList;
    procedure SetImageList(const Value: TImageList); virtual;

    procedure AddUIContainer(Parent: TFmxObject; ShowMandatory: boolean);
    procedure DoAddUIContainer (Parent: TFmxObject; UIInfo: TUIContainerInfo);
    procedure ControlNotifyEvent (Sender: TObject);
    function GetUIContainerInfo (Obj: TObject): TUIContainerInfo;
    procedure UpdateMandatoryStatus (Control: TControl); overload;
    function UpdateMandatoryStatus(const UIContainerInfo: TUIContainerInfo; Control: TControl): integer; overload;
    function CollectUIValue (rec: TSqlRecord; Control: TControl; Silent: boolean): boolean;
    procedure DoPopulateUI(rec: TSqlRecord; Parent: TFmxObject; ShowMandatory: boolean);
    procedure UpdateFilterHint; virtual;
    procedure DoFetch; override;
  public
    CustomRecordStater: TCustomSqlRecordStater;
    apsFormClosed, apsHasData, apsHasSelectedRecord, //apsSelectionChanging,
    apsSelectedIDChanged, apsSelectedIDChangedDelayed,
    apsSelectedRecordChanged, apsSelectedRecordChangedDelayed: TAppState;
    apsAllDataEntered, apsVisibleControlsValid, apsCreate, apsUpdate, apsCU, apsMasterCU : TAppState;

    ShowMandatoryIndicators, StickyCreate: boolean;
    // e.g. MasterLinkKeyName is normally ID and DetailLinkKeyName is foreign key that points to master record
    // MasterLinkKeyName can be some other key e.g. a foreign key to a third table, that way details are relative to that third table instead
    MasterLinkKeyName, DetailLinkKeyName: RawUTF8;
    // This variable indicates that the purpose of the model is to produce value for this Field, e.g. the lookup models selected ID is stored in this field of the table that needs the lookup
    AdditionalFilter: TQueryFilter;
    QueryParameters: TQueryParameters;
    class var dicControlToModel: TDictionary <TFMXObject, TSqlRecordModel>;
    class constructor Create;
    class destructor Destroy;

    constructor Create (ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController); virtual;
    destructor Destroy; override;

    procedure ClearHistory;

    property SelectedRecord: TSqlRecord read GetSelectedRecord write SetSelectedRecord;
    property SelectedRecordID: TID read GetSelectedRecordID;

//    procedure AssignButtonActionEvents;
//    procedure UnassignButtonActionEvents;

    function StateByName (const Name: string): TAppState;
    procedure Cancel; virtual;
    procedure Edit (ID: TID = NULL_ID); virtual;
//    function SelectedSqlRecord: TSqlRecord; virtual;
    procedure PopulateUI(rec: TSqlRecord; Parent: TFmxObject; ShowMandatory: boolean); overload;
    procedure CollectValuesFrom (rec: TSqlRecord; Parent: TFmxObject);
    function UpdateMandatoryStatus: integer; overload;

    procedure CloseUIDialogue; virtual;
    procedure ShowUIDialogue;
    procedure UIFillTestData; virtual;
    procedure FreeNotification(AObject: TObject); override;

    procedure AttachButton (Button: TCustomButton; ModelAction: TModelAction); virtual;
    procedure AttachButtons (Buttons: TArray<TCustomButton>; ModelActions: TArray<TModelAction>); overload; virtual;
    procedure AttachButtons (Container: TControl); overload; virtual;

    procedure DetachButton (Button: TCustomButton); virtual;
    procedure DetachButtons (Buttons: TArray<TCustomButton>); overload;
    procedure DetachButtons (Container: TControl); overload;
    procedure DetachFilterUI;
    procedure SetFilterUI(const AEdit: TCustomEdit; AButtonOn, AButtonOff: TCustomButton);
    function CopyForTutorial: string;

    property UILayout: TLayout read FUILayout write SetUILayout;
    property UIDialogueOptions: TUIDialogueOptions read FUIDialogueOptions write FUIDialogueOptions;

    property FilterEdit: TCustomEdit read FFilterEdit;
    property FilterEnabled: boolean read FFilterEnabled write SetFilterEnabled;

    property SelectedID : TID read FSelectedID write SetSelectedID;
    property PreviousSelectedID: TID read FPreviousSelectedID;
    property Name: string read FName;
    property Master: TSqlRecordModel read FMaster write SetMaster;
    property MasterID: TID read GetMasterID write SetMasterID;
    property HasMaster: boolean read GetHasMaster;
    property ApsOwnerCU: TAppState read FApsOwnerCU write SetApsOwerCU;
    property SqlRecordClass: TSqlRecordClass read FSqlRecordClass;
    property ActionList: TActionList read GetActionList;
    property UIDialogueForm: TModelForm read FUIDialogueForm write SetUIDialogueForm;
    property Active: boolean read FActive write SetActive;
    property AllowedActions: TModelActions read FAllowedModelActions write FAllowedModelActions;
    property TargetFieldName: RawUTF8 read GetTargetFieldName write FTargetFieldName;
    property ImageList : TImageList read FImageList write SetImageList;
    property GeneralFilter: string read FGeneralFilter write SetGeneralFilter;
    property crudRecord: TSqlRecord read GetCrudRecord write SetCrudRecord;
    property UIStates: word read FUIStates write SetUIStates;
    property CrudRecordWithDetails: TRecordWithDetails read FCrudRecordWithDetails write SetCrudRecordWithDetails;
  end;

  IBaseModelForm = interface
    ['{5A7899EB-23B0-44F4-9190-F7D58E18C5BC}']
    procedure SetModel (Model: TBaseModel);
  end;

var
  ModelForms: TModelForms;
  // contains list of UI controls with associated models:
  // dicControlModels: TDictionary <TFmxObject, TSqlRecordModel>;

  DefaultModelActionHints: array [TModelAction] of string;
  NavigationModelActions: TModelActions = [TModelAction.First, TModelAction.Previous,
                                           TModelAction.Next, TModelAction.Last];

implementation

uses
  FMX.GridLookupEdit, FMX.Models, FMX.HistoryEdit;

var
  lstWarnedAboutInvalid: TList;

constructor TBaseModel.Create (const AName: string; AController: TObservableController);
begin
  inherited Create;
  FStates := TAppStateDictionary.Create ([doOwnsValues]);
  apsFetching := TAppState.CreateAsSingle (AName + 'Fetching', TAppStateTiming.Immediate);
  FStates.Add (apsFetching.Name, apsFetching);
  Activity := TActivityElement.Create (BusyChanged);
  Controller := AController;
end;

destructor TBaseModel.Destroy;
begin
  CancelPostponedCalls (self);
  Activity.Free;
//  for var pair in FStates do
//      pair.Value.Free;
  FStates.Free;
  inherited;
end;

function TBaseModel.GetName: string; // IName
begin
  result := FName;
end;

procedure TBaseModel.ObserveState(State: TServerState; var Outcome: TOutcome);
begin
  if State = TServerState.BeforeControllerDestroy
     then Controller := nil;
end;

procedure TBaseModel.AppStateNotification (const State: TAppState);
begin

end;

procedure TBaseModel.FreeNotification(AObject: TObject);
begin

end;

procedure TBaseModel.Fetch;
begin
  CancelPostponedCall(FPostponedFetch);
  DoFetch;
end;

procedure TBaseModel.FPostponedFetch;
begin
  DoFetch;
end;

procedure TBaseModel.DoFetch;
begin
  FetchDateTime := now;
  FetchTime := GetTickCount64;
  apsFetching.Activate;
  Activity.BeginActivity;
end;

procedure TBaseModel.PostponeFetch;
begin
  PostponeOnce (FPostponedFetch, 500);
end;

procedure TBaseModel.Reload;
begin
  if not ApsFetching.Active then Fetch;
end;

procedure TBaseModel.BusyChanged(Sender: TObject);
begin
end;

procedure TBaseModel.SetController(const Value: TObservableController);
begin
  if FController.IsAssigned
     then FController.Unsubscribe(self);

  FController := Value;

  if FController.IsAssigned
     then FController.Subscribe(self);
end;


procedure TModelForm.AfterCollect;
begin

end;

procedure TModelForm.AfterPopulate;
begin

end;

procedure TModelForm.BeforePopulate;
begin

end;

procedure TModelForm.SetModel(Model: TSqlRecordModel);
begin
  FModel := Model;
end;

{ TModelForms }

constructor TModelForms.Create;
begin
  inherited;
  dic := TDictionary<TSqlRecordClass, TModelForm>.Create;
end;

destructor TModelForms.Destroy;
begin
  dic.Free;
  inherited;
end;

procedure TModelForms.FreeNotification(AObject: TObject);
begin
  for var pair in dic do
    if pair.Value = AObject then
      begin
        dic.Remove(pair.Key);
        exit;
      end;
end;

procedure PrepareEnumComboBox (fie: TFieldInfoEnum; cbx: TComboBox);
begin
  var lsp := dicEnumToDisplayValues[fie.TypeName];
  for var i := 0 to lsp.Count - 1 do
    cbx.Items.Add (lsp[i]^);
end;

function TModelForms.GetInstance(Controller: TObservableController; ASqlRecordClass: TSqlRecordClass; ForceNew: boolean): TModelForm;
var
  cl: TClass;
  FUIClass: TModelFormClass;
  ctrl: TControl;
  PropInfo: TSQLPropInfo;
//  gle: TGridLookupEdit;
  fi: TFieldInfo;

  procedure PositionControl;
  begin
    if ctrl.IsNotAssigned then exit;

    if result.ActiveControl.IsNotAssigned
      then result.ActiveControl := ctrl;

    var lay := result.FindComponent ('layMain');
    if (lay <> nil) and (lay is TLayout)
      then ctrl.Parent := TLayout(lay)
      else ctrl.Parent := result;
    ctrl.Align := TAlignLayout.Top;
    ctrl.Margins.Left   := 5;
    if (ctrl is TCheckBox)
       then ctrl.Margins.Top    := 15
       else ctrl.Margins.Top    := 30;
    ctrl.Margins.Right  := 5;
    ctrl.Margins.Bottom := 5;
    ctrl.Align := TAlignLayout.MostTop;
    ctrl.Height := ctrl.Height * 1.2;
    if not (ctrl is TCheckBox) then
      begin
        var lab := TLabel.Create(ctrl);
//        if result.FindComponent ('lab' + fi.Name)
//          then exit;
        lab.Name := 'lab' + fi.Name;
        lab.Parent := ctrl;
        lab.Position.X := 2;
        lab.Position.Y := -16;
        lab.AutoSize := true;
        lab.TextSettings.WordWrap := false;
      end;
  end;

begin
  if not ForceNew and dic.TryGetValue(ASqlRecordClass, result) then exit;

  var s := ASqlRecordClass.crudSQLTableName.AsString;
  cl := FindClassByName ('Tfrm' + s); // find form class that matches the sql record name
  if (cl = nil) then
    cl := FindClassByName  ('Tfrm' + s + 'Input');
  if (cl = nil) and (s.EndsWith('Table')) then
    cl := FindClassByName ('Tfrm' + s.Substring(0, s.Length - 5));
  if (cl = nil) then
    cl := FindClassByName ('TfrmAutoInput');

  if cl = nil then ShowErrorAndAbort('No form for ' + ASqlRecordClass.crudSQLTableName.AsString);
//  ctrl := nil;
  if cl.InheritsFrom (TModelForm)
    then
      begin
        FUIClass := TModelFormClass(cl);
        Application.CreateForm(FUIClass, result);

        result.FreeNotification(self);
        try
//          result.Name := copy (result.ClassName, 2); // tutorial needs name, maybe noone else
          result.Name := 'frm' + ASqlRecordClass.crudSQLTableName.AsString;
        except
          // ignore if perhaps we need more than one form of this type?
        end;

        if ClassHasAttribute (FUIClass, AutoUIAttribute)
           or ClassHasAttribute (FUIClass.ClassParent, AutoUIAttribute) then
          begin
            var ctx := TRttiContext.Create;
            try
              //var t := ctx.GetType(ASqlRecordClass);
              //for var p in t.GetProperties do
              for fi in ASqlRecordClass.CrudClass.FieldInfoList do
                begin
                  PropInfo := ASqlRecordClass.FieldByName (fi.Name);
                  if PropInfo = nil then continue; // ID je sad u FieldInfo listi ali nema propinfo jer mormot...

                  ctrl := nil;
                  if ( (PropInfo.SQLFieldType = TSQLFieldType.sftInteger) and (fi.ForeignTableName <> '') )
                     then
                        begin
                          var edit := TEdit.Create(result);
                          ctrl := edit;
                          PositionControl;
                          edit.Name := 'edt' + fi.Name;
                          {gle := }TGridLookupEdit.Create (Controller.Model.Table [StringToUTF8(fi.ForeignTableName)], edit,
                                                             Controller, ASqlRecordClass, fi.Name {fi.ForeignKeyName});
                          continue; // skip re-positioning the control
                        end
                     else if not fi.Displayable then continue;

                  if fi.IsEnumeratedType then
                    begin
                      var fie := TFieldInfoEnum(fi);
                      var cbx := TComboBox.Create(result);
                      PrepareEnumComboBox (fie, cbx);
                      cbx.Name := 'cbx' + fi.Name;
                      ctrl := cbx;
                    end;

                  if ctrl = nil then case PropInfo.SQLFieldType of
                    TSQLFieldType.sftDateTime:
                      begin
                        if fi.IsDate and result.FindComponent ('dae' + fi.Name).IsNotAssigned then
                          begin
                            var dae := TDateEdit.Create(result);
                            ctrl := dae;
                            dae.ShowClearButton := not ASqlRecordClass.IsMandatoryField (fi.Name);
                            dae.Name := 'dae' + fi.Name;
                          end;

                        if fi.IsTime and result.FindComponent ('tie' + fi.Name).IsNotAssigned then
                          begin
                            PositionControl;
                            var tie := TTimeEdit.Create(result);
                            ctrl := tie;
                            tie.Name := 'tie' + fi.Name;
                          end;
                      end;
                    TSQLFieldType.sftUTF8Text, TSQLFieldType.sftCurrency, TSQLFieldType.sftInteger,
                                               TSQLFieldType.sftFloat:
                      begin
                        if result.FindComponent ('edt' + fi.Name) <> nil then continue;
                        if result.FindComponent ('mem' + fi.Name) <> nil then continue;
                        var edit := TEdit.Create(result);
                        ctrl := edit;
                        edit.Name := 'edt' + fi.Name;
                      end;
                    TSQLFieldType.sftBoolean:
                      begin
                        if result.FindComponent ('chb' + fi.Name) <> nil then continue;
                        var chb := TCheckBox.Create(result);
                        ctrl := chb;
                        chb.Name := 'chb' + fi.Name;
                      end;
                  end;//case

                  if ctrl.IsAssigned
                    then PositionControl;

//                  if gle.IsAssigned
//                     then gle.CreateClearButton;
                end;
            finally
              ctx.Free;
            end;
          end;

        AutoTabOrder (result);
        if not dic.ContainsKey (ASqlRecordClass)
           then dic.Add(ASqlRecordClass, result);
        result.AutoHeight;
      end
    else result := nil;
end;

{ TUIContainerInfo }

procedure TUIContainerInfo.BeginUpdate;
begin
  inc (FUpdatingCount);
end;

constructor TUIContainerInfo.Create;
begin
  inherited;
  FieldInfoLabels   := TDictionary<TFieldInfo, TLabel>.Create;
  FieldInfoControls := TDictionary<TFieldInfo, TControl>.Create;
  ControlFieldInfos := TDictionary<TControl, TFieldInfo>.Create;
  OriginalEvents    := TDictionary<TControl, TNotifyEvent>.Create;
  TabOrder          := TList<TControl>.Create;
end;

destructor TUIContainerInfo.Destroy;
begin
  FieldInfoLabels.Free;
  FieldInfoControls.Free;
  ControlFieldInfos.Free;
  OriginalEvents.Free;
  TabOrder.Free;

  inherited;
end;

procedure TUIContainerInfo.EndUpdate;
begin
  dec (FUpdatingCount);
end;

function TUIContainerInfo.Updating: boolean;
begin
  result := FUpdatingCount > 0;
end;

{ TBaseModel }

procedure TSqlRecordModel.Cancel;
begin
end;

class constructor TSqlRecordModel.Create;
begin
  inherited;
  dicControlToModel := TDictionary <TFMXObject, TSqlRecordModel>.Create;
end;

class destructor TSqlRecordModel.Destroy;
begin
  dicControlToModel.Free;
  inherited;
end;

constructor TSqlRecordModel.Create (ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController);
begin
  if not AController.Model.Contains (ASqlRecordClass) then
     raise Exception.Create(ASqlRecordClass.SQLTableName.AsString + ' is not part of the model!');

  inherited Create (AName, AController);

  FPreviousMasterID := INVALID_ID;
  SelectedIDHistory := TRecordHistoryList.Create;
  SelectedRecordHistory := TRecordHistoryList.Create;

  UIContainerInfos := TUIContainerInfos.Create ([doOwnsValues]);
  FForeignTaskObserver := TForeignTaskObserver.Create (false);
  FUIDialogueOptions := [TUIDialogueOption.AutoUI, TUIDialogueOption.CloseOnSuccess,
                         TUIDialogueOption.CloseOnCancelUpdate, TUIDialogueOption.CloseOnCancelCreate,
                         TUIDialogueOption.Modal
                        {, TUIDialogueOption.AutoTabOrder ommited because the base form class does autoorder,
                        unclear if this is a good option here in general}];
  FAllowedModelActions := [TModelAction.First, TModelAction.Previous, TModelAction.Next, TModelAction.Last,
                           TModelAction.Post, TModelAction.Cancel, TModelAction.Show];

  ShowMandatoryIndicators := true;

  if AController.LoggedUser.IsAssigned then
    begin
      if ASqlRecordClass.CrudAllowed (TCRUD.Create, AController.LoggedUser.GroupRightsID)
                                  then include (FAllowedModelActions, TModelAction.Create);
      if ASqlRecordClass.CrudAllowed (TCRUD.Read, AController.LoggedUser.GroupRightsID)
                                  then include (FAllowedModelActions, TModelAction.Read);
      if ASqlRecordClass.CrudAllowed (TCRUD.Update, AController.LoggedUser.GroupRightsID)
                                  then include (FAllowedModelActions, TModelAction.Update);
      if ASqlRecordClass.CrudAllowed (TCRUD.Delete, AController.LoggedUser.GroupRightsID)
                                  then include (FAllowedModelActions, TModelAction.Delete);
    end;

  FSqlRecordClass := ASqlRecordClass;
  if AName = ''
     then FName := ClassName
     else FName := AName;

  CreateStates;

  FModelActions := TList<TModelAction>.Create;
  uiActions := TList<TAction>.Create;
  FButtons := TList<TCustomButton>.Create;
  FModelActionsToActions := TDictionary<TModelAction, TAction>.Create;
  FActionToButton := TDictionary<TBasicAction, TCustomButton>.Create;
  FOriginalEvents := TDictionary<TModelAction, TNotifyEvent>.Create;
end;

destructor TSqlRecordModel.Destroy;
begin
  SetFilterUI (nil, nil, nil);
  UnRegisterStateObserver(self);
  Controller := nil;
  FreeAndNil (CustomRecordStater);
  FreeAndNil (FModelActions);
  FreeAndNil (uiActions);
  FreeAndNil (FButtons);
  FreeAndNil (FModelActionsToActions);
  FreeAndNil (FActionToButton);
  FreeAndNil (FOriginalEvents);
  FreeAndNil (UIContainerInfos);
  FreeAndNil (SelectedIDHistory);
  FreeAndNil (SelectedRecordHistory);
  inherited;
end;

function TSqlRecordModel.StatesRequiredForCreate: TAppStateArray;
begin
  if Master.IsAssigned
    then result := [Master.apsHasSelectedRecord]
    else result := [];
  if CustomRecordStater.IsAssigned
     then result := result + CustomRecordStater.RequiredStates(TModelAction.Create);
  if Master.IsAssigned and Master.CustomRecordStater.IsAssigned
     then result := result + Master.CustomRecordStater.RequiredDetailStates(SqlRecordClass, TModelAction.Create);
end;

function TSqlRecordModel.StatesForbiddenForCreate: TAppStateArray;
begin
  result := [apsCU, apsOwnerCU];
  if CustomRecordStater.IsAssigned
     then result := result + CustomRecordStater.ForbiddenStates(TModelAction.Create);
  if Master.IsAssigned and Master.CustomRecordStater.IsAssigned
     then result := result + Master.CustomRecordStater.ForbiddenDetailStates(SqlRecordClass, TModelAction.Create);
  if Master.IsAssigned
     then result := result + [Master.apsCU];
end;

function TSqlRecordModel.StatesRequiredForUpdate: TAppStateArray;
begin
  result := [apsHasSelectedRecord];
  if CustomRecordStater.IsAssigned
     then result := result + CustomRecordStater.RequiredStates(TModelAction.Update);
  if Master.IsAssigned and Master.CustomRecordStater.IsAssigned
     then result := result + Master.CustomRecordStater.RequiredDetailStates(SqlRecordClass, TModelAction.Update);
end;

procedure TSqlRecordModel.UIFillTestData;
begin

end;

procedure TSqlRecordModel.FreeNotification(AObject: TObject);
begin
  if AObject = FFilterEdit
     then FFilterEdit := nil;
end;

function TSqlRecordModel.StatesForbiddenForUpdate: TAppStateArray;
begin
  result := [apsCU, apsOwnerCU];
  if CustomRecordStater.IsAssigned
     then result := result + CustomRecordStater.ForbiddenStates(TModelAction.Update);
  if Master.IsAssigned and Master.CustomRecordStater.IsAssigned
     then result := result + Master.CustomRecordStater.ForbiddenDetailStates(SqlRecordClass, TModelAction.Update);
  if Master.IsAssigned
     then result := result + [Master.apsCU];
end;

function TSqlRecordModel.StatesRequiredForDelete: TAppStateArray;
begin
  result := [apsHasSelectedRecord];
  if CustomRecordStater.IsAssigned
     then result := result + CustomRecordStater.RequiredStates(TModelAction.Delete);
  if Master.IsAssigned and Master.CustomRecordStater.IsAssigned
     then result := result + Master.CustomRecordStater.RequiredDetailStates(SqlRecordClass, TModelAction.Delete);
end;

function TSqlRecordModel.StatesForbiddenForDelete: TAppStateArray;
begin
  result := [apsCU, apsOwnerCU];
  if CustomRecordStater <> nil
     then result := result + CustomRecordStater.ForbiddenStates(TModelAction.Delete);
  if Master.IsAssigned and Master.CustomRecordStater.IsAssigned
     then result := result + Master.CustomRecordStater.ForbiddenDetailStates(SqlRecordClass, TModelAction.Delete);
  if Master.IsAssigned
     then result := result + [Master.apsCU];
end;

procedure TSqlRecordModel.Edit(ID: TID);
begin
  if ID.IsNULL then ID := SelectedID;
  Controller.RecordReadLock (SqlRecordClass, ID, self);
end;

procedure TSqlRecordModel.ExecuteAction(ModelAction: TModelAction);
begin

end;

function TSqlRecordModel.CreateState (const NameSuffix: string;
                                 Timing: TAppStateTiming = TAppStateTiming.Immediate; ChildState: TAppState = nil): TAppState;
begin
  result := TMormotAppState.Create (SqlRecordClass, FName + '.' + NameSuffix, Timing, ChildState);
  FStates.Add (result.Name, result);
end;

procedure TSqlRecordModel.CreateStates;
begin
  apsSelectedIDChanged :=            CreateState ('SelectedIDChanged', TAppStateTiming.Immediate);
  apsSelectedIDChangedDelayed :=     CreateState ('SelectedIDChangedDelayed', TAppStateTiming.Delayed);

  apsSelectedRecordChanged :=        CreateState ('SelectedRecordChanged', TAppStateTiming.Immediate);
  apsSelectedRecordChangedDelayed := CreateState ('SelectedRecordChangedDelayed', TAppStateTiming.Delayed);

  //apsSelectionChanging :=            CreateState ('SelChanging');
  apsHasData           :=            CreateState ('HasData');
  apsHasSelectedRecord :=            CreateState ('HasSelectedRecord');
end;

function TSqlRecordModel.GetTargetFieldName: RawUTF8;
begin
  if FTargetFieldName <> ''
     then Result := FTargetFieldName
     else Result := SqlRecordClass.crudSQLTableName + 'ID';
end;

procedure TSqlRecordModel.DoMasterIDChanged;
begin

end;

procedure TSqlRecordModel.MasterIDChanged;
begin
  if FPreviousMasterID <> MasterID
    then DoMasterIDChanged;
end;

function TSqlRecordModel.GetHasMaster: boolean;
begin
  result := FMaster.IsAssigned;
end;

function TSqlRecordModel.GetMasterID: TID;
begin
  if FMaster.IsAssigned
     then result := FMaster.SelectedID
     else result := FMasterID;
end;

//function TSqlRecordModel.SelectedSqlRecord: TSqlRecord;
//begin
//  result := nil;
//end;

procedure TSqlRecordModel.OnModelAction(Sender: TObject);
begin
  var ModelAction := TModelAction (TComponent(sender).Tag);
  var NotifyEvent : TNotifyEvent;

  if FOriginalEvents.TryGetValue (ModelAction, NotifyEvent) and Assigned(NotifyEvent)
     then NotifyEvent (Sender);

  ExecuteAction (ModelAction);
end;

//procedure TSqlRecordModel.UnassignButtonActionEvents;
//begin
//  var i := 0;
//
//  for var Action in uiActions do
//    begin
//      var ma := FModelActions [i];
//      Action.OnExecute := FOriginalEvents [ma];
//
//      FActions[ma] := Action;
//      if (TMethod(FOriginalEvents [ma]) = TMethod(Action.OnExecute)) and (TMethod(FOriginalEvents [ma]).Code <> nil)
//         then raise Exception.Create('Buttons assigned twice?')
//         else ;
//      Action.OnExecute := OnModelAction;
//      inc (i);
//    end;
//end;

procedure TSqlRecordModel.SetActive(const Value: boolean);
begin
  FActive := Value;
  // not used atm
end;

procedure TSqlRecordModel.SetCrudRecord(const Value: TSqlRecord);
begin

end;

procedure TSqlRecordModel.SetCrudRecordWithDetails(const Value: TRecordWithDetails);
begin
  FCrudRecordWithDetails := Value;
  UseRecordWithDetails := true;
end;

procedure TSqlRecordModel.SetImageList(const Value: TImageList);
begin
  FImageList := Value;
end;

procedure TSqlRecordModel.SetMaster (const Value: TSqlRecordModel);
begin
  if Value = FMaster then exit;

  UnRegisterStateObserver (self);
  FMaster := Value;
  if FMaster.IsAssigned then
     begin
       RegisterStateObserver (self, TAppStateAction.Notification,
                                    [FMaster.apsSelectedIDChanged, FMaster.apsSelectedRecordChangedDelayed], []);
       apsMasterCU := FMaster.apsCU;
       MasterTableName := FMaster.SqlRecordClass.SQLTableName.AsString;
       MasterLinkKeyName := 'ID';
       DetailLinkKeyName := FMaster.SqlRecordClass.crudSQLTableName + 'ID';
     end
     else
       apsMasterCU := nil;
end;

procedure TSqlRecordModel.SetMasterID(const Value: TID);
begin
  FMasterID := Value;
  MasterIDChanged;
end;

procedure TSqlRecordModel.DoSetSelectedID(const Value: TID); // only called if ID is about to be actually changed
var
  h: TRecordHistory;
begin
  h.ID := FSelectedID;
  h.DateTime := now;
  SelectedIDHistory.Add(h);
  while SelectedIDHistory.Count > 20 do SelectedIDHistory.Delete(0);

  FPreviousSelectedID := FSelectedID;
  FSelectedID := Value;

  apsSelectedIDChanged.Activate;
  apsSelectedIDChangedDelayed.Activate;
end;

procedure TSqlRecordModel.CheckSelectedRecord;
var
  h: TRecordHistory;
  r: TSqlRecord;
  id: TID;
begin
  r := SelectedRecord;
  if r.IsAssigned
     then id := r.IDValue
     else id := NULL_ID;

  if (SelectedRecordHistory.Count = 0) or (SelectedRecordHistory[SelectedRecordHistory.Count - 1].ID <> id) then
    begin
      h.ID := id;
      h.DateTime := now;
      if r.IsAssigned and (r is TSqlRecordEx)
         then h.Brief := TSqlRecordEx(r).AsBriefString;
      SelectedRecordHistory.Add(h);
      apsSelectedRecordChanged.CycleOnOff;
      apsSelectedRecordChangedDelayed.CycleOnOff;
    end;

  ApsHasSelectedRecord.SetActiveIf(SelectedRecord.IsAssigned);
end;


procedure TSqlRecordModel.SetSelectedID(const Value: TID);
begin
  if not Locked and (Value <> SelectedID) then
    begin
      DoSetSelectedID(Value);
      AfterSetSelectedID;
    end;
end;

procedure TSqlRecordModel.SetSelectedRecord(const Value: TSqlRecord);
begin
  raise Exception.Create('This model cannot set selected record');
end;

function TSqlRecordModel.GetSelectedRecordID: TID;
var
  rec: TSqlRecord;
begin
  rec := SelectedRecord;
  if rec.IsAssigned
     then result := rec.IDValue
     else result := NULL_ID;
end;

procedure TSqlRecordModel.AfterSetSelectedID;
begin
  if SelectedID = SelectedRecordID
    then CheckSelectedRecord
    else if SelectedID.IsNotNULL then Fetch;
end;

function TSqlRecordModel.StateByName(const Name: string): TAppState;
begin
  FStates.TryGetValue (Name, result);
end;

procedure TSqlRecordModel.ClearHistory;
begin
  SelectedIDHistory.Clear;
  SelectedRecordHistory.Clear;
end;

procedure TSqlRecordModel.CloseUIDialogue;
begin
  if FUIDialogueForm <> nil
    then FUIDialogueForm.Close;
end;

procedure TSqlRecordModel.UIDialogueFormOnShow (Sender: TObject);
begin
  if FUILayout <> nil
    then FocusControlOrChild (FUILayout)
    else FocusControlOrChild (FUIDialogueForm);
end;

procedure TSqlRecordModel.ShowUIDialogue;
begin
//  if not (TUIDialogueOption.Enabled in UIDialogueOptions) then exit;

  if (FUIDialogueForm = nil) and (FUILayout = nil)
    then FUIDialogueForm := ModelForms.GetInstance(Controller, SqlRecordClass, true);
  if FUIDialogueForm = nil then
      begin
        if FUILayout = nil then raise Exception.Create('UI Layout not specified!');

        FUIDialogueForm := TModelForm.CreateNew(Application);
        FUIDialogueForm.OnShow := UIDialogueFormOnShow;
        var parentForm := GetParentForm(FUILayout);
        var display := Screen.DisplayFromForm(ParentForm);
        FUIDialogueForm.Left := round((display.WorkareaRect.Width  - FUIDialogueForm.Width)  / 2);
        FUIDialogueForm.Top  := round((display.WorkareaRect.Height - FUIDialogueForm.Height) / 2);
        FUIDialogueForm.BorderStyle := TFmxFormBorderStyle.SizeToolWin;

        FUILayout.Parent := FUIDialogueForm;
        FUILayout.Visible := true;
        FUIDialogueForm.HandleNeeded;


        //FUIDialogueForm.ClientHeight := ceil(FUILayout.Height); //ceil (maxheight + 10);
        var maxheight : double := 0;
        for var control in FUILayout.Controls do
          maxheight := max (maxheight, control.Position.Y + control.Size.Height + control.Margins.Bottom);

        FUIDialogueForm.MinWidth := ceil (FUILayout.Width);
        FUIDialogueForm.MinHeight := ceil (maxheight + 10);
        FUIDialogueForm.ClientWidth := ceil (FUILayout.Width);
        FUIDialogueForm.ClientHeight := ceil (maxheight + 10);

        FUILayout.Align := TAlignLayout.Client;
        FUIDialogueForm.Position := TFormPosition.ScreenCenter;
      end;

  if FUIDialogueForm.Visible and (TUIDialogueOption.CloseOnSuccess in UIDialogueOptions)
    then ShowErrorAndAbort('Molim završite sa postojećim unosom za: ' +
                                  Controller.Model.GetTableDisplayName(SqlRecordClass));

  if FUIDialogueForm.Caption = '' then
     FUIDialogueForm.Caption := Controller.Model.GetTableDisplayName(SqlRecordClass);

  if FUIDialogueForm.Model <> self
     then FUIDialogueForm.SetModel (self);

  if not FModelActionsToActions.ContainsKey (TModelAction.Post)
     then raise Exception.Create('Post button not assigned for ' + self.Name);
  if not FModelActionsToActions.ContainsKey (TModelAction.Cancel)
     then raise Exception.Create('Cancel button not assigned for ' + self.Name);

  if TUIDialogueOption.AutoUI in UIDialogueOptions
     then begin
            FUIDialogueForm.BeforePopulate;
            PopulateUI;
            FUIDialogueForm.AfterPopulate;
          end;

  if apsFormClosed = nil then
    begin
      apsFormClosed := TFormClosedAppState.FindOrCreateFor(FUIDialogueForm);
      RegisterStateObserver(self, TAppStateAction.Notification, [apsFormClosed], []);
    end;

  if {$IFDEF DEBUG} false {$ELSE} TUIDialogueOption.Modal in UIDialogueOptions {$ENDIF}
    then
      begin
        FUIDialogueForm.FormStyle := TFormStyle.StayOnTop;
        FUIDialogueForm.ShowModal;
      end
    else
      FUIDialogueForm.Show;
end;

procedure TSqlRecordModel.SetUIDialogueForm(const Value: TModelForm);
begin
  FUIDialogueForm := Value;
  if Value <> nil
     then begin
            Include (FUIDialogueOptions, TUIDialogueOption.Enabled);
            Value.SetModel(self);
            if TUIDialogueOption.AutoTabOrder in UIDialogueOptions
               then AutoTabOrder (Value);
          end
     else Exclude (FUIDialogueOptions, TUIDialogueOption.Enabled);

  if Value <> nil then
     UIStateRegistry.RegisterObserver (FUIDialogueForm, TAppStateAction.Notification, [apsCU], []);
end;

procedure TSqlRecordModel.SetUILayout(const Value: TLayout);
begin
  FUILayout := Value;
  if FUILayout = nil then exit;
  FUILayout.Visible := false;
  Include (FUIDialogueOptions, TUIDialogueOption.Enabled);
  if TUIDialogueOption.AutoTabOrder in UIDialogueOptions
     then AutoTabOrder (Value);
  AttachButtons(Value);
end;

procedure TSqlRecordModel.UIStatesChanged;
begin
  UpdateMandatoryStatus;
end;

procedure TSqlRecordModel.SetUIStates(const Value: word);
begin
  FUIStates := Value;
  PostponeOnce (UIStatesChanged);
end;

function TSqlRecordModel.GetActionList: TActionList;
begin
  if FActionList = nil then
    begin
      FActionList := TActionList.Create (nil);
      FActionList.Images := ImageList;

//      var actStates := TAction.Create (ActionList);
//      actStates.ShortCut := TextToShortCut('CTRL+ALT+S');
//      actStates.OnExecute := OnShowStatesUI;
    end;
  result := FActionList;
end;

function TSqlRecordModel.GetCrudRecord: TSqlRecord;
begin
  result := nil;
end;

procedure TSqlRecordModel.CollectValuesFrom (rec: TSqlRecord; Parent: TFmxObject);
var
  Obj: TFmxObject;
  Model: TSqlRecordModel;
  FieldInfo: TFieldInfo;
  FieldName: string;
begin
  if Parent.Children <> nil
    then for Obj in Parent.Children do
      begin
        if Obj is TControl then
          CollectValuesFrom (rec, TControl(Obj));

        FieldName := FieldOfControl (obj);
        if FieldName = '' then continue;

        FieldInfo := rec.FieldInfoList.ItemsByName[FieldName];
        if FieldInfo.IsNotAssigned then
           if now <> 0 then continue; // indicates potential problem

        if not ClassHasAttribute (FieldInfo.ClassType, AutoUIAttribute) then continue;

        if dicControlToModel.TryGetValue (Obj, Model)
            then if rec.HasFieldByName (FieldName)
                 then rec.SetFieldVariant (FieldName, Model.SelectedID)
                 else raise Exception.Create('Record does not have field ' + FieldName)
            else
              begin
                if not rec.crudClass.HasFieldByName (FieldName) then continue;

        //        if Control is Tdate
        //           then crudRecord.SetFieldVariant (FieldName, TCustomEdit(Control).Text);
                if Obj is TTimeEdit then
                  begin
                     var t: TDateTime := TTimeEdit(Obj).GetSmartTime + trunc (rec.GetFieldVariant(FieldName));
                     SetPropValue(rec, FieldName, t);
                  end
                else if Obj is TDateEdit then
                   begin
                     var dt: TDate := TDateEdit(Obj).GetSmartDate + frac (rec.GetFieldVariant(FieldName));
                     SetPropValue(rec, FieldName, dt);
                   end
                else if Obj is TComboBox
                   then if FieldInfo.IsEnumeratedType or not FieldInfo.IsTextual
                        then rec.SetFieldVariant (FieldName, TComboBox(Obj).ItemIndex)
                        else rec.SetFieldVariant (FieldName, TComboBox(Obj).Text)
                else if Obj is TCustomEdit
                   then if not rec.TrySetFieldStringValue (FieldName, TCustomEdit(Obj).Text)
                        then ShowErrorAndAbort('Vrednost: ' + TCustomEdit(Obj).Text + #13#10' ne može biti upisana u polje ' + FieldName)
                        else
                else if Obj is TCustomMemo then
                  begin
                     if not rec.TrySetFieldStringValue (FieldName, TCustomMemo(Obj).Text)
                        then ShowErrorAndAbort('Vrednost: ' + TCustomEdit(Obj).Text + #13#10' ne može biti upisana u polje ' + FieldName);
                  end
                else if Obj is TCheckBox
                   then rec.SetFieldVariant (FieldName, TCheckBox(Obj).IsChecked);
              end;
      end;
end;

procedure TSqlRecordModel.CollectUIData (rec: TSqlRecord);
begin
  if FUIDialogueForm <> nil
     then begin
            CollectValuesFrom(rec, FUIDialogueForm);
            FUIDialogueForm.AfterCollect;
          end
     else if FUILayout <> nil
          then CollectValuesFrom(rec, FUILayout);
end;

procedure TSqlRecordModel.PopulateUI;
begin
end;

procedure TSqlRecordModel.PopulateUI(rec: TSqlRecord);
begin
  if FUIDialogueForm <> nil
     then PopulateUI(rec, FUIDialogueForm, ShowMandatoryIndicators)
     else if FUILayout <> nil
          then PopulateUI(rec, FUILayout, ShowMandatoryIndicators);
  PostponeOnce (UIStatesChanged);
//  apsAllDataEntered.SetActiveIf (UpdateMandatoryStatus = 0);
end;

procedure TSqlRecordModel.CollectGeneralFilter;
begin
  if (FFilterEdit = nil) or not FilterEnabled
     then GeneralFilter := ''
     else GeneralFilter := FFilterEdit.Text;
end;

procedure TSqlRecordModel.FilterEditOnEnter(Sender: TObject);
begin
  (Sender as TCustomEdit).SelectAll;
end;

procedure TSqlRecordModel.DetachFilterUI;
begin
  if FFilterEdit <> nil
    then begin
           FFilterEdit.RemoveFreeNotify(self);
           FFilterEdit.OnChangeTracking := OriginalFilterEditOnChangeTracking;
         end;

  if assigned (FFilterButtonOn) then
     FFilterButtonOn.OnClick := nil;
  if assigned (FFilterButtonOff) then
     FFilterButtonOff.OnClick := nil;

  FFilterEdit := nil;
  FFilterButtonOn := nil;
  FFilterButtonOff := nil;
end;

procedure TSqlRecordModel.SetFilterUI(const AEdit: TCustomEdit; AButtonOn, AButtonOff: TCustomButton);
begin
  DetachFilterUI;

  FFilterEdit := AEdit;
  FFilterButtonOn := AButtonOn;
  FFilterButtonOff := AButtonOff;

  if assigned (AButtonOn) then
     AButtonOn.OnClick := OnFilterButtonOnClick;
  if assigned (AButtonOff) then
     AButtonOff.OnClick := OnFilterButtonOffClick;

  if FFilterEdit = nil then exit;
  FFilterEdit.AddFreeNotify(self);
  OriginalFilterEditOnChangeTracking := FFilterEdit.OnChangeTracking;
  FFilterEdit.OnChangeTracking := FilterEditOnChangeTracking;
  FFilterEdit.OnEnter := FilterEditOnEnter;
  OriginalFilterHint := FFilterEdit.Hint;
  UpdateFilterHint;
end;

procedure TSqlRecordModel.UpdateFilterHint;
var
  s: string;
  FieldInfo: TFieldInfo;
begin
  if FFilterEdit = nil then exit;
  s := '';
  for FieldInfo in SqlRecordClass.FieldInfoList.GeneralFilter do
    begin
      if s <> '' then s := s + ', ';
      s := s + Controller.Model.GetFieldDisplayName (SqlRecordClass, FieldInfo.Name);
    end;
  if OriginalFilterHint = ''
     then FFilterEdit.Hint := 'Opšti filter'#13 + s
     else FFilterEdit.Hint := OriginalFilterHint + #13 + s;
  FFilterEdit.ShowHint := true;
end;

procedure TSqlRecordModel.OnFilterButtonOnClick(Sender: TObject);
begin
  FilterEnabled := true;
end;

procedure TSqlRecordModel.OnFilterButtonOffClick(Sender: TObject);
begin
  FilterEnabled := false;
end;

procedure TSqlRecordModel.SetGeneralFilter(const Value: string);
begin
  FGeneralFilter := Value;
end;

procedure TSqlRecordModel.FilterEditOnChangeTracking(Sender: TObject);
begin
  FilterEnabled := FFilterEdit.Text <> '';
  if assigned (OriginalFilterEditOnChangeTracking)
     then OriginalFilterEditOnChangeTracking(Sender);
end;

procedure TSqlRecordModel.SetFilterEnabled(AValue: boolean);
begin
  FFilterEnabled := AValue;
//  if FFilterButton <> nil then
//    if FFilterEnabled
//      then FFilterButton.ImageIndex := II_FilterOn
//      else FFilterButton.ImageIndex := II_FilterOff;
  PostponeOnce (CollectGeneralFilter, 500);
end;

function TSqlRecordModel.ControlNameMatches (const ControlName, TableName: string; FieldName: string): boolean;
// npr. moze edtPerson a moze i edtDocumentPerson u smislu tabela je document a field person ako vise od jedne tabele ima kontrole na formi
begin
  var temp := ControlName.ToLower.Substring (3); // to lowercase and remove first 3 chars

  if FieldName.EndsWith ('TableID')
     then FieldName := FieldName.ToLower.Substring(0, FieldName.Length-7) {TODO -oOwner -cGeneral : unused I think}
     else FieldName := FieldName.ToLower;
  result := (temp = FieldName) or (temp = TableName.ToLower + FieldName);
end;

function TSqlRecordModel.FieldOfControl (Control: TFmxObject): string;
var
  Model: TSqlRecordModel;
  Name: string;
begin
  if Control.Name = '' then exit ('');

  if dicControlToModel.TryGetValue (Control, Model) // check if a model is associated with the control
     then exit (Model.TargetFieldName.AsString);

  for var fi in SqlRecordClass.FieldInfoList.All do
    begin
      Name := fi.Name;

      if ControlNameMatches (Control.Name, SqlRecordClass.SQLTableName.AsString, Name)
         then exit (Name);

      // e.g. map Person to PersonID field - a view may have added a Person field from join but table will only have PersonID
      if fi.IsForeignKey and ControlNameMatches (Control.Name + 'ID', SqlRecordClass.SQLTableName.AsString, Name)
         then exit (Name);
    end;

  result := '';
end;

function TSqlRecordModel.FieldInfoOfControl (Control: TFmxObject): TFieldInfo;
var
  Model: TSqlRecordModel;
  Name: string;
begin
  result := nil;

  if (Control.Name = '') or (Control is TCustomButton) or (Control is TLayout) or (Control is TTabItem) then exit;

  if dicControlToModel.TryGetValue (Control, Model)
     then exit (SqlRecordClass.FieldInfoList.ItemsByName[Model.TargetFieldName.AsString]);

  for var fi in SqlRecordClass.FieldInfoList.All do
    begin
      Name := fi.Name;
      if ControlNameMatches (Control.Name, SqlRecordClass.SQLTableName.AsString, Name)
         then exit (fi);

      if fi.IsForeignKey and Name.EndsWith ('ID') then
        begin
         Name := copy (Name, 1, Name.Length - 2);
         if ControlNameMatches (Control.Name, SqlRecordClass.SQLTableName.AsString, Name)
            then exit (fi);
        end;
    end;
end;


//procedure TSqlRecordModel.UpdateMandatoryUI(rec: TSqlRecord; Parent: TFmxObject; ShowMandatory: boolean);
//begin
//
//end;

function TSqlRecordModel.GetUIContainerInfo (Obj: TObject): TUIContainerInfo;
var
  Control: TFmxObject;
begin
  if not (Obj is TFmxObject) then exit (nil);

  Control := TFmxObject (Obj);
  if not UIContainerInfos.TryGetValue(Control, result)
     then result := GetUIContainerInfo (Control.Parent);
end;

function TSqlRecordModel.CollectUIValue (rec: TSqlRecord; Control: TControl; Silent: boolean): boolean;
var
  UIInfo: TUIContainerInfo;
  Model: TSqlRecordModel;
  FieldInfo: TFieldInfo;
  FieldName: string;
begin
  result := true;

  if dicControlToModel.TryGetValue (Control, Model)
     then if rec.HasFieldByName (Model.TargetFieldName.AsString)
          then rec.SetFieldVariant (Model.TargetFieldName.AsString, Model.SelectedID)
          else raise Exception.Create('Record does not have field ' + Model.TargetFieldName.AsString)
     else
        begin
          UIInfo := GetUIContainerInfo (Control);
          FieldInfo := UIInfo.ControlFieldInfos[Control];
          if not ClassHasAttribute (FieldInfo.ClassType, AutoUIAttribute) then exit (true);
          FieldName := FieldInfo.Name;
          if not rec.crudClass.HasFieldByName (FieldName) then exit; //ignore view fields
          if Control is TTimeEdit then
            begin
               var t: TTime := TTimeEdit(Control).GetSmartTime;
               SetPropValue(rec, FieldName, t);
            end
          else if Control is TDateEdit then
             begin
               var dt: TDate := TDateEdit(Control).GetSmartDate;
               SetPropValue(rec, FieldName, dt);
             end
          else if Control is TComboBox
             then if FieldInfo.IsEnumeratedType or not FieldInfo.IsTextual
                        then rec.SetFieldVariant (FieldName, TComboBox(Control).ItemIndex)
                        else rec.SetFieldVariant (FieldName, TComboBox(Control).Text)
          else if Control is TCustomEdit
             then begin
                    result := rec.TrySetFieldStringValue (FieldName, TCustomEdit(Control).Text);
                    if not result and not Silent
                       then ShowErrorAndAbort('Vrednost: ' + TCustomEdit(Control).Text + #13#10' ne može biti upisana u polje ' + FieldName);
                  end
          else if Control is TCustomMemo then
            begin
               result := rec.TrySetFieldStringValue (FieldName, TCustomMemo(Control).Text);
               if not result and not Silent
                  then ShowErrorAndAbort('Vrednost: ' + TCustomMemo(Control).Text + #13#10' ne može biti upisana u polje ' + FieldName);
            end
          else if Control is TCheckBox
             then rec.SetFieldVariant (FieldName, TCheckBox(Control).IsChecked);
        end;
end;

function TSqlRecordModel.UpdateMandatoryStatus: integer;
var
  Control: TControl;
begin
  result := 0;
  for var pair in UIContainerInfos do
    for Control in pair.Value.ControlFieldInfos.Keys do
        result := result + UpdateMandatoryStatus (pair.Value, Control);

  CheckVisibleMandatoryStatus;
end;

function TSqlRecordModel.UpdateMandatoryStatus (const UIContainerInfo: TUIContainerInfo; Control: TControl): integer;
var
  FieldInfo: TFieldInfo;
  Lab: TLabel;
  Invalid: boolean;
begin
  result := 0;
  if not crudRecord.IsAssigned then exit;
  FieldInfo := UIContainerInfo.ControlFieldInfos[Control];

  Invalid := not CollectUIValue (crudRecord, Control, true);
  Invalid := Invalid or FieldInfo.IsUINotNull and FieldInfo.UINotNullInfo.Active(UIStates)
             and not ValidateIsNotNull(crudRecord, FieldInfo.Name);
  Invalid := Invalid or not ValidateWithAttributes(crudRecord, FieldInfo.Name);
  if Invalid
     then inc (result);

  if UIContainerInfo.FieldInfoLabels.TryGetValue (FieldInfo, Lab)
    then
      begin
        Lab := UIContainerInfo.FieldInfoLabels[FieldInfo];
        Lab.SetWarningState (Invalid and UIContainerInfo.ShowMandatory);
      end
    else if Invalid and not lstWarnedAboutInvalid.Contains (Control)
         then begin
                lstWarnedAboutInvalid.Add(Control);
                ShowErrorDialogue (FieldInfo.Name + ' nije validno ali nema labelu, molim prijavite grešku.');
              end;
end;

function TSqlRecordModel.CheckVisibleMandatoryStatus: integer;
var
  Control: TControl;
begin
  result := 0;
  for var pair in UIContainerInfos do
    for Control in pair.Value.ControlFieldInfos.Keys do
       if Control.Visible
          then result := result + CheckVisibleMandatoryStatus (pair.Value, Control);
  apsVisibleControlsValid.SetActiveIf(result = 0);
end;

function TSqlRecordModel.CheckVisibleMandatoryStatus (const UIContainerInfo: TUIContainerInfo; Control: TControl): integer;
var
  FieldInfo: TFieldInfo;
  Lab: TLabel;
  Invalid: boolean;
begin
  result := 0;
  if not crudRecord.IsAssigned then exit;
  FieldInfo := UIContainerInfo.ControlFieldInfos[Control];
  if UIContainerInfo.FieldInfoLabels.TryGetValue (FieldInfo, Lab) and crudRecord.crudClass.HasFieldByName (FieldInfo.Name) then
    begin
      Invalid := FieldInfo.IsUINotNull and FieldInfo.UINotNullInfo.Active(UIStates) and not ValidateIsNotNull(crudRecord, FieldInfo.Name);
      Invalid := Invalid or not ValidateWithAttributes(crudRecord, FieldInfo.Name);
      if Invalid then inc (result);
    end;
end;

procedure TSqlRecordModel.UpdateMandatoryStatus (Control: TControl);
var
  UIContainerInfo: TUIContainerInfo;
begin
  if not crudRecord.IsAssigned then exit;
  UIContainerInfo := GetUIContainerInfo (Control);
  UpdateMandatoryStatus(UIContainerInfo, Control);
end;

procedure TSqlRecordModel.ControlNotifyEvent (Sender: TObject);
var
  NotifyEvent: TNotifyEvent;
  UIInfo: TUIContainerInfo;
  Control : TControl;
begin
  UIInfo := GetUIContainerInfo (Sender);
  if UIInfo.Updating then exit;

  Control := TControl (Sender);
  if UIInfo.OriginalEvents.TryGetValue (Control, NotifyEvent)
     then NotifyEvent (Sender);

  //UpdateMandatoryStatus (Control);
  PostponeOnce (CheckAllDataEntered);
end;

function TSqlRecordModel.CopyForTutorial: string;
var
  Builder: TStringBuilder;
  line: string;
  Model: TSqlRecordModel;

  procedure DoAppend;
  begin
    Builder.Append('  ');
    Builder.Append(line);
    Builder.Append(#13#10);
  end;

begin
  Builder := TStringBuilder.Create;

  for var pair in UIContainerInfos do
    for var control in pair.Value.TabOrder do
      begin
        line := '';
        if control is TCustomComboBox
           then
             begin
               line := format ('!DropDown %s', [control.Name]);
               DoAppend;
               line := format ('!Select %s %d', [control.Name, TCustomComboBox(control).ItemIndex]);
             end
        else if control is TCustomComboEdit
           then
             begin
               dicControlToModel.TryGetValue(control, Model);

               var s := TCustomComboEdit(control).Text;

               if Model is TGridLookupEdit then
                 begin
                   var index := s.IndexOf(BRIEF_FIELD_DELIMITER);
                   if index <> -1 then s := copy (s, 1, index);
                 end;

               line := format ('!SlowType %s "%s"', [control.Name, s]);
               //DoAppend;
               //line := format ('!Select %s %d', [control.Name, TCustomComboEdit(control).ItemIndex]);

               if Model is TGridLookupEdit then
                 begin
                   DoAppend;
                   line := format ('!Select %s 0', [control.Name]);
                 end;
             end
        else if control is TCustomEdit
           then line := format ('!SlowType %s "%s"', [control.Name, TCustomEdit(control).Text])
        else if control is TDateEdit
           then line := format ('!SetDate %s "%d"', [control.Name, trunc (TDateEdit(control).Date)])
        else if control is TTimeEdit
           then line := format ('!SetTime %s "%d"', [control.Name, round (frac(TTimeEdit(control).Time) / DelphiSecond)])
        else if control is TCheckBox
           then line := format ('!Check %s %s', [control.Name,
                                               StrUtils.IfThen (TCheckBox(control).IsChecked, 'true', 'false')]);
        if line <> '' then DoAppend;
      end;
  result := Builder.ToString;
  Builder.Free;
end;

procedure TSqlRecordModel.CheckAllDataEntered;
begin
  apsAllDataEntered.SetActiveIf (UpdateMandatoryStatus = 0);
end;

procedure TSqlRecordModel.DoAddUIContainer (Parent: TFmxObject; UIInfo: TUIContainerInfo);
var
  Obj: TFmxObject;
  FieldInfo: TFieldInfo;
  Control: TControl;
  NotifyEvent: TNotifyEvent;
  TabOrder, TabOrder2: integer;

  procedure ReplaceEvent;
  begin
      if Control is TCustomEdit then
        begin
          NotifyEvent := (Control as TCustomEdit).OnChangeTracking;
          (Control as TCustomEdit).OnChangeTracking := ControlNotifyEvent;
        end
      else if Control is TCustomMemo then
        begin
          NotifyEvent := (Control as TCustomMemo).OnChangeTracking;
          (Control as TCustomMemo).OnChangeTracking := ControlNotifyEvent;
        end
      else if Control is TComboBox then
        begin
          var cbx := Control as TComboBox;
          NotifyEvent := cbx.OnChange;
          cbx.OnChange := ControlNotifyEvent;
        end
      else if Control is TDateEdit then
        begin
          NotifyEvent := (Control as TDateEdit).OnChange;
          (Control as TDateEdit).OnChange := ControlNotifyEvent;
        end;

    if Assigned(NotifyEvent)
       then UIInfo.OriginalEvents.Add (Control, NotifyEvent);
  end;

begin

  if ClassHasAttribute (Parent.ClassType, SkipAutomationAttribute)
     or (Parent.Children = nil) then exit;

  for Obj in Parent.Children do
    begin
      FieldInfo := FieldInfoOfControl(obj);
      if FieldInfo <> nil then
        begin
          if obj is TLabel
             then UIInfo.FieldInfoLabels.TryAdd(FieldInfo, obj as TLabel)
             else if obj is TControl then
                  begin
                    Control := obj as TControl;

                    TabOrder := RelevantParentTabOrder (Control) * 1000 + Control.TabOrder ;

                    var index := 0;
                    // find where to insert
                    while (index < UIInfo.TabOrder.Count) do
                      begin
                        TabOrder2 := RelevantParentTabOrder (UIInfo.TabOrder[index]) * 1000 + UIInfo.TabOrder[index].TabOrder;
                        if TabOrder2 > TabOrder
                           then break;
                        inc (index);
                      end;

                    UIInfo.TabOrder.Insert (index, Control);

                    var Skip := UIInfo.FieldInfoControls.ContainsKey(FieldInfo);
                    if Skip and not (obj is TTimeEdit) // allow a time edit to exist because a date edit may already be assigned to this field
                      then ShowErrorAndAbort(FieldInfo.Name + ' already added to UIInfo.FieldInfoControls?');
                    if not Skip then UIInfo.FieldInfoControls.Add(FieldInfo, Control);

                    if UIInfo.ControlFieldInfos.ContainsKey(Control)
                      then ShowErrorAndAbort(Control.Name + ' already added to UIInfo.ControlFieldInfos?');
                    UIInfo.ControlFieldInfos.Add(Control, FieldInfo);
                    if UIInfo.ShowMandatory {and FieldInfo.NotNull }then
                      begin
                        ReplaceEvent;
                      end;
                  end;
        end;
      DoAddUIContainer (Obj, UIInfo);
    end;
end;

procedure TSqlRecordModel.DoFetch;
begin
  inherited;

  FPreviousMasterID := MasterID;
end;

procedure TSqlRecordModel.AddUIContainer(Parent: TFmxObject; ShowMandatory: boolean);
var
  UIInfo: TUIContainerInfo;
begin
  UIInfo := TUIContainerInfo.Create;
  UIInfo.ShowMandatory := ShowMandatory;
  DoAddUIContainer (Parent, UIInfo);
  UIContainerInfos.Add (Parent, UIInfo);
end;

procedure TSqlRecordModel.PopulateUI(rec: TSqlRecord; Parent: TFmxObject; ShowMandatory: boolean);
var
  UIInfo: TUIContainerInfo;
begin
  if not UIContainerInfos.TryGetValue (Parent, UIInfo)
    then AddUIContainer (Parent, ShowMandatory);
  UIInfo := UIContainerInfos[Parent];
  UIInfo.BeginUpdate;
  DoPopulateUI(rec, Parent, ShowMandatory);
  UIInfo.EndUpdate;
end;

procedure TSqlRecordModel.DoPopulateUI(rec: TSqlRecord; Parent: TFmxObject; ShowMandatory: boolean);
var
  Obj: TFmxObject;
  Model: TSqlRecordModel;
  FieldName: string;
  FieldInfo: TFieldInfo;

  procedure ClearControl;
  begin
    if Obj is TDateEdit
      then TDateEdit(Obj).IsEmpty := true
    else if Obj is TLabel then
      begin
        TLabel(Obj).Text := FController.Model.GetFieldDisplayName (SqlRecordClass, FieldName);
//        TLabel(Obj).Position.X := 2;
      end
    else if Obj is TTimeEdit
      then TTimeEdit(Obj).IsEmpty := true
    else if Obj is TComboBox
      then TComboBox(Obj).ItemIndex := -1
    else if Obj is TCustomEdit
       then TCustomEdit(Obj).Text := ''
    else if Obj is TCustomMemo
       then TCustomMemo(Obj).Text := ''
    else if Obj is TCheckBox
       then TCheckBox(Obj).IsChecked := false;
  end;

begin
  if ClassHasAttribute (Parent.ClassType, SkipAutomationAttribute) then exit;

  for Obj in Parent.Children do
    begin
      // ELEM! Ako je labela labPerson za neki spoljni kljuc tipa PersonID, da bi prevod nasao kako se to polje zove mora da postoji
      // polje u view "Person" - ako ne postoji onda stavi ime labeli labPersonID pa postavi prevod za to polje

//      if Obj.Name = 'labBasisForExemption' then
//          if now = 0 then exit;

      FieldName := FieldOfControl (obj);
      FieldInfo := SqlRecordClass.FieldInfoList.ItemsByName[FieldName];
      if (FieldName = '') and
         ( (Obj is TCustomEdit) or (Obj is TComboBox) )
         then if TFmxObject(Obj).TagString <> 'ignore'
              then ShowErrorDialogue (TComponent(Obj).Name + ' is for unknown field');

      if FieldName <> ''
        then if dicControlToModel.TryGetValue (Obj, Model)
          then
            begin
              Model.ClearHistory;
//              Model.SelectedID := NULL_ID; // to force change events hmmmmmmmmmm
              if rec.IsAssigned then
                begin
                  Model.SelectedID := rec.GetFieldVariant(FieldName);
                  // doesnt matter if its already fetching anyway, could be smarter code...
                  if (Model is TLookupEditModel) and TLookupEditModel(Model).FetchIfEmpty
                    then TLookupEditModel(Model).PostponeFetch;
                end
                else Model.SelectedID := NULL_ID;
            end
          else
            if rec = nil
            then ClearControl
            else
              begin
        //        if Control is Tdate
        //           then crudRecord.SetFieldVariant (FieldName, TCustomEdit(Control).Text);
//                PropInfo := rec.FieldByName (FieldName);
                if FieldName = 'RevisionType' then
                   if now = 0 then exit;

                if Obj is TLabel
                   then begin
                          var lab := rec.FieldDisplayName (FieldName);
                          // ako labela pripada editu koji pripada modelu, onda dodaj "ID" na field name za potrebe NotNull provere
                          if dicControlToModel.TryGetValue (Obj.Parent, Model)
                            then FieldName := FieldName + 'ID';
                          if ShowMandatory then
                            begin
                              //var NotNullField := rec.IsMandatoryField (FieldName);
                              //if NotNullField then lab := lab + '*';
                            end;
                          TLabel(Obj).Text := lab;
//                          TLabel(Obj).Position.X := 2;
                        end;

                if FieldInfo.IsAssigned and not ClassHasAttribute (FieldInfo.ClassType, AutoUIAttribute) then continue;

                if Obj is TDateEdit
                   then begin
                          var dt: TDate := rec.GetFieldVariantDisplayValue (FieldName);
                          TDateEdit(Obj).SetSmartDate (dt);
                        end
                else if Obj is TTimeEdit
                   then begin
                          var t: TTime := rec.GetFieldVariantDisplayValue (FieldName);
                          TTimeEdit(Obj).SetSmartTime (t);
                        end
                else if Obj is TComboBox
                     then if FieldInfo.IsEnumeratedType or not FieldInfo.IsTextual
                          then
                            begin
                              var cbx := TComboBox(Obj);

                              if FieldInfo.IsEnumeratedType then
                                begin
                                  var fie := TFieldInfoEnum (rec.FieldInfoList.ItemsByName[FieldName]);
                                  if cbx.Items.Count = 0
                                     then PrepareEnumComboBox (fie, cbx);
                                end;

                              cbx.ItemIndex := rec.GetFieldVariant(FieldName);
                              if Assigned (cbx.OnChange)
                                 then cbx.OnChange(cbx);
                            end
                          else TComboBox(Obj).Text := rec.GetFieldVariant(FieldName)
                else if Obj is TCustomEdit
                   then begin
                          if FieldInfo <> nil
                             then TCustomEdit(Obj).Text := FieldInfo.GetDisplayString (rec)
                             else TCustomEdit(Obj).Text := rec.GetFieldVariantDisplayValue (FieldName);
                        end
                else if Obj is TCustomMemo
                   then TCustomMemo(Obj).Text := rec.GetFieldVariantDisplayValue (FieldName);

                if Obj is TCheckBox
                   then begin
                          TCheckBox(Obj).IsChecked := rec.GetFieldVariantDisplayValue (FieldName);
                          TCheckBox(Obj).Text := FController.Model.GetFieldDisplayName (SqlRecordClass, FieldName);
                        end;
              end
        {$IFDEF DEBUG}
        else
          if (Obj is TCustomEdit) and TCustomEdit(Obj).TextPrompt.IsEmpty
             then TCustomEdit(Obj).TextPrompt := '?';
        {$ELSE}
        ;
        {$ENDIF}

      if (Obj is TControl) and (Obj.Children <> nil) then
        DoPopulateUI (rec, Obj, ShowMandatory);
    end;
end;

procedure TSqlRecordModel.SetApsOwerCU(const Value: TAppState);
begin
  FApsOwnerCU := Value;
  apsCU.Child := FApsOwnerCU;
end;

procedure TSqlRecordModel.DetachButton (Button: TCustomButton);
begin
  var index := FButtons.IndexOf(Button);
  if index = -1 then exit;

  FActionToButton.Remove(Button.Action);

  // if buttons had original actions, restore their OnExecute
  if (Button.Action is TInternalAction)
     then begin
            Button.Action.Free;
            Button.OnClick := nil;
          end
     else begin
            UnregisterStateObserver(Button.Action);
            if FOriginalEvents.ContainsKey (FModelActions[index])
               then Button.Action.OnExecute := FOriginalEvents [FModelActions[index]];
          end;

  FModelActionsToActions.Remove (FModelActions[index]);
  FOriginalEvents.Remove (FModelActions[index]);

  FButtons.Delete(index);
  FModelActions.Delete(index);
  uiActions.Delete(index);
end;

procedure TSqlRecordModel.DetachButtons(Container: TControl);
begin
  if Container = nil then exit;

  for var ctrl in Container.Controls do
    if ctrl is TCustomButton then
      for var Action := Low (TModelAction) to High (TModelAction) do
        if string (ctrl.Name).ToLower.EndsWith (System.TypInfo.GetEnumName(System.TypeInfo(TModelAction), integer(Action)).ToLower)
          then
            begin
              DetachButton(ctrl as TCustomButton);
              break;
            end;
end;

procedure TSqlRecordModel.DetachButtons (Buttons: TArray<TCustomButton>);
begin
  for var Button in Buttons do DetachButton (Button);
end;

//procedure TSqlRecordModel.AssignButtonActionEvents;
//begin
//  var i := 0;
//
//  for var Action in uiActions do
//    begin
//      var ma := FModelActions [i];
//      Action.Tag := integer(ma);
//
//      FActions[ma] := Action;
//      if (TMethod(FOriginalEvents [ma]) = TMethod(Action.OnExecute)) and (TMethod(FOriginalEvents [ma]).Code <> nil)
//         then raise Exception.Create('Buttons assigned twice?')
//         else FOriginalEvents [ma] := Action.OnExecute;
//      Action.OnExecute := OnModelAction;
//      inc (i);
//    end;
//end;

procedure TSqlRecordModel.AttachButton (Button: TCustomButton; ModelAction: TModelAction);
begin
  if FModelActions.Contains (ModelAction)
     then raise Exception.Create('TSqlRecordModel.AttachButton: Duplicate buttons/action for ' +
          System.TypInfo.GetEnumName(System.TypeInfo(TModelAction), integer(ModelAction)));

  FButtons.Add (Button);
  FModelActions.Add (ModelAction);
  if Button.Images <> nil
    then Actionlist.Images := Button.Images; // always update in case image list changed in meantime

  if not (ModelAction in FAllowedModelActions)
    then begin
           uiActions.Add (nil);
           Button.Enabled := false;
           exit;
         end;

  if Button.Action = nil
    then begin
           var a := TInternalAction.Create (ActionList);
           a.ImageIndex := Button.ImageIndex;
           a.OnExecute := Button.OnClick;
           a.Text := Button.Text;
           if Button.Hint = '' then
              begin
                a.Hint := DefaultModelActionHints[ModelAction];
                Button.ShowHint := true;
              end
              else a.Hint := Button.Hint;

//               if Button.Hint <> ''
//                  then a.Hint := Button.Hint
//                  else begin
//                         if (Button.ImageIndex >= 0) and (Button.ImageIndex < length(ImageIndexToHint))
//                            then begin
//                                   a.Hint := ImageIndexToHint[Button.ImageIndex];
//                                   Button.Hint := a.Hint;
//                                 end;
//                       end;

           Button.ShowHint := a.Hint <> '';
           a.Name := 'actfor_' + Button.Owner.Name + '_' + Button.Name ;
           //Button.ImageIndex := -1;
           Button.Action := a;
           Button.Images := TCustomImageList (Actionlist.Images);
         end
    else if not (Button.Action is TAction)
            then raise Exception.Create('Assigned action is not TAction');
  uiActions.Add (TAction(Button.Action));
  if Button.Action <> nil
    then Button.Action.Tag := integer(ModelAction);
  FModelActionsToActions.Add(ModelAction, TAction (Button.Action));
  FActionToButton.Add(Button.Action, Button);

  if Button.Action is TInternalAction
    then if Assigned(Button.OnClick)
         then FOriginalEvents.Add (ModelAction, Button.OnClick)
         else
    else if Assigned (Button.Action.OnExecute)
         then FOriginalEvents.Add (ModelAction, Button.Action.OnExecute)
         else;

  Button.Action.OnExecute := OnModelAction;

  case ModelAction of
      TModelAction.Cancel: RegisterStateObserver (Button.Action, TAppStateAction.Enabled,
                                                  [apsCU], [Controller.apsTaskRunning]);
      TModelAction.Post:   RegisterStateObserver (Button.Action, TAppStateAction.Enabled,
                                                  [apsCU{, apsAllDataEntered}], [Controller.apsTaskRunning]);

      TModelAction.Create: RegisterStateObserver (Button.Action, TAppStateAction.Enabled,
                                                  StatesRequiredForCreate, StatesForbiddenForCreate);

      TModelAction.Read:   RegisterStateObserver (Button.Action, TAppStateAction.Enabled,
                                                  [], [apsCU, apsOwnerCU, apsMasterCU]);

      TModelAction.Update: RegisterStateObserver (Button.Action,  TAppStateAction.Enabled,
                                                  StatesRequiredForUpdate, StatesForbiddenForUpdate);

      TModelAction.Show:   RegisterStateObserver (Button.Action,  TAppStateAction.Enabled,
                                                  StatesRequiredForUpdate, StatesForbiddenForUpdate);

      TModelAction.Delete: RegisterStateObserver (Button.Action, TAppStateAction.Enabled,
                                                  StatesRequiredForDelete, StatesForbiddenForDelete);

      TModelAction.First, TModelAction.Previous, TModelAction.Next, TModelAction.Last:
                                                 RegisterStateObserver (Button.Action, TAppStateAction.Enabled,
                                                 StatesRequiredForCreate, StatesForbiddenForCreate);
    end //case
end;

procedure TSqlRecordModel.AttachButtons(Buttons: TArray<TCustomButton>; ModelActions: TArray<TModelAction>);
begin
  for var i := Low (Buttons) to High (Buttons) do
    AttachButton (Buttons[i], ModelActions[i]);
end;

procedure TSqlRecordModel.AttachButtons (Container: TControl);
var
  fedt: TCustomEdit;
  fbtnOn, fbtnOff: TCustomButton;
  ctrl: TControl;

  function FindChildControl (Parent: TControl; AClass: TClass; const EndsWith: string): TControl;
  begin
    result := nil;
    for var ctrl in Parent.Controls do
      begin
        if ( string (ctrl.Name).ToLower.EndsWith (EndsWith)
             or
             string (ctrl.Name).ToLower.EndsWith (EndsWith + SqlRecordClass.SQLTableName.AsString.ToLower)
             or
             string (ctrl.Name).ToLower.EndsWith (EndsWith + SqlRecordClass.CrudClass.SQLTableName.AsString.ToLower)
           )
           then
             if ctrl.InheritsFrom(AClass)
                then exit (ctrl);
        result := FindChildControl (ctrl, AClass, EndsWith);
        if result <> nil then break;
      end;
  end;

begin
  if Container = nil then exit;
  //fedt := nil;
  fbtnOn := nil;
  fbtnOff := nil;

  for var Action := Low (TModelAction) to High (TModelAction) do
    begin
      ctrl := FindChildControl (Container, TCustomButton, System.TypInfo.GetEnumName(System.TypeInfo(TModelAction), integer(Action)).ToLower);
      if ctrl <> nil
         then AttachButton(ctrl as TCustomButton, Action);
    end;

  if not FModelActions.Contains (TModelAction.Post) then
    begin
      ctrl := FindChildControl (Container, TCustomButton, 'save');
      if ctrl <> nil
         then AttachButton(ctrl as TCustomButton, TModelAction.Post);
    end;

  {$IFDEF DEBUG}
  if (TModelAction.Create in FAllowedModelActions)
     and not FModelActionsToActions.ContainsKey(TModelAction.Create) then
     ShowErrorDialogue('No create action found in ' + Container.Name);

  if (TModelAction.Update in FAllowedModelActions)
     and not FModelActionsToActions.ContainsKey(TModelAction.Update) then
     ShowErrorDialogue('No Update action found in ' + Container.Name);

  if (TModelAction.Delete in FAllowedModelActions)
     and not FModelActionsToActions.ContainsKey(TModelAction.Delete) then
     ShowErrorDialogue('No Delete action found in ' + Container.Name);
  {$ENDIF}


//  for var ctrl in Container.Controls do
//    begin
//      if ctrl is TCustomButton then
//        for var Action := Low (TModelAction) to High (TModelAction) do
//          begin
//            if string (ctrl.Name).ToLower.EndsWith (System.TypInfo.GetEnumName(System.TypeInfo(TModelAction), integer(Action)).ToLower)
//              then
//                begin
//                  AttachButton(ctrl as TCustomButton, Action);
//                  break;
//                end
//              else
//                if (string (ctrl.Name).EndsWith ('FilterOn'))
//                  then fbtnOn := TCustomButton(ctrl)
//                else if (string (ctrl.Name).EndsWith ('FilterOff'))
//                  then fbtnOff := TCustomButton(ctrl)
//                else if (string (ctrl.Name).EndsWith ('Print'))
//                  then if not ClassHasAttribute (SqlRecordClass, PrintableAttribute)
//                       then ctrl.Visible := false;
//          end
//      else if ctrl is TCustomEdit and (string (ctrl.Name).EndsWith ('Filter'))
//        then fedt := TCustomEdit(ctrl)
//      else if ctrl is TLayout
//        then AttachButtons (ctrl);
//    end;

  fedt := TCustomEdit (FindChildControl(Container, TCustomEdit, 'filter'));
  if fedt <> nil then SetFilterUI(fedt, fbtnOn, fbtnOff);
end;

{ TMormotAppState }

constructor TMormotAppState.Create (T: TSQLRecordClass; const Context: string;
                                    const ATiming: TAppStateTiming = TAppStateTiming.Immediate; AChildState: TAppState = nil);
begin
  inherited CreateAsSingle (T.SQLTableName.AsString + '_' + Context, ATiming, AChildState);
end;

{ TActivityElement }

function TActivityElement.GetBusy: boolean;
begin
  result := FBusy > 0;
end;

procedure TActivityElement.BeginActivity;
begin
  inc (FBusy);

  if FIndicator.IsAssigned and (FBusy=1) then
    begin
      FIndicator.Enabled := true;
      FIndicator.Visible := true;
//          FIndicator.BringToFront;
      FOnBysy (self);
    end
end;

constructor TActivityElement.Create(AOnBysy: TNotifyEvent);
begin
  inherited Create;
  FOnBysy := AOnBysy;
end;

destructor TActivityElement.Destroy;
begin
  FreeAndNil (FIndicator);
//  Indicator := nil;
  inherited;
end;

procedure TActivityElement.EndActivity;
begin
  dec (FBusy);

  if FIndicator.IsAssigned and not Busy then
    begin
      FIndicator.Enabled := false;
      FIndicator.Visible := false;
      FOnBysy (self);
    end;
end;

procedure TActivityElement.FreeNotification(AObject: TObject);
begin
  if AObject = Indicator then
     Indicator := nil;
end;

procedure TActivityElement.SetIndicator(const Value: TAniIndicator);
begin
  if FIndicator.IsAssigned
     then FIndicator.RemoveFreeNotify(self);
  FIndicator := Value;
  if FIndicator.IsAssigned
     then begin
            FIndicator.Enabled := Busy;
            FIndicator.Visible := Busy;
            FIndicator.AddFreeNotify(self);
          end;
end;


initialization
  for var ma : TModelAction := TModelAction.First to TModelAction.Cancel do
    DefaultModelActionHints [ma] := (System.TypInfo.GetEnumName(System.TypeInfo(TModelAction), integer(ma)));
//  dicControlModels := TDictionary <TFmxObject, TSqlRecordModel>.Create;
  ModelForms := TModelForms.Create;
  lstWarnedAboutInvalid := TList.Create;

finalization
//  dicControlModels.Free;
  ModelForms.Free;
  lstWarnedAboutInvalid.Free;
end.
