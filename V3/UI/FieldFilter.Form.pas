﻿unit FieldFilter.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit,
  FMX.Objects, StrUtils,
  Syncommons, MormotTypes, MormotMods, FMX.Layouts, FMX.ListBox, FMX.Common.Utils, FMX.GridLookupEdit,
  Common.Utils, ObservableController, MormotAttributes, FMX.DateTimeCtrls, Transliteration;

type
  TfrmFieldFilter = class(TRestrictedSizeForm)
    edtValue: TEdit;
    labValue: TLabel;
    labFilterField: TLabel;
    lsbValues: TListBox;
    spbClear: TSpeedButton;
    chbValue: TCheckBox;
    dae1: TDateEdit;
    dae2: TDateEdit;
    cbxEditValueOperator: TComboBox;
    StatusBar: TStatusBar;
    labExplanation: TLabel;
    spbCaseSensitivity: TSpeedButton;
    cbxDae1: TComboBox;
    cbxDae2: TComboBox;
    cbxList: TComboBox;
    labTL: TLabel;
    labOperator: TLabel;
    gplButtons: TGridPanelLayout;
    butCancel: TButton;
    btnOK: TButton;
    btnReset: TButton;
    procedure btnOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spbClearClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure cbxEditValueOperatorChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtValueChangeTracking(Sender: TObject);
    procedure butCancelClick(Sender: TObject);
  private
    procedure PrepareEnum;
    procedure PrepareBoolean;
    procedure PrepareText;
    function GetFieldName: string;
    procedure PrepareDate;
    procedure PrepareNumber;
    procedure UpdateExplanation;
    function GetOperator(cbx: TComboBox): TFilterOperator;
    procedure SetOperator(cbx: TComboBox; op: TFilterOperator);
    function GetOperatorAsExplanation(cbx: TComboBox): string;
    function GetFlags: cardinal;
    type
      TFilterType = (ftText, ftBoolean, ftDate, ftLookup, ftEnum, ftNumber);
    procedure PrepareLookup;
    procedure SetFilterType(const Value: TFilterType);
    procedure SetupOperatorCombo (cbx: TComboBox; Operators: TFilterOperators);
  protected
    gle : TGridLookupEdit;
    FFilterType: TFilterType;
    AutoTranslit, AlreadyFiltered: boolean;
    FFieldInfo: TFieldInfo;
    FController: TObservableController;
    FFilter: TQueryFilter;
    FFieldDisplayName: string;
    function FShowFor(AController: TObservableController; const AFieldInfo: TFieldInfo; var Filter: TQueryFilter): TModalResult;
    property FilterType: TFilterType read FFilterType write SetFilterType;
    property FieldName: string read GetFieldName;
  public
//    class function ShowFor (const FieldName, FieldDisplayName: string; var Filter: TQueryFilter): TModalResult; overload;
    class function ShowFor(AController: TObservableController; const AFieldInfo: TFieldInfo; var Filter: TQueryFilter): TModalResult;
  end;

implementation

{$R *.fmx}

uses Mormot;

{ TfrnGridColumnFilter }

procedure TfrmFieldFilter.btnOKClick(Sender: TObject);
begin
  case FilterType of
    ftNumber: if not IsNumerical (StripThousandsSeparator (edtValue.Text))
      then ShowErrorAndAbort ('Vrednost mora biti numerička.');
  end;
  ModalResult := mrOK;
end;

procedure TfrmFieldFilter.butCancelClick(Sender: TObject);
begin

end;

//class function TfrmGridColumnFilter.ShowFor(const FieldInfo: TFieldInfo; var Filter: TQueryFilter): TModalResult;
//begin
//  result := ShowFor (StringToUTF8(FieldInfo.Name), StringToUTF8(FieldInfo.DisplayName), Filter);
//end;

procedure TfrmFieldFilter.cbxEditValueOperatorChange(Sender: TObject);
begin
  UpdateExplanation;
end;

procedure TfrmFieldFilter.edtValueChangeTracking(Sender: TObject);
begin
  UpdateExplanation;
end;

function TfrmFieldFilter.GetOperatorAsExplanation (cbx: TComboBox): string;
begin
  result := ' ' + FilterOperatorAsText [GetOperator(cbx)] + ' ';
  if spbCaseSensitivity.IsPressed and edtValue.Visible
     then result := ' (sa kapitalizacijom)' + result;
end;

procedure TfrmFieldFilter.UpdateExplanation;
var
  s: string;
begin
  if not Visible then exit;

  s := FFieldDisplayName;
  case FilterType of
    ftLookup, ftNumber: s := s + GetOperatorAsExplanation (cbxEditValueOperator) + edtValue.Text;
    ftText: s := s + GetOperatorAsExplanation (cbxEditValueOperator) + '"' + edtValue.Text + '"';
    ftBoolean: s := s + IfThen (chbValue.IsChecked, ' = da', ' = ne');
    ftDate: begin
              if dae1.IsEmpty and dae2.IsEmpty then
                begin
                  labExplanation.Text := 'Bez filtera';
                  exit;
                end;
              if not dae1.IsEmpty then
                begin
                  s := s + GetOperatorAsExplanation (cbxDae1) + dae1.Text;
                end;

              if not dae2.IsEmpty then
                begin
                  if not dae1.IsEmpty
                    then s := s + ' i ';
                  s := s + GetOperatorAsExplanation (cbxDae2) + dae2.Text;
                end;

            end;
    ftEnum: begin
              if cbxList.ItemIndex = 0
                 then s := s + ' je u ('
                 else s := s + ' nije u (';
              for var i := 0 to lsbValues.Count - 1 do
                if lsbValues.ListItems[i].IsChecked then s := s + lsbValues.ListItems[i].Text + ', ';
              s := s.TrimRight ([',', ' ']) + ')';
            end;
  end;
  s := 'Uslov: ' + s;
  labExplanation.Text := s;
end;

procedure TfrmFieldFilter.FormActivate(Sender: TObject);
begin
  case FilterType of
    ftLookup, ftText, ftNumber: edtValue.SetFocus;
    ftBoolean: chbValue.SetFocus;
    ftDate: dae1.SetFocus;
    ftEnum: lsbValues.SetFocus;
  end;

  UpdateExplanation;
end;

procedure TfrmFieldFilter.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := TCloseAction.caFree;
end;

procedure ChangeComboBoxFont(ComboBox: TComboBox);
var
  TextObject: TText;
begin
  // Access the ComboBox's style object to modify the font
  TextObject := ComboBox.FindStyleResource('text') as TText;
  if Assigned(TextObject) then
  begin
    TextObject.Font.Size := TextObject.Font.Size + 2;  // Set font size
//    TextObject.Font.Family := 'Arial';  // Set font family
//    TextObject.Font.Style := [TFontStyle.fsBold];  // Set font style (e.g., bold)
  end;
end;

procedure TfrmFieldFilter.FormCreate(Sender: TObject);
begin
  ChangeComboBoxFont(cbxEditValueOperator);
end;

procedure TfrmFieldFilter.SetupOperatorCombo (cbx: TComboBox; Operators: TFilterOperators);
begin
  cbx.Items.Clear;
  for var op in Operators do
      cbx.Items.Add (FilterOperatorAsSymbol[op].AsString);
end;

function TfrmFieldFilter.GetOperator (cbx: TComboBox): TFilterOperator;
var
  s: RawUTF8;
begin
  result := TFilterOperator.Less;
  for s in FilterOperatorAsSymbol do
    begin
      if cbx.Selected.Text = s.AsString then exit;
      inc (result);
    end;

  ShowErrorAndAbort('No operator selected');
end;

procedure TfrmFieldFilter.PrepareLookup;
begin
  FilterType := ftLookup;

  SetupOperatorCombo (cbxEditValueOperator, [TFilterOperator.Equal, TFilterOperator.NotEqual]);

  var table := FController.Model.Table[StringToUTF8 (FFieldInfo.ForeignTableName)];
  var CrudTable := FController.Model.Table[table.crudSQLTableName];
  gle := TGridLookupEdit.Create (CrudTable, edtValue, FController, nil);
  gle.FetchIfEmpty := not ClassHasAttribute(CrudTable, LargeTableAttribute);
  gle.GridAlignment := TAlignLayout.Right;

  if AlreadyFiltered
     then
       begin
         var index := FFilter.IndexOfField (FFieldInfo.NameForFilter);
         gle.SelectedID := FFilter.Values[index];
         SetOperator (cbxEditValueOperator, FFilter.Operators[index])
       end
     else
         SetOperator (cbxEditValueOperator, TFilterOperator.Equal);
end;

procedure TfrmFieldFilter.PrepareEnum;
begin
  FilterType := TFilterType.ftEnum;

  var efi := TFieldInfoEnum (FFieldInfo);
  var lst := dicEnumToDisplayValues[efi.TypeName];
  for var i := 0 to lst.Count - 1 do
       lsbValues.Items.Add(lst[i]^);

  if AlreadyFiltered then
    begin
      var v := FFilter.Fieldvalue(FFieldInfo.NameForFilter);
      for var j := 0 to TDocVariantData (v).Count-1 do
       begin
         lsbValues.ListItems[TDocVariantData (v)[j]].IsChecked := true;
       end;
    end;
end;

procedure TfrmFieldFilter.PrepareBoolean;
begin
  FilterType := TFilterType.ftBoolean;
  chbValue.Text := FFieldDisplayName;
  if AlreadyFiltered
    then chbValue.IsChecked := FFilter.Fieldvalue(FFieldInfo.NameForFilter);
end;

procedure TfrmFieldFilter.PrepareText;
begin
  SetupOperatorCombo (cbxEditValueOperator, [TFilterOperator.Less, TFilterOperator.LessOrEqual, TFilterOperator.Equal,
                                             TFilterOperator.Like, TFilterOperator.GreaterOrEqual, TFilterOperator.Greater,
                                             TFilterOperator.NotEqual]);

  FilterType := TFilterType.ftText;
  if AlreadyFiltered
    then begin
           var index := FFilter.IndexOfField (FFieldInfo.NameForFilter);
           edtValue.Text := FFilter.Values[index];
           SetOperator (cbxEditValueOperator, FFilter.Operators[index]);
           spbCaseSensitivity.IsPressed := FFilter.CheckFlag (index, qff_CaseSensitive);
         end
    else begin
           edtValue.Text := '';
           SetOperator (cbxEditValueOperator, TFilterOperator.Like);
         end;
end;

procedure TfrmFieldFilter.SetOperator(cbx: TComboBox; op: TFilterOperator);
begin
  cbx.ItemIndex := cbx.Items.IndexOf(FilterOperatorAsSymbol[op].AsString);
end;

procedure TfrmFieldFilter.PrepareDate;
var
  index: integer;
begin
  SetupOperatorCombo (cbxDae1, [TFilterOperator.Less, TFilterOperator.LessOrEqual, TFilterOperator.Equal,
                                             TFilterOperator.GreaterOrEqual, TFilterOperator.Greater,
                                             TFilterOperator.NotEqual]);
  SetupOperatorCombo (cbxDae2, [TFilterOperator.Less, TFilterOperator.LessOrEqual, TFilterOperator.Equal,
                                             TFilterOperator.GreaterOrEqual, TFilterOperator.Greater,
                                             TFilterOperator.NotEqual]);
  SetOperator (cbxDae1, TFilterOperator.GreaterOrEqual);
  SetOperator (cbxDae2, TFilterOperator.LessOrEqual);

  FilterType := TFilterType.ftDate;
  index := FFilter.IndexOfField (FFieldInfo.NameForFilter);
  dae1.IsEmpty := index = -1;
  if not dae1.IsEmpty
     then begin
            dae1.DateTime := FFilter.Values[index];
            SetOperator (cbxDae1, FFilter.Operators[index]);
            index := FFilter.IndexOfField (FFieldInfo.NameForFilter, index + 1); //get index of 2nd date
          end;

  dae2.IsEmpty := index = -1;
  if not dae2.IsEmpty
     then begin
            dae2.DateTime := FFilter.Values[index];
            SetOperator (cbxDae2, FFilter.Operators[index]);
          end;
end;

procedure TfrmFieldFilter.PrepareNumber;
begin
  FilterType := TFilterType.ftNumber;
  if AlreadyFiltered
    then begin
           var index := FFilter.IndexOfField (FFieldInfo.NameForFilter);
           edtValue.Text := FFilter.Values[index];
           SetOperator (cbxEditValueOperator, FFilter.Operators[index]);
         end;
end;

function TfrmFieldFilter.GetFieldName: string;
begin
  result := FFieldInfo.NameForFilter;
end;

procedure TfrmFieldFilter.SetFilterType(const Value: TFilterType);
begin
  FFilterType := Value;

  lsbValues.Visible := Value in [TFilterType.ftEnum];
  edtValue.Visible := Value in [TFilterType.ftText, TFilterType.ftLookup, TFilterType.ftNumber];
  chbValue.Visible := Value in [TFilterType.ftBoolean];
  dae1.Visible := Value in [TFilterType.ftDate];
  dae2.Visible := Value in [TFilterType.ftDate];

  case FilterType of
    ftDate: Height := round (180 + dae2.Height + dae2.Margins.Top + dae2.Margins.Bottom + StatusBar.Height);
    ftEnum: begin
              ActiveControl := lsbValues;
              Height := round (lsbValues.Height + lsbValues.Position.Y + gplButtons.Height + 90 + StatusBar.Height);
            end;
    ftLookup: Height := round (500 + StatusBar.Height);
    else Height := round (220 + StatusBar.Height);
    end;

  MinHeight := Height;
  MinWidth := Width;
  btnReset.Enabled := AlreadyFiltered;
end;

function TfrmFieldFilter.GetFlags: cardinal;
begin
  if spbCaseSensitivity.IsPressed
     then result := qff_CaseSensitive
     else result := 0;
end;

function TfrmFieldFilter.FShowFor(AController: TObservableController; const AFieldInfo: TFieldInfo;
                                                    var Filter: TQueryFilter): TModalResult;
begin
  FController := AController;

  spbCaseSensitivity.ImageIndex := FController.dicImageIndexes['charcase'];
  spbCaseSensitivity.Images := FController.ImageList;

  spbCaseSensitivity.Visible := false;

  FFieldInfo := AFieldInfo;
  FFilter := Filter;
  FFieldDisplayName := AController.Model.GetFieldDisplayName(FFieldInfo.SqlRecordClass, FFieldInfo.DisplayName);
  labFilterField.Text := labFilterField.Text + FFieldDisplayName;
  AlreadyFiltered := Filter.ContainsField (FFieldInfo.NameForFilter);
  AutoTranslit := AFieldInfo.HasTranslitCopy;
  labTL.Visible := AutoTranslit;
  gle := nil;

  if FFieldInfo.IsEnumeratedType
    then PrepareEnum
    else if FFieldInfo.IsBoolean
    then PrepareBoolean
    else if (FFieldInfo.MormotFieldInfo.SQLFieldType in [TSQLFieldType.sftDateTime])
    then PrepareDate
    else if (FFieldInfo.MormotFieldInfo.SQLFieldType = TSQLFieldType.sftCurrency)
    then PrepareNumber
    else if (FFieldInfo.ForeignTableName <> '') and not FFieldInfo.IsPlain
    then PrepareLookup
    else PrepareText;

  result := ShowModal;
  case result of
     mrOK:      begin
                  case FilterType of
                    ftText: begin
                              var value := edtValue.Text;
                              if AutoTranslit
                                 then value := TSimpleTranslit.ToLatin(value);
                              Filter.AddOrSet (FFieldInfo.NameForFilter, GetOperator (cbxEditValueOperator), value, GetFlags);
                            end;
//                         else Filter.AddOrSet ('lower(' + FieldName + ')', GetOperator (cbxEditValueOperator), );
                    ftNumber: Filter.AddOrSet (FFieldInfo.NameForFilter, GetOperator (cbxEditValueOperator), edtValue.Text);
                    ftEnum:
                      begin
                        var chi := lsbValues.CheckedIndexes;
                        var varr := VarArrayCreate([0, high (chi)], varInteger);
                        for var i := low(chi) to high (chi) do
                          varr[i] := chi[i];
                        if cbxList.ItemIndex = 0
                          then Filter.AddOrSet (FFieldInfo.NameForFilter, TFilterOperator.InValues, varr)
                          else Filter.AddOrSet (FFieldInfo.NameForFilter, TFilterOperator.NotInValues, varr);
                      end;
                    ftBoolean: Filter.AddOrSet (FieldName, TFilterOperator.Equal, chbValue.IsChecked);
                    ftDate: begin
                              Filter.RemoveField(FieldName);
                              if not dae1.IsEmpty
                                 then Filter.Add (FFieldInfo.NameForFilter, GetOperator(cbxDae1), dae1.DateTime);
                              if not dae2.IsEmpty
                                 then Filter.Add (FFieldInfo.NameForFilter, GetOperator(cbxDae2), dae2.DateTime);
                             end;
                    ftLookup:
                      begin
                        Filter.AddOrSet (FieldName, GetOperator (cbxEditValueOperator), gle.SelectedID);

                      end;
                  end;
                end;
     mrNoToAll: Filter.RemoveField (FieldName);
  end;

  gle.Free;
end;

class function TfrmFieldFilter.ShowFor (AController: TObservableController;
                                        const AFieldInfo: TFieldInfo; var Filter: TQueryFilter): TModalResult;
var
  instance: TfrmFieldFilter;
begin
  Application.CreateForm(TfrmFieldFilter, instance);
  result := instance.FShowFor (AController, AFieldInfo, Filter);
end;

procedure TfrmFieldFilter.spbClearClick(Sender: TObject);
begin
  if gle <> nil
    then gle.ClearSelection
    else edtValue.Text := '';
  edtValue.SetFocus;
end;

end.
