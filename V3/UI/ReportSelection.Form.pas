unit ReportSelection.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts, FMX.ListBox, FMX.Controls.Presentation,
  FMX.StdCtrls, ReportTemplate, ReportProducer,
  mORMot, SynCommons, Common.Utils, FMX.Common.Utils;

type
  TfrmReportSelection = class(TForm)
    lsbReports: TListBox;
    gplPostCancel: TGridPanelLayout;
    btnOK: TButton;
    btnCancel: TButton;
    procedure lsbReportsDblClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
  private
    PreviousReportTemplate: TSqlReportTemplate;
    FSqlRecord, FPrintSqlRecord: TSqlRecord;
    FReportProducer: TReportProducer;
    function ShowFor (ASqlRecord: TSqlRecord; AReportProducer: TReportProducer; AutoSelectIfOneOption: boolean = true): TModalResult;
    procedure Print;
    procedure DoPrint;
  public
    function SelectedReportTemplate: TSqlReportTemplate;
    class function Show (ASqlRecord: TSqlRecord; AReportProducer: TReportProducer; AutoSelectIfOneOption: boolean = true): TModalResult;
  end;

var
  frmReportSelection: TfrmReportSelection;

implementation

{$R *.fmx}

uses Tutorial.Form;

{ TfrmReportSelection }

class function TfrmReportSelection.Show(ASqlRecord: TSqlRecord; AReportProducer: TReportProducer;
               AutoSelectIfOneOption: boolean = true): TModalResult;
begin
  if frmReportSelection = nil
     then Application.CreateForm (TfrmReportSelection, frmReportSelection);

  result := frmReportSelection.ShowFor (ASqlRecord, AReportProducer);
end;

function TfrmReportSelection.ShowFor(ASqlRecord: TSqlRecord; AReportProducer: TReportProducer;
         AutoSelectIfOneOption: boolean = true): TModalResult;
begin
  FSqlRecord := ASqlRecord;
  FReportProducer := AReportProducer;
  AReportProducer.Populate(ASqlRecord, frmReportSelection.lsbReports.Items);
  var index := lsbReports.Items.IndexOfObject (PreviousReportTemplate);
  if index > -1
    then lsbReports.ItemIndex := index
    else if lsbReports.Items.Count > 0
       then lsbReports.ItemIndex := 0;

  if (lsbReports.Items.Count = 1) and AutoSelectIfOneOption
    then begin
           Print;
           result := mrOK;
         end
    else if TfrmTutorial.Active then
      begin
        inherited Show;
        result := mrOK;
      end
      else result := ShowModal;
end;

procedure TfrmReportSelection.lsbReportsDblClick(Sender: TObject);
begin
  if lsbReports.Selected.IsAssigned
     then btnOK.SimulateClick;
end;

function TfrmReportSelection.SelectedReportTemplate: TSqlReportTemplate;
begin
  if lsbReports.ItemIndex > -1
    then result := TSqlReportTemplate (lsbReports.Items.Objects[lsbReports.ItemIndex])
    else result := nil;
end;

procedure TfrmReportSelection.btnCancelClick(Sender: TObject);
begin
  if TfrmTutorial.Active then Close;
end;

procedure TfrmReportSelection.btnOKClick(Sender: TObject);
begin
  Print;
end;

procedure TfrmReportSelection.Print;
begin
  FPrintSqlRecord := FSqlRecord;
  FSqlRecord := nil;
  PreviousReportTemplate := SelectedReportTemplate;

  PostponeOnce (DoPrint);
  ModalResult := mrOK;
  Close;
end;

procedure TfrmReportSelection.DoPrint;
begin
  FReportProducer.Produce(SelectedReportTemplate, FPrintSqlRecord);
end;

procedure TfrmReportSelection.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil (FSqlRecord);
end;

end.
