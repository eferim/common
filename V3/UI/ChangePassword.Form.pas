﻿unit ChangePassword.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts,
  FMX.Edit, mormot, mormotmods, ObservableController, FMX.Common.Utils, CommonMethods;

type

  TfrmChangePassword = class(TBaseObserverForm, IControllerTaskObserver, IServerObserver)
    gplOkCancel: TGridPanelLayout;
    btnSave: TButton;
    btnCancel: TButton;
    edtOldPass: TEdit;
    labOldPass: TLabel;
    edtNewPass: TEdit;
    labNewPass: TLabel;
    edtConfPass: TEdit;
    labConfPass: TLabel;
    labDescription: TLabel;
    spbShowPassword: TSpeedButton;
    spbShowNewPassword: TSpeedButton;
    spbShowNewPassword2: TSpeedButton;
    procedure butSaveClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure spbShowPasswordMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure spbShowPasswordMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    Success, AllowCancel: boolean;
    OldPassword: string;
    class var FInstance: TfrmChangePassword;
    procedure ObserveTask (Task: TObservableTask);
  public
    class procedure ShowInstance (AController: TObservableController; OldPassword: string = '';
                                  const AllowCancel: boolean = true);
  end;



implementation

{$R *.fmx}

uses
  SynCommons;

class procedure TfrmChangePassword.ShowInstance (AController: TObservableController; OldPassword: string = '';
                                                 const AllowCancel: boolean = true);
begin
  if not Assigned(FInstance) then
    begin
      TfrmChangePassword.Controller := AController; // class property
      Application.CreateForm(TfrmChangePassword, FInstance);
    end;
  FInstance.OldPassword := OldPassword;
  FInstance.AllowCancel := AllowCancel;
  FInstance.ShowModal;
end;

procedure TfrmChangePassword.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmChangePassword.butSaveClick(Sender: TObject);
var
  OldPasswordHash: RawUTF8;
begin
  OldPasswordHash := TSQLAuthUserExClass(Controller.Model.AuthUserClass).ComputeHashedPassword (StringToUTF8 (edtOldPass.Text));
  if (Controller.LoggedUser.PasswordHashHexa <> OldPasswordHash)
     then ShowErrorAndAbort ('Stara lozinka nije tačna');

  if (edtNewPass.Text <> edtConfPass.Text)
     then ShowErrorAndAbort ('Potvrda lozinke nije tačna');

  Controller.CheckPasswordValidity (edtNewPass.Text);

  btnSave.Enabled := false;
  btnCancel.Enabled := false;

  var user := TSqlAuthUserEx(Controller.LoggedUser.CreateCopy);
  user.PasswordPlain := StringToUTF8 (edtConfPass.Text);
  Controller.SetMyData (OldPasswordHash, user, self);
end;

procedure TfrmChangePassword.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := AllowCancel or Success;
end;

procedure TfrmChangePassword.FormShow(Sender: TObject);
begin
  Success := false;

  spbShowPassword.ImageIndex := Controller.dicImageIndexes['showpassword'];
  spbShowNewPassword.ImageIndex := Controller.dicImageIndexes['showpassword'];
  spbShowNewPassword2.ImageIndex := Controller.dicImageIndexes['showpassword'];

  spbShowPassword.Images := Controller.ImageList;
  spbShowNewPassword.Images := Controller.ImageList;
  spbShowNewPassword2.Images := Controller.ImageList;

  edtOldPass.Text := OldPassword;
  btnCancel.Enabled := AllowCancel;

  if OldPassword <> ''
     then edtNewPass.SetFocus
     else edtOldPass.SetFocus;
end;

procedure TfrmChangePassword.ObserveTask(Task: TObservableTask);
begin
  Success := Task.Success;

  if Success
     then ModalResult := mrOk
     else labDescription.Text := Task.Outcome.DescriptionInSingleLine;

  btnSave.Enabled := true;
  btnCancel.Enabled := AllowCancel;
end;

procedure TfrmChangePassword.spbShowPasswordMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  TEdit(TControl(Sender).Parent).Password := false;
end;

procedure TfrmChangePassword.spbShowPasswordMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  TEdit(TControl(Sender).Parent).Password := true;
end;

end.
