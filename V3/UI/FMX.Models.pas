﻿unit FMX.Models;

interface

uses
  System.SysUtils, System.UITypes, System.Classes, System.Generics.Collections,
  System.Types, System.TypInfo, System.Rtti, System.Actions, math,
  FMX.Grid,  FMX.ActnList,  FMX.TabControl,  FMX.Forms,  FMX.StdCtrls,  FMX.Edit,  FMX.Controls, FMX.ListBox,
  FMX.ComboEdit, FMX.Types, FMX.Graphics, FMX.ImgList, FMX.Layouts, FMX.DateTimeCtrls, FMX.Memo,

  syncommons,  mORMot, IniFiles, IOUtils,
  MormotMods, MormotTypes, MormotAttributes, SqlRecords, Common.Utils,
  FMX.Common.Utils, UI.StateManagement, ObservableController, Controller.Tasks, Validation, FMX.BaseModel;

const
  Default_Grid_Column_Width = 150;

type

  {$SCOPEDENUMS ON}

  TSqlRecordStater  = class (TCustomSqlRecordStater)
    Owner: TBaseModel;

    constructor Create (AOwner: TSqlRecordModel); virtual;

    function RequiredStates (Action: TModelAction): TAppStateArray; override;
    function ForbiddenStates (Action: TModelAction): TAppStateArray; override;
    function RequiredDetailStates (cl: TSqlRecordClass; Action: TModelAction): TAppStateArray; override;
    function ForbiddenDetailStates (cl: TSqlRecordClass; Action: TModelAction): TAppStateArray; override;

    procedure CreateStates (States: TAppStateDictionary); virtual;
    procedure Check (rec: TSqlRecord); virtual;
  end;

  TLookupModel = class (TSqlRecordModel)
  private
//    FbtnClearSelection: TControl;
//    procedure SetbtnClearSelection(const Value: TControl);
//    procedure OnClearClick(Sender: TObject);

  protected
    OriginalOnChange: TNotifyEvent;
    procedure CreateStates; override;
    procedure Populate; virtual;
    function SelectedText: string; virtual; abstract;
    //function GetSelectedID: TID; override;
    procedure AppStateNotification(const State: TAppState); override;
//    procedure AfterLookupPopulate; virtual; abstract;
  public
    CustomLookup: boolean;
    constructor Create (ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController); override;
    destructor Destroy; override;

    procedure ClearSelection; virtual;
    procedure ClearLookup;
  end;

(*  TLookupComboBoxModel = class (TLookupModel)
  private
    SuspendEvent: boolean;
    function HasAllItemsItem: boolean;
    procedure SetItemIndex(index: integer);
    function ComboBoxHasSelection: boolean;
  protected
    FComboBox: TComboBox;
    procedure OnChange (Sender: TObject);
    procedure Populate; override;
    function SelectedText: string; override;
    procedure DoSetSelectedID (const ID: TID); override;
    procedure FetchLookup; override;
    procedure MasterIDChanged; override;
//    function GetSelectedID: TID; override;
    procedure AfterLookupPopulate; override;
  public
    AllItemsItem: string;
    procedure ObserveLookup(Task: TObservableLookupTask); override;
    procedure AfterConstruction; override;
    constructor Create (ASqlRecordClass: TSqlRecordClass; AComboBox: TComboBox; AController: TObservableController); reintroduce; virtual;
    destructor Destroy; override;
  end;        *)


  TLookupEditModel = class (TLookupModel)
  private
    procedure SetFetchIfEmpty(const Value: boolean);
    function GetText: string;
    procedure SetText(const Value: string);
    procedure SetCustomEdit(const Value: TCustomEdit); virtual;
  protected
    FetchingSelectedRecord, FetchingBecauseEmpty, NonFilteredFetchWasEmpty,
    FExpLookup, FTyping, FFetchIfEmpty: boolean;
    FCustomEdit: TCustomEdit;
    //procedure Populate; override;
    function SelectedText: string; override;
    procedure OnTyping (Sender: TObject); virtual;
    procedure OnChange (Sender: TObject); virtual; abstract;
    procedure OnEnter (Sender: TObject);
    procedure DoMasterIDChanged; override;
//    function GetSelectedID: TID; override;
    procedure CreateStates; override;
    procedure ClearList; virtual; abstract;
    procedure DoOnEnter; virtual;
    procedure PrepareFilter (var qp: TQueryParameters); virtual;
    function GeneralFilter: string; virtual;
    procedure DoFetch; override;

    property CustomEdit: TCustomEdit read FCustomEdit write SetCustomEdit;
  public
    BarcodeEntry: boolean;
    AutoSelectSingleItem, AutoNext, ClearOnEnter: boolean;
    //apsRecordSelected{, ApsOnChange}: TAppState;

    destructor Destroy; override;
    constructor Create (ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController); override;

    procedure ClearAndFocus;
    function UnresolvedEntry: boolean;
    procedure Clear;
    procedure ClearSelection; override;
    property Text: string read GetText write SetText;
    property FetchIfEmpty: boolean read FFetchIfEmpty write SetFetchIfEmpty;
  end;

  (*
  TLookupComboEditModel<T: TSqlRecord> = class (TLookupEditModel)
  protected
    FComboEdit: TComboEdit;
    procedure ClearList; override;
    procedure DoOnEnter; override;
    procedure OnChange (Sender: TObject); override;
  public
    procedure Populate; override;
    procedure DoSetSelectedID(const ID: TID); override;
    constructor Create (AComboEdit: TComboEdit; AController: TObservableController); reintroduce;
  end;
  *)

//  TBaseSqlRecordModel <T : TSQLRecord> = class;

  TCrudSqlRecordModel = class (TSqlRecordModel)
  private
    function GetCurrentOperation: TCrud;
  protected
    FCrudRecord : TSqlRecord;
//    procedure OnShowStatesUI(Sender: TObject);

    procedure CreateStates; override;

    procedure ExecuteAction(ModelAction: TModelAction); override;
    procedure PopulateUI; override;
    procedure SetCrudRecord(const Value: TSqlRecord); override;
    function GetCrudRecord: TSqlRecord; override;
    procedure DoPost; virtual;
    procedure DoDelete; virtual;
    function CanCreateRecord (rec: TSqlRecord): boolean; virtual;
    procedure AppStateNotification (const State: TAppState); override;
  public
    RecordStater: TSqlRecordStater;
//    constructor Create (ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController); override;
    destructor Destroy; override;

    procedure Post;
    procedure Cancel; override;
    procedure Delete;
    procedure CreateRecord; overload; virtual;
    procedure CreateRecord (rec: TSqlRecord); overload; virtual;
    procedure LockForUpdate(ID: TID);

    procedure ProcessUnlock (Task: TSingleRecordTask);

    property crudRecord: TSqlRecord read GetCrudRecord write SetCrudRecord;
    property CurrentOperation: TCrud read GetCurrentOperation;
  end;

  TORMComboBoxModel = class
    class procedure Setup (Items: TStrings; Model: TSqlModelEx);
    class function SelectedTable (ComboBox: TCustomComboBox; Model: TSqlModelEx): TSqlRecordClass;
    class procedure SetupFields (SqlRecordClass: TSqlRecordClass; Items: TStrings; Model: TSqlModelEx);
    class function SelectedField (ComboBox: TCustomComboBox): TFieldInfo;
  end;

//  TBaseSqlRecordModel <T : TSQLRecord> = class (TBaseSqlRecordModel)
//  private
//  protected
//    function GetSelectedRecord: T; reintroduce; virtual; abstract;
//    procedure SetCrudRecord(const Value: T);
//    function GetCrudRecord: T;
//  public
//    property crudRecord: T read GetCrudRecord write SetCrudRecord;
//    property SelectedRecord: T read GetSelectedRecord;
//  end;
(*
  TSingleRecordModel = class (TBaseModel, IRecordObserver{<T>})
  private
  protected
    FSqlRecord: TSqlRecord;
    function GetSelectedRecord: TSqlRecord; override;
//    function GetSelectedID: TID; override;
    procedure SetSqlRecord(const Value: TSqlRecord);
    function GetSqlRecord: TSqlRecord;
  public
//    constructor Create (const AName: string); override;
    destructor Destroy; override;

    procedure ObserveRecord (Task: TSingleRecordTask);
    procedure Reload; override;
    property SqlRecord: TSqlRecord read GetSqlRecord write SetSqlRecord;
  end;

  TSingleRecordModel <T : TSQLRecord> = class (TSingleRecordModel)
  protected
    function GetSelectedRecord: T; reintroduce;
    function GetSqlRecord: T;
    procedure SetSqlRecord(const Value: T);
  public
    property SqlRecord: T read GetSqlRecord write SetSqlRecord;
  end;
*)
  function RecordDeleteConfirmation (const BriefRecord: string): boolean;
  procedure PrepareGrid (Model: TSQLModelEx; SqlRecordClass: TSQLRecordClass; {Master: TSqlRecordModel; }
                         ForeignKeyName: string; grid: TStringGrid; RowNumbers, Brief: boolean; IgnoreFields: string = '');
//  procedure AutoColumnWidths(Grid: TStringGrid);


var
  LookupComboEditFetchDelay: cardinal = 500;
  DefaultCurrencyDecimals: integer = 2;
  DefaultDoubleDecimals: integer = 2;

//  dicFieldDisplayNameTranslations: TDictionary <string, string>;


implementation

uses
  FMX.DialogService.Sync, FMX.StateManagement.Form, FMX.GridLookupEdit;

{procedure AutoColumnWidths(Grid: TStringGrid);
var
  Column: TColumn;
  TextWidth: Single;
  Bitmap : TBitmap;
  s: string;
  iRow, iCol, slen: Integer;
  TotalWidth: single;
begin
  // Create a temporary bitmap to measure text width
  TotalWidth := 0;
  Bitmap := TBitmap.Create;
  try
    Bitmap.Canvas.Font.Assign(grid.TextSettings.Font);
    // Iterate through all columns
    for iCol := 0 to Grid.ColumnCount - 1 do
    begin
      Column := Grid.Columns[iCol];
      // Measure the width of the header text
      s := Column.Header;
      slen := s.Length;
      for iRow := 0 to min (100, grid.RowCount - 1) do
        if grid.Cells[iCol, iRow].Length > slen then
          begin
            s := grid.Cells[iCol, iRow];
            slen := s.Length;
          end;

      TextWidth := Bitmap.Canvas.TextWidth(s);

      // Set the column width to the text width plus some padding
      Column.Width := min (300, TextWidth + 20); // Adjust the padding as needed
      TotalWidth := TotalWidth + Column.Width;

//      if (iCol = Grid.ColumnCount - 1) and (Grid.Width - 25 - TotalWidth > 0)
//         then Column.Width := Column.Width + (Grid.Width - 50 - TotalWidth);
    end;
  finally
    Bitmap.Free;
  end;
end;                      }

procedure PrepareGrid (Model: TSQLModelEx; SqlRecordClass: TSQLRecordClass; {Master: TSqlRecordModel;}
                       ForeignKeyName: string;
                       grid: TStringGrid; RowNumbers, Brief: boolean; IgnoreFields: string = '');
var
  col: TColumn;
  sl: TStringList;
  fi: TFieldInfo;
  DisplayName: string;
  field: TSQLPropInfo;
begin
  if IgnoreFields <> ''
    then
      begin
        sl := TStringList.Create;
        sl.CommaText := IgnoreFields;
      end
    else sl := nil;

  //if grid.ColumnCount > 0 then raise Exception.Create('Grid already has columns?!');

  if (grid.ColumnCount = 0) and RowNumbers
     then grid.AddColumn (TIntegerColumn, '#', '', 50);

{  if Brief and ClassHasAttribute (SqlRecordClass, IDIsBrief) then
    begin
      DisplayName := Model.GetFieldDisplayName (SqlRecordClass, 'ID');
      col := grid.AddColumn  (TIntegerColumn, DisplayName, 'ID');
      col.Width := Default_Grid_Column_Width div 4;
      col.TagObject := SqlRecordClass.FieldInfoList.ItemsByName['ID'];
    end;
}
  for fi in SqlRecordClass.FieldInfoList.Displayable do
    begin
      if sl.IsAssigned and (sl.IndexOf (fi.Name) > -1)
         then continue;

      if Brief and not fi.Brief
         then continue;

      if not ForeignKeyName.IsEmpty and (fi.ForeignKeyName = ForeignKeyName)
//      if Master.IsAssigned and (fi.ForeignTableName <> '')
//        then
//          if (fi.ForeignTableName = Master.SqlRecordClass.SQLTableName.AsString)
//          or (fi.ForeignTableName = Master.SqlRecordClass.crudSQLTableName.AsString)
         then continue;

      DisplayName := Model.GetFieldDisplayName (SqlRecordClass, fi.DisplayName);

      field := SqlRecordClass.FieldByName (fi.Name);

//      col := nil;

      if field.IsAssigned
        then case field.SQLFieldType of
            TSQLFieldType.sftBoolean: col := grid.AddColumn  (TCheckColumn,   DisplayName, fi.Name);
            TSQLFieldType.sftInteger: col := grid.AddColumn  (TIntegerColumn, DisplayName, fi.Name);
            TSQLFieldType.sftCurrency: begin
                                         col := grid.AddColumn  (TFloatColumn,  DisplayName, fi.Name);
                                         TFloatColumn(col).DecimalDigits := DefaultCurrencyDecimals;
                                         TFloatColumn(col).ShowThousandSeparator := true;
                                       end;
            TSQLFieldType.sftFloat: if fi.IsDate // mormot stores date as float for whatever reason
                                      then col := grid.AddColumn  (TDateColumn,  DisplayName, fi.Name)
                                      else begin
                                             col := grid.AddColumn  (TFloatColumn,  DisplayName, fi.Name);
                                             TFloatColumn(col).DecimalDigits := DefaultCurrencyDecimals;
                                           end;
            TSQLFieldType.sftCreateTime, TSQLFieldType.sftModTime, TSQLFieldType.sftDateTime:
                                     //FieldDefs.Add(P.Name.AsString, ftDateTime);
                                     if true //fi.IsDateTime
                                        then col := grid.AddColumn  (TStringColumn, DisplayName, fi.Name)
                                        else col := grid.AddColumn  (TDateColumn, DisplayName, fi.Name)
          else col := grid.AddColumn  (TStringColumn, DisplayName, fi.Name);
          end //case
        else // no mormot field info, can be public property e.g. calculated field
          if fi is TFieldInfoCurrency then
            begin
              col := grid.AddColumn  (TFloatColumn,  DisplayName, fi.Name);
              TFloatColumn(col).DecimalDigits := DefaultCurrencyDecimals;
              TFloatColumn(col).ShowThousandSeparator := true;
            end
            else col := grid.AddColumn  (TStringColumn, DisplayName, fi.Name);

        if col.IsAssigned then
          begin
            col.Width := Default_Grid_Column_Width;
            col.TagObject := fi;
          end;
    end;

//  AutoColumnWidths (grid);
  sl.Free;
end;
(*
procedure PrepareGrid (Model: TSQLModelEx; SqlRecordClass: TSQLRecordClass; grid: TStringGrid; RowNumbers: boolean; IgnoreField: RawUTF8 = '');
begin
  if (grid.ColumnCount = 0) and RowNumbers
     then grid.AddColumn (TIntegerColumn, '#', '', 50);

  var ctx := TRttiContext.Create;
  try

    var TableType := ctx.GetType(SqlRecordClass);
    if SqlRecordClass.ClassParent.InheritsFrom (TSqlRecordEx) and (SqlRecordClass.ClassParent <> TSqlRecordEx)
       then PrepareGrid (Model, TSqlRecordClass (SqlRecordClass.ClassParent), grid, false, IgnoreField);

    for var prop in TableType.GetProperties do
      begin
        if prop.Parent <> TableType then continue;

        for var Attribute in prop.GetAttributes do
          if Attribute is DisplayableAttribute then
          begin
            if prop.HasAttribute (HiddenIfViewAttribute)
               then continue;

            var da := DisplayableAttribute (Attribute);
            var DisplayName: string := '';

            DisplayName := Model.GetFieldDisplayName (SqlRecordClass, prop.Name);

            if DisplayName = '' then
               if da.DisplayName = ''
                  then DisplayName := prop.Name
                  else DisplayName := da.DisplayName;

            var field := SqlRecordClass.RecordProps.Fields.ByRawUTF8Name(StringToUTF8(prop.Name));
            if field.Name = IgnoreField
               then continue;
            case field.SQLFieldType of
              TSQLFieldType.sftBoolean: grid.AddColumn  (TCheckColumn,   DisplayName, prop.Name);
              TSQLFieldType.sftInteger: grid.AddColumn  (TIntegerColumn, DisplayName, prop.Name);
              TSQLFieldType.sftCurrency, TSQLFieldType.sftFloat: grid.AddColumn  (TFloatColumn,  DisplayName, prop.Name);
              TSQLFieldType.sftCreateTime, TSQLFieldType.sftModTime, TSQLFieldType.sftDateTime:
                                       //FieldDefs.Add(P.Name.AsString, ftDateTime);
                                       grid.AddColumn  (TDateColumn, DisplayName, prop.Name)
            else grid.AddColumn  (TStringColumn, DisplayName, prop.Name);
            end;

            //rtti nonsense to get boolean type if (GetTypeData(prop.PropertyType.Handle)^.BaseType^ = TypeInfo(Boolean))
            break;
          end;
      end;

  finally
    ctx.Free;
  end;

end;
*)

function RecordDeleteConfirmation (const BriefRecord: string): boolean;
begin
  {$IFDEF ENGLISH}
  result := ConfirmationDialog('Are you sure you want to delete this record?'#13#13 + BriefRecord);
  {$ELSE}
  result := ConfirmationDialog('Da li ste sigurni da želite da obrišete ovaj slog?'#13#13 + BriefRecord);
  {$ENDIF}
end;

{ TLookupComboModel<T> }
(*
procedure TLookupComboBoxModel.AfterConstruction;
begin
  inherited;
  Populate;
  SelectedID := NULL_ID;
end;

constructor TLookupComboBoxModel.Create(ASqlRecordClass: TSqlRecordClass; AComboBox: TComboBox; AController: TObservableController);
begin
  inherited Create (ASqlRecordClass, 'lcbm' + AComboBox.Owner.Name + '.' + AComboBox.Name, AController);

  FComboBox := AComboBox;
  OriginalOnChange := FComboBox.OnChange;
  FComboBox.OnChange := OnChange;

//  RegisterStateObserver(FComboBox, TAppStateAction.Enabled, [], [apsFetching]);
end;

destructor TLookupComboBoxModel.Destroy;
begin
  UnRegisterStateObserver(FComboBox);
  inherited;
end;

procedure TLookupComboBoxModel.FetchLookup;
begin
  if ApsFetching.Active then exit;

  inherited;

  if FNextSelectedID.IsNull
    then FNextSelectedID := SelectedID;

  if HasMaster and MasterID.IsNull then
    begin
      ClearLookup;
      ApsFetching.Deactivate;
      exit;
    end;

  var qp := default (TQueryParameters);
  qp.Filter.AddFrom(AdditionalFilter);
  qp.MasterTableName.AsString := MasterTableName;
  if HasMaster
     then begin
            qp.MasterID := Master.SelectedID;
            qp.Filter.Add(DetailLinkKeyName, TFilterOperator.Equal, MasterID);
          end;
  Controller.GetLookup (FSqlRecordClass, qp, self);
end;

//function TLookupComboBoxModel.GetSelectedID: TID;
//begin
//  if ComboBoxHasSelection then
//    begin
//      var index := FComboBox.ItemIndex;
//      if HasAllItemsItem then dec (index);
//      result := Lookup.IDs[index];
//    end
//    else result := FNextSelectedID;
//end;

function TLookupComboBoxModel.HasAllItemsItem: boolean;
begin
  result := AllItemsItem <> '';
end;

procedure TLookupComboBoxModel.Populate;
begin
  inherited;
  SuspendEvent := true;
  FComboBox.BeginUpdate;
  FComboBox.Items.Clear;
  if HasAllItemsItem then
    FComboBox.Items.Add(AllItemsItem);
  var fl := Lookup;
  if fl<>nil then
    //for var pair in fl.Items do
    for var s in fl.Values do
      FComboBox.Items.Add(s.AsString {pair.Value.AsString});
  FComboBox.EndUpdate;
  SuspendEvent := false;
end;

function TLookupComboBoxModel.SelectedText: string;
begin
  if ComboBoxHasSelection
    then result := FComboBox.Selected.Text
    else result := '';
end;

procedure TLookupComboBoxModel.MasterIDChanged;
begin
  inherited;
  FetchLookup;
end;

procedure TLookupComboBoxModel.ObserveLookup(Task: TObservableLookupTask);
begin
  inherited;
end;

procedure TLookupComboBoxModel.OnChange(Sender: TObject);
begin
  if Assigned (OriginalOnChange) then
     OriginalOnChange (FComboBox);
  if SuspendEvent then exit;

//  apsSelectionChangedDelayed.CycleOnOff;
//  apsSelectionChangedInstant.CycleOnOff;
end;

procedure TLookupComboBoxModel.SetItemIndex(index: integer);
begin
  if HasAllItemsItem then inc (index);
  FComboBox.ItemIndex := index;
end;

procedure TLookupComboBoxModel.AfterLookupPopulate;
begin
  SelectedID := FNextSelectedID;
end;

function TLookupComboBoxModel.ComboBoxHasSelection: boolean;
begin
  if HasAllItemsItem
     then result := FComboBox.ItemIndex > 0
     else result := FComboBox.ItemIndex > -1;
end;

procedure TLookupComboBoxModel.DoSetSelectedID(const ID: TID);
begin
  if ID.IsNull
    then SetItemIndex (-1)
    else SetItemIndex (Lookup.IndexOf (ID));

  if ID.IsNotNull and not ComboBoxHasSelection
    then begin
           FNextSelectedID := ID;
           FetchLookup;
         end
    else if ApsFetching.Active
         then FNextSelectedID := ID
         else FNextSelectedID := NULL_ID;
  ApsSelectionChangedDelayed.CycleOnOff;
  ApsSelectionChangedInstant.CycleOnOff;
end;
*)


{ TLookupModel<T> }

procedure TLookupModel.ClearSelection;
begin
  SelectedID := null_ID;
end;

constructor TLookupModel.Create (ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController);
begin
  inherited;
  FName := AName;
  AdditionalFilter := default (TQueryFilter);
end;

procedure TLookupModel.CreateStates;
begin
  inherited;
  ApsFetching := CreateState (Name + '.Fetching');
end;

destructor TLookupModel.Destroy;
begin
  FreeAndNil (Activity);
  inherited;
end;

//function TLookupModel.GetSelectedID: TID;
//begin
//  if Lookup.IsAssigned and (SelectedText <> '') and (Lookup.MasterID = MasterID)
//     then Lookup.Reverse.TryGetValue (StringToUTF8 (SelectedText), result)
//     else if FNextSelectedID.IsNotNull
//          then result := FNextSelectedID
//          else result := NULL_ID;
//end;

procedure TLookupModel.ClearLookup;
begin
  Populate;
  SelectedID := NULL_ID;
end;

procedure TLookupModel.Populate;
begin

end;

procedure TLookupModel.AppStateNotification(const State: TAppState);
begin
  inherited;
  if Master.IsAssigned and (State = Master.apsSelectedRecordChangedDelayed)
    then MasterIDChanged;
end;

procedure TLookupEditModel.SetCustomEdit(const Value: TCustomEdit);
begin
  if FCustomEdit.IsAssigned
     then begin
            FCustomEdit.RemoveFreeNotify (self);
            dicControlToModel.Remove (FCustomEdit);
          end;
  FCustomEdit := Value;
  if FCustomEdit = nil then exit;

  FCustomEdit.OnTyping := OnTyping;
  OriginalOnChange := FCustomEdit.OnChange;
  FCustomEdit.OnChange := OnChange;
  FCustomEdit.OnEnter := OnEnter;
  if Value.IsAssigned
     then dicControlToModel.Add (Value, self);
end;

procedure TLookupEditModel.SetFetchIfEmpty(const Value: boolean);
begin
  FFetchIfEmpty := Value;
//  if Value and (Lookup.Count = 0)
//    then StartFetchTimer;
end;

//procedure TLookupModel.OnClearClick(Sender: TObject);
//begin
//  ClearSelection;
//end;

//procedure TLookupModel.SetbtnClearSelection(const Value: TControl);
//begin
//  FbtnClearSelection := Value;
//  if Value.IsAssigned
//     then if Assigned (Value.OnClick)
//          then raise Exception.Create ('btnClearSelection OnClick assigned')
//          else Value.OnClick := OnClearClick;
//end;

{ TLookupComboEditModel<T> }

//procedure TLookupEditModel.AfterLookupPopulate;
//begin
//  if FNextSelectedID.IsNotNull
//     then SelectedID := FNextSelectedID;
//end;

(*

constructor TLookupComboEditModel<T>.Create(AComboEdit: TComboEdit; AController: TObservableController);
begin
  inherited Create (T, 'lcem' + AComboEdit.Owner.Name + '.' + AComboEdit.Name, AController);
  CustomEdit := AComboEdit;
  FComboEdit := AComboEdit;
end;

procedure TLookupComboEditModel<T>.ClearList;
begin
  FComboEdit.Items.Clear;
end;

procedure TLookupComboEditModel<T>.OnChange (Sender: TObject);
begin
  if FComboEdit.ItemIndex <> - 1 then
    begin
      SelectedID := Lookup.IDs[FComboEdit.ItemIndex];
{      var NewID: TID;
      if Lookup.Reverse.TryGetValue (StringToUTF8 (FComboEdit.Text), NewID)
        then SelectedID := NewID;}
    end;

  if Assigned (OriginalOnChange) then
     OriginalOnChange (FComboEdit);
//  ApsOnChange.CycleOnOff;
end;

procedure TLookupComboEditModel<T>.DoOnEnter;
begin
  inherited;

  if not FComboEdit.DroppedDown and (Lookup.Count > 0)
     then begin
            FComboEdit.DropDown;
            //FComboEdit.SetFocus;
          end;
end;

procedure TLookupComboEditModel<T>.Populate;
var
  s: RawUTF8;
begin
  inherited;

  var fl := Lookup;

  if FSelectedID.IsNotNull  then
    if (fl <> nil) and fl.Items.TryGetValue(FSelectedID, s)
     then
       begin
         FComboEdit.Text := s.AsString;
         exit;
       end;

  FComboEdit.BeginUpdate;
  FComboEdit.Items.BeginUpdate;
  FComboEdit.Items.Clear;

  if fl<>nil then
    for var u in fl.Values do
      FComboEdit.Items.Add(u.AsString);

  FComboEdit.CloseDropDown;
  FComboEdit.DropDownCount := min (12, FComboEdit.Items.Count);

  if FComboEdit.IsFocused then
     FComboEdit.DropDown;

  FComboEdit.Items.EndUpdate;
  FComboEdit.EndUpdate;

  if (FComboEdit.ItemIndex = -1) and (FComboEdit.Items.Count = 1)
     then begin
            if AutoSelectSingleItem and not FComboEdit.Text.IsEmpty
               then SelectedID := fl.IDs[0];
//            FComboEdit.ItemIndex := 0;
//            FSelectedID := fl.IDs[0];
//            FNextSelectedID := FSelectedID;
            if AutoNext
               then begin
                      var nxt := FComboEdit.ParentControl.GetTabList.FindNextTabStop(FComboEdit, true, false);
                      if (nxt <> nil)
                         then nxt.SetFocus;
                    end;
          end;

//  FComboEdit.SetFocus;
end;

procedure TLookupComboEditModel<T>.DoSetSelectedID(const ID: TID);
begin
  inherited;

  try
    if ID.IsNotNull
      then FComboEdit.ItemIndex := Lookup.IndexOf (ID)
      else FComboEdit.ItemIndex := -1;

    if FComboEdit.ItemIndex = -1 then
      begin
        if not FTyping then
          begin
            //FComboEdit.Clear; this would clear all items which we dont want
            FComboEdit.Text := '';
          end;
        if ID.IsNotNull then StartFetchTimer;
      end;
  except

  end;

end;

*)

procedure TLookupEditModel.ClearAndFocus;
begin
  Clear;
  if FCustomEdit.CanFocus
     then FCustomEdit.SetFocus;
end;

procedure TLookupEditModel.ClearSelection;
begin
  inherited;
  FCustomEdit.Text := '';
  if FCustomEdit.CanFocus
     then FCustomEdit.SetFocus;
end;

procedure TLookupEditModel.Clear;
begin
  Text := '';
  FSelectedID.Clear;
end;

constructor TLookupEditModel.Create(ASqlRecordClass: TSqlRecordClass; const AName: string; AController: TObservableController);
begin
  inherited;
  CustomLookup := true;
  FetchIfEmpty := not ClassHasAttribute (ASqlRecordClass, LargeTableAttribute);
end;

procedure TLookupEditModel.CreateStates;
begin
  inherited;
  //ApsOnChange := CreateState (Name+'_OnChange');aa
  //ApsRecordSelected := CreateState (FSqlRecordClass, Name+'_RecordSelected');
end;

destructor TLookupEditModel.Destroy;
begin
  CustomEdit := nil;
  inherited;
end;

procedure TLookupEditModel.PrepareFilter (var qp: TQueryParameters);
begin
  qp.Filter.AddFrom(AdditionalFilter);
end;

function TLookupEditModel.GeneralFilter: string;
begin
  result := CustomEdit.Text;
end;

procedure TLookupEditModel.DoFetch;
begin
//  if FNextSelectedID.IsNull
//    then FNextSelectedID := SelectedID;

  if ( FCustomEdit.Text.IsEmpty and FSelectedID.IsNull {and FNextSelectedID.IsNull }and not FetchIfEmpty )
     or
     ( Master.IsAssigned and MasterID.IsNull)
     then
    begin
      ClearList;
      Activity.EndActivity;
      exit;
    end;

  inherited;

  var qp := default (TQueryParameters);

  FetchingBecauseEmpty := false;

  if FSelectedID.IsNotNULL
    then qp.Filter.Add('rowid', TFilterOperator.Equal, FSelectedID)
    else if GeneralFilter <> ''
         then qp.GeneralFilter.AsString := GeneralFilter
         else FetchingBecauseEmpty := true;

  PrepareFilter (qp);

  qp.MasterTableName.AsString := MasterTableName;

  if HasMaster
     then begin
            qp.MasterID := Master.SelectedID;
            qp.Filter.Add(DetailLinkKeyName, TFilterOperator.Equal, MasterID);
          end
     else qp.MasterID := MasterID; // for manually assigned master id?

  qp.Page.Limit := 20;

  if FExpLookup
     then Controller.RecordsRead (FSqlRecordClass, qp, self)
     else Controller.GetLookup (FSqlRecordClass, qp, self);
end;

//function TLookupEditModel.GetSelectedID: TID;
//begin
//  result := FSelectedID;
//end;

function TLookupEditModel.GetText: string;
begin
  result := FCustomEdit.Text;
end;

procedure TLookupEditModel.OnEnter(Sender: TObject);
begin
  DoOnEnter;
end;

procedure TLookupEditModel.DoOnEnter;
begin
  If ClearOnEnter  {and (FComboEdit.ItemIndex > -1)} then
    begin
      ClearList;
      SelectedID := NULL_ID;
      FCustomEdit.Text := '';
    end;

//  if FetchIfEmpty and (Lookup.Count = 0) then FetchLookup
end;

procedure TLookupEditModel.OnTyping(Sender: TObject);
begin
  FTyping := true;
  SelectedID := NULL_ID;
  FTyping := false;
  PostponeFetch;
end;

function TLookupEditModel.UnresolvedEntry: boolean;
begin
  result := SelectedID.IsNULL and not FCustomEdit.Text.IsEmpty;
end;

function TLookupEditModel.SelectedText: string;
begin
  result := FCustomEdit.Text;
end;

procedure TLookupEditModel.DoMasterIDChanged;
begin
  inherited;

  NonFilteredFetchWasEmpty := false;
  SelectedID := NULL_ID;
//  try
//    FComboEdit.Clear;
//    FComboEdit.Text := '';
//  except
//
//  end;
  {if WaitingToFetch
       then FetchLookup
            else }if FetchIfEmpty and not ApsFetching.Active
          then PostponeFetch;
end;


(*
procedure TLookupEditModel.DoSetSelectedID(const ID: TID);
begin
  if ID.IsNotNULL and SelectedSqlRecord.IsNotAssigned then
    begin
      // we have a "desired" ID, but no selected record, so we are fetching data and will come back once its fetched
      FSelectedID := ID; // store it as if it is selected in order to be persistent
      FetchingSelectedRecord := true;
      exit;
    end;

  if (FSelectedID = ID) and not ID.IsNULL and not FetchingSelectedRecord then exit;

  FetchingSelectedRecord := false;
  FSelectedID := ID;
  ApsRecordSelected.SetActiveIf (ID.IsNotNull);

  ApsSelectionChangedDelayed.CycleOnOff;
  ApsSelectionChangedInstant.CycleOnOff;

//aaa  apsRecordSelected.SetActiveIf (GetSelectedRecord <> nil);
//  ApsSelectionChanging.CycleOnOff;
//  ApsSelectionChangedInstant.CycleOnOff;
//  ApsSelectionChangedDelayed.CycleOnOff;
//   aaa
end;
*)

procedure TLookupEditModel.SetText(const Value: string);
begin
  SelectedID := NULL_ID;
  FCustomEdit.Text := Value;
  PostponeFetch;
end;

procedure TCrudSqlRecordModel.Cancel;
begin
  if not apsCU.Active then exit;

  if crudRecord <> nil then
    begin
      if apsCreate.Active
         then begin
                apsCreate.Deactivate;
                crudRecord := nil;
                if TUIDialogueOption.CloseOnCancelCreate in UIDialogueOptions
                   then CloseUIDialogue;
              end
         else Controller.RecordUnlock (crudRecord, self);
    end
    else // wtf + does this happen ever...
      if TUIDialogueOption.CloseOnCancelCreate in UIDialogueOptions
         then CloseUIDialogue;
end;

function TCrudSqlRecordModel.CanCreateRecord (rec: TSqlRecord): boolean;
begin
  result := true;
end;

procedure TCrudSqlRecordModel.AppStateNotification(const State: TAppState);
begin
  inherited;
  if (State = apsFormClosed) and State.Active and apsCU.Active then
    begin
      if apsUpdate.Active and SelectedID.IsNull then exit; // already cancelled, why OnClose twice???
      {TODO -oOwner -cGeneral : check^}
      Cancel;
      // SelectedID := NULL_ID; zasto???
    end;
end;

destructor TCrudSqlRecordModel.Destroy;
begin
  FreeAndNil (FActionList);
  FreeAndNil (FCrudRecord);
  inherited;
end;

procedure TCrudSqlRecordModel.CreateRecord;
begin
  CreateRecord (SqlRecordClass.Create);
end;

procedure TCrudSqlRecordModel.CreateRecord(rec: TSqlRecord);
begin
  SelectedID := NULL_ID;
  if not CanCreateRecord (rec) then
    begin
      rec.Free;
      exit;
    end;
  crudRecord := rec;

  if (Master <> nil)
     then crudRecord.SetFieldVariant (DetailLinkKeyName.AsString, Master.SelectedID);
  crudRecord.ApplyDefaults;

  apsCreate.Activate;
  ShowUIDialogue;
end;

procedure TCrudSqlRecordModel.CreateStates;
begin
  inherited;
  ApsCU     := CreateState ('CU', TAppStateTiming.Immediate, apsOwnerCU);
  ApsCreate := CreateState (CrudToString [TCRUD.Create], TAppStateTiming.Immediate, apsCU);
  ApsUpdate := CreateState (CrudToString [TCRUD.Update], TAppStateTiming.Immediate, apsCU);
  apsAllDataEntered := CreateState ('AllDataEntered', TAppStateTiming.Immediate);
  apsVisibleControlsValid := CreateState ('VisibleControlsValid', TAppStateTiming.Immediate);
end;

procedure TCrudSqlRecordModel.LockForUpdate (ID: TID);
begin
  Controller.RecordReadLock(SqlRecordClass, ID, self);
end;

procedure TCrudSqlRecordModel.Delete;
begin
  DoDelete;
end;

procedure TCrudSqlRecordModel.DoDelete;
begin
  Controller.RecordDelete(GetSelectedRecord.CreateCopy, self);
end;

procedure TCrudSqlRecordModel.ExecuteAction(ModelAction: TModelAction);
begin
  case ModelAction of
    TModelAction.Create:    CreateRecord;
    TModelAction.Update:    LockForUpdate (SelectedID);
    TModelAction.Delete:    if RecordDeleteConfirmation (SelectedRecord.AsBriefStringWithFieldNames(FController.Model))
                               then Delete;
    TModelAction.Post:      Post;
    TModelAction.Cancel:    Cancel;
    TModelAction.Show:      ShowUIDialogue;
  end; //case
end;

function TCrudSqlRecordModel.GetCrudRecord: TSqlRecord;
begin
  result := FCrudRecord;
end;

function TCrudSqlRecordModel.GetCurrentOperation: TCrud;
begin
  if apsCreate.Active
     then result := TCrud.Create
     else if apsUpdate.Active
     then result := TCrud.Update
     else result := TCrud.Unknown;
end;

//procedure TBaseSqlRecordModel<T>.OnShowStatesUI (Sender: TObject);
//begin
//  TfrmAppStates.ShowForm;
//end;

procedure TCrudSqlRecordModel.ProcessUnlock (Task: TSingleRecordTask);
begin
  if (crudRecord <> nil) and (Task.QueryParameters.ID = crudRecord.IDValue) and (crudRecord.RecordClass = Task.SqlRecordClass)
    then
      begin
        // do this even if unlock fails
        crudRecord := nil;
        apsUpdate.Deactivate;
        if Task.Success
           then
             if TUIDialogueOption.CloseOnCancelUpdate in UIDialogueOptions
               then CloseUIDialogue
               else
           else Task.ShowError;
      end;
end;

procedure TCrudSqlRecordModel.PopulateUI;
begin
  inherited;
  if crudRecord.IsAssigned
     then PopulateUI (crudRecord)
     else PopulateUI (SelectedRecord);
end;

procedure TCrudSqlRecordModel.SetCrudRecord(const Value: TSqlRecord);
begin
  if FCrudRecord <> nil then FCrudRecord.Free;

  FCrudRecord := Value;
end;

procedure TCrudSqlRecordModel.DoPost;
begin
  if UseRecordWithDetails
    then
      begin
        crudRecord.ToRecordWithDetails(FCrudRecordWithDetails);
        Controller.RecordCreateOrUpdateWithDetails(FCrudRecordWithDetails, CurrentOperation, self);
        UseRecordWithDetails := false;
      end
    else
        Controller.RecordCreateOrUpdate(crudRecord.CreateCopy, nil, self);
end;

procedure TCrudSqlRecordModel.Post;
var
  error: string;
begin
  if (TUIDialogueOption.AutoUI in UIDialogueOptions)
     and not (TUIDialogueOption.CustomCollect in UIDialogueOptions)
     then CollectUIData (crudRecord);
  error := crudRecord.ValidateEx;
  if error <> '' then
     ShowErrorAndAbort (error);
  DoPost;
end;

{function TBaseModel.CollectFieldValueFrom (rec: TSqlRecord; ParentControl: TControl; const FieldName: string): boolean;
var
  Control: TControl;
  Model: TBaseModel;
begin
  for Control in ParentControl.Controls do
    begin
      if ControlNameMatches (Control.Name, rec.SQLTableName.AsString, FieldName) then
        begin
  //        if Control is Tdate
  //           then crudRecord.SetFieldVariant (FieldName, TCustomEdit(Control).Text);
          if Control is TCustomEdit
             then rec.SetFieldVariant (FieldName, TCustomEdit(Control).Text);
          if Control is TCheckBox
             then rec.SetFieldVariant (FieldName, TCheckBox(Control).IsChecked);
          exit (true);
        end;
      if CollectFieldValueFrom (rec, Control, FieldName) then exit (true);

      if dicControlModels.TryGetValue (Control, Model) then
         rec.SetFieldVariant (FieldName, Model.SelectedID);
    end;
  result := false;
end;

function TBaseModel.CollectFieldValueFrom(rec: TSqlRecord; ParentForm: TForm; const FieldName: string): boolean;
var
  Obj: TFmxObject;
begin
  for Obj in ParentForm.Children do
    begin
      if ControlNameMatches (Obj.Name, rec.SQLTableName.AsString, FieldName) then
        begin
  //        if Control is Tdate
  //           then crudRecord.SetFieldVariant (FieldName, TCustomEdit(Control).Text);
          if Obj is TCustomEdit
             then rec.SetFieldVariant (FieldName, TCustomEdit(Obj).Text);
          if Obj is TCheckBox
             then rec.SetFieldVariant (FieldName, TCheckBox(Obj).IsChecked);
          exit (true);
        end;
      if Obj is TControl then
        if CollectFieldValueFrom (rec, TControl(Obj), FieldName) then exit (true);
    end;
  result := false;
end;
}

//procedure TCrudSqlRecordModel.RecordSelectionChanged;
//begin
//  apsRecordSelected.SetActiveIf (GetSelectedRecord <> nil);
//  ApsSelectionChanging.CycleOnOff;
//  ApsSelectionChangedInstant.CycleOnOff;
//  ApsSelectionChangedDelayed.CycleOnOff;
//
//  if RecordStater <> nil
//     then RecordStater.Check (GetSelectedRecord);
//end;
//
//function TBaseSqlRecordModel.SelectedSqlRecord: TSqlRecord;
//begin
//  result := SelectedRecord;
//end;

{ TSingleRecordModel<T> }
(*
destructor TSingleRecordModel.Destroy;
begin
  FSqlRecord.Free;
  inherited;
end;

function TSingleRecordModel.GetSelectedRecord: TSqlRecord;
begin
  result := SqlRecord;
end;

function TSingleRecordModel.GetSqlRecord: TSqlRecord;
begin
  if FCrudRecord <> nil
    then result := FCrudRecord
    else result := FSqlRecord;
end;

function TSingleRecordModel<T>.GetSelectedRecord: T;
begin
  result := T(inherited SelectedRecord);
end;

function TSingleRecordModel<T>.GetSqlRecord: T;
begin
  result := T(inherited GetSqlRecord);
end;

procedure TSingleRecordModel.ObserveRecord (Task: TSingleRecordTask);
begin
//  if (Task.NamedInitiator <> self as IServerObserver) then exit;

  if Task.Success
    then case Task.Operation of
      TCRUD.Create:    begin
                         FLastCreatedID := Task.SqlRecord.IDValue;
                         apsCreate.Deactivate;
                         if StickyCreate
                            then CreateRecord
                            else begin
                                   crudRecord := nil;
                                   SqlRecord := Task.SqlRecord.CreateCopy;
                                   if TUIDialogueOption.CloseOnSuccess in UIDialogueOptions
                                      then CloseUIDialogue;
                                 end;
                       end;
      TCRUD.ReadLock : begin
                         crudRecord := Task.SqlRecord.CreateCopy;
                         apsUpdate.Activate;
                         ShowUIDialogue;
                       end;
      TCRUD.Update   : begin
                         SqlRecord := Task.SqlRecord.CreateCopy;
                         apsUpdate.Deactivate;
                         if TUIDialogueOption.CloseOnSuccess in UIDialogueOptions
                            then CloseUIDialogue;
                       end;
      TCRUD.Delete   : SqlRecord := nil;
      TCRUD.Unlock   : ProcessUnlock (Task);
    end // case
  else case Task.Operation of
      TCRUD.Unlock   : ProcessUnlock (Task); // treat as unlocked even if it returned false, it should mean it was not locked in first place though unclear why it would happen
    else Task.ShowError;
    end;//case
end;

procedure TSingleRecordModel.Reload;
begin
  if SelectedID.IsNotNull
     then SelectedID := SelectedID; // forces reload
end;

procedure TSingleRecordModel.DoSetSelectedID(const Value: TID);
begin
  if Value.IsNull
     then SqlRecord := nil
     else Controller.RecordRead (SqlRecordClass, Value, '', self);
end;

procedure TSingleRecordModel.SetSqlRecord(const Value: TSqlRecord);
begin
  FSqlRecord.Free;
  FSqlRecord := Value;

  RecordSelectionChanged;
end;

procedure TSingleRecordModel<T>.SetSqlRecord(const Value: T);
begin
  inherited SetSqlRecord (Value);
end;
*)

{ TStatedSqlRecord<T> }

procedure TSqlRecordStater.Check(rec: TSqlRecord);
begin
end;

constructor TSqlRecordStater.Create (AOwner: TSqlRecordModel);
begin
  inherited Create;
  Owner := AOwner;
  CreateStates (AOwner.States);
end;

procedure TSqlRecordStater.CreateStates(States: TAppStateDictionary);
begin
end;

function TSqlRecordStater.ForbiddenDetailStates(cl: TSqlRecordClass; Action: TModelAction): TAppStateArray;
begin
  result := [];
end;

function TSqlRecordStater.ForbiddenStates(Action: TModelAction): TAppStateArray;
begin
  result := [];
end;

function TSqlRecordStater.RequiredDetailStates(cl: TSqlRecordClass; Action: TModelAction): TAppStateArray;
begin
  result := [];
end;

function TSqlRecordStater.RequiredStates(Action: TModelAction): TAppStateArray;
begin
  result := [];
end;

{ TTableComboBoxModel }

class function TORMComboBoxModel.SelectedTable(ComboBox: TCustomComboBox; Model: TSqlModelEx): TSqlRecordClass;
begin
  result := model.Tables [integer (ComboBox.Items.Objects[ComboBox.ItemIndex])];
end;

class procedure TORMComboBoxModel.Setup(Items: TStrings; Model: TSqlModelEx);
var
  index: integer;
begin
  Items.Clear;
  var sl := TStringList.Create;
  sl.Sorted := true;
  index := 0;
  for var table in Model.Tables do
      begin
        sl.AddObject (Model.GetTableDisplayName (table), pointer(index));
        inc (index);
      end;
  Items.AddStrings(sl);
  sl.Free;
end;

class procedure TORMComboBoxModel.SetupFields(SqlRecordClass: TSqlRecordClass; Items: TStrings; Model: TSqlModelEx);
begin
  Items.Clear;
  for var fi in SqlRecordClass.FieldInfoList do
      Items.AddObject (fi.DisplayName, fi);
end;

class function TORMComboBoxModel.SelectedField (ComboBox: TCustomComboBox): TFieldInfo;
begin
  result := TFieldInfo(ComboBox.SelectedObject);
end;

end.

