unit DBStats.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, System.Rtti, FMX.Grid.Style, FMX.StdCtrls, FMX.Grid,
  FMX.Controls.Presentation, FMX.ScrollBox,
  SynCommons, mormot, SqlRecords,
  MormotTypes, MormotMods, ObservableController, Controller.Tasks;

type
  TfrmDBStats = class(TForm, IControllerTaskObserver, IServerObserver)
    stg: TStringGrid;
    stcName: TStringColumn;
    stcCount: TStringColumn;
    btnClose: TButton;
    StringColumn1: TStringColumn;
    StringColumn2: TStringColumn;
    StringColumn3: TStringColumn;
    StringColumn4: TStringColumn;
    procedure btnCloseClick(Sender: TObject);
  protected
    function GetName: string;
    procedure ObserveTask (Task: TObservableTask);
  public
    class procedure ShowInstance (Controller: TObservableController);
  end;

var
  frmDBStats: TfrmDBStats;

implementation

{$R *.fmx}

uses BusinessLogic;

{ TfrmDBStats }

procedure TfrmDBStats.btnCloseClick(Sender: TObject);
begin
  Close;
end;

function TfrmDBStats.GetName: string;
begin
  result := Name;
end;

procedure TfrmDBStats.ObserveTask(Task: TObservableTask);
var
  lookup: TLookup;

function AuthGroupName (ID: TID): string;
begin
  if lookup.IDIndexes.ContainsKey (ID)
    then result := lookup.Items[ID].AsString
    else result := '';
end;

begin
  if Task is TCustomDBStatsTask then
    begin
      var tsk := TCustomDBStatsTask(Task);
      stg.RowCount := length (tsk.Tables);
      lookup := Task.Controller.Lookups[Task.Controller.Model.AuthGroupClass];
      for var i := low (tsk.Tables) to high (tsk.Tables) do
        begin
          stg.Cells[0, i] := tsk.Tables[i].AsString;
          stg.Cells[1, i] := tsk.Counts[i].ToString;
          var ral := tsk.RequiredAuthLevels[i];

          stg.Cells[2, i] := AuthGroupName (ral[TBasicCrud.Create]);
          stg.Cells[3, i] := AuthGroupName (ral[TBasicCrud.Read]);
          stg.Cells[4, i] := AuthGroupName (ral[TBasicCrud.Update]);
          stg.Cells[5, i] := AuthGroupName (ral[TBasicCrud.Delete]);
        end;
    end;
end;

class procedure TfrmDBStats.ShowInstance(Controller: TObservableController);
begin
  if frmDBStats = nil then Application.CreateForm(TfrmDBStats, frmDBStats);
  frmDBStats.Show;
  Controller.DBStats(frmDBStats);
end;

end.
