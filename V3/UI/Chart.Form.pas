unit Chart.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMXTee.Engine, FMXTee.Series, FMXTee.Procs,
  FMXTee.Chart, FMX.Layouts, FMX.Controls.Presentation, FMX.StdCtrls, FMX.ListBox, System.Rtti, FMX.Grid.Style,
  FMX.ScrollBox, FMX.Grid, FMX.GridModel, FMX.Models, FMX.BaseModel, ObservableController, Common.Utils,
  UI.StateManagement, mormot, MormotMods, FMX.Common.Utils, FMX.DateTimeCtrls, math, SqlRecords, Generics.Collections,
  MormotAttributes;

type

  TfrmChart = class(TRestrictedSizeForm, IAppStateObserver)
    chart: TChart;
    layCustom: TLayout;
    cbxTable: TComboBox;
    Label1: TLabel;
    cbxXField: TComboBox;
    Label2: TLabel;
    cbxYField: TComboBox;
    Label3: TLabel;
    stg: TStringGrid;
    tieInterval: TTimeEdit;
    Label4: TLabel;
    cbxType: TComboBox;
    Label5: TLabel;
    spl: TSplitter;
    Series1: TLineSeries;
    procedure cbxTableChange(Sender: TObject);
    procedure tieIntervalChange(Sender: TObject);
    procedure cbxTypeChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure AppStateNotification (const State: TAppState);
    procedure UpdateChart;
    procedure DrawHistogram;
    procedure FShowPie (const Title, SeriesFieldName, ValueFieldName: string; Records: TSqlRecords); overload;
    procedure FShowPie (const Title: string; const Names: TArray<string>; const Values: TArray<double>); overload;
    procedure FShowTimeHistogram (const Title, IndexFieldName, XFieldName, YFieldName: string;
                                   Records: TSqlRecords; Interval: double);
    procedure FShowLine (const Title, SeriesFieldName, XFieldName, YFieldName: string;
                                   Records: TSqlRecords);
    procedure FShow (Settings: TChartAttributeSettings; Records: TSqlRecords);
    procedure HideCustomElements;
  public
    grm: TGridModel;
    Controller: TObservableController;
    class procedure ShowInstance (AController: TObservableController);
    class function ShowPie (Title: string; Names: TArray<string>; Values: TArray<double>): TfrmChart; overload;
//    class function ShowPie (Title: string; Records: TSqlRecords; const SeriesFieldName, ValueFieldName: string): TfrmChart; overload;
    class function ShowPie (const Title, SeriesFieldName, ValueFieldName: string; Records: TSqlRecords): TfrmChart; overload;
    class function Show (Settings: TChartAttributeSettings; Records: TSqlRecords): TfrmChart; overload;
    class function ShowTimeHistogram (const Title, IndexFieldName, XFieldName, YFieldName: string;
                                   Records: TSqlRecords; Interval: double): TfrmChart;

  end;

var
  frmChart: TfrmChart;

implementation

{$R *.fmx}

procedure TfrmChart.AppStateNotification(const State: TAppState);
begin
  if (State = grm.apsFetching) and not State.Active
    then UpdateChart;
end;

procedure TfrmChart.cbxTableChange(Sender: TObject);
begin
  grm.Free;
  var rcl := TORMComboBoxModel.SelectedTable(cbxTable, Controller.Model);
  stg.ClearColumns;
  if rcl = nil then exit;

  grm := TGridModel.Create(rcl, 'grmChart', Controller);
  grm.AllowedActions := NavigationModelActions;
  grm.Grid := stg;
  grm.Fetch;

  RegisterStateObserver(self, TAppStateAction.Notification, [grm.apsFetching], []);

  TORMComboBoxModel.SetupFields(rcl, cbxXField.Items, Controller.Model);
  TORMComboBoxModel.SetupFields(rcl, cbxYField.Items, Controller.Model);
end;

procedure TfrmChart.cbxTypeChange(Sender: TObject);
begin
  UpdateChart;
end;

class function TfrmChart.Show(Settings: TChartAttributeSettings; Records: TSqlRecords): TfrmChart;
begin
  Application.CreateForm (TfrmChart, result);
  result.FShow(Settings, Records);
end;

class procedure TfrmChart.ShowInstance(AController: TObservableController);
begin
  if frmChart = nil then
    begin
      Application.CreateForm (TfrmChart, frmChart);
      frmChart.Controller := AController;
      TORMComboBoxModel.Setup(frmChart.cbxTable.Items, AController.Model);
    end;
  frmChart.Show;
end;

class function TfrmChart.ShowPie (const Title, SeriesFieldName, ValueFieldName: string; Records: TSqlRecords): TfrmChart;
begin
  Application.CreateForm (TfrmChart, result);
  result.FShowPie (Title, SeriesFieldName, ValueFieldName, Records);
end;

class function TfrmChart.ShowPie (Title: string; Names: TArray<string>; Values: TArray<double>): TfrmChart;
begin
  Application.CreateForm (TfrmChart, result);
  result.FShowPie (Title, Names, Values);
end;

class function TfrmChart.ShowTimeHistogram (const Title, IndexFieldName, XFieldName, YFieldName: string;
                                             Records: TSqlRecords; Interval: double): TfrmChart;
begin
  Application.CreateForm (TfrmChart, result);
  result.FShowTimeHistogram(Title, IndexFieldName, XFieldName, YFieldName, Records, Interval);
end;

procedure TfrmChart.HideCustomElements;
begin
  layCustom.Visible := false;
  stg.Visible := false;
  spl.Visible := false;
end;

procedure TfrmChart.FShow(Settings: TChartAttributeSettings; Records: TSqlRecords);
begin
  case Settings.ChartType of
    TChartType.Line: FShowLine(Settings.Title, Settings.SeriesFieldName, Settings.XFieldName, Settings.YFieldName, Records);
    TChartType.Bar: FShowTimeHistogram (Settings.Title, Settings.XFieldName, Settings.XFieldName, Settings.YFieldName,
                                        records, Settings.Interval);
    TChartType.Pie: FShowPie (Settings.Title, Settings.SeriesFieldName, Settings.YFieldName, Records);
  end;
//  chart.Title.Text.Text := Title;
//  HideCustomElements;
end;

procedure TfrmChart.FShowLine(const Title, SeriesFieldName, XFieldName, YFieldName: string; Records: TSqlRecords);
var
  rec: TSQLRecord;
  X, Y: double;
  series: TLineSeries;
  SeriesName: string;
  dicSeries: TDictionary <string, TLineSeries>;
begin
  chart.Title.Text.Text := Title;
  dicSeries := TDictionary <string, TLineSeries>.Create;

  HideCustomElements;
  chart.SeriesList.Clear;

  for rec in Records.EnumByField(XFieldName) do
    begin
      SeriesName := rec.GetFieldStringValue(SeriesFieldName);
      if not dicSeries.TryGetValue (SeriesName, series) then
        begin
          series := TLineSeries.Create (chart);
          series.Marks.Visible := false;
          Series.XValues.DateTime := true;
          Series.ValueFormat := 'dd/mm/yy hh:nn';
          chart.AddSeries(series);
          series.Title := SeriesName;
          dicSeries.Add(SeriesName, series);
        end;
      X := rec.GetFieldVariant (XFieldName);
      Y := rec.GetFieldVariant(YFieldName);
      if (series.Count > 0) and (series.YValues[series.Count - 1] = Y)
        then
          series.XValues[series.Count - 1] := X
        else
          begin
            if (series.Count > 0)
               then series.AddXY(X - DelphiSecond, series.YValues[series.Count - 1]);
            series.AddXY(X, Y);
            series.AddXY(X, Y);
          end;
    end;

  chart.Legend.Visible := dicSeries.Count > 1;
  chart.Legend.LegendStyle := TLegendStyle.lsSeries;
  chart.Legend.CheckBoxes := true;
  chart.BottomAxis.DateTimeFormat := 'dd/mm/yy hh:nn';
  dicSeries.Free;
  Show;
end;

procedure TfrmChart.FShowPie(const Title, SeriesFieldName, ValueFieldName: string; Records: TSqlRecords);
var
  dic: TDictionary <string, double>;
  v: double;
  sName: string;
  Names: TArray<string>;
  Values: TArray<double>;
begin
  dic := TDictionary <string, double>.Create;

  for var rec in Records.Records do
    begin
      sName := rec.GetFieldVariantDisplayValue(SeriesFieldName);
      if not dic.TryGetValue (sName, v) then v := 0;
      if ValueFieldName.IsEmpty
         then v := v + 1
         else  v := v + rec.GetFieldVariant(ValueFieldName);
      dic.AddOrSetValue (sName, v);
    end;

  SetLength(Names, dic.Count);
  SetLength(Values, dic.Count);
  var i := 0;
  for var pair in dic do
    begin
      Names[i] := pair.Key;
      Values[i] := pair.Value;
      inc (i);
    end;
  dic.Free;

  FShowPie (Title, Names, Values);
end;

procedure TfrmChart.FShowPie (const Title: string; const Names: TArray<string>; const Values: TArray<double>);
var
  series: TPieSeries;
  i: integer;
begin
  chart.Title.Text.Text := Title;
  HideCustomElements;
  chart.SeriesList.Clear;
  series := TPieSeries.Create (chart);
  chart.AddSeries(series);
  chart.Legend.Visible := true;
  chart.Legend.LegendStyle := TLegendStyle.lsValues;
  for i := low (Names) to  High (Names) do
     series.AddPie(Values[i], Names[i]);
  Show;
end;

procedure TfrmChart.FShowTimeHistogram (const Title, IndexFieldName, XFieldName, YFieldName: string;
                                   Records: TSqlRecords; Interval: double);
var
  iRecord, ValueCount, i: integer;
  rec: TSQLRecord;
  FirstX, LastX, X: TDateTime;
  YValues: TArray <double>;
  series: TBarSeries;
begin
  chart.Title.Text.Text := Title;

  HideCustomElements;
  chart.Legend.Visible := false;
  chart.SeriesList.Clear;
  series := TBarSeries.Create (chart);
  series.Marks.Visible := false;
  chart.AddSeries(series);

  FirstX := Records.Indexes[IndexFieldName].Records[0, true].GetFieldVariant(XFieldName);
  LastX  := Records.Indexes[IndexFieldName].Records[Records.Count - 1, true].GetFieldVariant(XFieldName);

  FirstX := trunc (FirstX / Interval) * Interval;
  ValueCount := ceil ((LastX - FirstX) / Interval);
  SetLength (YValues, ValueCount);

  for i := 0 to ValueCount - 1 do YValues[i] := 0;

  //i := 0;
  for rec in Records.EnumByField(IndexFieldName) do
    begin
      X := rec.GetFieldVariant (XFieldName);
      if X > LastX
         then raise Exception.Create('wtf');

      iRecord := trunc ((X - FirstX) / Interval);
      if YFieldName.IsEmpty
         then YValues[iRecord] := YValues[iRecord] + 1
         else YValues[iRecord] := YValues[iRecord] + rec.GetFieldVariant (YFieldName);

      //inc (i);
    end;

  for i := 0 to ValueCount - 1 do
    Series.AddXY (FirstX + i * tieInterval.Time + tieInterval.Time / 2, YValues[i]);

  Series.XValues.DateTime := true;
  Show;
end;

procedure TfrmChart.tieIntervalChange(Sender: TObject);
begin
  UpdateChart;
end;

procedure TfrmChart.DrawHistogram;
var
  fiX, fiY: TFieldInfo;
  YValues: TArray <double>;
  Series: TChartSeries;
  index, i, ValueCount: integer;
  FirstX, LastX, X: TDateTime;
begin
  Series := chart.SeriesList[0];
  Series.Clear;

  fiX := TORMComboBoxModel.SelectedField (cbxXField);
  fiY := TORMComboBoxModel.SelectedField (cbxYField);

  grm.SetSort (fiX.Name, true);

  LastX  := grm.RecordInRow(grm.Records.Count - 1).GetFieldVariant (fiX.Name);
  FirstX := grm.RecordInRow(0).GetFieldVariant (fiX.Name);

  FirstX := trunc (FirstX / tieInterval.Time) * tieInterval.Time;
  ValueCount := ceil ((LastX - FirstX) / tieInterval.Time);
  SetLength (YValues, ValueCount);

  for i := 0 to ValueCount - 1 do YValues[i] := 0;

  for i := 0 to grm.Records.Count - 1 do
    begin
      X := grm.RecordInRow(i).GetFieldVariant (fiX.Name);
      if X > LastX
         then raise Exception.Create('wtf');

      index := trunc ((X - FirstX) / tieInterval.Time);
      if fiY = nil
         then YValues[index] := YValues[index] + 1
         else YValues[index] := YValues[index] + grm.RecordInRow(i).GetFieldVariant (fiY.Name);
    end;

  for i := 0 to ValueCount - 1 do
    Series.AddXY (FirstX + i * tieInterval.Time + tieInterval.Time / 2, YValues[i]);

  Series.XValues.DateTime := true;
end;

procedure TfrmChart.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not layCustom.Visible then Action := TCloseAction.caFree;
end;

procedure TfrmChart.UpdateChart;
begin
//  if (cbxYField.ItemIndex = -1) then exit;
  chart.Title.Text.Text := cbxYField.Text;

  case cbxType.ItemIndex of
    0: DrawHistogram;
    end;
end;

end.
