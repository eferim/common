﻿unit CommonMethods;

interface

uses
  System.Types, System.UITypes, System.Classes,
  FMX.Forms, FMX.Controls, FMX.StdCtrls, FMX.Types, FMX.Edit, FMX.ListBox, FMX.DateTimeCtrls, FMX.Layouts,
  FMX.Memo, FMX.Memo.Types, winapi.messages, windows,
  ObservableController, MormotMods, UI.StateManagement, FMX.Models, math, FMX.ComboEdit, FMX.Grid,
  FMX.GridModel, FMX.GridLookupEdit, Generics.Collections, FMX.Common.Utils,
  mormot, SynCommons, Transliteration, FMX.Text,
  Client.MormotMods, MormotTypes, Common.Utils, LocalSettings, System.Contnrs, FMX.Dialogs, FMX.BaseModel;

const
  // NB min podrzana rezolucija resolution treba da bude 1366×768
//  MAND = 'mandatory';
  NEXT = 'Next';
  SAVE = 'Save';

type

  TBaseObserverForm = class(TModelForm, IServerObserver, IServerStateObserver, IAppStateObserver)
  private
    class var FController: TObservableController;
  protected
    FWndProc: TWndMethod;
    function GetName: string;
    procedure ObserveState(State: TServerState; var Outcome: TOutcome); virtual;
    procedure AppStateNotification(const State: TAppState); virtual;
    procedure WMNCHitTest(var Msg: TWMNCHitTest);
    procedure CreateHandle; override;
  public
    class var LocalSettings: TMormotClientSettings;
    procedure AfterConstruction; override;
    destructor Destroy; override;

    function CreateGridLookupEdit <T: TSqlRecord> (edt: TCustomEdit; AMasterSqlRecordClass: TSQLRecordClass;
                                                   AMasterForeignKeyName: string = ''): TGridLookupEdit<T>;
    function CreateGridModel <T: TSqlRecord>(grd: TStringGrid; layCU: TLayout; ButtonsContainer: TControl): TMormotGridModel<T>;
    function CreateDetailGridModel <T: TSqlRecord> (grd: TStringGrid; layCU: TLayout; ButtonsContainer: TControl;
                                                    AMaster: TSqlRecordModel; ADetailLinkKeyName: RawUTF8 = ''): TMormotGridModel<T>;

    class property Controller: TObservableController read FController write FController;
  end;

  TBaseInputForm = class(TBaseObserverForm)
  private
    lay : TLayout;
    cp : TCalloutPanel;
    spl : TSplitter;
    memo : TMemo;
    FLinkedEdit: TCustomEdit;
    EventsAssigned, FSidePanelVisible : boolean;
    FMainLayout : TLayout;
    procedure OnEllipsesEditMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure SetActiveControlGroup(const Value: integer);
    procedure AssignEvents;
  protected
    apsCreate, apsUpdate, apsCU: TAppState;
    FActiveControlGroup: integer; // if UI is split e.g. onto separate tab sheets
    dicOriginalComboBoxOnChange: TDictionary <TComboBox, TNotifyEvent>;
    procedure SetInitialFocus; virtual;
{$region 'Side panel'}
    procedure PrepareDetailView; virtual;
    procedure ShowDetailSidepanel(AMainLayout : TLayout; AWidth : integer); virtual;
    procedure HideDetailSidepanel; virtual;
    procedure ToggleDetailSidepanel(AWidth : integer; MainLayout : TLayout; ALinkedEdit : TEdit);
    procedure SetCalloutOffset(Sender : TControl);
    property LinkedEdit : TCustomEdit read FLinkedEdit write FLinkedEdit;
    procedure MemoEnter(Sender : TObject);
    procedure MemoTyping(Sender : TObject);
{$endregion}
    procedure ClearAllFields({frm : TForm}); virtual;
    procedure Resize; override;
    procedure DoShow; override;
    procedure DoHide; override;
  public
    procedure KeyUp(var Key: Word; var KeyChar: WideChar; Shift: TShiftState); override;
//    procedure ShowUpdate; virtual;
//    procedure ShowNew; virtual;
    procedure AfterConstruction; override;
    procedure SetModel (Model: TSqlRecordModel); override;
    destructor Destroy; override;
    property ActiveControlGroup: integer read FActiveControlGroup write SetActiveControlGroup;
  end;

implementation
uses
  System.Rtti,
  System.TypInfo,
  System.SysUtils,
  FMX.Objects,
  BusinessLogic,
  FMX.Platform.Win, FMX.Platform, Winapi.CommCtrl;

{$region 'TBaseInputForm'}
procedure TBaseInputForm.SetActiveControlGroup(const Value: integer);
begin
  FActiveControlGroup := Value;
  //AreMandatoryFieldsPopulated;
end;

procedure TBaseInputForm.SetCalloutOffset(Sender: TControl);
begin
  cp.CalloutOffset := Sender.LocalToAbsolute(TPointF.Zero).Y;
end;

procedure TBaseInputForm.SetModel(Model: TSqlRecordModel);
begin
  if Model = FModel then exit;
  if FModel <> nil then FModel.DetachButtons(FindComponent ('gplPostCancel') as TControl);

  inherited;
  apsCreate := Model.apsCreate;
  apsUpdate := Model.apsUpdate;
  apsCU := Model.apsCU;
  model.AttachButtons(FindComponent ('gplPostCancel') as TControl);
end;

//procedure TBaseInputForm.SetRequiredFields(Controls: TArray<TControl>);
//begin
//  for var Control in Controls do Control.TagString := MAND;
//end;

procedure TBaseInputForm.HideDetailSidepanel;
begin
  if not assigned(FMainLayout) then Exit;

  width := Width - Round(lay.Width) - round(spl.Width);
  lay.Visible := False;
  spl.Visible := false;

  FMainLayout.Align := TAlignLayout.Client;
  FSidePanelVisible := false;
end;

procedure TBaseInputForm.KeyUp;
begin
  inherited;

  if ( (Key = ord ('C')) and (Shift = [ssAlt, ssCtrl]) )
     or
     ( (Key = VK_INSERT) and (Shift = [ssAlt, ssCtrl]) )
     then if apsCU.Active
          then FModel.crudRecord.CopyToClipboard (FModel.Controller.Model, false)
          else FModel.SelectedRecord.CopyToClipboard (FModel.Controller.Model, false);

  if ( (Key = ord ('V')) and (Shift = [ssAlt, ssCtrl]) )
     or
     ( (Key = VK_INSERT) and (Shift = [ssAlt, ssShift]) )
     then if apsCU.Active
          then begin
                 FModel.crudRecord.PasteFromClipboard (true);
                 FModel.PopulateUI(FModel.crudRecord, self, true);
                 FModel.UpdateMandatoryStatus;
               end
          else; // dont paste if not in edit mode... or ask user if he wants to edit? ;

//  if (Key = ord ('C')) and (Shift = [ssAlt, ssCtrl])
//     then if apsCU.Active
//          then FModel.crudRecord.CopyToClipboard(false)
//          else FModel.SelectedSqlRecord.CopyToClipboard(false);

  {$IFDEF DEBUG}
  // load sample record data
  if (Key >= ord ('1')) and (Key <= ord ('9'))
    and (Shift = [ssAlt, ssCtrl]) and (apsCreate.Active)
     then begin
            var Index := Key - ord ('1'); // 1 - based
            if FModel.SqlRecordClass.CrudClass.SampleRecords.Count <= Index
               then ShowErrorAndAbort('Ne postoji test podatak sa ovim indeksom');
            FModel.PopulateUI(FModel.SqlRecordClass.CrudClass.SampleRecords[Index], self, true);
            FModel.UpdateMandatoryStatus;
          end;

  if (Shift = [ssAlt, ssCtrl]) and (Key = ord ('S'))
     then
       begin
         var rec := FModel.SqlRecordClass.Create;
         FModel.CollectValuesFrom(rec, self);
         FModel.SqlRecordClass.CrudClass.SampleRecords.Add (rec);
         FModel.SqlRecordClass.SampleRecords.Save;
         ShowMessage ('Dodato sa indeksom #' + FModel.SqlRecordClass.SampleRecords.Count.ToString);
       end;

  // generate random test data
  if (Key = ord ('T')) and (Shift = [ssAlt, ssCtrl]) and (apsCreate.Active)
     then FModel.UIFillTestData;

  // SWITCH FROM ON TOP TO MODAL WITH CTRL F12
  if (Key = vkF12) and (Shift = [ssCtrl])
     then FormStyle := TFormStyle.Normal;

  if (Key = ord ('I')) and (Shift = [ssShift, ssCtrl])
     then if FModel.CrudRecord.IsAssigned
             then ShowMessage ('ID: ' + FModel.CrudRecord.IDValue.ToString)
     else if FModel.SelectedRecord.IsAssigned
             then ShowMessage ('ID: ' + FModel.SelectedRecord.IDValue.ToString);
  {$ENDIF}

{TODO -oOwner -cGeneral : Make translit manager to allow to attach to every edit independently of this form class?}
  //Translit
  if ( (Key = ord ('C')) or (Key = ord ('L')) )
     and (Shift = [ssShift, ssCtrl]) and (apsCU.Active)
     then begin
            var obj := Screen.ActiveForm.Focused.GetObject;

            if obj is TCustomEdit
              then
                begin
                  var ced := TCustomEdit(obj);
                  var text := ced.Text;
                  if ced.SelLength > 0
                    then
                      begin
                        var selText := Copy(ced.text, ced.selStart + 1, ced.selLength);
                        var selStart := ced.SelStart;
                        if (Key = ord('C'))
                           then selText := TSimpleTranslit.ToCyrillic(selText)
                           else selText := TSimpleTranslit.ToLatin(selText);
                        Delete(text, selStart + 1, ced.selLength);
                        Insert(selText, text, ced.selStart + 1);
                        ced.Text := text;
                        ced.SelStart := selStart; // Keep selection at same place
                        ced.SelLength := Length(selText);
                      end
                    else
                      begin
                        var pos := ced.CaretPosition;
                        if (Key = ord ('C'))
                          then text := TSimpleTranslit.ToCyrillic(text)
                          else text := TSimpleTranslit.ToLatin(text);
                        ced.Text := text;
                        ced.CaretPosition := pos;
                      end;
                end
              else if obj is TCustomMemo then
                begin
                  var mem := TCustomMemo(obj);
                  var text := mem.Text;
                  if mem.SelLength > 0
                      then
                        begin
                          var selText := Copy(mem.text, mem.selStart + 1, mem.selLength);
                          var selStart := mem.SelStart;
                          if (Key = ord('C'))
                             then selText := TSimpleTranslit.ToCyrillic(selText)
                             else selText := TSimpleTranslit.ToLatin(selText);

                          mem.DeleteFrom(mem.TextPosToPos(selStart), mem.SelLength, [TDeleteOption.CanUndo]);
                          mem.InsertAfter(mem.TextPosToPos(selStart), selText, [TInsertOption.CanUndo]);

                          Delete(text, selStart + 1, mem.selLength);
                          Insert(selText, text, mem.selStart + 1);
                          mem.Text := text;
                          mem.SelStart := selStart; // Keep selection at same place
                          mem.SelLength := Length(selText);
                        end
                      else
                        begin
                          var pos := mem.CaretPosition;
                          if (Key = ord ('C'))
                             then text := TSimpleTranslit.ToCyrillic(text)
                             else text := TSimpleTranslit.ToLatin(text);
                          //mem.Text := text;

                          mem.DeleteFrom(mem.TextPosToPos(0), mem.Text.Length, [TDeleteOption.CanUndo]);
                          mem.InsertAfter(mem.TextPosToPos(0), Text, [TInsertOption.CanUndo]);

                          mem.CaretPosition := pos;
                        end;
                end
          end;
end;

procedure TBaseInputForm.MemoEnter(Sender: TObject);
begin
  if Assigned(LinkedEdit) then
  begin
    memo.Lines.Text := LinkedEdit.Text;
    memo.GoToTextEnd;
  end;
end;

procedure TBaseInputForm.MemoTyping(Sender: TObject);
begin
  LinkedEdit.Text := memo.Lines.Text;
end;

procedure TBaseInputForm.ShowDetailSidepanel(AMainLayout : TLayout; AWidth : integer);
begin
  FMainLayout := AMainLayout;
  FMainLayout.Align := TAlignLayout.Left;

  PrepareDetailView;
  spl.Visible := true;
  lay.Visible := true;

  Width := width + round(AWidth);
  if Assigned(FLinkedEdit) then
    memo.Lines.Text := FLinkedEdit.Text;
  memo.SetFocus;
  FSidePanelVisible := true;
end;

//procedure TBaseInputForm.ShowMandatoryFieldExclamation(obj : TControl; ShowExclamation : boolean = true);
//begin
//  if not Assigned(obj) or not FModel.ShowMandatoryIndicators then exit;
//  for var chld in obj.Children do
//    if chld is TLabel then
//      (chld as TLabel).SetWarningState(ShowExclamation);
//end;

procedure TBaseInputForm.AfterConstruction;
begin
  inherited;
  dicOriginalComboBoxOnChange := TDictionary <TComboBox, TNotifyEvent>.Create;
  FActiveControlGroup := -1;
  if LocalSettings.FormsStayOntop
     then FormStyle := TFormStyle.StayOnTop;
end;

procedure TBaseInputForm.OnEllipsesEditMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  var el := TEllipsesEditButton (Sender);
  var par := el.Parent;
  while (par<>nil) and not (par is TLayout) do par := par.Parent;
  if par = nil then raise Exception.Create('TEllipsesEditButton needs TLayout as one of parents');

  ToggleDetailSidepanel(Width, par as TLayout, TEdit(el.Parent.Parent)); // wtf zasto je 2 puta parent...
end;
(*
function TBaseInputForm.AreMandatoryFieldsPopulated: boolean;
begin
  result := true;
  for var i := 0 to ComponentCount - 1 do
  begin
    if not (Components[i] is TControl) then continue;
    var ctrl := Components[i] as TControl;
    if (ActiveControlGroup > -1) and (ctrl.Tag <> ActiveControlGroup) then continue;
    if fmodel.SqlRecordClass.IsMandatoryField (copy (ctrl.Name, 4)) then
    //if (ctrl.TagString = MAND) then
    begin
      if ctrl is TCustomEdit then
        if (ctrl as TCustomEdit).Text = '' then
        begin
          Result := false;
          ShowMandatoryFieldExclamation(ctrl);
        end
        else
          ShowMandatoryFieldExclamation(ctrl, false);
      if ctrl is TComboBox then
        if (ctrl as TComboBox).ItemIndex < 1 then
        begin
          Result := false;
          ShowMandatoryFieldExclamation(ctrl);
        end
        else
          ShowMandatoryFieldExclamation(ctrl, false);
      if ctrl is TDateEdit then
        if (ctrl as TDateEdit).IsEmpty then
        begin
          Result := false;
          ShowMandatoryFieldExclamation(ctrl);
        end
        else
          ShowMandatoryFieldExclamation(ctrl, false);
    end;
  end;
end;

procedure TBaseInputForm.ChangeTrackingHandler(Sender: TObject);
var
  Event: TNotifyEvent;
begin
  AreMandatoryFieldsPopulated;
  if Sender is TComboBox then
     if dicOriginalComboBoxOnChange.TryGetValue(Sender as TComboBox, Event)
        and Assigned(Event) then Event (Sender as TComboBox);
end;
*)

procedure TBaseInputForm.ClearAllFields;
begin
  for var i := 0 to ComponentCount - 1 do
  begin
    if Components[i] is TEdit then
      (Components[i] as TEdit).Text := '';
    if Components[i] is TComboBox then
      (Components[i] as TComboBox).ItemIndex := 0;
    if Components[i] is TDateEdit then
      (Components[i] as TDateEdit).Date := 0;
  end;
end;

destructor TBaseInputForm.Destroy;
begin
  dicOriginalComboBoxOnChange.Free;
  inherited;
end;

procedure TBaseInputForm.AssignEvents;
begin
  AutoTabOrder (self);

  for var i := 0 to ComponentCount - 1 do
    begin
//      if Components[i] is TEdit then
//        (Components[i] as TEdit).OnChangeTracking := ChangeTrackingHandler
//      else if Components[i] is TComboBox then
//        begin
//          var cbx := Components[i] as TComboBox;
//          dicOriginalComboBoxOnChange.Add (cbx, cbx.OnChange);
//          cbx.OnChange := ChangeTrackingHandler;
//        end
//      else if Components[i] is TDateEdit then
//        (Components[i] as TDateEdit).OnChange := ChangeTrackingHandler;

      if Components[i] is TEllipsesEditButton then
        TEllipsesEditButton (Components[i]).OnMouseDown := OnEllipsesEditMouseDown;
    end;
end;

procedure TBaseInputForm.DoHide;
begin
  inherited;
  if FSidePanelVisible then HideDetailSidepanel;
end;

procedure TBaseInputForm.SetInitialFocus;
begin
  FocusControlOrChild(self);
end;

procedure TBaseInputForm.DoShow;
begin
  if not EventsAssigned then
    begin
      EventsAssigned := true;
      AssignEvents;
    end;
  //AreMandatoryFieldsPopulated;
  inherited;
  if (ActiveControl <> nil)
    then
      if not ActiveControl.IsFocused and ActiveControl.CanFocus
        then ActiveControl.SetFocus
        else
    else SetInitialFocus;


end;

//procedure TBaseInputForm.ShowNew;
//begin
//  FNew := true;
//  ClearAllFields(self);
//  AreMandatoryFieldsPopulated;
//  show;
//end;
//
//procedure TBaseInputForm.ShowUpdate;
//begin
//  FNew := false;
//  AreMandatoryFieldsPopulated;
//  Show;
//end;

procedure TBaseInputForm.ToggleDetailSidepanel(AWidth: integer; MainLayout : TLayout; ALinkedEdit : TEdit);
begin
  if not FSidePanelVisible then
  begin
    LinkedEdit := ALinkedEdit;
    ShowDetailSidepanel(MainLayout, Width);
    SetCalloutOffset(ALinkedEdit);
  end
  else
  begin
    if LinkedEdit = ALinkedEdit then
      HideDetailSidepanel
    else
    begin
      LinkedEdit := ALinkedEdit;
      SetCalloutOffset(ALinkedEdit);
      memo.Text := ALinkedEdit.Text;
    end;
  end;
end;

procedure TBaseInputForm.PrepareDetailView;
begin
  if not Assigned(lay) then
  begin
    spl := TSplitter.Create(self);
    spl.Parent := self;
    spl.Align := TAlignLayout.left;
    spl.Width := 3;
    spl.Visible := false;

    lay := TLayout.Create(self);
    lay.Parent := self;
    lay.Align := TAlignLayout.client;
    cp := TCalloutPanel.Create(lay);
    cp.Parent := lay;
    cp.Align := TAlignLayout.Client;
    cp.CalloutPosition := TCalloutPosition.left;
    memo := TMemo.Create(lay);
    memo.Parent := cp;
    memo.Align := TAlignLayout.Client;
    memo.TextSettings.WordWrap := true;
    memo.OnEnter := MemoEnter;
    memo.OnChangeTracking := MemoTyping;

    lay.Visible := false;
    with memo.Margins do
    begin
      Left := 3;
      Right := 3;
      top := 3;
      Bottom := 3;
    end;
  end;
end;

//procedure TBaseInputForm.ResetRequiredFields;
//begin
//  for var i := 0 to ComponentCount - 1 do
//    if Components[i] is TControl then
//    begin
//      var c := (Components[i] as TControl);
//      if c.TagString = MAND then
//        c.TagString := '';
//    end;
//end;

procedure TBaseInputForm.Resize;
var
  i : integer;
begin
  inherited;

  var minHeight := 0;

  for i := 0 to ComponentCount - 1 do
    if Components[i] is TControl then
      if TControl (Components[i]).BoundsRect.Bottom < Screen.Height*0.9
         then minHeight := max (minHeight, round(TControl (Components[i]).BoundsRect.Bottom));

  if Height <> max (minHeight, Height) then
    begin
      Height := max (minHeight, Height);
    end;
end;

{$endregion}
{ TBaseObserverForm }

procedure TBaseObserverForm.AfterConstruction;
begin
  inherited;

  Controller.Subscribe(self);
  Controller.PersistentUserSettings.ReadFormPosition(self);
end;

procedure TBaseObserverForm.AppStateNotification(const State: TAppState);
begin

end;

function TBaseObserverForm.CreateDetailGridModel<T>(grd: TStringGrid; layCU: TLayout; ButtonsContainer: TControl;
                                                    AMaster: TSqlRecordModel; ADetailLinkKeyName: RawUTF8 = ''): TMormotGridModel<T>;
begin
  result := TMormotGridModel<T>.Create ('', Controller, nil, AMaster, ADetailLinkKeyName);
  result.Grid := grd;
  result.UILayout := layCU;
  if ButtonsContainer<>nil then result.AttachButtons(ButtonsContainer);
end;

function TBaseObserverForm.CreateGridLookupEdit <T> (edt: TCustomEdit; AMasterSqlRecordClass: TSQLRecordClass;
                                                     AMasterForeignKeyName: string = ''): TGridLookupEdit<T>;
begin
  result := TGridLookupEdit<T>.Create (edt, Controller, AMasterSqlRecordClass, AMasterForeignKeyName);
//  result.ImageList := BL.ActiveImageList; should not be needed because of OnFormShow handling, ?
end;

function TBaseObserverForm.CreateGridModel <T>(grd: TStringGrid; layCU: TLayout; ButtonsContainer: TControl): TMormotGridModel<T>;
begin
  result := TMormotGridModel<T>.Create ('', Controller);
  result.Grid := grd;
  result.UILayout := layCU;
  if ButtonsContainer<>nil then result.AttachButtons(ButtonsContainer);
end;

function TBaseObserverForm.GetName: string;
begin
  result := Name;
end;

procedure TBaseObserverForm.ObserveState(State: TServerState; var Outcome: TOutcome);
begin
  if State = TServerState.BeforeSaveSettings then
    begin
      Controller.PersistentUserSettings.WriteFormPosition(self);
      hide;
    end;
  if (State = TServerState.AfterLogin) and Outcome.Success then
    begin
      Controller.PersistentUserSettings.ReadFormPosition(self);
    end;

  if State = TServerState.BeforeControllerDestroy then
    begin
      FController := nil; // no need to unsub
    end;
end;

procedure TBaseObserverForm.WMNCHitTest(var Msg: TWMNCHitTest);
const
  EdgeSize = 5;  // Adjust this value if needed
begin
  inherited;

  // Check if the hit test is in the client area
  if Msg.Result = HTCLIENT then
  begin
    // Get the position of the cursor relative to the form
//    var X := Msg.XPos - Left;
    var Y := Msg.YPos - Top;

    // Determine if the cursor is within the top resize area
    if Y < EdgeSize then
      Msg.Result := HTTOP
//    else if Y >= Height - EdgeSize then
//      Msg.Result := HTBOTTOM
//    else if X < EdgeSize then
//      Msg.Result := HTLEFT
//    else if X >= Width - EdgeSize then
//      Msg.Result := HTRIGHT;
  end;
end;

function GET_X_LPARAM(lParam: LPARAM): Integer;
begin
  Result := Smallint(LOWORD(lParam));
end;

function GET_Y_LPARAM(lParam: LPARAM): Integer;
begin
  Result := Smallint(HIWORD(lParam));
end;

function MySubclassProc(hWnd: HWND; uMsg: UINT; wParam: WPARAM; lParam: LPARAM;
  uIdSubclass: UINT_PTR; dwRefData: DWORD_PTR): LRESULT; stdcall;
begin
  case uMsg of
    WM_NCHitTest:
    begin
      var form := TBaseObserverForm(dwRefData);
      Result := DefSubclassProc(hWnd, uMsg, wParam, lParam);

      if (Result = HTCAPTION) and (GET_Y_LPARAM(lParam) - form.Top < 5) then
         Result := HTTOP;
      exit;
    end;
    WM_NCDESTROY:
      RemoveWindowSubclass(hWnd, @MySubclassProc, uIdSubclass);
  end;
  Result := DefSubclassProc(hWnd, uMsg, wParam, lParam);
end;

procedure TBaseObserverForm.CreateHandle;
begin
  inherited;
  SetWindowSubclass(FormToHWND(Self), @MySubclassProc, 1, DWORD_PTR(Self));
end;

destructor TBaseObserverForm.Destroy;
begin
  inherited;

  if Assigned (Controller)
     then Controller.Unsubscribe(self);

  exit;
  var s := Name;
  try
    inherited;
  except on e: exception do
    begin
      ShowErrorDialogue (s+'.Destroy crashed');
      raise e;
    end;
  end;
end;

procedure InitStandardClasses;
var
  ICC: TInitCommonControlsEx;
begin
  ICC.dwSize := SizeOf(TInitCommonControlsEx);
  ICC.dwICC := ICC_STANDARD_CLASSES;
  InitCommonControlsEx(ICC);
end;

initialization
  InitStandardClasses;

end.
