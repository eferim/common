﻿unit Translator2Classes;

{ Purpose:
  I simply did not like the default way to handle translation. I do not claim this is better
  but to me it feels easier to use and has certain advantages. I tried to make it minimally intrusive
  and require the least amount of effort to include translation into an existing project.

  Usage: (also, see demo)
  1) download files and add path to them to Delphi (in project settings or in tools/options for all projects)
  2) include this unit in your project

  Then, in your project's code, do things in this order:
  3) add (or "register") all string arrays with Translator.AddStringArray
     these arrays are the intended way to translate strings typically used in runtime to produce content
     such as various dialogue prompts or status labels.
     Arrays can be indexed by integer or an enumerated type.
     This step has to be done every time program starts because Translator needs to have the memory address
     of the strings, and it has to be done before loading the translation data.
  4) load the translations by calling Translator.LoadFromResource
     (this will not do anything until initial configuration - explained below - is completed once)
  5) call Translator.OpenUI from your program whenever there is a need to update
     translatable elements or provide new translation values.
     This only works if Translation_UI is defined in project options, that way you can choose
     to only include the UI form in debug version and handle translation yourself or in the
     release version as well and let users provide translations. Merging a user translation with
     the main one however remains to be coded.
  6) save the translation definition from the UI. After saving for the first time, include the
     generated file (default is project_name.trn) into project resources and name the resource "Translation"
  8) to change current program language (i.e. to translate to another language) simply set
     Active to true and ActiveLanguage to the desired language.
     All existing forms and string arrays will be translated, newly created forms will be translated
     in the BeforeShow message handler.
}


// In the following comments, (TUI*) means that the call is generally intended for the Translation UI
// and probably wont be used by the actual program code

interface

uses Classes, System.Generics.Collections, System.Generics.Defaults, StrUtils, System.Messaging,
     SysUtils, System.Hash, System.TypInfo, RTTI, System.Types, IOUtils, System.RegularExpressions, Common.Utils
     {$IFDEF FIREMONKEY}
     , FMX.Forms, FMX.Types
     {$ENDIF};

const
  Default_Property_Name = '(default)';
  Translator_Path_Delimiter = #$2794;// '➔'; //#$001F;
  Translator_Leading_Path_Asterisk = '*'#$2794;
  Translator_Trailing_Path_Asterisk = #$2794'*';
  Parent_Delimiter = '.';
  {$I Translator2.inc}

type

  TTranslatorOption = (ExcludeTranslationForm, ExcludeDefaultCaptions, FormSubcomponents);
  TTranslatorOptions = set of TTranslatorOption;

  TTranslationElement = class
  private
    FPath, FInstancePath: string;
  public
    Parent, ClassName, InstanceName, PropertyName: string;
    Valid: boolean;
    Translations: array [0 .. Max_Translator_Languages] of string;
    constructor Create (const AParent, AClassName, AInstanceName: string; APropertyName: string = ''); overload;
    constructor Create (const Path: string); overload;
    function Path: string;
    function InstancePath: string;
    //function TranslateTo (const Language: string): string;
    //procedure AddTranslation (const Language, Value: string);
    function GeneralFilterMatch (const Filter: string): boolean;
    function FilterMatch (index: integer): boolean;

    type

    TDictionaryComparer = class(TEqualityComparer<TTranslationElement>)
      public
        function Equals(const Left, Right: TTranslationElement): Boolean; override;
        function GetHashCode(const Value: TTranslationElement): Integer; override;
      end;

    TListComparer = class(TComparer<TTranslationElement>)
      public
        function Compare(const Left, Right: TTranslationElement): Integer; override;
      end;

    class var DictionaryComparer: IEqualityComparer<TTranslationElement>;
    class var ListComparer: IComparer<TTranslationElement>;

    class constructor Create;

    class function ComputePath (const AParent, AClassName, AInstanceName, APropertyName: string): string; inline;
    class function ComputeInstancePath (const AParent, AClassName, AInstanceName: string): string; inline;
    class function ComputeCustomPath (const Strings: TArray <string>): string;
  end;

  TTranslationElementList = class (TObjectList <TTranslationElement>)
  public
//    procedure Assign (Source: TTranslationElementList);
    function Add (Element: TTranslationElement): integer;
  end;

  TStringArray = array of string;
  PStringArray = ^TStringArray;

  TStrStrDictionary = TDictionary <string, string>;
  TIntStrDictionary = TDictionary <integer, string>;
  TListOfStringPointers = TList<^string>;

  // class for translation of arrays of string, could be enum or integer-indexed doesnt matter
  TStringsInfo = class
  public
    // default i.e. values supplied by code i.e. whatever is in the array at the moment it is registered for translation
    // order is same as in the array
    DefaultValues: array of string;
    // pointer to array of strings, translation will overwrite the content
    lstStringPointers: TListOfStringPointers;
    // maps each default string to the corresponding index in the array
    // needed when the translation UI wants to translate one of the strings to a new language
    dicDefaultStringToIndex: TDictionary <string, integer>;
    // dictionary that maps language name to a dictionary which maps array index to the translated value
    dicTranslations: TObjectDictionary <string, TIntStrDictionary>;

    constructor Create;
    destructor  Destroy; override;

    function Count: integer;
  end;

  TStringAndStringsInfoPair = TPair<string, TStringsInfo>;

  T2WayDictionary<TKey, TValue> = class (TDictionary <TKey, TValue>)
  public
    Reverse: TDictionary <TValue, TKey>;
    procedure Add (AKey: TKey; AValue: TValue);
    procedure Remove (AKey: TKey);
    procedure Clear;

    constructor Create;
    destructor Destroy; override;
  end;

  TStringArrays = class (TObjectDictionary<string, TStringsInfo>)
  private
    slSortedKeys: TStringList;
    function GetSortedByKey (const Index: integer): TStringAndStringsInfoPair;
//    function GetSortedByKeyToStrings(const Index: integer): TArray<string>;
  public
    constructor Create(Ownerships: TDictionaryOwnerships);
    destructor Destroy; override;
    procedure Add (const AKey: string; AValue: TStringsInfo);
    procedure AddOrSetValue(const AKey: string; AValue: TStringsInfo);
    procedure Clear;

    property SortedByKey          [const Index: integer]: TStringAndStringsInfoPair read GetSortedByKey;
//    property SortedByKeyToStrings [const Index: integer]: TArray<string> read GetSortedByKeyToStrings;
  end;

  TLanguages = class (T2WayDictionary <string, integer>)
    function ToString: string; override;
  end;

  TTranslatorStringList = class (TStringList)
    function Contains (const s: string): boolean;
  end;

  TTranslator = class// (TInterfacedObject)
  private
    FDefaultFileName, FActiveLanguage: string;
    FActiveLanguageIndex: integer;
    FModified: boolean;
    lstTranslatedForms: TList<TObject>;

    procedure LoadFromStringList(sl: TStringList);
    procedure SetActiveLanguage(const Value: string); virtual;
    function GetDefaultFileName: string;
    procedure BuildExcludedRegEx;
    function GetExcluded: string;
    procedure SetExcluded(const Value: string);

    procedure SetActive(const Value: boolean);
    function GetLanguageCount: integer;
    function AddElement (Element: TTranslationElement): integer;
    procedure Delete(const index: integer);
  protected
    dicElementsOfParent: TObjectDictionary<string, TTranslationElementList>;
    //rgxExcluded: TList<TRegEx>;
    slExcluded: TTranslatorStringList;
    FActive: boolean;
    FDefaultLanguage: string;
  public
    Options: TTranslatorOptions;
    lstElements: TTranslationElementList;
    dicPathsToElements: TDictionary <string, TTranslationElement>;
    dicStringArrays: TStringArrays;
    FormPrefix, StringsPrefix: string;
    dicLanguages : TLanguages;

    constructor Create;
    destructor Destroy; override;

    procedure SetDefaultExcludedProperties;

    // for translating custom elements such as database table and field names
    // it is a good idea to invalidate parent name e.g. "DB" before adding all table and field names
    // and then cleaning up afterwards to remove any references to elements that are no longer present
    procedure InvalidateParent (const Parent: string; const Recursive: boolean);
    procedure InvalidateParentPrefix (const ParentPrefix: string);
    procedure CleanupInvalid   (const Parent: string; const Recursive: boolean); overload; // walk through these parents' elements
    procedure CleanupInvalid; overload; //walk through everything

    // adding something to translation can be repeated at any time,
    // translator will add new elements and remove old ones if they no longer exist as children of supplied element

    // add an element to translation while optionally supplying translated value in specified language
    function Add (const AParentName, AInstanceName: string; const AClassName: string = ''; const APropertyName: string = ''; APropertyValue: string = ''; ALanguage: string = ''): integer;

    procedure AddForAllWithThisName (const AInstanceName, ALanguage, ATranslation: string; OnlyIfCurrentlyMissing: boolean);
    function CountAllWithThisNameWithoutTranslation(const AInstanceName, ALanguage: string): integer;

    // add component and all its translatable properties to translation
    procedure AddComponent(const AParentName: string; com: TComponent; TreatAsForm, Recursive: boolean);

    // For easier database-related stuff
    procedure AddDBTable (ATableName: string; ADisplayName: string = '');
    procedure AddDBField (ATableName, AFieldName: string; ADisplayName: string = '');
    // e.g. for TSqlAuthor TranslatableAttribute['PluralName', Authors]
    procedure AddDBTableAttribute(ATableName, AttributeValue, ADefaultValue: string);
    procedure InvalidateDBElements;

    // add an array of string for translation
    function AddStringArray(const ArrayName: string; var StringArray: array of string): TStringsInfo;
    // (TUI*) Supply translation for one of the strings in the array
    procedure AddStringTranslation (const ArrayName, DefaultValue, Language, Translation : string);
    // (TUI*)
    function GetArrayStringPath(const ArrayName, DefaultValue: string): string;
    procedure AddLanguage (const ALanguage: string);
    procedure RemoveLanguage (const ALanguage: string);

    // Load/Save operations, default file name is most likely sufficient
    procedure LoadFromFile (FileName: string = '');
    function  LoadFromResource(Identifier: string = ''): boolean;
    function LoadFromResourceOrCreateDefault: boolean;
    procedure SaveToFile (FileName: string = ''; AllowOverwrite: boolean = true);

    // (TUI*) methods to find translation if it exists for specified language, dont return anything else such as default language
    function  FindTranslation (const AParent, AClassName, AName, APropertyName, ALanguage: string): string; overload;
    function  FindTranslation (const Path, Language: string): string; overload;
    // for string arrays:
    function  FindStringTranslation (const ArrayName, DefaultValue, Language: string): string;

    function FindByPath (const AParent, AClassName, AName, APropertyName: string): TTranslationElement;

    // methods to apply the translation i.e. replace current strings with translated ones
    // forms will be automatically translated before they are shown
    // (if not already translated and if Active is set to true)
    function  Translate (const AParent, AClassName, AName, APropertyName: string; ALanguage: string = ''): string; overload;
    function  Translate (const Element: TTranslationElement; Language: string = ''): string; overload;
    function  TranslateDBTable (const ATableName: string; ALanguage: string = ''): string;
    function  TranslateDBField (const ATableName, AFieldName: string; ALanguage: string = ''): string;
    function  TranslateTableAttribute (const ATableName, AAttributeValue: string; ALanguage: string = ''): string;

    procedure TranslateComponent (const AParentName: string; com: TComponent; TreatAsForm, Recursive: boolean; Language: string = '');
    procedure TranslateStrings;

    procedure Exclude         (const SubPath: string);
    procedure ExcludeInstance (const InstanceName: string);
    procedure ExcludeParent   (const ParentName: string);
    procedure ExcludeProperty (const PropertyName: string);
    procedure UnExclude      (const SubPath: string);

    function InstanceIsExcluded (const Name: string): boolean;
    function ParentIsExcluded (const Name: string): boolean;
    function SubPathIsExcluded (const SubPath: string): boolean;
    function PropertyIsExcluded (const PropertyName: string): boolean;
    function PropertyOfClassOrInstanceIsExcluded (const PropertyName, ClassOrInstanceName: string): boolean;
    function ElementIsExcluded (const Element: TTranslationElement): boolean;
    function ClassIsExcluded (const ClassName: string): boolean;

    procedure Clear;
    procedure Test;

    property Active: boolean read FActive write SetActive;
    property ActiveLanguage: string read FActiveLanguage write SetActiveLanguage;
    property DefaultLanguage: string read FDefaultLanguage write FDefaultLanguage;
    property Excluded: string read GetExcluded write SetExcluded;
    property LanguageCount: integer read GetLanguageCount;
    property Modified: boolean read FModified write FModified;
    property DefaultFileName: string read GetDefaultFileName write FDefaultFileName;
  end;

  {$IFDEF Firemonkey}
  TFMXTranslator = class (TTranslator, IFreeNotification)
  private
    procedure FreeNotification(AObject: TObject);

    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;

    procedure FormBeforeShownHandler(const Sender: TObject; const M: TMessage);
  protected
    procedure SetActiveLanguage(const Value: string); override;
  public
    procedure AfterConstruction; override;
    destructor Destroy; override;
    // add form and all its components to translation
    procedure AddForm(frm: TCommonCustomForm);
    // add all forms in the application, language will first be set to default
    procedure AddAllForms;

    procedure TranslateForms (Language: string = '');
    procedure TranslateForm (frm: TCommonCustomForm; Language: string = '');


    {$IFDEF Translation_UI}
    procedure OpenUI;
    {$ENDIF}
  end;
  {$ENDIF}

var
  Translator : TTranslator;
  {$IFDEF Firemonkey}
  FMXTranslator: TFMXTranslator;
  {$ENDIF}

implementation

{$IFDEF Translation_UI}
uses FMX.TranslationEditor;
{$ENDIF}

{ TTranslator }

const
  EscapeChar = '\';

  sn_Languages = '[Languages]';
  sn_Excluded = '[Excluded]';
//  sn_ExcludedObjects = '[ExcludedObjects]';
//  sn_Translations = '[Objects]';
  sn_Elements = '[Elements]';
  sn_Strings  = '[Strings]';

function Encode(const S: string): string;
begin
  Result := StringReplace(S, EscapeChar, EscapeChar + EscapeChar, [rfReplaceAll]);
  Result := StringReplace(Result, #13, EscapeChar + 'r', [rfReplaceAll]);
  Result := StringReplace(Result, #10, EscapeChar + 'n', [rfReplaceAll]);
end;

function Decode(const S: string): string;
begin
  Result := StringReplace(S, EscapeChar + 'n', #10, [rfReplaceAll]);
  Result := StringReplace(Result, EscapeChar + 'r', #13, [rfReplaceAll]);
  Result := StringReplace(Result, EscapeChar + EscapeChar, EscapeChar, [rfReplaceAll]);
end;

procedure TTranslator.CleanupInvalid(const Parent: string; const Recursive: boolean);
var
  lst: TTranslationElementList;
begin
  if dicElementsOfParent.TryGetValue(Parent, lst)
    then for var i := lst.Count - 1 downto 0 do
      if not lst[i].Valid
        then
          begin
            if Recursive
               then CleanupInvalid(Parent + Parent_Delimiter + lst[i].InstanceName, false);

            Delete (lstElements.IndexOf (lst[i]));
          end
        else //valid
    //else raise Exception.Create('dicElementsOfParent element not found for Parent = ' + Parent);
end;

procedure TTranslator.CleanupInvalid;
var
  i : integer;
begin
  for i := lstElements.Count - 1 downto 0 do
    if not lstElements[i].Valid then Delete (i);
end;


procedure TTranslator.Clear;
begin
  while lstElements.Count > 0
        do Delete(0);
end;

procedure TTranslator.SetDefaultExcludedProperties;
begin
  ExcludeProperty('Name');
  ExcludeProperty('DefaultStyleLookupName');
  ExcludeProperty('ParentClassStyleLookupName');
  ExcludeProperty('TextControlStyle');
  ExcludeProperty('MenuBarStyle');
  ExcludeProperty('GridStyle');
  ExcludeProperty('StyleLookup');
end;

constructor TTranslator.Create;
begin
  inherited;
  FormPrefix := 'UI';
  StringsPrefix := 'Strings';

  lstTranslatedForms := TList<TObject>.Create;
  //rgxExcluded := TList<TRegEx>.Create;
  Options := [ExcludeTranslationForm, ExcludeDefaultCaptions, FormSubcomponents];
  DefaultLanguage := 'en';

  lstElements := TTranslationElementList.Create (TTranslationElement.ListComparer);
  lstElements.OwnsObjects := true;
  dicPathsToElements := TDictionary <string, TTranslationElement>.Create;

  dicStringArrays := TStringArrays.Create ([doOwnsValues]);
  dicLanguages := TLanguages.Create;
  dicLanguages.Add (DefaultLanguage, 0);

  slExcluded := TTranslatorStringList.Create;
  slExcluded.Sorted := true;

  SetDefaultExcludedProperties;

  dicElementsOfParent := TObjectDictionary<string,TTranslationElementList>.Create ([doOwnsValues]);
end;

//procedure TTranslator.AfterCreateFormHandler(const Sender: TObject; const M: TMessage);
//begin
//  if not Active then
//     exit;
//
//  if M is TAfterCreateFormHandle then
//    Translate (TAfterCreateFormHandle(M).Value);
//end;

function TTranslator.GetExcluded: string;
begin
  result := slExcluded.Text;
end;

function TTranslator.GetArrayStringPath(const ArrayName, DefaultValue: string): string;
begin
  result := ArrayName + Translator_Path_Delimiter + DefaultValue;
end;

function TTranslator.GetDefaultFileName: string;
begin
  if FDefaultFileName = ''
     then result := ChangeFileExt(FindProjectFilePath, '.trn')
     else result := FDefaultFileName;
end;

function TTranslator.AddElement (Element: TTranslationElement): integer;
var
  lstElementsOfParent: TTranslationElementList;
begin
  result := lstElements.Add(Element);
  dicPathsToElements.Add (Element.Path, Element);

  if not dicElementsOfParent.TryGetValue (Element.parent, lstElementsOfParent)
     then begin
            lstElementsOfParent := TTranslationElementList.Create (TTranslationElement.ListComparer);
            lstElementsOfParent.OwnsObjects := false;
            dicElementsOfParent.Add (Element.parent, lstElementsOfParent);
          end;
  lstElementsOfParent.Add(Element);
end;

procedure TTranslator.LoadFromStringList(sl: TStringList);
var
  Language, value: string;
  tranel: TTranslationElement;
  iDelimiter, iLanguage, isl: integer;
//  DefaultLanguageFound: boolean;

procedure Read (slSection: TStringList);
begin
  inc (isl);
  slSection.BeginUpdate;
  while (isl < sl.Count) and not sl[isl].StartsWith('[') do
    begin
      slSection.Add(sl[isl]);
      inc (isl);
    end;
  slSection.EndUpdate;
end;

procedure ReadStrings;
var
  pair : TArray <string>;
  StringsInfo: TStringsInfo;
  index: integer;
  DefaultString: string;
  dicTranslation: TIntStrDictionary;
  {
  $ConnectionStatus.Disconnected
  sr=Nije povezan
  }
begin
  inc (isl);
  while (sl.Count > isl) and (not sl[isl].StartsWith ('[')) do
    begin
      if not sl[isl].StartsWith ('$')
         then raise Exception.Create('Translation element not found');
      pair := sl[isl].Substring(1).Split (['.']);
      DefaultString := pair[1];
      inc (isl);
      if dicStringArrays.TryGetValue(pair[0], StringsInfo) // if its not registered by code, ignore
        then
          begin
            if not StringsInfo.dicDefaultStringToIndex.TryGetValue(DefaultString, index)
              then index := -1;
            while (sl.Count > isl) and not sl[isl].StartsWith ('$') and not sl[isl].StartsWith ('[') do
              begin
                if index > -1 then
                  begin
                    pair := sl[isl].Split(['=']);
                    if not StringsInfo.dicTranslations.TryGetValue (pair[0], dicTranslation) then
                      begin
                        dicTranslation := TIntStrDictionary.Create;
                        StringsInfo.dicTranslations.Add (pair[0], dicTranslation);
                      end;
                    dicTranslation.Add(index, pair[1]);
                  end;
                inc (isl);
              end;

          end;
    end;


end;

begin
  sl.BeginUpdate;
  for isl := sl.Count - 1 downto 0 do
      if sl[isl].IsEmpty then sl.Delete(isl);
  sl.EndUpdate;

  isl := 0;
  dicLanguages.Clear;
  slExcluded.Clear;

  while (isl <> sl.Count) do
    if (sl[isl] = sn_Languages) then
      begin
        dicLanguages.Clear;
        inc (isl);
        while (isl < sl.Count) and not sl[isl].StartsWith('[') do
          begin
            dicLanguages.Add (sl[isl], dicLanguages.Count);
            inc (isl);
          end;
      end
    else if (sl[isl] = sn_Excluded)  then
      begin
        Read (slExcluded);
        BuildExcludedRegEx;
      end
//    else if (sl[isl] = sn_ExcludedObjects) then
//      Read (slExcludedObjects)
    else if (sl[isl] = sn_Strings) then
      ReadStrings
    else if (sl[isl] = sn_Elements) then
      begin // elements
        inc (isl);
        while (isl < sl.Count) and not sl[isl].StartsWith('[') do
        begin
          if not sl[isl].StartsWith ('$')
             then raise Exception.Create('Translation element not found');
          tranel := TTranslationElement.Create (sl[isl].Substring(1));
          AddElement (tranel);

          inc (isl);

          iLanguage := -1;

          while (isl < sl.Count) and not sl[isl].StartsWith('$') and not sl[isl].StartsWith('[') do
            begin
              iDelimiter := sl[isl].IndexOf('=');
              Language := sl[isl].Substring(0, iDelimiter);
              if Language <> ''
                 then iLanguage := dicLanguages[Language]
                 else inc (iLanguage);
              value := Decode (sl[isl].Substring(iDelimiter + 1));
              tranel.Translations[iLanguage] := value;
              inc (isl);
            end;
        end;
      end;
end;

{$IFDEF Translation_UI}
procedure TFMXTranslator.OpenUI;
begin
  if frmTranslationEditor = nil
     then Application.CreateForm(TfrmTranslationEditor, frmTranslationEditor);
  frmTranslationEditor.Show;
end;
{$ENDIF}

function TTranslator.AddStringArray(const ArrayName: string; var StringArray: array of string): TStringsInfo;
var
  //DefaultValues: TStrStrDictionary;
  slCleanup: TStringList;
  iCleanup: integer;
begin
  if not dicStringArrays.TryGetValue(ArrayName, result) then
     begin
       result := TStringsInfo.Create;
       dicStringArrays.AddOrSetValue (ArrayName, result);
     end;

  for var i := Low (StringArray) to High (StringArray) do
      result.lstStringPointers.Add (@StringArray[i]);

  //si.Ref := @StringArray[0];

  slCleanup := TStringList.Create;
  slCleanup.Sorted := true;
  for var i := low (result.DefaultValues) to high (result.DefaultValues) do
      slCleanup.Add(result.DefaultValues[i]);

  SetLength (result.DefaultValues, length(StringArray));
  for var i := low (StringArray) to high (StringArray) do
    begin
      result.DefaultValues[i] := StringArray[i];
      iCleanup := slCleanup.IndexOf(StringArray[i]);
      if iCleanup > -1 then slCleanup.Delete (iCleanup);
    end;

  for var pair in result.dicTranslations do
    for var sRemove in slCleanup do
      if result.dicDefaultStringToIndex.TryGetValue (sRemove, iCleanup)
         then pair.Value.Remove(iCleanup);
  slCleanup.Free;

  result.dicDefaultStringToIndex.Clear;
  for var i := low (StringArray) to high (StringArray) do
    result.dicDefaultStringToIndex.Add(StringArray[i], i);
end;

procedure TTranslator.AddStringTranslation(const ArrayName, DefaultValue, Language, Translation: string);
var
  si: TStringsInfo;
  isd: TIntStrDictionary;
  index: integer;
begin
  if not dicStringArrays.TryGetValue(ArrayName, si)
     then raise Exception.Create(ArrayName + ' not registered!');

  index := si.dicDefaultStringToIndex[DefaultValue];
  if not si.dicTranslations.TryGetValue(Language, isd)
     then begin
            isd := TIntStrDictionary.Create;
            si.dicTranslations.Add(Language, isd)
          end;

  isd.AddOrSetValue(index, Translation);
end;

procedure TTranslator.LoadFromFile(FileName: string = '');
var
  sl: TStringList;
begin
  if FileName = ''
     then FileName := DefaultFileName;

  lstElements.Clear;
  sl := TStringList.Create;
  sl.LoadFromFile (FileName);
  LoadFromStringList(sl);
  sl.Free;
end;

function TTranslator.LoadFromResource(Identifier: string = ''): boolean;
var
  ResStream: TResourceStream;
  sl: TStringList;
begin
  if Identifier = ''
     then Identifier := 'Translation';

  if FindResource(HInstance, PChar(Identifier), RT_RCDATA) = 0
     then exit (false);

  result := true;
  ResStream := TResourceStream.Create(HInstance, Identifier, RT_RCDATA);
  ResStream.Position := 0;
  sl := nil;
  try
    sl := TStringList.Create;
    sl.LoadFromStream (ResStream, TEncoding.UTF8);
    LoadFromStringList(sl);
  finally
    ResStream.Free;
    sl.Free;
  end;
end;

function TTranslator.LoadFromResourceOrCreateDefault: boolean;
begin
  result := LoadFromResource;
  if not result then
     begin
       SetDefaultExcludedProperties;
       SaveToFile ('', false);
       raise Exception.Create ('It is normal to see this error for a new project: '#13#13+
                               'Translator is unable to load translation from resource, please include the .trn file in resource manager with identifier "Translation"');
     end;
end;

procedure TTranslator.SaveToFile (FileName: string = ''; AllowOverwrite: boolean = true);
var
  path, prev: string;
  sl : TStringList;
  iLanguage: integer;

procedure WriteSection (Name: string; slSection: TStringList);
begin
  sl.Add('');
  sl.Add(Name);
  for var lang in slSection do
      sl.Add(lang);
end;

begin
  if FileName = ''
     then FileName := DefaultFileName;

  if FileExists (FileName) then
    begin
      if not AllowOverwrite then exit;
      DeleteFile (FileName+'.bak');
      TFile.Move(FileName, FileName+'.bak');
    end;

  sl := TStringList.Create;

  sl.Add('');
  sl.Add(sn_Languages);
  for var key := 0 to dicLanguages.Count - 1 do
    sl.Add(dicLanguages.Reverse[key]);

  WriteSection (sn_Excluded, slExcluded);
//  WriteSection (sn_ExcludedObjects, slExcludedObjects);

  sl.Add('');
  sl.Add(sn_Elements);
  for var pair in dicElementsOfParent do
    begin
      prev := '';
      for var TranslationElement in pair.Value do
        begin
          path := TranslationElement.Path;
          sl.Add ('$'+path);
          for iLanguage := 0 to LanguageCount - 1 do
            if not TranslationElement.Translations[iLanguage].IsEmpty
               then sl.Add (dicLanguages.Reverse[iLanguage] + '=' + Encode(TranslationElement.Translations[iLanguage]));
        end;
    end;

  sl.Add('');
  sl.Add(sn_Strings);
  for var pairStrings in dicStringArrays do
    begin
      var index := 0;
      var Translation: string;
      for var DefaultString in pairStrings.Value.DefaultValues do
        begin
          path := pairStrings.Key + '.' + DefaultString;
          sl.Add ('$'+path);
          if pairStrings.Value.dicDefaultStringToIndex.TryGetValue(DefaultString, index)
            then for var pairTranslation in pairStrings.Value.dicTranslations do
              begin
                if pairTranslation.Value.TryGetValue (index, Translation)
                   then sl.Add (pairTranslation.Key + '=' + Translation);
              end;
        end;
    end;

  sl.SaveToFile (FileName, TEncoding.UTF8);
  sl.Free;
end;

procedure TTranslator.SetActive(const Value: boolean);
begin
  FActive := Value;
end;

function TTranslator.GetLanguageCount: integer;
begin
  result := dicLanguages.Count;
end;

procedure TTranslator.SetActiveLanguage(const Value: string);
begin
  if not dicLanguages.ContainsKey (Value) then exit;

  FActiveLanguage := Value;
  FActiveLanguageIndex := dicLanguages[Value];
  TranslateStrings;
end;

procedure TTranslator.Delete (const index: integer);
var
  Element: TTranslationElement;
  lst: TTranslationElementList;
begin
  Element := lstElements[index];
  if dicElementsOfParent.TryGetValue(Element.Parent, lst)
     then begin
            lst.Remove(Element);
            if lst.Count = 0
               then dicElementsOfParent.Remove(Element.Parent);
          end
     else raise Exception.Create('element doesnt belong to any parents?');
  dicPathsToElements.Remove(Element.Path);
  lstElements.Delete(index);
end;

procedure TTranslator.SetExcluded(const Value: string);
begin
  slExcluded.Text := Value;
  for var i := lstElements.Count - 1 downto 0 do
      if ElementIsExcluded (lstElements[i]) then
         Delete (i);
  Modified := true;
end;

function CreateRegExFromPattern(const Pattern: string): string;
begin
  // Convert asterisk-based pattern to regular expression
  Result := TRegEx.Escape(Pattern); // Escape special characters
  Result := '^' + Result.Replace('\*', '.*').Replace('\?', '.').Replace('\.', '\.');
  Result := Result + '$'; // Ensure full string match
end;

procedure TTranslator.BuildExcludedRegEx;
begin
//  rgxExcluded.Clear;
//  for var s in slExcludedProperties do
//    if s.Contains ('*')
//       then rgxExcluded.Add(TRegEx.Create(CreateRegExFromPattern(s), [roIgnoreCase]));
end;

procedure TTranslator.TranslateComponent (const AParentName: string; com: TComponent; TreatAsForm, Recursive: boolean; Language: string = '');
var
  ctx : TRttiContext;
  t : TRttiType;
  p : TRttiProperty;
  AClassName, InstanceName, PropValue: string;
  Element: TTranslationElement;
begin
  if TreatAsForm
    then InstanceName := ''
    else InstanceName := com.Name;

  // for optimization only
  if InstanceIsExcluded (InstanceName) or
     ParentIsExcluded (AParentName) then
    exit;

  AClassName := com.ClassName;
  ctx := TRttiContext.Create;
  try
    t := ctx.GetType(com.ClassType);

    for p in t.GetProperties do
      begin
        if not p.IsWritable then
           continue;
        if not (p.PropertyType.TypeKind in [tkUString, tkChar, tkString, tkLString]) then
           continue;


        //element := find TTranslationElement.Create (Parent, InstanceName, p.Name);
        //path := element.Path;

        Element := FindByPath (AParentName, AClassName, InstanceName, p.Name);
        if Element = nil then continue;

        PropValue := Translate (Element, Language);
        if PropValue = '' then continue;

        case p.PropertyType.TypeKind of
          tkUString, tkChar, tkString, tkLString : p.SetValue (com, PropValue);
        end;
      end;
  finally
    ctx.Free;
  end;

  if Recursive then
    begin
      var NewParent := '';

      if AParentName = ''
         then NewParent := com.Name
         else if TreatAsForm
              then NewParent := AParentName + Parent_Delimiter + com.ClassName
              else NewParent := AParentName + Parent_Delimiter + com.Name;

      for var i := 0 to com.ComponentCount - 1 do
        TranslateComponent(NewParent, com.Components[i], false, Recursive, Language);
    end;
end;

procedure TTranslator.TranslateStrings;
var
  index: integer;
  Translation: TIntStrDictionary;
  DefaultString, Translated: string;
begin
  for var pair in dicStringArrays do
    begin
      Translation := nil;

      pair.Value.dicTranslations.TryGetValue (FActiveLanguage, Translation);
      for index := low (pair.Value.DefaultValues) to high (pair.Value.DefaultValues) do
        begin
          DefaultString := pair.Value.DefaultValues[index];
          if (Translation <> nil) and Translation.TryGetValue(index, Translated)
             then pair.Value.lstStringPointers[index]^ := Translated
             else pair.Value.lstStringPointers[index]^ := DefaultString;
        end;
    end;
end;

procedure TTranslator.UnExclude(const SubPath: string);
begin

end;

procedure TTranslator.InvalidateDBElements;
begin
  InvalidateParent('DB', false);
  InvalidateParentPrefix('DB.');
end;

procedure TTranslator.InvalidateParent (const Parent: string; const Recursive: boolean);
var
  lstElements: TTranslationElementList;
  TranslationElement : TTranslationElement;
begin
  lstElements := nil;
  dicElementsOfParent.TryGetValue(Parent, lstElements);
  //   then raise Exception.Create('No elements of parent ' + Parent + ' found.');

  if lstElements <> nil
     then for TranslationElement in lstElements do
        TranslationElement.Valid := false;

  if Recursive then
    begin
      var ParentPattern := Parent + Parent_Delimiter;
      for var pair in dicElementsOfParent do
        if pair.Key.StartsWith (ParentPattern)
           then InvalidateParent(pair.Key, false);
    end;
end;

procedure TTranslator.InvalidateParentPrefix(const ParentPrefix: string);
var
  TranslationElement : TTranslationElement;
begin
  for var pair in dicElementsOfParent do
    if pair.Key.StartsWith (ParentPrefix, true) then
      for TranslationElement in pair.Value do
          TranslationElement.Valid := false;
end;

function TTranslator.SubPathIsExcluded(const SubPath: string): boolean;
begin //parent.name.* name.* *.name
  result := slExcluded.Contains(SubPath)
            or
            slExcluded.Contains(SubPath + Translator_Trailing_Path_Asterisk)
            or
            slExcluded.Contains(Translator_Leading_Path_Asterisk + SubPath);
end;

function TTranslator.ClassIsExcluded(const ClassName: string): boolean;
begin
  result := slExcluded.Contains(ClassName + Translator_Trailing_Path_Asterisk);
end;

function  TTranslator.PropertyIsExcluded (const PropertyName: string): boolean;
begin
  result := slExcluded.Contains(Translator_Leading_Path_Asterisk + PropertyName);
end;

function TTranslator.PropertyOfClassOrInstanceIsExcluded(const PropertyName, ClassOrInstanceName: string): boolean;
begin
  result := slExcluded.Contains(ClassOrInstanceName + Translator_Path_Delimiter + PropertyName);
end;

function TTranslator.InstanceIsExcluded(const Name: string): boolean;
begin
  result := slExcluded.Contains(Translator_Leading_Path_Asterisk + Name + Translator_Trailing_Path_Asterisk);
end;

function TTranslator.ParentIsExcluded (const Name: string): boolean;
begin
  result := slExcluded.Contains(Name + Translator_Trailing_Path_Asterisk);
end;

procedure TTranslator.AddComponent(const AParentName: string; com: TComponent; TreatAsForm, Recursive: boolean);
var
  ctx : TRttiContext;
  t : TRttiType;
  InstanceName, ElementClassName, PropValue: string;

  procedure ProcessType (t : TRttiType; const ComponentName, ParentPropertyName: string; obj: TObject; Recursive: boolean);
  var
    prop : TRttiProperty;
    ppn: string;
  begin
    for prop in t.GetProperties do
      begin
        case prop.PropertyType.TypeKind of
          tkUString, tkChar, tkString, tkLString :
            begin
              if not prop.IsWritable then
                 continue;
              PropValue := prop.GetValue(obj).ToString;

              if PropValue = '' then continue;

              if ParentPropertyName <> ''
                 then ppn := ParentPropertyName + Translator_Path_Delimiter + prop.Name
                 else ppn := prop.Name;

              Add (AParentName, ComponentName, com.ClassName, ppn, PropValue);
            end;
          tkClass:
            begin
              if prop.Name = 'Border' then
                if now=0 then exit;

              if prop.PropertyType.AsInstance.MetaclassType.InheritsFrom(TStrings) then
                begin
                  var Value := Prop.GetValue(obj);
                  //if Value.AsObject is TStrings then
                  if not Value.IsEmpty
                     then //TStrings(Value.AsObject).Clear;
                          Add (AParentName, ComponentName, com.ClassName, prop.Name, TStrings (Value.AsObject).Text);
                end;

              if not Recursive then continue;

              if ParentPropertyName <> ''
                 then ppn := ParentPropertyName + Translator_Path_Delimiter + prop.Name
                 else ppn := prop.Name;
              ProcessType (ctx.GetType(prop.ClassType), ComponentName, ppn, prop.GetValue(obj).AsObject, false);
            end
          else continue;
        end;
       end;
  end;
begin
  if TreatAsForm
    then
      begin // since there can be multiple forms of same type we will ignore the form name and just use the class name instead
        InstanceName     := '';
        ElementClassName := com.ClassName;
      end
    else
      begin
        if com.Name = '' then exit; // this is not a form, and it has no name so ignore whatever it is

        InstanceName     := com.Name;
        ElementClassName := com.ClassName;
      end;

  ctx := TRttiContext.Create;
  try
    t := ctx.GetType(com.ClassType);
    ProcessType (t, InstanceName, '', com, true);
  finally
    ctx.Free;
  end;

  if Recursive then
    begin
      var NewParent := '';
      if AParentName = ''
         then NewParent := com.Name
         else if TreatAsForm
              then NewParent := AParentName + Parent_Delimiter + ElementClassName
              else NewParent := AParentName + Parent_Delimiter + com.Name;
      for var i := 0 to com.ComponentCount - 1 do
        AddComponent(NewParent, com.Components[i], false, (FormSubcomponents in Options));
    end;
end;

procedure TTranslator.AddDBTable(ATableName, ADisplayName: string);
begin
  Add ('DB', ATableName, 'DBTable', '', ADisplayName);
end;

procedure TTranslator.AddDBField(ATableName, AFieldName, ADisplayName: string);
begin
  Add ('DB.' + ATableName, AFieldName, 'DBField', '', ADisplayName);
end;

procedure TTranslator.AddDBTableAttribute(ATableName, AttributeValue, ADefaultValue: string);
begin
  Add ('DB.' + ATableName, AttributeValue, 'TRA', '', ADefaultValue);
end;

procedure TTranslator.AddForAllWithThisName (const AInstanceName, ALanguage, ATranslation: string; OnlyIfCurrentlyMissing: boolean);
begin
  var iLanguage := dicLanguages[ALanguage];
  for var Element in lstElements do
    if (Element.InstanceName = AInstanceName) and
       ( not OnlyIfCurrentlyMissing or (Element.Translations [iLanguage] = '') )
         then Element.Translations [iLanguage] := ATranslation;
end;

function TTranslator.CountAllWithThisNameWithoutTranslation (const AInstanceName, ALanguage: string): integer;
begin
  result := 0;
  var iLanguage := dicLanguages[ALanguage];
  for var Element in lstElements do
    if (Element.InstanceName = AInstanceName) and ( Element.Translations [iLanguage] = '')
         then inc (result);
end;

function TTranslator.Add (const AParentName, AInstanceName: string; const AClassName: string = ''; const APropertyName: string = ''; APropertyValue: string = ''; ALanguage: string = ''): integer;
var
  Element: TTranslationElement;
begin
  if PropertyIsExcluded (APropertyName) or PropertyOfClassOrInstanceIsExcluded (APropertyName, AClassName)
    then exit (-1);

  if (APropertyName = 'Caption') then
    if (APropertyValue = AInstanceName) and (ExcludeDefaultCaptions in Options)
      then exit (-1);

  if ALanguage = ''
     then ALanguage := DefaultLanguage;

  Element := FindByPath (AParentName, AClassName, AInstanceName, APropertyName);  //TTranslationElement.Create(
  if Element = nil
    then
      begin
        Element := TTranslationElement.Create(AParentName, AClassName, AInstanceName, APropertyName);
        if ElementIsExcluded (Element) then
          begin
            Element.Free;
            exit (-1);
          end;
        result := AddElement(Element);
      end
    else
      begin
        Element.Valid := not ElementIsExcluded (Element);
        result := lstElements.IndexOf(Element);
      end;

  if Element.Valid
     then Element.Translations[Translator.dicLanguages[ALanguage]] := APropertyValue;

  Modified := true;
end;

function TTranslator.FindByPath (const AParent, AClassName, AName, APropertyName: string): TTranslationElement;
begin
  result := nil;
  dicPathsToElements.TryGetValue (TTranslationElement.ComputePath(AParent, AClassName, AName, APropertyName),
                                  result);
end;

function TTranslator.FindTranslation (const AParent, AClassName, AName, APropertyName, ALanguage: string): string;
var
  TranslationElement: TTranslationElement;
begin
  result := '';
  TranslationElement := FindByPath (AParent, AClassName, AName, APropertyName);
  if TranslationElement <> nil
     then result := TranslationElement.Translations[dicLanguages[ALanguage]]
     else if APropertyName <> ''
          then result := APropertyName
          else result := AName;
end;

function TTranslator.Translate (const AParent, AClassName, AName, APropertyName: string; ALanguage: string = ''): string;
var
  Element: TTranslationElement;
begin
  Element := FindByPath (AParent, AClassName, AName, APropertyName);
  if Element <> nil
     then result := Translate (Element, ALanguage)
     else if APropertyName <> ''
          then result := APropertyName
          else result := AName;
end;

function TTranslator.TranslateDBTable (const ATableName: string; ALanguage: string = ''): string;
begin
  result := Translate ('DB', 'DBTable', ATableName, '', ALanguage);
end;

function TTranslator.TranslateDBField (const ATableName, AFieldName: string; ALanguage: string = ''): string;
begin
  result := Translate ('DB.' + ATableName, 'DBField', AFieldName, ALanguage);
end;

function TTranslator.TranslateTableAttribute (const ATableName, AAttributeValue: string; ALanguage: string = ''): string;
begin
  result := Translate ('DB.' + ATableName, 'TRA', AAttributeValue, ALanguage);
end;

procedure TTranslator.Test;
begin
  Add('Test Parent', 'Test Instance', 'Test Class', 'Test Property', 'Test Value');
end;

function TTranslator.Translate(const Element: TTranslationElement; Language: string = ''): string;
begin
  if Language = ''
     then result := Element.Translations[FActiveLanguageIndex]
     else result := Element.Translations[dicLanguages[Language]];
  if (result = '') then
     result := Element.Translations[dicLanguages[DefaultLanguage]];
  if (result = '') then
     result := Element.PropertyName;
  if (result = '') then
     result := Element.InstanceName;
end;

destructor TTranslator.Destroy;
begin
  dicPathsToElements.Free;
  lstElements.Free;
  dicStringArrays.Free;
  dicLanguages.Free;
  slExcluded.Free;
  dicElementsOfParent.Free;

  //rgxExcluded.Free;
  lstTranslatedForms.Free;
  inherited;
end;

function TTranslator.ElementIsExcluded(const Element: TTranslationElement): boolean;
begin
  result := ParentIsExcluded(Element.Parent)
            or InstanceIsExcluded(Element.InstanceName)
            or ClassIsExcluded(Element.ClassName)
            or PropertyIsExcluded(Element.PropertyName)
            or PropertyOfClassOrInstanceIsExcluded(Element.PropertyName, Element.ClassName)
            or PropertyOfClassOrInstanceIsExcluded(Element.PropertyName, Element.InstanceName)
            or SubPathIsExcluded(Element.InstancePath)
            or SubPathIsExcluded(Element.Path)
            ;
end;

procedure TTranslator.Exclude (const SubPath: string);
var
  i : integer;
begin
  slExcluded.Add(SubPath);
  for i := lstElements.Count - 1 downto 0 do
    if ElementIsExcluded (lstElements[i]) then Delete (i);

//  InvalidateParent(ObjectNameOrPath, true);
//  CleanupInvalid(ObjectNameOrPath, true);
end;

procedure TTranslator.ExcludeInstance(const InstanceName: string);
begin
  Exclude(Translator_Leading_Path_Asterisk + InstanceName + Translator_Trailing_Path_Asterisk);
end;

procedure TTranslator.ExcludeParent(const ParentName: string);
begin
  Exclude(ParentName + Translator_Trailing_Path_Asterisk);
end;

procedure TTranslator.ExcludeProperty(const PropertyName: string);
begin
  Exclude(Translator_Leading_Path_Asterisk + PropertyName);
end;

function TTranslator.FindStringTranslation(const ArrayName, DefaultValue, Language: string): string;
var
  si: TStringsInfo;
  index : integer;
  Translation: TIntStrDictionary;
begin
  if Language = DefaultLanguage
     then exit (DefaultValue);

  if dicStringArrays.TryGetValue(ArrayName, si)
     and si.dicDefaultStringToIndex.TryGetValue (DefaultValue, index)
     and si.dicTranslations.TryGetValue (Language, Translation)
     and Translation.TryGetValue(index, result)
       then //we found it
       else result := '';
end;

procedure TTranslator.AddLanguage (const ALanguage: string);
begin
  if dicLanguages.ContainsKey (ALanguage)
    then raise Exception.Create  ('Language already present')
    else dicLanguages.Add(ALanguage, dicLanguages.Count);
end;

procedure TTranslator.RemoveLanguage (const ALanguage: string);
begin
  if dicLanguages.ContainsKey (ALanguage)
    then raise Exception.Create  ('Language not registered')
    else dicLanguages.Remove (ALanguage);
end;

function TTranslator.FindTranslation(const Path, Language: string): string;
var
  Element : TTranslationElement;
begin
  if dicPathsToElements.TryGetValue(Path, Element)
     then result := Element.Translations[dicLanguages[Language]]
     else result := '';
end;

{ TTranslationElement }

class constructor TTranslationElement.Create;
begin
  DictionaryComparer := TDictionaryComparer.Create;
  ListComparer := TListComparer.Create;
end;

constructor TTranslationElement.Create(const AParent, AClassName, AInstanceName: string; APropertyName: string);
begin
  inherited Create;
  Parent := AParent;
  InstanceName := AInstanceName;
  ClassName := AClassName;
  PropertyName := APropertyName;
  Valid := true;
end;

constructor TTranslationElement.Create(const Path: string);
var
  s: TArray<string>;
begin // Parent->Name[->(classname)]->PropertyName
  s := Path.Split([Translator_Path_Delimiter]);
  Create (s[0], s[1], s[2], s[3]);
end;

function TTranslationElement.Path: string;
begin
  if FPath = ''
     then FPath := ComputePath (Parent, ClassName, InstanceName, PropertyName);
  result := FPath;
end;

function TTranslationElement.GeneralFilterMatch (const Filter: string): boolean;
begin
  if lowercase(Parent).Contains (Filter) or
     lowercase(InstanceName).Contains (Filter) or
     lowercase(ClassName).Contains (Filter) or
     lowercase(PropertyName).Contains (Filter)
    then exit (true);

  for var iLang := 0 to Max_Translator_Languages - 1 do
  if lowercase(Translations[iLang]).Contains (Filter)
    then exit (true);

  result := false;
end;

function TTranslationElement.FilterMatch (index: integer): boolean;
//0 Show everything
//1 Show if any translation is missing
//2...n Show if this language is missing
var
  i : integer;
begin
  result := false;

  if (index > 1)
    then
      exit (Translations[index-2].IsEmpty)
    else
      for i := 0 to Translator.LanguageCount-1 do
        if Translations[i].IsEmpty
          then exit (true);
end;

function TTranslationElement.InstancePath: string;
begin
  if FInstancePath = ''
     then FInstancePath := ComputeInstancePath(Parent, ClassName, InstanceName);
  result := FInstancePath;
end;

{ TTranslationToLanguage.TComparer }

function TTranslationElement.TDictionaryComparer.Equals(const Left, Right: TTranslationElement): Boolean;
begin
  result := (Left.Parent = Right.Parent)
        and (Left.InstanceName = Right.InstanceName)
        and (Left.PropertyName = Right.PropertyName);
end;

function TTranslationElement.TDictionaryComparer.GetHashCode(const Value: TTranslationElement): Integer;
begin
  Result :=  THashBobJenkins.GetHashValue (Value.Parent, 0);
  Result :=  THashBobJenkins.GetHashValue (Value.InstanceName, Result);
  Result :=  THashBobJenkins.GetHashValue (Value.PropertyName, Result);
end;

{ TTranslationToLanguage.TListComparer }

function TTranslationElement.TListComparer.Compare(const Left, Right: TTranslationElement): Integer;
begin
  result := CompareStr(Left.Parent, Right.Parent);
  if result = 0 then
    result := CompareStr(Left.ClassName, Right.ClassName);
  if result = 0 then
    result := CompareStr(Left.InstanceName, Right.InstanceName);
  if result = 0 then
    result := CompareStr(Left.PropertyName, Right.PropertyName);
end;

{ TTranslatableStrings }

{ TCustomTranslatableStrings }

//function TCustomTranslatableStrings.GetValue(index: integer): string;
//begin
//  result := FValues[index];
//end;
//
//procedure TCustomTranslatableStrings.SetValue(index: integer; const Value: string);
//begin
//  if Length(FValues) <= index
//     then SetLength(FValues, index + 100);
//  FValues[index] := Value;
//end;

class function TTranslationElement.ComputePath(const AParent, AClassName, AInstanceName, APropertyName: string): string;
begin
  result := AParent + Translator_Path_Delimiter +
            AClassName + Translator_Path_Delimiter +
            AInstanceName + Translator_Path_Delimiter +
            APropertyName;
end;

class function TTranslationElement.ComputeCustomPath(const Strings: TArray <string>): string;
begin
  result := '';
  for var s in Strings do
    if result = ''
       then result := s
       else result := result + Translator_Path_Delimiter + s;
end;

class function TTranslationElement.ComputeInstancePath (const AParent, AClassName, AInstanceName: string): string;
begin
  result := AParent + Translator_Path_Delimiter +
            AClassName + Translator_Path_Delimiter +
            AInstanceName;
end;

{ TStringsInfo }

function TStringsInfo.Count: integer;
begin
  result := length (DefaultValues);
end;

constructor TStringsInfo.Create;
begin
  inherited;
  dicDefaultStringToIndex := TDictionary <string, integer>.Create;
  dicTranslations := TObjectDictionary <string, TIntStrDictionary>.Create ([doOwnsValues]);
  lstStringPointers:= TListOfStringPointers.Create;
end;

destructor TStringsInfo.Destroy;
begin
  dicDefaultStringToIndex.Free;
  dicTranslations.Free;
  lstStringPointers.Free;

  inherited;
end;

{ TStringArrays }

procedure TStringArrays.Add(const AKey: string; AValue: TStringsInfo);
begin
  inherited Add (AKey, AValue);
  slSortedKeys.Add(AKey);
end;

procedure TStringArrays.AddOrSetValue(const AKey: string; AValue: TStringsInfo);
begin
  if ContainsKey (AKey)
     then self[AKey] := AValue
     else Add (AKey, AValue);
end;

procedure TStringArrays.Clear;
begin
  slSortedKeys.Clear;
  inherited;
end;

constructor TStringArrays.Create(Ownerships: TDictionaryOwnerships);
begin
  inherited Create (Ownerships);
  slSortedKeys := TStringList.Create;
end;


destructor TStringArrays.Destroy;
begin
  slSortedKeys.Free;
  inherited;
end;

function TStringArrays.GetSortedByKey (const Index: integer): TStringAndStringsInfoPair;
begin
  result.Key := slSortedKeys[Index];
  result.Value := self[result.Key];
end;

{function TStringArrays.GetSortedByKeyToStrings(const Index: integer): TArray<string>;
begin
  var pair := SortedByKey [index];
  setlength (result, pair.Value.Count);

  for var i := 0 to pair.Value.Count - 1 do
    begin
      result[i] := pair.Value.DefaultValues[i];
      DefaultString := pair.Value.DefaultValues[i];
      sgrStrings.Cells[1, row] := DefaultString;
      iDefaultString := pair.Value.dicDefaultStringToIndex[DefaultString];
      for iLang := 0 to Translator.Languages.Count - 1 do
        begin
          if pair.Value.dicTranslations.TryGetValue(Translator.Languages[iLang], isd)
             then if isd.TryGetValue (iDefaultString, TranslatedString)
                then sgrStrings.Cells[iLang + 2, row] := TranslatedString
                else sgrStrings.Cells[iLang + 2, row] := '';
        end;
    end;      aa
end;
}

{ TTranslationElementList }

function TTranslationElementList.Add(Element: TTranslationElement): integer;
var
  index: integer;
begin
  if BinarySearch(Element, index)
     then raise Exception.Create('Element already in list?');
  Insert (index, Element);
  result := index;
end;

{ T2WayDictionary<TKey, TValue> }

procedure T2WayDictionary<TKey, TValue>.Add(AKey: TKey; AValue: TValue);
begin
  inherited Add (AKey, AValue);
  Reverse.Add (AValue, AKey);
end;

procedure T2WayDictionary<TKey, TValue>.Clear;
begin
  Reverse.Clear;
  inherited;
end;

constructor T2WayDictionary<TKey, TValue>.Create;
begin
  inherited Create;
  Reverse := TDictionary<TValue,TKey>.Create;
end;

destructor T2WayDictionary<TKey, TValue>.Destroy;
begin
  Reverse.Free;
  inherited;
end;

procedure T2WayDictionary<TKey, TValue>.Remove(AKey: TKey);
var
  Value: TValue;
begin
  if TryGetValue (AKey, Value)
     then Reverse.Remove(Value);
  inherited Remove (AKey);
end;

{ TLanguages }

function TLanguages.ToString: string;
var
  i : integer;
begin
  //result := DefaultLanguage;
  for i := 0 to Count - 1 do
    result := result + Reverse[i] + sLineBreak;
  result := result.Trim;
end;

{ TTranslatorStringList }

function TTranslatorStringList.Contains(const s: string): boolean;
begin
  result := IndexOf(s) > -1;
end;

{$IFDEF FIREMONKEY}
procedure TFMXTranslator.FreeNotification(AObject: TObject);
begin
  lstTranslatedForms.Remove(AObject);
end;

procedure TFMXTranslator.AfterConstruction;
begin
  inherited;
  TMessageManager.DefaultManager.SubscribeToMessage(TFormBeforeShownMessage, FormBeforeShownHandler);
end;

destructor TFMXTranslator.Destroy;
begin
  for var obj in lstTranslatedForms do
      TFmxObject(obj).RemoveFreeNotify(self);
  inherited;
end;

procedure TFMXTranslator.TranslateForms;
var
  i : integer;
begin
  for i := 0 to Screen.FormCount - 1 do
     TranslateForm(Screen.Forms[i]);
end;

procedure TFMXTranslator.TranslateForm (frm: TCommonCustomForm; Language: string = '');
begin
  {$IFDEF Translation_UI}
  if (frm is TfrmTranslationEditor) and (ExcludeTranslationForm in Options)
     then exit;
  {$ENDIF}

  if not lstTranslatedForms.Contains (frm)
     then lstTranslatedForms.Add (frm);
  frm.AddFreeNotify(self);
  if Language = ''
     then Language := ActiveLanguage;
  TranslateComponent (FormPrefix, frm, true, true, Language);
end;

procedure TFMXTranslator.AddAllForms;
var
  i : integer;
begin
  ActiveLanguage := DefaultLanguage;

  for i := 0 to Screen.FormCount - 1 do
      AddForm(Screen.Forms[i]);
end;

procedure TFMXTranslator.AddForm(frm: TCommonCustomForm);
begin
  {$IFDEF Translation_UI}
  if (frm is TfrmTranslationEditor) and (ExcludeTranslationForm in Options)
     then exit;
  {$ENDIF}

  InvalidateParent     (FormPrefix, true);
  AddComponent (FormPrefix, frm, true, true);
  CleanupInvalid       (FormPrefix, true);
end;

function TFMXTranslator.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

procedure TFMXTranslator.SetActiveLanguage(const Value: string);
begin
  inherited;
  TranslateForms;
  TranslateStrings;
end;

function TFMXTranslator._AddRef: Integer;
begin
  Result := -1;
end;

function TFMXTranslator._Release: Integer;
begin
  Result := -1;
end;

procedure TFMXTranslator.FormBeforeShownHandler(const Sender: TObject; const M: TMessage);
begin
  if not Active then
     exit;

  if (M is TFormBeforeShownMessage) and not lstTranslatedForms.Contains (TFormBeforeShownMessage(M).Value) then
    TranslateForm (TFormBeforeShownMessage(M).Value);
end;

{$ENDIF}

initialization

{$IFDEF Firemonkey}
  FMXTranslator := TFMXTranslator.Create;
  Translator := FMXTranslator;
{$ELSE}
  Translator := TTranslator.Create;
{$ENDIF}

finalization
  Translator.Free;

end.
