unit PersistentUserSettings;

interface

uses
  System.Generics.Collections, SysUtils, System.Types, math,
  {$IFDEF FIREMONKEY}
  FMX.Forms, System.UITypes, FMX.Platform, FMX.Grid, FMX.Controls, FMX.Types,
  {$ELSE}
  VCL.Forms, VCL.Grids, VCL.Controls,
  WinApi.Windows,
  {$ENDIF}
  SynCommons,
  Common.Utils,
  MormotTypes, MormotMods;

type

  TpusApplication = class (TDictionary <string, variant>)
    function Read (const Name: string): variant;
    procedure Write (const Name: string; Values: variant);
    function ToVariant: variant;
    constructor CreateFrom (const v: variant);
  end;

  TPersistentUserSettings = class
  private
    AppName: string;
    Items: TObjectDictionary <string, TpusApplication>;
  public
    constructor Create;
    destructor Destroy; override;

    function Read (const Name: string): variant;
    function HasField (const Name: string): boolean;
    procedure Write (const Name: string; Values: variant);
    procedure WriteFormPosition (Form: TCustomForm);
    procedure ReadFormPosition (Form: TCommonCustomForm);
    {$IFDEF FIREMONKEY}
    procedure WriteGridStates (Grids: TArray<TStringGrid>);
    procedure ReadGridStates (Grids: TArray<TStringGrid>);
    procedure ReadAlignedElement (Element: TControl);
    procedure WriteAlignedElement (Element: TControl);
    {$ENDIF}
    procedure WriteGridState (Grid: TStringGrid);
    function ReadGridState(Grid: TStringGrid): boolean;
    function ToVariant: variant;
    procedure FromVariant (const v: variant);
    procedure Clear;
  end;

var
  pus: TPersistentUserSettings;

implementation

{ TPersistentUserSettings }

procedure TPersistentUserSettings.Clear;
begin
  Items.Clear;
end;

constructor TPersistentUserSettings.Create;
begin
  inherited;
  Items := TObjectDictionary <string, TpusApplication>.Create ([doOwnsValues]);
  AppName := ExtractFileName (ApplicationExeName);
end;

destructor TPersistentUserSettings.Destroy;
begin
  Items.Free;
  inherited;
end;

procedure TPersistentUserSettings.FromVariant(const v: variant);
var
  i : integer;
  pudApp : variant;
  app : TpusApplication;
  name: string;
begin
  try
    Items.Clear;

    for i := 0 to TDocVariantData (v).Count - 1 do
      begin
        pudApp := TDocVariantData (v).Values[i];
        name := TDocVariantData (pudApp).Names[i].AsString;
        app := TpusApplication.CreateFrom(TDocVariantData (pudApp).Values[i]);
        Items.Add (Name, app);
      end;
  finally
//    if now = 0
//       then WriteLn ('wtf');
  end;
end;

function TPersistentUserSettings.HasField(const Name: string): boolean;
var
  app: TpusApplication;
begin
  if Items.TryGetValue (AppName, app)
     then result := app.ContainsKey(Name)
     else result := false;
end;

function TPersistentUserSettings.Read(const Name: string): variant;
var
  app: TpusApplication;
begin
  if Items.TryGetValue (AppName, app)
     then result := app.Read(Name)
     else VarClear(result);
end;

{$IFDEF FIREMONKEY}
procedure TPersistentUserSettings.WriteFormPosition(Form: TCustomForm);
var
  v: variant;
//  FWinService: IFMXWindowService;
begin
  v := TDocVariant.NewObject([]);
  v.WindowState := Form.WindowState;
  v.Left := Form.Left;
  v.Top := Form.Top;
  v.Width := Form.Width;
  v.Height := Form.Height;

{  if not TPlatformServices.Current.SupportsPlatformService(IFMXWindowService, FWinService) then
      raise EUnsupportedPlatformService.Create('IFMXWindowService');
  var r := FWinService.GetWindowRect(Form);
  v.ServiceLeft := r.Left;
  v.ServiceTop  := r.Top;
  v.ServiceWidth := r.Width;
  v.ServiceHeight := r.Height;
}
  Write (Form.Name, v);
end;

procedure TPersistentUserSettings.ReadFormPosition (Form: TCommonCustomForm);
var
  v: variant;
begin
  if self = nil then exit;

  v := Read(Form.Name);

  if not VarIsEmptyOrNull (v) then
    begin
      Form.Left := max (0, v.Left);
      Form.Top  := max (0, V.Top);
      Form.Width := min (Round(Screen.Width), Round(V.Width));
      Form.Height := min (Round(Screen.Height), Round(V.Height));

      if Form.Left + Form.Width > Screen.Width
         then Form.Left := max (0, round(Screen.Width) - Form.Width);
      if Form.Left + Form.Width > Screen.Width
         then Form.Width := round(Screen.Width);

      if Form.Top + Form.Height > Screen.Height
         then Form.Top := max (0, round(Screen.Height) - Form.Height);
      if Form.Top + Form.Height > Screen.Height
         then Form.Height := round(Screen.Height);

      Form.WindowState := v.WindowState;
    end;
end;

function TPersistentUserSettings.ReadGridState(Grid: TStringGrid): boolean;
var
  i, iCol : integer;
  pair: variant;
begin
  result := false;

  try
    var v := Read(Grid.Owner.Name + '.' + Grid.Name);
    result := not VarIsEmptyOrNull (v);
    if result then
      begin
        for i := 0 to TDocVariantData (v).Count - 1 do
          begin
            pair := TDocVariantData (v).Values[i];
            for iCol := 0 to Grid.ColumnCount - 1 do
              if Grid.Columns[iCol].Name = pair.Name then
                begin
                  Grid.Columns[iCol].Width := pair.Width;
                  if i < Grid.ColumnCount
                     then Grid.Columns[iCol].Index := i;
                  break;
                end;
          end;
      end;
  except

  end;
end;

procedure TPersistentUserSettings.ReadGridStates(Grids: TArray<TStringGrid>);
begin
  for var Grid in Grids do ReadGridState(Grid);
end;

procedure TPersistentUserSettings.WriteGridState(Grid: TStringGrid);
var
  v, vPair: variant;
begin
  varClear (v);
  for var iCol := 0 to Grid.ColumnCount - 1 do
    begin
      vPair := TDocVariant.NewObject([]);
      vPair.Name := Grid.Columns[iCol].Name;
      vPair.Width := Grid.Columns[iCol].Width;
      TDocVariantData (v).AddItem(vPair);
    end;

  Write (Grid.Owner.Name + '.' + Grid.Name, v);
end;

procedure TPersistentUserSettings.WriteGridStates(Grids: TArray<TStringGrid>);
begin
  for var Grid in Grids do WriteGridState(Grid);
end;

procedure TPersistentUserSettings.WriteAlignedElement(Element: TControl);
var
  v: variant;
begin
  varClear (v);
  case Element.Align of
    TAlignLayout.Left,   TAlignLayout.MostLeft,
    TAlignLayout.Right,  TAlignLayout.MostRight: v := Element.Width;

    TAlignLayout.Top,    TAlignLayout.MostTop,
    TAlignLayout.Bottom, TAlignLayout.MostBottom: v := Element.Height;
    else exit;
    end;

  Write (format ('%s.%s.Size', [Element.Owner.Name, '.']), v);
end;

procedure TPersistentUserSettings.ReadAlignedElement(Element: TControl);
begin
  var v := Read (format ('%s.%s.Size', [Element.Owner.Name, '.']));
  if not VarIsEmptyOrNull (v) then
    case Element.Align of
      TAlignLayout.Left,   TAlignLayout.MostLeft,
      TAlignLayout.Right,  TAlignLayout.MostRight: Element.Width := v;

      TAlignLayout.Top,    TAlignLayout.MostTop,
      TAlignLayout.Bottom, TAlignLayout.MostBottom: Element.Height := v;
      else exit;
      end;
end;


{$ELSE}

procedure TPersistentUserSettings.WriteFormPosition(Form: TCustomForm);
var
  WindowPlacement: TWindowPlacement;
  R: TRect;
  v: variant;
begin
  WindowPlacement.Length := SizeOf(WindowPlacement);
  Win32Check(GetWindowPlacement(Form.Handle, @WindowPlacement));
  R := WindowPlacement.rcNormalPosition;

  v := TDocVariant.NewObject([]);
  v.WindowState := Form.WindowState;
  v.Left := R.Left;
  v.Top := R.Top;
  v.Width := R.Width;
  v.Height := R.Height;
  Write (Form.Name, v);
end;

procedure TPersistentUserSettings.ReadFormPosition (Form: TCustomForm);
var
  WindowPlacement: TWindowPlacement;
//  R: TRect;
begin
  var v := Read(Form.Name);
  if not VarIsEmptyOrNull (v) then
    begin
      WindowPlacement.Length := SizeOf(WindowPlacement);
      Win32Check(GetWindowPlacement(Form.Handle, @WindowPlacement));
      WindowPlacement.rcNormalPosition.Left := V.Left;
      WindowPlacement.rcNormalPosition.Top := V.Top;
      WindowPlacement.rcNormalPosition.Width := V.Width;
      WindowPlacement.rcNormalPosition.Height := V.Height;

      Win32Check(SetWindowPlacement(Form.Handle, WindowPlacement));

      Form.WindowState := v.WindowState;
    end;
end;

procedure TPersistentUserSettings.ReadGridState(Grid: TStringGrid);
begin

end;

procedure TPersistentUserSettings.WriteGridState(Grid: TStringGrid);
begin

end;

{$ENDIF}

function TPersistentUserSettings.ToVariant: variant;
var
  pudApp : variant;
begin
  varClear (result);

  for var pair in Items do
    begin
      pudApp := TDocVariant.NewObject([pair.Key, pair.Value.ToVariant]);
      TDocVariantData (result).AddItem(pudApp);
    end;
end;

procedure TPersistentUserSettings.Write(const Name: string; Values: variant);
var
  app: TpusApplication;
begin
  if not Items.TryGetValue (AppName, app) then
    begin
      app := TpusApplication.Create;
      Items.Add (AppName, app);
    end;

   app.Write(Name, Values)
end;

{ TpudApplication }

constructor TpusApplication.CreateFrom(const v: variant);
begin
  inherited Create;

  for var i := 0 to TDocVariantData (v).Count - 1 do
    begin
      var pusElement := TDocVariantData (v).Values[i];
      Add (TDocVariantData (pusElement).Names[0].AsString, TDocVariantData (pusElement).Values[0]);
    end;
end;

function TpusApplication.Read(const Name: string): variant;
begin
  if not TryGetValue (Name, result)
    then VarClear(result);
end;

function TpusApplication.ToVariant: variant;
begin
  varClear (result);

  for var pair in self do
    begin
      var pudSettings := TDocVariant.NewObject([pair.Key, pair.Value]);
      TDocVariantData (result).AddItem(pudSettings);
    end;
end;

procedure TpusApplication.Write(const Name: string; Values: variant);
begin
  AddOrSetValue(Name, Values);
end;

{initialization
  pus := TPersistentUserSettings.Create;
  StoreFormPos;
  var v := pus.ToVariant;
  pus.FromVariant(v);
  RestoreFormPos;
}

end.
