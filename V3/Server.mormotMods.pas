﻿unit Server.mormotMods;

{$IFNDEF SERVER}
Dodati SERVER u conditional defines u serveru
Ovaj unit ne treba da bude included u clientu
{$ENDIF}

{
  Info: Encryption happens in TWebSocketProcess.SendFrame
  actual bytes sent done in TCrtSocket.TrySndLow
}

interface

uses
  {$IFDEF Firemonkey}
  not to be used in client
  {$ENDIF}

  SysUtils, Contnrs, System.Generics.Collections, Classes, System.Types, System.Generics.Defaults,
  Variants, StrUtils, math, DateUtils, System.TypInfo, RTTI, mORMotSQLite3, SyncObjs,
  SynCrtSock, mORMotService,
  SqlRecords, Validation, IOUtils, SynLog, ZConnection, ZSqlProcessor, ZDataSet, Data.DB,
  {$ifdef MSWINDOWS}
  WinApi.Windows, ShellAPI,//for inline of synlocker...
  {$endif}
  //todo rework:
  SynDBZeos, SynCommons, SynDB, mormot, SynTable,
  Common.Utils, MormotTypes, MormotMods, MormotAttributes;

  {$SCOPEDENUMS ON}
const
  NULL_ID = 0;

  FB_ORDER_TRIGGER = 'recreate trigger % FOR % ACTIVE BEFORE % AS ';
  FB_ORDER_BI_TRIGGER_CODE =  'begin update % set % = % + 1, updating_order = 1 where % >= new.% %; end ;';
  FB_ORDER_BD_TRIGGER_CODE =  'begin update % set % = % - 1, updating_order = 1 where % > % %; end ;';
  FB_ORDER_BU_TRIGGER_CODE =  'begin ' +
                              '  if (new.updating_order IS NULL) then ' +
                              '  begin ' +
                              '    if (new.%f > old.%f) then update %t set %f = %f - 1, updating_order = 1 ' +
                              '       where %f > old.%f and %f <= new.%f %c; ' +
                              '    if (new.%f < old.%f) then update %t set %f = %f + 1, updating_order = 1 ' +
                              '     where %f >= new.%f and %f < old.%f %c; ' +
                              '  end ' +
                              '  new.updating_order = NULL;'+
                              'end ;';

  // "renamed" syn log constants
  sllCommonInfo    = sllCustom1;
  sllCommonWarning = sllCustom2;
  sllCommonError   = sllCustom3;

type

  TServiceSingleEx = class (TServiceSingle)
  private
    class procedure DoIUAndHalt (Install: boolean);
  public
    constructor Create; reintroduce; virtual;
    /// launch as Console application
    constructor CreateAsConsole; reintroduce;

    class var Name, DisplayName: string;
    class procedure Setup (const AName, ADisplayName: string);
    class procedure InstallAndHalt;
    class procedure UnInstallAndHalt;
    class procedure CheckRunParameters;
  end;

  TIDGenerator = class
  protected
    CriticalSection: TCriticalSection;
    FCurrentID: TID;
  public
    constructor Create (Server: TSQLRestServerDB; SqlRecordClass: TSQLRecordClass);
    function NextID: TID;
  end;

  TServerSettings = class (TPersistent)
  private
    FDBEngine: TSQLDBDefinition;
    FServerName, FDBPath, FUsername, FPassword: RawUTF8;
    FLibraryLocation: TFileName;
    FConsoleLog, FAppendExePath: boolean;
    FPort: RawUTF8;
    FCallbacks: boolean;
    FExecLog: boolean;
    FReportsPath: RawUTF8;
    FArchiveAfterDays: integer;
    FRootName: RawUTF8;
    FPublicFilesPath: RawUTF8;
    FChangeLog: boolean;
  public
    FileName: string;
    constructor Create (const AFileName: TFileName); virtual;
    constructor CreateForSqlite;

    function DatabaseExists: boolean;
    procedure CreateDatabase;
    procedure CreateDatabaseIfNeeded;
  published
    property DBEngine: TSQLDBDefinition read FDBEngine write FDBEngine;
    property ServerName: RawUTF8 read FServerName write FServerName; // i.e. network host name
    property LibraryLocation: TFileName read FLibraryLocation write FLibraryLocation;
    property AppendExePath: boolean read FAppendExePath write FAppendExePath;
    property DBPath: RawUTF8 read FDBPath write FDBPath;
    property ReportsPath: RawUTF8 read FReportsPath write FReportsPath;
    property PublicFilesPath: RawUTF8 read FPublicFilesPath write FPublicFilesPath;
    property Username: RawUTF8 read FUsername write FUsername;
    property Password: RawUTF8 read FPassword write FPassword;
    property ConsoleLog: boolean read FConsoleLog write FConsoleLog;
    property Port: RawUTF8 read FPort write FPort;
    property Callbacks: boolean read FCallbacks write FCallbacks;
    property ExecLog: boolean read FExecLog write FExecLog;
    property ChangeLog: boolean read FChangeLog write FChangeLog;
    property ArchiveAfterDays: integer read FArchiveAfterDays write FArchiveAfterDays;
    property RootName: RawUTF8 read FRootName write FRootName;
  end;

  TSQLDBZEOSConnectionPropertiesEx = class (TSQLDBZEOSConnectionProperties) //TODO make helper?
  private
    procedure DeleteBeforeDeleteTrigger(MasterTable, DetailTable: TSqlRecordClass);
    procedure CreateCascadeDeleteRestrictedTrigger(MasterTable, DetailTable: TSqlRecordClass; ForeignKey: string = '');
    procedure CreateCascadeDeleteAllowedTrigger(MasterTable, DetailTable: TSqlRecordClass; ForeignKey: string = '');
    // returns record count
    function ExecuteNoResult(const aSQL: RawUTF8; const Params: array of const): integer; reintroduce;

    //procedure DeleteBeforeDeleteTrigger(MasterTable, DetailTable: TSqlRecordClass);
  public
    function IndexExists(const aTableName: RawUTF8; const aFieldNames: array of RawUTF8; const aIndexName: RawUTF8): boolean;
    function ViewExists(const Name: RawUTF8): boolean;
    function TableExists(const Name: RawUTF8): boolean;
    function FieldExists (const TableName, FieldName: RawUTF8): boolean;
    function ForeignKeyConstraintExists (MasterTable, DetailTable: TSqlRecordClass; ForeignKey: RawUTF8 = ''): boolean;
    function ForeignKeyConstraintName (MasterTable, DetailTable: TSqlRecordClass;
             ForeignKey: RawUTF8 = ''): RawUTF8;
    procedure CreateForeignKeyConstraint(MasterTable, DetailTable: TSqlRecordClass;
              Action: TReferentialAction; ForeignKey: RawUTF8 = '');

    function SQLAddIndex (const aTableName: RawUTF8; const AFieldNames: array of RawUTF8; AUnique: boolean;
                          ADescending: boolean=false; const AIndexName: RawUTF8=''): RawUTF8; override;
    function SQLAddFullTextIndex (const ATableName, AFieldNames, ALanguage: RawUTF8; AUnique: boolean;
                          ADescending: boolean=false; AIndexName: RawUTF8=''): RawUTF8;
    function CheckVersion(ModelHash: RawUTF8; var CurrentVersion: integer): boolean;
    procedure ExecuteResourceStringSQL (const Identifier: string); overload;
    procedure ExecuteResourceStringSQL (const Identifiers: array of string); overload;
    procedure DropView (const Name: RawUTF8);
    procedure DropViews (const Names: array of RawUTF8); overload;
    procedure DropViews (Model: TSqlModelEx); overload;
    procedure CreateBlankViews(Model: TSqlModel);
    procedure CreateView (const sql: RawUTF8; SqlRecordClass: TSqlRecordClass); overload;
    procedure CreateView(SqlRecordClass: TSqlRecordExClass); overload;
    //procedure CreateViews(Views: array of TSqlRecordExClass);
    procedure CreateCalculatedFields(Model: TSqlModelEx); virtual;
    procedure CreateViews(Model: TSqlModelEx);
    procedure CreateIndexes(Model: TSqlModelEx);
    procedure CopyTableData (Dest: TSqlRecordClass; const Source: RawUTF8);
    function GetCalculatedFieldDefinition(const TableName, FieldName: string): string;
    //procedure CreateViews(Model: TSqlModel); // doesnt work because my code fails to determine class parent
  end;

  TSQLRestHelper = class helper for TSQLRest
  class var a: TObject;
  class constructor create;
  public
    function Delete(rec: TSQLRecord): boolean; overload;
    function TableHasRowsWhere(Table: TSQLRecordClass; Where: RawUTF8): boolean;
  end;

  TFunRecordLocker = class
    type
      TLockedRecord = record
        TimeStamp: TDateTime;
        Session: cardinal;
        SqlRecordClass: TSQLRecordClass;
        ID: TID;
        procedure Renew;
        constructor Create (ASqlRecordClass: TSQLRecordClass; AID: TID; UnknownSession: boolean = false);
      end;
  private
    function DoLock(cl: TSQLRecordClass; ID: TID; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean;
  protected
    SynLock: TSynLocker;
    Records: TList<TLockedRecord>;
    function FFind (cl: TSQLRecordClass; ID: TID): integer;
  public
    constructor Create;
    destructor Destroy; override;

    function IsLocked(rec: TSQLRecord): boolean; overload;
    function IsLocked(cl: TSQLRecordClass; ID: TID): boolean; overload;

    function Lock(rec: TSQLRecord; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean; overload;
    function Lock(cl: TSQLRecordClass; ID: TID; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean; overload;

    function Unlock(cl: TSQLRecordClass; ID: TID): boolean; overload;
    function Unlock(rec: TSQLRecord): boolean; overload;

    function RenewLocks: integer;
  end;

  TDBChange = record // for change callback/notif... one day...
    SqlRecordClass: TSQLRecordClass;
    ID: TID;
    Operation: TCRUD;
    Session: cardinal;
    TimeStamp: TDateTime;
  end;

  TRecordOrderInfo = record
    Field, GroupField{, OrderGroupFieldCondition}: RawUTF8;
    Value, GroupFieldValue: variant;
    RecordFound, OrderExists: boolean;
  end;

  TTransactionOperation = (Start, Commit, Rollback, Done);

  TSQLRestServerDBEx = class (TSQLRestServerDB)
  private
    procedure DoExecLog (const TableName, Content: RawUTF8; const Operation: TCrud; const Outcome: TOutcome); overload;
    procedure DoExecLog(Value: TSqlRecord; const Operation: TCrud; const Outcome: TOutcome); overload;
    procedure DoExecLog(Table: TSQLRecordClass; ID: TID; const Operation: TCrud; const Outcome: TOutcome); overload;
    procedure ReplaceRowID(var sql: RawUTF8);
    function CheckCUFailure(rec: TSQLRecord; Outcome: TOutcome): TOutcome;
    procedure LogTransactionInfo(Operation: TTransactionOperation; const Reason: string);
//    function DirectRetrieve(const SQLWhere: RawUTF8; Value: TSQLRecord; const aCustomFieldsCSV: RawUTF8=''): TOutcome;
  protected
    FModel : TSQLModelEx;
    FDB: TSQLDBZEOSConnectionPropertiesEx;
    dicTriggers: TDictionary <string, string>;
    FExecLogStream: TStream;
    FExecLogWriter : TStreamWriter;
    slSqlDeleteObsoleteFieldsTables, slObsoleteFieldAttributes: TStringList;
    TranCount: integer;
    TransactionRequest: TSQLRestServerURIContext;

    procedure CreateTriggers; {virtual;} // unused? delete?
    procedure CopyAggregateRecords(const AggregateTableName, MasterIDName, DetailIDName: string; ExistingMasterID, NewMasterID: TID);
    function InternalAdd(Value: TSQLRecord; SendData: boolean; CustomFields: PSQLFieldBits; ForceID,
                                            DoNotAutoComputeFields: boolean): TID; override;
    procedure AddChangeLog (Table: TSQLRecordClass; const ID: TID; const Operation: TCrud; const json: RawUTF8);
    //function GetRecordOrderInfo(const model: TSQLModelEx; const rec: TSqlRecord): TRecordOrderInfo;
  public
    ImagePath: TFileName;
    RecordLocker: TFunRecordLocker;
    ImportInProgress, UseRecordLocker: boolean;
    DBConnectionProperties: TSQLDBConnectionPropertiesThreadSafe;
    IDGenerators: TDictionary <TSQLRecordClass, TIDGenerator>;
    AllowedCascades, RestrictedCascades: TCascades;
    DBChanges: TList<TDBChange>;
    Settings: TServerSettings;
    SimulateOneError, SimulateOneException: boolean;
    DirectRead: boolean;
//    constructor Create(AModel: TSQLModel; AHandleUserAuthentication: boolean=false; AOwnDB: boolean=false); reintroduce;
    constructor Create(AModel: TSQLModelEx; const aDBFileName: TFileName; ADBConnectionProperties: TSQLDBZEOSConnectionPropertiesEx; aHandleUserAuthentication: boolean=false; AOwnDB: boolean=false);
//    constructor Create(AModel: TSQLModel; ADB: TSQLDataBase; AZeosDB: TSQLDBZEOSConnectionPropertiesEx; aHandleUserAuthentication: boolean=false; AOwnDB: boolean=false); reintroduce; overload;

    destructor Destroy; override;

    function MultiFieldValuesEx (Table: TSQLRecordClass; const FieldNames, WhereClause: RawUTF8;
                                 var jsonRecords: RawUTF8): TOutcome; overload;
    function MultiFieldValuesEx (Table: TSQLRecordClass; const FieldNames, WhereClauseFormat: RawUTF8;
                                 const BoundsSQLWhere: array of const; var jsonRecords: RawUTF8): TOutcome; overload;
    function MultiFieldValuesDirect(Table: TSQLRecordClass; var sql, jsonRecords: RawUTF8): TOutcome;

    procedure InitializeTables(Options: TSQLInitializeTableOptions); override;

    function ValidateUniqueIfNotNull (rec: TSQLRecord): string;
    procedure CheckValidation (Value: TSQLRecord; var Outcome: TOutcome);
    function DirectFindID(SqlRecordClass: TSqlRecordClass; const sqlWhere: RawUTF8): TOutcome; overload;
    function DirectFindID(SqlRecordClass: TSqlRecordClass; const sqlWhere: string): TOutcome; overload;

    function RestrictIfTableHasForeignKey(Table: TSQLRecordClass; const FieldName: RawUTF8; FKID: TID; var Outcome: TOutcome): boolean;
    function TableHasForeignKey(Table: TSQLRecordClass; const FKName: RawUtf8; FKID: TID): boolean;
    function BatchSend(Batch: TSQLRestBatch; var Outcome: TOutcome): integer; overload;
    function ReadLock(rec: TSQLRecord; var Outcome: TOutcome): boolean; overload;
    function ReadLock(Table: TSQLRecordClass; aID: TID; var Outcome: TOutcome): boolean; overload;
    function UnLock(Table: TSQLRecordClass; aID: TID): boolean; overload; override;
    function UnLock(rec: TSQLRecord): boolean; overload;

    function Retrieve(aID: TID; Value: TSQLRecord; var Outcome: TOutcome; ForUpdate: boolean=false): boolean; reintroduce; overload;
    function Retrieve(Value: TSQLRecord; var Outcome: TOutcome; Lock: boolean = false): boolean; overload;
    function Retrieve(Value: TSQLRecord): boolean; overload;
    function Retrieve(const SQLWhere: RawUTF8; Value: TSQLRecord; const aCustomFieldsCSV: RawUTF8=''): TOutcome; reintroduce; overload;

    function RetrieveAsJson (Table: TSQLRecordClass; const ID: TID; var json: RawUTF8): TOutcome;
    function FindID (RecordClass: TSQLRecordClass; Condition: RawUTF8): TID;

    function UpdateAndUnlock(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''; DoNotAutoComputeFields: boolean=false): TOutcome;
    function Kick(AUserID: TID): boolean;
    function RetrieveSqlRecords<T: TSQLRecord>(const FormatSQLWhere: RawUTF8; const BoundsSQLWhere: array of const;
                                               const Fields: RawUTF8 = ''): TSQLRecords<T>; overload;
//    function RetrieveSqlRecordsDirect<T: TSQLRecord>(const FormatSQLWhere: RawUTF8; const BoundsSQLWhere: array of const;
//                                               const Fields: RawUTF8 = ''): TSQLRecords<T>;

    function RetrieveSqlRecords<T: TSQLRecord>: TSQLRecords; overload;
    function RetrieveSqlRecords (RecordClass: TSQLRecordClass; const FormatSQLWhere: RawUTF8;
                                 const BoundsSQLWhere: array of const; const Fields: RawUTF8 = ''): TSQLRecords; overload;
    function Add(Value: TSQLRecord; SendData: boolean = true; ForceID: boolean=false; DoNotAutoComputeFields: boolean=false): TOutcome; reintroduce; overload;
    function Add(Value: TSQLRecord; Batch: TSQLRestBatch): TOutcome; reintroduce; overload;

    function AddOrCrash(Value: TSQLRecord; SendData: boolean = true; ForceID: boolean=false; DoNotAutoComputeFields: boolean=false): TOutcome;

    function AddWithFields(Value: TSQLRecord; Fields: RawUTF8): TOutcome;
    function AddMany (Field: TSQLRecordMany; SourceID: TID; IDs: TIDDynArray; Batch: TSQLRestBatch = nil): TOutcome;
    function ReplaceMany (Field: TSQLRecordMany; SourceID: TID; IDs: TIDDynArray): TOutcome;

    function UpdateEx(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''; DoNotAutoComputeFields: boolean=false): TOutcome; overload;
    function UpdateEx(Value: TSQLRecord; const CustomFields: TSQLFieldBits; DoNotAutoComputeFields: boolean): TOutcome; overload;
    function Update(Value: TSQLRecord; const CustomFields: TSQLFieldBits=[]; DoNotAutoComputeFields: boolean=false): boolean; override;
    function UpdateOrCrash(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''): TOutcome;
    //function RecordCanBeUpdated(Table: TSQLRecordClass; ID: TID; Action: TSQLEvent; ErrorMsg: PRawUTF8 = nil): boolean; override;

    function DeleteEx(Table: TSQLRecordClass; ID: TID): TOutcome; reintroduce; overload;
    function DeleteEx(rec: TSQLRecord): TOutcome; reintroduce; overload;
    function Delete(Table: TSQLRecordClass; ID: TID): boolean; override;
    function DeleteDetailsRecursively (Table: TSQLRecordClass; ID: TID): TOutcome;
    function DeleteDetailRecords (Tables: TArray<TSQLRecordClass>; FKName: RawUTF8; ID: TID): TOutcome; // delete details of this record

    function AddIDGenerator (Table: TSQLRecordClass): TIDGenerator;
    function RetrieveListObjArrayEx (var ObjArray; Table: TSQLRecordClass; Parameters: TQueryParameters;
                                     const CustomFieldsCSV: RawUTF8=''): boolean;
    function RetrieveListObjArrayDirect (var ObjArray; Table: TSQLRecordClass; Parameters: TQueryParameters;
                                     const CustomFieldsCSV: RawUTF8=''): boolean; overload;
    function RetrieveListObjArrayDirect (var ObjArray; Table: TSQLRecordClass; const sqlWhere: RawUTF8;
                                     const CustomFieldsCSV: RawUTF8=''): boolean; overload;

    //procedure AddFunction (Name: RawUTF8);
    // transaction control
    procedure LockAndStartTransaction (const Reason: string = '');
    procedure CommitAndUnlock (const Reason: string = '');
    procedure RollbackAndUnlock (const Reason: string = '');
    procedure UnLockAndEndTransaction (Success: boolean; const Reason: string = '');

    procedure RegisterDBChange (const SqlRecordClass: TSqlRecordClass; const ID: TID; const Operation: TCRUD);
    function CreateBatch: TSQLRestBatch;
    function CheckUniqueFields (rec: TSqlRecord): TOutcome; overload;
    procedure GenerateID (rec: TSqlRecord);
    procedure CreateIndexes; virtual;
    procedure CreateIndexesFromAttributes (Tables: TSqlRecordClassDynArray);
    procedure CreateReflectionTriggers;
    procedure CheckForObsoleteSchema (ModelFull: TSqlModelEx);
    procedure CheckTriggers;
    function EnumValueAdded (Table: TSqlRecordClass; const FieldName: RawUTF8; const NewValue, NewVersion: integer): integer;
    procedure UpdateDBVersion(ModelHash: RawUTF8; NewVersion: integer = 0);
    function Sessions: TSynObjectListLocked;

    procedure EnforceMirrorFieldValues (ConPropEx: TSQLDBZEOSConnectionPropertiesEx);
    procedure GetDBStats (out Tables: TRawUTF8DynArray; out Counts: TIDDynArray;
                                       out RequiredAuthLevels: TRequiredAuthLevelsArray);
    procedure CheckDevAccounts;
    procedure CreateEnumFunctions;

    procedure WipeAllTables;
    procedure Wipe (Tables: TArray<TSqlRecordClass>); overload;
    procedure Wipe (Table: TSqlRecordClass); overload;
  published
    procedure Sum(Ctxt: TSQLRestServerURIContext);
    procedure Image(Ctxt: TSQLRestServerURIContext);
    property Model: TSQLModelEx read FModel;

    property DataBase: TSQLDBZEOSConnectionPropertiesEx read FDB;
  end;

  TMormotServiceImplementer = class (TInterfacedObject)
  protected
    function FGetFile (const FileName: TFileName): TServiceCustomAnswer;
  public
//    class var DB: TSQLDBZEOSConnectionPropertiesEx;
    class var RestServer: TSQLRestServerDBEx;
    class function AuthUserClass: TSqlAuthUserExClass;
//    class var AuthUserClass: TSqlAuthUserExClass;
    function SessionUserID: TID;
    function ValidateParameters(const Parameters: TQueryParameters): boolean;
    function Model: TSqlModelEx; virtual;
    procedure LogWarning (const s: string);
  end;

//  TServerInfo = class (TPersistent)
//  private
//    FVersion: RawUTF8;
//    FExeName: RawUTF8;
//    FLogFile: RawUTF8;
//  published
//    property ExeName: RawUTF8 read FExeName write FExeName;
//    property Version: RawUTF8 read FVersion write FVersion;
//    property LogFile: RawUTF8 read FLogFile write FLogFile;
//  end;

  TMormotServiceInfo = class (TPersistent)
  private
    FSettingsPath: RawUTF8;
    FExePath: RawUTF8;
    FServiceName: RawUTF8;
    FLogPath: RawUTF8;
//    FRegistryPath: RawUTF8;
//    FCompanyName: RawUTF8;
    FServiceDisplayName: RawUTF8;
    FLoaded: boolean;
//    function RegistryKey: string;
//    procedure SetCompanyName(const Value: RawUTF8);  // combine path with service name i.e. path/ServiceName
  public
    property Loaded: boolean read FLoaded;
    constructor CreateFromFile (AFileName: TFileName = '');
    procedure Write(AFileName: TFileName = '');
    constructor Create (const AServiceName, AServiceDisplayName, ASettingsPath: string);
//    property RegistryPath: RawUTF8 read FRegistryPath write FRegistryPath;
//    property CompanyName: RawUTF8 read FCompanyName write SetCompanyName;
  published
    property ExePath: RawUTF8 read FExePath write FExePath;
    property ServiceName: RawUTF8 read FServiceName write FServiceName;
    property ServiceDisplayName: RawUTF8 read FServiceDisplayName write FServiceDisplayName;
    property SettingsPath: RawUTF8 read FSettingsPath write FSettingsPath;
    property LogPath: RawUTF8 read FLogPath write FLogPath;
  end;

  TServerManagerService = class (TMormotServiceImplementer, IServerManagerService)
  protected
//    function ServerName: RawUTF8; virtual;
//    procedure GetInfo (out Info: TServerInfo);
    procedure SimulateOneError;
    procedure SimulateOneException;
    function RedirectFKs (const TableName: RawUTF8; const ValidID, InvalidID: TID; DeleteInvalid: boolean): TOutcome;
    function GetFKInfo (const TableName: RawUTF8; const ID: TID): TOutcome;
    function CustomRequest (const Command: RawUTF8; const Parameters: variant; const Content: RawUTF8;
                            out ResponseParameters: variant; out ResponseContent: RawUTF8): TOutcome; virtual;
  end;

  TSQLRestServerAuthenticationEx = class (TSQLRestServerAuthenticationDefault)
  protected
    function CheckPassword(Ctxt: TSQLRestServerURIContext;
      User: TSQLAuthUser; const aClientNonce, aPassWord: RawUTF8): boolean; override;
  end;

  procedure InstallService(const ServiceName, DisplayName, LoadOrder: PChar; const FileName: string; const Port: SockString; const Silent: Boolean);
  // modified function from SynTable;
  function ConsoleWaitForOneOf (const Keys: array of word): cardinal;
//  procedure ConsoleWarning (const s: string);
  procedure ModelSanityCheck (Model: TSqlModel);
  procedure PGBackupDatabase(const PGDumpPath, HostName, UserName, Password, DBName, BackupFilePath: string);
  procedure PGRestoreDatabase(const PGRestorePath, HostName, UserName, Password, BackupFilePath: string; Create: boolean = false);
  procedure DumpRows (Rows: ISQLDBRows);
  procedure PrepareLogFolder;
  procedure ConsoleAfterServerStart (RestServer: TSQLRestServerDBEx; const Port: RawUTF8);
  procedure CheckIfVerbose;
  procedure LogServerVersion;
  procedure LogPrivileges;
  procedure LogFormats;

var
  SqlRecordLockTimeout: UInt16 = 30; //seconds
  ServiceInfo: TMormotServiceInfo;

implementation

uses Winapi.Messages, Common.Implementations;

function TSQLRestServerAuthenticationEx.CheckPassword(Ctxt: TSQLRestServerURIContext;
         User: TSQLAuthUser; const aClientNonce, aPassWord: RawUTF8): boolean;
begin
  if User is TSqlAuthUserEx
    then result := inherited and TSqlAuthUserEx (User).Active
    else result := inherited;
end;

procedure PrepareLogFolder;
begin
  var s := TPath.Combine (ApplicationExeFolder, 'Logs');
  ForceDirectories(s);
  TSQLLog.Family.DestinationPath := s;
end;

procedure CheckIfVerbose;
begin
  if FindCmdLineSwitch ('verbose') then
    begin
      TSQLLog.Family.Level := LOG_VERBOSE;
      TSQLLog.Family.EchoToConsole := LOG_VERBOSE;
    end;
end;

procedure ConsoleAfterServerStart (RestServer: TSQLRestServerDBEx; const Port: RawUTF8);
begin
  TSQLLog.Add.Log (TSynLogInfo.sllCustom1, 'Server running on ' +
                   StringToUTF8(GetIPAddress) + ':' + Port);

  writeln('');

  {$IFDEF DEBUG}
  var key: char;

  while true do
    begin
      TextColor(ccLightGray);
      writeln('Press [1/2] [Enter] to simulate one error/exception.'); //, F3 to pause/unpause doesnt work
      writeln('Press just [Enter] to close the server.'); //, F3 to pause/unpause doesnt work

      ReadLn (key);
//      key := ConsoleWaitForOneOf ([VK_F1, VK_RETURN, VK_F2, VK_F3]);
      case key of
        '1': begin
                 RestServer.SimulateOneError := true;
                 TextColor(ccLightRed);
                 writeln('OK, Simulating one error');
               end;
        '2': begin
                 RestServer.SimulateOneException := true;
                 TextColor(ccLightRed);
                 writeln('OK, Simulating one exception');
               end;
        else break;
{        VK_F3: begin
               writeln('Paused');
  //                       httpServer.HttpServer.Paused := true;
               key := ConsoleWaitForOneOf ([VK_RETURN, VK_F3]);
  //                       httpServer.HttpServer.Paused := false;
               if key = VK_F3
                  then writeln('Unpaused')
                  else break;
             end;}
      end;
    end;
  {$ELSE}
   writeln('Press [Enter] to close the server');
   ReadLn;
  {$ENDIF}

//  ConsoleWaitForEnterKey; // ReadLn if you do not use main thread execution

  writeln('');
  writeln('Closing server, please wait...');
  {$IFDEF DEBUG}
  IsConsole := False; // to report mem leaks
  {$ENDIF}
end;

{ TSQLDBZEOSConnectionPropertiesEx }

function TSQLDBZEOSConnectionPropertiesEx.ExecuteNoResult(const aSQL: RawUTF8; const Params: array of const): integer;
begin
  if aSQL = '' then raise Exception.Create('SQL is empty!');
  result := inherited;
end;

procedure TSQLDBZEOSConnectionPropertiesEx.CreateView(SqlRecordClass: TSqlRecordExClass);
var
  sql: RawUTF8;
begin
  try
    if ViewExists (SqlRecordClass.SQLTableName)
      then begin
             raise Exception.Create ('View already exists: ' + SqlRecordClass.SQLTableName.AsString);
             exit;
           end;
    sql := SqlRecordClass.ViewSQL;
    if sql = ''
       then raise Exception.Create('View SQL is empty for ' + SqlRecordClass.SQLTableName.AsString);
    TSQLLog.Add.Log (sllInfo,sql);
    ExecuteNoResult(sql, []);
  except on e: exception do
    if TableExists(SqlRecordClass.SQLTableName)
      then raise Exception.Create(SqlRecordClass.SQLTableName.AsString + ' exists as table, cant create view.')
      else raise Exception.Create ('Failed to create view for ' + SqlRecordClass.SQLTableName.AsString + #13#13 +
                                        SqlRecordClass.ViewSQL.AsString  + #13#13 + e.Message);
  end;
end;

procedure TSQLDBZEOSConnectionPropertiesEx.CreateView(const sql: RawUTF8; SqlRecordClass: TSqlRecordClass);
begin
  try
    ExecuteNoResult(sql, []);
  except on e: exception do
    raise Exception.Create ('Failed to create view for ' + SqlRecordClass.SQLTableName.AsString + #13#13 +
                                        sql.AsString  + #13#13 + e.Message);
  end;
end;

//procedure TSQLDBZEOSConnectionPropertiesEx.CreateViews(Views: array of TSqlRecordExClass);
//begin
//  for var View in Views do
//    CreateView (View);
//end;

procedure TSQLDBZEOSConnectionPropertiesEx.CreateViews(Model: TSqlModelEx);
begin
  for var Table in Model.Tables do
    if Table.IsView then CreateView (TSqlRecordExClass(Table));
end;

procedure TSQLDBZEOSConnectionPropertiesEx.DropView(const Name: RawUTF8);
begin
  if TableExists (Name) then raise Exception.Create('Cant drop view because it exists as table: ' + Name.AsString);

  case fDBMS of
   dSQLite    : ExecuteNoResult('DROP VIEW IF EXISTS ' + Name, []);
   dPostgreSQL: ExecuteNoResult('DROP VIEW IF EXISTS ' + Name + ' CASCADE', []);
   else
      if (fDBMS <> dFirebird) or ViewExists(Name)
        then
          try ExecuteNoResult('DROP VIEW ' + Name, []); except end;
  end;
end;

procedure TSQLDBZEOSConnectionPropertiesEx.DropViews(const Names: array of RawUTF8);
begin
  for var Name in Names do DropView (Name);
end;

procedure TSQLDBZEOSConnectionPropertiesEx.DropViews(Model: TSqlModelEx);
begin
  var ctx := TRttiContext.Create;
  try
    for var table in model.Tables do
      if table.IsView then
      try
        if not Model.Contains (table) then
           raise Exception.Create(table.SQLTableName.AsString + ' is not part of the model!');

        DropView (table.SQLTableName);
      except

      end;
  finally
    ctx.Free;
  end;
end;

function TSQLDBZEOSConnectionPropertiesEx.CheckVersion(ModelHash: RawUTF8; var CurrentVersion: integer): boolean;
var
  rows: ISQLDBRows;
begin
  if not TableExists ('databaseinfo') or not FieldExists ('databaseinfo', 'versionnumber')
     then begin
            TSQLLog.Add.Log(sllCommonWarning, 'DB Version table not found...',[]);
            exit (false);
          end;

  try
    rows := Execute (FormatUTF8 ('SELECT ModelHash, VersionNumber FROM DatabaseInfo LIMIT 1', []), []);
  except
    TSQLLog.Add.Log(sllCommonWarning, 'DB Version table invalid...',[]);
    exit(false); // ignore the crash
  end;

  try
    CurrentVersion := 0;
    if not rows.Step() then exit (false);
    CurrentVersion := rows.ColumnInt('VersionNumber');
    if CurrentVersion <> TSqlDatabaseInfo.ExpectedVersion then
      begin
        TSQLLog.Add.Log (sllCommonWarning, 'Old DB version=% detected, expected=%',
                        [CurrentVersion, TSqlDatabaseInfo.ExpectedVersion]);
        exit (false);
      end;

    var CurrentHash := rows.ColumnUTF8('ModelHash');
    if CurrentHash <> ModelHash then
      begin
        TSQLLog.Add.Log (sllCommonWarning, 'DB Hash changed', []);
        exit (false);
      end;
    result := true;
  finally
    rows.ReleaseRows;
  end;
end;

procedure TSQLDBZEOSConnectionPropertiesEx.CopyTableData(Dest: TSqlRecordClass; const Source: RawUTF8);
var
  FieldList: RawUTF8;
begin
  FieldList := '';
  for var i := 0 to Dest.RecordProps.Fields.Count - 1 do
      FieldList := FieldList + Dest.RecordProps.Fields.Items[i].Name + ',';
  FieldList := copy (FieldList, 1, length(FieldList)-1);
  ExecuteNoResult(FormatUTF8 ('INSERT INTO % (%) select % from %', [Dest.SQLTableName, FieldList, FieldList, Source]), []);
end;

procedure TSQLDBZEOSConnectionPropertiesEx.CreateBlankViews(Model: TSqlModel);
var
  TableType: TRttiType;
begin
  var ctx := TRttiContext.Create;
  try
    for var table in model.Tables do
      begin
        TableType := ctx.GetType(table);
        for var Attribute in TableType.GetAttributes do
          if Attribute is ViewAttribute then
            begin
              try ExecuteNoResult(FormatUTF8 ('CREATE VIEW % as select null as dummy from AUTHGROUP where 0=1', [table.SQLTableName]), []); except end;
              break;
            end;
      end;
  finally
    ctx.Free;
  end;
end;

procedure TSQLDBZEOSConnectionPropertiesEx.CreateForeignKeyConstraint(MasterTable, DetailTable: TSqlRecordClass;
         Action: TReferentialAction; ForeignKey: RawUTF8 = '');
var
  sql, cname: RawUTF8;
begin
  cname := ForeignKeyConstraintName (MasterTable, DetailTable, ForeignKey);
  sql := FormatUTF8 ('ALTER TABLE % DROP CONSTRAINT IF EXISTS % ', [DetailTable.SQLTableName, cname]);
  ExecuteNoResult (sql, []);
  sql := FormatUTF8 ('ALTER TABLE % ADD CONSTRAINT % ' +
                     'FOREIGN KEY (%) REFERENCES %(ID) ON DELETE %',
                     [DetailTable.SQLTableName, cname,
                      ForeignKey, MasterTable.SQLTableName, ReferentialActionStrings[Action]]);
  ExecuteNoResult (sql, []);
end;

function TSQLDBZEOSConnectionPropertiesEx.ForeignKeyConstraintName (MasterTable, DetailTable: TSqlRecordClass;
         ForeignKey: RawUTF8 = ''): RawUTF8;
begin
  if ForeignKey = '' then ForeignKey := MasterTable.SQLTableName + 'ID';
  result := FormatUTF8 ('FKC_%_%_%', [DetailTable.SQLTableName, ForeignKey, MasterTable.SQLTableName]);
end;

function TSQLDBZEOSConnectionPropertiesEx.ForeignKeyConstraintExists (MasterTable, DetailTable: TSqlRecordClass;
          ForeignKey: RawUTF8 = ''): boolean;
var
  rows: ISQLDBRows;
begin
  if ForeignKey = '' then ForeignKey := MasterTable.SQLTableName + 'ID';

  try
    var sql := FormatUTF8 ('SELECT * FROM pg_constraint WHERE conrelid = %::regclass AND confrelid = %::regclass AND conname = %',
               [QuotedStr (MasterTable.SQLTableName), QuotedStr (DetailTable.SQLTableName),
                QuotedStr (ForeignKeyConstraintName(MasterTable, DetailTable, ForeignKey))]);
    rows := Execute (sql, []);
    result := rows.Step;
  finally

  end;
end;

function TSQLDBZEOSConnectionPropertiesEx.GetCalculatedFieldDefinition (const TableName, FieldName: string): string;
var
  SQL: RawUTF8;
  rows: ISQLDBRows;
begin
  SQL := FormatUTF8('SELECT pg_get_expr(ad.adbin, ad.adrelid) AS definition ' +
                'FROM pg_attrdef ad ' +
                'JOIN pg_attribute a ON a.attrelid = ad.adrelid ' +
                'WHERE a.attrelid = ''%''::regclass ' +
                'AND a.attname = ''%'';', [TableName.ToLower, FieldName.ToLower]);
    rows := Execute (sql, []);
    if rows.Step
       then result := rows.ColumnString(0)
       else result := '';
end;

procedure TSQLDBZEOSConnectionPropertiesEx.CreateCalculatedFields(Model: TSqlModelEx);
var
  cfa: CalculatedFieldAttribute;
  ctx : TRttiContext;
  sql: RawUTF8;
  sqlExisting: string;
  ColType: string;
begin
  ctx := TRttiContext.Create;

  try
    for var table in model.Tables do
      if not table.IsView then
        begin
          var TableType := ctx.GetType(table);

          for var prop in TableType.GetProperties do
              begin
                cfa := prop.GetAttribute<CalculatedFieldAttribute>;
                if cfa.IsAssigned
                   then
                     begin
                       sqlExisting := GetCalculatedFieldDefinition(Table.SQLTableName.AsString, prop.Name);

                       if not StringCompareIgnoringCRLF (sqlExisting, cfa.sql) then
                         begin
                           case prop.PropertyType.TypeKind of
                              System.TTypeKind.tkUString,
                              System.TTypeKind.tkLString,
                              System.TTypeKind.tkString : ColType := 'TEXT';
                              System.TTypeKind.tkInteger: ColType := 'INTEGER';
                              else raise Exception.Create('Unsupported calculated field type');
                           end;

                           sql := FormatUTF8 ('ALTER TABLE % DROP COLUMN IF EXISTS %;'#13 +
                                              'ALTER TABLE % ADD COLUMN % % GENERATED ALWAYS AS ' +
                                              '(%) STORED;',
                                              [table.SQLTableName, prop.Name,
                                               table.SQLTableName, prop.Name, ColType,
                                               cfa.sql]);
                           ExecuteNoResult(sql, []);

                           sqlExisting := GetCalculatedFieldDefinition(Table.SQLTableName.AsString, prop.Name);
                           if not StringCompareIgnoringCRLF (sqlExisting, cfa.sql) then
                             begin
                               TSQLLog.Add.Log (sllCommonWarning, 'PG altered the definition of calculated field, please update attribute? ', []);
                               TSQLLog.Add.Log (sllCommonWarning, 'Intended:', []);
                               TSQLLog.Add.Log (sllCommonWarning, StringToUTF8 (
                                 StringReplace(StringReplace(cfa.sql, #13, '', [rfReplaceAll]), #10, '', [rfReplaceAll])
                               ), []);
                               TSQLLog.Add.Log (sllCommonWarning, 'Actual:', []);
                               TSQLLog.Add.Log (sllCommonWarning, StringToUTF8 (
                                 StringReplace(StringReplace(sqlExisting, #13, '', [rfReplaceAll]), #10, '', [rfReplaceAll])
                               ), []);
                             end;
                         end;
                     end;
              end;
          end;
  finally
    ctx.Free;
  end;
end;

procedure TSQLDBZEOSConnectionPropertiesEx.CreateCascadeDeleteAllowedTrigger(MasterTable, DetailTable: TSqlRecordClass;
          ForeignKey: string = '');
var
  TriggerName, Sql: RawUTF8;
begin
  if ForeignKey = '' then
    ForeignKey := MasterTable.SQLTableName.AsString + 'ID';

  // Define trigger name based on table names
  TriggerName := FormatUTF8('%_%_BD', [MasterTable.SQLTableName, DetailTable.SQLTableName]);

  // Create SQL for the BEFORE DELETE trigger
  Sql := FormatUTF8(
    'DROP TRIGGER IF EXISTS % ON %; ' +
    'CREATE OR REPLACE FUNCTION %() RETURNS TRIGGER AS $$ ' +
    'BEGIN ' +
    'DELETE FROM % WHERE % = OLD.ID; ' +  // Deletes details based on the foreign key
    'RETURN OLD; ' +
    'END; $$ LANGUAGE plpgsql; ' +
    'CREATE TRIGGER % BEFORE DELETE ON % ' +
    'FOR EACH ROW EXECUTE FUNCTION %();',
    [TriggerName, MasterTable.SQLTableName,
     TriggerName, DetailTable.SQLTableName, StringToUTF8(ForeignKey), TriggerName, MasterTable.SQLTableName, TriggerName]);

  ExecuteNoResult (sql, []);
end;

procedure TSQLDBZEOSConnectionPropertiesEx.CreateCascadeDeleteRestrictedTrigger(MasterTable, DetailTable: TSqlRecordClass;
          ForeignKey: string = '');
var
  TriggerName, Sql: RawUTF8;
begin
  if ForeignKey = '' then
    ForeignKey := MasterTable.SQLTableName.AsString + 'ID';

  // Define trigger name based on table names
  TriggerName := FormatUTF8('%_%_BD', [MasterTable.SQLTableName, DetailTable.SQLTableName]);

  // Create SQL for the BEFORE DELETE trigger to restrict deletion if details exist
  Sql := FormatUTF8(
    'DROP TRIGGER IF EXISTS % ON %; ' +
    'CREATE OR REPLACE FUNCTION %() RETURNS TRIGGER AS $$ ' +
    'BEGIN ' +
    'IF EXISTS (SELECT 1 FROM % WHERE % = OLD.ID) THEN ' +
    'RAISE EXCEPTION ''Cannot delete % with ID %, as related records exist in %'' ' +
    'USING ERRCODE = ''foreign_key_violation'', ' +
    'HINT = ''Delete related records in % first.''; ' +
    'END IF; ' +
    'RETURN OLD; ' +
    'END; $$ LANGUAGE plpgsql; ' +
    'CREATE TRIGGER % BEFORE DELETE ON % ' +
    'FOR EACH ROW EXECUTE FUNCTION %();',
    [TriggerName, MasterTable.SQLTableName, TriggerName, DetailTable.SQLTableName, ForeignKey, MasterTable.SQLTableName, 'OLD.ID', DetailTable.SQLTableName,
     DetailTable.SQLTableName, TriggerName, MasterTable.SQLTableName, TriggerName]);

  ExecuteNoResult (sql, []);
end;


procedure TSQLDBZEOSConnectionPropertiesEx.DeleteBeforeDeleteTrigger(MasterTable, DetailTable: TSqlRecordClass);
var
  TriggerName, Sql: RawUTF8;
begin
  // Define trigger name based on the MasterTable name
  TriggerName := FormatUTF8('%_%_BD', [MasterTable.SQLTableName, DetailTable.SQLTableName]);

  // Create SQL to drop the trigger
  Sql := FormatUTF8(
    'DROP TRIGGER IF EXISTS % ON %;',
    [TriggerName, MasterTable.SQLTableName]);

  ExecuteNoResult (sql, []);
end;

procedure TSQLDBZEOSConnectionPropertiesEx.CreateIndexes(Model: TSqlModelEx);
var
  sql: RawUTF8;
  ctx : TRttiContext;
begin
  ctx := TRttiContext.Create;

  try
    for var table in model.Tables do
      if not table.IsView then
        begin
          var IsLarge := ClassHasAttribute (table, LargeTableAttribute);

          var TableType := ctx.GetType(table);

          for var prop in TableType.GetProperties do
            begin
              if prop.HasAttribute (DBIndexedAttribute) // stored AS_UNIQUE is handled by mormot
                 or
                 (IsLarge and prop.HasAttribute (GeneralFilterAttribute))
                 then
                   begin
                     if prop.HasAttribute (FullTextIndexAttribute)
                        then sql := SQLAddFullTextIndex (table.SQLTableName, StringToUTF8(prop.name), 'english', false)
                        else sql := SQLAddIndex (table.SQLTableName, [StringToUTF8(prop.name)], false);
                     ExecuteNoResult(sql, []);
                   end;

//              var fkca := prop.GetAttribute<FKRuleAttribute>;
//              if fkca <> nil then
//                CreateForeignKeyConstraint (fkca.SqlRecordClass, table, fkca.OnDelete, prop.Name);
// had to disable... because IDs are never nulls with mormot... there is TNullableInteger idk if its possible to try that...

                var fkra := prop.GetAttribute<FKRuleAttribute>;
                if fkra.IsAssigned then
                  begin
                    if fkra.IsAssigned and not fkra.CustomName and (prop.Name <> 'ParentID')
                       //and ( (copy (prop.Name, 1, length (prop.Name) - 2) <> fkra.SqlRecordClass.SQLTableName.AsString) )
                       and (prop.Name <> fkra.SqlRecordClass.SQLTableName.AsString + 'ID')
                       then
                         if prop.Name.EndsWith(fkra.SqlRecordClass.SQLTableName.AsString + 'ID')
                           then TSQLLog.Add.Log(sllCommonInfo,
                                  FormatUTF8 ('Foreign key is probably ok? % => %', [prop.Name, fkra.SqlRecordClass.SQLTableName]))
                           else TSQLLog.Add.Log(sllCommonWarning,
                                  FormatUTF8 ('Suspicious foreign key definition? % => %', [prop.Name, fkra.SqlRecordClass.SQLTableName]));

                    case fkra.OnDelete of
                      TReferentialAction.Cascade:
                        CreateCascadeDeleteAllowedTrigger(fkra.SqlRecordClass, table, prop.Name);
                      TReferentialAction.Restrict:
                        CreateCascadeDeleteRestrictedTrigger(fkra.SqlRecordClass, table, prop.Name)
                      else
                        DeleteBeforeDeleteTrigger(fkra.SqlRecordClass, table);
                    end;//case
                  end;
            end;
        end;
  finally
    ctx.Free;
  end;
end;

//procedure TSQLDBZEOSConnectionPropertiesEx.CreateViews(Model: TSqlModel);
//var
//  TableType: TRttiType;
//begin
//  var ctx := TRttiContext.Create;
//  try
//    for var table in model.Tables do
//      begin
//        TableType := ctx.GetType(table);
//        for var Attribute in TableType.GetAttributes do
//          if Attribute is ViewAttribute then
//            begin
//              try
//                ExecuteNoResult(TSqlRecordEx(table).ViewSQL, []);
//              except on e: exception do
//                raise Exception.Create ('Failed to create view for ' + table.SQLTableName.AsString + #13#13 +
//                                        TSqlRecordEx(table).ViewSQL  + #13#13 + e.Message);
//              end;
//              break;
//            end;
//      end;
//  finally
//    ctx.Free;
//  end;
//end;

procedure TSQLDBZEOSConnectionPropertiesEx.ExecuteResourceStringSQL(const Identifiers: array of string);
begin
  for var id in Identifiers do
    ExecuteResourceStringSQL(id);
end;

function TSQLDBZEOSConnectionPropertiesEx.FieldExists(const TableName, FieldName: RawUTF8): boolean;
var
  sql: RawUTF8;
  rows: ISQLDBRows;
begin
  sql := FormatUTF8 ('SELECT column_name FROM information_schema.columns WHERE table_name = ''%'' AND column_name = ''%''', [TableName, FieldName]);
  rows := Execute (sql, []);
  result := rows.Step();
  rows.ReleaseRows;
end;

procedure TSQLDBZEOSConnectionPropertiesEx.ExecuteResourceStringSQL(const Identifier: string);
begin
  if FindResource(HInstance, PChar(Identifier), RT_RCDATA) = 0
     then raise Exception.Create('Resource not found: ' + Identifier + #13#13'Perhaps a file of similar name needs to be added to resources with this identifier?');
  ExecuteNoResult(StringToUTF8 (GetResourceString(Identifier)), []);
end;

function TSQLDBZEOSConnectionPropertiesEx.IndexExists(const aTableName: RawUTF8;
  const aFieldNames: array of RawUTF8;
  const aIndexName: RawUTF8): boolean;
var
  IndexName,FieldsCSV, Owner, Table: RawUTF8;
  rows: ISQLDBRows;
begin
  if (self=nil) or (aTableName='') or (high(aFieldNames)<0) then
    exit (false);

  if aIndexName=''
    then
      begin
        SQLSplitTableName(aTableName,Owner,Table);
        if (Owner<>'') and
           not (fDBMS in [dMSSQL,dPostgreSQL,dMySQL,dFirebird,dDB2,dInformix]) then
          // some DB engines do not expect any schema in the index name
          IndexName := Owner+'.';
        FieldsCSV := RawUTF8ArrayToCSV(aFieldNames,'');
        if length(FieldsCSV)+length(Table)>27 then
          // sounds like if some DB limit the identifier length to 32 chars
          IndexName := IndexName+'INDEX'+
            crc32cUTF8ToHex(Table)+crc32cUTF8ToHex(FieldsCSV) else
          IndexName := IndexName+'NDX'+Table+FieldsCSV;
        end
      else
          IndexName := aIndexName;
  case fDBMS of
    dFirebird:  begin
                   rows := Execute (FormatUTF8 ('SELECT RDB$INDEX_ID FROM RDB$INDICES WHERE RDB$INDEX_NAME = %',
                           [uppercase(QuotedStr (IndexName))]), []);
                   result := rows.Step();
                   rows.ReleaseRows;
                end;
    dSQLite:    begin
                   rows := Execute(FormatUTF8('SELECT name FROM sqlite_master WHERE type = ''index'' AND name = %',
                             [QuotedStr(IndexName)]), []);
                   result := rows.Step();
                   rows.ReleaseRows;
                end;
    dPostgreSQL:begin
                   rows := Execute('SELECT 1 FROM pg_indexes WHERE indexname = ' + QuotedStr(IndexName), []);
                   result := rows.Step();
                   rows.ReleaseRows;
                end
    else       result := false;
    end;
end;

function TSQLDBZEOSConnectionPropertiesEx.SQLAddFullTextIndex(const ATableName, AFieldNames, ALanguage: RawUTF8; AUnique, ADescending: boolean; AIndexName: RawUTF8): RawUTF8;
begin
  //CREATE INDEX idx_fulltext_search ON my_table USING gin(to_tsvector('english', text_column));

  if AIndexName = '' then AIndexName := 'FTNDX' + ATableName + AFieldNames;
  //if too big name then AIndexName := crc32cUTF8ToHex(Table)+crc32cUTF8ToHex(FieldsCSV) else
  result := FormatUTF8 ('CREATE INDEX IF NOT EXISTS % ON % USING gin(to_tsvector(%, %))', [AIndexName, ATableName, QuotedStr (ALanguage), AFieldNames]);
end;

function TSQLDBZEOSConnectionPropertiesEx.SQLAddIndex (const ATableName: RawUTF8; const AFieldNames: array of RawUTF8;
                                                       AUnique: boolean; ADescending: boolean = false;
                                                       const AIndexName: RawUTF8 = ''): RawUTF8;
begin
  case fDBMS of
    dFirebird, dSQLite, dPostgreSQL :
      if aIndexName.IsEmpty or not IndexExists (aTableName, aFieldNames, AIndexName)
                then result := inherited
                else result := '';
     else       result := inherited
  end;
end;

function TSQLDBZEOSConnectionPropertiesEx.TableExists(const Name: RawUTF8): boolean;
var
  rows: ISQLDBRows;
begin
  case fDBMS of
    dSQLite: begin
                 rows := Execute (FormatUTF8 ('SELECT name FROM sqlite_master WHERE type=''table'' AND upper (name)=''%''',
                         [uppercase(Name)]), []);
                 result := rows.Step();
                 rows.ReleaseRows;
               end;
    dPostgreSQL: begin
                   rows := Execute (FormatUTF8 (
                                 'SELECT table_name FROM information_schema.tables'#13+
                                 'WHERE table_schema = ''public'''#13+
                                 'AND table_type = ''BASE TABLE'''#13+
                                 'AND upper (table_name) = ''%''',
                                 [uppercase(Name)]), []);
                   result := rows.Step();
                   rows.ReleaseRows;
                 end
    else       result := false;
    end;
end;

function TSQLDBZEOSConnectionPropertiesEx.ViewExists(const Name: RawUTF8): boolean;
var
  rows: ISQLDBRows;
begin
  case fDBMS of
    dFirebird: begin
                 rows := Execute (FormatUTF8 ('SELECT first 1 * FROM rdb$view_relations WHERE RDB$VIEW_NAME = %',
                         [uppercase(QuotedStr (Name))]), []);
                 result := rows.Step();
//                 rows.ReleaseRows;
               end;
    dSQLite:   begin
                 rows := Execute (FormatUTF8 ('SELECT name FROM sqlite_master WHERE type = ''view'' and name = %',
                         [QuotedStr (Name)]), []);
                 result := rows.Step();
               end;
    dPostgreSQL: begin
                   rows := Execute (FormatUTF8 (
                                 'SELECT table_name FROM information_schema.tables'#13+
                                 'WHERE table_schema = ''public'''#13+
                                 'AND table_type = ''VIEW'''#13+
                                 'AND upper (table_name) = ''%''',
                                 [uppercase(Name)]), []);
                   result := rows.Step();
                   rows.ReleaseRows;
                 end
    else       result := false;
    end;
end;

{ TSQLRestHelper }

class constructor TSQLRestHelper.create;
begin
  inherited;
  a := TObject.Create;
end;

function TSQLRestHelper.Delete(rec: TSQLRecord): boolean;
begin
  result := Delete (rec.RecordClass, rec.ID);
end;

function SortDynArraySQLRecordID(const A,B): integer;
begin
  result := TSQLRecord(A).IDValue - TSQLRecord(B).IDValue;
end;

function TSQLRestHelper.TableHasRowsWhere(Table: TSQLRecordClass; Where: RawUTF8): boolean;
var
  rec: TSQLRecord;
begin
  rec := Table.Create;
  Retrieve (Where, rec);
  result := rec.ID <> 0;
  rec.Free;
end;

{ TFunRecordLocker }

constructor TFunRecordLocker.Create;
begin
  inherited Create;
  Records := TList<TLockedRecord>.Create;
  SynLock.Init;
end;

destructor TFunRecordLocker.destroy;
begin
  Records.Free;
  SynLock.Done;
  inherited;
end;

function TFunRecordLocker.FFind(cl: TSQLRecordClass; ID: TID): integer;
var
  i : integer;
begin
  for i := 0 to Records.Count-1 do
    if (Records[i].ID = ID) and (Records[i].SqlRecordClass = cl) then exit (i);
  result := -1;
end;

function TFunRecordLocker.IsLocked(cl: TSQLRecordClass; ID: TID): boolean;
begin
  SynLock.Lock;
  try
    result := FFind(cl, ID) <> -1;
  finally
    SynLock.UnLock;
  end;
end;

function TFunRecordLocker.IsLocked(rec: TSQLRecord): boolean;
begin
  result := IsLocked(rec.RecordClass, rec.IDValue);
end;

function TFunRecordLocker.Lock(rec: TSQLRecord; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean;
begin
  result := Lock (rec.RecordClass, rec.IDValue, UnknownSession);
end;

function TFunRecordLocker.DoLock(cl: TSQLRecordClass; ID: TID; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean;
begin
  SynLock.Lock;
  try
    var index := FFind (cl, ID);
    AlreadyLocked := index > -1;
    if AlreadyLocked
      then
        if SecondsSince(Records[index].TimeStamp) > SqlRecordLockTimeout
          then Records.Delete(index)
          else if Records[index].Session = ServiceContext.Request.Session
            then exit (true) // allow re-lock by same session
            else exit (false);

    Records.Add(TLockedRecord.Create (cl, ID, UnknownSession));
    result := true;
  finally
    SynLock.UnLock;
  end;
end;

function TFunRecordLocker.Lock(cl: TSQLRecordClass; ID: TID; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean;
begin
  for var i := 1 to 3 do
    begin
      result := DoLock (cl, ID, AlreadyLocked, UnknownSession);
      if result then exit;
      sleep (100);
    end;
end;

function TFunRecordLocker.RenewLocks: integer;
var
  i : integer;
begin
  result := 0;
  SynLock.Lock;
  try
    for i := 0 to Records.Count-1 do
     if Records[i].Session = ServiceContext.Request.Session
       then begin
              Records[i].Renew;
              inc (result);
            end;
  finally
    SynLock.UnLock;
  end;
end;

function TFunRecordLocker.Unlock(rec: TSQLRecord): boolean;
begin
  result := Unlock (rec.RecordClass, rec.ID);
end;

function TFunRecordLocker.Unlock(cl: TSQLRecordClass; ID: TID): boolean;
begin
  SynLock.Lock;
  try
    var index := FFind (cl, ID);
    if index = -1 then
      exit (false);

    result := Records[index].Session = ServiceContext.Request.Session;
    if result
      then Records.Delete(index);
  finally
    SynLock.UnLock;
  end;
end;

{ TFunRecordLocker.TLockedRecord }

constructor TFunRecordLocker.TLockedRecord.Create(ASqlRecordClass: TSQLRecordClass; AID: TID; UnknownSession: boolean = false);
begin
  TimeStamp := now;
  if UnknownSession
     then Session := NULL_ID
     else Session := ServiceContext.Request.Session;
  SqlRecordClass := ASqlRecordClass;
  ID := AID;
end;

procedure TFunRecordLocker.TLockedRecord.Renew;
begin
  TimeStamp := now;
end;

{ TIDGenerator }

constructor TIDGenerator.Create(Server: TSQLRestServerDB; SqlRecordClass: TSQLRecordClass);
begin
  inherited Create;
  FCurrentID := Server.TableMaxID (SqlRecordClass);
end;

function TIDGenerator.NextID: TID;
begin
  result := AtomicIncrement(FCurrentID);
end;

procedure TSQLRestServerDBEx.InitializeTables(Options: TSQLInitializeTableOptions);
var t: PtrInt;
begin
  //inherited;

  if (self<>nil) and (Model<>nil) then
    for t := 0 to Model.TablesMax do
      if not Model.Tables[t].IsView and not TableHasRows(Model.Tables[t]) then
        Model.Tables[t].InitializeTable(self,'',Options);
end;

function TSQLRestServerDBEx.InternalAdd(Value: TSQLRecord; SendData: boolean; CustomFields: PSQLFieldBits;
         ForceID, DoNotAutoComputeFields: boolean): TID;
var
  Generator: TIDGenerator;
begin
  if not ForceID then
    if IDGenerators.TryGetValue (Value.RecordClass, Generator) then
      begin
        ForceID := true;
        Value.IDValue := Generator.NextID;
      end;
  result := inherited InternalAdd(Value, SendData, CustomFields, ForceID, DoNotAutoComputeFields);
end;

function TSQLRestServerDBEx.Add(Value: TSQLRecord; Batch: TSQLRestBatch): TOutcome;
var
  Generator: TIDGenerator;
begin
//  var err := Value.Validate;
//  if err <>'' then exit (ServerErrorOutcome(seValidationFailed, err));
  CheckValidation (Value, result);
  if not Result.Success then exit;

  var ForceID := false;
  if IDGenerators.TryGetValue (Value.RecordClass, Generator) then
    begin
      ForceID := true;
      Value.IDValue := Generator.NextID;
    end;
  if Batch.Add (Value, true, ForceID) = -1
     then result := ServerErrorOutcome(seBatchFailed)
     else result.SetSuccess;
end;

function TSQLRestServerDBEx.AddWithFields(Value: TSQLRecord; Fields: RawUTF8): TOutcome;
begin
  try
    if Fields = ''
       then Fields := '*';
    result.SetID (inherited Add (Value, Fields));
    if result.ID <> NULL_ID
       then Value.IDValue := result.ID
       else begin
              result := ServerErrorOutcome(seAddFailed);
              result := CheckCUFailure(Value, result);
            end;
  except on e: exception do
    begin
      if pos ('duplicate key', e.Message) > 0
        then result := ServerErrorOutcome(seDuplicateKey)
        else result := ServerDBErrorOutcome(seAddFailed, Value.SQLTableName.AsString, '', e.Message);
    end
    else result := ServerDBErrorOutcome(seAddFailed, Value.SQLTableName.AsString, '', 'WTF');
  end;

  if Settings.ExecLog then DoExecLog (Value, TCrud.Create, result);
end;

procedure TSQLRestServerDBEx.DoExecLog (const TableName, Content: RawUTF8; const Operation: TCrud; const Outcome: TOutcome);
begin
  TMonitor.Enter(self);

  try

    if FExecLogStream = nil then
      begin
        var AFileName := ApplicationExeName + '.execlog.txt';
        if FileExists(AFileName) then
           FExecLogStream := TFileStream.Create(AFileName, fmOpenWrite or fmShareDenyNone)
         else
           FExecLogStream := TFileStream.Create(AFileName, fmCreate);

        FExecLogWriter := TStreamWriter.Create(FExecLogStream, TEncoding.UTF8);
      end;

    FExecLogWriter.Write(TableName.AsString + '.' + System.TypInfo.GetEnumName(TypeInfo(TCrud), ord(Operation)) + ': ' + Content.AsString + #13);

  finally
    TMonitor.Exit(self);
  end;

end;

procedure TSQLRestServerDBEx.DoExecLog (Value: TSqlRecord; const Operation: TCrud; const Outcome: TOutcome);
begin
  DoExecLog (Value.SQLTableName, Value.GetJSONValues (true, Value.IDValue.IsNotNULL, Value.RecordProps.CopiableFieldsBits{GetNonVoidFields}, []), Operation, Outcome);
end;

procedure TSQLRestServerDBEx.DoExecLog(Table: TSQLRecordClass; ID: TID; const Operation: TCrud; const Outcome: TOutcome);
begin
  DoExecLog (Table.SQLTableName, Int64ToUtf8(ID), Operation, Outcome);
end;

procedure TSQLRestServerDBEx.EnforceMirrorFieldValues (ConPropEx: TSQLDBZEOSConnectionPropertiesEx);
//var
//  ModelEx: TSQLModelEx;
//  rows: ISQLDBRows;
//  rmf: TRestrictedMirrorField;
//  original, masterTableName, sql, sqlExec: RawUTF8;
//  pair: TPair<TSqlRecordClass, TRestrictedMirrorFieldList>;
begin
(*
  if not (Model is TSQLModelEx) then exit;
  ModelEx := TSQLModelEx (Model);
  for pair in ModelEx.RestrictedMirrorFieldsOfMaster do
    begin
      for rmf in pair.Value do
        begin
          masterTableName := pair.Key.SQLTableName;
          original := masterTableName + '.' + rmf.OriginalFieldName;
//          sql := FormatUTF8 ('SELECT %.ID as DetailID, %.% as Original FROM %, % '+
//                                       'WHERE % = %.ID and (Original <> % '+
//                                       'OR (Original IS NULL and % IS NOT NULL) ' +
//                                       'OR (Original IS NOT NULL and % IS NULL)) ',
          sql := FormatUTF8 ('SELECT %.ID as DetailID, % FROM %, % '#13+
                                       'WHERE % = %.ID and ( (% <> %) '#13+
                                       'OR ( (% IS NULL) and (% IS NOT NULL) )#13 ' +
                                       'OR ( (% IS NOT NULL) and (% IS NULL) ) ) ',

                     [rmf.TableClass.SQLTableName, original, masterTableName, rmf.TableClass.SQLTableName,
                      rmf.FullForeignKeyName, masterTableName, original, rmf.FullMirrorFieldName,
                      original, rmf.FullMirrorFieldName,
                      original, rmf.FullMirrorFieldName]);

          rows := ConPropEx.Execute (sql, []);
          while rows.Step() do
            begin
              TSQLLog.Add.Log(sllInfo,'Enforcing mirror field ' + rmf.FullMirrorFieldName + ' DetailID = ' + VariantToUTF8(rows.Column['DetailID']));
              sqlExec := FormatUTF8 ('UPDATE % SET %=% where ID=%',
                         [rmf.TableClass.SQLTableName, rmf.MirrorFieldName,
                          rows.Column['Original'], rows.Column['DetailID']]);
              ConPropEx.ExecuteNoResult (sqlExec, []);
            end;
        end;
     end;
     *)
end;

function TSQLRestServerDBEx.FindID(RecordClass: TSQLRecordClass; Condition: RawUTF8): TID;
begin
  result := OneFieldValueInt64 (RecordClass, 'RowID', Condition, NULL_ID);
end;

function TSQLRestServerDBEx.Add(Value: TSQLRecord; SendData: boolean = true; ForceID: boolean=false; DoNotAutoComputeFields: boolean=false): TOutcome;
begin
  CheckValidation (Value, result);
  if not Result.Success then exit;

  Value.BeforeWrite;

  try
    result.SetID (inherited Add (Value, SendData, ForceID, DoNotAutoComputeFields));
    if result.ID <> NULL_ID
       then Value.IDValue := result.ID
       else begin
              result := ServerErrorOutcome(seAddFailed);
              result := CheckCUFailure(Value, result);
            end;
  except on e: exception do
    begin
      if pos ('duplicate key', e.Message) > 0
        then result := ServerErrorOutcome(seDuplicateKey)
        else result := ServerDBErrorOutcome(seAddFailed, Value.SQLTableName.AsString, '', e.Message);
    end
    else result := ServerDBErrorOutcome(seAddFailed, Value.SQLTableName.AsString, '', 'WTF');
  end;

  if Settings.ExecLog then DoExecLog (Value, TCrud.Create, result);
end;

//procedure TSQLRestServerDBEx.AddFunction(Name: RawUTF8);
//var
//  fcode: RawUTF8;
//begin
//  DB.Execute('drop function ' + Name, []);
//  fcode.AsString := TFile.ReadAllText(TPath.Combine(RootPath, Name.AsString + '.sql'));
//  funServerMain.DB.Execute(fcode, []);
//end;

function TSQLRestServerDBEx.Kick(AUserID: TID): boolean;
var
  i: integer;
  sess: TAuthSession;
begin
  result := false;
  fSessions.Safe.Lock;
  try
    for i := fSessions.Count-1 downto 0 do
      begin
        sess := TAuthSession(fSessions.List[i]);
        if (sess.User <> nil) and (sess.User.IDValue = AUserID) then
          SessionDelete(i, ServiceContext.Request);
        result := true;
      end;
  finally
    fSessions.Safe.UnLock;
  end;
end;

function TSQLRestServerDBEx.AddIDGenerator(Table: TSQLRecordClass): TIDGenerator;
begin
  result := TIDGenerator.Create(self, Table);
  IDGenerators.Add(Table, result);
end;

function TSQLRestServerDBEx.AddMany (Field: TSQLRecordMany; SourceID: TID; IDs: TIDDynArray; Batch: TSQLRestBatch = nil): TOutcome;
begin
  for var DestID in IDs do
    if not Field.ManyAdd(self, SourceID, DestID, false, Batch)
      then exit (ServerErrorOutcome(seDetailTableOperationFailed));
end;

function TSQLRestServerDBEx.AddOrCrash(Value: TSQLRecord; SendData, ForceID, DoNotAutoComputeFields: boolean): TOutcome;
begin
  result := Add (Value, SendData, ForceID, DoNotAutoComputeFields);
  if not result.Success then
    raise Exception.Create(result.Description);
end;

function TSQLRestServerDBEx.ReplaceMany (Field: TSQLRecordMany; SourceID: TID; IDs: TIDDynArray): TOutcome;
var
  DestIDs: TIDDynArray;
begin
  Field.DestGet(self, SourceID, DestIDs);
  for var DestID in DestIDs do
    Field.ManyDelete(self, SourceID, DestID);
  AddMany (Field, SourceID, IDs);
end;

function TSQLRestServerDBEx.BatchSend(Batch: TSQLRestBatch; var Outcome: TOutcome): integer;
begin
  result := inherited BatchSend (Batch);
  if result <> 200
     then Outcome := ServerErrorOutcome(seBatchFailed, result.ToString)
     else Outcome.SetSuccess;
end;

constructor TSQLRestServerDBEx.Create(AModel: TSQLModelEx; const aDBFileName: TFileName; ADBConnectionProperties: TSQLDBZEOSConnectionPropertiesEx; aHandleUserAuthentication: boolean=false; AOwnDB: boolean=false);
begin
  if TSqlDatabaseInfo.ExpectedVersion = 0 then raise Exception.Create('Please set TSqlDatabaseInfo.ExpectedVersion!');

  inherited Create (AModel, aDBFileName, AHandleUserAuthentication);
  slSqlDeleteObsoleteFieldsTables := TStringList.Create;
  slObsoleteFieldAttributes := TStringList.Create;
  FModel := AModel;
  FDB := ADBConnectionProperties;
  UseRecordLocker := true;
  DBConnectionProperties := ADBConnectionProperties;
  RecordLocker := TFunRecordLocker.Create;
  IDGenerators := TDictionary <TSQLRecordClass, TIDGenerator>.Create;
  DBChanges := TList<TDBChange>.Create;
  dicTriggers := TDictionary <string, string>.Create;
end;

destructor TSQLRestServerDBEx.Destroy;
begin
  slSqlDeleteObsoleteFieldsTables.Free;
  slObsoleteFieldAttributes.Free;
  RecordLocker.Free;
  for var Pair in IDGenerators do
      Pair.Value.Free;
  IDGenerators.Free;
  DBChanges.Free;
  dicTriggers.Free;

  FExecLogWriter.Free;
  FExecLogStream.Free;

  inherited;
end;

function TSQLRestServerDBEx.CreateBatch: TSQLRestBatch;
begin
  result := TSQLRestBatch.Create(self, nil, maxInt, [boRollbackOnError]);
end;

procedure TSQLRestServerDBEx.CreateEnumFunctions;
var
  ctx : TRttiContext;
  TableType: TRttiType;
  Table: TSqlRecordClass;
  rows: ISQLDBRows;
  sql: RawUTF8;
  dicExisting: TDictionary<string, string>;
  slSkip: TStringList;

  procedure CreateFor(EnumType: TRttiEnumerationType);
  var
    i: Integer;
    fnName, ExistingCode, EnumValueName: string;
    EnumValue: Integer;
    fnCode, SqlFunction: RawUTF8;
  begin
    if EnumType.Name = 'Boolean' then exit;

    for i := EnumType.MinValue to EnumType.MaxValue do
    begin
      EnumValueName := EnumType.GetNames[i];
      EnumValue := i;
      fnName := FormatUTF8('Enum_%_%', [EnumType.Name, EnumValueName]).AsString.ToLower;
      if slSkip.Contains (fnName) then continue;
      slSkip.Add (fnName);
      ExistingCode := '';
      dicExisting.TryGetValue(fnName, ExistingCode);
      fnCode := FormatUTF8(
              'RETURNS INTEGER AS $$ BEGIN RETURN %; END; $$ LANGUAGE plpgsql IMMUTABLE;', [EnumValue]);
      SqlFunction := FormatUTF8('CREATE OR REPLACE FUNCTION public.%() %', [fnName, fnCode]);

      if //false //ExistingCode = SqlFunction
         ExistingCode.ToLower.Contains('return ' + EnumValue.ToString + ';')
         { PG returns function body a bit rearranged from what we sent it,
           who knows if all versions would do same, uncertain to compare, but we can look for "return x;" I guesss? }
        then
        else begin
               FDB.ExecuteNoResult(SqlFunction, []);
               TSQLLog.Add.Log (sllCommonWarning, SqlFunction);
             end;
      if ExistingCode <> ''
         then dicExisting.Remove(fnName);
    end;
  end;

begin
  dicExisting := TDictionary<string, string>.Create;
  slSkip := TStringList.Create;

  sql :=  'SELECT ' +
        '  p.proname AS function_name, ' +
        '  pg_catalog.pg_get_functiondef(p.oid) AS function_definition ' +
        'FROM ' +
        '  pg_catalog.pg_proc p ' +
        '  JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace ' +
        'WHERE ' +
        '  p.proname ILIKE ''Enum_%'' ' +
        '  AND pg_catalog.pg_function_is_visible(p.oid) ' +
        '  AND n.nspname <> ''pg_catalog'' ' +
        '  AND n.nspname <> ''information_schema'';';

  rows := Fdb.Execute(sql, []);
  while rows.step do
    dicExisting.Add (rows.ColumnString(0).ToLower, rows.ColumnString(1));

  ctx := TRttiContext.Create;
  try
    for Table in Model.Tables do
      begin
        TableType := ctx.GetType(table);
        for var prop in TableType.GetProperties do
          begin
            if prop.PropertyType is TRttiEnumerationType then
               CreateFor (TRttiEnumerationType(prop.PropertyType));
          end;
      end;
  finally
    ctx.Free;
  end;

  for var pair in dicExisting do
    begin
      sql := FormatUTF8('DROP FUNCTION %', [pair.Key.ToLower]);
      TSQLLog.Add.Log (sllCommonWarning, sql);
      Execute(sql);
    end;

  slSkip.Free;
  dicExisting.Free;
end;

procedure TSQLRestServerDBEx.AddChangeLog (Table: TSQLRecordClass; const ID: TID; const Operation: TCrud; const json: rawutf8);
var
  rec: TSqlChangeLog;
begin
  rec := TSqlChangeLog.Create;
  rec.TableName := Table.SQLTableName;
  rec.OldValues := json;
  rec.RecordID := ID;
  rec.Change := Operation;
  rec.ChangeTime := now;
  Add (rec);
  rec.Free;
end;

function TSQLRestServerDBEx.RetrieveAsJson (Table: TSQLRecordClass; const ID: TID; var json: RawUTF8): TOutcome;
begin
  var rec := Table.Create;
  try
    Retrieve (ID, rec, result);
    if result.Success
       then json := rec.GetJSONValues(true, true, rec.RecordProps.CopiableFieldsBits {rec.GetNonVoidFields}, [])
       else begin
              json := '';
              exit (ServerErrorOutcome(seRecordNotFound));
            end;
  finally
    rec.Free;
  end;
end;

function TSQLRestServerDBEx.DeleteEx(Table: TSQLRecordClass; ID: TID): TOutcome;
var
  lst: TCascadeList;
  rule: TCascade;
  json: RawUTF8;
begin
  if Assigned (RestrictedCascades) and RestrictedCascades.TryGetValue(Table, lst) then
    for rule in lst do
        if RestrictIfTableHasForeignKey(rule.TableClass, rule.ForeignKeyName, ID, result)
           then exit;

(*
  if Assigned (AllowedCascades) and AllowedCascades.TryGetValue(Table, lst) then
    for rule in lst do
      begin
        //sql := FormatUTF8 ('delete from % where % = %', [rule.TableClass.SQLTableName, rule.ForeignKeyName, ID]);
        //FDB.ExecuteNoResult(sql, []);
        //result := DeleteEx (rule.TableClass.crudClass
      end;
        //if RestrictIfTableHasForeignKey(rule.TableClass, rule.ForeignKeyName, ID, result)
*)
  if Settings.ChangeLog
    then result := RetrieveAsJson (Table, ID, json)
    else begin
           json := '';
           result.Success := true;
         end;

  if not result.Success then exit;

  result.Success := inherited Delete (Table, ID);

  if Settings.ExecLog then DoExecLog (Table, ID, TCrud.Delete, result);

  if Settings.ChangeLog then AddChangeLog (Table, ID, TCrud.Delete, json);
end;

function TSQLRestServerDBEx.DeleteEx(rec: TSQLRecord): TOutcome;
begin
  result := DeleteEx (rec.RecordClass, rec.IDValue);
end;

procedure TSQLRestServerDBEx.GenerateID(rec: TSqlRecord);
var
  gen: TIDGenerator;
begin
  if IDGenerators.TryGetValue (rec.RecordClass, gen)
    then rec.IDValue := gen.NextID
    else raise Exception.Create('No generator for ' + rec.SQLTableName.AsString);
end;

procedure TSQLRestServerDBEx.GetDBStats(out Tables: TRawUTF8DynArray; out Counts: TIDDynArray; out RequiredAuthLevels: TRequiredAuthLevelsArray);
begin
  SetLength(Tables, Model.lstTablesAndViews.Count);
  SetLength(Counts, Model.lstTablesAndViews.Count);
  SetLength(RequiredAuthLevels, Model.lstTablesAndViews.Count);
  for var i := 0 to Model.lstTablesAndViews.Count - 1 do
    begin
      Tables[i] := Model.lstTablesAndViews[i].SQLTableName;
      Counts[i] := TableRowCount (Model.lstTablesAndViews[i]);
      RequiredAuthLevels[i] := Model.lstTablesAndViews[i].CrudPermissions.ToRequiredAuthLevels;
    end;
end;

procedure TSQLRestServerDBEx.CreateIndexes;
begin

end;

procedure TSQLRestServerDBEx.CreateIndexesFromAttributes (Tables: TSqlRecordClassDynArray);
var
  FieldNames: TRawUTF8DynArray;// Array<RawUTF8>;
  FieldInfo: TFieldInfo;
  sql: RawUTF8;
begin
  if length(Tables) = 0
     then Tables := Model.Tables;

  var ctx := TRttiContext.Create;
  for var Table in Tables do
    if not Table.IsView then
      begin
        var TableType := ctx.GetType(table);

        for var att in TableType.GetAttributes do
          if att is DBMultiFieldIndexAttribute then
            begin
              var mfia := DBMultiFieldIndexAttribute(att);
              if not CreateSQLMultiIndex (Table, mfia.UTF8Fields, mfia.Unique, mfia.NameFor(Table.SQLTableName))
                 then raise Exception.Create('Cant create index ' + mfia.NameFor(Table.SQLTableName).AsString);
            end;

        FieldNames := [];
        for FieldInfo in Table.FieldInfoList do
          begin
            if FieldInfo.Indexed then
              CreateSQLIndex (Table, [StringToUTF8 (FieldInfo.Name)], false);

            if FieldInfo.IsAlternateUK then
              begin
                SetLength (FieldNames, length(FieldNames) + 1);
                FieldNames[length(FieldNames) - 1] := StringToUTF8 (FieldInfo.Name);
              end;
          end;

        if length(FieldNames) > 0 then
           begin
             sql := FDB.SQLAddIndex(table.SQLTableName, FieldNames, true);
             FDB.ExecuteNoResult(sql, []);
           end;
      end;
end;

function TSQLRestServerDBEx.TableHasForeignKey(Table: TSQLRecordClass; const FKName: RawUtf8; FKID: TID): boolean;
begin
  var WhereClause := FKName + ' = ' + Int64ToUtf8 (FKID);
  var id := OneFieldValue(Table, 'ID', WhereClause);
  result := id <> '';
end;

function TSQLRestServerDBEx.Retrieve(Value: TSQLRecord; var Outcome: TOutcome; Lock: boolean = false): boolean;
begin
  if Lock then exit (ReadLock(Value, Outcome));

  if not inherited Retrieve (Value.IDValue, Value)
     then Outcome := ServerErrorOutcome(seRecordNotFound)
     else Outcome.SetID (Value.IDValue);
  result := Outcome.Success;
end;

function TSQLRestServerDBEx.Retrieve(Value: TSQLRecord): boolean;
begin
  result := inherited Retrieve (Value.IDValue, Value);
end;

function TSQLRestServerDBEx.RetrieveListObjArrayEx (var ObjArray; Table: TSQLRecordClass; Parameters: TQueryParameters;
                                                    const CustomFieldsCSV: RawUTF8=''): boolean;
begin
  if Parameters.OrderBy = ''
    then Parameters.OrderBy := Table.DefaultSortField;

  result := inherited RetrieveListObjArray (ObjArray, Table, Parameters.MormotSql, []);
end;

function TSQLRestServerDBEx.RetrieveListObjArrayDirect (var ObjArray; Table: TSQLRecordClass; const sqlWhere: RawUTF8;
                                     const CustomFieldsCSV: RawUTF8=''): boolean;
var
  TableIndex: integer;
  Rest: TSqlRest;
  SqlTable: TSQLTable;
  json, sql: RawUTF8;
begin
  TableIndex := Model.GetTableIndex (Table);
  Rest := fStaticVirtualTable[TableIndex];
  sql := Rest.SQLComputeForSelect(Table, CustomFieldsCSV, sqlWhere);
  ReplaceRowID (sql);
  json := Rest.EngineList (sql,false);
  SqlTable := TSQLTableJSON.CreateFromTables([Table],sql,json);
  SqlTable.ToObjArray(ObjArray,Table);
  SqlTable.Free;
  result := true;
end;

function TSQLRestServerDBEx.RetrieveListObjArrayDirect (var ObjArray; Table: TSQLRecordClass; Parameters: TQueryParameters;
                                     const CustomFieldsCSV: RawUTF8=''): boolean;
begin
  if Parameters.OrderBy = ''
    then Parameters.OrderBy := Table.DefaultSortField;
  result := RetrieveListObjArrayDirect(ObjArray, Table, Parameters.MormotSql, CustomFieldsCSV);
end;

function TSQLRestServerDBEx.RetrieveSqlRecords<T> (const FormatSQLWhere: RawUTF8;
                                                   const BoundsSQLWhere: array of const;
                                                   const Fields: RawUTF8): TSQLRecords<T>;
begin
  result := TSQLRecords<T>.Create;
  inherited RetrieveListObjArray(result.Records, T, FormatSQLWhere, BoundsSQLWhere, Fields);
  result.Init (nil);
end;

function TSQLRestServerDBEx.RetrieveSqlRecords(RecordClass: TSQLRecordClass; const FormatSQLWhere: RawUTF8;
                                               const BoundsSQLWhere: array of const; const Fields: RawUTF8)
                                               : TSQLRecords;
begin
  result := TSQLRecords.Create(RecordClass);
  inherited RetrieveListObjArray(result.Records, RecordClass, FormatSQLWhere, BoundsSQLWhere, Fields);
  result.Init (nil);
end;

function TSQLRestServerDBEx.RetrieveSqlRecords<T>: TSQLRecords;
begin
  result := RetrieveSqlRecords<T>('', []);
end;

function TSQLRestServerDBEx.DirectFindID(SqlRecordClass: TSqlRecordClass; const sqlWhere: string): TOutcome;
begin
  result := DirectFindID(SqlRecordClass, StringToUTF8 (sqlWhere));
end;

function TSQLRestServerDBEx.DirectFindID(SqlRecordClass: TSqlRecordClass; const sqlWhere: RawUTF8): TOutcome;
var
  TableIndex: integer;
  Rest: TSqlRest;
  sql, json: RawUTF8;
  temp: RawUTF8;
begin
  try
    TableIndex := Model.GetTableIndex (SqlRecordClass);
    Rest := fStaticVirtualTable[TableIndex];
    //ReplaceRowID (sql);
    sql := FormatUTF8 ('SELECT ID FROM % WHERE % LIMIT 1', [sqlRecordClass.SQLTableName, sqlWhere]);
    json := Rest.EngineList(sql);
    temp := json.SubString('[{"id":', '}');
    result.Success := not temp.IsEmpty;
    if not temp.IsEmpty
      then result.ID := temp.AsString.ToInteger
      else result.ID := NULL_ID;

//    result.ID := json.AsString.ToInteger; // '[{"id":369}]'#$A
  except on e: exception do
    begin
      result := ServerErrorOutcome(seException, e.Message);
      TSQLLog.Add.Log (sllInfo, sqlWhere);
    end;

  end;
end;

(*
function TSQLRestServerDBEx.DirectRetrieve(const SQLWhere: RawUTF8; Value: TSQLRecord;
      const ACustomFieldsCSV: RawUTF8=''): TOutcome;
var
  TableIndex: integer;
  Rest: TSqlRest;
  json, sql: RawUTF8;
begin
  try
    sql := Rest.SQLComputeForSelect(Value.RecordClass, ACustomFieldsCSV, sqlWhere) + #13' LIMIT 1';
    ReplaceRowID (sql);
    json := Rest.EngineList EngineList (sql,false);

    TableIndex := Model.GetTableIndex (Table);
    Rest := fStaticVirtualTable[TableIndex];
    jsonRecords := Rest.EngineList (sql,false);
    result.SetSuccess;
    {TODO -oOwner -cGeneral : ovde gore ako pukne mormot maskira pa ne znamo da je problem u releas-u kad nema delfija da reaguje...}
  except on e: exception do
    begin
      result := ServerErrorOutcome(seException, e.Message);
      TSQLLog.Add.Log (sllInfo, sql);
    end;
    //raise;
  end;
end;
*)
function TSQLRestServerDBEx.Retrieve(const SQLWhere: RawUTF8; Value: TSQLRecord;
      const aCustomFieldsCSV: RawUTF8=''): TOutcome;
begin
  {if DirectRead
      then result := DirectRetrieve(SQLWhere, Value, aCustomFieldsCSV)
          else }result.Success := inherited Retrieve(SQLWhere, Value, aCustomFieldsCSV);
end;

function TSQLRestServerDBEx.Retrieve (aID: TID; Value: TSQLRecord; var Outcome: TOutcome;
                                      ForUpdate: boolean=false): boolean;
begin
  Value.IDValue := aID;
  exit (Retrieve (Value, Outcome, ForUpdate));
end;

function TSQLRestServerDBEx.ReadLock(Table: TSQLRecordClass; aID: TID; var Outcome: TOutcome): boolean;
begin
  var rec := Table.Create;
  result := inherited Retrieve(AID, rec, true);
  Outcome.Success := result;
  rec.Free;
end;

function TSQLRestServerDBEx.ReadLock(rec: TSQLRecord; var Outcome: TOutcome): boolean;
var
  AlreadyLocked: boolean;
  //         Locker.Locked    Mormot.Locked
  // case A: not locked       false            we lock and exit
  // case B: locked by this   true             we just read and exit
  // case C: locked by other  true             we just read and exit

  // case B: locked by same user, locked by mormot, Lock returns true, alreadylocked=true, we just read and exit
  // case C: locked by unknown, locked by mormot, Lock returns true, alreadylocked=true, we just read and exit
begin
  var ID := rec.IDValue;

  if UseRecordLocker and not RecordLocker.Lock (rec, AlreadyLocked) // true if not locked, or locked by same user, or locked but timed-out
     then begin
            Outcome := ServerErrorOutcome(seRecordAlreadyLocked);
            exit (false);
          end;

  // if we get this far, locking was allowed but record could have been already previously locked on mormot level by same user
  // retrive record with mormot-lock only if not alrady locked and exit if successful
  if inherited Retrieve (ID, rec, not AlreadyLocked)
     then begin
            Outcome.SetSuccess;
            exit (true);
          end;

  // we are here because mormot has it already locked, unlock our lock
  if UseRecordLocker
     then RecordLocker.Unlock (rec.RecordClass, ID);

  // failed, so unlock record on mormot level if we are the ones who locked it
  if not AlreadyLocked
     then inherited UnLock(rec.RecordClass, ID);

  //try to see why it failed
  if inherited Retrieve (ID, rec, false)
     then begin
            if UseRecordLocker
               then RecordLocker.Lock (rec.RecordClass, ID, AlreadyLocked, true); // add a lock by unknown so that it can time out in 30 sec
            Outcome := ServerErrorOutcome(seRecordAlreadyLockedByMormot);
            exit (false);
          end
     else begin
            Outcome := ServerErrorOutcome(seRecordNotFound);
            exit (false);
          end;
end;

procedure TSQLRestServerDBEx.RegisterDBChange(const SqlRecordClass: TSqlRecordClass; const ID: TID; const Operation: TCRUD);
var
  dbc: TDBChange;
begin
  exit;

  dbc.SqlRecordClass := SqlRecordClass;
  dbc.ID := ID;
  dbc.Operation := Operation;
  if ServiceContext.Request.IsAssigned
     then dbc.Session := ServiceContext.Request.Session
     else dbc.Session := 0;
  dbc.TimeStamp := now;
  DBChanges.Add(dbc);
  while (DBChanges.Count>0) and (SecondsSince (DBChanges[0].TimeStamp) > 30) do
        DBChanges.Delete(0);
end;

function TSQLRestServerDBEx.RestrictIfTableHasForeignKey(Table: TSQLRecordClass; const FieldName: RawUTF8; FKID: TID; var Outcome: TOutcome): boolean;
begin
  result := TableHasForeignKey (Table, FieldName, FKID);
  if result
     then begin
            Outcome := ServerDBErrorOutcome(seCascadeRestriction, Table.SQLTableName.AsString, FieldName.AsString);
          end;
end;

procedure TSQLRestServerDBEx.LockAndStartTransaction(const Reason: string = '');
begin
  try
    LogTransactionInfo (TTransactionOperation.Start, Reason);

    if (TranCount > 0) and (TransactionRequest <> ServiceContext.Request)
      then raise Exception.Create('ServiceContext.Request <> TransactionRequest!');

    inc (TranCount);

    if TranCount > 1 then exit; // already started/locked

    TransactionRequest := ServiceContext.Request;

    fAcquireExecution[execORMWrite].Safe.Lock;
    if not DBConnectionProperties.MainConnection.Connected
       then DBConnectionProperties.MainConnection.Connect;
    DBConnectionProperties.MainConnection.StartTransaction;
  except
    halt; // if this fails it is too risky to do anything else imo
  end;
end;

function TSQLRestServerDBEx.MultiFieldValuesEx (Table: TSQLRecordClass; const FieldNames, WhereClauseFormat: RawUTF8;
                                              const BoundsSQLWhere: array of const; var jsonRecords: RawUTF8): TOutcome;
var where: RawUTF8;
begin
  where := FormatUTF8(WhereClauseFormat, [], BoundsSQLWhere);
  result := MultiFieldValuesEx(Table, FieldNames, where, jsonRecords);
end;

// look for unique fields, see if there is already a different record with that value to avoid duplicate key errors
//function TSQLRestServerDBEx.CheckUniqueFields(rec: TSqlRecord): TOutcome;
//var
//  ID: TID;
//  value: RawUTF8;
//begin
//  result.SetSuccess;
//  for var field in rec.RecordProps.Fields.List do
//    if aIsUnique in field.Attributes then
//      begin
//        value := field.GetValue(rec, true);
//        if field.SQLFieldType in STRING_FIELDS then value := '''' + value + '''';
//
//        ID := OneFieldValueInt64(rec.RecordClass, 'rowID',
//                                 FormatUTF8 ('(% = %)', [field.Name, value], []), 0);
//        if ID <> rec.IDValue

//      end;
//end;

procedure TSQLRestServerDBEx.CheckDevAccounts;
begin
  if not HasConsole then exit;

  {$IFDEF DEBUG}
  if OneFieldValueInt64 (Model.AuthUserClass, 'ID', 'Developer=1') = 0
    then begin
           WriteLn ('Devs not found, create?');
           if ReadConsoleCharacter = 'y' then
              Model.AuthGroupClass.CreateDefaultUsers (self);
         end;
  {$ENDIF}
end;

procedure TSQLRestServerDBEx.CheckTriggers;
var
  sql: RawUTF8;
  Rows: ISQLDBRows;
  Segments: TArray<string>;

  procedure Check (TableName: string);
  begin
    if Model.Table[StringToUTF8 (TableName)] = nil then
      begin
        TSQLLog.Add.Log(TSynLogInfo.sllWarning, FormatUTF8 ('Trigger: % TABLE DOES NOT EXIST: %', [rows.ColumnString(0), TableName]));
        TSQLLog.Add.Log(TSynLogInfo.sllWarning, FormatUTF8 ('DROP TRIGGER % ON %;', [rows.ColumnString(0), Segments[0]]));
      end;
  end;

begin
  sql := 'SELECT trigger_name FROM information_schema.triggers where trigger_name like ''%_bd''';
  rows := Fdb.Execute(sql, []);
  while rows.Step do
    begin
      Segments := rows.ColumnString(0).Split (['_']);
      if length (Segments) = 3 then
        begin
          Check (Segments[0]);
          Check (Segments[1]);
        end;
    end;
end;

procedure TSQLRestServerDBEx.CheckForObsoleteSchema (ModelFull: TSqlModelEx);
var
  Table: TSQLRecordClass;
  Rows: ISQLDBRows;
  i : integer;
  sql: RawUTF8;
begin
  for table in model.Tables do
    begin
      if ClassHasAttribute (Table, IgnoreObsoleteFieldsAttribute)
         then continue;

      if not FDB.TableExists (Table.SQLTableName) then continue;

      sql := FormatUTF8('SELECT * FROM % LIMIT 1', [table.SQLTableName.ToUpper]);
      rows := Fdb.Execute(sql, []);
      for i := 0 to rows.ColumnCount - 1 do
        if not table.FieldInfoList.HasField(rows.ColumnName(i).AsString)
           then
             begin
                var ctx := TRttiContext.Create;
                try
                  var attr := ctx.GetAttribute<IgnoreObsoleteFieldsAttribute>(Table);
                  if not Assigned (attr) or (attr.Fields.IndexOf (rows.ColumnName(i).AsString) = -1) then
                    slSqlDeleteObsoleteFieldsTables.Add (format ('ALTER TABLE %s DROP COLUMN %s;',
                                                        [table.SQLTableName.ToUpper.AsString, rows.ColumnName(i).AsString]));
                finally
                  ctx.Free;
                end;
             end;
      rows.ReleaseRows;
    end;

  sql := 'SELECT table_name, table_type FROM information_schema.tables ' +
         'WHERE table_schema = ''public'' AND table_type IN (''BASE TABLE'', ''VIEW'') ORDER BY table_name';
  rows := Fdb.Execute(sql, []);

  while rows.Step do
    if ModelFull.Table[rows.ColumnUTF8 (0)] = nil then
      if rows.ColumnUTF8 (1) = 'VIEW'
         then slSqlDeleteObsoleteFieldsTables.Insert (0, 'DROP VIEW ' + rows.ColumnString (0) + ';')
         else slSqlDeleteObsoleteFieldsTables.Insert (slSqlDeleteObsoleteFieldsTables.Count, 'DROP TABLE ' + rows.ColumnString (0) + ';');

  sql := 'SELECT tc.table_schema, tc.table_name, kc.column_name, tc.constraint_name ' +
          'FROM information_schema.table_constraints AS tc ' +
          'JOIN information_schema.key_column_usage AS kc ' +
          '    ON tc.constraint_name = kc.constraint_name ' +
          '    AND tc.table_schema = kc.table_schema ' +
          'WHERE tc.constraint_type = ''UNIQUE'' ' +
          'ORDER BY tc.table_schema, tc.table_name, kc.column_name;';
  rows := Fdb.Execute(sql, []);
  while rows.Step do
    begin
      var TableName := rows.ColumnUTF8 (1);
      table := ModelFull.Table[TableName];
      if table = nil then Continue;
      var FieldName := rows.ColumnString (2);
      var fi := table.FieldInfoList.ItemsByName[FieldName];

      if fi.IsAssigned and not (aIsUnique in fi.MormotFieldInfo.Attributes)
        then slSqlDeleteObsoleteFieldsTables.Add (FormatUTF8 ('ALTER TABLE % DROP CONSTRAINT %',
             [table.SQLTableName, rows.ColumnString (3)]).AsString);
    end;

  if slSqlDeleteObsoleteFieldsTables.Count > 0 then
    begin
      TSQLLog.Add.Log(sllCommonWarning,'');
      TSQLLog.Add.Log(sllCommonWarning,'================================================');
      TSQLLog.Add.Log(sllCommonWarning,'');
      for i := 0 to slSqlDeleteObsoleteFieldsTables.Count - 1 do
        TSQLLog.Add.Log(sllCommonWarning, slSqlDeleteObsoleteFieldsTables[i]);
      TSQLLog.Add.Log(sllCommonWarning,'');
      TSQLLog.Add.Log(sllCommonWarning,'================================================');
      TSQLLog.Add.Log(sllCommonWarning,'');
      TSQLLog.Add.Log(sllCommonWarning,'There are obsolete tables or fields?! Check log (above)');
     {$ifdef debug}
      if HasConsole then
        begin
          TSQLLog.Add.Log(sllCommonWarning,'Press [y, Enter] to execute the above SQL code or just [Enter] to continue.');
          var key := 'n';
          ReadLn (key);
          if key = 'y' then
            begin
              TSQLLog.Add.Log(sllCommonWarning, 'Are you absolutely sure?');
              ReadLn (key);
              if key = 'y' then
                FDB.ExecuteNoResult(StringToUTF8 (slSqlDeleteObsoleteFieldsTables.Text), []);
            end;
        end;
     {$endif}
    end;

end;

// look for unique fields, see if there is already a different record with that value to avoid duplicate key errors
// return first field name that has a value match
function TSQLRestServerDBEx.CheckUniqueFields(rec: TSqlRecord): TOutcome;
var
  ID: TID;
  value: RawUTF8;
  i: integer;
  field: TSQLPropInfo;
begin
  result.SetSuccess;

  for i := 0 to rec.RecordProps.Fields.Count-1 do
  begin
    field := rec.RecordProps.Fields.Items[i];

    if aIsUnique in field.Attributes then
      begin
        value := field.GetValue(rec, true);
        if field.SQLFieldType in STRING_FIELDS then value := '''' + value + '''';

        ID := OneFieldValueInt64(rec.RecordClass, 'rowID',
                                 FormatUTF8 ('(% = %)', [field.Name, value], []), 0);
        if (ID <> NULL_ID) and (ID <> rec.IDValue)
           then exit (ServerDBErrorOutcome(seDuplicateKey, rec.SQLTableName.AsString, field.Name.AsString));
      end;
  end;
end;

procedure TSQLRestServerDBEx.LogTransactionInfo(Operation: TTransactionOperation; const Reason: string);
begin
  if (TranCount = 0) and (Operation = TTransactionOperation.Start) then
     begin
       TSQLLog.Add.Log(sllCommonWarning, '');
       TSQLLog.Add.Log(sllCommonWarning,     ' =========   =========  ======');
     end;

  TSQLLog.Add.Log (sllCommonWarning, format (' TRANCOUNT  Operation  Reason',
                   [TranCount, System.TypInfo.GetEnumName(TypeInfo(TCrud), ord(Operation)), Reason]));

  TSQLLog.Add.Log (sllCommonWarning, format (' %9.9s  %9.9s  %s',
                   [TranCount.ToString, System.TypInfo.GetEnumName(TypeInfo(TTransactionOperation), ord(Operation)), Reason]));

  if (TranCount = 0) and (Operation = TTransactionOperation.Done) then
     begin
       TSQLLog.Add.Log(sllCommonWarning,     ' =========   =========  ======');
       TSQLLog.Add.Log(sllCommonWarning, '');
     end;

end;

procedure TSQLRestServerDBEx.CommitAndUnlock (const Reason: string = '');
begin
  try
    if ServiceContext.Request <> TransactionRequest
      then raise Exception.Create('ServiceContext.Request <> TransactionRequest!');

//    add datetime add session?
    if TranCount = 0
      then raise Exception.Create('Trying to commit but transaction count is zero!');

    LogTransactionInfo (TTransactionOperation.Commit, Reason);

    dec (TranCount);
    if TranCount > 0 then exit;

    DBConnectionProperties.MainConnection.Commit;
    fAcquireExecution[execORMWrite].Safe.UnLock;

    LogTransactionInfo (TTransactionOperation.Done, Reason);
  except
    RollbackAndUnlock;
  end;
end;

procedure TSQLRestServerDBEx.RollbackAndUnlock(const Reason: string = '');
begin
  try
    if ServiceContext.Request <> TransactionRequest
      then raise Exception.Create('ServiceContext.Request <> TransactionRequest!');

    LogTransactionInfo (TTransactionOperation.Rollback, Reason);

    if TranCount = 0 then exit; // must have been rolled back elsewhere... hmm... e.g. haltstockprocessing

    dec (TranCount);
    if TranCount > 0 then exit;

    DBConnectionProperties.MainConnection.Rollback;
    fAcquireExecution[execORMWrite].Safe.UnLock;

    LogTransactionInfo (TTransactionOperation.Done, Reason);
  except
    halt; // if this fails it is too risky to do anything else imo
  end;
end;

procedure TSQLRestServerDBEx.UnLockAndEndTransaction(Success: boolean; const Reason: string = '');
begin
  if Success
    then CommitAndUnlock (Reason)
    else RollbackAndUnlock (Reason);
end;

function TSQLRestServerDBEx.Sessions: TSynObjectListLocked;
begin
  result := fSessions;
end;

procedure TSQLRestServerDBEx.Sum(Ctxt: TSQLRestServerURIContext);
begin
  // a test published method
  with Ctxt do
    Results([InputDouble['a']+InputDouble['b']]);
end;

function TSQLRestServerDBEx.UnLock(Table: TSQLRecordClass; aID: TID): boolean;
begin
  result := (not UseRecordLocker or RecordLocker.UnLock (Table, aID)) and
            inherited Unlock (Table, aID);

  if result and (Table.crudClass <> Table)
     then result := UnLock (Table.crudClass, AID);
end;

function TSQLRestServerDBEx.UnLock(rec: TSQLRecord): boolean;
begin
  if rec = nil then exit (true);

  result := (not UseRecordLocker or RecordLocker.UnLock (rec)) and
            inherited Unlock (rec.RecordClass, rec.IDValue);

  if result and (rec.crudClass <> rec.RecordClass)
     then result := UnLock (rec.crudClass, rec.IDValue);
end;

function TSQLRestServerDBEx.ValidateUniqueIfNotNull (rec: TSQLRecord): string;
var
  value: string;
  HasValue: boolean;
begin
  if rec is TSqlRecordEx
    then
      begin
        for var FieldName in TSqlRecordEx(rec).UniqueIfNotNullFields do
          begin
            var f := rec.FieldByName (FieldName);

            if f.SQLFieldType in STRING_FIELDS
               then begin
                      value := rec.GetFieldValue (StringToUTF8 (FieldName)).AsString;
                      HasValue := not value.IsEmpty;
                    end
               else try
                      // assume numeric
                      var v := rec.GetFieldVariantValue (StringToUTF8 (FieldName));
                      HasValue := not VarIsEmptyOrNull (v) and (v > 0);
                      if not HasValue then exit;
                      value := VarToStr(v);
                    except on e: exception do
                      exit (e.Message);
                    end;

            //value := rec.GetFieldValue (StringToUTF8 (FieldName)).AsString;

            if HasValue then
              begin
                var WhereClause := FormatUTF8 ('(% = ''%'')', [FieldName, value], []);
                var id := OneFieldValue(rec.RecordClass, 'ID', WhereClause);
                // if record is found and the ID doesnt match the ID of the crud record then disallow
                if (id <> '') and (id.AsString <> rec.IDValue.ToString) then
                  exit (format (tcFieldMustHaveUniqueValue, [FieldName]));
              end;
          end;

      end
    else result := '';
end;

procedure TSQLRestServerDBEx.Wipe(Tables: TArray<TSqlRecordClass>);
begin
  for var Table in Tables do
    Execute ('DELETE FROM ' + Table.SQLTableName);
end;

procedure TSQLRestServerDBEx.Wipe (Table: TSqlRecordClass);
begin
  Execute ('DELETE FROM ' + Table.SQLTableName);
end;

procedure TSQLRestServerDBEx.WipeAllTables;
begin
// hmm redosled...
  raise Exception.Create('Not implemented');
end;

procedure TSQLRestServerDBEx.CheckValidation (Value: TSQLRecord; var Outcome: TOutcome);
var
  err: string;
begin
  err := Value.ValidateEx ('', ImportInProgress);
  if err = ''
     then err := ValidateUniqueIfNotNull (Value);
  if err <>''
     then Outcome := ServerDBErrorOutcome(seValidationFailed, Value.SQLTableName.AsString, '', err)
     else Outcome.Success := true;
end;

procedure TSQLRestServerDBEx.CopyAggregateRecords (const AggregateTableName, MasterIDName, DetailIDName: string;
          ExistingMasterID, NewMasterID: TID);
var
  sql: RawUTF8;
begin
  sql.AsString := Format('INSERT INTO %s (%s, %s) ' +
                   'SELECT %d, %s FROM %s WHERE %s = %d;',
                   [AggregateTableName, MasterIDName, DetailIDName,
                    NewMasterID, DetailIDName, AggregateTableName,
                    MasterIDName, ExistingMasterID]);

  DataBase.ExecuteNoResult(sql, []);
end;

function TSQLRestServerDBEx.CheckCUFailure(rec: TSQLRecord; Outcome: TOutcome): TOutcome;
var
  sql, val: RawUTF8;
  fi: TFieldInfo;
  rows: ISQLDBRows;
begin
  for fi in rec.FieldInfoList do
    if fi.MormotFieldInfo.IsAssigned and (aIsUnique in fi.MormotFieldInfo.Attributes) then
      begin
        val.AsString := rec.GetFieldStringValue(StringToUTF8(fi.Name));
        if fi.IsTextual then val := QuotedStr(val);

        sql := FormatUTF8 ('SELECT ID FROM % WHERE % = % and ID <> %',
               [rec.SQLTableName, fi.Name, val, rec.IDValue]);

        rows := DataBase.Execute (sql, []);

        if rows.Step then
          begin
            Outcome.ErrorCode := seDuplicateKey;
            Outcome.ErrorFieldName := fi.Name;
            Outcome.Description := 'Već postoji slog sa istom vrednosti polja koje mora biti jedinstveno.';
            exit (Outcome);
          end;
      end;
  result := Outcome;
end;

function TSQLRestServerDBEx.UpdateEx(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''; DoNotAutoComputeFields: boolean = false): TOutcome;
var
  json: RawUTF8;
begin
  result.SetSuccess;
  CheckValidation (Value, result);
  if not result.Success then exit;

  if Settings.ChangeLog
    then result := RetrieveAsJson (Value.RecordClass, Value.IDValue, json)
    else json := '';
  if not result.Success then exit;

  result.Success := inherited Update (Value, CustomCSVFields, DoNotAutoComputeFields);
  if not result.Success then exit (CheckCUFailure(Value, result));

  if Settings.ChangeLog
     then AddChangeLog (Value.RecordClass, Value.IDValue, TCrud.Update, json);

  RegisterDBChange (Value.RecordClass, Value.ID, TCRUD.Update);
  Result.ID := Value.IDValue;

  if Settings.ExecLog then DoExecLog (Value, TCrud.Update, result);
end;

function TSQLRestServerDBEx.UpdateEx(Value: TSQLRecord; const CustomFields: TSQLFieldBits; DoNotAutoComputeFields: boolean): TOutcome;
begin
  result.SetSuccess;
  CheckValidation (Value, result);
  result.Success := result.Success and inherited Update (Value, CustomFields, DoNotAutoComputeFields);
  if Result.Success
     then RegisterDBChange (Value.RecordClass, Value.ID, TCRUD.Update);
end;

function TSQLRestServerDBEx.Update(Value: TSQLRecord; const CustomFields: TSQLFieldBits=[]; DoNotAutoComputeFields: boolean=false): boolean;
begin
  result := inherited;
//  outcome.SetSuccess;
//  outcome := UpdateEx (Value, CustomFields, DoNotAutoComputeFields);
//  result := outcome.Success;
end;

function TSQLRestServerDBEx.UpdateOrCrash(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''): TOutcome;
begin
  result.Success := Update (Value, CustomCSVFields);
  if not result.Success
     then raise Exception.Create('Mandatory ADD failed'#13 + result.Description);
end;

function TSQLRestServerDBEx.UpdateAndUnlock(Value: TSQLRecord; const CustomCSVFields: RawUTF8; DoNotAutoComputeFields: boolean): TOutcome;
begin
  if UseRecordLocker and not RecordLocker.IsLocked (Value)
    then exit (ServerErrorOutcome(seRecordNotLocked));

  result := UpdateEx (Value, CustomCSVFields, DoNotAutoComputeFields);

  if result.Success then UnLock(Value);
end;

procedure TSQLRestServerDBEx.UpdateDBVersion(ModelHash: RawUTF8; NewVersion: integer = 0);
begin
  var ID := OneFieldValueInt64(TSqlDatabaseInfo, 'RowID', '');
  if NewVersion = 0 then NewVersion := TSqlDatabaseInfo.ExpectedVersion;
  var rec := TSqlDatabaseInfo.Create;
  rec.VersionNumber := NewVersion;
  rec.ModelHash := ModelHash;
  rec.IDValue := ID;
  AddOrUpdate(rec);
  rec.Free;

  TSQLLog.Add.Log(sllCommonWarning, 'DB Version updated, new version is %' , [NewVersion]);
end;

function TSQLRestServerDBEx.EnumValueAdded (Table: TSqlRecordClass; const FieldName: RawUTF8; const NewValue, NewVersion: integer): integer;
var
  sql: RawUTF8;
begin
  try
    FDB.MainConnection.StartTransaction;
    sql := FormatUTF8 ('UPDATE % SET % = % + 1 WHERE % >= %', [Table.SqlTableName, FieldName, FieldName, FieldName, NewValue], []);
    result := FDB.ExecuteNoResult(sql, []);
    UpdateDBVersion('', NewVersion); // this is a temporary update
    FDB.MainConnection.Commit;
  except
    FDB.MainConnection.Rollback;
    raise
  end;
end;


procedure TSQLRestServerDBEx.Image(Ctxt: TSQLRestServerURIContext);
begin
(*
idk how to use this on client side, replaced with interface which is better anyway

if ImagePath = '' then ImagePath := TPath.Combine (ExtractFilePath (ParamStr(0)), 'Images');

  if ContextUserInfo = nil then
    begin
      Ctxt.Results([], HTTP_FORBIDDEN);
      exit;
    end;

  var FileName := Ctxt.InputString['Name'];
  if FileName.IndexOfAny (['\', '/'{, '.'}]) > 0 then
    begin
      Ctxt.Results([], HTTP_FORBIDDEN);
      exit;
    end;

  FileName := TPath.Combine (ImagePath, FileName);

  if FileExists (FileName)
     then begin
            var blob : TSQLRawBlob;
            var fs := TFileStream.Create(FileName, fmOpenRead);
            blob := StreamToRawByteString(fs);
            fs.Free;
            Ctxt.ReturnFile(FileName);
            //Ctxt.Results ([blob]);
          end
     else Ctxt.Results ([], HTTP_NOTFOUND);
*)
end;

function TSQLRestServerDBEx.Delete(Table: TSQLRecordClass; ID: TID): boolean;
var
  o: TOutcome;
begin
  o.SetSuccess;
  o := DeleteEx (Table, ID);
  result := o.Success;
end;

function TSQLRestServerDBEx.DeleteDetailRecords(Tables: TArray<TSQLRecordClass>; FKName: RawUTF8; ID: TID): TOutcome;
begin
  for var table in Tables do
    Execute (FormatUTF8 ('DELETE FROM % where % = %', [table.SQLTableName, FKName, ID]));
  result.SetSuccess;
end;

function TSQLRestServerDBEx.DeleteDetailsRecursively(Table: TSQLRecordClass; ID: TID): TOutcome;
// minor optimization: bilo bi brze kad bi bio IDs: array<TID> da ne proverava za svaki record gde su details
var
  DetailTable: TSQLRecordClass;
  FieldInfo: TFieldInfo;
  SqlRecords: TSqlRecords;
  rec: TSqlRecord;
  sqlWhere: RawUTF8;
begin
  result.Success := true;
  for DetailTable in Model.lstTables do
    begin
      for FieldInfo in DetailTable.FieldInfoList do
        if FieldInfo.IsForeignKey and (FieldInfo.ForeignTableName = Table.SQLTableName.AsString) then
           begin
             sqlWhere := FormatUTF8('% = %', [FieldInfo.Name, ID]);
             SqlRecords := RetrieveSqlRecords(DetailTable, sqlWhere, [], 'ID');
             for rec in SqlRecords.Records do
               begin
                 result := DeleteDetailsRecursively(rec.RecordClass, rec.IDValue);
                 if result.Success
                    then result := DeleteEx (rec);
                 if not result.Success then break;
               end;
           end;
    end;
end;

procedure TSQLRestServerDBEx.ReplaceRowID (var sql: RawUTF8);
var
  pRowID: integer;
begin
  pRowID := SynCommons.Pos (' RowID,', sql);
  if (pRowID > 1) {and (sql[pRowID-1] = #13) }then
    begin
      sql[pRowID+1] := ' ';
      sql[pRowID+2] := ' ';
      sql[pRowID+3] := ' ';
    end;
end;

function TSQLRestServerDBEx.MultiFieldValuesDirect(Table: TSQLRecordClass; var sql, jsonRecords: RawUTF8): TOutcome;
var
  TableIndex: integer;
  Rest: TSqlRest;
begin
  try
    ReplaceRowID (sql);
    TableIndex := Model.GetTableIndex (Table);
    Rest := fStaticVirtualTable[TableIndex];
    jsonRecords := Rest.EngineList (sql,false);
    result.SetSuccess;
    {TODO -oOwner -cGeneral : ovde gore ako pukne mormot maskira pa ne znamo da je problem u releas-u kad nema delfija da reaguje...}
  except on e: exception do
    begin
      result := ServerErrorOutcome(seException, e.Message);
      TSQLLog.Add.Log (sllInfo, sql);
    end;
    //raise;
  end;
end;

function TSQLRestServerDBEx.MultiFieldValuesEx (Table: TSQLRecordClass; const FieldNames, WhereClause: RawUTF8;
                                                var jsonRecords: RawUTF8): TOutcome;
var sql: RawUTF8;
begin
  sql := trim (SQLComputeForSelect(Table,FieldNames,WhereClause));
  if sql=''
    then exit (ServerErrorOutcome(seUnknownTable));

  if DirectRead
    then result := MultiFieldValuesDirect(Table, sql, jsonRecords)
    else try
           jsonRecords := EngineList (sql);
           result.SetSuccess;
         except on e: exception do
           result := ServerErrorOutcome(seException, e.Message);
         end;
{
var
  TableIndex: integer;
  Rest: TSqlRest;
  SqlTable: TSQLTable;
  json, sql : RawUTF8;
  ReturnedRowCount: integer;

begin
  if Parameters.OrderBy = ''
    then Parameters.OrderBy := Table.DefaultSortField;

  TableIndex := Model.GetTableIndex (Table);
  sql := Parameters.MormotSql;
  Rest := fStaticVirtualTable[TableIndex];
  sql := Rest.SQLComputeForSelect(Table, aCustomFieldsCSV, sql);
  json := Rest.EngineList (sql,false);
  SqlTable := TSQLTableJSON.CreateFromTables([Table],sql,json);
  SqlTable.ToObjArray(ObjArray,Table);
  SqlTable.Free;
end;
}
end;

procedure TSQLRestServerDBEx.CreateReflectionTriggers;
//var
//  RecordClass: TSQLRecordClass;
//  sql: RawUTF8;
begin
//  for RecordClass in Fmodel.Tables do
//    if RecordClass is TSqlRecordExClass then
//      begin
//        sql := TSqlRecordExClass(RecordClass).ReflectionTriggerSQL;
//        if sql <> '' then ExecuteNoResult(sql);
//      end;
//
end;

procedure TSQLRestServerDBEx.CreateTriggers; // not used... 99% sure
//var
//  sql, Field, GroupField, GroupCondition: RawUTF8;
//  sqlCode, TriggerName, ExistingSQL: string;
//  rows: ISQLDBRows;
begin
(*  rows := FDB.Execute ('select RDB$TRIGGER_NAME, RDB$TRIGGER_SOURCE from RDB$TRIGGERS', []);
  while rows.Step() do
    dicTriggers.Add (rows.ColumnString(0), rows.ColumnString(1));
  rows.ReleaseRows;

  // for table order fields?
  for var tt in Fmodel.TableOrderFields.Keys do
    begin
      field := FModel.TableOrderFields[tt];
      GroupField := Fmodel.TableOrderGroupFields[tt];

      if GroupField <> ''
       then GroupCondition := FormatUTF8('and % = new.%', [GroupField, GroupField])
       else GroupCondition := '';

      // check if updating order field exists
      rows := FDB.Execute ('select count ( * ) from RDB$RELATION_FIELDS WHERE RDB$RELATION_NAME = ? AND RDB$FIELD_NAME = ''UPDATING_ORDER''', [tt.SQLTableName.ToUpper]);
      rows.Step;
      if rows.ColumnInt (0) <> 1 then
        FDB.Execute ('alter table ' + tt.SQLTableName + ' add UPDATING_ORDER INTEGER', []);
      rows.ReleaseRows;

      TriggerName := ('TR_BI_' + tt.SQLTableName.AsString + '_' + field.AsString).ToUpper;
      dicTriggers.TryGetValue(TriggerName, ExistingSQL);
      sql     := FormatUTF8(FB_ORDER_TRIGGER, [TriggerName, tt.SQLTableName, 'INSERT']);
      sqlCode := FormatUTF8(FB_ORDER_BI_TRIGGER_CODE, [tt.SQLTableName, Field, Field, Field, Field, GroupCondition]).AsString;

      if ExistingSQL + ' ;' <> 'AS ' + sqlCode then
         FDB.Execute (sql + StringToUTF8 (sqlCode), []);

      TriggerName := ('TR_BU_' + tt.SQLTableName.AsString + '_' + field.AsString).ToUpper;
      dicTriggers.TryGetValue(TriggerName, ExistingSQL);
      sql     := FormatUTF8(FB_ORDER_TRIGGER, [TriggerName, tt.SQLTableName, 'UPDATE']);
      sqlCode := ReplaceStr(FB_ORDER_BU_TRIGGER_CODE, '%f', field.AsString);
      sqlCode := ReplaceStr(sqlCode, '%t', tt.SQLTableName.AsString);
      sqlCode := ReplaceStr(sqlCode, '%c', GroupCondition.AsString);

      if ExistingSQL + ' ;' <> 'AS ' + sqlCode then
         FDB.Execute (sql + StringToUTF8 (sqlCode), []);

      TriggerName := ('TR_BD_' + tt.SQLTableName.AsString + '_' + field.AsString).ToUpper;

      if GroupField <> ''
       then GroupCondition := FormatUTF8('and % = old.%', [GroupField, GroupField])
       else GroupCondition := '';

      dicTriggers.TryGetValue(TriggerName, ExistingSQL);
      sql     := FormatUTF8(FB_ORDER_TRIGGER, [TriggerName, tt.SQLTableName, 'DELETE']);
      sqlCode := FormatUTF8(FB_ORDER_BD_TRIGGER_CODE, [tt.SQLTableName, Field, Field, Field, Field, GroupCondition]).AsString;

      if ExistingSQL + ' ;' <> 'AS ' + sqlCode then
         FDB.Execute (sql + StringToUTF8 (sqlCode), []);

    end;

{  if result.OrderExists <> ''
     then begin
            var rec := tt.Create;
            try
              result.RecordFound := Retrieve(ID, rec);
              if result.RecordFound then
                begin
                  result.Value := rec.GetFieldVariantValue(result.Field);
                  if result.GroupField <> ''
                    then result.GroupFieldValue := rec.GetFieldVariantValue(result.GroupField);
            RestServer.LockAndStartTransaction;
          end;
  try
    if OrderGroupField <> ''
       then OrderGroupFieldCondition := FormatUTF8('and % = %', [OrderGroupField, OrderGroupFieldValue])
       else OrderGroupFieldCondition := '';

  finally

  end;
}
*)
end;

procedure InstallService(const ServiceName, DisplayName, LoadOrder: PChar;
  const FileName: string; const Port: SockString; const Silent: Boolean);
const
  cInstallMsg = 'Your service was Installed sucesfuly!';
  cSCMError = 'Error trying to open SC Manager';
var
  SCMHandle  : SC_HANDLE;
  SvHandle   : SC_HANDLE;
begin
  SCMHandle := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);

  if SCMHandle = 0 then
    begin
      if HasConsole
         then WriteLn (cSCMError)
         else MessageBox(0, cSCMError, ServiceName, MB_ICONERROR or MB_OK or MB_TASKMODAL or MB_TOPMOST);
      Exit;
    end;

  THttpApiServer.AddUrlAuthorize('',Port{'8080'},false,'+');

  try
    SvHandle := CreateService(SCMHandle,
                              ServiceName,
                              DisplayName,
                              SERVICE_ALL_ACCESS,
                              SERVICE_WIN32_OWN_PROCESS,
                              SERVICE_AUTO_START,
                              SERVICE_ERROR_IGNORE,
                              pchar(FileName),
                              LoadOrder,
                              nil,
                              nil,
                              nil,// pchar('NT AUTHORITY\NetworkService'),  // NT AUTHORITY\NetworkService
                              nil);
    CloseServiceHandle(SvHandle);

    if not Silent then
      if HasConsole
         then WriteLn (cInstallMsg)
         else MessageBox(0, cInstallMsg, ServiceName, MB_ICONINFORMATION or MB_OK or MB_TASKMODAL or MB_TOPMOST);
  finally
    CloseServiceHandle(SCMHandle);
  end;
end;


function TMormotServiceImplementer.SessionUserID: TID;
begin
  result := ServiceContext.Request.SessionUser;
end;

function TMormotServiceImplementer.ValidateParameters(const Parameters: TQueryParameters): boolean;
begin
  if Parameters.MasterTableName <> '' then
     if Model.GetTableIndex(Parameters.MasterTableName) = -1
        then exit (false);
  result := true;
end;

class function TMormotServiceImplementer.AuthUserClass: TSqlAuthUserExClass;
begin
  result := RestServer.Model.AuthUserClass;
end;

function TMormotServiceImplementer.FGetFile(const FileName: TFileName): TServiceCustomAnswer;
begin
  result := default (TServiceCustomAnswer);
  if FileExists (FileName)
     then
       begin
         var fs := TFileStream.Create(FileName, fmOpenRead);
         try
           result.Content := StreamToRawByteString(fs);
           result.Status := HTTP_SUCCESS;
           result.Header := BINARY_CONTENT_TYPE;
         finally
           fs.Free;
         end;
       end
     else
       begin
         Result.Header := TEXT_CONTENT_TYPE_HEADER;
         result.Status := HTTP_NOTFOUND;
       end;
end;

procedure TMormotServiceImplementer.LogWarning(const s: string);
begin
  TSQLLog.Add.Log(TSynLogInfo.sllWarning, s);
end;

function TMormotServiceImplementer.Model: TSqlModelEx;
begin
  result := TSqlModelEx(RestServer.Model);
end;


function ConsoleWaitForOneOf (const Keys: array of word): cardinal;
{$ifdef DELPHI5OROLDER}
begin
  readln;
end;
{$else}
var msg: TMsg;
    key: word;
begin
  result := 0;
  while true do
    begin
      for key in Keys do
        if ConsoleKeyPressed(key) then exit (key);
      {$ifndef LVCL}
      if GetCurrentThreadID=MainThreadID then
        CheckSynchronize{$ifdef WITHUXTHEME}(1000){$endif}  else
      {$endif}
        WaitMessage;
      while PeekMessage(msg,0,0,0,PM_REMOVE) do
        if Msg.Message=WM_QUIT then
          exit else begin
          TranslateMessage(Msg);
          DispatchMessage(Msg);
        end;
    end;
end;
{$endif DELPHI5OROLDER}

//procedure ConsoleWarning (const s: string);
//begin
//  if not HasConsole then exit;
//
//  TextColor (TConsoleColor.ccYellow);
//  WriteLn (s);
//  TextColor (TConsoleColor.ccWhite);
//end;

// check if all tables that are not aggregations have general filter fields
// check if a view is mistreated as a table
procedure ModelSanityCheck (Model: TSqlModel);
begin
  for var Table in Model.Tables do
    begin
      if not Table.IsAggregation and (Table.GeneralFilterFields.Count = 1) and (Table.GeneralFilterFields[0] = 'RowID')
          then TSynLog.Add.Log (sllWarning, 'No general filter fields for ' + Table.SQLTableName);

      if not Table.IsView and Table.SQLTableName.AsString.EndsWith('View')
         then raise Exception.Create(Table.SQLTableName.AsString + ' is not a view?');
    end;
end;

constructor TServerSettings.Create(const AFileName: TFileName);
begin
  inherited Create;
  FileName := AFileName;
  if not FileName.Contains ('\')
    then
      begin
        // try from current folder first
        FileName := TPath.Combine (GetCurrentDir, AFileName);
        if not TFIle.Exists (FileName)
          then FileName := TPath.Combine (ExtractFilePath (ParamStr(0)), AFileName);
      end;
//  ObjectToJSONFile(self, fn+'.tmp');
  if not JSONFileToObject(FileName, self, TServerSettings) then
    begin
      ObjectToJSONFile(self, FileName + '.template');
      raise Exception.Create('Failed to load JSON file into settings, check the created template file');
    end;

  if DBEngine = TSQLDBDefinition.dUnknown then
    raise Exception.Create('Please configure database in the settings file!');

  if ArchiveAfterDays = 0
     then ArchiveAfterDays := 10;

  if Port = '' then
    raise Exception.Create('Please configure port in the settings file!');

  AssertDllBitness (LibraryLocation);
end;

constructor TServerSettings.CreateForSqlite;
begin
  Create ('SQLite-Settings.json');
end;

{ TServiceSingleEx }

class procedure TServiceSingleEx.CheckRunParameters;
begin
  if FindCmdLineSwitch ('uninstall') then UnInstallAndHalt;
  if FindCmdLineSwitch ('install')   then InstallAndHalt;
  if FindCmdLineSwitch ('elevate') and (GetPrivilegeLevel = 0)
    then ElevateSelf;
end;

constructor TServiceSingleEx.Create;
begin
  SetCurrentDir(ApplicationExeFolder); // service otherwise starts in sys32
  inherited Create(Name, DisplayName);// SERVICE_NAME, SERVICE_DISPLAY_NAME);
end;

constructor TServiceSingleEx.CreateAsConsole;
begin
 // manual switch to console mode
  AllocConsole;
  // define the log level
  with TSQLLog.Family do begin
    Level := LOG_VERBOSE;
    EchoToConsole := LOG_STACKTRACE;
  end;
end;

class procedure TServiceSingleEx.DoIUAndHalt(Install: boolean);
begin
  var svc := Create;
  try
    write('.');
    if Install then svc.Install() else svc.Remove;
    write('. installed = ');
    write (svc.Installed);
  finally
    svc.Free;
  end;

  halt;
end;

class procedure TServiceSingleEx.InstallAndHalt;
begin
  write('Installing service ' + Name);
  DoIUAndHalt(true);
end;

class procedure TServiceSingleEx.Setup(const AName, ADisplayName: string);
begin
  Name := AName;
  DisplayName := ADisplayName;
end;

class procedure TServiceSingleEx.UnInstallAndHalt;
begin
  write('Uninstalling service ' + Name);
  DoIUAndHalt(false);
end;

{ TServerManagerService }

//procedure TServerManagerService.GetInfo (out Info: TServerInfo);
//begin
//  Info := TServerInfo.Create;
//  Info.ExeName.AsString := ApplicationExeName;
//  Info.Version.AsString := GetBuildInfoAsString;
//  Info.LogFile.AsString := TSQLLog.Add.FileName;
//end;

function TServerManagerService.CustomRequest (const Command: RawUTF8; const Parameters: variant; const Content: RawUTF8;
                                              out ResponseParameters: variant; out ResponseContent: RawUTF8): TOutcome;
var
  rec: TSqlRecord;
begin
  if Command.AsString.ToLower = 'delete recursively' then
    begin
      var tt := model.Table[SynCommons.UpperCase(StringToUTF8(Parameters.TableName))];
      rec := tt.Create;
      rec.IDValue := Parameters.IDValue;
      result := RestServer.DeleteDetailsRecursively (tt, rec.IDValue);
      if result.Success
         then result := RestServer.DeleteEx(tt, rec.IDValue);
      exit;
    end;

  result.SetError(seNotImplemented);
end;

function TServerManagerService.GetFKInfo (const TableName: RawUTF8; const ID: TID): TOutcome;
var
  sql: RawUTF8;
begin
  result.RecordCount := 0;
  for var Table in Model.Tables do
    if not Table.IsView then
      for var field in Table.FieldInfoList do
        if field.IsForeignKey and (field.ForeignTableName = TableName.AsString) then
            begin
              sql := FormatUTF8('select count (*) AS C from % WHERE % = %', [table.SQLTableName.ToUpper, field.Name, ID]);
              var rows := RestServer.DataBase.Execute (sql, []);
              rows.step;
              result.RecordCount := result.RecordCount + rows.ColumnInt('C');
            end;
  result.Success := true;
end;

function TServerManagerService.RedirectFKs (const TableName: RawUTF8; const ValidID, InvalidID: TID; DeleteInvalid: boolean): TOutcome;
var
  sql: RawUTF8;
begin
  if (ValidID = InvalidID) or (ValidID.IsNULL) or (InvalidID.IsNULL)
     then exit (ServerErrorOutcome(seInvalidParameters));

  result.RecordCount := 0;
  for var Table in Model.Tables do
    if not Table.IsView then
      for var field in Table.FieldInfoList do
        if field.IsForeignKey and (field.ForeignTableName = TableName.AsString) then
            begin
              sql := FormatUTF8('update % set % = % where % = %',
                                 [table.SQLTableName.ToUpper, field.Name, ValidID, field.Name, InvalidID]);
              result.RecordCount := result.RecordCount + RestServer.DataBase.ExecuteNoResult (sql, []);
            end;

  result.Success := true;
  if DeleteInvalid then
    begin
      var tcs := TCommonService.Create;
      try
        result := tcs.Delete(TableName, InvalidID);
      finally
        tcs.Free;
      end;
    end;
end;

//function TServerManagerService.ServerName: RawUTF8;
//begin
//  result.AsString := ApplicationExeName;
//end;

procedure TServerManagerService.SimulateOneError;
begin
  TSQLRestServerDBEx(ServiceContext.Request.Server).SimulateOneError := true;
end;

procedure TServerManagerService.SimulateOneException;
begin
  TSQLRestServerDBEx(ServiceContext.Request.Server).SimulateOneException := true;
end;

function PGDatabaseExists (Settings: TServerSettings): boolean; //(const Server, Port, Username, Password, DatabaseName: string): Boolean;
var
  ZConnection: TZConnection;
  ZQuery: TZQuery;
begin
//  Result := False;
  ZConnection := TZConnection.Create(nil);
  try
    ZConnection.Protocol := 'postgresql';
    ZConnection.HostName := Settings.ServerName.AsString;
//    ZConnection.Port := 5432;
    ZConnection.Database := 'postgres'; // Connect to the default 'postgres' database
    ZConnection.User := settings.Username.AsString;
    ZConnection.Password := settings.Password.AsString;
    ZConnection.LibraryLocation := Settings.LibraryLocation;
    ZConnection.Connect;

    ZQuery := TZQuery.Create(nil);
    try
      ZQuery.Connection := ZConnection;
      ZQuery.SQL.Text := 'SELECT 1 FROM pg_database WHERE datname = :DatabaseName';
      ZQuery.Params.ParamByName('DatabaseName').AsString := settings.DBPath.AsString;
      ZQuery.Open;
      Result := not ZQuery.IsEmpty; // If query returns a row, the database exists
    finally
      ZQuery.Free;
    end;
  finally
    ZConnection.Disconnect;
    ZConnection.Free;
  end;
end;

function TServerSettings.DatabaseExists: boolean;
begin
  case DBEngine of
    dSQLite     : result := FileExists (DBPath.AsString);
    dPostgreSQL : result := PGDatabaseExists(self);
    else          result := false;
  end;
end;

procedure PGCreateDatabase(Settings: TServerSettings); //(const Server, Port, Username, Password, DatabaseName: string);
var
  ZConnection: TZConnection;
  ZQuery: TZQuery;
begin
  ZConnection := TZConnection.Create(nil);
  try
    ZConnection.Protocol := 'postgresql';
    ZConnection.HostName := Settings.ServerName.AsString;
//    ZConnection.Port := 5432;
    ZConnection.Database := 'postgres'; // Connect to the default 'postgres' database
    ZConnection.User := Settings.Username.AsString;
    ZConnection.Password := Settings.Password.AsString;
    ZConnection.LibraryLocation := Settings.LibraryLocation;
    ZConnection.Connect;

    ZQuery := TZQuery.Create(nil);
    try
      ZQuery.Connection := ZConnection;
      ZQuery.SQL.Text := 'CREATE DATABASE "' + Settings.DBPath.AsString + '"';
      ZQuery.SQL.Add('TEMPLATE template0');
      ZQuery.SQL.Add('LC_COLLATE = ''sr-Latn-RS-x-icu'' ');
      ZQuery.SQL.Add('LC_CTYPE = ''sr-Latn-RS-x-icu'' ');
      ZQuery.ExecSQL;
    finally
      ZQuery.Free;
    end;
  finally
    ZConnection.Disconnect;
    ZConnection.Free;
  end;
end;

procedure TServerSettings.CreateDatabase;
begin
  case DBEngine of
    dSQLite     : ; // mormot will create it
    dPostgreSQL : PGCreateDatabase(self);
  end;
end;

procedure TServerSettings.CreateDatabaseIfNeeded;
begin
  if not DatabaseExists then
    if HasConsole
      then
        begin
          writeln('');
          writeln('Database file does not exist. Expected path: ');
          writeln(DBPath.AsString);
          writeln('');
          writeln('Create new one? If yes, press [y] and [enter]');
          var yesno: string;
          Readln (yesno);
          if yesno = 'y'
            then begin
                   writeln('Creating database...');
                   CreateDatabase;
                   writeln('Database created, you can restore backup now if you want. Press any key to continue.');
                   Readln (yesno);
                 end
            else halt;
        end
      else
        begin
          TSQLLog.Add.Log(sllError,'Database file not found: ' + DBPath,[]);
        end;
end;

{ TServerRegistryInfo }

//function TMormotServiceRegistryInfo.RegistryKey: string;
//begin
//  result := FRegistryPath.AsString + '\' + ServiceName.AsString;
//end;

//procedure TMormotServiceInfo.SetCompanyName(const Value: RawUTF8);
//begin
//  FCompanyName := Value;
//  FRegistryPath := 'Software\' + Value;
//end;

constructor TMormotServiceInfo.Create (const AServiceName, AServiceDisplayName, ASettingsPath: string);
begin
  inherited Create;
  ServiceName.AsString := AServiceName;
  ServiceDisplayName.AsString := AServiceDisplayName;
  SettingsPath.AsString := ASettingsPath;
  ExePath.AsString := ApplicationExeName;
  LogPath.AsString := TSQLLog.Add.FileName;
end;

//procedure TMormotServiceInfo.Read;
//var
//  json: RawUTF8;
//  valid: boolean;
//begin
//  json.AsString := ReadFromRegistry(RegistryKey, 'ServerRegistryInfo');
//  JSONToObject(self, @json, valid);
//  if not valid then raise Exception.Create('Failed to load into TServerRegistryInfo');
//end;
//
//procedure TMormotServiceRegistryInfo.Write;
//begin
//  ADLSWriteToRegistry(RegistryKey, 'ServerRegistryInfo', ObjectToJSON (self).AsString);
//end;

constructor TMormotServiceInfo.CreateFromFile (AFileName: TFileName = '');
//var
//  json: RawUTF8;
//  valid: boolean;
begin
  if AFileName = ''
    then AFileName := TPath.Combine (GetCurrentDir, 'ServiceInfo.json');
  inherited Create;

  if not FileExists (AFileName) then exit;

  if not JSONFileToObject(AFileName, self, TMormotServiceInfo)
    then raise Exception.Create('Failed to load JSON file into TMormotServiceInfo');
//  json.AsString := TFile.ReadAllText(AFileName);
//  JSONToObject(self, @json, valid);
//
//
//  if not valid then raise Exception.Create('Failed to load into TMormotServiceInfo');
  FLoaded := true;
end;

procedure TMormotServiceInfo.Write(AFileName: TFileName = '');
begin
  if AFileName = ''
    then AFileName := TPath.Combine (ApplicationExeFolder, 'ServiceInfo.json');

  TFile.WriteAllText(AFileName, ObjectToJSON(self, [woHumanReadable]).AsString);
end;

procedure PGBackupDatabase(const PGDumpPath, HostName, UserName, Password, DBName, BackupFilePath: string);
var
  fn, Command: string;
begin
  // Form the pg_dump command
  Command := '@echo off'#13#10
             +format ('SET PGPASSWORD=%s'#13#10, [Password])
             +format('"%s" -h %s -U %s -v --create -d "%s" %s', [PGDumpPath, HostName, UserName, DBName, BackupFilePath])
             +#13#10
             +'echo Database restoration completed.'#13#10
             +'pause';

  // Execute the command
//  ShellExecute(0, 'open', 'cmd.exe', PChar('/C ' + Command), nil, SW_HIDE);
  fn := TPath.Combine (ApplicationExeFolder, 'restore.bat');
  TFile.WriteAllText(fn, Command);
  ShellExecute(0, 'open', PChar(fn), nil, nil, SW_SHOW);
end;

procedure PGRestoreDatabase(const PGRestorePath, HostName, UserName, Password, BackupFilePath: string; Create: boolean = false);
// NB: DB name is not needed because it is part of the backup, stupid...
//-D tells it to connect to the default DB just for purpose of creating the DB...
// NB2 creating doesnt work because the custom db init code fails for some reason...
// use restore on existing but blank DB instead
var
  BatchFileContent: TStringList;
  fn: string;
begin
  fn := TPath.Combine (ApplicationExeFolder, 'restore.bat');
  BatchFileContent := TStringList.Create;
  try
    // Add the password line
    BatchFileContent.Add('@echo off');
    BatchFileContent.Add(Format('set PGPASSWORD=%s', [Password]));
    BatchFileContent.Add('');
    var sCreate: string := '';
    if Create then sCreate := '--create';
    // -- create will fail if DB exists, which is what we want, for safety
    BatchFileContent.Add(format ('"%s" -h %s -U %s -d postgres -v %s "%s"', [PGRestorePath, HostName, UserName, sCreate, BackupFilePath]));
    BatchFileContent.Add('');
    BatchFileContent.Add('echo Database restoration completed.');
    BatchFileContent.Add('pause');

    // Save to the batch file
    BatchFileContent.SaveToFile(fn);
  finally
    BatchFileContent.Free;
  end;

  ShellExecute(0, 'open', PChar(fn), nil, nil, SW_SHOW);
end;

procedure DumpRows (Rows: ISQLDBRows);
var
  RowCount, iCol: integer;
  Line: string;
begin
  if not HasConsole then exit;

  WriteLn ('');
  Line := '';
  for iCol := 0 to Rows.ColumnCount - 1 do
      Line := Line + Rows.ColumnName(iCol).AsString + ' | ';
  WriteLn (Line);
  WriteLn ('-------------------------------');
  RowCount := 0;
  while Rows.Step do
    begin
      Line := '';
      for iCol := 0 to Rows.ColumnCount - 1 do
        Line := Line + Rows.ColumnString(iCol) + ' | ';
      WriteLn (Line);
      inc (RowCount);
    end;
  WriteLn ('------------------------------- ' + RowCount.ToString + ' rows');
  WriteLn ('');
end;

procedure LogServerVersion;
begin
  TSQLLog.Add.Log(TSynLogInfo.sllCustom1, 'Server version: ' + GetBuildInfoAsString);
end;

procedure LogPrivileges;
begin
  var PL := GetPrivilegeLevel;
  case PL of
      0: TSQLLog.Add.Log(TSynLogInfo.sllCustom1, 'Program is not running with elevated privileges.');
      1: TSQLLog.Add.Log(TSynLogInfo.sllCustom1, 'Program is running with elevated privileges.');
      2: TSQLLog.Add.Log(TSynLogInfo.sllCustom1, 'Program is running as a system service.');
    end;
end;

procedure LogFormats;
begin
  TSQLLog.Add.Log(TSynLogInfo.sllCustom1, 'Formats:   ' +
                                          CurrToStrF (123456.789, ffNumber, 2) + '   ' +
                                          DateToStr (now) + '    ' +
                                          TimeToStr (now));
end;

initialization

  TInterfaceFactory.RegisterInterfaces ([TypeInfo(IServerManagerService)]);
  LOG_CONSOLE_COLORS[sllCommonInfo] := ccLightGreen;
  LOG_LEVEL_TEXT[sllCommonInfo] := ' c.info';

  LOG_CONSOLE_COLORS[sllCommonWarning] := ccYellow;
  LOG_LEVEL_TEXT[sllCommonWarning] := ' c.warn';

  LOG_CONSOLE_COLORS[sllCommonError] := ccRed;
  LOG_LEVEL_TEXT[sllCommonError] := ' c.ERR';

end.

