﻿unit ObservableController;

interface

uses System.Generics.Collections, SysUtils, System.Contnrs,
     mormot, SynCommons, SynTable, RTTI, TypInfo, Classes, SyncObjs, FMX.Forms, FMX.Types,
     MormotTypes, mormotMods, MormotAttributes, sqlRecords, Common.Utils, Validation, FMX.Common.Utils,
     UI.StateManagement, PersistentUserSettings, Common.Interfaces, Translator2Classes,
     FMX.ImgList;

const
  ecPreviousTaskFailed = -1;
  ecException          = -2;
  ecCommunicationError = -3;

type

  {$SCOPEDENUMS ON}

  TServerState = (AfterLogin, BeforeSaveSettings, BeforeLogout, AfterLogout, CriticalError, BeforeControllerDestroy);

  IServerObserver = interface;

  TObservableController = class;
  TTaskID = cardinal;

  TObservableTask = class // name is ok because the task is kinda observable, through the controller notifications
  public
    type TState = (Created, Executing, Finished, Destroying);
  protected
    FQueueCount: integer;
    FController: TObservableController;
    FState: TObservableTask.TState;
    FID: TTaskID;
    Client: TSQLHttpClientWebsocketsEx;
    procedure NotifyState;
    procedure ExecuteInMainThread(AMethod: TThreadMethod);
//    function GetClient: TSQLHttpClientWebsocketsEx;
    procedure DoExecute; virtual;
    procedure DoNotifyController; virtual;
    procedure SetState (AState: TObservableTask.TState);
//    property Client: TSQLHttpClientWebsocketsEx read GetClient;
  public
    Initiator: IServerObserver;
    QueryParameters: TQueryParameters;
    Operation: TCRUD;
    Outcome: TOutcome;
    function Success: boolean;
    constructor Create (AController: TObservableController; AOperation: TCRUD; const AParameters: TQueryParameters; AInitiator: IServerObserver = nil); virtual;
    destructor Destroy; override;
    procedure Execute;
    procedure NotifyController;
    procedure ShowError;
    function Subject: string; virtual; // e.g. TSqlRecordClass name
    function VerboseDescription: string;
//    procedure DetachInitiator;

    property State: TObservableTask.TState read FState;
    property ID: TTaskID read FID;
    property Controller: TObservableController read FController;
  end;

  TTaskThread = class (TThread)
  private
    procedure NotifyBusy;
    procedure NotifyIdle;
  protected
    CompletedTasks: TThreadList<TObservableTask>;
    FController: TObservableController;
    MainThreadNotifications: boolean;
    procedure Notify;
  public
    Primary: boolean;
    constructor Create (AController: TObservableController);
    destructor Destroy; override;

    procedure Execute; override;
  end;

  TSqlRecordTask = class (TObservableTask)
  protected
    FSqlRecordClass: TSQLRecordClass;
    procedure DoExecute; override;
  public
    function Subject: string; override;
    constructor Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass; AOperation: TCRUD; const AParameters: TQueryParameters; AInitiator: IServerObserver = nil); reintroduce; virtual;
    property SqlRecordClass: TSQLRecordClass read FSqlRecordClass;
  end;

  TSingleRecordTask = class (TSqlRecordTask)
  private
    FFields: RawUTF8;
  protected
    FSqlRecord: TSqlRecord;
    procedure SetSqlRecord(const Value: TSqlRecord);
  public
    destructor Destroy; override;
    property SqlRecord: TSqlRecord read FSqlRecord write SetSqlRecord;
    property Fields: RawUTF8 read FFields write FFields;
  end;

  THistoryTask = class (TObservableTask)
  protected
    procedure DoExecute; override;
    procedure DoNotifyController; override;
  public
    TableName, FieldName, CurrentValue: string;
    Values: TArray<RawUTF8>;
    constructor Create(AController: TObservableController; const ATableName, AFieldName, ACurrentValue: string; Initiator: IServerObserver = nil); reintroduce;
  end;

  TDBMaintenanceKind = (RedirectFK, RedirectFKAndDelete, GetFKInfo);

  TDBMaintenanceTask = class (TObservableTask)
  protected
    procedure DoExecute; override;
  public
    SqlRecordClass: TSqlRecordClass;
    Kind: TDBMaintenanceKind;
    ValidID, InvalidID : TID;
    constructor Create(AController: TObservableController; AKind: TDBMaintenanceKind; Initiator: IServerObserver = nil); reintroduce;
  end;

  IServerObserver = interface (INamed)
    ['{3F49566E-D921-40D0-BA0B-7CD90C8EE8E9}']
  end;

  IMultipleTaskInitiator = interface (IServerObserver)
    ['{658576C0-804E-4C2F-BFD0-88FC81235B94}']
  end;

  // interface to optionally observe foreign tasks, function allows the observer to turn on/off this behaviour
  IForeignTaskObserver = interface
  ['{5806C17B-4A73-4FCE-A7B4-A8904178DD75}']
    function GetEnabled: boolean;
    procedure SetEnabled(const Value: boolean);
    property Enabled : boolean read GetEnabled write SetEnabled;
  end;

  TForeignTaskObserver = class (TInterfacedObject, IForeignTaskObserver)
  private
    FEnabled: boolean;
    function GetEnabled: boolean;
    procedure SetEnabled(const Value: boolean);
  public
    constructor Create (AEnabled: boolean);
    property Enabled : boolean read GetEnabled write SetEnabled;
  end;

  TRecordsTask = class (TSqlRecordTask)
  protected
    procedure DoNotifyController; override;
    procedure DoExecute; override;
  public
    SqlRecords: TSqlRecords;
    constructor Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass; AOperation: TCRUD;
                        const AParameters: TQueryParameters; AInitiator: IServerObserver = nil); override;
    destructor Destroy; override;
  end;

  TGenObservableSingleRecordTask = class (TSingleRecordTask)
  protected
    procedure DoExecute; override;
    procedure DoNotifyController; override;
  public
    constructor Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass; AOperation: TCRUD;
                        const AParameters: TQueryParameters; AInitiator: IServerObserver = nil); reintroduce; virtual;
  end;

  TRecordCreateOrUpdateWithDetailsTask = class (TGenObservableSingleRecordTask)
  protected
    procedure DoExecute; override;
  public
    RecordWithDetails: TRecordWithDetails;
    constructor Create (AController: TObservableController; ARecordWithDetails: TRecordWithDetails; AInitiator: IServerObserver = nil); reintroduce; virtual;
  end;

  TObservableLookupTask = class (TSqlRecordTask)
  protected
    procedure DoExecute; override;
    procedure DoNotifyController; override;
  public
    OwnsLookup: boolean;
    Lookup: TLookup;
    IDs: TIDDynArray;
    Values: TRawUTF8DynArray;
    constructor Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass;
                        const AParameters: TQueryParameters; AInitiator: IServerObserver = nil); reintroduce;
    destructor Destroy; override;
  end;

  TCustomLoginTask = class (TObservableTask)
  private
    procedure DoLogin;
  protected
    //varPersistentUserSettings: variant;
    procedure DoExecute; override;
    procedure DoAfterLogin; virtual; abstract;
  public
    Username, Password: string;
    HashedPassword: boolean;
    MandatoryServices: TMormotServices;
  end;

  TCustomRequestTask = class (TObservableTask)
  private
  protected
    procedure DoExecute; override;
  public
    Command, RequestContent, ResponseContent: RawUTF8;
    RequestParameters, ResponseParameters: variant;
    constructor Create (AController: TObservableController; const ACommand: string; const AParameters: variant;
                        const ARequestContent: string; AInitiator: IServerObserver = nil); reintroduce;
  end;


  TObservableController = class (TNonARCInterfacedObject, IFreeNotification)
  private
    function GetLoggedIn: boolean;
  protected
    ActiveThreads: integer;
    FModelClass: TSqlModelClass;
    slObserverNames: TStringList;
    Timer: TTimer;
    Observers : TList<IServerObserver>;
    PrimaryThread : TTaskThread;
    ActiveInitiators : TList<IServerObserver>;
    procedure ProcessTaskCompletion (Task: TObservableTask); virtual;
    procedure TaskFinished (Task: TObservableTask);
    function CreateClient: TSQLHttpClientWebsocketsEx;
    procedure CreateClients;
    procedure FreeClients;
    procedure Notify (Task: TObservableTask); virtual;
    procedure NotifyTaskState (Task: TObservableTask); virtual;
    procedure NotifyHistory (Task: THistoryTask); virtual;
    procedure NotifyObserveRecords (Task: TRecordsTask);
    procedure NotifyObserveRecord (Task: TGenObservableSingleRecordTask);
    procedure NotifyLookup (Task: TObservableLookupTask);
    procedure NotifyState(State: TServerState; var Outcome: TOutcome);
    procedure NotifyObserveUnlock(Task: TGenObservableSingleRecordTask);
    procedure NotifyBusy;
    procedure NotifyIdle;
    function DefaultPassword: string; virtual;

    function ObservesTask (intfObserver: IServerObserver; Task: TObservableTask): boolean;
    procedure OnTimer (Sender: TObject);

    procedure FreeNotification(AObject: TObject);
    function CreateLoginTask: TCustomLoginTask; virtual; abstract;
    procedure GetLookupsFor (const SqlRecordClass: TSQLRecordClass; MasterTableName: RawUTF8 = '');
    procedure AddTask (Task: TObservableTask);
  public
    Password: string;// password of last successful login
    LastTaskFinishedTime : TDateTime;
    Lookups: TLookups;
    ltReplacable, ltAlternatives: TArray <TSQLRecordClass>;

    apsTaskRunning: TAppState;
    //Client: TSQLHttpClientWebsocketsEx;

    Clients: TThreadList<TSQLHttpClientWebsocketsEx>;

    Address, Port, WebSocketsEncryptionKey, Language: string;
    TaskThreads: TList<TTaskThread>;
    MainThreadNotifications: boolean;
    LoggedUser : TSqlAuthUserEx;
    Model: TSqlModelEx;
    MandatoryServices, AdminServices: TMormotServices; // for login
    CriticalErrorTask: TObservableTask;
    LoggingOut: boolean; // if false we are logging out to close the program, if true we are actually logging out and want to login again
    LogOutTaskActive: boolean;
    PersistentUserSettings: TPersistentUserSettings;
    PingInterval: integer;
    dicImageIndexes: TDictionary <string, integer>;
    ImageList: TImageList;
    DummyConnection: boolean;
    ReceiveTimeout, ThreadCount: integer;
    Tasks: TThreadList<TObservableTask>;
    constructor Create (const AAddress, APort, AWebSocketsEncryptionKey, ALanguage: string;
                        AModelClass: TSqlModelClass; AThreadCount: integer; AMainThreadNotifications: boolean = true); virtual;
    destructor Destroy; override;

    procedure Subscribe   (Observer: IServerObserver);
    procedure Unsubscribe (Observer: IServerObserver);

    function AcquireClient: TSQLHttpClientWebsocketsEx;
    function TryAcquireClient: TSQLHttpClientWebsocketsEx;
    procedure CheckPasswordValidity (const APassword: string); virtual;
    procedure ReleaseClient (AClient: TSQLHttpClientWebsocketsEx);

    procedure DummyLogin;
    procedure Login(const AUserName, APassword: string; aHashedPassword: boolean = false); virtual;
    procedure Logout (ASaveSettings: boolean); virtual;
    procedure ResetPassword (const LogonName: RawUTF8; Initiator: IServerObserver); virtual; abstract;
    procedure SetMyData(const OldPassword: RawUTF8; User: TSqlRecord; Initiator: IServerObserver); virtual; abstract;
    procedure DBStats(Initiator: IServerObserver); virtual; abstract;

    procedure Ping;
    procedure GetPublicFile (const FileName: string);
    procedure SaveUserSettings;

    function ImmediateRecordCreate(const SqlRecord: TSqlRecord): TOutcome;

    function GetFieldDisplayValue(const SqlRecord: TSQLRecord; fi: TFieldInfo; IntLookups: TLookups): variant; overload;
    function GetFieldDisplayValue(const SqlRecord: TSQLRecord; const FieldName: string; IntLookups: TLookups): variant; overload;
    procedure FetchHistory (ASqlRecordClass: TSqlRecordClass; const AFieldName, ACurrentValue: string; const QueryParameters: TQueryParameters; Initiator: IServerObserver);

    procedure GetLookup (SqlRecordClass: TSqlRecordClass; const QueryParameters: TQueryParameters; Initiator: IServerObserver = nil);

    function RecordCreate  (const SqlRecord: TSQLRecord; DetailIDs: TIDDictionary = nil; Initiator: IServerObserver = nil): TTaskID; overload;
    function RecordsCreate (const Records: TArray<TSQLRecord>; Fields: string = ''; Initiator: IServerObserver = nil): TTaskID;

    function RecordCreateOrUpdateWithDetails (const ARecordWithDetails: TRecordWithDetails; AOperation: TCRUD;
                                              Initiator: IServerObserver = nil): TTaskID;

//    procedure RecordCreate<T: TSqlRecord> (const SqlRecord: T; DetailIDs: TIDDictionary = nil; Initiator: IServerObserver = nil); overload;
    function RecordCreateOrUpdate (const SqlRecord: TSqlRecord; DetailIDs: TIDDictionary = nil; Initiator: IServerObserver = nil): TTaskID;

    function RecordRead (T: TSqlRecordClass; ID: TID; const IDSourceFieldName: RawUTF8; Initiator: IServerObserver = nil): TTaskID; overload;
    function RecordRead (T: TSqlRecordClass; QueryParameters: TQueryParameters; Initiator: IServerObserver = nil): TTaskID; overload;

    function RecordReadLock (T: TSqlRecordClass; ID: TID; Initiator: IServerObserver = nil): TTaskID;

    function RecordUnlock (const SqlRecord: TSQLRecord; Initiator: IServerObserver = nil): TTaskID; overload;
    function RecordUnlock (T: TSqlRecordClass; const ID: TID; Initiator: IServerObserver = nil): TTaskID; overload;

    function RecordUpdate (const SqlRecord: TSqlRecord; Fields: string = ''; DetailIDs: TIDDictionary = nil; Initiator: IServerObserver = nil): TTaskID; overload;
    function RecordUpdate (const SqlRecord: TSqlRecord; Initiator: IServerObserver = nil): TTaskID; overload;

    function RecordDelete (const SqlRecord: TSqlRecord; Initiator: IServerObserver = nil): TTaskID; overload;  //NB this does NOT take ownership of sqlRecord
    function RecordDelete (SqlRecordClass: TSqlRecordClass; ID: TID; Initiator: IServerObserver = nil): TTaskID; overload;

    function RecordsRead (SqlRecordClass: TSqlRecordClass; const QueryParameters: TQueryParameters; Initiator: IServerObserver = nil): TTaskID; overload;
    function RecordsRead (TableName: RawUTF8; const QueryParameters: TQueryParameters; Initiator: IServerObserver = nil): TTaskID; overload;

    procedure RedirectFKs (SqlRecordClass: TSqlRecordClass; ValidID, InvalidID: TID; DeleteInvalid: boolean; Initiator: IServerObserver);
    procedure GetFKInfo (SqlRecordClass: TSqlRecordClass; ID: TID; Initiator: IServerObserver);

    procedure GetReportFile (const FileName: string; AInitiator: IServerObserver = nil);
    procedure SetReportFile (const FileName: string; Stream: TStream; AInitiator: IServerObserver = nil);

    function GetTableHistory (ASqlRecordClass: TSQLRecordClass; QueryParameters: TQueryParameters; AInitiator: IServerObserver = nil): TTaskID;

    function CustomRequest (const Command: string; Parameters: variant; const Content: string; Initiator: IServerObserver = nil): TTaskID; overload;
    function CustomRequest (const Command: string; Parameters: variant; Initiator: IServerObserver = nil): TTaskID; overload;

    property LoggedIn: boolean read GetLoggedIn;
  end;

  IServerStateObserver = interface (IServerObserver)
  ['{360B7DF4-5CAD-499B-B651-7B461A51A4AA}']
    procedure ObserveState (State: TServerState; var Outcome: TOutcome);
  end;

  ITaskStateObserver = interface (IServerObserver)
  ['{AC1FA322-5A51-48BF-B426-6CC615B66011}']
    procedure ObserveTaskState (Task: TObservableTask);
  end;

  IUnlockObserver = interface (IServerObserver)
  ['{9949275E-B11F-468B-8652-7DC582499415}']
    procedure ObserveUnlock (Task: TSingleRecordTask);
  end;

  IRecordObserver = interface (IServerObserver)
  ['{BD560E33-86DB-428B-A9CD-2731CF854DF2}']
    procedure ObserveRecord (Task: TSingleRecordTask);
  end;

  IControllerTaskObserver = interface (IServerObserver)
  ['{32DAD301-ACD2-4E02-A683-CAE714FE7385}']
    procedure ObserveTask (Task: TObservableTask);
  end;

  IHistoryObserver = interface (IServerObserver)
  ['{FAA6AF22-DA62-4376-AB60-47E66328C531}']
    procedure ObserveHistory (Task: THistoryTask);
  end;

  IRecordsObserver = interface (IServerObserver)
  ['{AB294115-81CB-4AA6-8774-111E768E69FE}']
    procedure ObserveRecords(Task: TRecordsTask);
  end;

  IActivityObserver = interface (IServerObserver)
  ['{D3F3C84D-A876-4F3D-AADA-EF6A8C4A9FCC}']
    procedure ObserveActivity (const Busy: boolean);
  end;

  function DefaultParameters: TQueryParameters;

var
  NextTaskID: cardinal = 1;

implementation

uses
  Controller.Tasks, Tasks.Form;

function DefaultParameters: TQueryParameters;
begin
  result := default (TQueryParameters);
end;

{ TObservableController }

procedure TObservableController.AddTask(Task: TObservableTask);
begin
  if LogOutTaskActive then
     {$IFDEF DEBUG}
     raise Exception.Create('Adding task while logging out?');
     {$ELSE}
     Task.Free;
     {$ENDIF}

  Tasks.Add(Task);
end;

procedure TObservableController.CheckPasswordValidity(const APassword: string);
begin
  if length (APassword) < 6 then ShowErrorAndAbort('Dužina lozinke mora biti bar 6 karaktera.');
  if pos (DefaultPassword, APassword) > 0
    then ShowErrorAndAbort('Lozinka ne može sadžati početnu tj. resetovanu lozinku.');
end;

constructor TObservableController.Create(const AAddress, APort, AWebSocketsEncryptionKey, ALanguage: string;
            AModelClass: TSqlModelClass; AThreadCount: integer; AMainThreadNotifications: boolean = true);
var
  i: integer;
begin
  inherited Create;
  ThreadCount := AThreadCount;
  Tasks := TThreadList<TObservableTask>.Create;
  Clients := TThreadList<TSQLHttpClientWebsocketsEx>.Create;
  TaskThreads := TList<TTaskThread>.Create;
  dicImageIndexes := TDictionary <string, integer>.Create;
  PingInterval := 20;
  slObserverNames := TStringList.Create;
  Language := ALanguage;
  FModelClass := AModelClass;
  MainThreadNotifications := AMainThreadNotifications;
  ActiveThreads := 0;
  apsTaskRunning := TAppState.CreateAsSingle ('TaskRunning');
//  semThreads := TSemaphore.Create (nil, AThreadCount, AThreadCount, 'ControllerThreads');

  PrimaryThread := TTaskThread.Create (self);
  PrimaryThread.Primary := true;
  TaskThreads.Add (PrimaryThread);
  for i := 2 to ThreadCount do
      TaskThreads.Add (TTaskThread.Create (self));

//  TaskThread.FreeOnTerminate := true;
  Address := AAddress;
  Port := APort;
  WebSocketsEncryptionKey := AWebSocketsEncryptionKey;

  Observers := TList<IServerObserver>.Create;
  ActiveInitiators := TList<IServerObserver>.Create;
  Lookups := TLookups.Create(ltReplacable, ltAlternatives);
  //UserRecordClass := TSQLAuthUser;
  //AuthGroupClass := TSQLAuthGroup;

//  CreateClient; // create client just so that we also have a model available, they will be re-created when logging in

  Timer := TTimer.Create (nil);
  Timer.OnTimer := OnTimer;

  if FindCmdLineSwitch('tasks') then
     begin
       TfrmTasks.CreateInstance;
       frmTasks.SetController (self);
       frmTasks.Show;
     end;
end;

function TObservableController.DefaultPassword: string;
begin
  result := '12345';
end;

destructor TObservableController.Destroy;
var
  thread: TTaskThread;
begin
  Timer.Free;
  for thread in TaskThreads do
    thread.Terminate;

  while ActiveThreads > 0 {not semThreads. TryEnter} do
    begin
      while CheckSynchronize do ;
      sleep (2);
      //Application.ProcessMessages;
    end;
//  csThread.Free;
  var Outcome: TOutcome;
  Outcome.SetSuccess;

  try
    NotifyState(TServerState.BeforeControllerDestroy, Outcome);
  except

  end;

  FreeClients;

  FreeAndNil (Lookups);
  FreeAndNil (Observers);
  FreeAndNil (ActiveInitiators);
  FreeAndNil (LoggedUser);
  FreeAndNil (slObserverNames);
  FreeAndNil (dicImageIndexes);
  FreeAndNil (Clients);
  FreeAndNil (TaskThreads);
  FreeAndNil (Tasks);
  FreeAndNil (apsTaskRunning);
  inherited;
end;

procedure TObservableController.DummyLogin;
begin
  DummyConnection := true;
  CreateClients;
end;

function TObservableController.CreateClient: TSQLHttpClientWebsocketsEx;
begin
  Model := FModelClass.Create (false, Language);
{TODO -oOwner -cGeneral : ActionItem}
  //Model := FModelClass.Create (false, '' Language);
  result := TSQLHttpClientWebsocketsEx.Create (Model, Address, Port, WebSocketsEncryptionKey);
  result.DummyConnection := DummyConnection;
  result.ReceiveTimeout := ReceiveTimeout;
end;

procedure TObservableController.CreateClients;
var
  i: integer;
begin
  FreeClients;

  var lst := Clients.LockList;
  try
    for i := 1 to ThreadCount do
      lst.Add (CreateClient);
  finally
    Clients.UnlockList;
  end;
end;

function TObservableController.CustomRequest (const Command: string; Parameters: variant; Initiator: IServerObserver = nil): TTaskID;
begin
  result := CustomRequest(Command, Parameters, '', Initiator);
end;

function TObservableController.CustomRequest (const Command: string; Parameters: variant; const Content: string;
         Initiator: IServerObserver = nil): TTaskID;
begin
  var task := TCustomRequestTask.Create (self, Command, Parameters, Content, Initiator);
  result := task.ID;
  AddTask (task);
end;

procedure TObservableController.FreeClients;
begin
  var lst := Clients.LockList;
  try
    while lst.Count > 0 do
      begin
        lst[0].Free;
        lst.Delete(0);
      end;
  finally
    Clients.UnlockList;
  end;
end;

procedure TObservableController.Subscribe(Observer: IServerObserver);
var
  ifn: IFreeNotificationBehavior;
begin
  if Observer = nil then raise Exception.Create('Observer is nil!');
  if Observers.Contains (Observer) then raise Exception.Create('Observer ' + Observer.Name + ' already subscribed!');

  Observers.Add (Observer);
  slObserverNames.Add (Observer.Name);

  if Supports (Observer, IFreeNotificationBehavior, ifn)
     then ifn.AddFreeNotify(self);
end;

procedure TObservableController.Unsubscribe(Observer: IServerObserver);
var
  ifn: IFreeNotificationBehavior;
  index: integer;
begin
  if self = nil then exit; // allow unsubscribe when controller was destroyed and cleaned up, I guess?

  try
    index := Observers.IndexOf(Observer);
    if index > -1 then
      begin
        slObserverNames.Delete (Observers.IndexOf(Observer));
        Observers.Remove (Observer);
      end;

    if Supports (Observer, IFreeNotificationBehavior, ifn)
       then ifn.RemoveFreeNotify(self);
  except
    begin
      ShowErrorDialogue(Observer.Name);
      raise;
    end;
  end;
end;

procedure TObservableController.TaskFinished(Task: TObservableTask);
begin
  ProcessTaskCompletion (Task);
  Notify (Task);
  LastTaskFinishedTime := now;
end;

function TObservableController.GetFieldDisplayValue(const SqlRecord: TSQLRecord; fi: TFieldInfo; IntLookups: TLookups): variant;
var
  s: string;
begin
  if fi = nil then
    begin
      varclear (result);
      exit;
    end;

  if fi.Name = 'ID'
     then exit (SqlRecord.IDValue);

  if fi.MormotFieldInfo.IsNotAssigned or (fi.Name = 'ProfitPerItem')
    then exit (GetPropValue(SqlRecord, fi.Name));

  var P := fi.MormotFieldInfo;
  case P.SQLFieldType of
    sftSessionUserID: begin
                        P.GetVariant(SqlRecord, result);
                        if Lookups.ResolveAsString (Model.AuthUserClass{ UserRecordClass}, result, s)
                          then result := s
                          else result := '?';
                      end;
    TSQLFieldType.sftTID, TSQLFieldType.sftID:
      begin
        var cl := TSQLPropInfoRTTITID (P).RecordClass;
        if (cl = nil) and (TSQLPropInfoRTTITID (P).PropType.Kind = SynCommons.tkClass)
          then if TSQLPropInfoRTTITID (P).PropType.Name = 'TSQLAuthGroup'
               then cl := Model.AuthGroupClass
               else cl := {Client.}Model.TableExact [TSQLPropInfoRTTITID (P).PropType.Name];
        P.GetVariant(SqlRecord, result);
        if cl = nil
           then // leave the ID
           else if ( (IntLookups <> nil) and IntLookups.ResolveAsString (cl, result, s) )
                   or
                   ( (Lookups <> nil) and Lookups.ResolveAsString (cl, result, s) )
                   then result := s
                   else
                     if result = 0
                       then varClear (result)// if null just clear, dont lookup
                       else if Lookups.ResolveAsString (cl, result, s)
                         then result := s;
      end;
    else result := fi.GetDisplayString(SqlRecord);
  end
end;

procedure TObservableController.FetchHistory(ASqlRecordClass: TSqlRecordClass; const AFieldName, ACurrentValue: string; const QueryParameters: TQueryParameters; Initiator: IServerObserver);
begin
  inherited;
  var task := THistoryTask.Create (self, ASqlRecordClass.SQLTableName.AsString, AFieldName, ACurrentValue, Initiator);
  task.QueryParameters := QueryParameters;
  AddTask (task);
end;

procedure TObservableController.FreeNotification(AObject: TObject);
var
  intf: IServerObserver;
begin
  if supports (AObject, IServerObserver, intf)
    then Unsubscribe(intf);
end;

function TObservableController.RecordReadLock (T: TSqlRecordClass; ID: TID; Initiator: IServerObserver = nil): TTaskID;
begin
  var task := TGenObservableSingleRecordTask.Create (self, T, TCRUD.ReadLock, DefaultParameters, Initiator);
  task.SqlRecord := T.Create;
  task.QueryParameters.ID := ID;
  result := task.ID;
  AddTask (task);
end;

function TObservableController.ImmediateRecordCreate(const SqlRecord: TSqlRecord): TOutcome;
begin
  var Client := AcquireClient;
  try
    var intf := Client.Service<ICommonService>;
    var tn := SqlRecord.RecordClass.crudSQLTableName;
    var json := SqlRecord.GetJSONValues (true, SqlRecord.IDValue.IsNotNULL,
                SqlRecord.RecordProps.CopiableFieldsBits {SqlRecord.GetNonVoidFields}, []);
    result := intf.Add(tn, '', json, SqlRecord.DetailIDs);
    SqlRecord.Free;
  finally
    ReleaseClient(Client);
  end;
end;

function TObservableController.GetFieldDisplayValue(const SqlRecord: TSQLRecord; const FieldName: string; IntLookups: TLookups): variant;
var
  s: string;
  P: TSQLPropInfo;
begin
  if FieldName = 'ID'
     then exit (SqlRecord.IDValue)
     else P := SqlRecord.RecordProps.Fields.ByRawUTF8Name(StringToUTF8(FieldName));

  if P = nil then raise Exception.Create('Field ' + FieldName + ' not found in ' + SqlRecord.SQLTableName.AsString);

  result := SqlRecord.GetFieldVariantDisplayValue (FieldName);

  case P.SQLFieldType of
    sftSessionUserID: if Lookups.ResolveAsString (Model.AuthUserClass{ UserRecordClass}, result, s) then result := s;
    TSQLFieldType.sftTID, TSQLFieldType.sftID:
      begin
        var cl := TSQLPropInfoRTTITID (P).RecordClass;
        if (cl = nil) and (TSQLPropInfoRTTITID (P).PropType.Kind = SynCommons.tkClass)
          then if TSQLPropInfoRTTITID (P).PropType.Name = 'TSQLAuthGroup'
               then cl := Model.AuthGroupClass
               else cl := {Client.}Model.TableExact [TSQLPropInfoRTTITID (P).PropType.Name];
        if cl = nil
           then // leave the ID
           else if ( (IntLookups <> nil) and IntLookups.ResolveAsString (cl, result, s) )
                   or
                   ( (Lookups <> nil) and Lookups.ResolveAsString (cl, result, s) )
                   then result := s
                   else
                     if result = 0
                       then varClear (result)// if null just clear, dont lookup
                       else if Lookups.ResolveAsString (cl, result, s)
                         then result := s;
      end;
  end
end;

procedure TObservableController.Notify(Task: TObservableTask);
var
  i : integer;
  intf: IControllerTaskObserver;
begin
  if (Task.Initiator <> nil) and Supports (Task.Initiator, IControllerTaskObserver, intf)
    then intf.ObserveTask(Task);

  for i := Observers.Count - 1 downto 0 do
    if (Observers[i] <> Task.Initiator)
       and Supports (Observers[i], IControllerTaskObserver, intf)
       and ObservesTask(Observers[i], Task)
           then intf.ObserveTask(Task);

  if not Task.Success
     then if Task.Outcome.IsCriticalError
       then
         begin
           CriticalErrorTask := Task;
           NotifyState(TServerState.CriticalError, Task.Outcome);
           CriticalErrorTask := nil;
         end
       else Task.ShowError;
end;

procedure TObservableController.NotifyBusy;
var
  i : integer;
  intf: IActivityObserver;
begin
  apsTaskRunning.Activate;
  for i := Observers.Count - 1 downto 0 do
    if Supports (Observers[i], IActivityObserver, intf)
       then intf.ObserveActivity (true);
end;

procedure TObservableController.NotifyIdle;
var
  i : integer;
  intf: IActivityObserver;
begin
  apsTaskRunning.Deactivate;
  for i := Observers.Count - 1 downto 0 do
    if Supports (Observers[i], IActivityObserver, intf)
       then intf.ObserveActivity (false);
end;

function TObservableController.ObservesTask (intfObserver: IServerObserver; Task: TObservableTask): boolean;
var
  ifo: IForeignTaskObserver;
begin
  result := (intfObserver = Task.Initiator) or
            ( Supports (intfObserver, IForeignTaskObserver, ifo) and ifo.Enabled );
end;

procedure TObservableController.OnTimer(Sender: TObject);
begin
  if (LoggedUser <> nil) and (SecondsSince (LastTaskFinishedTime) > PingInterval)
     then Ping;
end;

procedure TObservableController.NotifyHistory(Task: THistoryTask);
var
  i : integer;
  intf: IHistoryObserver;
  intfObserver: IServerObserver;
begin
  for i := Observers.Count - 1 downto 0 do
    begin
      intfObserver := Observers[i];

      if Supports (intfObserver, IHistoryObserver, intf)
         and ObservesTask(intfObserver, Task)
            then intf.ObserveHistory (Task);
    end;
end;

procedure TObservableController.NotifyObserveUnlock(Task: TGenObservableSingleRecordTask);
var
  intfUO: IUnlockObserver;
begin
  for var i := Observers.Count - 1 downto 0 do
    if Supports (Observers[i], IUnlockObserver, intfUO)
       and ObservesTask (Observers[i], Task)
           then intfUO.ObserveUnlock(Task);
end;

procedure TObservableController.NotifyState(State: TServerState; var Outcome: TOutcome);
var
  i : integer;
  intf: IServerStateObserver;
begin
  i := Observers.Count - 1;
  try
    while i >= 0 do
      begin
        if Observers[i] = nil then
          if now = 0 then exit else continue;

        if Supports (Observers[i], IServerStateObserver, intf)
           then intf.ObserveState(State, Outcome);
        dec (i);
      end;
  except on e: exception do
    raise Exception.Create('TObservableController.NotifyState failed for observer ' + slObserverNames[i] + #13#13 + e.Message);
  end;
end;

procedure TObservableController.NotifyTaskState(Task: TObservableTask);
var
  i : integer;
  intf: ITaskStateObserver;
begin
  for i := Observers.Count - 1 downto 0 do
    if Supports (Observers[i], ITaskStateObserver, intf)
       then intf.ObserveTaskState(Task);
end;

procedure TObservableController.ProcessTaskCompletion(Task: TObservableTask);
begin
  if Task is TCustomLoginTask then
    begin
      if Task.Success
        then begin
               if LoggedUser = nil then //wtf
                  if now = 0 then exit;

               PersistentUserSettings.FromVariant (LoggedUser.Settings {TLoginTask(Task).varPersistentUserSettings});
               Password := TCustomLoginTask(Task).Password;
             end
        else PersistentUserSettings.Clear;
      NotifyState(TServerState.AfterLogin, Task.Outcome);
      if Task.Success
         then GetLookup(Model.AuthGroupClass, DefaultParameters);
    end;

  if Task is TLogoutTask then
    begin
      FreeClients;
      NotifyState(TServerState.AfterLogout, Task.Outcome);
      LoggingOut := false;
      LogOutTaskActive := false;
    end;
end;

procedure TObservableController.GetPublicFile(const FileName: string);
begin
  var task := TGetPublicFileTask.Create (self, TCRUD.Read, default (TQueryParameters));
  task.FileName := FileName;
  AddTask (task);
end;

procedure TObservableController.Ping;
begin
  var task := TPingTask.Create(self, TCRUD.Unknown, default (TQueryParameters), nil);
  AddTask (task);
end;

procedure TObservableController.Login(const AUserName, APassword: string; aHashedPassword: boolean);
begin
  if (apsTaskRunning <> nil) and apsTaskRunning.Active then
     ShowErrorDialogue('Login when busy?');

  FreeAndNil (LoggedUser);
  CreateClients;
  var task := CreateLoginTask;
  task.Username := AUserName;
  task.Password := APassword;
  task.HashedPassword := aHashedPassword;
  task.MandatoryServices := MandatoryServices;
  AddTask (task);
end;

procedure TObservableController.Logout (ASaveSettings: boolean);
begin
  if ASaveSettings and (LoggedUser <> nil)
     then SaveUserSettings;

  var Outcome := default(TOutcome);
  NotifyState(TServerState.BeforeLogout, Outcome);

  var task := TLogoutTask.Create (self, TCRUD.Unknown, DefaultParameters);
  AddTask (task);
  LogOutTaskActive := true;
end;

procedure TObservableController.SaveUserSettings;
begin
  var Outcome := default (TOutcome);
  NotifyState(TServerState.BeforeSaveSettings, Outcome);

  var task := TSetMySettingsTask.Create(self, PersistentUserSettings.ToVariant);
  AddTask (task);
end;

function TObservableController.RecordsCreate(const Records: TArray<TSQLRecord>; Fields: string; Initiator: IServerObserver): TTaskID;
begin
  var task := TRecordsCreateTask.Create (self, TCrud.Create, DefaultParameters, Initiator);
  task.Records := Records;
  task.Fields.AsString := Fields;
  result := task.ID;
  AddTask (task);
end;

function TObservableController.RecordsRead (TableName: RawUTF8; const QueryParameters: TQueryParameters; Initiator: IServerObserver = nil): TTaskID;
begin
  result := RecordsRead (Model.Table[TableName], QueryParameters, Initiator);
end;

function TObservableController.RecordsRead (SqlRecordClass: TSqlRecordClass; const QueryParameters: TQueryParameters;
                                             Initiator: IServerObserver = nil): TTaskID;
begin
  if not Model.Contains (SqlRecordClass) then
     raise Exception.Create(SqlRecordClass.SQLTableName.AsString + ' is not part of the model!');

  var task := TRecordsTask.Create (self, SqlRecordClass, TCRUD.Read, QueryParameters, Initiator);
  task.QueryParameters := QueryParameters;
  result := task.ID;
  AddTask (task);
end;

{ TObservableTask }

function TObservableTask.Subject: string;
begin
  result := '';
end;

constructor TObservableTask.Create(AController: TObservableController; AOperation: TCRUD; const AParameters: TQueryParameters; AInitiator: IServerObserver);
begin
  if AInitiator <> nil then
    begin
      {$IFDEF MILOS}
//      if not Supports (AInitiator, IMultipleTaskInitiator) and (AController.ActiveInitiators.IndexOf(AInitiator) > -1)
//         then raise Exception.Create('Initiator ' + AInitiator.Name + '  started 2nd task?');
      {$ENDIF}
      AController.ActiveInitiators.Add(AInitiator);
    end;
  inherited Create;
  FID := NextTaskID;
  inc (NextTaskID);
  FController := AController;
  Operation := AOperation;
  QueryParameters := AParameters;
  Initiator := AInitiator;
  SetState (TObservableTask.TState.Created);
end;

destructor TObservableTask.Destroy;
begin // called from main thread
  SetState (TObservableTask.TState.Destroying);
  if FQueueCount > 0 then
    {if now = 0 then }raise Exception.Create('Task.QueueCount > 0');

  inherited;
end;

//procedure TObservableTask.DetachInitiator;
//begin
//  Initiator := nil;
//end;

procedure TObservableTask.DoExecute;
begin

end;

procedure TObservableTask.DoNotifyController;
begin
  FController.TaskFinished(self);
end;

procedure TObservableTask.Execute;
begin
  try
    SetState (TObservableTask.TState.Executing);
    Client := FController.AcquireClient;
    try
      DoExecute;
    finally
      if Client <> nil then
        begin
          FController.ReleaseClient (Client);
          Client := nil;
        end;
    end;
    SetState (TObservableTask.TState.Finished);
  except on e: exception do
    Outcome.SetError(ecException, e.Message);
  end;
end;

procedure TObservableTask.ExecuteInMainThread (AMethod: TThreadMethod);
begin
  if TThread.CurrentThread.ThreadID = System.MainThreadID
     then begin
            if FQueueCount > 0
               then if now = 0 then raise Exception.Create('hmmm');
            AtomicIncrement (FQueueCount);
            AMethod;
          end
     else begin
            AtomicIncrement (FQueueCount);
            TThread.Queue(nil, AMethod);
          end;
end;

procedure TObservableTask.SetState(AState: TObservableTask.TState);
begin
  FState := AState;
  ExecuteInMainThread (NotifyState);
  while FQueueCount > 0 do
    sleep (1);
end;

procedure TObservableTask.ShowError;
begin
  if Outcome.ErrorHandled then exit;

  Outcome.ErrorHandled := true;

  if Outcome.IsCriticalError
    then // supress critical errors, should be handled in re-login dialogue
    else
      begin
        var s: string := Outcome.Description;
        if s = ''
           then s := format ('Unknown error %d', [Outcome.ErrorCode]);
        if Outcome.ErrorTableName <> ''
           then s := s + #13#10#13#10'Tabela: ' + FController.Model.GetTableDisplayName (Outcome.ErrorTableName);
        if Outcome.ErrorFieldName <> ''
           then s := s + #13#10#13#10'Polje: ' + FController.Model.GetFieldDisplayName (Outcome.ErrorTableName, Outcome.ErrorFieldName);

        ShowErrorDialogue (s);
      end;
end;

function TObservableTask.Success: boolean;
begin
  result := Outcome.Success;
end;

function TObservableTask.VerboseDescription: string;
var
  val: TValue;
  s: string;
  p : TRttiProperty;
  ctx : TRttiContext;
  rtt: TRttiType;
begin
  result := QueryParameters.ToString;
  if Initiator <> nil then
    result := 'Initiator = ' + Initiator.Name + sLineBreak + result;
  p := nil;

  try
    ctx := TRttiContext.Create;
    rtt := ctx.GetType(ClassType);
    for P in rtt.GetProperties do
      begin
        if (p.Name.ToLower <> 'password') then
          begin
            val := P.GetValue(self);
            if p.Parent <> rtt then break;
            if not val.IsEmpty and not val.IsObjectInstance and not val.IsArray and not val.IsObject
              then begin
                     s := val.ToString;
                     if s <> '' then
                       result := result + format ('[%s = %s] ', [P.Name, val.ToString]);
                   end;
          end;
      end;
    ctx.Free;
  except on e: exception do
    try
      result := e.message + ' ';
      if p <> nil
         then result := result + p.Name;
    except
    end;
  end;
end;

//function TObservableTask.GetClient: TSQLHttpClientWebsocketsEx;
//begin
//  result := FController.Client;
//end;

procedure TObservableTask.NotifyController;
begin
  if Initiator <> nil
     then FController.ActiveInitiators.Remove(Initiator);

  DoNotifyController;
end;

procedure TObservableTask.NotifyState;
begin
  AtomicDecrement(FQueueCount);
  try
    FController.NotifyTaskState (self);
  except
    FController.NotifyTaskState (self);
  end;
end;

destructor TSingleRecordTask.Destroy;
begin
  FreeAndNil (FSqlRecord);
  inherited;
end;

procedure TSingleRecordTask.SetSqlRecord(const Value: TSqlRecord);
begin
  FSqlRecord.Free;
  FSqlRecord := Value;
end;

{ TTaskThread }

constructor TTaskThread.Create (AController: TObservableController);
begin
  inherited Create;
  MainThreadNotifications := AController.MainThreadNotifications;
  FreeOnTerminate := true;
  FController := AController;
  CompletedTasks := TThreadList<TObservableTask>.Create;
end;

destructor TTaskThread.Destroy;
begin
  CompletedTasks.Free;
  inherited;
end;

procedure TTaskThread.NotifyBusy;
begin
  FController.NotifyBusy;
end;

procedure TTaskThread.NotifyIdle;
begin
  FController.NotifyIdle;
end;

procedure TTaskThread.Execute;
var
  t: TObservableTask;
  busy: boolean;
  LTasks: TList<TObservableTask>;
  ID: integer;
begin
  inherited;
  ID := AtomicIncrement(FController.ActiveThreads);

  TThread.NameThreadForDebugging (classname + ID.ToString, CurrentThread.ThreadID);
  t := nil;
  busy := false;

  repeat
    begin
      LTasks := FController.Tasks.LockList;
      if LTasks.Count > 0
        then
          begin
            t := LTasks[0];
            if not Primary and not (t.Operation in [TCrud.Read, TCrud.ReadMany])
              then t := nil
              else LTasks.Delete (0);
          end;
      FController.Tasks.UnlockList;
      if t.IsAssigned then
        begin
          if not busy
             then if MainThreadNotifications // call notification from this thread because multithreading in console is tricky
                  then TThread.Queue(nil, NotifyBusy)
                  else NotifyBusy;
          busy := true;
          t.Execute;
          CompletedTasks.Add (t);
          if MainThreadNotifications
             then TThread.Queue(nil, Notify)
             else Notify;
          {TODO -oOwner -cGeneral : ActionItem}
          //Task.SetState (processed?);
          t := nil;
        end
        else begin
               if busy
                  then if MainThreadNotifications
                       then TThread.Queue(nil, NotifyIdle)
                       else NotifyIdle;
               busy := false;
               sleep (5);
             end;
    end;
  until Terminated;

  AtomicDecrement(FController.ActiveThreads);
end;

procedure TTaskThread.Notify; // this method is TThread.Queued
var
  t: TObservableTask;
  LCompletedTasks: TList<TObservableTask>;
begin
  while true do
    begin
      t := nil;
      LCompletedTasks := CompletedTasks.LockList;
      try
        if LCompletedTasks.Count > 0 then
          begin
            t := LCompletedTasks[0];
            LCompletedTasks.Delete(0);
          end;
      finally
        CompletedTasks.UnlockList;
      end;

      if (t = nil) or (t.FQueueCount > 0)
        then exit;

      try
        t.NotifyController;
      finally
        t.Free;
      end;
    end;
end;

{ TObservableSqlRecordTask }

constructor TSqlRecordTask.Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass;
                                             AOperation: TCRUD; const AParameters: TQueryParameters; AInitiator: IServerObserver);
begin
  inherited Create (AController, AOperation, AParameters, AInitiator);
  FSqlRecordClass := ASqlRecordClass;
end;

procedure TSqlRecordTask.DoExecute;
begin
  inherited;

  if Client.Services.Count = 0
     then raise Exception.Create('No services registered yet!');
end;

function TSqlRecordTask.Subject: string;
begin
  result := SqlRecordClass.SQLTableName.AsString;
end;

{ THistoryTask }

constructor THistoryTask.Create(AController: TObservableController; const ATableName, AFieldName, ACurrentValue: string; Initiator: IServerObserver);
begin
  inherited Create (AController, TCRUD.Read, DefaultParameters, Initiator);
  TableName := ATableName;
  FieldName := AFieldName;
  CurrentValue := ACurrentValue;
end;

procedure THistoryTask.DoNotifyController;
begin
  inherited;
  FController.NotifyHistory (self);
end;

{ TForeignTaskObserver }

constructor TForeignTaskObserver.Create(AEnabled: boolean);
begin
  inherited Create;
  FEnabled := AEnabled;
end;

function TForeignTaskObserver.GetEnabled: boolean;
begin
  result := FEnabled;
end;

procedure TForeignTaskObserver.SetEnabled(const Value: boolean);
begin
  FEnabled := Value;
end;

constructor TRecordsTask.Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass; AOperation: TCRUD; const AParameters: TQueryParameters; AInitiator: IServerObserver = nil);
begin
  inherited;
  SqlRecords := TSqlRecords.Create (ASqlRecordClass);
end;

destructor TRecordsTask.Destroy;
begin
  SqlRecords.Free;
  inherited;
end;

procedure TRecordsTask.DoNotifyController;
begin
  inherited;
  FController.NotifyObserveRecords (self);
end;

procedure TObservableController.NotifyObserveRecords(Task: TRecordsTask);
var
  i : integer;
  intf: IRecordsObserver;
begin
  if (Task.Initiator <> nil) and Supports (Task.Initiator, IRecordsObserver, intf)
    then intf.ObserveRecords(Task);

  for i := Observers.Count - 1 downto 0 do
    if (Observers[i] <> Task.Initiator)
       and Supports (Observers[i], IRecordsObserver, intf)
       and ObservesTask (Observers[i], Task)
            then intf.ObserveRecords(Task);
end;

procedure THistoryTask.DoExecute;
begin
  inherited;

  var intf := Client.Service<ICommonService>;
  Outcome := intf.FetchHistory(StringToUTF8 (TableName), StringToUTF8(FieldName), StringToUTF8(CurrentValue), QueryParameters, Values);
end;

{ TObservableMultipleRecordsTask }

procedure TRecordsTask.DoExecute;
var
  tempTable : TSQLTableJSON;
  ol : TObjectList;
  jsonRecords: RawUTF8;
begin
  inherited;
  //var sql := QueryParameters.MormotSql;
  //Outcome.Success := Client.RetrieveListObjArray (SqlRecords.Records, FSqlRecordClass, sql, []);

  var intf := Client.Service<ICommonService>;
  var tn := FSqlRecordClass.SQLTableName;

  Outcome := intf.ReadMany (tn, QueryParameters, jsonRecords);
  if Outcome.Success then
    begin
      if jsonRecords <> ''
        then
          begin
            ol := TObjectList.Create;
            tempTable := TSQLTableJSON.CreateFromTables([FSqlRecordClass], '', jsonRecords);
            try
              tempTable.ToObjectList(ol, FSqlRecordClass);
              SetLength (SqlRecords.Records, ol.Count);
              for var i := 0 to ol.Count - 1 do
                SqlRecords.Records[i] := TSqlRecord (ol[i]);
              ol.OwnsObjects := false;
            finally
              ol.Free;
              tempTable.Free;
            end;
          end
        else
            SetLength (SqlRecords.Records, 0);

      Outcome.RecordCount := SqlRecords.Count;
    end;
end;

{ TGenObservableSingleRecordTask<T> }

constructor TGenObservableSingleRecordTask.Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass; AOperation: TCRUD; const AParameters: TQueryParameters; AInitiator: IServerObserver = nil);
begin
  inherited Create (AController, ASqlRecordClass, AOperation, AParameters, AInitiator);
end;

procedure TGenObservableSingleRecordTask.DoExecute;
var
  tn, json: RawUTF8;
begin
  inherited;

  var intf := Client.Service<ICommonService>;
  if SqlRecord = nil
     then json := ''
     else if Fields <> ''
          then json := SqlRecord.GetJSONValues (true, SqlRecord.IDValue.IsNotNULL, Fields, [])
          else json := SqlRecord.GetJSONValues (true, SqlRecord.IDValue.IsNotNULL,
                       SqlRecord.RecordProps.CopiableFieldsBits {SqlRecord.GetNonVoidFields}, []);

  if self.Operation in [TCRUD.Read, TCRUD.ReadLock, TCRUD.UnLock]
    then tn := FSqlRecordClass.SQLTableName
    else tn := FSqlRecordClass.crudSQLTableName;

  case self.Operation of
    TCRUD.Create : begin
                     Outcome := intf.Add(tn, Fields, json, SqlRecord.DetailIDs);
                     SqlRecord.IDValue := Outcome.ID;
                   end;
    TCRUD.Read,
    TCRUD.ReadLock : begin
                       var jsonRecord: RawUTF8;
                       Outcome := intf.Read(tn, QueryParameters, jsonRecord, Operation = TCRUD.ReadLock);
                       SqlRecord.FillFrom(jsonRecord);
                       //Client.Retrieve(QueryParameters.ID, SqlRecord, false);
                     end;
//    TCRUD.ReadLock : begin
//                       var jsonRecord: RawUTF8;
//
//                       Outcome := intf.ReadLock(tn, QueryParameters.ID, jsonRecord);
//                       SqlRecord.FillFrom(jsonRecord);
//                     end;
    TCRUD.Unlock :  Outcome := intf.Unlock(tn, QueryParameters.ID);
    TCRUD.Update:   Outcome := intf.Update(tn, json, SqlRecord.DetailIDs, Fields);
    TCRUD.Delete :  if SqlRecord.IsAssigned
                       then Outcome := intf.Delete(tn, SqlRecord.IDValue)
                       else Outcome := intf.Delete(tn, QueryParameters.ID);
  end;
end;

procedure TGenObservableSingleRecordTask.DoNotifyController;
begin
  FController.NotifyObserveRecord (self);
  inherited;
end;

procedure TObservableController.NotifyObserveRecord(Task: TGenObservableSingleRecordTask);
var
  i : integer;
  intf: IRecordObserver;
begin
  if (Task.Initiator <> nil) and Supports (Task.Initiator, IRecordObserver, intf)
    then intf.ObserveRecord(Task);

  for i := Observers.Count - 1 downto 0 do
    if (Observers[i] <> Task.Initiator)
       and Supports (Observers[i], IRecordObserver, intf)
       and ObservesTask(Observers[i], Task)
         then intf.ObserveRecord(Task);
end;

procedure TObservableController.NotifyLookup (Task: TObservableLookupTask);
var
  i : integer;
  intf: ILookupObserver;
begin
  inherited;
  if Task.Initiator = nil then
     begin
       Lookups.Dictionary.AddOrSetValue(Task.SqlRecordClass, task.Lookup);
       task.OwnsLookup := false;
     end;

  for i := Observers.Count - 1 downto 0 do
    if Supports (Observers[i], ILookupObserver, intf) //and Supports (Observers[i], IServerObserver, intfObserver)
       and ObservesTask (Observers[i], Task)
         then intf.ObserveLookup(Task);
end;


function TObservableController.RecordUnlock(const SqlRecord: TSQLRecord; Initiator: IServerObserver): TTaskID;
begin
  var task := TGenObservableSingleRecordTask.Create (self, SqlRecord.RecordClass, TCRUD.Unlock, DefaultParameters, Initiator);
  task.QueryParameters.ID := SqlRecord.IDValue;
  result := task.ID;
  AddTask (task);
end;

function TObservableController.RecordUnlock(T: TSqlRecordClass; const ID: TID; Initiator: IServerObserver): TTaskID;
begin
  var task := TGenObservableSingleRecordTask.Create (self, T, TCRUD.Unlock, DefaultParameters, Initiator);
  task.QueryParameters.ID := ID;
  result := task.ID;
  AddTask (task);
end;

function TObservableController.RecordCreateOrUpdate(const SqlRecord: TSqlRecord; DetailIDs: TIDDictionary; Initiator: IServerObserver): TTaskID;
begin
  if SqlRecord.IDValue.IsNULL
     then result := RecordCreate (SqlRecord, DetailIDs, Initiator)
     else result := RecordUpdate (SqlRecord, '', DetailIDs, Initiator);
end;

function TObservableController.RecordCreateOrUpdateWithDetails(const ARecordWithDetails: TRecordWithDetails; AOperation: TCRUD;
         Initiator: IServerObserver): TTaskID;
begin
  var task := TRecordCreateOrUpdateWithDetailsTask.Create (self, ARecordWithDetails, Initiator);
  task.Operation := AOperation;
  result := task.ID;
  AddTask (task);
end;

function TObservableController.RecordCreate(const SqlRecord: TSQLRecord; DetailIDs: TIDDictionary; Initiator: IServerObserver): TTaskID;
var
  attr: TCustomAttribute;
  str, FailedField, error: string;
begin
  if not Validation.ValidateWithAttributes (SqlRecord, 0, false, attr, FailedField, error)
     then begin
            str := format (tcFieldValidationError,
                             [Translator.TranslateDBField (SqlRecord.SQLTableName.AsString, FailedField), error]);
            ShowErrorAndAbort (str);
          end;

  var task := TGenObservableSingleRecordTask.Create (self, SqlRecord.RecordClass, TCRUD.Create, DefaultParameters, Initiator);
  task.SqlRecord := SqlRecord;
  result := task.ID;
  AddTask (task);
end;

//procedure TObservableController.RecordCreate<T>(const SqlRecord: T; DetailIDs: TIDDictionary; Initiator: IServerObserver);
//var
//  attr: TCustomAttribute;
//  str, FailedField, error: string;
//begin
//  if not Validation.ValidateWithAttributes (SqlRecord, 0, false, attr, FailedField, error)
//     then begin
//            str := format (tcFieldValidationError,
//                             [Translator.TranslateDBField (T.SQLTableName.AsString, FailedField), error]);
//            ShowErrorAndAbort (str);
//          end;
//
//  var task := TGenObservableSingleRecordTask.Create (self, T, TCRUD.Create, DefaultParameters, Initiator);
//  task.SqlRecord := SqlRecord;
//  AddTask (task);
//end;

function TObservableController.RecordUpdate (const SqlRecord: TSqlRecord; Initiator: IServerObserver = nil): TTaskID;
begin
  result := RecordUpdate(SqlRecord, '', nil, Initiator);
end;

function TObservableController.RecordUpdate (const SqlRecord: TSqlRecord; Fields: string = ''; DetailIDs: TIDDictionary = nil; Initiator: IServerObserver = nil): TTaskID;
begin
  var task := TGenObservableSingleRecordTask.Create (self, SqlRecord.RecordClass, TCRUD.Update,
              DefaultParameters, Initiator);
  task.SqlRecord := SqlRecord;
  task.Fields.AsString := Fields;
  task.QueryParameters.ID := SqlRecord.IDValue;
  result := task.ID;
  AddTask (task);
end;

procedure TObservableController.RedirectFKs (SqlRecordClass: TSqlRecordClass; ValidID, InvalidID: TID;
                                             DeleteInvalid: boolean; Initiator: IServerObserver);
var
  task: TDBMaintenanceTask;
begin
  if DeleteInvalid
     then task := TDBMaintenanceTask.Create (self, TDBMaintenanceKind.RedirectFKAndDelete, Initiator)
     else task := TDBMaintenanceTask.Create (self, TDBMaintenanceKind.RedirectFK, Initiator);
  task.SqlRecordClass := SqlRecordClass;
  task.ValidID := ValidID;
  task.InvalidID := InvalidID;
  AddTask (task);
end;

function TObservableController.AcquireClient: TSQLHttpClientWebsocketsEx;
begin
  result := nil;

  repeat
    var lst := Clients.LockList;
    try
      if lst.Count > 0 then
        begin
          result := lst[0];
          lst.Delete(0);
        end;
    finally
      Clients.UnlockList;
      if result = nil then sleep (1);
    end;
  until result <> nil;
end;

function TObservableController.TryAcquireClient: TSQLHttpClientWebsocketsEx;
begin
  var lst := Clients.LockList;
  try
    if lst.Count > 0 then
      begin
        result := lst[0];
        lst.Delete(0);
      end
      else result := nil;
  finally
    Clients.UnlockList;
  end;
end;

procedure TObservableController.ReleaseClient(AClient: TSQLHttpClientWebsocketsEx);
begin
  Clients.Add (AClient);
end;

procedure TObservableController.GetFKInfo(SqlRecordClass: TSqlRecordClass; ID: TID; Initiator: IServerObserver);
begin
  var task := TDBMaintenanceTask.Create (self, TDBMaintenanceKind.GetFKInfo, Initiator);
  task.SqlRecordClass := SqlRecordClass;
  task.ValidID := ID;
  AddTask (task);
end;

procedure TObservableController.GetReportFile (const FileName: string; AInitiator: IServerObserver = nil);
begin
  var task := TGetReportFileTask.Create (self, TCRUD.Read, FileName, AInitiator);
  AddTask (task);
end;

function TObservableController.GetTableHistory (ASqlRecordClass: TSQLRecordClass; QueryParameters: TQueryParameters;
                                                AInitiator: IServerObserver): TTaskID;
begin
  var task := TGetTableHistoryTask.Create (self, ASqlRecordClass, QueryParameters, AInitiator);
  result := task.ID;
  AddTask (task);
end;

procedure TObservableController.SetReportFile(const FileName: string; Stream: TStream; AInitiator: IServerObserver);
begin
  var task := TSetReportFileTask.Create (self, TCRUD.Update, FileName, AInitiator);
  task.Stream := TMemoryStream.Create;
  task.Stream.CopyFrom(Stream, 0);
  AddTask (task);
end;

function TObservableController.RecordRead(T: TSqlRecordClass; ID: TID; const IDSourceFieldName: RawUTF8; Initiator: IServerObserver): TTaskID;
begin
  var task := TGenObservableSingleRecordTask.Create (self, T, TCRUD.Read, DefaultParameters, Initiator);
  task.SqlRecord := T.Create;
  task.QueryParameters.ID := ID;
  result := task.ID;
  AddTask (task);
end;

function TObservableController.RecordRead(T: TSqlRecordClass; QueryParameters: TQueryParameters; Initiator: IServerObserver): TTaskID;
begin
  var task := TGenObservableSingleRecordTask.Create (self, T, TCRUD.Read, QueryParameters, Initiator);
  task.SqlRecord := T.Create;
  result := task.ID;
  AddTask (task);
end;

function TObservableController.RecordDelete (const SqlRecord: TSqlRecord; Initiator: IServerObserver = nil): TTaskID;
begin
  var task := TGenObservableSingleRecordTask.Create (self, SqlRecord.RecordClass, TCRUD.Delete, DefaultParameters, Initiator);
  task.SqlRecord := SqlRecord;
  result := task.ID;
  AddTask (task);
end;

function TObservableController.RecordDelete(SqlRecordClass: TSqlRecordClass; ID: TID; Initiator: IServerObserver = nil): TTaskID;
begin
  var task := TGenObservableSingleRecordTask.Create (self, SqlRecordClass, TCRUD.Delete, DefaultParameters, Initiator);
  task.QueryParameters.ID := ID;
  result := task.ID;
  AddTask (task);
end;

function TObservableController.GetLoggedIn: boolean;
begin
  result := LoggedUser <> nil;
end;

procedure TObservableController.GetLookup (SqlRecordClass: TSqlRecordClass; const QueryParameters: TQueryParameters; Initiator: IServerObserver = nil);
begin
  var task := TObservableLookupTask.Create (self, SqlRecordClass, QueryParameters, Initiator);
  task.QueryParameters := QueryParameters;
  {if Client.DummyData
      then task.Outcome.Success := true
          else }AddTask (task);
end;

procedure TObservableController.GetLookupsFor (const SqlRecordClass: TSQLRecordClass; MasterTableName: RawUTF8 = '');
begin
  var ctx := TRttiContext.Create;
  try
    var TableType := ctx.GetType(SqlRecordClass);
      for var prop in TableType.GetProperties do
        for var Attribute in prop.GetAttributes do
          if Attribute is MormotFKAttribute then
            begin
              var at := Attribute as MormotFKAttribute;
              if not Lookups.HasValidLookup (at.SqlRecordClass) //and not PendingLookups.Contains (lk)
                 and (MasterTableName <> at.SqlRecordClass.SqlTableName)
                then
                  GetLookup (at.SqlRecordClass, DefaultParameters);
                  //PendingLookups.Add(lk);
            end;
  finally
    ctx.Free;
  end;
end;

constructor TObservableLookupTask.Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass;
            const AParameters: TQueryParameters; AInitiator: IServerObserver = nil);
begin
  inherited Create (AController, ASqlRecordClass, TCRUD.Read, AParameters, AInitiator);
  Lookup := TLookup.Create;
  // if no initiator the controller should take the lookup into ownership, otherwise
  // its up to observers to take it if they want to
  OwnsLookup := true;
end;

destructor TObservableLookupTask.Destroy;
begin
  if OwnsLookup
     then Lookup.Free;
  inherited;
end;

procedure TObservableLookupTask.DoNotifyController;
begin
  inherited;
  FController.NotifyLookup(self);
end;

procedure TObservableLookupTask.DoExecute;
begin
  inherited;

  var intf := Client.Service<ICommonService>;
  Outcome := intf.GetLookup(SqlRecordClass.ClassName, IDs, Values, QueryParameters);
  Lookup.Add(IDs, Values);
end;

{ TDBMaintenanceTask }

constructor TDBMaintenanceTask.Create(AController: TObservableController; AKind: TDBMaintenanceKind; Initiator: IServerObserver);
begin
  inherited Create (AController, TCRUD.Unknown, DefaultParameters, Initiator);
  Kind := AKind;
end;

procedure TDBMaintenanceTask.DoExecute;
begin
  inherited;

  var intf := Client.Service<IServerManagerService>;

  case Kind of
    TDBMaintenanceKind.RedirectFK:
      begin
        outcome := intf.RedirectFKs(SqlRecordClass.SQLTableName, ValidID, InvalidID, false);
      end;
    TDBMaintenanceKind.RedirectFKAndDelete:
      begin
        outcome := intf.RedirectFKs(SqlRecordClass.SQLTableName, ValidID, InvalidID, true);
      end;
    TDBMaintenanceKind.GetFKInfo:
      begin
        outcome := intf.GetFKInfo(SqlRecordClass.SQLTableName, ValidID);
      end;
  end;
end;

{ TAddOrCreateWithDetailsTask }

constructor TRecordCreateOrUpdateWithDetailsTask.Create(AController: TObservableController; ARecordWithDetails: TRecordWithDetails; AInitiator: IServerObserver);
begin
  var tt := AController.model.Table[SynCommons.UpperCase(ARecordWithDetails.TableName)];
  inherited Create (AController, tt, TCRUD.Unknown, default (TQueryParameters), AInitiator);
  RecordWithDetails := ARecordWithDetails;
//  FSqlRecord := TSqlRecord;
end;

procedure TRecordCreateOrUpdateWithDetailsTask.DoExecute;
begin
  var intf := Client.Service<ICommonService>;

  Outcome := intf.AddOrUpdate(RecordWithDetails);
end;

{ TCustomLoginTask }

procedure TCustomLoginTask.DoLogin;
begin
  if not Client.ServerTimestampSynchronize then
    begin
      Outcome.SetError (seConnectionFailed, Client.LastErrorCode.ToString+': ' + Client.LastErrorMessage.AsString {, e.Message});
      exit;
    end;

  Outcome.Success := Client.SetUser(UserName, Password);

  if Outcome.Success
    then
      begin

        try
          Client.ServiceMustRegister(MandatoryServices, sicShared);

          if Client.SessionUser.GroupRightsID = 1 then
            try
              Client.ServiceMustRegister(FController.AdminServices, sicShared);
            except
              Outcome.SetError (seMandatoryServiceUnavailable);
              exit;
            end;

        except
          Outcome.SetError (seMandatoryServiceUnavailable);
          exit;
        end;

      end
    else
      Outcome.SetError (seLoginFailed);
end;

procedure TCustomLoginTask.DoExecute;
var
  lst: TList<TSQLHttpClientWebsocketsEx>;
  c: TSQLHttpClientWebsocketsEx;
begin
  inherited;

  lst := TList<TSQLHttpClientWebsocketsEx>.Create;

  try
    repeat
      begin
        DoLogin;
        if not Outcome.Success then break; // this client will get released by inherited

        if lst.Count = 0
           then DoAfterLogin; // get user data, other stuff, but only once
        lst.Add(Client); // list contains all clients that the task wont released by inherited's Execute
        Client := FController.TryAcquireClient;
      end;
    until Client = nil;
  finally
    for c in lst do
        FController.ReleaseClient(c);
    lst.Free;
  end;
end;

{ TCustomCommandTask }

constructor TCustomRequestTask.Create (AController: TObservableController; const ACommand: string; const AParameters: variant;
                        const ARequestContent: string; AInitiator: IServerObserver = nil);
begin
  inherited Create (AController, TCrud.Unknown, DefaultParameters, AInitiator);
  Command.AsString := ACommand;
  RequestContent.AsString := ARequestContent;
  RequestParameters := AParameters;
end;

procedure TCustomRequestTask.DoExecute;
begin
  inherited;

  var intf := Client.Service<IServerManagerService>;
//  var Params := DocVariant (['ShopName', 'Blue shop', 'FileName', 'a.txt']);
  Outcome := intf.CustomRequest(Command, RequestParameters, RequestContent, ResponseParameters, ResponseContent);
end;


end.
