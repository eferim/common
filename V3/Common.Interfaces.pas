unit Common.Interfaces;

interface

uses SynCommons, Mormot,
     MormotMods, MormotTypes;

type

  IReportService = interface(IInvokable)
  ['{E159BC67-3630-4C03-A740-19C38C845115}']
    function GetReportFile (const Name: RawUTF8): TServiceCustomAnswer;
    function SetReportFile(const FileName: RawUTF8; const FileContent: RawByteString): TOutcome;
  end;

  ICommonService = interface(IInvokable)
    ['{8C68112A-5B59-43E8-913D-0A4B3A408D26}']
    function GetPublicFile (const Name: RawUTF8): TServiceCustomAnswer;
    function Debug (const Values: variant): TOutcome;
    function Ping: TOutcome;
    function SetMySettings (const varSettings: variant): TOutcome;

    function Add      (const table, fields, jsonRecord : RawUTF8; const DetailIDs: TArray<TSqlRecordDetailIDs>): TOutcome;
    function AddMany  (const table, fields : RawUTF8; const jsonRecords: TRawUTF8DynArray): TOutcome;
    function AddOrUpdate (const ARecordWithDetails: TRecordWithDetails; const AOperation: TCRUD = TCRUD.Unknown): TOutcome;
    function Read     (const table : RawUTF8; const Parameters: TQueryParameters; var jsonRecord: RawUTF8;
                       const Lock: boolean): TOutcome;
    function ReadMany (const table : RawUTF8; const Parameters: TQueryParameters; var jsonRecords: RawUTF8): TOutcome;
    function Unlock   (const table : RawUTF8; const ID: TID): TOutcome;
    function Update   (const table, jsonRecord : RawUTF8; const DetailIDs: TArray<TSqlRecordDetailIDs>;
                       const Fields: RawUTF8 = ''): TOutcome;
    function UpdateAndUnlock (const table, jsonRecord : RawUTF8; const Fields: RawUTF8 = ''): TOutcome;
    function Delete   (const table : RawUTF8; const ID: TID): TOutcome;

    function FetchHistory (const ATableName, AFieldName, ACurrentValue: RawUTF8; const Parameters: TQueryParameters;
                           out Values: TArray<RawUTF8>): TOutcome;
    function GetLookup    (const cl: string; out IDs: TIDDynArray; out Values: TRawUTF8DynArray;
                           const QueryParameters: TQueryParameters): TOutcome;
    function GetExpLookup (const table: RawUTF8; out jsonRecords: RawUTF8; const QueryParameters: TQueryParameters): TOutcome;
    function GetTableHistory (const TableName: RawUTF8; out jsonRecordsCurrent, jsonRecordsHistory: RawUTF8;
                              const QueryParameters: TQueryParameters): TOutcome;
    function GetQuery (out Records: TSqlQueryRecordObjArray; var Parameters: TQueryParameters): TOutcome;
  end;

implementation

end.
