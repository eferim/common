﻿unit mormotMods;

{
Info: Encryption happens in TWebSocketProcess.SendFrame
actual bytes sent done in TCrtSocket.TrySndLow
}

interface

uses
  SysUtils, Contnrs, System.Generics.Collections, Classes, SynCrypto, Generics.Defaults,
  IOUtils, Variants, StrUtils, math, System.TypInfo, RTTI, mORMotHttpClient, SynCrtSock,
  {$ifdef MSWINDOWS}
  Windows, //for inline of synlocker...
  {$endif}
  SynCommons, SynDB, mormot, SynTable, Validation,
  Common.Utils, MormotTypes, MormotAttributes, Translator2Classes;

  {$SCOPEDENUMS ON}

const

  BRIEF_FIELD_DELIMITER = ' ' + chrBlackRightPointingTriangle + ' ';
  DUMMY_VIEW_JOIN = ';';

  Mormot_Administrator_Group_ID = 1;
  Mormot_Supervisor_Group_ID = 2;
  Mormot_User_Group_ID = 3;
  Mormot_Guest_Group_ID = 4;

  {$IFDEF DEBUG}
  Client_Timeout = 5 * 60000;
  {$ELSE}
  Client_Timeout = 10000;
  {$ENDIF}

type

  TMormotDate = longint; // use this instead of date? cus tdate gets stored in db as float for whatever mormot reason

  TMormotServices = array of PTypeInfo;

  IServerManagerService = interface(IInvokable)
  ['{A6D4B645-5FA7-4CC8-A38E-46086EAC1C4E}']
//    procedure GetInfo (out Info: TServerInfo);
    procedure SimulateOneError;
    procedure SimulateOneException;
    function RedirectFKs (const TableName: RawUTF8; const ValidID, InvalidID: TID; DeleteInvalid: boolean): TOutcome;
    function GetFKInfo (const TableName: RawUTF8; const ID: TID): TOutcome;
    function CustomRequest (const Command: RawUTF8; const Parameters: variant; const Content: RawUTF8;
                            out ResponseParameters: variant; out ResponseContent: RawUTF8): TOutcome;
  end;

  TSqlRecordFilterAndStat = class
  public
    Name, StatisticName: string;
    Enabled, Visible: boolean;
    StatisticValue: variant;
    Index, VariantType: integer;
  end;

  TSqlRecordFiltersAndStats = class (TObjectList<TSqlRecordFilterAndStat>)
  private
    function GetValues(index: integer): variant;
    procedure SetValues(index: integer; const Value: variant);
  public
    Any: boolean; // by default true: any met condition makes the record pass the filter
    EnabledCount: integer;
    constructor Create;

    function Add (const Name, StatisticName: string; VariantType: integer = 0; const Enabled: boolean = false;
                  const Visible: boolean = true): integer;
    procedure Assign(fas: TSqlRecordFiltersAndStats);
    procedure AddFrom(fas: TSqlRecordFiltersAndStats);

    procedure IncValue (const index: integer); overload; {$IFNDEF DEBUG}inline;{$ENDIF}
    procedure IncValue (const index: integer; const Amount: variant); overload; {$IFNDEF DEBUG}inline;{$ENDIF}
    function Toggle (index: integer): boolean;
    procedure ClearValues;
    property Values [index: integer]: variant read GetValues write SetValues;

  end;

  FieldInfoClassAttribute = class;

  [AutoUI]
  TFieldInfo = class
  private
    function GetDisplayName: string;
    function GetNameForFilter: string;
  protected
    FNameForFilter, FDisplayName: string;
  public
    SubstituteSortField, Name: string;
    Alignment: TFieldAlignment;
    Indexed, NotNull, ReadOnly, Displayable, Brief, ZeroIsNull, IsUINotNull, IsGeneralFilter,
    IsAlternateUK, IsPlain, IsForeignKey, IsBoolean, IsEnumeratedType, IsDate, IsTime, HasTranslitCopy,
    IsCalculated, IsTextual, ShouldTranslit, IsReportQualifier, IsUnique: boolean;
    Index: integer;
    MormotFieldInfo: TSQLPropInfo;
    ForeignTableName, ForeignKeyName: string;
    SqlRecordClass: TSqlRecordClass;
    UINotNullInfo: TUINotNullInfo;
    constructor Create (ASqlRecordClass: TSqlRecordClass; prop: TRttiProperty; AIndex: integer; AMormotFieldInfo: TSQLPropInfo); overload;
    constructor Create (ASqlRecordClass: TSqlRecordClass; AName: string); overload;
    function GetDisplayString (rec: TSqlRecord): string; virtual;
    function GetDisplayVariant(rec: TSqlRecord): variant;
    procedure SetOrigin (Origin: FieldInfoClassAttribute); virtual; // attribute that told us which fieldinfo class to use
    function DelphiType: string;

    property DisplayName: string read GetDisplayName write FDisplayName;
    property NameForFilter: string read GetNameForFilter write FNameForFilter;
  end;

  TFieldInfoClass = class of TFieldInfo;

  [AutoUI]
  TFieldInfoCurrency = class (TFieldInfo)
  public
    function GetDisplayString (rec: TSqlRecord): string; override;
  end;

  TFieldInfoWithTypeName = class (TFieldInfo)// for translatable enum and booleans
  public
    TypeName: string;
//    procedure SetOrigin (Origin: FieldInfoClassAttribute); override;
//    function GetDisplayString (rec: TSqlRecord): string; override;
  end;

  [AutoUI]
  TFieldInfoEnum = class (TFieldInfoWithTypeName)
  public
    procedure SetOrigin (Origin: FieldInfoClassAttribute); override;
    function GetDisplayString (rec: TSqlRecord): string; override;
  end;

  [AutoUI]
  TFieldInfoBoolean = class (TFieldInfoWithTypeName)
  public
    function GetDisplayString (rec: TSqlRecord): string; override;
    procedure SetOrigin (Origin: FieldInfoClassAttribute); override;
  end;

  // no AutoUI because display value includes translation of table name
  [AutoUI]
  TFieldInfoTableName = class (TFieldInfo)
  public
    function GetDisplayString (rec: TSqlRecord): string; override;
  end;

  // attribute which specifies which class to use for TFieldInfo creation
  FieldInfoClassAttribute = class (TCustomAttribute)
  private
    FFieldInfoClass: TFieldInfoClass;
  public
    constructor Create (AFieldInfoClass: TFieldInfoClass);
    property FieldInfoClass: TFieldInfoClass read FFieldInfoClass;
  end;

  FieldInfoEnumAttribute = class (FieldInfoClassAttribute)
  private
    //procedure SetStrings (Strings: array of string);
  protected
    FList: TListOfStringPointers;
    EnumTypeName: string;
  public
    constructor Create (const AEnumTypeName: string);
  end;

  FieldInfoBooleanAttribute = class (FieldInfoClassAttribute)
  private
    //procedure SetStrings (Strings: array of string);
  protected
    FList: TListOfStringPointers;
    SubTypeName: string;
  public
    constructor Create (const ASubTypeName: string);
  end;

  FieldInfoTableNameAttribute = class (FieldInfoClassAttribute)
  public
    constructor Create;
  end;

  TSqlRecordEx = class (TSQLRecordVirtualTableAutoID)
  protected
    FCreationID: cardinal;
    FDetailIDs: TArray<TSqlRecordDetailIDs>;
    procedure DoBeforeWrite; virtual;
    function DoValidateEx (ForImport: boolean = false): string; virtual;
  public
    SqlRecords: TObject; //holds pointer to "owner" SqlRecords object
    Filtered: boolean;
    class function FiltersAndStats: TSqlRecordFiltersAndStats;
    class procedure InternalDefineModel(Props: TSQLRecordProperties); override;
    procedure ApplyDefaults; virtual;
    procedure CalculateStatistics (const Statistics: TSqlRecordFiltersAndStats); virtual;
    function MatchesAll(const Filters: TSqlRecordFiltersAndStats): boolean; virtual;
    function MatchesAny(const Filters: TSqlRecordFiltersAndStats): boolean; virtual;
    function Matches(const Filters: TSqlRecordFiltersAndStats): boolean;

//    function ValidateLocally: string; virtual; // validate without any db access
//    function ValidateLocally(Level: integer; ForImport: boolean;
//                            out FirstFailedAttribute: TCustomAttribute; out FailedPropertyName, Error: string) : boolean; virtual;

    function Validate(aRest: TSQLRest; const aFields: TSQLFieldBits=[0..MAX_SQLFIELDS-1];
             aInvalidFieldIndex: PInteger=nil; aValidator: PSynValidate=nil): string; override;
    function GetDisplayVariant(const PropName: string): variant; overload; virtual;
    function GetDisplayVariant(const fi: TFieldInfo): variant; overload; virtual;
    function GetFieldVariant(const PropName: string): Variant; override;

    function Transliterate (const Value: RawUTF8): RawUTF8; virtual;
    class procedure InitializeTable(Server: TSQLRestServer; const FieldName: RawUTF8; Options: TSQLInitializeTableOptions); override;
    class function GetSQLCreate(aModel: TSQLModel): RawUTF8; override;
    class function UniqueIfNotNullFields: TStringList;
    class function ViewSQL: RawUTF8; virtual; // generate view SQL, the record has to be a "view sqlrecord" with attributes that describe foreign relationships and joins
    class function ReflectionTriggerSQL: RawUTF8;
    function CreateCopy: TSQLRecordEx; reintroduce;
    {$IFDEF DEBUG}
    constructor Create; override;
    {$ENDIF}
    destructor Destroy; override;
  end;

  // WTF! nemam pojma kako i gde su CreatedAt, ModifiedAt i ModifiedByUserID deklarisani/kreirani, kako postoje u plain TSqlRecord ali NEKAKO postoje... ako se uradi ovo dole onda bude konflikt...
  TSQLRecordTimeStamped = class(TSqlRecordEx)
  private
    fCreatedAt: TCreateTime;
    fModifiedAt: TModTime;
  published
    property CreatedAt: TCreateTime read fCreatedAt write fCreatedAt;
    property ModifiedAt: TModTime read fModifiedAt write fModifiedAt;
  end;

  TSQLRecordTracked = class(TSQLRecordTimeStamped)
  private
    FModifiedByUserID: TSessionUserID;
  public
  published
    property ModifiedByUserID: TSessionUserID read FModifiedByUserID write FModifiedByUserID;
  end;

  TSQLRecordTrackedAndNoted = class(TSQLRecordTracked)
  private
    FNotes: RawUTF8;
  published
    property Notes: RawUTF8 read FNotes write FNotes;
  end;

  TSqlRecordExClass = class of TSqlRecordEx;

  TServerError = integer;

  TServerWarning = (MoreRecordsAvailable);

  TIDHelper = record helper for TID
    function ToRawUTF8: RawUTF8; inline;
    function ToString: string; inline;
    function ToPointer: pointer; inline;
    function IsAuto: boolean; inline;
    function IsNULL: boolean; inline;
    function IsNotNULL: boolean; inline; // cap?
    procedure Clear; inline;
//    procedure SetNull;??
  end;

  TCrudPermissions = class (TDictionary <TCrud, TAuthGroupID>)
  private
    function GetValues(index: TCrud): TAuthGroupID;
  public
    function ToRequiredAuthLevels: TRequiredAuthLevels;
    property Values [index: TCrud]: TAuthGroupID read GetValues; default;
  end;

  // Meant to be dictionary containing arrays of all foreign key IDs of x-to-many relationship
  // e.g. User record has IndexDictionary['Warehouses'] = [ID1, ID2, ID3]
  TIDDictionary = class (TDictionary <string, TIDDynArray>)
    public
      function CreateCopy: TIDDictionary;
  end;

  IMormotInterface = IINvokable;

  TSQLRecordList = class;

  TSQLRecordLists = class (TObjectList<TSQLRecordList>)
    function FindListForTable (Table: TSQLRecordClass): TSQLRecordList; overload;
    function FindListForTable (Table: string): TSQLRecordList; overload;
  end;

  TSQLRecordListFields = array of RawUTF8;

  // Za laksi rad sa listom recorda, verovatno sluzi samo za ORM access pa mozda nece biti preterano korisna
  TSQLRecordList = class
  private
    ObjectList: TObjectList{<TSQLRecord>};
    function GetItems(index: integer): TSQLRecord;
    function GetCount: integer;
  public
    Fields: TSQLRecordListFields;
    Table: TSQLRecordClass;
    constructor Create (ATable: TSQLRecordClass; const AFields: TSQLRecordListFields);
    destructor Destroy; override;

    procedure LoadJson (FileName: TFileName);
    procedure SaveJson (FileName: TFileName);

    function IndexOfID (ID: TID): integer;
    function ItemWithID (ID: TID): TSQLRecord;
    procedure Add (rec: TSqlRecord);
    procedure Clear;

    property Items [index: integer]: TSQLRecord read GetItems; default;
    property Count: integer read GetCount;
  end;

  TSQLSampleRecordList = class (TSQLRecordList)
  protected
    function SampleFileName: string;
  public
    procedure Load;
    procedure Save;
  end;

  TFieldInfoList = class (TList<TFieldInfo>)

    type TFieldInfoListEnum = class
    protected
      FCurrent: integer;
      FOwner: TFieldInfoList;
    public
      constructor Create (const AOwner: TFieldInfoList);
      destructor Destroy; override;
      function GetEnumerator: TFieldInfoListEnum;
      function MoveNext:Boolean; virtual;
      function GetCurrent: TFieldInfo;
      property Current: TFieldInfo read GetCurrent;
    end;

    TFieldInfoListEnumDisplayable = class (TFieldInfoListEnum)
    public
      function MoveNext:Boolean; override;
    end;

    TFieldInfoListEnumNotNull = class (TFieldInfoListEnum)
    public
      function MoveNext:Boolean; override;
    end;

    TFieldInfoListEnumGeneralFilter = class (TFieldInfoListEnum)
    public
      function MoveNext:Boolean; override;
    end;

  private
    function GetItemsByName(index: string): TFieldInfo;
  protected
    FDicNameToItem: TObjectDictionary<string, TFieldInfo>;
  public
    SqlRecordClass: TSqlRecordClass;
    HasBriefFields, HasReadOnlyFields: boolean;
    IDIsBrief: boolean; // because ID field wont be in the list...
    function All: TFieldInfoListEnum;
    function Displayable: TFieldInfoListEnum;
    function NotNull: TFieldInfoListEnum;
    function GeneralFilter: TFieldInfoListEnum;
    function WritableFieldsAsCSV: RawUTF8;
    function Add (Item: TFieldInfo): integer;
    procedure Insert (index: integer; Item: TFieldInfo);
    function HasField (const Name: string): boolean;
    function CalculatedFields: RawUTF8; //csv
    function FieldListForSelect: RawUTF8;
    constructor Create (ASqlRecordClass: TSqlRecordClass);
    destructor Destroy; override;
    property ItemsByName[index: string] : TFieldInfo read GetItemsByName;
  end;

//  TFieldDisplayNames = class (TDictionary <string, string>)
//  public
//    function Get (SqlRecordClass: TSQLRecordClass; FieldName: string): string;
//  end;
//
//  TModelTranslatableContent = class (TDictionary <string, string>)// not used, for future, replace the one above
//  public
//    CurrentLanguage: string;
//    function FieldDisplayName (SqlRecordClass: TSQLRecordClass; FieldName: string): string;
//    function EnumDisplayValue (const EnumType, EnumValue: string): string;
//    function CustomContent    (const Identifier, DefaultValue: string): string;
//  end;

  TSqlModelEx = class;

  TSQLRecordHelper = class helper (TObjectHelper) for TSQLRecord
  private
    function GetAsTID: TID;
    function GetDetailIDs: TArray<TSqlRecordDetailIDs>;
    procedure SetDetailIDs(const Value: TArray<TSqlRecordDetailIDs>);
    class function SubstitutesFileName: string;
  public
    function GetFieldValue        (const PropName: RawUTF8): RawUTF8; reintroduce;
    function GetFieldStringValue  (const PropName: RawUTF8): string; overload;
    function GetFieldStringValue  (const PropName: string): string;  overload;
    function GetFieldVariantValue (const PropName: string): variant; overload;
    function GetFieldVariantValue (const PropName: RawUTF8): variant; overload;
    function GetFieldVariantDisplayValue(const PropName: string): variant; overload;
    function GetFieldVariantDisplayValue(const fi: TFieldInfo): variant; overload;
    function GetLookupClass       (const AFieldName: string; AuthUserClass: TSQLRecordClass): TSQLRecordClass;
    function GenerateRandomValue  (const PropName: string; TestID: integer): string;
    procedure ApplyDefaults;
    class function SampleRecords: TSqlSampleRecordList;
    class function TableDisplayName: string;
    class function FieldDisplayName (const FieldName: string): string;
    class function DefaultSortField: RawUTF8;
    class function SecondarySortField: RawUTF8;
    class function GeneralFilterFields: TGeneralFilterFields;
    class function FieldListForSelect: RawUTF8;
    class function DisplayableFieldListForSelect: RawUTF8;
    class function crudSQLTableName: RawUTF8;
    class function crudClass: TSQLRecordClass;
    class function FieldInfoList: TFieldInfoList;
    class function GetActualSortFieldName (const FieldName: string): string;
    class function CrudAllowed (Operation: TCRUD; Group: integer): boolean;
    class function CrudPermissions: TCrudPermissions;
    class function IsView: boolean;
    class function IsAggregation: boolean;
    class function FieldByName (const Name: string): TSQLPropInfo;
    class function HasFieldByName (const Name: string): boolean;
    class function FieldIndex(const Name: string): integer;
    class function TotalFieldCount: integer;
    class function IsMandatoryField (const FieldName: string): boolean;
    class function Hash: TSHA256;
    class function HasTrimAttribute: boolean;
    class function SubstituteKeyName: string;
    class procedure AddSubstitute (ValidRecord, InvalidRecord: TSqlRecord); overload;
    class procedure AddSubstitute(ValidSubstituteKeyValue, InvalidSubstituteKeyValue: RawUTF8); overload;
    procedure PrintableFields(out Fields: TStringList);
    function FillPrepareWithOrder(aClient: TSQLRest; const aIDs: array of Int64; const OrderBy: RawUTF8 = ''; const aCustomFieldsCSV: RawUTF8=''): boolean;
    procedure LoadFromISQLDBRows (isql: ISQLDBRows; const Prefix: RawUTF8);
    procedure FillDummy;
    procedure PlaceCopyInto (var p: pointer);
    function ValidateEx (out FirstFailedAttribute: TCustomAttribute; out FailedPropertyName, Error: string;
                         const Fields: RawUTF8 = ''; const Level: integer = 0; ForImport: boolean = false): boolean; overload;
    function ValidateEx (const Fields: RawUTF8 = ''; ForImport: boolean = false): string; overload;

    function ContainsUpdatedFields (Original: TSqlRecord): boolean;
//    function FieldDisplayNames: TFieldDisplayNames;
    property AsTID: TID read GetAsTID;
    function AsString: string; // intented as summary, skip non text or non numeric fields
    function AsBriefString: string; // only use brief fields, if defined
    function AsBriefStringWithFieldNames(Model: TSqlModelEx): string;
    function ToVariant (FieldNames: array of RawUTF8): variant;
    function ToRecordWithDetails (Fields: RawUTF8 = ''): TRecordWithDetails; overload;
    procedure ToRecordWithDetails (var rwd: TRecordWithDetails; Fields: RawUTF8 = ''); overload;
    function TrySetFieldStringValue (const FieldName, Value: string): boolean;
    function FieldIsDifferent (OtherRecord: TSqlRecord; const FieldName: RawUTF8): boolean;
    function GetModifiedAt: TModTime;
    procedure ApplySubstitute;
    procedure TrimAll;
    procedure BeforeWrite;
    function EvaluateFilter (Fields: TRawUTF8DynArray; Operators: TFilterOperatorDynArray; Values: TVariantDynArray): boolean;

    property DetailIDs: TArray<TSqlRecordDetailIDs> read GetDetailIDs write SetDetailIDs;
  end;

  TSQLAuthUserHelper = class helper for TSQLAuthUser
  private
    function GetGroupRightsID: TID;
    procedure SetGroupRightsID(const Value: TID);
  public
    property GroupRightsID: TID read GetGroupRightsID write SetGroupRightsID;
  end;

  TSqlRecordArray = array of TSqlRecord;

  TServiceFactoryHelper = class helper for TServiceFactory
  public
    procedure GetOrCrash (out Obj);
  end;

  TServiceContainerHelper = class helper for TServiceContainer
  private
    function HelperGetService(const aURI: RawUTF8): TServiceFactory;
  public
    property Services[const aURI: RawUTF8]: TServiceFactory read HelperGetService; default;
  end;

  TDynArrayHelper = record helper for TDynArray
    function Contains (const Elem): boolean;
  end;

  TCascade = record
    TableClass: TSQLRecordClass;
    ForeignKeyName: RawUTF8;
  end;

  TCascadeList = TList<TCascade>;

  TCascades = class (TObjectDictionary <TSqlRecordClass, TCascadeList>)
  public
    procedure Add (MasterClass, DetailClass: TSqlRecordClass; ForeignKeyName: RawUTF8 = '');
  end;

  TRestrictedMirrorField = record
    TableClass, ForeignTableClass: TSQLRecordClass;
    // e.g. RequestID, VehicleID, VehicleID
    ForeignKeyName, OriginalFieldName, MirrorFieldName: RawUTF8;
    AutoFillIfNull: boolean;
    function FullForeignKeyName: RawUTF8;
    function FullMirrorFieldName: RawUTF8;
  end;

  TRestrictedMirrorFieldList = TList<TRestrictedMirrorField>;

  // dictionary that translates a table into a list of foreign tables and mirror fields
  // to be used for both directions, i.e. one list with master as key and one with detail table as key
  TRestrictedMirrorFields = class (TObjectDictionary <TSqlRecordClass, TRestrictedMirrorFieldList>)
  public
    procedure Add (TableClass: TSqlRecordClass; rcl : TRestrictedMirrorField);
  end;

  TRestrictedField = record
    TableClass: TSQLRecordClass; // e.g. SqlScanType, "Kidney scan"
    FieldName: RawUTF8;
    FTClass: TSQLRecordClass; // e.g. SqlScan, ScanTypeID
    ForeignKeyName: RawUTF8;
  end;

  TRestrictedFieldList = TList<TRestrictedField>;

  TRestrictedFields = class (TObjectDictionary <TSQLRecordClass, TRestrictedFieldList>);

//  TSQLAuthGroupClass = class;

  TSQLAuthUserExClass = class of TSQLAuthUserEx;

  TSQLAuthGroupEx = class(TSQLAuthGroup)
  private
    FDescription: RawUTF8;
  protected
    class function DefaultPassword (const GroupID: TID): RawUTF8; virtual;

    {$IFDEF SERVER}
    class procedure CreateDefaultGroups (Server: TSQLRestServer); virtual;
    class function DefaultDevPassword (const GroupID: TID): RawUTF8; virtual;
  public
    class procedure CreateDefaultUsers (Server: TSQLRestServer); virtual;
    class procedure InitializeTable (Server: TSQLRestServer; const FieldName: RawUTF8;
                                     Options: TSQLInitializeTableOptions); override;
    {$ENDIF}
  public
    const
      GROUP_ID_ADMIN = 1;
      GROUP_ID_SUPERVISOR = 2;
      GROUP_ID_USER = 3;
  published
    property Description: RawUTF8 index 50 read FDescription write FDescription;
  end;

  TSQLAuthGroupExClass = class of TSQLAuthGroupEx;

  TSQLModelEx = class (TSqlModel)
  private
//    function GetTitle(cl: TSqlRecordClass): string;
  protected
//    FTableTitles: TDictionary <TSqlRecordClass, string>;
//    dicFieldDisplayNames : TFieldDisplayNames;
    Language: string;
    class var AlreadySavedTranslationFile: boolean;
    procedure GenerateViewORM (RecordClass: TSQLRecordClass);
    procedure FGetJoinsFields (BaseClass, RecordClass: TSqlRecordClass; ExcludeClasses: TList<TSqlRecordClass>;
                               var Joins, PrivateFields, PublishedFields: string);
  public
    AllowedCascades, RestrictedCascades : TCascades;
    RestrictedFields: TRestrictedFields;
    RestrictedMirrorFieldsOfMaster, RestrictedMirrorFieldsOfDetail : TRestrictedMirrorFields;
    TableOrderFields: TDictionary <TSqlRecordClass, RawUTF8>; // if a table has a field that represents order of records
    TableOrderGroupFields: TDictionary <TSqlRecordClass, RawUTF8>; // addition to above, to act as grouping, usually a foreign key
    lstTables, lstTablesAndViews: TList<TSQLRecordClass>;
    constructor Create(const Tables: array of TSQLRecordClass; SkipViews: boolean; const ALanguage: string; const ARoot: RawUTF8='root'); overload;
    constructor Create (SkipViews: boolean; ALanguage: string = ''); overload; virtual;
    destructor Destroy; override;
    procedure RestrictUserCascades;
    class function AuthUserClass: TSQLAuthUserExClass; virtual;
    class function AuthGroupClass: TSQLAuthGroupExClass; virtual;
    // translation-related

    function GetTableDisplayName(SqlRecordClass: TSQLRecordClass): string; overload;
    function GetTableDisplayName(const TableName: string): string; overload;

    function GetFieldDisplayName(SqlRecordClass: TSQLRecordClass; FieldName: string): string; overload;
    function GetFieldDisplayName(const TableName, FieldName: string): string; overload;

//    function GetEnumDisplayValue(const EnumType, EnumValue: string): string;

    function ImportUnknownFieldContent: string; virtual;
    function Contains (SqlRecordClass: TSQLRecordClass): boolean;
    function Hash: RawUTF8;
//    property TableTitles [SqlRecordClass: TSQLRecordClass]: string read GetTitle;
  end;

  TSqlModelClass = class of TSqlModelEx;

//  TSQLRecordsRecord = class (TSQLRecord)
//
//    destructor Destroy; override;
//  end;

  // Wrapper for mormot client class
  TSQLHttpClientWebsocketsEx = class (TSQLHttpClientWebsockets)  // cant use default TSQLHttpClient(http.sys) because callbacks are not supported
  private
    FReceiveTimeout: cardinal;
    procedure SetReceiveTimeout(const Value: cardinal);
  protected
    FUpgradedToSockets: boolean;
    FWebSocketsEncryptionKey, FUsername, FPassword: string;
    LastRelogin: TDateTime;
  public
    Model: TSQLModel; // TODO there is already a model in inherited but unsure if it can be accessed safely in constructor
    DummyConnection, UseCallbacks: boolean;
    constructor Create (AModel: TSQLModel; const Address, Port: string; AWebSocketsEncryptionKey: string); virtual;

    procedure RegisterServices; virtual;

    function ServiceMustRegister(const aInterfaces: array of PTypeInfo;
                                       aInstanceCreation: TServiceInstanceImplementation=sicSingle;
                                       const aContractExpected: RawUTF8=''): boolean;
    procedure UpgradeToSockets;
    function SetUser(const AUserName, APassword: string; aHashedPassword: Boolean=false): boolean; reintroduce;
    function ReLogin: boolean; virtual;
    function RetrieveListObjArray(var ObjArray; Table: TSQLRecordClass;
      const FormatSQLWhere: RawUTF8; const BoundsSQLWhere: array of const;
      const aCustomFieldsCSV: RawUTF8=''): boolean; reintroduce;
  public
    property UpgradedToSockets: boolean read FUpgradedToSockets;
    property ReceiveTimeout: cardinal read FReceiveTimeout write SetReceiveTimeout;
    property Username: string read FUsername;
  end;

  TSqlAuthUserEx = class (TSQLAuthUser)
  private
    FSettings: variant;
    FNotes: RawUTF8;
    FDeveloper: boolean;
    FActive: boolean;
  public
    function Validate: string;
  published
    [Displayable]
    property Active: boolean read FActive write FActive;

    property Developer: boolean read FDeveloper write FDeveloper;

    property Settings: variant read FSettings write FSettings;

    property Notes: RawUTF8 read FNotes write FNotes;
  end;

  TSqlQueryRecord = class (TSqlRecord)
  private
    FValues: TArray<variant>;
    FNames: TArray<RawUTF8>;
  published
    property Names: TArray<RawUTF8> read FNames write FNames;
    property Values: TArray<variant> read FValues write FValues;
  end;
  TSqlQueryRecordObjArray  = TArray<TSqlQueryRecord>;

  [CrudPermissions (Mormot_Administrator_Group_ID)]
  TSqlDatabaseInfo = class (TSqlRecordEx)
  private
    FVersionNumber: integer;
    FModelHash: RawUTF8;
    FDetails: variant;
  public
    class var ExpectedVersion: integer;
  published
    [GeneralFilter]
    property VersionNumber: integer read FVersionNumber write FVersionNumber;

    property Details: variant read FDetails write FDetails;

    property ModelHash: RawUTF8 read FModelHash write FModelHash;
  end;

  TSqlChangeLog = class (TSqlRecordEx)
  private
    FTableName: RawUTF8;
    FRecordID: TID;
    FChange: TCRUD;
    FOldValues: RawUTF8;
    FChangeTime: TDateTime;
    fModifiedAt: TModTime;
    FOldValues_TL: RawUTF8;
  published
    property ChangeTime: TDateTime read FChangeTime write FChangeTime;

    [GeneralFilter, Brief]
    property TableName: RawUTF8 read FTableName write FTableName;

    property RecordID: TID read FRecordID write FRecordID;

    [GeneralFilter, Brief, FieldInfoEnum ('TCrud')]
    property Change: TCRUD read FChange write FChange;

    [GeneralFilter, Displayable, Brief, HasTranslitCopy]
    property OldValues: RawUTF8 read FOldValues write FOldValues;

    [GeneralFilter]
    property OldValues_TL: RawUTF8 read FOldValues_TL write FOldValues_TL;

//    property CreatedAt: TCreateTime read fCreatedAt write fCreatedAt;
    property ModifiedAt: TModTime read fModifiedAt write fModifiedAt;
  end;

  TSqlReminder = class (TSQLRecordTrackedAndNoted)
  private
    FReminderDate: TDateTime;
    FDescription: RawUTF8;
    FNotifyGroup: TID;
  published
    [Displayable, GeneralFilter]
    property ReminderDate: TDateTime read FReminderDate write FReminderDate;

    [Displayable, GeneralFilter]
    property Description: RawUTF8 read FDescription write FDescription;

    [Displayable]
    property NotifyGroup: TID read FNotifyGroup write FNotifyGroup;
  end;

  TRecordDistances = class (TList<TPair<TSqlRecord, Integer>>)
  public
    function Add (rec: TSqlRecord; Distance: integer): integer;
    procedure Sort;
  end;

  procedure RegisterJSONSerializerClasses (const Classes: array of TClass);

  function ServerErrorOutcome (const error: TServerError; const AdditionalDescription: string = ''): TOutcome;

  function ServerDBErrorOutcome (const error: TServerError; const TableName, FieldName: string;
                               const AdditionalDescription: string = ''): TOutcome; overload;
  function ServerDBErrorOutcome (const error: TServerError; const TableName, FieldName: RawUTF8;
                               const AdditionalDescription: string = ''): TOutcome; overload;

  function RegisterServerError (sMessage: string): TServerError;
  function DateTimeToSqlString (const ADateTime: TDateTime): string;
  function DateToSqlString (const ADate: TDate): string;
  function DocVariant (const NameValuePairs: array of const): variant;

var
  slServerErrorMessages: TStringList;

  seNoError, seUnknown,
  seConnectionFailed, seLoginFailed, seMandatoryServiceUnavailable, seUserRecordNotFound,
  seRecordAlreadyLocked, seCascadeRestriction, seMirroredFieldRestriction, seRestrictedField, seIncorrectMirrorValue,
  seRecordNotFound, seUnknownTable, seNoRecordAccess, seBatchFailed,
  seRecordNotLocked, seRecordAlreadyLockedByMormot, seRecordChangeNotAllowed,
  seNotImplemented, seDetailTableOperationFailed, seException, seWrongCRUD, seNotRecordOwner, seDetailsMissing,
  seValidationFailed, seAddFailed, seUpdateFailed, seInvalidParameters, seRecordValueNotFound,
  seFileNotFound, seDuplicateKey, seDBFunctionFailed, seExpLookupNotDefined, sePermissionDenied,
  seSimulatedError: TServerError;
  // key: name of enum, value: list of string pointers (so that they can be replaced by translation)
  dicEnumToDisplayValues : TDictionary <string, TListOfStringPointers>;

  FSMormot : TFormatSettings;

  mmStrings: TArray<string>; // mormot mods strings...

  mmsUserNameTooShort, mmsUserDisplayNameTooShort, mmsPaswordTooShort, mmsGroupIsMandatory: integer;

implementation



var
  SQLRecordLists: TSQLRecordLists;
  FServerError: TServerError;
  dicDefaultSortFields: TDictionary <TSQLRecordClass, RawUTF8>;
  dicGeneralFilters : TObjectDictionary <TSQLRecordClass, TGeneralFilterFields>;
  dicFieldInfos: TDictionary <TSQLRecordClass, TFieldInfoList>;
  dicUniqueIfNotNullFields: TDictionary <TSQLRecordClass, TStringList>;
  dicTableGroupPermissions: TDictionary <TSQLRecordClass, TCrudPermissions>;
  dicFiltersAndStats: TObjectDictionary<TSqlRecordClass, TSqlRecordFiltersAndStats>;
  dicClassToTrim: TDictionary<TSqlRecordClass, boolean>;
  dicClassToSampleRecords: TObjectDictionary<TSqlRecordClass, TSqlSampleRecordList>;
  dicClassToSubstitutes: TObjectDictionary<TSqlRecordClass, TStringList>;

{ TSQLRecordList }

procedure TSQLRecordList.Add(rec: TSqlRecord);
begin
  ObjectList.Add(rec);
end;

procedure TSQLRecordList.Clear;
begin
  ObjectList.Clear;
end;

constructor TSQLRecordList.Create (ATable: TSQLRecordClass; const AFields: TSQLRecordListFields);
begin
  inherited Create;
  ObjectList := TObjectList.Create;
  ObjectList.OwnsObjects := true;
  SQLRecordLists.Add (self);
  Table := ATable;
  Fields := AFields;
end;

destructor TSQLRecordList.Destroy;
begin
  ObjectList.Free;
  SQLRecordLists.Remove(self);
  inherited;
end;

function TSQLRecordList.GetItems(index: integer): TSQLRecord;
begin
  result := TSQLRecord(ObjectList[index]);
end;

function TSQLRecordList.GetCount: integer;
begin
  result := ObjectList.Count
end;

function TSQLRecordList.IndexOfID(ID: TID): integer;
var
  i : integer;
begin
  for i := 0 to ObjectList.Count-1 do
    if TSQLRecord(ObjectList[i]).ID = ID then exit (i);
  result := -1;
end;

function TSQLRecordList.ItemWithID(ID: TID): TSQLRecord;
begin
  for result in ObjectList do
    if TSQLRecord(result).ID = ID then exit;
  result := nil;
end;

procedure TSQLRecordList.LoadJson(FileName: TFileName);
var
  sl: TStringList;
  jsonString: string;
  json: RawUTF8;
  rec: TSqlRecord;
begin
  Clear;
  if not FileExists (FileName) then exit;

  sl := TStringList.Create;
  try
    sl.LoadFromFile(FileName, TEncoding.UTF8);
    for jsonString in sl do
      begin
        json.AsString := jsonString;
        rec := Table.CreateFrom(json);
        ObjectList.Add(rec);
      end;
  finally
    sl.Free;
  end;
end;

procedure TSQLRecordList.SaveJson(FileName: TFileName);
var
  sl: TStringList;
  rec: TSqlRecord;
begin
  sl := TStringList.Create;
  try
    for rec in ObjectList do
      sl.Add (rec.GetJSONValues (true, false, rec.RecordProps.CopiableFieldsBits {rec.GetNonVoidFields}).AsString);
    sl.SaveToFile(FileName, TEncoding.UTF8);
  finally
    sl.Free;
  end;
end;

function TSQLRecordHelper.GetFieldStringValue(const PropName: RawUTF8): string;
begin
  result := UTF8ToString (GetFieldValue (PropName));
end;

procedure TSQLRecordHelper.PrintableFields(out Fields: TStringList);
begin
  var ctx := TRttiContext.Create;
  try
    var t := ctx.GetType(Self.ClassType);
    for var p in t.GetProperties do
      begin
        var attr := p.GetAttribute<PrintableAttribute>;
        if (attr <> nil) and (p.PropertyType.TypeKind in [System.tkChar, System.tkString, System.tkLString]) then
          begin
            var pf := (attr as PrintableAttribute).PrintField;
            if pf = '' then
              pf := p.Name;
            Fields.AddPair(pf, p.GetValue(self).AsString);
          end;
      end;
  finally
    ctx.Free;
  end;
end;

class function TSQLRecordHelper.SampleRecords: TSqlSampleRecordList;
begin
  if not dicClassToSampleRecords.TryGetValue(self, result) then
    begin
      result := TSqlSampleRecordList.Create (self, []);
      result.Load;
      dicClassToSampleRecords.Add(self, result);
    end;
end;

class function TSQLRecordHelper.SecondarySortField: RawUTF8;
begin
  var ctx := TRttiContext.Create;
  try
    var at := ctx.GetAttribute<SecondarySortFieldAttribute> (self);
    if at <> nil
       then result.AsString := SecondarySortFieldAttribute(at).FieldName
       else result := '';
  finally
    ctx.Free;
  end;
end;

procedure TSQLRecordHelper.SetDetailIDs(const Value: TArray<TSqlRecordDetailIDs>);
begin
  if self is TSqlRecordEx
     then TSqlRecordEx(self).FDetailIDs := Value
     else raise Exception.Create('Trying to set DetailIDs for a non TSqlRecordEx record');
end;

class function TSQLRecordHelper.SubstituteKeyName: string;
var
  ska: SubstituteKeyAttribute;
begin
  if self.InheritsFrom (TSQLAuthUser)
     then exit ('DisplayName');
  
  var ctx := TRttiContext.Create;
  try
    var TableType := ctx.GetType(self);
    for var prop in TableType.GetProperties do
      begin
        ska := prop.GetAttribute<SubstituteKeyAttribute>;
        if ska <> nil then
           exit (prop.Name);
      end;
    result := '';
  finally
    ctx.Free;
  end;
end;

class function TSQLRecordHelper.DefaultSortField: RawUTF8;
begin
  if not dicDefaultSortFields.TryGetValue(self, result) then
    begin
      var ctx := TRttiContext.Create;
      try
        var TableType := ctx.GetType(self);
        for var prop in TableType.GetProperties do
          for var Attribute in prop.GetAttributes do
            if Attribute is DefaultSortFieldAttribute then
            begin
              dicDefaultSortFields.Add(self, StringToUTF8 (prop.Name));
              exit (StringToUTF8 (prop.Name))
            end;
        dicDefaultSortFields.Add(self, '');
        result := '';
      finally
        ctx.Free;
      end;
    end;
end;

function TSQLRecordHelper.GetDetailIDs: TArray<TSqlRecordDetailIDs>;
begin
  if self is TSqlRecordEx
    then result := TSqlRecordEx(self).FDetailIDs
    else result := nil //or setlength(result, 0);
end;


class function TSQLRecordHelper.DisplayableFieldListForSelect: RawUTF8;
begin
  result := '';

  var ctx := TRttiContext.Create;
  try
    var TableType := ctx.GetType(self);
    if ClassParent.InheritsFrom (TSqlRecordEx) and (ClassParent <> TSqlRecordEx)
       then result := TSqlRecordClass (ClassParent).DisplayableFieldListForSelect;

    for var prop in TableType.GetProperties do
      begin
        if prop.Parent <> TableType then continue;

        for var Attribute in prop.GetAttributes do
          if Attribute is DisplayableAttribute then
          begin
            var da := DisplayableAttribute (Attribute);
            var DisplayName: string;
            if da.DisplayName = ''
               then DisplayName := prop.Name
               else DisplayName := da.DisplayName;

            var field := RecordProps.Fields.ByRawUTF8Name(StringToUTF8(prop.Name));
//            if field.Name = IgnoreField
//               then continue;

            if result <> '' then result := result + ', ';
            result := result + SQLTableName+'.' + Field.Name;

            break;
          end;
      end;

  finally
    ctx.Free;
  end;
end;

function TSQLRecordHelper.EvaluateFilter (Fields: TRawUTF8DynArray; Operators: TFilterOperatorDynArray;
                                          Values: TVariantDynArray): boolean;
var
  i: integer;
  v: variant;
//  fi: TFieldInfo;
begin
  i := 0;
  for var FieldName in Fields do
    begin
      if not HasFieldByName(FieldName.AsString) then exit (false);
      v := GetFieldVariant(FieldName.AsString);
      if not CompareValue (v, Values[i], Operators[i])
              then exit (false);
      inc (i);
    end;
  result := true;
end;

class function TSQLRecordHelper.TableDisplayName: string;
begin
  if Translator.IsAssigned
    then result := Translator.TranslateDBTable(SQLTableName.AsString)
    else result := SQLTableName.AsString;
end;

class function TSQLRecordHelper.FieldByName(const Name: string): TSQLPropInfo;
begin
  result := RecordProps.Fields.ByRawUTF8Name(StringToUTF8(Name));
  if (result = nil) and (ClassParent.InheritsFrom(TSqlRecord))
     then result := TSQLRecordClass(ClassParent).FieldByName(Name);
end;

class function TSQLRecordHelper.HasFieldByName (const Name: string): boolean;
begin
  result := (Name = 'ID') or (FieldByName (Name) <> nil);
end;

class function TSQLRecordHelper.Hash: TSHA256;
var
  iField: integer;
  AttHash, tn: RawUTF8;
  ctx : TRttiContext;
  TableType: TRttiType;

begin
  result.init;
  tn := SqlTableName;
  result.Update (pointer (tn), length(tn));

  ctx := TRttiContext.Create;
  try
    TableType := ctx.GetType(self);

    for var Attribute in TableType.GetAttributes do
      if Attribute is HashedAttribute then
        begin
          AttHash.AsString := HashedAttribute(Attribute).Hash;
          result.Update (pointer (AttHash), length(AttHash));
        end;

    for var prop in TableType.GetProperties do
      begin
        if prop.PropertyType is TRttiEnumerationType then
          begin
            var ept := TRttiEnumerationType(prop.PropertyType);
            var name := ept.Name;
            result.Update (pointer (name), length(name));
            for name in ept.GetNames do
                result.Update (pointer (name), length(name));
          end;

        for var Attribute in prop.GetAttributes do
          begin
            if Attribute is HashedAttribute
               then
                 begin
                   AttHash.AsString := HashedAttribute(Attribute).Hash;
                   if AttHash = ''
                      then AttHash.AsString := prop.Name + '.' + Attribute.ClassName;
                   result.Update (pointer (AttHash), length(AttHash));
                 end;
          end;
      end;
  finally
    ctx.Free;
  end;

  for iField := 0 to RecordProps.Fields.Count - 1 do
    begin
      var Field := RecordProps.Fields.Items[iField];
      result.Update (pointer (Field.Name), length(Field.Name));
    end;

  if IsView then
    begin
      var sql := TSqlRecordExClass (self).ViewSQL;
      result.Update (pointer (sql), length(sql));
    end;
end;

class function TSQLRecordHelper.HasTrimAttribute: boolean;
begin
  if not dicClassToTrim.TryGetValue(self, result) then
     begin
       result := ClassHasAttribute(self, TrimAttribute);
       dicClassToTrim.Add (self, result);
     end;
end;

class function TSQLRecordHelper.IsMandatoryField(const FieldName: string): boolean;
begin
  for var fi in FieldInfoList.NotNull do
    if fi.Name = FieldName then exit (true);
  result := false;
end;

class function TSQLRecordHelper.FieldIndex(const Name: string): integer;
begin
  result := RecordProps.Fields.IndexByName(StringToUTF8(Name));
  if (result = -1)
    then if (ClassParent.InheritsFrom(TSqlRecord))
            then result := TSQLRecordClass(ClassParent).FieldIndex(Name)
            else exit (-1)
    else if (ClassParent.InheritsFrom(TSqlRecord))
         then result := result + TSQLRecordClass(ClassParent).TotalFieldCount;
end;

procedure TSQLRecordHelper.ToRecordWithDetails (var rwd: TRecordWithDetails; Fields: RawUTF8 = '');
begin
  rwd.TableName := crudSQLTableName;
  rwd.Fields := Fields;
  if Fields <> ''
    then rwd.jsonRecord := GetJSONValues (true, IDValue.IsNotNULL, Fields, [])
    else rwd.jsonRecord := GetJSONValues (true, IDValue.IsNotNULL, GetNonVoidFields, []);
end;

function TSQLRecordHelper.ToRecordWithDetails (Fields: RawUTF8 = ''): TRecordWithDetails;
begin
  result := default (TRecordWithDetails);
  ToRecordWithDetails (result, Fields);
end;

class function TSQLRecordHelper.TotalFieldCount: integer;
begin
  result := RecordProps.Fields.Count;
  if (ClassParent.InheritsFrom(TSqlRecord))
     then result := result + TSQLRecordClass(ClassParent).TotalFieldCount;
end;

function TSQLRecordHelper.ToVariant (FieldNames: array of RawUTF8): variant;
var
  varpair: variant;
  FieldName: RawUTF8;
begin
  varClear (result);

  for FieldName in FieldNames do
    begin
      varpair := TDocVariant.NewObject([FieldName, GetFieldVariantValue(FieldName)]);
      TDocVariantData (result).AddItem(varpair);
    end;
end;

procedure TSQLRecordHelper.TrimAll;
var
  iField: integer;
  Field: TSQLPropInfo;
begin
  for iField := 0 to RecordProps.Fields.Count - 1 do
    begin
      Field := RecordProps.Fields.Items[iField];
      if Field.SQLFieldType = sftUTF8Text
        then
          Field.SetVariant(self, GetFieldStringValue(Field.Name).Trim);
    end;
end;

function TSQLRecordHelper.TrySetFieldStringValue(const FieldName, Value: string): boolean;
begin
  var Field := FieldByName (FieldName);
  case Field.SQLFieldType of
        sftFloat, sftCurrency:
          begin
            if Value = ''
              then
                begin
                  result := FieldInfoList.ItemsByName[FieldName].ZeroIsNull;
                  Field.SetVariant(self, 0);
                end
              else
                begin
                  var dbl: double;
                  result := TryCurrencyStrToFloat (Value, dbl);
                  if result
                     then SetFieldVariant(FieldName, dbl)
                     else Field.SetVariant(self, 0);
                end;
          end;
        sftUTF8Text: begin
                       result := true;
                       Field.SetVariant(self, Value);
                     end;
        sftID, sftInteger:
          begin
            if Value = ''
              then
                begin
                  result := FieldInfoList.ItemsByName[FieldName].ZeroIsNull;
                  Field.SetVariant(self, 0);
                end
              else
                begin
                  var i: int64;
                  result := TryStrToInt64(Value, i);
                  if result
                     then Field.SetVariant(self, i)
                     else Field.SetVariant(self, 0);
                end;
          end
        else result := false;
      end; //case
end;

class function TSQLRecordHelper.FieldDisplayName(const FieldName: string): string;
begin
  if FieldName.EndsWith ('TableID')
    then result := Translator.TranslateDBTable(FieldName.Substring(0, FieldName.Length-2)) // "Table" ostaje jer je to sad uvek sufix
    else result := Translator.TranslateDBField(SQLTableName.AsString, FieldName);
//  var fdn: TFieldDisplayNames;
//  if dicTableClassToFieldDisplayLabels.TryGetValue (self, fdn)
//     then result := fdn.Get(self, FieldName);
//  if result = ''
//     then result := FieldName;
//     aa
end;

class function TSQLRecordHelper.FieldListForSelect: RawUTF8;
var
  iField: integer;
  Field: TSQLPropInfo;
begin
  result := '';
  for iField := 0 to RecordProps.Fields.Count - 1 do
    begin
      if iField > 0 then result.AppendSQLComma;
      Field := RecordProps.Fields.Items[iField];
      result.AppendSQL (SQLTableName+'.' + Field.Name);
    end;
end;

const INLINED_MAX = 10;

procedure TSQLRecordHelper.FillDummy;
var
  FieldName: RawUTF8;
//  iColumn: integer;
begin
  for var iField := 0 to RecordProps.Fields.Count - 1 do
    begin
      var Field := RecordProps.Fields.Items[iField];
      FieldName := Field.Name;
      case Field.SQLFieldType of
        sftBlob, sftBlobDynArray, sftBlobCustom :
             begin
             end;
        sftBoolean: if random > 0.5 then Field.SetVariant(self, true);
        sftFloat: Field.SetVariant(self, -100 + random * 200);
        sftDateTime: Field.SetVariant(self, now + random + RandomRange(-10, +10));
        sftCurrency: Field.SetVariant(self, random * 100);
        sftUTF8Text: Field.SetVariant(self, FieldName + StringToUTF8 ( IntToStr (RandomRange(100, 999))));
        sftID: Field.SetVariant(self, RandomRange(1, 999));
      end; //case
    end;
end;

function TSQLRecordHelper.FillPrepareWithOrder(aClient: TSQLRest; const aIDs: array of Int64; const OrderBy, aCustomFieldsCSV: RawUTF8): boolean;
begin
  if high(aIDs)<0 then
    result := false else
    result := FillPrepare(aClient,SelectInClause('id',aIDs,'',INLINED_MAX) + OrderBy.AsFieldListToOrderBy, aCustomFieldsCSV);
end;

class function TSQLRecordHelper.GeneralFilterFields: TGeneralFilterFields;
begin
  if not dicGeneralFilters.TryGetValue(self, result) then
    begin
      result := TGeneralFilterFields.Create;
      var ctx := TRttiContext.Create;
      try
        var TableType := ctx.GetType(self);
        for var prop in TableType.GetProperties do
          for var Attribute in prop.GetAttributes do
            if Attribute is GeneralFilterAttribute
              then result.Add(StringToUTF8(prop.Name));

        if InheritsFrom (TSQLAuthGroup)
           then result.Add('Ident');

        if InheritsFrom (TSQLAuthUser)
           then result.Add('DisplayName');

        if (result.Count = 0) or ClassHasAttribute (TableType, IDIsGeneralFilter) then // add at least ID if nothing else was defined
          result.Add ('RowID');

        dicGeneralFilters.Add(self, result);
      finally
        ctx.Free;
      end;
    end;
end;

function TSQLRecordHelper.GenerateRandomValue(const PropName: string; TestID: integer): string;
begin
  var ctx := TRttiContext.Create;
  try
    var t := ctx.GetType(Self.ClassType);
    var p := t.GetProperty(PropName);
    var attr := p.GetAttribute<TestValueGeneratorAttribute>;

    if attr <> nil
       then result := attr.Generate
       else case p.PropertyType.TypeKind of
                 System.tkInteger:
                   result := RandomRange(1, 100).ToString;
                 System.tkEnumeration:
                   result := RandomRange(1, 3).ToString;
                 System.tkFloat:
                   result := (RandomRange(1, 100000)/1000).ToString;
                 System.tkVariant, System.tkString, System.tkWChar, System.tkLString, System.tkWString, System.tkUString:
                   result := PropName {+ ' TEST'} + ' ' + TestID.ToString;
                 //tkSet, tkClass, tkMethod,
                 //tkArray, tkRecord, tkInterface, tkInt64, tkDynArray,
                 //tkClassRef, tkPointer, tkProcedure, tkMRecord
                 else raise Exception.Create('Cant create random value for property ' + p.Name + ' because of its type');
               end; //case

    var MaxLength := (p as TRttiInstanceProperty).Index;
    if MaxLength > 0 then
      while length (result) > MaxLength do
        result := '...' + result.Substring(5);
  finally
    ctx.Free;
  end;
end;

class function TSQLRecordHelper.GetActualSortFieldName(const FieldName: string): string;
var
  fi: TFieldInfo;
begin
  fi := FieldInfoList.ItemsByName[FieldName];
  if fi <> nil
     then result := fi.SubstituteSortField;
  if result = ''
     then result := FieldName;
end;

function TSQLRecordHelper.GetAsTID: TID;
begin
  result := TID(self);
end;

function TSQLRecordHelper.GetFieldStringValue(const PropName: string): string;
begin
  result := UTF8ToString (GetFieldValue (StringToUTF8(PropName)));
end;

function TSQLRecordHelper.GetFieldVariantDisplayValue(const PropName: string): variant;
begin
  if self is TSqlRecordEx
     then result := TSqlRecordEx(self).GetDisplayVariant(PropName)
     else result := GetFieldVariantValue(PropName);
end;

function TSQLRecordHelper.GetFieldVariantDisplayValue(const fi: TFieldInfo): variant;
begin
  if self is TSqlRecordEx
     then result := TSqlRecordEx(self).GetDisplayVariant(fi)
     else result := GetFieldVariantValue(fi.Name);
end;

function TSQLRecordHelper.GetFieldVariantValue(const PropName: RawUTF8): variant;
begin
  result := GetFieldVariantValue(PropName.AsString);
end;

function TSQLRecordHelper.GetFieldVariantValue(const PropName: string): variant;
begin
//  if PropName = 'DatePerformed'
//     then if now=0 then exit;

  VarClear(result);
  if PropName.ToUpper = 'ID'
     then result := ID
     else
    begin
      var P := RecordProps.Fields.ByRawUTF8Name(StringToUTF8(PropName));
      if P=nil
        then result := self.GetFieldVariant (PropName)
        else begin
               case P.SQLFieldType of
                 sftDateTime{, sftDate}:
                               begin
                                 P.GetVariant(self,result);

                                 var dt: TDateTime := TVarData(Result).VDate;
                                 VarClear(result);
                                 result := dt;

                                 if (result = 0) or ((VarType(Result) = varDate) and (TVarData(Result).VDate <= 0))
                                   then varClear (result);
                               end;
                 sftCreateTime,
                 sftModTime:    begin
                                  P.GetVariant(self,result);
                                  if result = 0
                                    then varClear (result)
                                    else result := TimeLogToDateTime (result);
                                end;
                 sftUTF8Text:   begin
                                  var r: RawUTF8;
                                  P.GetValueVar(self,true, r, nil);
                                  RawUTF8ToVariant(r, TVarData(result), varUString);
                                end;
                 sftTID:        begin
                                  P.GetVariant(self,result);
                                  if result = 0 then VarClear(result);
                                end;
                 sftInteger, sftCurrency, sftFloat:
                                begin
                                  P.GetVariant(self,result);
                                  if (result = 0) then
                                    begin
                                      var fi := FieldInfoList.ItemsByName[PropName];
                                      if (fi <> nil)
                                        then
                                          if fi.ZeroIsNull
                                             then VarClear(result)
                                             else
                                        else if now=0 then exit;
                                    end;
                                end
                 else P.GetVariant(self,result);
               end;
             end;
    end;
end;

function TSQLRecordHelper.GetLookupClass(const AFieldName: string; AuthUserClass: TSQLRecordClass): TSQLRecordClass;
begin
  var P := RecordProps.Fields.ByRawUTF8Name(StringToUTF8(AFieldName));
  if P = nil then exit (nil);

  case P.SQLFieldType of
    sftSessionUserID: result := AuthUserClass;
    sftTID: result := TSQLPropInfoRTTITID (P).RecordClass;
    else result := nil;
  end;
end;

class function TSQLRecordHelper.IsAggregation: boolean;
begin
  result := ClassHasAttribute(self, AggregationAttribute);
end;

class function TSQLRecordHelper.IsView: boolean;
begin
  result := ClassHasAttribute(self, ViewAttribute);
end;

procedure TSQLRecordHelper.LoadFromISQLDBRows(isql: ISQLDBRows; const Prefix: RawUTF8);
var
  iField, iColumn : integer;
  FieldName: RawUTF8;
begin
  for iField := 0 to RecordProps.Fields.Count - 1 do
    begin
      var Field := RecordProps.Fields.Items[iField];
      FieldName := Field.Name;
      iColumn := isql.ColumnIndex (Prefix + FieldName);
      if iColumn <> - 1
         then begin
                case Field.SQLFieldType of
                  sftBlob, sftBlobDynArray, sftBlobCustom :
                       begin
//                         var u := isql.ColumnUTF8 (iColumn);
  //                       Field.SetValue (self, @u, true);
                         //Field.SetBinary(self, @u, pointer (cardinal (@u) + length (u) - 1) );

                         var v := isql.ColumnVariant(iColumn);
                         Field.SetVariant(self, v);
                       end;
                  sftUTF8Text:
                    begin
                      var v := isql.ColumnVariant(iColumn);
                      if VarIsNull(v)
                        then v := '';
                      Field.SetVariant(self, v);
                    end
                  else begin
                         var v := isql.ColumnVariant(iColumn);
                         Field.SetVariant(self, v);
                       end;
                end;

              end;
    end;
end;

class function TSQLRecordHelper.SubstitutesFileName: string;
begin
  result := TPath.Combine(FindProjectFileFolder, SQLTableName.AsString + '.substitutes.txt');
end;

class procedure TSQLRecordHelper.AddSubstitute(ValidRecord, InvalidRecord: TSqlRecord);
begin
  if SubstituteKeyName = '' then exit;

  TFile.AppendAllText(SubstitutesFileName, format ('%s=%s',
                                                 [InvalidRecord.GetFieldStringValue(SubstituteKeyName),
                                                  ValidRecord.GetFieldStringValue(SubstituteKeyName)]));
end;

class procedure TSQLRecordHelper.AddSubstitute(ValidSubstituteKeyValue, InvalidSubstituteKeyValue: RawUTF8);
begin
  if SubstituteKeyName = '' then exit;

  TFile.AppendAllText(SubstitutesFileName, format ('%s=%s',
                                                 [InvalidSubstituteKeyValue,
                                                  ValidSubstituteKeyValue]));
end;

procedure TSQLRecordHelper.ApplyDefaults;
begin
  if self is TSqlRecordEx then TSqlRecordEx(self).ApplyDefaults;
end;

function TSQLRecordHelper.AsBriefString: string;
var
//  v: variant;
  s, sAppend: string;
begin
{  if FieldInfoList.IDIsBrief
    then result := IDValue.ToString
        else }result := '';

  sAppend := '';
  for var fi in FieldInfoList.All do
    if fi.Brief then
      begin
        s := fi.GetDisplayString(self);
//        v := GetFieldVariantDisplayValue (fi.Name);
//        s := VarToStr(v);
        if sAppend <> ''
          then sAppend := sAppend + BRIEF_FIELD_DELIMITER;
        if s = ''
          then // if no value, leave sAppend to hold the delimiters
          else // otherwise append to result and clear sAppend
            begin
              sAppend := sAppend + s;
              if result <> '' then result := result + BRIEF_FIELD_DELIMITER;
              result := result + sAppend;
              sAppend := '';
            end;
      end;

  if result = '' then result := AsString;
end;

function TSQLRecordHelper.AsBriefStringWithFieldNames(Model: TSqlModelEx): string;
var
  s: string;

  procedure Add (const Name, Value: string);
  begin
    if result <> '' then result := result + sLineBreak;
    result := result + Name + ': ' + Value;
  end;

begin
  result := '';
//  if FieldInfoList.IDIsBrief
//    then Add ('ID', IDValue.ToString);

  for var fi in FieldInfoList.All do
    if fi.Brief then
      begin
        s := fi.GetDisplayString(self);
        if s <> '' then Add (Model.GetFieldDisplayName (RecordClass, fi.Name), s);
      end;
end;

function TSQLRecordHelper.AsString: string;
var
  iField : integer;
  FieldName: RawUTF8;
  Field: TSQLPropInfo;
  temp: string;
//  fil: TFieldInfoList;
begin
  result := '';
//  if self is TSqlRecordEx
//     then fil := TSqlRecordEx (self).BriefFields
//     else fil := nil;

  for iField := 0 to RecordProps.Fields.Count - 1 do
    begin
      Field := RecordProps.Fields.Items[iField];
      FieldName := Field.Name;
//      if (sl <> nil) and (sl.Count > 0) and (sl.IndexOf (FieldName.AsString) = -1)
//         then continue;
      case Field.SQLFieldType of
        TSQLFieldType.sftUTF8Text, TSQLFieldType.sftDateTime:
          begin
            temp := GetFieldVariantDisplayValue(Field.Name.AsString);
            if temp <> '' then //not VarIsNull (temp) then
              begin
                if result <> '' then result := result + ', ';
                result := result + temp; //Field.GetValue(self, false);
              end;
          end;
        TSQLFieldType.sftCurrency, sftInteger, sftFloat:
          begin
            var v := GetFieldVariantDisplayValue(Field.Name.AsString);
            if not VarIsNull (v)
               and (
                     (VarIsStr(v) and (v<>'')) or (v<>0)
                   ) then
              begin
                if result <> '' then result := result + ', ';
                temp := v;
                result := result + temp; //Field.GetValue(self, false);
              end;
          end;
       end; //case
    end;
end;

procedure TSQLRecordHelper.BeforeWrite;
begin
  if HasTrimAttribute
    then TrimAll;

  if self is TSqlRecordEx then
    begin
      var rex := TSqlRecordEx(self);
      rex.DoBeforeWrite;

      for var f in rex.FieldInfoList do
          if f.HasTranslitCopy then
             rex.SetFieldVariant(f.Name+'_TL', rex.Transliterate(GetFieldValue(f.MormotFieldInfo.Name)));
    end;
end;

//function TSQLRecordHelper.FieldDisplayNames: TFieldDisplayNames;
//begin
//  dicTableClassToFieldDisplayLabels.TryGetValue(self.RecordClass, result);
//end;

function TSQLRecordHelper.ContainsUpdatedFields (Original: TSqlRecord): boolean;
var
  iField : integer;
  FieldName: RawUTF8;
//  ft : TSQLFieldType;
  Field: TSQLPropInfo;
begin
  for iField := 0 to RecordProps.Fields.Count - 1 do
    begin
      Field := RecordProps.Fields.Items[iField];
      //var OriginalField := Original.RecordProps.Fields.Items[iField];
      if Field.GetValue (self, false) <> Field.GetValue (Original, false) then
        begin
          FieldName := Field.Name;
          //ft := Field.SQLFieldType;
          case Field.SQLFieldType of
            TSQLFieldType.sftTID, TSQLFieldType.sftID:
              if Original.GetFieldVariantValue(FieldName.AsString) <> NULL_ID
                then exit (true);
            TSQLFieldType.sftUTF8Text:
              if Field.GetValue(Original, false) <> ''
                then exit (true);
            else exit (true);
           end; //case
        end;
    end;
  result := false;
end;

class function TSQLRecordHelper.CrudPermissions: TCrudPermissions;
var
  cl: TClass;
begin
  dicTableGroupPermissions.TryGetValue(self, result);

  if result = nil then
    begin
      result := TCrudPermissions.Create;

      var ctx := TRttiContext.Create;

      cl := self;
      var t := ctx.GetType(cl);
      while true do
        begin
          if not cl.InheritsFrom (TSqlRecord) then break;
          for var attr in t.GetAttributes do
             if attr is CrudPermissionsAttribute then
                begin
                  var cpa := CrudPermissionsAttribute(attr);
                  for var bc := TBasicCRUD.Create to TBasicCRUD.Delete do
                    if cpa.RequiredLevels[bc] <> NULL_ID then
                      // if already set by inherited, ignore i.e. allow override
                      if not result.ContainsKey (BasicCrudToCrud[bc])
                           then result.Add(BasicCrudToCrud[bc], cpa.RequiredLevels[bc]);
                end;

          t := t.BaseType;
          cl := cl.ClassParent;
        end;

      ctx.Free;
      dicTableGroupPermissions.Add (self, result);
    end;
end;

class function TSQLRecordHelper.CrudAllowed(Operation: TCRUD; Group: integer): boolean;
var
  cp: TCrudPermissions;
  required: TAuthGroupID;
begin
  cp := CrudPermissions;

  if cp.TryGetValue(Operation, required)
     then result := required >= Group
     else result := false;
end;

//type
//  TCustomAttributeClass = class of TCustomAttribute;
//
//function FindAttribute (T: TCustomAttributeClass; cl: TClass): TCustomAttribute;
//begin
//  var ctx := TRttiContext.Create;
//  try
//    var tp := ctx.GetType(cl);
//    for var attr in tp.GetAttributes do
//      if attr is T then
//        exit (attr);
//  finally
//    ctx.Free;
//  end;
//end;

class function TSQLRecordHelper.crudClass: TSQLRecordClass;
begin
  var ctx := TRttiContext.Create;
  try
    var tp := ctx.GetType(self);
    for var attr in tp.GetAttributes do
      if attr is CrudClassAttribute then
        exit (CrudClassAttribute(attr).SqlRecordClass);

    if IsView then exit (TSqlRecordClass(ClassParent))

  finally
    ctx.Free;
  end;

  result := self;
end;

class function TSQLRecordHelper.crudSQLTableName: RawUTF8;
begin
  result := crudClass.SQLTableName;
//  var ctx := TRttiContext.Create;
//  try
//    var tp := ctx.GetType(self);
//    for var attr in tp.GetAttributes do
//      if attr is CrudClassAttribute then
//        exit (CrudClassAttribute(attr).SqlRecordClass.SQLTableName);
//
//    if IsView then exit (TSqlRecordClass(ClassParent).SQLTableName)
//
//  finally
//    ctx.Free;
//  end;
//
//  result := SQLTableName;
end;

procedure TSQLRecordHelper.PlaceCopyInto(var p: pointer);
begin
  p := self.CreateCopy;
end;

function TSQLRecordHelper.ValidateEx (const Fields: RawUTF8 = ''; ForImport: boolean = false): string;
var
  FirstFailedAttribute: TCustomAttribute;
  FailedPropertyName: string;
begin
  ValidateEx (FirstFailedAttribute, FailedPropertyName, result, Fields, 0, ForImport);
  if (result = '') and (self is TSqlRecordEx)
     then result := TSqlRecordEx(self).DoValidateEx(ForImport);

  if result <> ''
     then if FailedPropertyName = ''
          then //result already has all the info we can get
          else result := format (tcFieldValidationError, [Translator.TranslateDBField (SQLTableName.AsString, FailedPropertyName), result])
end;

function TSQLRecordHelper.ValidateEx (out FirstFailedAttribute: TCustomAttribute; out FailedPropertyName, Error: string;
                                      const Fields: RawUTF8 = ''; const Level: integer = 0; ForImport: boolean = false): boolean;
var
  ind: integer;
  slFields: TStringList;
//  attr: TCustomAttribute;
begin
  // call some generic validation first, which works with any class object
  result := Validation.ValidateWithAttributes (self, Level, ForImport, FirstFailedAttribute, FailedPropertyName, Error, Fields.AsString);

  if Fields <> ''
    then
      begin
        slFields := TStringList.Create;
        slFields.CommaText := Fields.AsString;
      end
    else slFields := nil;
  try
    if result then
  //        result := format (tcFieldValidationError, [Translator.TranslateDBField (SQLTableName.AsString, FailedField), error])
      begin
        FirstFailedAttribute := nil;
        FailedPropertyName := '';
        Error := '';
        var ctx := TRttiContext.Create;
        try
          var TableType := ctx.GetType(self.ClassType);
          for var prop in TableType.GetProperties do
            begin // check index i.e. max length
              if (slFields = nil) or not slFields.Contains (prop.Name) then continue;

              ind := (Prop as TRttiInstanceProperty).Index;
              if (ind > 0) and
                (prop.PropertyType.TypeKind in [System.tkChar, System.tkString, System.tkLString]) and
                (length (prop.GetValue(self).AsString) > ind)
               then
                 begin
                   Error := format (tcMaxLength, [ind]);
                   Error := format (tcFieldValidationError, [Translator.TranslateDBField (SQLTableName.AsString, prop.Name), error]);
                   FailedPropertyName := prop.Name;
                   exit (false);
                 end;

            var mfka := prop.GetAttribute<MormotFKAttribute>;
            if (mfka <> nil) then
              begin
                if not (mfka.SelfReferencingAllowed) and (mfka.SqlRecordClass = RecordClass)
                   and IDValue.IsNotNULL and (prop.GetValue(self).AsOrdinal = IDValue)
                   then begin
                          FirstFailedAttribute := mfka;
                          FailedPropertyName := prop.Name;
                          Error := 'Polje ne može pokazivati na samog sebe';
                          exit;
                        end;
              end;
            end;
        finally
        end;
      end;
  finally
    slFields.Free;
  end;
end;

function TSQLRecordHelper.GetFieldValue(const PropName: RawUTF8): RawUTF8;
var
  left, right: RawUTF8;
  PropInfo: TSQLPropInfo;
  list: TSQLRecordList;
  t: string;
begin
  var p := pos ('.', PropName.AsString);
  if p > 0
    then
      begin
        Split (PropName, '.', left, right);
        PropInfo := RecordProps.Fields.ByName(pointer(left));
        if PropInfo<>nil then
          begin
            t := PropInfo.SQLFieldRTTITypeName.AsString;
            var tbdpos := pos ('ToBeDeletedID', t);
            if tbdpos > 0 then
              t := t.Remove(tbdpos - 1);
            list := SQLRecordLists.FindListForTable(t);
            if list = nil
               then result.AsString := t
               else begin
                      var rec := list.ItemWithID(GetFieldVariant(left.AsString));
                      if rec = nil
                         then result := ''
                         else result := rec.GetFieldValue(right);
                    end;
          end;
      end
    else
      if PropName = 'ID'
        then result := StringToUTF8 (IntToStr(ID))
        else result := inherited GetFieldValue(PropName);
end;

{ TSQLRecordLists }

function TSQLRecordLists.FindListForTable(Table: TSQLRecordClass): TSQLRecordList;
begin
  for result in self do
    if result.Table = Table then exit;
  result := nil;
end;

function TSQLRecordLists.FindListForTable(Table: string): TSQLRecordList;
begin
  for result in self do
    if result.Table.ClassName = Table then exit;
  result := nil;
end;

{ TSQLAuthUserHelper }

function TSQLAuthUserHelper.GetGroupRightsID: TID;
begin
  result := TID (GroupRights);
end;

procedure TSQLAuthUserHelper.SetGroupRightsID(const Value: TID);
begin
  GroupRights := TSQLAuthGroup (Value);
end;

procedure RegisterJSONSerializerClasses (const Classes: array of TClass);
begin
  for var ClassItem in Classes do
    TJSONSerializer.RegisterClassForJSON(ClassItem);
end;

{ TServiceFactoryHelper }

procedure TServiceFactoryHelper.GetOrCrash(out Obj);
begin
  if not Get (Obj)
     then raise Exception.Create('Service not defined');
end;

{ TServiceContainerHelper }

function TServiceContainerHelper.HelperGetService(const aURI: RawUTF8): TServiceFactory;
begin
  result := GetService(aURI);
  if result = nil
    then raise Exception.Create('Service ' + aURI.AsString + ' not registered');
end;

{ TIDDictionary }

function TIDDictionary.CreateCopy: TIDDictionary;
begin
  if self = nil then exit (nil);

  result := TIDDictionary.Create;
  for var key in self.Keys do
    result.Add(key, self[key]);
end;

{ TDynArrayHelper }

function TDynArrayHelper.Contains(const Elem): boolean;
begin
  result := IndexOf (Elem) > -1;
end;

{ TIDHelper }

procedure TIDHelper.Clear;
begin
  self := NULL_ID;
end;

function TIDHelper.IsAuto: boolean;
begin
  result := self = AUTO_ID;
end;

function TIDHelper.IsNotNull: boolean;
begin
  result := self <> NULL_ID;
end;

function TIDHelper.IsNull: boolean;
begin
  result := self = NULL_ID;
end;

function TIDHelper.ToPointer: pointer;
begin
  result := pointer (self);
end;

function TIDHelper.ToRawUTF8: RawUTF8;
begin
  result := Int64ToUtf8(self);
end;

function TIDHelper.ToString: string;
begin
  result := IntToString(self);
end;

{ TRestrictedCascades }

procedure TCascades.Add(MasterClass, DetailClass: TSqlRecordClass; ForeignKeyName: RawUTF8 = '');
var
  lst : TList<TCascade>;
  rc: TCascade;
begin
  if ForeignKeyName = '' then
    if DetailClass.RecordProps.Fields.ByRawUTF8Name (MasterClass.SQLTableName + 'ID') <> nil
       then ForeignKeyName := MasterClass.SQLTableName + 'ID'
       else if DetailClass.RecordProps.Fields.ByRawUTF8Name (MasterClass.SQLTableName) <> nil
       then ForeignKeyName := MasterClass.SQLTableName
       else raise Exception.Create('Cant find foreign key for ' + DetailClass.SQLTableName.AsString + ' -> ' + MasterClass.SQLTableName.AsString);

  if not TryGetValue(MasterClass, lst) then
     begin
       lst := TList<TCascade>.Create;
       inherited Add (MasterClass, lst);
     end;

  rc.TableClass := DetailClass;
  rc.ForeignKeyName := ForeignKeyName;

  lst.Add(rc);
end;

function ServerDBErrorOutcome (const error: TServerError; const TableName, FieldName: RawUTF8;
                             const AdditionalDescription: string = ''): TOutcome;
begin
  result := ServerDBErrorOutcome (error, TableName.AsString, FieldName.AsString, AdditionalDescription);
end;

function ServerDBErrorOutcome (const error: TServerError; const TableName, FieldName: string;
                               const AdditionalDescription: string = ''): TOutcome;
begin
  result := ServerErrorOutcome (error);
  result.ErrorTableName := TableName;
  result.ErrorFieldName := FieldName;
  if AdditionalDescription <> '' then
     result.Description := result.Description + #13#10 + AdditionalDescription
end;

function ServerErrorOutcome (const error: TServerError; const AdditionalDescription: string = ''): TOutcome;
begin
  if error = seRecordChangeNotAllowed then
    if HInstance = 1 then Exit; //todo wtf is this...
  if error = seRecordNotFound then
    if HInstance = 1 then Exit;

  result.Success := false;
  result.ErrorCode := integer(error);
  result.Description := slServerErrorMessages [error];
  if AdditionalDescription <> '' then
     result.Description := result.Description + #13#10 + AdditionalDescription
end;

function RegisterServerError (sMessage: string): TServerError;
begin
  slServerErrorMessages.Add(sMessage);
  result := FServerError;
  inc (FServerError);
end;

{ TSQLRecordsRecord }

//destructor TSQLRecordsRecord.Destroy;
//begin
//  if Records <> nil then
//    raise Exception.Create('This record is still in a SqlRecords array!');
//  inherited;
//end;

{ TFunSQLModel }

class function TSQLModelEx.AuthGroupClass: TSQLAuthGroupExClass;
begin
  result := TSQLAuthGroupEx;
end;

class function TSQLModelEx.AuthUserClass: TSQLAuthUserExClass;
begin
  result := TSqlAuthUserEx;
end;

constructor TSQLModelEx.Create(const Tables: array of TSQLRecordClass; SkipViews: boolean;
            const ALanguage: string; const aRoot: RawUTF8);
var
  temp: array of TSqlRecordClass;
  i: integer;
  rcl: TRestrictedMirrorField;
  ctx : TRttiContext;
begin
  lstTables := TList<TSQLRecordClass>.Create;
  lstTablesAndViews := TList<TSQLRecordClass>.Create;

  i := 0;

  for var Table in Tables do
    if lstTablesAndViews.Contains(Table)
      then raise Exception.Create('Table ' + Table.SQLTableName.AsString + ' added to model twice!')
      else begin
             lstTablesAndViews.Add (Table);
             if not Table.IsView
                then lstTables.Add (Table);
           end;

  if SkipViews
    then
      begin
        setlength (temp, length(Tables));
        for var Table in Tables do
          begin
            if Table.IsView
               then continue;

            temp[i] := Table;
            inc (i);
          end;
        setlength (temp, i);

        inherited Create (temp, aRoot);
      end
    else inherited Create (Tables, aRoot);

  Language := ALanguage;

  if not SkipViews then
    for var Table in Tables do
      if Table.IsView
         then Props[Table].Kind := rCustomForcedID;
       //  result.Props[TSqlLicenseView].itoNoCreateMissingField := true;

  RestrictedCascades := TCascades.Create ([doOwnsValues]);
  AllowedCascades := TCascades.Create ([doOwnsValues]);

  RestrictedMirrorFieldsOfMaster := TRestrictedMirrorFields.Create ([doOwnsValues]);
  RestrictedMirrorFieldsOfDetail := TRestrictedMirrorFields.Create ([doOwnsValues]);
  RestrictedFields := TRestrictedFields.Create;

//  FTableTitles := TDictionary<TSqlRecordClass, string>.Create;
  TableOrderFields := TDictionary<TSqlRecordClass, RawUTF8>.Create;
  TableOrderGroupFields := TDictionary<TSqlRecordClass, RawUTF8>.Create;

  ctx := TRttiContext.Create;

  try
    for var Table in Tables do
      begin
//        if not SkipViews
//           then dicTableClassToFieldDisplayLabels.AddOrSetValue (Table, dicFieldDisplayNames); {TODO - -cGeneral : make safer, this has to be done only once}
        var TableType := ctx.GetType(Table);

        for var prop in TableType.GetProperties do
          begin
            {$ifdef GenerateTranslationFile}
            if not SkipViews and prop.HasAttribute (DisplayableAttribute)
               then GetFieldDisplayName(Table, prop.Name);
            {$endif}
            for var Attribute in prop.GetAttributes do
              begin
                if Attribute is FKRuleAttribute then
                  begin
                    var MasterTable := FKRuleAttribute (Attribute).SqlRecordClass;
//                    var MasterClassName := copy (prop.PropertyType.Name, 5, length(prop.PropertyType.Name) - 2 - 4);
//                    var MasterTable := self.Table[StringToUTF8 (MasterClassName)];
//                    if MasterTable = nil then
//                      begin
//                        MasterClassName := copy (prop.Name, 1, length(prop.Name) - 2);
//                        MasterTable := self.Table[StringToUTF8 (MasterClassName)];
//                      end;
                    if MasterTable = nil
                       then raise Exception.Create(format ('Cant find master table for restricted FK %s[%s] -> %s',
                            [prop.Name, prop.PropertyType.Name, '?'{MasterClassName}]));

                    if MasterTable.IsView or Table.IsView
                       then continue;// skip if either is a view because it inherited the table and the cascade was already defined for that table

                    if Attribute is FKRuleAttribute
                      then case FKRuleAttribute(Attribute).OnDelete of
                        TReferentialAction.Restrict: RestrictedCascades.Add (MasterTable, Table, StringToUTF8 (prop.Name));
                        TReferentialAction.Cascade: AllowedCascades.Add    (MasterTable, Table, StringToUTF8 (prop.Name)); //FKCascadeRestrictedAttribute
                      end;
                  end;

                if not Table.IsView and (Attribute is FTRestrictedFieldAttribute) then
                  begin
                    var rfa := FTRestrictedFieldAttribute(Attribute);

                    var rf: TRestrictedField;
                    rf.TableClass := Table;
                    rf.FieldName.AsString := prop.Name;

                    rf.FTClass := self.Table[rfa.ForeignTableClass.SqlTableName];
                    if rf.FTClass = nil then
                       raise Exception.Create('Cant find foreign table class ' + rfa.ForeignTableClass.SqlTableName.AsString);

                    if rfa.ForeignKeyName = ''
                       then rf.ForeignKeyName := table.SQLTableName + 'ID'
                       else rf.ForeignKeyName := rfa.ForeignKeyName;

                    var lst: TRestrictedFieldList;
                    if not RestrictedFields.TryGetValue (Table, lst) then
                      begin
                        lst := TRestrictedFieldList.Create;
                        RestrictedFields.Add (Table, lst);
                      end;

                    lst.Add(rf);
                  end;

                if not Table.IsView and (Attribute is FTRestrictedMirrorFieldAttribute) then
                  // e.g. EstInspection.VehicleID mirrors Request.VehicleID
                  // Foreign table is Request, not Vehicle.
                  begin
                    var mfa := FTRestrictedMirrorFieldAttribute(Attribute);

                    var ForeignTableClassName := mfa.ForeignTableClass.SqlTableName;
                    var ForeignTableClass := self.Table[ForeignTableClassName];

                    if ForeignTableClass = nil then
                       raise Exception.Create('Cant find foreign table class ' + ForeignTableClassName.AsString);

                    rcl.TableClass := Table;
                    rcl.ForeignTableClass := ForeignTableClass;
                    if mfa.ForeignKeyName = ''
                       then rcl.ForeignKeyName := ForeignTableClass.SqlTableName + 'ID'
                       else rcl.ForeignKeyName := mfa.ForeignKeyName;
                    rcl.MirrorFieldName := StringToUTF8 (prop.Name);
                    if mfa.OriginalFieldName = ''
                       then rcl.OriginalFieldName := rcl.MirrorFieldName
                       else rcl.OriginalFieldName := mfa.OriginalFieldName;
                    rcl.AutoFillIfNull := mfa.AutoFillIfNull;
                    RestrictedMirrorFieldsOfMaster.Add (ForeignTableClass, rcl);
                    RestrictedMirrorFieldsOfDetail.Add (Table, rcl);
                    // lock master for update while doing insert? ensure server fills the field value
                    // or checks if correct before insert/update?
                  end;
              end;
          end;
      end;
  finally
    ctx.Free;
  end;

  {$ifdef GenerateTranslationFile}
  Translator.InvalidateDBElements;
  for var Table in Tables do
    begin
      Translator.AddDBTable(Table.SQLTableName.AsString);
      for var Field in Table.FieldInfoList.All do
        Translator.AddDBField(Table.SQLTableName.AsString, Field.Name);
    end;
  Translator.CleanupInvalid;
  {$endif}
end;

function TSQLModelEx.Contains(SqlRecordClass: TSQLRecordClass): boolean;
var
  i: integer;
begin
  for i := 0 to TablesMax  do
    if Tables[i] = SqlRecordClass then exit (true);
  result := false;
end;

constructor TSQLModelEx.Create (SkipViews: boolean; ALanguage: string = '');
begin
  raise Exception.Create('Should not be called, override and call the other base constructor from the inherited constructor');
end;

destructor TSQLModelEx.Destroy;
begin
  FreeAndNil(lstTables);
  FreeAndNil(lstTablesAndViews);
  FreeAndNil(RestrictedCascades);
  FreeAndNil(AllowedCascades);
  FreeAndNil(RestrictedMirrorFieldsOfMaster);
  FreeAndNil(RestrictedMirrorFieldsOfDetail);
  FreeAndNil(RestrictedFields);
//  FreeAndNil(FTableTitles);
  FreeAndNil(TableOrderFields);
  FreeAndNil(TableOrderGroupFields);
  inherited;
end;

//function TSQLModelEx.GetEnumDisplayValue(const EnumType, EnumValue: string): string;
//begin
//  if self = nil then exit (EnumValue);
//  if not dicFieldDisplayNames.TryGetValue(EnumType + '.' + EnumValue, result) or (result = '')
//     then begin
//            dicFieldDisplayNames.AddOrSetValue (EnumType + '.' + EnumValue,  '');
//            result := EnumValue;
//          end;
//end;

function TSQLModelEx.GetFieldDisplayName(SqlRecordClass: TSQLRecordClass; FieldName: string): string;
begin
  result := GetFieldDisplayName(SqlRecordClass.SQLTableName.AsString, FieldName);
end;

procedure TSQLModelEx.FGetJoinsFields (BaseClass, RecordClass: TSqlRecordClass; ExcludeClasses: TList<TSqlRecordClass>;
                               var Joins, PrivateFields, PublishedFields: string);
var
  fi: TFieldInfo;
  LocalFieldName: string;
begin
  ExcludeClasses.Add (RecordClass);

  for fi in RecordClass.FieldInfoList do
    begin
      if fi.IsForeignKey then
        begin
          var rc := Table[StringToUTF8 (fi.ForeignTableName)];
          if ExcludeClasses.Contains(rc) then continue;

          if fi.ForeignKeyName = ''
            then Joins := Joins + format ('  [SimpleView (TSql%s)]'#13, [fi.ForeignTableName])
                           //    constructor Create (AReferencedClass: TSqlRecordClass; const ReferencedKeyName, ForeignKeyName: RawUTF8);
            else Joins := Joins + format ('  [SimpleView (TSql%s, ID, %s)]'#13, [fi.ForeignTableName, fi.ForeignKeyName]);

          FGetJoinsFields (BaseClass, rc, ExcludeClasses, Joins, PrivateFields, PublishedFields);
        end;

      if (BaseClass <> RecordClass) and (fi.Name <> 'ID') and (fi.Displayable) then
        begin
          if true//BaseClass.HasFieldByName (fi.Name)
            then begin
                   LocalFieldName := RecordClass.SQLTableName.AsString + fi.Name;
                   PublishedFields := PublishedFields + format ('    [Displayable, ForeignField(TSql%s, ''%s'')]'#13, [RecordClass.SQLTableName, fi.Name]);
                 end
            else begin
                   LocalFieldName := fi.Name;
                   PublishedFields := PublishedFields + format ('    [Displayable, ForeignField(TSql%s)]'#13, [RecordClass.SQLTableName]);
                 end;

          PublishedFields := PublishedFields + format ('    property %s: %s read F%s write F%s;'#13#13, [LocalFieldName, fi.DelphiType, LocalFieldName, LocalFieldName]);

          PrivateFields := PrivateFields + format ('    F%s: %s;'#13, [LocalFieldName, fi.DelphiType]);
        end;
    end;

end;

procedure TSQLModelEx.GenerateViewORM(RecordClass: TSQLRecordClass);
var
  sb: TStringBuilder;
  Joins, PrivateFields, PublishedFields: string;
//  fi: TFieldInfo;
  ExcludeClasses : TList<TSqlRecordClass>;
begin
  sb := TStringBuilder.Create;
  Joins := '';
  PublishedFields := '';
  PrivateFields := '';
  ExcludeClasses := TList<TSqlRecordClass>.Create;
  FGetJoinsFields (RecordClass, RecordClass, ExcludeClasses, Joins, PrivateFields, PublishedFields);

  sb.Append(Joins);
  sb.Append(format ('  %s = class (TSql%s)'#13, [RecordClass.ClassName, TSqlRecordClass(RecordClass.ClassParent).SQLTableName]));
  sb.Append('  private'#13);
  sb.Append(PrivateFields);
  sb.Append('  published'#13);
  sb.Append(PublishedFields);
  sb.Append('  end;'#13);

  TFile.WriteAllText (TPath.Combine (ApplicationExeFolder, 'GeneratedView.pas'), sb.ToString);
  sb.Free;
  ExcludeClasses.Free;
  halt;
end;

function TSQLModelEx.GetFieldDisplayName(const TableName, FieldName: string): string;
begin
  result := Translator.TranslateDBField(TableName, FieldName);
end;

function TSQLModelEx.GetTableDisplayName(const TableName: string): string;
begin
  result := Translator.TranslateDBTable(TableName);
end;

function TSQLModelEx.GetTableDisplayName(SqlRecordClass: TSQLRecordClass): string;
begin
  result := GetTableDisplayName(SqlRecordClass.SQLTableName.AsString);
end;

//function TSQLModelEx.GetTitle(cl: TSqlRecordClass): string;
//begin
//  if not FTableTitles.TryGetValue(cl, result)
//    then result := cl.SQLTableName.AsString;
//end;

function TSQLModelEx.Hash: RawUTF8;
var
  hashModel, hashTable: TSHA256;
  digestTable, digestModel: THash256;
begin
  hashModel.init;
  for var table in tables do
    begin
      hashTable := table.Hash;
      hashTable.Final(digestTable);
      hashModel.Update(@digestTable, sizeof(digestTable));
    end;

  var ctx := TRttiContext.Create;
  try
    var t := ctx.GetType(classtype);
      for var Attribute in t.GetAttributes do
        if Attribute is RequiredResource
          then begin
                 var rid := RequiredResource(Attribute).ResourceIdentifier;
                 hashModel.Update(pointer(rid), length(rid) * SizeOf(Char));
               end;
  finally
    ctx.Free;
  end;

  hashModel.Final(digestModel);

  result := SHA256DigestToString(digestModel);
end;

function TSQLModelEx.ImportUnknownFieldContent: string;
begin
  result := 'Import - Nepoznato';
end;

procedure TSQLModelEx.RestrictUserCascades;
begin
  for var table in Tables do
    begin
      var RecordProps := table.RecordProps;
      for var i := 0 to RecordProps.Fields.Count-1 do
          if RecordProps.Fields.Items[i].SQLFieldType = sftSessionUserID then
             RestrictedCascades.Add(AuthUserClass, table, RecordProps.Fields.Items[i].Name);
    end;
end;

constructor TSQLHttpClientWebsocketsEx.Create (AModel: TSQLModel; const Address, Port: string; AWebSocketsEncryptionKey: string);
begin
  inherited Create (ansistring(Address), ansistring(Port),
                    AModel, false {todo check https, currently}, '', '', Client_Timeout, Client_Timeout, Client_Timeout);
  Model := AModel;
  FWebSocketsEncryptionKey := AWebSocketsEncryptionKey;
  RetryOnceOnTimeout := false;
  if Model <> nil then Model.Owner := self;
  UseCallbacks := FindCmdLineSwitch('callbacks');
end;

procedure TSQLHttpClientWebsocketsEx.RegisterServices;
begin
  ServerTimestampSynchronize;
end;

function TSQLHttpClientWebsocketsEx.ServiceMustRegister(const aInterfaces: array of PTypeInfo; aInstanceCreation: TServiceInstanceImplementation; const aContractExpected: RawUTF8): boolean;
begin
  if length (aInterfaces) = 0
     then exit (true);

  result := ServiceRegister(aInterfaces, aInstanceCreation, aContractExpected);
  if not result
    then raise Exception.Create('Unable to register service ' + string (aInterfaces[0].Name));
end;

procedure TSQLHttpClientWebsocketsEx.UpgradeToSockets;
begin
  FUpgradedToSockets := true;
  if not ServerTimestampSynchronize
    then raise Exception.Create('Unable to connect ' + self.LastErrorMessage.AsString);

  if WebSocketsUpgrade(StringToUTF8 (FWebSocketsEncryptionKey)) <> ''
     then raise Exception.Create('Unable to upgrade connection ' + self.LastErrorMessage.AsString);
end;

procedure TSQLHttpClientWebsocketsEx.SetReceiveTimeout(const Value: cardinal);
begin
  FReceiveTimeout := Value;
end;

function TSQLHttpClientWebsocketsEx.SetUser(const AUserName, APassword: string; aHashedPassword: Boolean): boolean;
begin
  result := inherited SetUser (StringToUTF8 (aUserName), StringToUTF8 (aPassword), aHashedPassword);
  TMonitor.Enter(self);
  FUserName := AUserName;
  FPassword := APassword;
  TMonitor.Exit(self);
end;

function TSQLHttpClientWebsocketsEx.ReLogin: boolean;
begin
  TMonitor.Enter(self);
  var U := FUserName;
  var P := FPassword;
  if SecondsSince (LastRelogin) < 5 then
    begin
      TMonitor.Exit(self);
      sleep (2000);
      exit (true);
    end;

  LastRelogin := now;
  TMonitor.Exit(self);

  self.InternalClose;
  result := SetUser (U, P);
  if result then RegisterServices;
end;

function TSQLHttpClientWebsocketsEx.RetrieveListObjArray(var ObjArray; Table: TSQLRecordClass; const FormatSQLWhere: RawUTF8;
         const BoundsSQLWhere: array of const; const aCustomFieldsCSV: RawUTF8): boolean;
var
  arr: TSQLRecordObjArray absolute ObjArray;
begin
  if DummyConnection
   then
     begin
       var RowCount := RandomRange(5, 20);
       ObjArrayClear(arr);
       SetLength(arr, RowCount);
       for var i := 0 to RowCount-1 do
         begin
           arr[i] := Table.Create;
           arr[i].FillDummy;
         end;
       result := true;
     end
   else result := inherited;
end;

{ FKAttribute }

procedure TSqlRecordEx.ApplyDefaults;
var
  dva: DefaultValueAttribute;
begin
  var ctx := TRttiContext.Create;
  try
    var t := ctx.GetType(ClassInfo);
    for var prop in t.GetProperties do
      begin
        dva := prop.GetAttribute <DefaultValueAttribute>;
        if dva <> nil
           then dva.ApplyTo(prop, self);
      end;

  finally
    ctx.Free;
  end;
end;

procedure TSqlRecordEx.CalculateStatistics(const Statistics: TSqlRecordFiltersAndStats);
begin
end;

class function TSqlRecordEx.FiltersAndStats: TSqlRecordFiltersAndStats;
begin
  DebugString := ClassName;
  if dicFiltersAndStats = nil
     then dicFiltersAndStats := TObjectDictionary <TSQLRecordClass, TSqlRecordFiltersAndStats>.Create
                                ([doOwnsValues]);
  if not dicFiltersAndStats.TryGetValue(ClassInfo, result) then
      begin
        result := TSqlRecordFiltersAndStats.Create;
        dicFiltersAndStats.Add(ClassInfo, result);
        if ClassParent.InheritsFrom (TSqlRecordEx) and (ClassParent <> TSqlRecordEx) then
          result.AddFrom (TSqlRecordExClass (ClassParent).FiltersAndStats);
      end;
end;

{$IFDEF DEBUG}
var SqlRecordExID: cardinal = 1;

constructor TSqlRecordEx.Create;
begin
  inherited;
  FCreationID := SqlRecordExID;
  inc (SqlRecordExID);
end;
{$ENDIF}

function TSqlRecordEx.CreateCopy: TSQLRecordEx;
begin
  result := TSQLRecordEx(inherited CreateCopy);
  result.DetailIDs := DetailIDs;

end;

destructor TSqlRecordEx.Destroy;
begin
  if SqlRecords <> nil then
    raise Exception.Create('This record is still in a SqlRecords array!');
  inherited;
end;

procedure TSqlRecordEx.DoBeforeWrite;
begin

end;

function TSqlRecordEx.DoValidateEx(ForImport: boolean): string;
begin
  result := '';
end;

function TSqlRecordEx.Matches(const Filters: TSqlRecordFiltersAndStats): boolean;
begin
  if Filters.Any
    then result := MatchesAny (Filters)
    else result := MatchesAll (Filters);
end;

function TSqlRecordEx.MatchesAll(const Filters: TSqlRecordFiltersAndStats): boolean;
begin
  result := true;
end;

function TSqlRecordEx.MatchesAny(const Filters: TSqlRecordFiltersAndStats): boolean;
begin
  result := Filters.EnabledCount = 0;
end;

class function TSqlRecordEx.ReflectionTriggerSQL: RawUTF8;
//var
//  ctx : TRttiContext;
//  prop: TRttiProperty;
begin
//  ctx := TRttiContext.Create;
//  try
//    for prop in t.GetProperties do
//      begin
//        var attr := prop.GetAttribute<refl>;
//        if (attr <> nil) and (p.PropertyType.TypeKind in [System.tkChar, System.tkString, System.tkLString]) then
//          begin
//            var pf := (attr as PrintableAttribute).PrintField;
//            if pf = '' then
//              pf := p.Name;
//            Fields.AddPair(pf, p.GetValue(self).AsString);
//          end;
//      end;
//  finally
//    ctx.Free;
//  end;
end;

function TSqlRecordEx.Transliterate (const Value: RawUTF8): RawUTF8;
begin
  result := Value;
end;

function TSqlRecordEx.GetDisplayVariant(const PropName: string): variant;
begin
  result := GetFieldVariantValue(PropName);
end;

function TSqlRecordEx.GetDisplayVariant(const fi: TFieldInfo): variant;
begin
  VarClear(result);
  if fi.Name = 'ID'
     then exit(ID);

  var P := fi.MormotFieldInfo;
  if P = nil // ??
    then exit (GetFieldVariant (fi.Name));

  case P.SQLFieldType of
     sftDateTime{, sftDate}:
                   begin
                     P.GetVariant(self,result);

                     var dt: TDateTime := TVarData(Result).VDate;
                     VarClear(result);
                     result := dt;

                     if (result = 0) or ((VarType(Result) = varDate) and (TVarData(Result).VDate <= 0))
                       then varClear (result);
                   end;
     sftCreateTime,
     sftModTime:    begin
                      P.GetVariant(self,result);
                      if result = 0
                        then varClear (result)
                        else result := TimeLogToDateTime (result);
                    end;
     sftUTF8Text:   begin
                      var r: RawUTF8;
                      P.GetValueVar(self,true, r, nil);
                      RawUTF8ToVariant(r, TVarData(result), varUString);
                    end;
     sftTID:        begin
                      P.GetVariant(self,result);
                      if result = 0 then VarClear(result);
                    end;
     sftBoolean:    begin
                      P.GetVariant(self,result);

                    end;
     sftInteger, sftCurrency, sftFloat:
                    begin
                      P.GetVariant(self,result);

                      if fi.IsDate
                        then
                          begin
                            var d: TDateTime := result; // doesnt work with TDate, variant becomes integer type
                            VarClear(result);
                            if (d = 0) and fi.ZeroIsNull
                              then //leave it cleared as null
                              else result := d;
                          end
                        else
                          begin
                            if (result = 0) and fi.ZeroIsNull
                              then VarClear(result);
                          end
                    end
     else P.GetVariant(self,result);
   end; //case
end;

function TSqlRecordEx.GetFieldVariant(const PropName: string): Variant;
begin
  if PropName = 'ID' then result := IDValue else result := inherited;
end;

class function TSqlRecordEx.GetSQLCreate(aModel: TSQLModel): RawUTF8;
begin
  {if IsView
       then result := ViewSQL
            else }result := inherited;
end;

class procedure TSqlRecordEx.InitializeTable(Server: TSQLRestServer; const FieldName: RawUTF8; Options: TSQLInitializeTableOptions);
begin
  if not IsView then inherited;

//  var ctx := TRttiContext.Create;
//  try
//    var t := ctx.GetType(ClassInfo);
//    for var a in t.GetAttributes do
//        if a is ViewAttribute then exit;
//  finally
//    ctx.Free;
//  end;
end;

class procedure TSqlRecordEx.InternalDefineModel(Props: TSQLRecordProperties);
begin
  inherited;
  Props.SetCustomCollationForAll(sftUTF8Text,'WIN32NOCASE');
  //Props.SetCustomCollationForAll(sftUTF8Text,'SYSTEMNOCASE');
end;

class function TSQLRecordHelper.FieldInfoList: TFieldInfoList;
var
  FieldAlignment: TFieldAlignment;
  cl: TClass;
  t: TRttiType;
  fi: TFieldInfo;
begin
  if not dicFieldInfos.TryGetValue (self, result) then
    begin
      var UserTable := TClass(self).ClassParent.InheritsFrom (TSqlAuthUserEx);// SqlTableName = 'ArchUser';
      result := TFieldInfoList.Create (self);

      var ctx := TRttiContext.Create;
      try
        cl := self;
        t := ctx.GetType(cl.ClassInfo);

        fi := TFieldInfo.Create (self, 'ID');
        fi.Brief := ClassHasAttribute(t, IDIsBrief);
        fi.Displayable := fi.Brief;
        result.Add(fi);

        while true do
          begin
            result.IdIsBrief := result.IdIsBrief or ClassHasAttribute(t, IDIsBrief);

            for var prop in t.GetProperties do
              begin
                if prop.Name = 'ID' then continue;// already handled above

                if prop.Name = 'ProfitPerItem' then
                  if now = 0 then exit;

                if result.HasField (prop.Name) then continue;// probably a public property turned published in a view


                var mfield := FieldByName(prop.Name);
//                if mfield = nil
//                   then continue;
                var attr := prop.GetAttribute<DisplayableAttribute>;

                if attr.IsNotAssigned and mfield.IsNotAssigned then continue;

                var findex := FieldIndex (prop.Name);

                if attr = nil
                   then FieldAlignment := TFieldAlignment.None
                   else FieldAlignment := attr.Alignment;

                var index := 0;
                for var f in result do
                  if (f.Alignment < FieldAlignment) or
                     ( (f.Alignment = FieldAlignment) and (f.Index < findex) )
                     then inc (index)
                     else break;

                var ficlass := TFieldInfo;
                var fica := prop.GetAttribute<FieldInfoClassAttribute>;
                if fica.IsAssigned
                   then ficlass := fica.FieldInfoClass; // use an inherited specialised TFieldInfo class e.g. for Enum
                //if prop.is currency hmm tesko?

                fi := ficlass.Create (self, prop, findex, RecordProps.Fields.ByRawUTF8Name(StringToUTF8(prop.Name)));
                if fica.IsAssigned
                   then fi.SetOrigin (fica);
                if UserTable and not fi.Displayable
                   then begin
                          fi.Displayable := (prop.Name = 'LogonName')
                                         or (prop.Name = 'DisplayName')
                                         or (prop.Name = 'GroupRights')
                                         ;
                          fi.Brief := fi.Displayable;
                        end;
                var dfa := DisplayableFieldsAttribute (ClassGetAttribute(t, DisplayableFieldsAttribute));
                if dfa.IsAssigned and dfa.Fields.Contains (prop.Name)
                   then fi.Displayable := true;

                try
                  result.Insert(index, fi);
                except
                  result.Insert(index, fi);
                end;
                if fi.ReadOnly
                   then result.HasReadOnlyFields := true;
                if fi.Brief
                   then result.HasBriefFields := true;
              end;

//            cl := cl.ClassParent;
//            if not cl.InheritsFrom (TSqlRecord) or (ClassParent = TSqlRecord) then break;
              break;
          end;

      finally
        ctx.Free;
      end;
      dicFieldInfos.Add (self, result);
    end;
end;

function TSQLRecordHelper.FieldIsDifferent(OtherRecord: TSqlRecord; const FieldName: RawUTF8): boolean;
begin
  result := OtherRecord.GetFieldVariantValue (FieldName.AsString) <>
            GetFieldVariantValue (FieldName.AsString);
end;

function TSQLRecordHelper.GetModifiedAt: TModTime;
begin
  result := 0;//FieldByName('ModifiedAt').get
end;

procedure TSQLRecordHelper.ApplySubstitute;
var
  sl: TStringList;
  FieldName: RawUTF8;
  Substitute: string;
begin
  FieldName.AsString := SubstituteKeyName;

  if FieldName = '' then exit;
  if not FileExists (SubstitutesFileName) then exit;
  if not dicClassToSubstitutes.TryGetValue(RecordClass, sl) then
    begin
      sl := TStringList.Create;
      sl.LoadFromFile(SubstitutesFileName, TEncoding.UTF8);
      dicClassToSubstitutes.Add(RecordClass, sl);
    end;

  Substitute := sl.Values[GetFieldStringValue(SubstituteKeyName)];
  if Substitute <> '' then SetFieldVariant (SubstituteKeyName, Substitute);
end;

class function TSqlRecordEx.UniqueIfNotNullFields: TStringList;
begin
  if not dicUniqueIfNotNullFields.TryGetValue (self, result) then
    begin
      var sl := TStringList.Create;

      var ctx := TRttiContext.Create;
      try
        var t := ctx.GetType(ClassInfo);

        for var p in t.GetProperties do
          if p.HasAttribute (UniqueIfNotNullAttribute) then
             sl.Add(p.Name);

        dicUniqueIfNotNullFields.Add (self, sl);
        result := sl;
      finally
        ctx.Free;
      end;
    end;
end;

function TSqlRecordEx.Validate(aRest: TSQLRest; const aFields: TSQLFieldBits; aInvalidFieldIndex: PInteger; aValidator: PSynValidate): string;
begin // this is mormot's call stack so we dont care anymore?
  result := inherited;
//  if result = ''
//    then ValidateLocally(0, false)
//  if result = ''
//     then result := ;
end;

(*
function TSqlRecordEx.ValidateLocally(Level: integer; ForImport: boolean;
                            out FirstFailedAttribute: TCustomAttribute; out FailedPropertyName, Error: string) : boolean;
var
  attr: TCustomAttribute;
begin
  result := '';

  var ctx := TRttiContext.Create;
  try
    var TableType := ctx.GetType(self.ClassType);
    for var prop in TableType.GetProperties do
      begin // check index i.e. max length
        attr := PropertyGetAttribute (prop, MormotFKAttribute);
        if (attr <> nil) then
          begin
            var mfka := MormotFKAttribute (attr);

            if not (mfka.SelfReferencingAllowed) and (mfka.SqlRecordClass = RecordClass)
               and IDValue.IsNotNULL and (prop.GetValue(self).AsOrdinal = IDValue)
               then begin
                      FirstFailedAttribute := nil;
                      FailedPropertyName := prop.Name;
                      Error := 'Polje ne može pokazivati na samog sebe';
                      exit;
                    end;
          end;
      end;

    ValidateWithAttributes(Level, ForImport
  finally
  end;
end;
*)

class function TSqlRecordEx.ViewSQL: RawUTF8;
var
  fn, BaseTable, GroupBy, sqlJoin: RawUTF8;
  ctx :TRttiContext;
  Grouped: boolean;
  AddFldsAttr: AdditionalViewFieldsAttribute;
  cva: CustomViewAttribute;
//  attrib: TCustomAttribute;
  TableType: TRttiType;
begin
  if not IsView
     then raise Exception.Create(SqlTableName.AsString + ' is not a view!');

  ctx := TRttiContext.Create;
  try

    cva := ctx.GetAttribute<CustomViewAttribute>(self);
    if cva <> nil then
      exit (cva.sqlJoin);

    // NB parent must also be SqlRecord based on the actual table that is being expanded
    Grouped := false;

    BaseTable := TSqlRecordClass(ClassParent).SqlTableName;
    GroupBy := BaseTable + '.id';

    result := FormatUTF8 ('CREATE VIEW % '#13#10'AS SELECT %, %.ID'#13#10,
              [SqlTableName, TSqlRecordClass(ClassParent).FieldInfoList.FieldListForSelect, BaseTable]);
//    result := result + TSqlRecordClass(ClassParent).FieldInfoList.FieldListForSelect;
    sqlJoin := FormatUTF8 (#13#13'FROM % '#13#10, [BaseTable]);

    TableType := ctx.GetType(self);

    AddFldsAttr := AdditionalViewFieldsAttribute (ClassGetAttribute (TableType, AdditionalViewFieldsAttribute));
    if AddFldsAttr <> nil then result := result + ', '#13#10 + AddFldsAttr.Fields;

    for var attr in TableType.GetAttributes do
      if attr is ViewAttribute then
        begin
          var vat := ViewAttribute(attr);
          if vat.sqlJoin = DUMMY_VIEW_JOIN then continue;

          if attr is GroupedViewAttribute
             then Grouped := true;

          sqlJoin := sqlJoin + vat.sqlJoin + #13;

//          if vat.ForeignTable = ''
//             then ForeignTable := BaseTable
//             else ForeignTable := vat.ForeignTable;
//          if vat.FKName = ''
//             then FKName :=
//             else FKName := vat.FKName;
//
//          if vat.ForeignTableIsDetail
//            then sqlJoin := sqlJoin + FormatUTF8 ('LEFT JOIN % % on %.ID = %.%ID'#13, // e.g. left join book BookAlias on Author.ID = book.AuthorID
//                 [vat.TableName, vat.AliasName, BaseTable, vat.FinalName, ForeignTable])
//            else sqlJoin := sqlJoin + FormatUTF8 ('LEFT JOIN % % on %.ID = %'#13, // e.g. left join Author AuthorAlias on Author.ID = AuthorID
//                 [vat.TableName, vat.AliasName, vat.FinalName, vat.FKName])
        end;

    for var prop in TableType.GetProperties do
      begin
        if prop.Parent <> TableType
          then continue;

        var FFSA := false;

        var temp: RawUTF8;
        temp := '';

        for var attr in prop.GetAttributes do
          if attr is ForeignFieldAttribute then
            begin
              result.AppendSQLComma;
              var ffa := ForeignFieldAttribute(attr);
              if ffa.FullFieldName = ''
                then
                  begin
                    fn := ffa.FieldName;
                    if fn = '' then fn := StringToUTF8 (prop.Name);
                    temp := FormatUTF8 ('%.%', [ffa.TableName, fn]);
                  end
                else
                  begin
                    fn := ffa.FullFieldName;
                    temp := fn;
                  end;
              if prop.Name <> fn.AsString
                 then temp.AppendSQL (' ' + StringToUTF8 (prop.Name));
              result.AppendSQL(temp);
              FFSA := FFSA or (attr is ForeignFieldSummAttribute);
            end;

        if Grouped and not FFSA and (prop.Visibility = TMemberVisibility.mvPublished) then
            begin
              if GroupBy <> ''
                 then GroupBy := GroupBy + ', ';
              if prop.Name <> fn.AsString
                then GroupBy := GroupBy + StringToUTF8 (prop.Name)
                else GroupBy := GroupBy + fn;
            end;
          end;

  finally
    ctx.Free;
  end;

  result := result + #13 + sqlJoin;

  if Grouped then
    begin
      GroupBy := #13'GROUP BY ' + GroupBy;
//      for var i := 1 to RecordProps.Fields.Count do
//        begin
//          if i > 1 then GroupBy := GroupBy + ', ';
//          GroupBy := GroupBy + StringToUTF8 (IntToStr (i));
//        end;
      result := result + GroupBy;
    end;
end;

{ ViewAttribute }

//constructor ViewAttribute.Create (const ATableName: RawUTF8; const AAliasName: RawUTF8 = ''; AFKName: RawUTF8 = ''; AForeignTableIsDetail: boolean = false);
//begin
//  TableName := ATableName;
//  AliasName := AAliasName;
//  ForeignTableIsDetail := AForeignTableIsDetail;
//  if AliasName = ''
//     then FinalName := TableName
//     else FinalName := AliasName;
//  if AFKName = ''
//     then FKName := FinalName + 'ID'
//     else FKName := AFKName
//end;

constructor TFieldInfo.Create (ASqlRecordClass: TSqlRecordClass; AName: string);
begin
  inherited Create;
  SqlRecordClass := ASqlRecordClass;
  Name := AName;
end;

function TFieldInfo.DelphiType: string;
begin
  if IsTextual
     then result := 'string'
  else if IsForeignKey or (Name = 'ID')
     then result := 'TID'
  else if IsBoolean
     then result := 'boolean'
  else if IsEnumeratedType
     then result := TFieldInfoEnum(self).TypeName
  else if IsDate
     then result := 'TDateTime'
  else case MormotFieldInfo.SQLFieldType of
    sftFloat, sftCurrency: result := 'double';
    sftDateTime: result := 'TDateTime'
    else raise Exception.Create('delphi type?');
    end;
end;

constructor TFieldInfo.Create (ASqlRecordClass: TSqlRecordClass; prop: TRttiProperty; AIndex: integer; AMormotFieldInfo: TSQLPropInfo);
begin
  inherited Create;

  SqlRecordClass := ASqlRecordClass;
  Name := prop.Name;
  MormotFieldInfo := AMormotFieldInfo;

  if MormotFieldInfo.IsAssigned
    then IsBoolean := MormotFieldInfo.SQLFieldType = TSQLFieldType.sftBoolean;
         // hmph... prop.PropertyType.TypeKind = TTypeKind.tkEnumeration

  if MormotFieldInfo.IsAssigned
    then IsEnumeratedType := MormotFieldInfo.SQLFieldType = TSQLFieldType.sftEnumerate
    else IsEnumeratedType := not IsBoolean and (prop.PropertyType.TypeKind = System.TTypeKind.tkEnumeration);

  if IsEnumeratedType and not (self is TFieldInfoEnum)
     then raise Exception.Create('Enumerated type ' + ASqlRecordClass.SqlTableName.AsString + '.' + prop.Name + ' not TFieldInfoEnum');

  IsTextual := prop.PropertyType.TypeKind in [System.TTypeKind.tkString, System.TTypeKind.tkWChar,
                                              System.TTypeKind.tkLString, System.TTypeKind.tkWString,
                                              System.TTypeKind.tkUString];
  Index := AIndex;
  IsCalculated := prop.HasAttribute(CalculatedFieldAttribute);
  var attr := prop.GetAttribute<DisplayableAttribute>;
  Displayable := attr <> nil;
  if Displayable
    then
      begin
        DisplayName := attr.DisplayName;
        Alignment := attr.Alignment;
      end
    else
      begin
        Alignment := TFieldAlignment.None;
      end;
  if DisplayName = ''
     then DisplayName := Name;
//  if Name = 'Name' then
//     if now = 0 then exit;

  var fka := prop.GetAttribute<MormotFKAttribute>;
  if fka <> nil then
    begin
      isForeignKey := true;
      ForeignTableName := fka.SqlRecordClass.SQLTableName.AsString;
    end;

  Brief            := prop.HasAttribute(BriefAttribute);
  ReadOnly         := prop.HasAttribute(ReadOnlyAttribute);
  NotNull          := prop.HasAttribute(NotNullAttribute);
  ZeroIsNull       := prop.HasAttribute(ZeroIsNullAttribute);
  IsDate           := prop.HasAttribute(DateAttribute);
  IsTime           := prop.HasAttribute(TimeAttribute);
  Indexed          := IsForeignKey or prop.HasAttribute(DBIndexedAttribute);
  HasTranslitCopy  := prop.HasAttribute(HasTranslitCopyAttribute);
  IsAlternateUK    := prop.HasAttribute(AlternateUK);
  IsGeneralFilter  := prop.HasAttribute(GeneralFilterAttribute);
  IsReportQualifier:= prop.HasAttribute(ReportQualifierAttribute);
  IsUnique:=          prop.HasAttribute(UniqueIfNotNullAttribute)
                      or (MormotFieldInfo.IsAssigned and (aIsUnique in MormotFieldInfo.Attributes));
  ShouldTranslit   := IsTextual and not HasTranslitCopy;

  var ffa := prop.GetAttribute<ForeignFieldAttribute>;
  if ffa <> nil then
    begin
      ForeignTableName := ffa.TableName.AsString;
      if ffa.ForeignKeyName.IsEmpty
         then ForeignKeyName := ForeignTableName + 'ID'
         else ForeignKeyName := ffa.ForeignKeyName.AsString;

      var fiForeign: TFieldInfo;

      // try to use field name as specified in attribute, if there is one
      var fnTemp := ffa.FieldName.AsString;
      if fnTemp.IsEmpty and not ffa.FullFieldName.IsEmpty
        then begin
               fnTemp := ffa.FullFieldName.AsString;
               if fnTemp.Contains ('.')
                  then
                    begin
                      var temp := fnTemp.Split (['.']);
                      fnTemp := temp[1];
                    end;
             end;
      if fnTemp.IsEmpty then fnTemp := Name;

      fiForeign := ffa.SQLRecordClass.FieldInfoList.ItemsByName[fnTemp];
      ZeroIsNull := ZeroIsNull or fiForeign.ZeroIsNull;

      if fiForeign.IsNotAssigned then
         raise Exception.Create(format ('Table %s does not have field %s', [ffa.SQLRecordClass.SQLTableName.AsString,
                                        fnTemp]));
      IsPlain := not fiForeign.IsUnique;
      //ffa. add FK name?
    end
    else IsPlain := true;

  var attUINotNull := prop.GetAttribute<UINotNull>;
  if attUINotNull <> nil then
    begin
      IsUINotNull := true;
      UINotNullInfo := attUINotNull.Info;
    end;

  var ssfa := prop.GetAttribute<SubstituteSortFieldAttribute>;
  if ssfa <> nil then
    SubstituteSortField := ssfa.FieldName;
end;


{ TFieldInfoList.TFieldInfoListEnum }

constructor TFieldInfoList.TFieldInfoListEnum.Create (const AOwner: TFieldInfoList);
begin
  inherited Create;
  FOwner := AOwner;
  FCurrent := -1;
end;

destructor TFieldInfoList.TFieldInfoListEnum.Destroy;
begin
  inherited;
end;

function TFieldInfoList.TFieldInfoListEnum.GetCurrent: TFieldInfo;
begin
  result := FOwner [FCurrent];
end;

function TFieldInfoList.TFieldInfoListEnum.GetEnumerator: TFieldInfoListEnum;
begin
  result := self;
end;

function TFieldInfoList.TFieldInfoListEnum.MoveNext: Boolean;
begin
  inc (FCurrent);
  result := FCurrent < FOwner.Count;
end;

{ TFieldInfoList }

function TFieldInfoList.Add(Item: TFieldInfo): integer;
begin
  FDicNameToItem.Add(Item.Name.ToLower, Item);
  result := inherited Add (Item);
end;

function TFieldInfoList.All: TFieldInfoListEnum;
begin
  result := TFieldInfoListEnum.Create (self);
end;

function TFieldInfoList.CalculatedFields: RawUTF8;
begin
  result := '';
  for var f in self do
    if f.IsCalculated then
       begin
         if result <> '' then result := result + ',';
         result := result + SqlRecordClass.SQLTableName + '.' + StringToUTF8(f.Name);
       end;
end;

constructor TFieldInfoList.Create (ASqlRecordClass: TSqlRecordClass);
begin
  inherited Create;
  SqlRecordClass := ASqlRecordClass;
  FDicNameToItem := TObjectDictionary<string, TFieldInfo>.Create ([doOwnsValues]);
end;

destructor TFieldInfoList.Destroy;
begin
  FDicNameToItem.Free;
  inherited;
end;

function TFieldInfoList.Displayable: TFieldInfoListEnum;
begin
  result := TFieldInfoListEnumDisplayable.Create (self);
end;

function TFieldInfoList.FieldListForSelect: RawUTF8;
begin
  var c := CalculatedFields;
  result := SqlRecordClass.FieldListForSelect;
  if not c.IsEmpty
     then result := result + ', ' +  c;
end;

function TFieldInfoList.GeneralFilter: TFieldInfoListEnum;
begin
  result := TFieldInfoListEnumGeneralFilter.Create (self);
end;

function TFieldInfoList.GetItemsByName(index: string): TFieldInfo;
begin
  result := nil;
  FDicNameToItem.TryGetValue (index.ToLower, result);
end;

function TFieldInfoList.HasField(const Name: string): boolean;
begin
  result := ItemsByName[Name].IsAssigned;
end;

procedure TFieldInfoList.Insert(index: integer; Item: TFieldInfo);
begin
  FDicNameToItem.Add(Item.Name.ToLower, Item);
  inherited Insert (index, Item);
end;

function TFieldInfoList.NotNull: TFieldInfoListEnum;
begin
  result := TFieldInfoListEnumNotNull.Create (self);
end;

function TFieldInfoList.WritableFieldsAsCSV: RawUTF8;
begin
  result := '';
  for var field in All do
    if field.ReadOnly then

end;

{ TFieldInfoList.TFieldInfoListEnumDisplayable }

function TFieldInfoList.TFieldInfoListEnumDisplayable.MoveNext: Boolean;
begin
  repeat
    result := inherited;
  until not result or FOwner[FCurrent].Displayable;
end;

{ TFieldInfoList.TFieldInfoListEnumNotNull }

function TFieldInfoList.TFieldInfoListEnumNotNull.MoveNext: Boolean;
begin
  repeat
    result := inherited;
  until not result or FOwner[FCurrent].NotNull;
end;


//function TFieldDisplayNames.Get (SqlRecordClass: TSQLRecordClass; FieldName: string): string;
//begin
//  TryGetValue (SqlRecordClass.SQLTableName.AsString + '.' + FieldName, result);
//  if (result = '')
//     then TryGetValue (FieldName, result);
//end;

{ CrudPermissionsAttribute }

//constructor CrudPermissionsAttribute.Create(const AOperations: array of TCrud; const ARequiredLevels: array of integer);
//begin
//  Items := TDictionary <TCrud, integer>.Create;
//  for var i := 0 to length(AOperations) do
//    Items.Add (AOperations[i], ARequiredLevels[i]);
//end;

{ TModelTranslatableContent }

//function TModelTranslatableContent.CustomContent(const Identifier, DefaultValue: string): string;
//begin
//
//end;
//
//function TModelTranslatableContent.EnumDisplayValue(const EnumType, EnumValue: string): string;
//begin
//  if self = nil then exit (EnumValue);
//  if not TryGetValue(EnumType + '.' + EnumValue, result)
//     then begin
//            AddOrSetValue (EnumType + '.' + EnumValue,  '');
//            result := EnumValue;
//          end;
//end;
//
//function TModelTranslatableContent.FieldDisplayName(SqlRecordClass: TSQLRecordClass; FieldName: string): string;
//begin
//
//end;

{ AdditionalViewFieldsAttribute }

procedure TRestrictedMirrorFields.Add (TableClass: TSqlRecordClass; rcl : TRestrictedMirrorField);
var
  lst: TRestrictedMirrorFieldList;
begin
  if not TryGetValue(TableClass, lst) then
     begin
       lst := TList<TRestrictedMirrorField>.Create;
       inherited Add (TableClass, lst);
     end;
  lst.Add (rcl);
end;

{ TRestrictedMirrorField }

function TRestrictedMirrorField.FullForeignKeyName: RawUTF8;
begin
  result := TableClass.SQLTableName + '.' + ForeignKeyName;
end;

function TRestrictedMirrorField.FullMirrorFieldName: RawUTF8;
begin
  result := TableClass.SQLTableName + '.' + MirrorFieldName;
end;

procedure TSqlRecordFiltersAndStats.ClearValues;
begin
  var v: variant;
  varclear (v);
  for var i := 0 to Count - 1 do
    Items[i].StatisticValue := v;
end;

procedure TSqlRecordFiltersAndStats.IncValue(const index: integer);
var
  value: variant;
begin
  var item := Items[index];
  value := item.StatisticValue;
  if VarIsNull (value)
    then item.StatisticValue := 1
    else item.StatisticValue := value + 1;
end;

procedure TSqlRecordFiltersAndStats.IncValue(const index: integer; const Amount: variant);
begin
  var item := Items[index];
  if VarIsEmptyOrNull (item.StatisticValue)
    then begin
           if item.VariantType <> 0
              then TVarData(item.StatisticValue).VType := item.VariantType;
           item.StatisticValue := Amount;
         end
    else item.StatisticValue := item.StatisticValue + Amount;
end;

procedure TSqlRecordFiltersAndStats.SetValues(index: integer; const Value: variant);
begin
  Items[index].StatisticValue := Value;
end;

function TSqlRecordFiltersAndStats.Toggle(index: integer): boolean;
begin
  result := not Items[index].Enabled;
  Items[index].Enabled := result;
  if result
    then inc (EnabledCount)
    else dec (EnabledCount);
end;

{ TSqlRecordFilters }

function TSqlRecordFiltersAndStats.Add (const Name, StatisticName: string; VariantType: integer = 0;
                                        const Enabled: boolean = false; const Visible: boolean = true): integer;
begin
  result := Count;
  var Item := TSqlRecordFilterAndStat.Create;
  Item.Name := Name;
  Item.StatisticName := StatisticName;
  Item.Enabled := Enabled;
  Item.VariantType := VariantType;
  Item.Visible := Visible;
  Item.Index := Count;
  VarClear(Item.StatisticValue);

  inherited Add (Item);
end;

procedure TSqlRecordFiltersAndStats.Assign(fas: TSqlRecordFiltersAndStats);
begin
  Clear;
  EnabledCount := 0;
  AddFrom (fas);
end;

procedure TSqlRecordFiltersAndStats.AddFrom(fas: TSqlRecordFiltersAndStats);
begin
  for var fs in fas do
    Add (fs.Name, fs.StatisticName, fs.VariantType, fs.Enabled, fs.Visible);
  EnabledCount := EnabledCount + fas.EnabledCount;
end;

constructor TSqlRecordFiltersAndStats.Create;
begin
  inherited Create (true);
  Any := true;
end;

function TSqlRecordFiltersAndStats.GetValues(index: integer): variant;
begin
  result := Items[index].StatisticValue;
end;

{ TCrudPermissions }

function TCrudPermissions.GetValues(index: TCrud): TAuthGroupID;
begin
  if not TryGetValue(index, result)
    then result := NULL_ID;
end;

function TCrudPermissions.ToRequiredAuthLevels: TRequiredAuthLevels;
begin
  for var bc := TBasicCRUD.Create to TBasicCRUD.Delete do
    result[bc] := self[BasicCrudToCrud[bc]];
end;

function TFieldInfo.GetDisplayName: string;
begin
  if FDisplayName.IsEmpty
     then result := Name
     else result := FDisplayName;
end;

function TFieldInfo.GetDisplayString(rec: TSqlRecord): string;
begin
  if rec.IsNotAssigned then exit ('');
  result := GetDisplayVariant(rec);
end;

function TFieldInfo.GetDisplayVariant(rec: TSqlRecord): variant;
begin
  if MormotFieldInfo.IsNotAssigned
    then if rec.IDValue.IsNULL
      then VarClear(result)
      else result := rec.IDValue
    else case MormotFieldInfo.SQLFieldType of
       sftDateTime{, sftDate}:
                     begin
                       MormotFieldInfo.GetVariant(rec, result);

                       var dt: TDateTime := TVarData(Result).VDate;
                       VarClear(result);
                       result := dt;

                       if (result = 0) or ((VarType(Result) = varDate) and (TVarData(Result).VDate <= 0))
                         then varClear (result);
                     end;
       sftCreateTime,
       sftModTime:    begin
                        MormotFieldInfo.GetVariant(rec, result);
                        if result = 0
                          then varClear (result)
                          else result := TimeLogToDateTime (result);
                      end;
       sftUTF8Text:   begin
                        var r: RawUTF8;
                        MormotFieldInfo.GetValueVar(rec, true, r, nil);
                        RawUTF8ToVariant(r, TVarData(result), varUString);
                      end;
       sftTID:        begin
                        MormotFieldInfo.GetVariant(rec, result);
                        if result = 0 then VarClear(result);
                      end;
       sftInteger, sftCurrency, sftFloat:
                      begin
                        MormotFieldInfo.GetVariant(rec, result);
                        if (result = 0) and ZeroIsNull
                          then VarClear(result);
                      end
       else MormotFieldInfo.GetVariant(rec, result);
     end; //case
//  result := rec.GetFieldVariantDisplayValue(Name);
//  FieldIndex
end;

function TFieldInfo.GetNameForFilter: string;
begin
  if FNameForFilter <> ''
     then Result := FNameForFilter
     else if (ForeignTableName <> '') and not IsPlain
          then result := ForeignTableName + 'ID'
          else if HasTranslitCopy
               then result := Name + '_TL'
               else result := Name;
end;

procedure TFieldInfo.SetOrigin(Origin: FieldInfoClassAttribute);
begin

end;

{ TFieldInfoCurrency }

function TFieldInfoCurrency.GetDisplayString(rec: TSqlRecord): string;
var
  v: variant;
begin
  MormotFieldInfo.GetVariant(rec,v);
  if ZeroIsNull and (v = 0)
     then result := ''
     else result := CurrToStrF(v, ffNumber, 0)  //CurrToStrF(v, ffCurrency, 0);
end;

{ FieldInfoClassAttribute }

constructor FieldInfoClassAttribute.Create(AFieldInfoClass: TFieldInfoClass);
begin
  inherited Create;
  FFieldInfoClass := AFieldInfoClass;
end;

{ TFieldInfoEnum }

function TFieldInfoEnum.GetDisplayString(rec: TSqlRecord): string;
var
  v: variant;
  lst: TListOfStringPointers;
begin
  lst := nil;
  dicEnumToDisplayValues.TryGetValue(TypeName, lst);

  if rec.IsNotAssigned
    then if lst.IsAssigned
         then exit (dicEnumToDisplayValues[TypeName][0]^)
         else exit ('');

  MormotFieldInfo.GetVariant(rec,v);
  try
    if lst <> nil
       then result := lst[integer(v)]^
       else result := integer(v).ToString;
//    if (integer(v) = 0) and ( pos ('|', result) > 0)
//      then
//        if ZeroIsAll
//          then
  except
    result := TypeName + 'does not have enum ID=' + v.ToString;
  end;
end;

//function TFieldInfoEnum.TypeName: string;
//begin
//  result := FieldInfoEnumAttribute(Origin).EnumTypeName;
//end;

{ FieldInfoEnumAttribute }

//procedure FieldInfoEnumAttribute.SetStrings (Strings: array of string);
//begin
//  for var i := Low (Strings) to High (Strings) do
//      FList.Add (@Strings[i]);
//end;

constructor FieldInfoEnumAttribute.Create (const AEnumTypeName: string);
begin
  inherited Create (TFieldInfoEnum);
  EnumTypeName := AEnumTypeName;
//  SetStrings (dicEnumToDisplayValues[EnumTypeName]^);
end;

procedure TFieldInfoEnum.SetOrigin(Origin: FieldInfoClassAttribute);
begin
  inherited;

  if Origin is FieldInfoEnumAttribute
     then TypeName := FieldInfoEnumAttribute(Origin).EnumTypeName;
end;

{ TSqlAuthUserEx }

function TSqlAuthUserEx.Validate: string;
begin
  if length (LogonName) < 8 then exit (mmStrings[mmsUserNameTooShort]);
  if length (DisplayName) < 2 then exit (mmStrings[mmsUserDisplayNameTooShort]);
  if Self.GroupRightsID.IsNULL then exit (mmStrings[mmsGroupIsMandatory]);
end;

function RegisterMMM (const s: string): integer;
begin
  result := length (mmStrings);
  SetLength (mmStrings, result + 1);
  mmStrings[result] := s;
end;

{ TSQLSampleRecordList }

function TSQLSampleRecordList.SampleFileName: string;
begin
  result := TPath.Combine (TPath.Combine (ApplicationExeFolder, 'SampleData'), Table.SQLTableName.AsString + '.txt');
end;

procedure TSQLSampleRecordList.Load;
begin
  {$IFDEF DEBUG}
  if FileExists (SampleFileName)
    then LoadJson (SampleFileName );
  {$ENDIF}
end;

procedure TSQLSampleRecordList.Save;
begin
  {$IFDEF DEBUG}
  ForceDirectories (TPath.Combine (ApplicationExeFolder, 'SampleData'));
  SaveJson (SampleFileName);
  {$ENDIF}
end;

{ TSQLAuthGroupEx }
{$IFDEF SERVER}

class procedure TSQLAuthGroupEx.InitializeTable (Server: TSQLRestServer; const FieldName: RawUTF8;
                                                 Options: TSQLInitializeTableOptions);
begin
  inherited InitializeTable(Server, FieldName, Options + [itoNoAutoCreateGroups, itoNoAutoCreateUsers]);

  if (Server<>nil) and (FieldName='') then
    if Server.HandleAuthentication then
      begin
        if not (itoNoAutoCreateGroups in Options)
           then CreateDefaultGroups (Server);
        var user := TSqlModelEx(Server.Model).AuthUserClass.Create;
        if not (itoNoAutoCreateUsers in Options)
           and (
               (Server.TableRowCount (Server.SQLAuthUserClass) = 0)
//               {$IFDEF DEBUG}or not Server.Retrieve('Developer=1', user){$ENDIF}
               )
               then CreateDefaultUsers (Server);
        user.Free;
      end;
end;

class procedure TSQLAuthGroupEx.CreateDefaultGroups (Server: TSQLRestServer);
var G: TSQLAuthGroupEx;
    A: TSQLAccessRights;
    AuthUserIndex, AuthGroupIndex: integer;
begin
  AuthGroupIndex := Server.Model.GetTableIndexExisting(Server.SQLAuthUserClass);
  AuthUserIndex := Server.Model.GetTableIndexExisting(Server.SQLAuthGroupClass);
  G := TSqlModelEx(Server.Model).AuthGroupClass.Create; //Server.SQLAuthGroupClass.Create;
    try
      const Non_Admin_Session_Timeout = 60;

      A := FULL_ACCESS_RIGHTS;
      G.Ident := 'Administrator';
      G.Description := 'Administracija sistema';
      G.SQLAccessRights := A;
      G.SessionTimeout := AuthAdminGroupDefaultTimeout;
      G.IDValue := 1;
      Server.Add(G,true, true);

      G.Ident := 'Supervizor';
      G.Description := 'Supervizor';
      A.AllowRemoteExecute := SUPERVISOR_ACCESS_RIGHTS.AllowRemoteExecute;
      A.Edit(AuthUserIndex,[soSelect]); // AuthUser  R/O
      A.Edit(AuthGroupIndex,[soSelect]); // AuthGroup R/O
      G.SQLAccessRights := A;
      G.IDValue := 2;
      Server.Add(G,true, true);

      Exclude(A.AllowRemoteExecute,reSQLSelectWithoutTable);
      Exclude(A.GET,AuthUserIndex); // no Auth R
      Exclude(A.GET,AuthGroupIndex);
      A.AllowRemoteExecute := [reService];
      FillcharFast(A.POST,SizeOf(TSQLFieldTables),0); // R/O access
      FillcharFast(A.PUT,SizeOf(TSQLFieldTables),0);
      FillcharFast(A.DELETE,SizeOf(TSQLFieldTables),0);
      G.SQLAccessRights := A;
      G.IDValue := 3;
      G.SessionTimeout := Non_Admin_Session_Timeout;

      G.Ident := 'Korisnik';
      G.Description := 'Običan korisnik';
      Server.Add(G, true, true);
    finally
      G.Free;
    end;
end;


class function TSQLAuthGroupEx.DefaultDevPassword(const GroupID: TID): RawUTF8;
begin
  result := '789456';
end;

class procedure TSQLAuthGroupEx.CreateDefaultUsers (Server: TSQLRestServer);
var
    U: TSqlAuthUserEx;
begin
  U := TSqlModelEx(Server.Model).AuthUserClass.Create;
  U.Active := true;
  U.Developer := true;

  try
    U.LogonName := 'DevAdmin';
    U.PasswordPlain := DefaultDevPassword(GROUP_ID_ADMIN);
    U.DisplayName := U.LogonName;
    U.GroupRightsID := GROUP_ID_ADMIN;
    Server.Add(U,true);

    U.LogonName := 'DevSupervisor';
    U.DisplayName := U.LogonName;
    U.GroupRightsID := GROUP_ID_SUPERVISOR;
    Server.Add(U,true);

    U.LogonName := 'DevUser';
    U.DisplayName := U.LogonName;
    U.GroupRightsID := GROUP_ID_USER;
    Server.Add(U,true);
  finally
    U.Free;
  end;
end;

{$ENDIF}

class function TSQLAuthGroupEx.DefaultPassword (const GroupID: TID): RawUTF8;
begin
  result := '12345';
end;

{ TFieldInfoList.TFieldInfoListEnumGeneralFilter }

function TFieldInfoList.TFieldInfoListEnumGeneralFilter.MoveNext: Boolean;
begin
  repeat
    result := inherited;
  until not result or FOwner[FCurrent].IsGeneralFilter;
end;

{ TFieldInfoTableName }

function TFieldInfoTableName.GetDisplayString(rec: TSqlRecord): string;
var
  v: variant;
//  s: string;
begin
  MormotFieldInfo.GetVariant(rec,v);
  {if false and (Translator <> nil)
     then begin
            var s := Translator.TranslateDBTable(v);
            if s = ''
               then result := VarToStr (v)
               else result := VarToStr (v) + ' (' + Translator.TranslateDBTable(v) + ')'
          end
     else }result := v;
end;

{ FieldInfoTableNameAttribute }

constructor FieldInfoTableNameAttribute.Create;
begin
  FFieldInfoClass := TFieldInfoTableName;
end;

{ TFieldInfoBoolean }

function TFieldInfoBoolean.GetDisplayString(rec: TSqlRecord): string;
var
  v: variant;
begin
  MormotFieldInfo.GetVariant(rec,v);
  try
    result := dicEnumToDisplayValues[TypeName][ord(boolean(v))]^;
//    if (integer(v) = 0) and ( pos ('|', result) > 0)
//      then
//        if ZeroIsAll
//          then
  except
    result := TypeName + 'does not have enum ID=' + v.ToString;
  end;
end;

procedure TFieldInfoBoolean.SetOrigin(Origin: FieldInfoClassAttribute);
begin
  inherited;

  if Origin is FieldInfoBooleanAttribute
     then TypeName := FieldInfoEnumAttribute(Origin).EnumTypeName;
end;

{ FieldInfoBooleanAttribute }

constructor FieldInfoBooleanAttribute.Create(const ASubTypeName: string);
begin
  inherited Create (TFieldInfoBoolean);
  SubTypeName := ASubTypeName;
end;

{ TRecordDistances }

function TRecordDistances.Add(rec: TSqlRecord; Distance: integer): integer;
begin
  var pair := TPair<TSqlRecord, Integer>.Create(rec, Distance);
  result := inherited Add(pair);
end;

procedure TRecordDistances.Sort;
begin
  inherited Sort(
    TComparer<TPair<TSqlRecord, Integer>>.Construct(
      function(const Left, Right: TPair<TSqlRecord, Integer>): Integer
      begin
        Result := Left.Value - Right.Value;
      end
    )
  );
end;


function DateTimeToSqlString (const ADateTime: TDateTime): string;
begin
  result := SqlString(DateTimeToStr(ADateTime, FSMormot));
end;

function DateToSqlString (const ADate: TDate): string;
begin
  result := SqlString(DateToStr(ADate, FSMormot));
end;

function DocVariant (const NameValuePairs: array of const): variant;
begin
  result := _ObjFast(NameValuePairs);
end;


initialization
//  {$ifdef milos}
  // PG naizgled uvek koristi US format, ali nisam siguran, kod mene je tako
  // ovo sluzi za pretvaranje stringa koji je PG napravio na osnovu broja, u string aplikacije
  FSMormot := TFormatSettings.Create;
  FSMormot.DecimalSeparator := '.';
  FSMormot.ThousandSeparator := ',';
  FSMormot.ListSeparator := '|';

  FSMormot.DateSeparator := '-';
  FSMormot.ShortDateFormat := 'YYYY-mm-dd';

  slServerErrorMessages := TStringList.Create;

  SQLRecordLists := TSQLRecordLists.Create;
  SQLRecordLists.OwnsObjects := false;

  seNoError                     := RegisterServerError ('');
  seUnknown                     := RegisterServerError ('Nepoznata greska');
  seConnectionFailed            := RegisterServerError ('Povezivanje nije uspelo');
  seLoginFailed                 := RegisterServerError ('Prijava nije uspela');
  seMandatoryServiceUnavailable := RegisterServerError ('Mandatory service unavailable');
  seUserRecordNotFound          := RegisterServerError ('User record not found');
  seRecordAlreadyLocked         := RegisterServerError ('Record already locked');
  seCascadeRestriction          := RegisterServerError ('Izmena sloga nije dozvoljena jer na njega ukazuje drugi slog');
  seMirroredFieldRestriction    := RegisterServerError ('Izmena nije dozvoljena, postoje povezani slogovi sa reflektovanim poljima u drugoj tabeli');
  seRestrictedField             := RegisterServerError ('Izmena polja nije dozvoljena, postoje povezani slogovi u drugoj tabeli');
  seIncorrectMirrorValue        := RegisterServerError ('Izmena nije dozvoljena, reflektovano polje nema odgovarajuću vrednost');
  seRecordNotFound              := RegisterServerError ('Record not found');
  seUnknownTable                := RegisterServerError ('Unknown table');
  seNoRecordAccess              := RegisterServerError ('No record access');
  seBatchFailed                 := RegisterServerError ('Batch failed');
  seRecordNotLocked             := RegisterServerError ('Record not locked');
  seRecordAlreadyLockedByMormot := RegisterServerError ('Record already locked by Mormot');
  seRecordChangeNotAllowed      := RegisterServerError ('Record change not allowed');
  seNotImplemented              := RegisterServerError ('Not implemented');
  seDetailTableOperationFailed  := RegisterServerError ('Detail Table Operation Failed');
  seException                   := RegisterServerError ('Exception');
  seWrongCRUD                   := RegisterServerError ('Wrong CRUD');
  seNotRecordOwner              := RegisterServerError ('Not Record Owner');
  seDetailsMissing              := RegisterServerError ('Details Missing');
  seValidationFailed            := RegisterServerError ('Validation failed');
  seAddFailed                   := RegisterServerError ('Add failed');
  seUpdateFailed                := RegisterServerError ('Update failed');
  seInvalidParameters           := RegisterServerError ('Invalid Parameters');
  seRecordValueNotFound         := RegisterServerError ('Record Value Not Found');
  seFileNotFound                := RegisterServerError ('File Not Found');
  seDuplicateKey                := RegisterServerError ('Duplicate Key');
  seDBFunctionFailed            := RegisterServerError ('DB Function Failed');
  seExpLookupNotDefined         := RegisterServerError ('Expanded lookup not defined');
  sePermissionDenied            := RegisterServerError ('Permission denied');
  seSimulatedError              := RegisterServerError ('Simulated error');

  SocketNeverRetries := true;

  dicDefaultSortFields     := TDictionary <TSQLRecordClass, RawUTF8>.Create;
  dicGeneralFilters        := TObjectDictionary <TSQLRecordClass, TGeneralFilterFields>.Create ([doOwnsValues]);
  dicFieldInfos            := TDictionary <TSQLRecordClass, TFieldInfoList>.Create;
  dicUniqueIfNotNullFields := TDictionary <TSQLRecordClass, TStringList>.Create;
  dicTableGroupPermissions := TDictionary <TSQLRecordClass, TCrudPermissions>.Create;
  dicClassToTrim           := TDictionary <TSQLRecordClass, boolean>.Create;
  dicEnumToDisplayValues   := TDictionary <string, TListOfStringPointers>.Create;
  dicClassToSampleRecords  := TObjectDictionary<TSqlRecordClass, TSqlSampleRecordList>.Create ([doOwnsValues]);
  dicClassToSubstitutes    := TObjectDictionary<TSqlRecordClass, TStringList>.Create ([doOwnsValues]);

  mmsUserNameTooShort := RegisterMMM ('Korisničko ime mora imati bar 8 karaktera.');
  mmsUserDisplayNameTooShort := RegisterMMM ('Korisničko puno ime mora imati bar 2 karaktera.');
  mmsPaswordTooShort  := RegisterMMM ('Lozinka mora imati bar 8 karaktera.');
  mmsGroupIsMandatory := RegisterMMM ('Korisnička grupa je obavezna.');

finalization
  SQLRecordLists.Free;
  slServerErrorMessages.Free;
  dicDefaultSortFields.Free;
  dicGeneralFilters.Free;

  for var pair in dicFieldInfos do
    pair.Value.Free;
  dicFieldInfos.Free;

  for var pair in dicUniqueIfNotNullFields do
    pair.Value.Free;
  dicUniqueIfNotNullFields.Free;
  for var value in dicTableGroupPermissions.Values do value.Free;
  dicTableGroupPermissions.Free;
  dicFiltersAndStats.Free;
  dicClassToTrim.Free;
  dicEnumToDisplayValues.Free;
  dicClassToSampleRecords.Free;
  dicClassToSubstitutes.Free;

end.

