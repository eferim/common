unit Controller.Tasks;


// unit for tasks that do not require specific NotifyXYZ calls

interface

uses SysUtils, Classes, Generics.Collections, System.Contnrs,
     mORMot, SynCommons,
     MormotTypes, MormotMods, SqlRecords, ObservableController, Common.Interfaces;

type

  TPingTask = class (TObservableTask)
  protected
    procedure DoExecute; override;
  end;

  TLogoutTask = class (TObservableTask)
    procedure DoExecute; override;
  end;

  TSetMySettingsTask = class (TObservableTask)
  protected
    varSettings: variant;
    procedure DoExecute; override;
  public
    constructor Create (AController: TObservableController; ASettings: variant; AInitiator: IServerObserver = nil); reintroduce;
  end;

  ILookupObserver  = interface (IServerObserver)
  ['{0DBCC7CD-357F-4B60-B509-90A2C4FB4B44}']
    procedure ObserveLookup(Task: TObservableLookupTask);
  end;

  TGetPublicFileTask = class (TObservableTask)
  protected
    procedure DoExecute; override;
  public
    Stream: TStream;
    FileName: string;
    destructor Destroy; override;
  end;

  TRecordsCreateTask = class (TObservableTask)
  protected
    procedure DoExecute; override;
  public
    Records: TArray<TSqlRecord>;
    Fields: RawUTF8;
  end;

  TCustomDBStatsTask = class (TObservableTask)
  protected
  public
    Tables: TRawUTF8DynArray;
    Counts: TIDDynArray;
    RequiredAuthLevels: TRequiredAuthLevelsArray;
  end;

  TReportFileTask = class (TObservableTask)
  public
    Stream: TStream;
    FileName: string;
    destructor Destroy; override;
    constructor Create(AController: TObservableController; AOperation: TCRUD; const AFileName: string;
                AInitiator: IServerObserver); reintroduce;
  end;

  TGetReportFileTask = class (TReportFileTask)
  protected
    procedure DoExecute; override;
  end;

  TSetReportFileTask = class (TReportFileTask)
  protected
    procedure DoExecute; override;
  end;

  TGetTableHistoryTask = class (TSqlRecordTask)
    procedure DoExecute; override;
  public
    CurrentRecords, HistoryRecords: TSqlRecords;
    constructor Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass;
                        const AQueryParameters: TQueryParameters; AInitiator: IServerObserver = nil); reintroduce;
    destructor Destroy; override;
  end;


implementation

{ TSetMySettings }

constructor TSetMySettingsTask.Create (AController: TObservableController; ASettings: variant; AInitiator: IServerObserver = nil);
begin
  inherited Create (AController, TCrud.Update, default (TQueryParameters), AInitiator);
  varSettings := ASettings;
end;

procedure TSetMySettingsTask.DoExecute;
begin
  inherited;

  var intf := Client.Service<ICommonService>;
  Outcome := intf.SetMySettings(varSettings);
end;

{ TLogoutTask }

procedure TLogoutTask.DoExecute;
begin
  inherited;
  sleep (500);
  Outcome.SetSuccess;
end;

{ TPingTask }

procedure TPingTask.DoExecute;
begin
  inherited;
  Client.ServerTimestampSynchronize;
  Outcome.Success := true;
end;

{ TGetPublicFileTask }

destructor TGetPublicFileTask.Destroy;
begin
  Stream.Free;
  inherited;
end;

procedure TGetPublicFileTask.DoExecute;
begin
  inherited;

  var intf := Client.Service<ICommonService>;
  var Response := intf.GetPublicFile (StringToUTF8 (FileName));

  var blob := Response.Content;
  Stream := RawByteStringToStream(blob);
  Stream.Position := 0;
  Outcome.Success := (Response.Status = HTTP_SUCCESS) and (Stream.Size > 0);
  if not Outcome.Success
     then Outcome.SetError (Response.Status, string(Response.Content));
end;

{ TRecordsCreateTask }

procedure TRecordsCreateTask.DoExecute;
var
  json: TRawUTF8DynArray;
begin
  inherited;

  var intf := Client.Service<ICommonService>;
  setlength (json, Length (Records));
  for var i := Low (Records) to High (Records) do
  if Fields <> ''
     then json[i] := Records[i].GetJSONValues (true, Records[i].IDValue.IsNotNULL, Fields, [])
     else json[i] := Records[i].GetJSONValues (true, Records[i].IDValue.IsNotNULL,
                     Records[i].RecordProps.CopiableFieldsBits {Records[i].GetNonVoidFields}, []);
  var tn := Records[0].RecordClass.crudSQLTableName;
  Outcome := intf.AddMany(tn, Fields, json);
end;

constructor TReportFileTask.Create(AController: TObservableController; AOperation: TCRUD; const AFileName: string; AInitiator: IServerObserver);
begin
  inherited Create (AController, AOperation, default (TQueryParameters), AInitiator);
  FileName := AFileName;
end;

destructor TReportFileTask.Destroy;
begin
  Stream.Free;
  inherited;
end;

{ TSetReportFileTask }

procedure TSetReportFileTask.DoExecute;
begin
  inherited;

  var intf := Client.Service<IReportService>;
  Stream.Position := 0;
  Outcome := intf.SetReportFile (StringToUTF8 (FileName), StreamToRawByteString(Stream));
end;

procedure TGetReportFileTask.DoExecute;
begin
  inherited;

  var intf := Client.Service<IReportService>;
  var Response := intf.GetReportFile (StringToUTF8 (FileName));

  var blob := Response.Content;
  Stream := RawByteStringToStream(blob);
  Stream.Position := 0;
  Outcome.Success := (Response.Status = HTTP_SUCCESS) and (Stream.Size > 0);
  if not Outcome.Success
     then Outcome.SetError (Response.Status, string(Response.Content));
end;

{ TGetGetChangeLogTask }

constructor TGetTableHistoryTask.Create (AController: TObservableController; ASqlRecordClass: TSQLRecordClass;
                                         const AQueryParameters: TQueryParameters; AInitiator: IServerObserver);
begin
  inherited Create (AController, ASqlRecordClass, TCRUD.ReadMany, AQueryParameters, AInitiator);
  CurrentRecords := TSqlRecords.Create (ASqlRecordClass);
  HistoryRecords := TSqlRecords.Create (ASqlRecordClass);
end;

destructor TGetTableHistoryTask.Destroy;
begin
  CurrentRecords.Free;
  HistoryRecords.Free;

  inherited;
end;

procedure TGetTableHistoryTask.DoExecute;
var
  tempTableCurrent, tempTableHistory : TSQLTableJSON;
  olCurrent, olHistory : TObjectList;
  jsonCurrentRecords, jsonHistoryRecords: RawUTF8;
begin
  inherited;

  var intf := Client.Service<ICommonService>;
  var tn := FSqlRecordClass.SQLTableName;

  Outcome := intf.GetTableHistory (tn, jsonCurrentRecords, jsonHistoryRecords, QueryParameters);

  tempTableCurrent := TSQLTableJSON.CreateFromTables([FSqlRecordClass], '', jsonCurrentRecords);
  tempTableHistory := TSQLTableJSON.CreateFromTables([FSqlRecordClass], '', jsonHistoryRecords);

  olCurrent := TObjectList.Create;
  olHistory := TObjectList.Create;

  tempTableCurrent.ToObjectList(olCurrent, FSqlRecordClass);
  tempTableHistory.ToObjectList(olHistory, FSqlRecordClass);

  CurrentRecords.AssignObjectList (olCurrent);
  HistoryRecords.AssignObjectList (olHistory);

  olCurrent.Free;
  olHistory.Free;

  tempTableCurrent.Free;
  tempTableHistory.Free;
end;

end.

