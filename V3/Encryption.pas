unit Encryption;

interface

uses DCPrijndael, SyncObjs, SysUtils;

function easDecryptString (const s, EncryptionHash : string): string;
function easEncryptString (const s, EncryptionHash: string): string;
function easGetStringHash(Source: string): string;

var
  DCP_rijndael: TDCP_rijndael;

implementation

uses
  dcpBase64, dcpCrypt2, DCPsha256;

//const
//eas_Encryption_Hash = 'k988pex04jhw4xa95ndf28tynj2gfd93k7huc';

var
  cs: TCriticalSection;

function easDecryptString (const s, EncryptionHash : string): string;
begin
  cs.Enter;
  DCP_rijndael.InitStr(EncryptionHash, TDCP_sha256);
  result := DCP_rijndael.DecryptString (s);
  cs.Leave;
end;

function easEncryptString (const s, EncryptionHash: string): string;
begin
  cs.Enter;
  DCP_rijndael.InitStr(EncryptionHash, TDCP_sha256);
  result := DCP_rijndael.EncryptString (s);
  cs.Leave;
end;

function DigestToStr(Digest: array of byte): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(Digest)-1 do
    Result := Result + LowerCase(IntToHex(Digest[i], 2));
end;

function easGetStringHash(Source: string): string;
var
  Hash: TDCP_sha256;
  Digest: array[0..31] of Byte;
begin
  Hash := TDCP_sha256.Create(nil);
  Hash.Init;
  Hash.UpdateStr(Source);
  Hash.Final(Digest);
  Hash.Free;
  Result := DigestToStr(Digest);
end;

initialization

  DCP_rijndael := TDCP_rijndael.Create (nil);
  cs := TCriticalSection.Create;

finalization

  FreeAndNil (DCP_rijndael);
  FreeAndNil (cs);
end.
