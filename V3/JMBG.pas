unit JMBG;

interface

type

TJMBG = class
  type
    TGender = (Male, Female);
  strict private
    FValue : string;
    FDoB : TDate;
    FGender : TGender;
    FControlDigit: char;
    class function FormatValidation(const jmbg : string) : boolean;
    class function ChecksumValidation(const jmbg : string) : boolean;
    class function Checksum(const jmbg : string) : string;
    procedure SetValue(const AValue: string);
  public
    class function GenerateRandom : string;
    class function IsValid(jmbg : string) : boolean;
    class function GenderType(jmbg : string) : TGender;
    class function DoBValidation(const jmbg : string; out ADoB : TDateTime) : boolean;
    property Value : string read FValue write SetValue;
    property  DateOfBirth : TDate read FDoB;
    property Gender : TGender read FGender;
    property ControlDigit : char read FControlDigit;
    constructor Create; overload;
    constructor Create(jmbg : string); overload;
  end;

implementation

uses
  System.SysUtils,
  System.TypInfo,
  System.DateUtils;

class function TJMBG.Checksum(const jmbg: string): string;
var
  Sum, Checksum : integer;
begin
  Sum := 0;
  for var i := 0 to 5 do
    Sum := Sum + ( (StrToInt(JMBG[i+1]) + StrToInt(JMBG[i+7])) * (7 - i) );

  Checksum := 11 - (Sum mod 11);
  if Checksum > 9 then
    Checksum := 0;

  Result := IntToStr(Checksum);
end;

class function TJMBG.ChecksumValidation(const jmbg: string): boolean;
begin
  Result := Checksum(jmbg) = jmbg[13];
end;

constructor TJMBG.Create(jmbg: string);
begin
  inherited Create;
  Value := jmbg;
end;

constructor TJMBG.Create;
begin
  inherited;
end;

class function TJMBG.DobValidation(const jmbg: string; out ADoB : TDateTime): boolean;
var
  day, mon, year: integer;
begin
  result := TryStrToInt (copy(jmbg, 1, 2), day)
            and
            TryStrToInt (copy(jmbg, 3, 2), mon)
            and
            TryStrToInt (copy(jmbg, 5, 3), year);
  if not result then exit;

  if year < 900 then
    inc(year, 2000)
  else
    inc(year,1000);
  result := TryEncodeDate(year, mon, day, Adob);
end;

class function TJMBG.FormatValidation(const jmbg : string) : boolean;
begin
  Result := true;
  if Length(jmbg) <> 13 then exit(false);
  for var i := 1 to 13 do
    if not CharInSet(jmbg[i], ['0'..'9']) then
      exit(false);
end;

class function TJMBG.GenderType(jmbg: string): TGender;
var
  g : integer;
begin
  Result := TGender.male;
  if (length(jmbg) = 13) and tryStrToInt(jmbg[10], g) then
    Result := TGender(ord(g > 4));
end;

class function TJMBG.GenerateRandom: string;
begin

  var t := Random(Trunc(Today));
  Result := format('%.2d', [DayOf(t)]);
  Result := Result + format('%.2d', [MonthOf(t)]);
  Result := Result + format('%.3d', [YearOf(t) mod 1000]);
  Result := Result + format('%.2d', [random(100)]);
  Result := Result + format('%.3d', [random(2) * 500 + random(500)]);

  Result := Result + TJMBG.Checksum(Result);

end;

class function TJMBG.IsValid(jmbg : string): boolean;
var
  dob : TDatetime;
begin
  Result := FormatValidation(jmbg) and DoBValidation(jmbg, dob) and ChecksumValidation(jmbg);
end;


procedure TJMBG.SetValue(const AValue: string);
var
  dob : TDateTime;
begin

  if TJMBG.FormatValidation(AValue) and TJMBG.DoBValidation(AValue, dob) and TJMBG.ChecksumValidation(AValue) then
  begin
    FDoB := dob;
    FGender := GenderType(AValue);
    FControlDigit := AValue[13];
    FValue := AValue;
  end
  else
    raise Exception.Create('Invalid JMBG!');
end;

initialization

  Randomize;

end.
