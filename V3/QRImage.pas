unit QRImage;

interface

uses
  System.Math, System.Classes,
  FMX.Objects,
  DelphiZXingQRCode;

type
  TQRCodeImage = class(TImage)
  private
    FQRCode : TDelphiZXingQRCode;
    function GetQZ: integer;
    procedure SetQZ(const Value: integer);
    function GetEnc: TQRCodeEncoding;
    procedure SetEnc(const Value: TQRCodeEncoding);
    function GetQRData: string;
    procedure SetQRData(const Value: string);
  protected
     procedure Paint; override;
  public
    procedure Update;
    property QuietZone : integer read GetQZ write SetQZ;
    property Encoding : TQRCodeEncoding read GetEnc write SetEnc;
    property QRData : string read GetQRData write SetQRData;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    /// <summary> Replace existing TImage object with constructed QRImage and copy parent, margins and alignment properties
    /// </summary>
    constructor Replace(var Image: Timage);
  end;

implementation

uses
  System.Types, System.UITypes,
  FMX.Graphics;

{ TQRCodeImage }

constructor TQRCodeImage.Create(AOwner: TComponent);
begin
  inherited;
  FQRCode := TDelphiZXingQRCode.Create;
end;

destructor TQRCodeImage.Destroy;
begin
  FQRCode.Free;
  inherited;
end;

function TQRCodeImage.GetQRData: string;
begin
  Result := FQRCode.Data;
end;

function TQRCodeImage.GetEnc: TQRCodeEncoding;
begin
  Result := FQRCode.Encoding;
end;

function TQRCodeImage.GetQZ: integer;
begin
  Result := FQRCode.QuietZone;
end;

procedure TQRCodeImage.Paint;
begin
  Update;
  inherited;

end;

constructor TQRCodeImage.Replace(var Image: Timage);
begin
  Create(Image.Owner);
  Align := Image.Align;
  Margins.Assign(Image.Margins);
  Parent := image.Parent;
  Image.Free;
  image := self;
end;

procedure TQRCodeImage.SetQRData(const Value: string);
begin
  FQRCode.Data := Value;
  Update;
end;

procedure TQRCodeImage.SetEnc(const Value: TQRCodeEncoding);
begin
  FQRCode.Encoding := Value;
  Update;
end;

procedure TQRCodeImage.SetQZ(const Value: integer);
begin
  FQRCode.QuietZone := Value;
  Update;
end;

procedure TQRCodeImage.Update;
var
  PixelData: TBitmapData;
  PixelColor : TAlphaColor;
  Ratio : single;
  smaller : integer;
  CanvRect : TRectF;

begin
  if FQRCode.Rows = 0 then  exit;

  Bitmap.SetSize(round(Width), round(Height));
  CanvRect := TRectF.Create(Margins.Left, Margins.Top, Width - Margins.Right, height - Margins.Bottom);
  smaller := round(min(CanvRect.Width, CanvRect.Height));
  Ratio := smaller / FQRCode.Rows;
  if Bitmap.Map(TMapAccess.Write, PixelData) then
    try
      for var j := 0 to Bitmap.Height - 1 do
        for var i := 0 to Bitmap.Width - 1 do
        begin
          if CanvRect.Contains(TPointF.Create(i,j)) then
          begin
            // Calculate the pixel color based on "QR cell"
            if FQRCode.IsBlack[round((i - Margins.Left - ratio/2) / ratio), round((j - Margins.Top- ratio/2) / ratio)] then
              PixelColor := TAlphaColorRec.Black
            else
              Pixelcolor := TAlphaColorRec.White;

            PixelData.SetPixel(i, j, PixelColor);
          end;
        end;

    finally
      Bitmap.Unmap(PixelData);
    end;
end;

end.
