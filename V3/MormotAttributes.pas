unit MormotAttributes;

interface

uses
  SysUtils, Contnrs, System.Generics.Collections, Classes, IOUtils,
  Variants, StrUtils, math, System.TypInfo, RTTI,
  {$ifdef MSWINDOWS}
  Windows, //for inline of synlocker...
  {$endif}
  mORMotHttpClient, SynCrtSock,
  SynCommons, SynDB, mormot, MormotTypes, SynTable, Common.Utils;

  {$SCOPEDENUMS ON}

type

//  SQLAttribute = class (TCustomAttribute);
//  PlainFieldAttribute = class (TCustomAttribute); // to indicate it is supposed to be treated as a simple field even if its lookup or something else complex

  HashedAttribute = class (TCustomAttribute)
  protected
    FHash: string;
  public
    function Hash: string;
  end;

  StatisticsAttribute = class (TCustomAttribute);// to indicate table should have statistics displayed

  TChartType = (Line, Bar, Pie);

  TChartAttributeSettings = record
    ChartType: TChartType;
    Title, SeriesFieldName, XFieldName, YFieldName: string;
    Interval: TDateTime;
  end;

  ChartAttribute = class (TCustomAttribute)
  public
    Settings: TChartAttributeSettings;
    constructor Create (AChartType: TChartType; const ATitle, ASeriesFieldName, AXFieldName, AYFieldName: string;
                        AInterval: TDateTime = 10 * DelphiMinute);
  end;

  // Alternate Unique Key: indicates this field is part of an alternative way of uniquely identifying the record
  AlternateUK = class (HashedAttribute);

  // can be used as key, can be put in substitute list via database maintenance tools so that re-import can substitute misspelled stuff with correct stuff
  SubstituteKeyAttribute = class (TCustomAttribute);

  RequiredResource = class (TCustomAttribute)
  private
    FResourceIdentifier: string;
  public
    constructor Create (const AResourceIdentifier: string);
    property ResourceIdentifier: string read FResourceIdentifier;
  end;

  MormotFKAttribute = class (HashedAttribute)
  protected
    constructor Create (ASqlRecordClass: TSQLRecordClass); overload;
    constructor Create (const ASqlRecordClassName: string); overload;
  public
    SelfReferencingAllowed: boolean;
    SqlRecordClass: TSQLRecordClass;
    SqlRecordClassName: string;
  end;

  FKRuleAttribute = class(MormotFKAttribute)
    const Custom_Name = true;
    var
    OnDelete: TReferentialAction;
    CustomName: boolean;
    constructor Create (ASqlRecordClass: TSQLRecordClass; AOnDeleteAction: TReferentialAction; ACustomName: boolean = false);
  end;

  FKRestrictedAttribute = class(FKRuleAttribute)
    constructor Create (ASqlRecordClass: TSQLRecordClass; ACustomName: boolean = false);
  end;

//  FKAllowedCascadeAttribute = class(FKCascadeAttribute);

  LargeTableAttribute = class(TCustomAttribute)
  end;

  // this att indicates that the content of the field should be transliterated and written to a "slave" field
  // with name in form of name_TL
  HasTranslitCopyAttribute = class(TCustomAttribute)
  end;

  // "foreign table restricted mirror field"
  // used to decorate a mirror field in a table that should match value in a foreign table
  // and prevent foreign table from changing its value of that field
  // useful to denormalize and include more foreign keys in tabels that dont strictly need them because
  // they are in other linked foreign tables already
  {TODO -oOwner -cGeneral : see below}
  FTRestrictedMirrorFieldAttribute = class (TCustomAttribute)
    ForeignTableClass: TSQLRecordClass;
    OriginalFieldName, ForeignKeyName: RawUTF8;
    AutoFillIfNull: boolean;
    constructor Create (AForeignTableClass: TSQLRecordClass; AAutoFillIfNull: boolean = false; AOriginalFieldName: RawUTF8 = ''; AForeignKeyName: RawUTF8 = '');
  end;

  // restrict changing the field value if the record is referenced in a ForeignTableClass record
  // e.g. prevent changing "Kidney scan" to "Lung scan" in ScanType table, if a Scan record already exists for this type
  FTRestrictedFieldAttribute = class (TCustomAttribute)
    ForeignTableClass: TSQLRecordClass;
    ForeignKeyName: RawUTF8;
    constructor Create (AForeignTableClass: TSQLRecordClass; AForeignKeyName: RawUTF8 = '');
  end;

  CalculatedFieldAttribute = class(HashedAttribute)
  public
    Sql: string;
    constructor Create (const ASql: string = '');
  end;

  GeneralFilterAttribute = class(TCustomAttribute)
//    constructor Create (const AFieldPath: string = ''); // if a FK is general filter, set this to FKTable
  end;

  // these attributes are meant for sql record class, because we cant tag the inherited ID field with attributes
  IDIsGeneralFilter = class(TCustomAttribute);
  IDIsBrief         = class(TCustomAttribute);

  // NAMING CONVENTIONS:
  // "ReferencedTable" is the table that the foreign key is pointing to
  // "ReferencedKeyName" is the field in that table that the foreign key is pointing to
  ViewAttribute = class(TCustomAttribute)
  public
    sqlJoin: RawUTF8;
//    TableName, AliasName,
//    FinalName, // if alias is supplied then use that value otherwise use TableName
//    FKName, ForeignTable: RawUTF8; // ForeignTable blank means the base table i.e. the inherited sql record
//    ForeignTableIsDetail: boolean;
//    constructor Create (const ATableName: RawUTF8; const AAliasName: RawUTF8 = ''; AFKName: RawUTF8 = ''; AForeignTable: RawUTF8 = ''; AForeignTableIsDetail: boolean = false);
    constructor Create (AJoin: RawUTF8);
  end;

  // indicate which detail tables MAY be cleared when master record is altered
  // used only once for now for documents of request...
  CascatedTablesAttribute = class(TCustomAttribute)
  public
    TableNames: TRawUTF8DynArray;
    constructor Create (ATableNames: RawUTF8);
  end;

  SimpleViewAttribute = class (ViewAttribute)
    //simple version, referenced ID is "ID" and FK name is Referenced table name + ID
    constructor Create (AReferencedClass: TSqlRecordClass); overload;
    // version where everything has to be specified, ForeignKeyName CAN be in tablename.fieldname format
    constructor Create (AReferencedClass: TSqlRecordClass; const ReferencedKeyName, ForeignKeyName: RawUTF8); overload;
    // version where everything has to be specified
    constructor Create (const ReferencedTableName, ReferencedKeyName, ForeignKeyName: RawUTF8); overload;
  end;

  GroupedViewAttribute = class (ViewAttribute);

  DisplayableFieldsAttribute = class (TCustomAttribute) // to set FieldInfo.Displayable for some basic fields that cant be attribute-tagged
  public
    Fields: TStringList;
    constructor Create (AFields: string);
    destructor Destroy; override;
  end;

  AutoUIAttribute = class (TCustomAttribute); // indicates that some or all of UI is missing and should be autogenerated

  DefaultValueAttribute = class (TCustomAttribute)
    procedure ApplyTo(p : TRttiProperty; Target: TObject); virtual;
  end;

  DefaultTodayAttribute = class (DefaultValueAttribute)
    procedure ApplyTo(p : TRttiProperty; Target: TObject); override;
  end;

  DefaultNowAttribute = class (DefaultValueAttribute)
    procedure ApplyTo(p : TRttiProperty; Target: TObject); override;
  end;

  DefaultVarAttribute = class (DefaultValueAttribute)
  public
    Value: variant;
    //constructor Create (AValue: variant); overload; delphi cant seem to handle this idk why
    constructor Create (AValue: boolean); overload;
    constructor Create (AValue: integer); overload;
    procedure ApplyTo(p : TRttiProperty; Target: TObject); override;
  end;

  ZeroIsNullAttribute = class (TCustomAttribute);

  DBIndexedAttribute = class (TCustomAttribute);
  DBMultiFieldIndexAttribute = class (HashedAttribute)
  public
    Fields: TArray<string>;
    UTF8Fields: TArray<RawUTF8>;
    Unique: boolean;
    FName: RawUTF8;
    constructor Create (const AFields: string; const AName: RawUTF8 = ''; const AUnique: boolean = true);
    function NameFor (TableName: RawUTF8): RawUTF8;
  end;
  FullTextIndexAttribute = class (DBIndexedAttribute);

  AllowDuplicateIDsAttribute = class (TCustomAttribute);

  DateAttribute = class (TCustomAttribute);
  TimeAttribute = class (TCustomAttribute);

  AggregationAttribute = class(TCustomAttribute);

  // for views that do not inherit an actual table, maybe needs better name
  // inherited sql join is actually full sql, bad name...
  CustomViewAttribute = class (ViewAttribute);

  // Initial purpose of this is to add an aggregate attribute to list of select, this only makes sense for SQLite
  AdditionalViewFieldsAttribute = class (TCustomAttribute)
  public
    Fields: RawUTF8;
    constructor Create (AFields: RawUTF8);
  end;

  CrudClassAttribute = class(TCustomAttribute)
  public
    SqlRecordClass: TSqlRecordClass;
    constructor Create (const ASqlRecordClass: TSqlRecordClass);
  end;

  ForeignFieldAttribute = class(TCustomAttribute)
    ForeignKeyName, // usually it is tablename+ID but if not, it could be fetched from the view attribute of the joined table but thats not easy...
    FieldName, TableName, FullFieldName: RawUTF8;
    SQLRecordClass: TSQLRecordClass;
    // can specify just table name if field is same as property name, or can specify full "path" e.g. for 'SUM (TABLENAME.FIELDNAME)'
    constructor Create (const ASQLRecordClass: TSQLRecordClass; const AFieldName: RawUTF8 = '';
                        const AFullFieldName: RawUTF8 = ''; const AForeignKeyName: RawUTF8 = '');
  end;

  ForeignFieldSummAttribute = class (ForeignFieldAttribute);

  DefaultSortFieldAttribute = class(TCustomAttribute)
  end;

  // currently not used... UI-data-collector model should auto trim by default maybe, but this can be useful for import
  TrimAttribute = class(TCustomAttribute)
  type
    TTrimType = (Left, Right, All);
  var
    TrimType : TTrimType;
    constructor Create(const ATrimType: TTrimType = TTrimType.All);
  end;

  UniqueIfNotNullAttribute = class(TCustomAttribute)
  end;

  BriefAttribute = class(TCustomAttribute)
  end;

  PrintableAttribute = class(TCustomAttribute)
  public
    PrintField : string;
    constructor Create(const APrintField : string = '');
  end;

  ReportQualifierAttribute = class(TCustomAttribute); // field value can be used to check if record qualifies for a report

  ReadOnlyAttribute = class(TCustomAttribute);

  DisplayableAttribute = class(TCustomAttribute)
  public
    DisplayName: string;
    Alignment : TFieldAlignment;
    constructor Create (const ADisplayName: string = ''; const AAlignment : TFieldAlignment = TFieldAlignment.None); overload;
    constructor Create (const AAlignment : TFieldAlignment); overload;
  end;

  IgnoreObsoleteFieldsAttribute = class(TCustomAttribute)
  public
    Fields: TStringList;
    constructor Create (const CSVFields: string);
    destructor Destroy; override;
  end;

  HiddenIfViewAttribute = class(TCustomAttribute);

  TestValueGeneratorAttribute = class(TCustomAttribute)
  public
    function Generate: string; virtual; abstract;
  end;

  StringConstTestValueGeneratorAttribute = class(TestValueGeneratorAttribute)
  public
    ID: integer;
    Value: string;
    AppendAutoIncrement: boolean;
    constructor Create (const AValue: string; AAppendAutoIncrement: boolean = true);
    function Generate: string; override;
  end;

  RandomNumberTestValueGeneratorAttribute = class(TestValueGeneratorAttribute)
  public
    Length: integer;
    constructor Create (const ALength: integer);
    function Generate: string; override;
  end;

  CrudPermissionsAttribute = class(TCustomAttribute)
  public
    RequiredLevels: TRequiredAuthLevels;
    constructor Create (const ARequiredLevel: TAuthGroupID); overload; // one for all
    constructor Create (AOperation: TBasicCrud; const ARequiredLevel: TAuthGroupID); overload;
    constructor Create (const ARequiredLevels: TRequiredAuthLevels); overload;
    constructor Create (ralCreate, ralRead, ralUpdate, ralDelete: TAuthGroupID); overload;
  end;

  TCrudPermissions = TDictionary <TCrud, TAuthGroupID>;

  ImportedFieldIndexAttribute = class (TCustomAttribute)
    Index: integer;
    RowOffset: integer;
    constructor Create (AIndex: integer; ARowOffset : integer = 0);
  end;

  ImportedFieldTargetAttribute = class (TCustomAttribute)
    TargetTable: TSQLRecordClass;
    TargetField: RawUTF8;
    constructor Create(ATargetTable: TSQLRecordClass; ATargetField: RawUTF8 = '');
  end;

  // use to indicate that import/migrate procedure should try to find a record in the specified table
  // based on the key field - if it doesnt find one then create otherwise update
  // if AKeyField is empty then update wont be attempted
  ImportedTableAttribute = class (TCustomAttribute)
    TargetTable: TSqlRecordClass;
    KeyFieldName, KeyFieldLocalName: RawUTF8;
    AllowLike: boolean;
    constructor Create (ATargetTable: TSQLRecordClass; AKeyFieldName: RawUTF8; AKeyFieldLocalName: RawUTF8 = ''; AAllowLike: boolean = false);
  end;

  ImportAsEnumAttribute = class (TCustomAttribute)
    Values: TStringList;
    constructor Create (AValues: string);
    destructor Destroy; override;
  end;

  SkipAutomationAttribute = class (TCustomAttribute);

  // as ancestor for attributes that have a field name attached
  FieldNameAttribute = class (TCustomAttribute)
    FieldName: string;
    constructor Create (AFieldName: string);
  end;

  SubstituteSortFieldAttribute = class (FieldNameAttribute);
  SecondarySortFieldAttribute = class (FieldNameAttribute);


implementation

uses MormotMods;

constructor MormotFKAttribute.Create(ASqlRecordClass: TSQLRecordClass);
begin
  inherited Create;
  SqlRecordClass := ASQLRecordClass;
  SqlRecordClassName := ASQLRecordClass.SQLTableName.AsString;
end;

{ ViewAttribute }

//constructor ViewAttribute.Create (const ATableName: RawUTF8; const AAliasName: RawUTF8 = ''; AFKName: RawUTF8 = ''; AForeignTableIsDetail: boolean = false);
//begin
//  TableName := ATableName;
//  AliasName := AAliasName;
//  ForeignTableIsDetail := AForeignTableIsDetail;
//  if AliasName = ''
//     then FinalName := TableName
//     else FinalName := AliasName;
//  if AFKName = ''
//     then FKName := FinalName + 'ID'
//     else FKName := AFKName
//end;

{ ForeignFieldAttribute }

constructor ForeignFieldAttribute.Create (const ASQLRecordClass: TSQLRecordClass; const AFieldName: RawUTF8 = '';
                                          const AFullFieldName: RawUTF8 = ''; const AForeignKeyName: RawUTF8 = '');
begin
  SQLRecordClass := ASQLRecordClass;
  TableName := ASQLRecordClass.SQLTableName;
  FieldName := AFieldName;
  FullFieldName := AFullFieldName;
  ForeignKeyName := AForeignKeyName;
  if AFieldName <> '' then
     if (AFieldName <> 'ID') and not ASQLRecordClass.FieldInfoList.HasField (AFieldName.AsString) then
        raise Exception.Create(ASQLRecordClass.SQLTableName.AsString + ' does not have field ' + AFieldName.AsString);
end;

{ ViewAttribute }

constructor ViewAttribute.Create(AJoin: RawUTF8);
begin
  inherited Create;
  sqlJoin := AJoin;
//  if sqlJoin = '' then raise Exception.Create('Join SQL is empty!');
end;

{ PrintableAttribute }

constructor PrintableAttribute.Create(const APrintField : string = '');
begin
  inherited Create;
  PrintField := APrintField;
end;

constructor DisplayableAttribute.Create(const AAlignment: TFieldAlignment);
begin
  Create ('', AAlignment);
end;


{ SimpleViewAttribute }

constructor SimpleViewAttribute.Create (const ReferencedTableName, ReferencedKeyName, ForeignKeyName: RawUTF8);
var
  sql: RawUTF8;
begin
  sql := FormatUTF8 ('LEFT JOIN % on %.% = %',
             [ReferencedTableName, ReferencedTableName, ReferencedKeyName, ForeignKeyName]);

  inherited Create (sql);
end;

constructor SimpleViewAttribute.Create (AReferencedClass: TSqlRecordClass; const ReferencedKeyName, ForeignKeyName: RawUTF8);
begin
  if ReferencedKeyName = ''
     then Create (AReferencedClass.SQLTableName, 'ID', ForeignKeyName)
     else Create (AReferencedClass.SQLTableName, ReferencedKeyName, ForeignKeyName)
end;

constructor SimpleViewAttribute.Create (AReferencedClass: TSqlRecordClass);
var
  ReferencedCrudClass: TSqlRecordClass;
begin
{  var TableClass := AReferencedClass;
  // get underlying CRUD table class if the referenced class is a view
  while TableClass.IsView
     do TableClass := TSqlRecordClass(TableClass.ClassParent);
  if ForeignKeyName = ''
     then ForeignKeyName := TableClass.SQLTableName + 'ID';
  if FKOriginTableName <> ''
     then ForeignKeyName := FKOriginTableName + '.' + ForeignKeyName;

  sql := FormatUTF8 ('LEFT JOIN % on %.ID = %',
             [AReferencedClass.SQLTableName, AReferencedClass.SQLTableName, ForeignKeyName]);
}

  if AReferencedClass.IsView and (AReferencedClass.crudClass <> nil)
     then ReferencedCrudClass := AReferencedClass.crudClass
     else ReferencedCrudClass := AReferencedClass;

  Create (AReferencedClass.SQLTableName, 'ID', ReferencedCrudClass.SQLTableName + 'ID');
end;



{ CrudPermissionsAttribute }

//constructor CrudPermissionsAttribute.Create(const AOperations: array of TCrud; const ARequiredLevels: array of integer);
//begin
//  Items := TDictionary <TCrud, integer>.Create;
//  for var i := 0 to length(AOperations) do
//    Items.Add (AOperations[i], ARequiredLevels[i]);
//end;

constructor CrudPermissionsAttribute.Create (const ARequiredLevel: TAuthGroupID);
begin
  inherited Create;
  for var bc := TBasicCrud.Create to TBasicCrud.Delete do
    RequiredLevels[bc] := ARequiredLevel;
end;

constructor CrudPermissionsAttribute.Create (AOperation: TBasicCrud; const ARequiredLevel: TAuthGroupID);
begin
  inherited Create;
  RequiredLevels[AOperation] := ARequiredLevel;
end;

constructor CrudPermissionsAttribute.Create (ralCreate, ralRead, ralUpdate, ralDelete: TAuthGroupID);
begin
  RequiredLevels[TBasicCrud.Create] := ralCreate;
  RequiredLevels[TBasicCrud.Read] := ralRead;
  RequiredLevels[TBasicCrud.Update] := ralUpdate;
  RequiredLevels[TBasicCrud.Delete] := ralDelete;
end;

constructor CrudPermissionsAttribute.Create (const ARequiredLevels: TRequiredAuthLevels);
begin
  inherited Create;
  RequiredLevels  := ARequiredLevels;
end;

{ AdditionalViewFieldsAttribute }

constructor AdditionalViewFieldsAttribute.Create(AFields: RawUTF8);
begin
  Fields := AFields;
end;

constructor FTRestrictedMirrorFieldAttribute.Create (AForeignTableClass: TSQLRecordClass; AAutoFillIfNull: boolean = false;
                                                    AOriginalFieldName: RawUTF8 = ''; AForeignKeyName: RawUTF8 = '');
begin
  ForeignTableClass := AForeignTableClass;
  OriginalFieldName := AOriginalFieldName;
  ForeignKeyName := AForeignKeyName;
  AutoFillIfNull := AAutoFillIfNull;
end;

{ StringConstTestValueGeneratorAttribute }

constructor StringConstTestValueGeneratorAttribute.Create(const AValue: string; AAppendAutoIncrement: boolean = true);
begin
  Value := AValue;
  AppendAutoIncrement := AAppendAutoIncrement;
end;

function StringConstTestValueGeneratorAttribute.Generate: string;
begin
  result := Value;
  if AppendAutoIncrement then
    begin
      inc (ID);
      result := result + '_' + ID.ToString;
    end;
end;

{ RandomNumberTestValueGeneratorAttribute }

constructor RandomNumberTestValueGeneratorAttribute.Create(const ALength: integer);
begin
  Length := ALength;
end;

function RandomNumberTestValueGeneratorAttribute.Generate: string;
begin
  result := '';
  for var i := 1 to Length do result := result + random (9).ToString;
end;

{ TrimAttribute }

constructor TrimAttribute.Create(const ATrimType: TTrimType = TTrimType.All);
begin
  inherited Create;
  TrimType := ATrimType;
end;

{ IgnoreObsoleteFieldsAttribute }

constructor IgnoreObsoleteFieldsAttribute.Create(const CSVFields: string);
begin
  Fields := TStringList.Create;
  Fields.CommaText := CSVFields;
end;

destructor IgnoreObsoleteFieldsAttribute.Destroy;
begin
  Fields.Free;
  inherited;
end;

constructor MormotFKAttribute.Create(const ASqlRecordClassName: string);
begin
  inherited Create;
  SqlRecordClassName := ASqlRecordClassName;
  FHash := ClassName + IfThen(SelfReferencingAllowed, 'YSR', 'NSR');
end;

{ CrudClass }

constructor CrudClassAttribute.Create(const ASqlRecordClass: TSqlRecordClass);
begin
  inherited Create;
  SqlRecordClass := ASqlRecordClass;
end;

{ DefaultDisplayAttribute }

constructor DisplayableAttribute.Create(const ADisplayName: string = ''; const AAlignment : TFieldAlignment = TFieldAlignment.None);
begin
  DisplayName := ADisplayName;
  Alignment := AAlignment;
end;

{ FTRestrictedFieldAttribute }

constructor FTRestrictedFieldAttribute.Create(AForeignTableClass: TSQLRecordClass; AForeignKeyName: RawUTF8);
begin
  ForeignTableClass := AForeignTableClass;
  ForeignKeyName := AForeignKeyName;
end;

{ CascatedTablesAttribute }

constructor CascatedTablesAttribute.Create(ATableNames: RawUTF8);
begin
  CSVToRawUTF8DynArray (@ATableNames[1], TableNames);
end;

{ ImportedTableAttribute }

constructor ImportedTableAttribute.Create (ATargetTable: TSQLRecordClass; AKeyFieldName: RawUTF8;
                                           AKeyFieldLocalName: RawUTF8 = ''; AAllowLike: boolean = false);
begin
  TargetTable := ATargetTable;
  KeyFieldName := AKeyFieldName;
  if AKeyFieldLocalName = ''
     then KeyFieldLocalName := AKeyFieldName
     else KeyFieldLocalName := AKeyFieldLocalName;
  AllowLike := AAllowLike;
end;

constructor ImportedFieldTargetAttribute.Create(ATargetTable: TSQLRecordClass; ATargetField: RawUTF8 = '');
begin
  TargetTable := ATargetTable;
  TargetField := ATargetField;
end;

{ ImportedFieldIndex }

constructor ImportedFieldIndexAttribute.Create (AIndex: integer; ARowOffset : integer = 0);
begin
  Index := AIndex;
  RowOffset := ARowOffset;
end;

{ DefaultTodayAttribute }

procedure DefaultTodayAttribute.ApplyTo(p: TRttiProperty; Target: TObject);
begin
  p.SetValue(Target, TDate(trunc(Now)));
end;

{ DefaultValueAttribute }

procedure DefaultValueAttribute.ApplyTo(p: TRttiProperty; Target: TObject);
begin

end;

{ DefaultVarAttribute }

procedure DefaultVarAttribute.ApplyTo(p: TRttiProperty; Target: TObject);
begin
  inherited;

  var RttiType := p.PropertyType;

  // Check if the property is an enumeration type

  if (RttiType.TypeKind = System.tkEnumeration) and (VarType(Value) <> varBoolean)
    then
      begin
        // Convert the variant value to an integer
        var IntValue := TValue.FromVariant(Value).AsInteger;

        // Convert the integer to the enumeration value
        var EnumValue := TValue.FromOrdinal(RttiType.Handle, IntValue);

        // Set the enumeration value
        p.SetValue(Target, EnumValue);
      end
    else
      p.SetValue(Target, TValue.FromVariant(Value));
end;

constructor DefaultVarAttribute.Create (AValue: boolean);
begin
  inherited Create;
  Value := AValue;
end;

constructor DefaultVarAttribute.Create(AValue: integer);
begin
  inherited Create;
  Value := AValue;
end;


{ ImportAsEnum }

constructor ImportAsEnumAttribute.Create(AValues: string);
begin
  inherited Create;
  Values := TStringList.Create;
  Values.Text := AValues;
end;

destructor ImportAsEnumAttribute.Destroy;
begin
  Values.Free;
  inherited;
end;

{ SecondarySortFieldAttribute }

constructor FieldNameAttribute.Create(AFieldName: string);
begin
  inherited Create;
  FieldName := AFieldName;
end;

{ RequiredResource }

constructor RequiredResource.Create(const AResourceIdentifier: string);
begin
  FResourceIdentifier := AResourceIdentifier;
end;

{ FKCascadeAttribute }

constructor FKRuleAttribute.Create (ASqlRecordClass: TSQLRecordClass; AOnDeleteAction: TReferentialAction;
            ACustomName: boolean = false);
begin
  inherited Create (ASqlRecordClass);
  OnDelete := AOnDeleteAction;
  CustomName := ACustomName;
  FHash := 'Rule:' + FHash + integer (OnDelete).ToString + CustomName.ToString;
end;

{ FKRestrictedAttribute }

constructor FKRestrictedAttribute.Create(ASqlRecordClass: TSQLRecordClass; ACustomName: boolean = false);
begin
  inherited Create (ASqlRecordClass, TReferentialAction.Restrict, ACustomName);
end;


{ THashedAttribute }

function HashedAttribute.Hash: string;
begin
  result := FHash;
end;

{ DefaultNowAttribute }

procedure DefaultNowAttribute.ApplyTo(p: TRttiProperty; Target: TObject);
begin
  p.SetValue(Target, TDateTime(Now));
end;

{ ChartAttribute }

constructor ChartAttribute.Create (AChartType: TChartType; const ATitle, ASeriesFieldName, AXFieldName,
                                   AYFieldName: string; AInterval: TDateTime = 10 * DelphiMinute);
begin
  inherited Create;
  Settings.ChartType := AChartType;
  Settings.Title := ATitle;
  Settings.SeriesFieldName := ASeriesFieldName;
  Settings.XFieldName := AXFieldName;
  Settings.YFieldName := AYFieldName;
  Settings.Interval := AInterval;
end;

{ DBMultiFieldIndexAttribute }

constructor DBMultiFieldIndexAttribute.Create(const AFields: string; const AName: RawUTF8 = ''; const AUnique: boolean = true);
begin
  Fields := AFields.Split ([',']);
  SetLength (UTF8Fields, length (Fields));
  for var i := low (Fields) to high (Fields) do
    UTF8Fields[i].AsString := Fields[i];
  Unique := AUnique;
  FHash := 'DBMultiFieldIndex:' + AFields + IfThen (AUnique, 'U', 'N');
  FName := AName;
end;

function DBMultiFieldIndexAttribute.NameFor(TableName: RawUTF8): RawUTF8;
begin
  if FName.IsEmpty
   then begin
          result := 'MFI' + TableName;
          for var f in Fields do result.AsString := result.AsString + f;
        end
   else result := FName
end;

{ DisplayableFieldsAttribute }

constructor DisplayableFieldsAttribute.Create(AFields: string);
begin
  inherited Create;
  Fields := TStringList.Create;
  Fields.CommaText := AFields;
end;

destructor DisplayableFieldsAttribute.Destroy;
begin
  Fields.Free;
  inherited;
end;

{ CalculatedFieldAttribute }

constructor CalculatedFieldAttribute.Create(const ASql: string);
begin
  inherited Create;
  Sql := ASql;
  FHash := ASql;
end;

end.
