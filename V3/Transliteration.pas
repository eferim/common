﻿unit Transliteration;

interface

uses Character, Classes, SysUtils, StrUtils, System.Generics.Collections;

type

  TSimpleTranslit = class
  private
  class var
    CyrD : TDictionary<char, string>;
    LatD : TDictionary<String, char>;
    class function WordTrans(const text : string) : string;
    class constructor Create;
    class destructor Destroy;

  var
    Txt : string;
  public
    class function ToLatin(const text : string) : string; overload;
    class function ToCyrillic(const text : string; TranslitRomanNumbers : boolean = false) : string; overload;
    function ToLatin : string; overload;
    function ToCyrillic : string; overload;
  end;

  function ConvertRomanNumber (Roman: string; RequireUppercase: boolean = true): cardinal;

implementation

class constructor TSimpleTranslit.Create;
begin
  inherited;
  CyrD := TDictionary<char, string>.Create(60);
  LatD := TDictionary<String, char>.Create(60);
  with CyrD do
  begin
    Add('А', 'A');
    Add('Б', 'B');
    Add('В', 'V');
    Add('Г', 'G');
    Add('Д', 'D');
    Add('Ђ', 'Đ');
    Add('Е', 'E');
    Add('Ж', 'Ž');
    Add('З', 'Z');
    Add('И', 'I');
    Add('Ј', 'J');
    Add('К', 'K');
    Add('Л', 'L');
    Add('Љ', 'Lj');
    Add('М', 'M');
    Add('Н', 'N');
    Add('Њ', 'Nj');
    Add('О', 'O');
    Add('П', 'P');
    Add('Р', 'R');
    Add('С', 'S');
    Add('Т', 'T');
    Add('Ћ', 'Ć');
    Add('У', 'U');
    Add('Ф', 'F');
    Add('Х', 'H');
    Add('Ц', 'C');
    Add('Ч', 'Č');
    Add('Џ', 'Dž');
    Add('Ш', 'Š');
    Add('а', 'a');
    Add('б', 'b');
    Add('в', 'v');
    Add('г', 'g');
    Add('д', 'd');
    Add('ђ', 'đ');
    Add('е', 'e');
    Add('ж', 'ž');
    Add('з', 'z');
    Add('и', 'i');
    Add('ј', 'j');
    Add('к', 'k');
    Add('л', 'l');
    Add('љ', 'lj');
    Add('м', 'm');
    Add('н', 'n');
    Add('њ', 'nj');
    Add('о', 'o');
    Add('п', 'p');
    Add('р', 'r');
    Add('с', 's');
    Add('т', 't');
    Add('ћ', 'ć');
    Add('у', 'u');
    Add('ф', 'f');
    Add('х', 'h');
    Add('ц', 'c');
    Add('ч', 'č');
    Add('џ', 'dž');
    Add('ш', 'š');
  end;
  for var p in CyrD do
    LatD.Add(p.Value, p.Key);
  LatD.Add('Dj', 'Ђ');
  LatD.Add('dj', 'ђ');
  LatD.Add('NJ', 'Њ');
  LatD.Add('LJ', 'Љ');
end;

class destructor TSimpleTranslit.Destroy;
begin
  CyrD.Free;
  LatD.Free;
  inherited;
end;

class function TSimpleTranslit.WordTrans(const text : string) : string;
var
  l, c, n, pl, pc : char;

begin
  c := #0;
  pl := #0;
  pc := #0;
  for l in text do
  begin
    if LatD.TryGetValue(l, c) then
    begin
      var uc := UpperCase(l);
      if (uc = 'N') or (uc = 'L') or (uc = 'D') then
      begin
        if pl <> #0 then
          Result := Result + pc;
        pl := l;
        pc := c;
      end
      else
        if pl <> #0 then
        begin
          if LatD.TryGetValue(pl + l, n) then
            Result := Result + n
          else
            Result := Result + pc + c;
          pl := #0;
          pc := #0;
        end
        else
          Result := Result + c;
    end
    else
    begin
      if pc <> #0 then
      begin
        Result := Result + pc + l;
        pl := #0;
        pc := #0;
      end
      else
        Result := Result + l;
    end;
  end;
  if pc <> #0 then
    Result := Result + pc
  else
    if pl <> #0 then
      Result := Result + pl;
end;

class function TSimpleTranslit.ToCyrillic(const text: string; TranslitRomanNumbers : boolean = false): string;
type
  state = (letter, nonletter);

var
  st : state;
  wrd : string;

  procedure translit;
  begin
    if ConvertRomanNumber(wrd) = 0  then
      Result := Result + WordTrans(wrd)
    else
      Result := Result + wrd;
    wrd := '';
  end;

begin
  if TranslitRomanNumbers then
    Exit(WordTrans(text));

  st := letter;
  for var i := 1 to length(text) do
  begin
    case st of
      letter:
      begin
        if text[i].IsLetter then
          wrd := wrd + text[i]
        else
        begin
          translit;
          st := nonletter;
          Result := Result + text[i];
        end
      end;
      nonletter:
      begin
        if text[i].IsLetter then
        begin
          st := letter;
          wrd := text[i];
        end
        else
          Result := Result + text[i];
      end;
    end;

  end;
  translit;
end;

class function TSimpleTranslit.ToLatin(const text : string): string;
var
  l : string;
begin
  result := '';
  for var c in text do
  begin
    if CyrD.TryGetValue(c, l) then
      result := Result + l
    else
      Result := Result + c;
  end;
end;

function TSimpleTranslit.ToCyrillic : string;
begin
  Result := ToCyrillic(Txt);
end;

function TSimpleTranslit.ToLatin : string;
begin
  Result := ToLatin(Txt);
end;


function ConvertRomanNumber (Roman: string; RequireUppercase: boolean = true): cardinal;

const
  RomanNumbers :      array [0..6] of char     = ('I', 'V', 'X', 'L',  'C',  'D',   'M');
  RomanNumberValues : array [0..6] of cardinal = ( 1,   5,  10,  50,  100,  500,  1000);

var
  SameCount, ind, indNumber, OriginalLength: integer;
  Character, PreviousCharacter: char;
  Indexes : array of cardinal;

begin
   if not RequireUppercase
      then Roman := Uppercase (Roman);

   OriginalLength := Length (Roman);

   SetLength (Indexes, OriginalLength);

   PreviousCharacter := ' ';
   SameCount := 0;

   for ind := 1 to OriginalLength do
     begin
       indNumber := Low (RomanNumbers);
       Character := Roman[ind];
       while indNumber <= High (RomanNumbers) do
         if Character = RomanNumbers[indNumber]
           then
             begin
               Indexes [ind - 1] := indNumber;
               break;
             end
           else inc (indNumber);

       if indNumber > High (RomanNumbers) then exit (0); // character is not roman numeral

       if PreviousCharacter = Character
          then begin
                 if SameCount = 3
                   then exit (0) // already 3 consecutive same characters, 4th is not allowed
                                 // (V L D cant repeat even twice but that is checked later)
                   else inc (SameCount);
               end
          else begin
                 PreviousCharacter := Character;
                 SameCount := 1;
               end;
     end;

   for ind := 0 to OriginalLength - 2 do
     begin
       if integer(Indexes [ind + 1]) - integer(Indexes [ind]) > 2
         then exit (0); // Allow IV, IX but not IL, IC etc, the difference must be at most 2 levels

       if (Indexes [ind + 1] >= Indexes [ind]) and ( (Indexes [ind] mod 2) = 1)
         then exit (0); // V, L, D cant appear in front of larger number, and they cant repeat either
     end;

   result := 100;

   for ind := 0 to OriginalLength - 1 do
     begin
       if (ind < OriginalLength - 1) and (Indexes[ind + 1] > Indexes[ind])
          then result := result - RomanNumberValues[Indexes[ind]]
          else result := result + RomanNumberValues[Indexes[ind]];
     end;
   Result := Result - 100;
end;

end.
