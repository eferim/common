unit Common.Implementations;

interface

uses
  IOUtils, SysUtils, Classes, System.Generics.Collections,
  mORMot, SynCommons, SynDB, SynLog, SqlRecords,
  MormotTypes, MormotMods, Server.MormotMods, Common.Interfaces;

type

  TCommonService = class (TMormotServiceImplementer, ICommonService)
  protected
    function FGetFile (const FileName: TFileName): TServiceCustomAnswer;
    function FAdd     (SqlRecordClass: TSqlRecordClass; const fields, jsonRecord : RawUTF8): TOutcome; overload;
    function FAdd     (SqlRecord: TSqlRecord; const fields : RawUTF8): TOutcome; overload;
    function FUpdate (AUpdateRecord: TSqlRecord; const DetailIDs: TArray<TSqlRecordDetailIDs>; const Fields: RawUTF8 = ''): TOutcome;
    procedure InsertAggregates (const MasterTableName: RawUTF8; const MasterID: TID; const DetailIDs: TArray<TSqlRecordDetailIDs>);
    function FAddOrUpdate (const ARecordWithDetails: TRecordWithDetails; const MasterTableName: RawUTF8; MasterID: TID;
             var TableIndex, RecordIndex: integer; const AOperation: TCRUD): TOutcome;
    function FindPrimaryKey (rec: TSqlRecord): boolean;
    function CheckForSimulatedErrors (var Outcome: TOutcome): boolean;
  public
    function GetPublicFile (const Name: RawUTF8): TServiceCustomAnswer;

    function Debug (const Values: variant): TOutcome;
    function Ping: TOutcome;
    function SetMySettings (const varSettings: variant): TOutcome;

    function Add      (const table, fields, jsonRecord : RawUTF8; const DetailIDs: TArray<TSqlRecordDetailIDs>): TOutcome;
    function AddMany  (const table, fields : RawUTF8; const jsonRecords: TRawUTF8DynArray): TOutcome;
    function Read     (const table : RawUTF8; const QueryParameters: TQueryParameters; var jsonRecord: RawUTF8;
                       const Lock: boolean): TOutcome;
    function ReadMany (const table : RawUTF8; const QueryParameters: TQueryParameters; var jsonRecords: RawUTF8): TOutcome;
    function Unlock   (const table : RawUTF8; const ID: TID): TOutcome;
    function Update   (const table, jsonRecord : RawUTF8; const DetailIDs: TArray<TSqlRecordDetailIDs>; const Fields: RawUTF8 = ''): TOutcome;
    function UpdateAndUnlock (const table, jsonRecord : RawUTF8; const Fields: RawUTF8 = ''): TOutcome;
    function Delete   (const table : RawUTF8; const ID: TID): TOutcome;

    function FetchHistory (const ATableName, AFieldName, ACurrentValue: RawUTF8; const QueryParameters: TQueryParameters; out Values: TArray<RawUTF8>): TOutcome;
    function GetLookup    (const cl: string; out IDs: TIDDynArray; out Values: TRawUTF8DynArray; const QueryParameters: TQueryParameters): TOutcome;
    function GetExpLookup (const table: RawUTF8; out jsonRecords: RawUTF8; const QueryParameters: TQueryParameters): TOutcome;

    function AddOrUpdate (const ARecordWithDetails: TRecordWithDetails; const AOperation: TCRUD = TCRUD.Unknown): TOutcome;
    function GetTableHistory (const TableName: RawUTF8; out jsonRecordsCurrent, jsonRecordsHistory: RawUTF8;
                              const QueryParameters: TQueryParameters): TOutcome;
    function GetQuery (out Records: TSqlQueryRecordObjArray; var Parameters: TQueryParameters): TOutcome;
  public
    PublicPath: string;
  end;

  TReportService = class (TCommonService, IReportService)
  public
    class var ReportsPath: string;
    class var MaxReportFileSize: integer;
    function GetReportFile (const Name: RawUTF8): TServiceCustomAnswer;
    function SetReportFile (const FileName: RawUTF8; const FileContent: RawByteString): TOutcome;
  end;

implementation

var
  ExpLookupSQLs: TDictionary <TSQLRecordClass, RawUTF8>;
  lstForHistoryOldValues: TList<RawUTF8>;

function TCommonService.FGetFile(const FileName: TFileName): TServiceCustomAnswer;
begin
  result := default (TServiceCustomAnswer);
  if FileExists (FileName)
     then begin
            var fs := TFileStream.Create(FileName, fmOpenRead);
            try
              result.Content := StreamToRawByteString(fs);
              result.Status := HTTP_SUCCESS;
              result.Header := BINARY_CONTENT_TYPE;
            finally
              fs.Free;
            end;
          end
     else begin
            Result.Header := TEXT_CONTENT_TYPE_HEADER;
            result.Status := HTTP_NOTFOUND;
          end;
end;

function TCommonService.FindPrimaryKey(rec: TSqlRecord): boolean;
var
  Filter: TQueryFilter;
  FieldInfo: TFieldInfo;
begin
  Filter := default (TQueryFilter);
  for FieldInfo in rec.FieldInfoList do
    if FieldInfo.IsAlternateUK then
       Filter.Add(FieldInfo.Name, TFilterOperator.Equal, rec.GetFieldVariant(FieldInfo.Name));

  if Filter.Count = 0
     then rec.IDValue := NULL_ID
     else rec.IDValue := RestServer.OneFieldValueInt64(rec.RecordClass, 'RowID', Filter.SQL);

  result := rec.IDValue.IsNotNULL;
end;

function TCommonService.GetPublicFile(const Name: RawUTF8): TServiceCustomAnswer;
begin
  result := default (TServiceCustomAnswer);

  if (PublicPath = '') or Name.AsString.Contains ('..') or Name.AsString.Contains (':')
    then
      begin
        Result.Header := TEXT_CONTENT_TYPE_HEADER;
        result.Status := HTTP_NOTFOUND;
        exit;
      end;

  result := FGetFile(TPath.Combine(PublicPath, Name.AsString));
end;

function TCommonService.GetQuery(out Records: TSqlQueryRecordObjArray; var Parameters: TQueryParameters): TOutcome;
begin
  // for queries that are not worthy of being views? hmmm
  exit (ServerErrorOutcome(seUnknownTable));
end;

var
  ehh: integer = 0;

function TCommonService.Debug(const Values: variant): TOutcome;
begin
  result := default (TOutcome);
  case Values of
    1 : if 15/ehh > 1 // divide by zero to force a crash on server
          then result.SetSuccess
          else result.SetError(999);
    2 : ServiceContext.Request.ForceServiceResultAsXMLObject := true;
    else result.SetSuccess;
  end;
end;

function TCommonService.FetchHistory (const ATableName, AFieldName, ACurrentValue: RawUTF8; const QueryParameters: TQueryParameters; out Values: TArray<RawUTF8>): TOutcome;
var
  Limit: integer;
begin
  result := default (TOutcome);
  Limit := QueryParameters.Page.Limit;
  if Limit = 0
    then Limit := 15;
  {TODO -oOwner -cGeneral : multiple table/field if same field content can appear across tables or even in same table across fields}

  var sql := FormatUTF8 ('SELECT distinct % FROM % WHERE UPPER(%) LIKE % AND % <> '''' ',
                         [AFieldName, ATableName, AFieldName, QuotedStr(UpperCaseUnicode(ACurrentValue + '%')), AFieldName]);
  if QueryParameters.WhereConditionSql <> ''
    then sql := sql + ' AND ' + QueryParameters.WhereConditionSql;

  sql := sql + #13 + 'ORDER BY 1 ASC limit ' + StringToUTF8 (Limit.ToString);
  var rows := RestServer.DataBase.Execute (sql, []);
  var index := 0;
  SetLength (Values, Limit);

  while rows.step do
    begin
      if (index < Limit)
        then
          begin
            Values[index] := rows.ColumnUTF8(0);
            inc (index);
          end
        else TSQLLog.Add.Log(TSynLogInfo.sllCustom1, 'History count wtf');
    end;
  SetLength (Values, index);
  result.SetSuccess;
end;

function TCommonService.GetTableHistory (const TableName: RawUTF8; out jsonRecordsCurrent, jsonRecordsHistory: RawUTF8;
                                         const QueryParameters: TQueryParameters): TOutcome;
var
  sqlWhere: RawUTF8;
  SqlRecordClass: TSqlRecordClass;
  oaChangeLog: TArray<TObject>;
  obj: TObject;
  sb: TStringBuilder;
  Params: TQueryParameters;
  NeedsComma: boolean;
begin
  Params := QueryParameters;

  // get history
  Params.Filter.Add('TableName', TFilterOperator.Equal, TableName);
  Params.ApplyGeneralFilter(lstForHistoryOldValues, FilterOperatorAsSQL[TFilterOperator.Like]);
  if Params.ID.IsNotNULL
     then Params.Filter.Add('RecordID', TFilterOperator.Equal, Params.ID);
  sqlWhere := Params.BuildSql ([TSqlSegment.WhereCondition,
                                       TSqlSegment.OrderBy, TSqlSegment.Limit]);
  RestServer.RetrieveListObjArrayDirect (oaChangeLog, TSqlChangeLog, sqlWhere);

  // get current
  SqlRecordClass := model.Table[SynCommons.UpperCase(TableName)];
  Params := QueryParameters;
  Params.ApplyGeneralFilter(SqlRecordClass.GeneralFilterFields, FilterOperatorAsSQL[TFilterOperator.Like]);
  if Params.ID.IsNotNULL
     then Params.Filter.Add('ID', TFilterOperator.Equal, Params.ID);
  sqlWhere := Params.BuildSql ([TSqlSegment.WhereCondition,
                                       TSqlSegment.OrderBy, TSqlSegment.Limit]);

  result := RestServer.MultiFieldValuesEx (SqlRecordClass, ''{aCustomFieldsCSV}, sqlWhere, jsonRecordsCurrent);
  if not result.Success then exit;

  sb := TStringBuilder.Create;
  try
    sb.Clear;
    sb.Append('[');
    NeedsComma := false;
    for obj in oaChangeLog do
        begin
          if NeedsComma then sb.Append(',');
          sb.Append (TSqlChangeLog(obj).OldValues);
          NeedsComma := true;
          obj.Free;
        end;
    sb.Append(']');
    jsonRecordsHistory.AsString := sb.ToString;

    TSQLLog.Add.Log(TSynLogInfo.sllCustom1, #13#13);
    TSQLLog.Add.Log(TSynLogInfo.sllCustom1, jsonRecordsCurrent.AsString);
    TSQLLog.Add.Log(TSynLogInfo.sllCustom1, #13#13);
    TSQLLog.Add.Log(TSynLogInfo.sllCustom1, jsonRecordsHistory.AsString);
    TSQLLog.Add.Log(TSynLogInfo.sllCustom1, #13#13);
  finally
    sb.Free;
  end;

  result.SetSuccess;
end;

function TCommonService.GetExpLookup(const table: RawUTF8; out jsonRecords: RawUTF8; const QueryParameters: TQueryParameters): TOutcome;
var
  IRows: ISQLDBRows;
  src: TSQLRecordClass;
  sql : RawUTF8;
begin
  result := default (TOutcome);
  src := Model.Table[StringToUTF8 (copy (table.AsString, 5))];
  if src = nil then exit (ServerErrorOutcome(seUnknownTable));

  if not ExpLookupSQLs.TryGetValue (src, sql)
     then exit (ServerErrorOutcome(seExpLookupNotDefined));

  IRows := RestServer.DataBase.Execute (sql + QueryParameters.FullStatementSql, []);
  jsonRecords := IRows.FetchAllAsJSON(false);
  result.SetSuccess;
end;

function TCommonService.GetLookup(const cl: string; out IDs: TIDDynArray; out Values: TRawUTF8DynArray;
                                     const QueryParameters: TQueryParameters): TOutcome;
var
  Query: TSQLTable;
begin
  result := default (TOutcome);
  var src := Model.Table[StringToUTF8 (copy (cl, 5))];
  if src = nil then exit (ServerErrorOutcome(seUnknownTable));
  var LocParams := QueryParameters;
  LocParams.OrderBy := 'LKP';

  if LocParams.GeneralFilter <> ''
     then LocParams.ApplyGeneralFilter (src.GeneralFilterFields, FilterOperatorAsSQL[TFilterOperator.Like]);

  var Fields: RawUTF8 := 'RowID, ';
  var first := true;

//  if length (src.GeneralFilterFields.Count) = 0 then
//    raise Exception.Create('No general filter fields defined for ' + cl);

  for var field in src.GeneralFilterFields do
    begin
      if not first then Fields := Fields + ' || '' ['' || ';
      Fields := Fields + FormatUTF8 ('coalesce (%, '''')', [field]);
      if not first then Fields := Fields + '|| '']''';
      first := false;
    end;

  Query := RestServer.MultiFieldValues (src, fields + ' as LKP', LocParams.MormotSql);

  try
    setlength (IDs, Query.RowCount);
    setlength (Values, Query.RowCount);
    var index := 0;
    while Query.Step do
      begin
        IDs[index] := Query.FieldAsInteger(0);
        Values[index] := Query.FieldAsRawUTF8(1);
        inc (index);
      end;
    result.SetSuccess;
  finally
    Query.Free;
  end;
end;

function TCommonService.Ping: TOutcome;
begin
  result.SetDefaultSuccess;
end;

function TCommonService.Read (const table : RawUTF8; const QueryParameters: TQueryParameters; var jsonRecord: RawUTF8; const Lock: boolean): TOutcome;
var
  r, rcrud: TSqlRecord;
begin
  if CheckForSimulatedErrors (result) then exit;

  result := default (TOutcome);
  rcrud := nil;
  var tt := model.Table[SynCommons.UpperCase(table)];
  if not tt.CrudAllowed (TCRUD.Read, ServiceContext.Request.SessionGroup)
     then exit (result.SetError(sePermissionDenied));

  r := tt.Create;
  try
    var TempID := QueryParameters.ID;
    if TempID.IsNULL
       then TempID := RestServer.OneFieldValueInt64(tt, 'RowID', QueryParameters.WhereConditionSql);

    if TempID.IsNull
       then exit (result.SetError(seRecordNotFound));

    r.IDValue := TempID;

    if Lock
      then
        if r.IsView
          then
            begin
              rcrud := r.crudClass.Create;
              rcrud.IDValue := TempID;
              RestServer.ReadLock(rcrud, result); {TODO -oOwner -cGeneral : is there a better way, this is an extra read just for locking because crud is somtimes done on view and sometimes on table, inconsistent...}
              if result.Success then RestServer.Retrieve (TempID, r, result, false);
            end
          else RestServer.ReadLock(r, result)
      else RestServer.Retrieve (TempID, r, result, false);

//    RestServer.Retrieve (TempID, r, result, Lock and not r.IsView); // this would lock the view if its a view...
//    if result.Success and Lock and r.IsView then
//      begin
//        RestServer.ReadLock (tt.crudClass, TempID, result); // lock crud table instead
//        if not result.Success
//           then exit (ServerErrorOutcome(seRecordAlreadyLockedByMormot));
//      end;

    if result.Success
      then jsonRecord := r.GetJSONValues (true, true, r.RecordProps.CopiableFieldsBits{GetNonVoidFields}, []);
  finally
    r.Free;
    rcrud.Free;
  end;
end;

function TCommonService.ReadMany(const table: RawUTF8; const QueryParameters: TQueryParameters; var jsonRecords: RawUTF8): TOutcome;
begin
  if CheckForSimulatedErrors (result) then exit;

//  RestServer.EngineList()
  result := default (TOutcome);

  var tt := model.Table[SynCommons.UpperCase(table)];

  if QueryParameters.GeneralFilter <> ''
     then QueryParameters.ApplyGeneralFilter (tt.GeneralFilterFields, FilterOperatorAsSQL[TFilterOperator.Like]);

//  {$IFNDEF DEBUG}
//  if (tt = AuthUserClass)
//     then QueryParameters.Filter.Add('Developer', TFilterOperator.NotEqual, 1);
//  {$ENDIF}

  if RestServer.DirectRead then QueryParameters.Filter.ReplaceField('RowID', 'ID');
  var sqlWhere := QueryParameters.BuildSql ([TSqlSegment.WhereCondition,
                                       TSqlSegment.OrderBy, TSqlSegment.Limit]);

  result := RestServer.MultiFieldValuesEx (tt, ''{aCustomFieldsCSV}, sqlWhere, jsonRecords);
end;

function TCommonService.SetMySettings(const varSettings: variant): TOutcome;
begin
  result := default (TOutcome);

  var User := AuthUserClass.Create;
  try
    User.IDValue := ServiceContext.Request.SessionUser;
    RestServer.Retrieve(User, result);
    if result.Success then
       begin
         User.Settings := varSettings;
         result := RestServer.UpdateEx(User, 'Settings');
       end;
  finally
    User.Free;
  end;
end;

function TCommonService.Unlock(const table: RawUTF8; const ID: TID): TOutcome;
begin
  if CheckForSimulatedErrors (result) then exit;

  result := default (TOutcome);

  var tt := model.Table[SynCommons.UpperCase(table)];
  result.Success := RestServer.UnLock(tt.crudClass, ID);
  if result.Success
     then result.ID := ID;
end;

procedure TCommonService.InsertAggregates (const MasterTableName: RawUTF8; const MasterID: TID; const DetailIDs: TArray<TSqlRecordDetailIDs>);
var
  iTable, iID: integer;
  MasterIDField, DetailIDField: RawUTF8;
  AggClass: TSQLRecordClass;
  Agg: TSqlRecord;
begin // Master: PlanItem Aggregate: PlanItemBeneficiary Detail: Beneficiary
  for iTable := low (DetailIDs) to high (DetailIDs) do
    begin
      if length (DetailIDs[iTable].IDs) = 0 then continue;

      AggClass := model.Table[SynCommons.UpperCase(DetailIDs[iTable].AggregateTableName)];
      agg := AggClass.Create;
      try
        for iID := low (DetailIDs[iTable].IDs) to high (DetailIDs[iTable].IDs) do
          begin

            if DetailIDs[iTable].MasterIDField = ''
              then MasterIDField := MasterTableName + 'ID'
              else MasterIDField := DetailIDs[iTable].MasterIDField;

            if DetailIDs[iTable].DetailIDField = ''
              then DetailIDField := DetailIDs[iTable].DetailTableName + 'ID'
              else DetailIDField := DetailIDs[iTable].DetailIDField;

            agg.SetFieldVariant(MasterIDField.AsString, MasterID);
            agg.SetFieldVariant(DetailIDField.AsString, DetailIDs[iTable].IDs[iID]);
            FAdd(agg, '');
          end;
      finally
        agg.Free;
      end;
    end;
end;

function TCommonService.FUpdate (AUpdateRecord: TSqlRecord; const DetailIDs: TArray<TSqlRecordDetailIDs>; const Fields: RawUTF8 = ''): TOutcome;
var
  OldRecord : TSqlRecord;
  FFields: RawUTF8;
  lst: TRestrictedMirrorFieldList;
  lstRestrictedFields: TRestrictedFieldList;
  rmf: TRestrictedMirrorField;
  LockedMasters: TDictionary <TSqlRecordClass, TID>;
//  tt : TSqlRecordClass;

  procedure FetchOldRecord;
  begin
     OldRecord := AUpdateRecord.Create;

     if not RestServer.Retrieve(AUpdateRecord.IDValue, OldRecord) then
       result := ServerErrorOutcome(seRecordNotFound);
  end;

begin
  if not AUpdateRecord.CrudAllowed (TCRUD.Update, ServiceContext.Request.SessionGroup)
     then exit (ServerErrorOutcome(sePermissionDenied));

  if Fields = ''
     then FFields := AUpdateRecord.FieldInfoList.WritableFieldsAsCSV
     else FFields := Fields; {TODO -oOwner -cGeneral : provera read only, mozda array of utf kao ulaz da bude lakse}

  OldRecord := nil;
  LockedMasters := nil;
  AUpdateRecord.BeforeWrite;


  try
    var s := AUpdateRecord.ValidateEx(Fields, RestServer.ImportInProgress);
    if s <> ''
       then exit (ServerDBErrorOutcome(seValidationFailed, AUpdateRecord.SQLTableName.AsString, '', s));

    // check mirrors for the case where this is master table
    // if this (master) table has any fields which are mirrored in other (detail) tables, prohibit the update if:
    // a) such a field in master record is modified
    // AND
    // b) there are already detail records, meaning their mirror field would no longer be valid
    // in the future perhaps allow simultaneous change of both but for now...

    if model.RestrictedMirrorFieldsOfMaster.TryGetValue(AUpdateRecord.RecordClass, lst) then
       begin
         FetchOldRecord;
         if not result.Success then exit;


         for rmf in lst do
           if     OldRecord.GetFieldVariantValue (rmf.OriginalFieldName.AsString) <>
              AUpdateRecord.GetFieldVariantValue (rmf.OriginalFieldName.AsString) then
                if RestServer.TableHasForeignKey (rmf.TableClass, rmf.ForeignKeyName, AUpdateRecord.IDValue) then
                   exit (ServerDBErrorOutcome(seMirroredFieldRestriction, rmf.TableClass.SQLTableName.AsString, '', ''));
       end;

    // check mirrors for the case where this is detail table
    // if table has any fields which are mirrors of a foreign table field:
    // a) lock the foreign table for update,
    // b) check if detail mirror field value matches

    if model.RestrictedMirrorFieldsOfDetail.TryGetValue(AUpdateRecord.RecordClass, lst) then
       begin
         LockedMasters := TDictionary <TSqlRecordClass, TID>.Create;
         for rmf in lst do
           begin
             var MasterRecord := rmf.ForeignTableClass.Create;
             try
               var ForeignID: TID;
               ForeignID := AUpdateRecord.GetFieldVariantValue (rmf.ForeignKeyName.AsString);
               if not RestServer.Retrieve(ForeignID, MasterRecord, true)
                  then exit (ServerDBErrorOutcome(seRecordNotFound, MasterRecord.SQLTableName.AsString, '', s));

               LockedMasters.Add (rmf.ForeignTableClass, ForeignID);
               if MasterRecord.GetFieldVariantValue (rmf.OriginalFieldName) <> AUpdateRecord.GetFieldVariantValue (rmf.MirrorFieldName) then
                 if rmf.AutoFillIfNull and (AUpdateRecord.FieldByName (rmf.MirrorFieldName.AsString).SQLFieldType = sftID)
                          and (AUpdateRecord.GetFieldVariantValue (rmf.MirrorFieldName) = 0)
                       then AUpdateRecord.SetFieldVariant (rmf.MirrorFieldName.AsString, MasterRecord.GetFieldVariantValue (rmf.OriginalFieldName.AsString))
                       else exit (ServerDBErrorOutcome(seIncorrectMirrorValue, MasterRecord.SQLTableName,
                                  rmf.MirrorFieldName, s));
             finally
               MasterRecord.Free;
             end;
           end;
       end;

    // check fields which are not allowed to be updated if the record is referenced by another table's record
    if model.RestrictedFields.TryGetValue(AUpdateRecord.RecordClass, lstRestrictedFields) then
      begin
        if OldRecord = nil then
           FetchOldRecord;
        if not result.Success then exit;

        for var rf in lstRestrictedFields do
           if OldRecord.FieldIsDifferent (AUpdateRecord, rf.FieldName) then
                if RestServer.TableHasForeignKey (rf.FTClass, rmf.ForeignKeyName, AUpdateRecord.IDValue) then
                   exit (ServerDBErrorOutcome(seRestrictedField, rmf.TableClass.SQLTableName, rf.FieldName));
      end;

    result := RestServer.UpdateEx (AUpdateRecord, Fields);

    if result.Success then
      for var det in DetailIDs do
        RestServer.DataBase.ExecuteNoResult(FormatUTF8 ('delete from % where % = %', [det.AggregateTableName, det.MasterIDField, result.ID]), []);

    InsertAggregates (AUpdateRecord.SQLTableName, result.ID, DetailIDs);

    if result.Success
       then RestServer.UnLock(AUpdateRecord);
  finally
    OldRecord.Free;
    if LockedMasters <> nil then
      begin
        for var pair in LockedMasters do
          RestServer.UnLock(pair.Key, pair.Value);
        LockedMasters.Free;
      end;

    if not result.Success then
      begin
        LogWarning (Result.Description);
        //LogWarning (jsonRecord.AsString);
      end;
  end;
end;

function TCommonService.Update (const table, jsonRecord : RawUTF8; const DetailIDs: TArray<TSqlRecordDetailIDs>; const Fields: RawUTF8 = ''): TOutcome;
var
//  OldRecord,
  UpdatedRecord: TSqlRecord;
//  FFields: RawUTF8;
//  lst: TRestrictedMirrorFieldList;
//  lstRestrictedFields: TRestrictedFieldList;
//  rmf: TRestrictedMirrorField;
//  LockedMasters: TDictionary <TSqlRecordClass, TID>;

  tt : TSqlRecordClass;

begin
  if CheckForSimulatedErrors (result) then exit;

  result := default (TOutcome);

//  if Fields = ''
//     then FFields := tt.FieldInfoList.WritableFieldsAsCSV
//     else FFields := Fields; {TODO -oOwner -cGeneral : provera read only, mozda array of utf kao ulaz da bude lakse}
  tt := model.Table[SynCommons.UpperCase(table)];
  UpdatedRecord := tt.CreateFrom(jsonRecord);
  try
    result := FUpdate (UpdatedRecord, DetailIDs, Fields);
  finally
    UpdatedRecord.Free;
  end;
end;

function TCommonService.UpdateAndUnlock(const table, jsonRecord: RawUTF8; const Fields: RawUTF8 = ''): TOutcome;
var
  r: TSqlRecord;
begin
  if CheckForSimulatedErrors (result) then exit;

  result := default (TOutcome);

  var tt := model.Table[SynCommons.UpperCase(table)];
  if not tt.CrudAllowed (TCRUD.Update, ServiceContext.Request.SessionGroup)
     then exit (result.SetError(sePermissionDenied));

  r := tt.CreateFrom(jsonRecord);
  try
    var s := r.ValidateEx(Fields, RestServer.ImportInProgress);
    if s <> ''
       then exit (result.SetError(seValidationFailed, s));
    result := RestServer.UpdateAndUnlock(r);
  finally
    r.Free;
    if not result.Success then
      begin
        LogWarning (Result.Description);
        LogWarning (jsonRecord.AsString);
      end;
  end;
end;

function TCommonService.Add (const table, fields, jsonRecord : RawUTF8; const DetailIDs: TArray<TSqlRecordDetailIDs>): TOutcome;
begin
  if CheckForSimulatedErrors (result) then exit;

  result := default (TOutcome);

  var tt := model.Table[SynCommons.UpperCase(table)];
  if not tt.CrudAllowed (TCRUD.Create, ServiceContext.Request.SessionGroup)
     then exit (result.SetError(sePermissionDenied));
  result := FAdd (tt, fields, jsonRecord);
  if result.Success then InsertAggregates(tt.SQLTableName, result.ID, DetailIDs);
end;

function TCommonService.FAdd(SqlRecord: TSqlRecord; const fields : RawUTF8): TOutcome;
var
  lst: TRestrictedMirrorFieldList;
  rmf: TRestrictedMirrorField;
  LockedMasters: TDictionary <TSqlRecordClass, TID>;
begin
  LockedMasters := nil;
  SqlRecord.BeforeWrite;
  try
    var s := SqlRecord.ValidateEx(Fields, RestServer.ImportInProgress);
    if s <> ''
       then exit (result.SetError(seValidationFailed, s));

    if model.RestrictedMirrorFieldsOfDetail.TryGetValue(SqlRecord.RecordClass, lst) then
       begin
         LockedMasters := TDictionary <TSqlRecordClass, TID>.Create;
         for rmf in lst do
           begin
             var MasterRecord := rmf.ForeignTableClass.Create;
             try
               var ForeignID: TID;
               ForeignID := SqlRecord.GetFieldVariantValue (rmf.ForeignKeyName.AsString);
               if not RestServer.Retrieve(ForeignID, MasterRecord, true)
                  then exit (ServerDBErrorOutcome (seRecordNotFound, MasterRecord.SQLTableName, rmf.ForeignKeyName));
               LockedMasters.Add (rmf.ForeignTableClass, ForeignID);
               if MasterRecord.GetFieldVariantValue (rmf.OriginalFieldName) <> SqlRecord.GetFieldVariantValue (rmf.MirrorFieldName) then
                 if rmf.AutoFillIfNull and (SqlRecord.FieldByName (rmf.MirrorFieldName.AsString).SQLFieldType = sftID)
                          and (SqlRecord.GetFieldVariantValue (rmf.MirrorFieldName) = 0)
                       then SqlRecord.SetFieldVariant (rmf.MirrorFieldName.AsString, MasterRecord.GetFieldVariantValue (rmf.OriginalFieldName.AsString))
                       else exit (ServerDBErrorOutcome(seIncorrectMirrorValue, MasterRecord.SQLTableName, rmf.MirrorFieldName));
             finally
               MasterRecord.Free;
             end;
           end;
       end;

    result := RestServer.AddWithFields(SqlRecord, fields);
  finally
    if LockedMasters <> nil then
      begin
        for var pair in LockedMasters do
          RestServer.UnLock(pair.Key, pair.Value);
        LockedMasters.Free;
      end;
    if not result.Success then
      begin
        LogWarning (Result.Description);
//        LogWarning (jsonRecord.AsString);
      end;
  end;
end;

function TCommonService.FAdd(SqlRecordClass: TSqlRecordClass; const fields, jsonRecord : RawUTF8): TOutcome;
begin
  var rec := SqlRecordClass.CreateFrom(jsonRecord);
  try
    result := FAdd (rec, fields);
  finally
    rec.Free;
  end;
end;

function TCommonService.AddMany(const table, fields: RawUTF8; const jsonRecords: TRawUTF8DynArray): TOutcome;
var
  RecordCount: integer;
begin
  result := default (TOutcome);

  var tt := model.Table[SynCommons.UpperCase(table)];
  if not tt.CrudAllowed (TCRUD.Create, ServiceContext.Request.SessionGroup)
     then exit (result.SetError(sePermissionDenied));

  result.SetSuccess;
  RecordCount := 0;
  for var rec in jsonRecords do
    begin
      result := FAdd (tt, fields, rec);
      if not result.Success then break;
      inc (RecordCount);
    end;
  result.RecordCount := RecordCount;
end;


function TCommonService.AddOrUpdate (const ARecordWithDetails: TRecordWithDetails; const AOperation: TCRUD = TCRUD.Unknown): TOutcome;
var
  Operation: TCRUD;
  TableIndex, RecordIndex: integer;
  tt: TSqlRecordClass;
  ti: TRecordWithDetailsTableInfo;
begin
  for ti in ARecordWithDetails.Tables do
    begin
      tt := model.Table[SynCommons.UpperCase(ti.TableName)];
      if not tt.CrudAllowed (TCrud.Create, ServiceContext.Request.SessionGroup)
         or not tt.CrudAllowed (TCrud.Update, ServiceContext.Request.SessionGroup)
         then exit (ServerErrorOutcome(sePermissionDenied));
    end;

  tt := model.Table[SynCommons.UpperCase(ARecordWithDetails.TableName)];

  var SqlRecord := tt.CreateFrom(ARecordWithDetails.jsonRecord);

  try
    if (AOperation = TCRUD.Unknown)
      then
        begin
          if SqlRecord.IDValue.IsAuto
            then FindPrimaryKey(SqlRecord);
          if SqlRecord.IDValue.IsNULL
            then Operation := TCRUD.Create
            else Operation := TCRUD.Update
        end
      else Operation := AOperation;

    if Operation = TCRUD.Update
       then result := FUpdate (SqlRecord, [], ARecordWithDetails.Fields)
       else begin
              result := FAdd (SqlRecord, ARecordWithDetails.Fields);
              SqlRecord.IDValue := result.ID;
            end;

    TableIndex := 0;
    RecordIndex := 0;
    if result.Success and (ARecordWithDetails.TableCount > 0)
       then result := FAddOrUpdate(ARecordWithDetails, ARecordWithDetails.TableName, SqlRecord.IDValue, TableIndex, RecordIndex, AOperation);
  finally
    SqlRecord.Free;
  end;
end;

function TCommonService.CheckForSimulatedErrors(var Outcome: TOutcome): boolean;
begin
  result := RestServer.SimulateOneError;
  RestServer.SimulateOneError := false;

  if result then Outcome := ServerErrorOutcome (seSimulatedError);

  if RestServer.SimulateOneException then
    begin
      RestServer.SimulateOneException := false;
      raise Exception.Create('Simulated exception');
    end;
end;

function TCommonService.FAddOrUpdate (const ARecordWithDetails: TRecordWithDetails; const MasterTableName: RawUTF8;
         MasterID: TID; var TableIndex, RecordIndex: integer; const AOperation: TCRUD): TOutcome;
var
  Operation: TCRUD;
  SqlRecord: TSqlRecord;
  Level, Count: integer;
  LastID: TID;
  LastTableName, ForeignKeyName: RawUTF8;
  sValidation: string;
  IDs: TIDDynArray;
  Filter: TQueryFilter;
begin
  // go through all tables on same level, if level doesnt change it means all records are equal
  // i.e. details of the same master, not sub-details
  Level := ARecordWithDetails.Tables[TableIndex].Level;
  LastID := NULL_ID;
  LastTableName := '';

  while (TableIndex < ARecordWithDetails.TableCount) and (ARecordWithDetails.Tables[TableIndex].Level = Level) do
    begin
      Count := ARecordWithDetails.Tables[TableIndex].Count;
      var tt := model.Table[SynCommons.UpperCase(ARecordWithDetails.Tables[TableIndex].TableName)];
  //      if not tt.CrudAllowed (Operation, ServiceContext.Request.SessionGroup)
  //         then exit (ServerErrorOutcome(sePermissionDenied));         this was checked in advance

      ForeignKeyName := ARecordWithDetails.Tables[TableIndex].ForeignKeyName;
      if ForeignKeyName = ''
         then ForeignKeyName := MasterTableName + 'ID';
      Filter := default (TQueryFilter);
      SetLength (IDs, Count);
      while (Count > 0) and result.Success do
        begin
          SqlRecord := tt.CreateFrom(ARecordWithDetails.Records[RecordIndex]);
          try
            SqlRecord.SetFieldVariant(ForeignKeyName.AsString, MasterID);

            if (AOperation = TCRUD.Unknown)
              then
                begin
                  if SqlRecord.IDValue.IsAuto
                    then FindPrimaryKey(SqlRecord);
                  if SqlRecord.IDValue.IsNULL
                    then Operation := TCRUD.Create
                    else Operation := TCRUD.Update
                end
              else Operation := AOperation;

              sValidation := SqlRecord.ValidateEx('', RestServer.ImportInProgress);
              if sValidation <> ''
                 then exit (result.SetError(seValidationFailed, sValidation));

            if Operation = TCRUD.Update
              then result := FUpdate (SqlRecord, [], ARecordWithDetails.Fields)
              else begin
                     result := FAdd (SqlRecord, ARecordWithDetails.Fields);
                     end;
            LastID := SqlRecord.IDValue;
            IDs[Count-1] := LastID;
            LastTableName := SqlRecord.SQLTableName;
            dec (Count);
            inc (RecordIndex);
          finally
            SqlRecord.Free;
          end;
        end;

      if ARecordWithDetails.Tables[TableIndex].Count > 0
         then Filter.AddForIDValues('ID', TFilterOperator.NotInValues, IDs);
      Filter.Add(ForeignKeyName, TFilterOperator.Equal, MasterID);
      RestServer.DataBase.ExecuteNoResult(FormatUTF8 ('delete from % where %',
                                    [ARecordWithDetails.Tables[TableIndex].TableName, Filter.SQL]), []);

      inc (TableIndex);
    end;

  if (TableIndex >= ARecordWithDetails.TableCount) then exit; // all records done

  if (ARecordWithDetails.Tables[TableIndex].Level < Level) then exit; // sub branch finished

  result := FAddOrUpdate (ARecordWithDetails, LastTableName, LastID, TableIndex, RecordIndex, AOperation); // enter sub branch
end;


function TCommonService.Delete(const table: RawUTF8; const ID: TID): TOutcome;
begin
  if CheckForSimulatedErrors (result) then exit;

  result := default (TOutcome);

  var tt := model.Table[SynCommons.UpperCase(table)];
  if not tt.CrudAllowed (TCRUD.Delete, ServiceContext.Request.SessionGroup)
     then exit (result.SetError(sePermissionDenied));
  if (tt = model.AuthUserClass) and (ID = SessionUserID) //dissalow self-delete
     then exit (result.SetError(sePermissionDenied));

  result := RestServer.DeleteEx(tt, ID);
  result.ID := ID;
end;

{ TReportService }

function TReportService.GetReportFile(const Name: RawUTF8): TServiceCustomAnswer;
begin
  result := default (TServiceCustomAnswer);

  if Name.AsString.Contains ('..') or Name.AsString.Contains (':') or Name.AsString.Contains ('\')
    then
      begin
        Result.Header := TEXT_CONTENT_TYPE_HEADER;
        result.Status := HTTP_NOTFOUND;
        exit;
      end;

  result := FGetFile(TPath.Combine(ReportsPath, Name.AsString));
end;

function TReportService.SetReportFile(const FileName: RawUTF8; const FileContent: RawByteString): TOutcome;
var
  fn: TFileName;
  fs: TFileStream;
  str: TStream;
begin
  if ReportsPath = ''
     then raise Exception.Create('Report path not set!');

  var sFileName := FileName.AsString;
  if sFileName.Contains ('..') or sFileName.Contains (':') or sFileName.Contains ('\') or sFileName.Contains ('/')
     or not TPath.HasValidFileNameChars(sFileName, false)
     then exit (ServerErrorOutcome(sePermissionDenied, 'Invalid file name'));

  if FileContent = ''
     then exit (ServerErrorOutcome(sePermissionDenied, 'File is empty'));

  fs := nil;
  str := nil;

  // try to delete existing
  try
    fn := TPath.Combine (ReportsPath, sFileName);
    if FileExists (fn) then
      begin
        TFile.Delete (fn);
        if FileExists (fn) then exit (ServerErrorOutcome(sePermissionDenied, 'Unable to delete file'));
      end;

    str := RawByteStringToStream(FileContent);
    str.Position := 0;
    if str.Size > MaxReportFileSize
      then exit (ServerErrorOutcome(seValidationFailed, format ('Maksimalna du�ina fajla je %dMB', [MaxReportFileSize div (1024*1024)])));

    fs := TFileStream.Create (fn, fmCreate);
    fs.CopyFrom(str, str.Size);
    result.Success := true;
  finally
    fs.Free;
    str.Free;
  end;
end;


initialization
  ExpLookupSQLs := TDictionary <TSQLRecordClass, RawUTF8>.Create;

  TInterfaceFactory.RegisterInterfaces ([TypeInfo(ICommonService)]);
  TInterfaceFactory.RegisterInterfaces ([TypeInfo(IReportService)]);
  lstForHistoryOldValues := TList<RawUTF8>.Create;
  lstForHistoryOldValues.Add('OldValues');

finalization
  ExpLookupSQLs.Free;
  lstForHistoryOldValues.Free;

end.
