unit SqlRecords;

interface

uses
  SysUtils, System.Generics.Collections, Classes, System.Types, System.Contnrs,
  Variants, math, Common.Utils, Transliteration,
  {$ifdef MSWINDOWS}
  Windows, //for inline of synlocker...
  {$endif}
  MormotTypes, MormotMods, MormotAttributes,
  SynCommons, mormot;

const
  Lookup_Timeout_Minutes = 0.5;

type

  TSQLRecordClassArray = TArray <TSQLRecordClass>;
  TSqlRecordArray = TArray<TSqlRecord>;
  TIDIntegerDictionary = TDictionary <TID, integer>;

  TLookup = class
  protected
  public
    CreationTime: TDateTime;
    IDIndexes: TIDIntegerDictionary;
    Items: TDictionary <TID, RawUTF8>;
    Reverse: TDictionary <RawUTF8, TID>;
    IDs: TIDDynArray;
    Values: TRawUTF8DynArray;
    //SortedValues: TRawUTF8DynArray;
    SortedValues : TStringList;
    MasterID: TID;
    procedure AfterConstruction; override;
    procedure Add (const AIDs: TIDDynArray; const AValues: TRawUTF8DynArray); reintroduce;
    function IndexOf (const ID: TID): integer;
    function Count: integer;
    destructor Destroy; override;
  end;

{  ILookupSource = interface
    function ResolveAsString (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: string): boolean;
    //function ResolveAsVariant (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID): variant;
  end;
}

  TLookups = class (TNonARCInterfacedObject{, ILookupSource})
  private
    Tables, Alternatives: TSQLRecordClassArray;
    function GetLookup(cl: TSQLRecordClass): TLookup;
  public
    Dictionary: TObjectDictionary <TSQLRecordClass, TLookup>;
    function ResolveAsString (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: string): boolean;
//    function ResolveAsVariant (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: variant): boolean;

    constructor Create (const ATables, AAlternatives: TSQLRecordClassArray);
    destructor Destroy; override;
    function HasValidLookup (cl: TSQLRecordClass): boolean;
    property Lookup [cl: TSQLRecordClass]: TLookup read GetLookup; default;
  end;

  IFieldValueResolver = interface
    function GetFieldDisplayValue (const SqlRecord: TSQLRecord; const FieldName: string): variant;
    function AuthUserClass: TSQLRecordClass;
  end;

  TSqlRecords = class
  private
    class var AllSqlRecords: TList<TObject>;
    class var NextID: integer;
    class constructor Create;
    class destructor Destroy;
    function GetFieldDisplayValue(index: integer; const FieldName: string): variant;
    function GetIsEmpty: boolean;
  var
    FInitTime: TDateTime;
    FCopyNo: integer;
    FID: integer;
    FSqlRecordClass: TSQLRecordClass;
    function GetCount: integer;
    type
      TSqlRecordsIndex = class
      private
        UseLookup, IsTextField: boolean;
        //LookupClass: TSQLRecordClass;
        function GetRecords(index: integer; Ascending: boolean = true): TSqlRecord;
        procedure FillRecordIDs;
        function FFind(const Value: variant; var FromIndex, ToIndex: integer): integer;
        function FFindText(const Value: variant; var FromIndex, ToIndex: integer): integer;
        function FindOrNearby(const Value: variant; var Index, FromIndex, ToIndex: integer): TSqlRecord; overload;
        function GetCount: integer;
      public
        FFieldName: string;
        IndexArray: TIntegerDynArray;
        RecordIDs: TIDIntegerDictionary; // maps record IDs to index in IndexArray
                                               // to find a record by ID in this index
        DynArray: TDynArray;
        FOwner: TSqlRecords;
        constructor Create (const AOwner: TSqlRecords; const AFieldName: string);
        destructor Destroy; override;
        procedure Init;
        procedure Sort(L, R: Integer; const Ascending: boolean = true);
        procedure SortText(L, R: Integer; const Ascending: boolean = true);

        procedure QuickSort(const Ascending: boolean = true);

        function Find(const Value: variant): TSqlRecord; overload;
        function Find(const Value: variant; var Index: integer): TSqlRecord; overload;

        function IndexOf(const Value: variant): integer;         // index of value (used in sorting) in this "DB"index...
        function IndexOfOriginal(const Value: variant): integer; // index of value (used in sorting) in owner records array
        function IndexOfRecordID(const ID: TID; const Ascending: boolean): integer; // returns index of record in this index's array
        function RecordWithID (const ID: TID): TSqlRecord; // find a record with this ID
        function Add (const rec: TSqlRecord): integer;
        function Delete (ID: TID): boolean;
        // for given index in the index's array, look for the corresponding owner's record and get the field value
        function ResolveVarValue(const index: integer): variant; inline;
        function ResolveStringValue(const index: integer): string; inline;
        property Records [index: integer; Ascending: boolean = true]: TSqlRecord read GetRecords;
        property Count: integer read GetCount;
      end;

      TFilteredEnum = class
      private
        FTotalCount, FCurrent: integer;
        FOwner: TSqlRecords;
        FIndex: TSqlRecordsIndex;
        FAscending: boolean;
        FFilterFields: TList<TFieldInfo>;
        FFilterValue: string;
        //FLookupClass: TSQLRecordClass;
      public
        constructor Create (const AOwner: TSqlRecords; const AIndex: TSqlRecordsIndex;
                            const AAscending: boolean = true; const AFilterFields: string = ''; const AFilterValue: string = '');
        destructor Destroy; override;
        function GetEnumerator: TFilteredEnum;
        function MoveNext:Boolean;
        function GetCurrent: TSqlRecord;
        property Current: TSqlRecord read GetCurrent;
      end;

    function GetIndexes(FieldName: string): TSqlRecordsIndex;

    var
      FIndexes: TDictionary <string, TSqlRecordsIndex>; // field name, index
  protected
    //ExternalLookupSource: ILookupSource;
    FUpdateCounter: integer;
    SSFIsText, SSFTypeDetermined: boolean;
    SSFName: string;
  public
    Records: TSqlRecordArray; // tsql record objects go only into this array
    DynArray: TDynArray;
    FieldValueResolver: IFieldValueResolver;
    FiltersAndStats: TSqlRecordFiltersAndStats;
    dicIDToIndex: TDictionary <TID, integer>;
    AllowDuplicateIDs: boolean;
    constructor Create (ASqlRecordClass: TSqlRecordClass);
    destructor Destroy; override;
    procedure RegisterRecord (const rec: TSqlRecord; const index: integer; const IsSqlRecordEx: boolean);
    procedure Init (const AFieldValueResolver: IFieldValueResolver);
    function CreateCopy: TSqlRecords;
    function Find(const FieldName: string; const Value: variant; StartFrom: integer = 0): TSqlRecord; overload;
    function Find(const FieldNames: TArray<string>; const Values: TArray<variant>; StartFrom: integer = 0): TSqlRecord; overload;
    function FindID (const ID: TID): TSqlRecord;
//    function Find(const FieldName: string; const Value: variant; var Index: integer; StartFrom: integer = 0): T; overload;
    function Lookup(const FindFieldName: string; const FindValue: variant; const LookupFieldName: string): variant;
    function EnumByField (FieldName: string; const Ascending: boolean = true;
                          const FilterField: string = ''; const FilterValue: string = ''): TFilteredEnum;
    function GetLookupClass (AFieldName: string): TSQLRecordClass;
    //function ResolveLookupAsVariant(const LookupClass: TSQLRecordClass; const LookupID: TID): variant;
    //function ResolveLookupAsString(const LookupClass: TSQLRecordClass; const LookupID: TID): string;
    procedure AssignObjectList (ol: TObjectList);
    procedure CalculateStatistics;
    procedure Add (ARecord: TSqlRecord);
    procedure Delete (ID: TID);
    procedure FreeAndDelete (ID: TID);
    procedure FreeAndDeleteOne(ID: TID);
    procedure Update (ARecord: TSqlRecord);
    procedure AddOne (ARecord: TSqlRecord);
    procedure DeleteOne (ID: TID);
    procedure UpdateOne (ARecord: TSqlRecord);
    procedure BeginUpdate;
    procedure EndUpdate;
    function InUpdate: boolean;
    procedure RecreateIndex (const Name: string);
    procedure FreeIndexes;
    function IndexOf (ARecord: TSqlRecord): integer; overload;
    function IndexOf (ID: TID): integer; overload;
    function Contains(ID: TID): boolean;
    function Sum (FieldName: string): double;
    function CompareSSF(Index1, Index2: integer): integer;  // compare records based on secondary sort field
    function CompareSSFAsVariants(Index1, Index2: integer): TVariantRelationship;
    function CompareField (Index1, Index2: integer; const FieldName: string): TVariantRelationship;
    function CompareFieldAsText (Index1, Index2: integer; const FieldName: string): integer;
    procedure SetFiltered (const Value: boolean);

    property Indexes [Name: string]: TSqlRecordsIndex read GetIndexes;
    property InitTime: TDateTime read FInitTime;
    property Count: integer read GetCount;
    property CopyNo: integer read FCopyNo;
    property ID: integer read FID;
    property SqlRecordClass: TSQLRecordClass read FSqlRecordClass;
    property IsEmpty: boolean read GetIsEmpty;
//    property Records: TSqlRecordArray read FRecords; // bad idea because entire array gets copied and returned from implicit function?
  end;

  TSqlRecords<T: TSqlRecord> = class (TSqlRecords)
  private
    function GetItems(index: integer): T;
//    function GetItems: TEnumerator<T>;
//    type
//      TEnumerator<T: TSqlRecord> = class
//      private
//        FTotalCount, FCurrent: integer;
//        FOwner: TSqlRecords;
//      public
//        constructor Create (const AOwner: TSqlRecords);
//        destructor Destroy; override;
//        function GetEnumerator: TFilteredEnum;
//        function MoveNext:Boolean;
//        function GetCurrent: T;
//        property Current: T read GetCurrent;
//      end;
  public
//    property Items: TEnumerator<T> read GetItems;
    constructor CreateFrom (Records: TSqlRecords);
    constructor Create;
    property Items [index: integer]: T read GetItems; default;
  end;

  function VarCompareValueForSort(const A, B: Variant): TVariantRelationship;

implementation


function VarCompareValueForSort(const A, B: Variant): TVariantRelationship;
begin
  // to treat empty variants as lowest value
  if VarIsEmpty (A)
    then if VarIsEmpty (B)
      then result := TVariantRelationship.vrEqual
      else result := TVariantRelationship.vrLessThan
    else if VarIsEmpty (B)
      then result := TVariantRelationship.vrGreaterThan
    else if VarType (A) = varUString
      then begin
             var c := WideCompareText (A, B);
             if c < 0
                then result := TVariantRelationship.vrLessThan
                else if c > 0
                then result := TVariantRelationship.vrGreaterThan
                else result := TVariantRelationship.vrEqual
           end
      else result := System.Variants.VarCompareValue(A, B);
end;

procedure TSqlRecords.Add(ARecord: TSqlRecord);
//var
//  pair: TPair <string, TSqlRecordsIndex>;
begin
  if not InUpdate then raise Exception.Create('Please use begin/end update or AddOne');

  DynArray.Add(ARecord);
  RegisterRecord (ARecord, Count-1, ARecord is TSqlRecordEx);
//  for pair in FIndexes do
//    pair.Value.Add (ARecord);
end;

procedure TSqlRecords.Delete (ID: TID);
var
  i : integer;
begin
  if not InUpdate then raise Exception.Create('Please use begin/end update or DeleteOne');

  i := IndexOf (ID);
  DynArray.Delete(i);
  dicIDToIndex.Remove(ID);
//  for var pair in FIndexes do
//    pair.Value.Delete (ID);
end;

procedure TSqlRecords.FreeAndDeleteOne (ID: TID);
begin
  BeginUpdate;
  FreeAndDelete (ID);
  EndUpdate;
end;

procedure TSqlRecords.FreeAndDelete (ID: TID);
var
  i : integer;
begin
  if not InUpdate then raise Exception.Create('Please use begin/end update or DeleteOne');

  i := IndexOf (ID);
  if Records[i] is TSQLRecordEx
     then TSQLRecordEx(Records[i]).SqlRecords := nil;
  Records[i].Free;
  DynArray.Delete(i);
  dicIDToIndex.Remove(ID);
//  for var pair in FIndexes do
//    pair.Value.Delete (ID);
end;


procedure TSqlRecords.DeleteOne(ID: TID);
begin
  BeginUpdate;
  Delete (ID);
  EndUpdate;
end;

class destructor TSqlRecords.Destroy;
begin
  inherited;
  AllSqlRecords.Free;
end;

procedure TSqlRecords.Update (ARecord: TSqlRecord);
begin
  if not InUpdate then raise Exception.Create('Please use begin/end update or UpdateOne');

  FreeAndDelete (ARecord.IDValue);
  Add (ARecord);
end;

procedure TSqlRecords.UpdateOne(ARecord: TSqlRecord);
begin
  BeginUpdate;
  Update (ARecord);
  EndUpdate;
end;

class constructor TSqlRecords.Create;
begin
  inherited;
  AllSqlRecords := TList<TObject>.Create;
end;

procedure TSqlRecords.AddOne(ARecord: TSqlRecord);
begin
  BeginUpdate;
  Add (ARecord);
  EndUpdate;
end;

procedure TSqlRecords.AssignObjectList(ol: TObjectList);
begin
  SetLength (Records, ol.Count);
  for var i := 0 to ol.Count - 1 do
      Records[i] := TSqlRecord (ol[i]);
  ol.OwnsObjects := false;
end;

procedure TSqlRecords.BeginUpdate;
begin
  inc (FUpdateCounter);
end;

procedure TSqlRecords.CalculateStatistics;
begin
  if SqlRecordClass.InheritsFrom (TSqlRecordEx) then
    begin
      FiltersAndStats.ClearValues;
      if Count = 0 then exit;

      for var i := 0 to Count - 1 do
        if not TSqlRecordEx(Records[i]).Filtered then
          TSqlRecordEx(Records[i]).CalculateStatistics (FiltersAndStats);
    end;
end;

function TSqlRecords.CompareSSFAsVariants(Index1, Index2: integer): TVariantRelationship;
begin
  result := CompareField (Index1, Index2, SSFName);
end;

function TSqlRecords.CompareSSF(Index1, Index2: integer): integer;
begin
  if not SSFTypeDetermined then
    begin
      var vt := VarType (Records[0].GetFieldVariantValue(SSFName));
      SSFIsText := vt = varString;
      SSFTypeDetermined := true;
    end;

  if SSFIsText
    then result := CompareFieldAsText (Index1, Index2, SSFName)
    else begin
           var vr := CompareField (Index1, Index2, SSFName);
           case vr of
             vrEqual: result := 0;
             vrLessThan: result := -1;
             vrGreaterThan: result := 1;
             vrNotEqual: raise Exception.Create('Unable to compare values of field ' + SSFName);
             else result := -99; // to avoid warning hmph
           end;
         end;
end;

function TSqlRecords.CompareField(Index1, Index2: integer; const FieldName: string): TVariantRelationship;
var
  v1, v2: variant;
begin
  v1 := Records[Index1].GetFieldVariantValue(FieldName);
  v2 := Records[Index2].GetFieldVariantValue(FieldName);
  result := VarCompareValueForSort(v1, v2);
end;

function TSqlRecords.CompareFieldAsText (Index1, Index2: integer; const FieldName: string): integer;
begin
  result := CompareText (GetFieldDisplayValue(Index1, FieldName), GetFieldDisplayValue(Index2, FieldName));
end;

function TSqlRecords.Contains(ID: TID): boolean;
begin
  result := IndexOf (ID) > -1;
end;

constructor TSqlRecords.Create (ASqlRecordClass: TSqlRecordClass);
begin
  inherited Create;
  SSFName := ASqlRecordClass.SecondarySortField.AsString;

  FSqlRecordClass := ASqlRecordClass;
  FIndexes := TDictionary<string, TSqlRecordsIndex>.Create;
  dicIDToIndex := TDictionary <TID, integer>.Create;
  AllSqlRecords.Add (self);
  inc (NextID);
  FID := NextID;

  if ASqlRecordClass.InheritsFrom (TSqlRecordEx) then
    begin
      FiltersAndStats := TSqlRecordFiltersAndStats.Create;
      FiltersAndStats.Assign(TSqlRecordExClass(ASqlRecordClass).FiltersAndStats);
    end;
end;

function TSqlRecords.CreateCopy: TSqlRecords;
var
  i: integer;
begin
  result := TSqlRecords.Create (FSqlRecordClass);
  SetLength (result.Records, Count);
  for i := Low (result.Records) to High (result.Records) do
    result.Records[i] := Records[i].CreateCopy;
  result.Init (FieldValueResolver);
  result.FCopyNo := CopyNo + 1;
  result.FID := ID;
end;

function TSqlRecords.GetCount: integer;
begin
  if self = nil
     then result := 0
     else result := length (Records);
end;

function TSqlRecords.GetIndexes(FieldName: string): TSqlRecordsIndex;
begin
  if FieldName = ''
     then raise Exception.Create('Index field name not specified');

  FieldName := SqlRecordClass.GetActualSortFieldName(FieldName);
  if not SqlRecordClass.FieldInfoList.HasField(FieldName)
     then raise Exception.Create('Trying to index by field that does not exist: ' + FieldName);

  if not FIndexes.TryGetValue (FieldName, result)
    then begin
           result := TSqlRecordsIndex.Create(self, FieldName);
           FIndexes.Add(FieldName, result);
         end;
end;

function TSqlRecords.GetIsEmpty: boolean;
begin
  result := Count = 0;
end;

function TSqlRecords.GetLookupClass(AFieldName: string): TSQLRecordClass;
begin
  if (Count = 0) or (FieldValueResolver = nil) then exit (nil);

  var SqlRecord := records[0] as TSqlRecord;
  result := SqlRecord.GetLookupClass(AFieldName, FieldValueResolver.AuthUserClass);
end;

{
function TSqlRecords.ResolveLookupAsVariant(const LookupClass: TSQLRecordClass; const LookupID: TID): variant;
begin
  var s := '';
  if FieldValueResolver. ResolveAsString(LookupClass, LookupID, s) //was AsVariant
    then result := s
    else VarClear(result);
end;


function TSqlRecords.ResolveLookupAsString(const LookupClass: TSQLRecordClass; const LookupID: TID): string;
begin
  if not ExternalLookupSource.ResolveAsString(LookupClass, LookupID, result) then result := '';
end;
}

destructor TSqlRecords.Destroy;
begin
  if AllSqlRecords.IndexOf (self) = -1 then
    raise Exception.Create('Invalid TSqlRecords pointer');

  FiltersAndStats.Free;

  if (Count > 0) and (Records[0] is TSQLRecordEx) then
    for var i := 0 to Count-1 do
        TSQLRecordEx (Records[i]).SqlRecords := nil;

  for var rec in Records do
      rec.Free;
  if FIndexes <> nil then
    for var Index in FIndexes.Values do
      Index.Free;
  FIndexes.Free;
  AllSqlRecords.Remove (self);

  FieldValueResolver := nil;
  dicIDToIndex.Free;
  inherited;
end;

procedure TSqlRecords.EndUpdate;
begin
  dec (FUpdateCounter);
  if FUpdateCounter = 0 then
    begin
      FreeIndexes;
      CalculateStatistics;
    end;
end;

function TSqlRecords.EnumByField (FieldName: string; const Ascending: boolean = true;
                          const FilterField: string = ''; const FilterValue: string = ''): TFilteredEnum;
begin
  if FieldName = ''
     then raise Exception.Create('Field name not specified.'); //todo should check if field exists...
  FieldName := SqlRecordClass.GetActualSortFieldName(FieldName);
  var index := Indexes[FieldName];
  result := TFilteredEnum.Create (self, index, Ascending, FilterField, FilterValue);
end;

function TSqlRecords.FindID (const ID: TID): TSqlRecord;
var
  index: integer;
begin
  if dicIDToIndex.TryGetValue (ID, index)
    then result := Records[index]
    else result := nil;
end;

function TSqlRecords.Find(const FieldName: string; const Value: variant; StartFrom: integer): TSqlRecord;
var
  Index, FromIndex, ToIndex: integer;
begin
  if length(Records) = 0 then exit (nil);

  var pIndex := Indexes[FieldName];
  FromIndex := StartFrom;
  ToIndex := high (Records);
  result := pIndex.FindOrNearby (Value, Index, FromIndex, ToIndex);
end;

function TSqlRecords.Find(const FieldNames: TArray<string>; const Values: TArray<variant>; StartFrom: integer = 0): TSqlRecord;
var
  i, Index, FromIndex, ToIndex: integer;
begin
  if length(Records) = 0 then exit (nil);

  var pIndex := Indexes[FieldNames[0]];
  FromIndex := StartFrom;
  ToIndex := high (Records);
  result := pIndex.FindOrNearby (Values[0], Index, FromIndex, ToIndex);

  if result.GetFieldVariant (FieldNames[0]) <> Values[0] then exit (nil);

  while (Index < Count) do
    begin
      for i := low (FieldNames) + 1 to high (FieldNames) do
        if pIndex.Records[Index, true].GetFieldVariant (FieldNames[i]) <> Values[i] then
          begin
            inc (Index);
            continue;
          end;
      exit (pIndex.Records[Index, true]);
    end;

  result := nil;
end;

function TSqlRecords.IndexOf (ARecord: TSqlRecord): integer;
begin
  result := IndexOf (ARecord.IDValue);
end;

function TSqlRecords.IndexOf (ID: TID): integer;
begin // records      [a f b d]
      // index [name] [1 3 4 2]
      //               0 1 2 3
  var IDIndex := Indexes['ID'];
  result := IDIndex.IndexOfOriginal(ID);
//  result := IDIndex.IndexArray[result];
end;

procedure TSqlRecords.RegisterRecord (const rec: TSqlRecord; const index: integer; const IsSqlRecordEx: boolean);
begin
  if IsSqlRecordEx then
    if TSQLRecordEx (rec).SqlRecords <> nil
       then raise Exception.Create('Record already belongs to a different TSqlRecords!')
       else TSQLRecordEx (rec).SqlRecords := self;

  if dicIDToIndex.ContainsKey(rec.IDValue)
    then if AllowDuplicateIDs
         then // ignore ID
         else raise Exception.Create('Duplicate ID? Needs AllowDuplicateIDs=true? Or bad view code? ' + SqlRecordClass.SQLTableName.AsString)
    else dicIDToIndex.Add(rec.IDValue, index);
end;

procedure TSqlRecords.Init (const AFieldValueResolver: IFieldValueResolver);
var
  IsEx: boolean;
  i: integer;
begin
  if ClassHasAttribute (FSqlRecordClass, AllowDuplicateIDsAttribute)
    then AllowDuplicateIDs := true;

  FieldValueResolver := AFieldValueResolver;
  DynArray.Init(TypeInfo(TSqlRecordArray), Records);
  FIndexes.Clear;
  dicIDToIndex.Clear;
  FInitTime := now;
  IsEx := (Count > 0) and (Records[0] is TSQLRecordEx);
  for i := 0 to Count-1 do
      RegisterRecord (Records[i], i, IsEx);
end;

function TSqlRecords.InUpdate: boolean;
begin
  result := FUpdateCounter > 0;
end;

function TSqlRecords.Lookup(const FindFieldName: string; const FindValue: variant; const LookupFieldName: string): variant;
begin
  var rec := Find (FindFieldName, FindValue);
  if rec <> nil
     then result := rec.GetFieldVariantValue (LookupFieldName)
     else result := varNull;
end;

procedure TSqlRecords.RecreateIndex(const Name: string);
var
  ind: TSqlRecordsIndex;
begin
  FIndexes.TryGetValue(Name, ind);
  if ind <> nil then
    begin
      FIndexes.Remove(Name);
      ind.Free;
    end;
  Indexes[Name];
end;

procedure TSqlRecords.FreeIndexes;
begin
  for var pair in FIndexes do pair.value.Free;
  FIndexes.Clear;
end;

procedure TSqlRecords.SetFiltered(const Value: boolean);
begin
  if SqlRecordClass.InheritsFrom (TSqlRecordEx)
    then for var rec in Records
         do TSqlRecordEx(rec).Filtered := Value;
end;

function TSqlRecords.Sum(FieldName: string): double;
var
  rec: TSqlRecord;
  temp: variant;
begin
  result := 0;

  var P := FSqlRecordClass.RecordProps.Fields.ByRawUTF8Name(StringToUTF8(FieldName));

  for rec in self.Records do
    begin
      P.GetVariant(rec, temp);
      result := result + temp;
    end;
end;

// ============

constructor TSqlRecords.TFilteredEnum.Create(const AOwner: TSqlRecords; const AIndex: TSqlRecordsIndex;
                                                const AAscending: boolean = true; const AFilterFields: string = ''; const AFilterValue: string = '');
begin
  inherited Create;
  if (AFilterFields = '') and (AFilterValue <> '')
    then
      begin
        FFilterFields := TList<TFieldInfo>.Create;
        for var fi in AOwner.SqlRecordClass.FieldInfoList.GeneralFilter do FFilterFields.Add(fi);
      end
    else
      begin
        var sl := TStringList.Create;
        sl.CommaText := AFilterFields;
        for var i := 0 to sl.Count - 1 do
          FFilterFields.Add(AOwner.SqlRecordClass.FieldInfoList.ItemsByName[sl[i]]);
        sl.Free;
      end;

  FOwner := AOwner;
  FIndex := AIndex;
  FAscending := AAscending;
  FFilterValue := TSimpleTranslit.ToLatin (AFilterValue).ToLower;
  FTotalCount := Length(FIndex.IndexArray);
  if FAscending
     then FCurrent := -1
     else FCurrent := FTotalCount;
end;

destructor TSqlRecords.TFilteredEnum.Destroy;
begin
  FFilterFields.Free;
  inherited;
end;

function TSqlRecords.TFilteredEnum.GetCurrent: TSqlRecord;
begin
//  if fcurrent = 0 then
//    if now=0 then exit;

  result := FOwner.Records[FIndex.IndexArray[FCurrent]];
end;

function TSqlRecords.TFilteredEnum.MoveNext: Boolean;
var
  s: string;
  p: integer;
  FilterMatched: boolean;
begin
  FilterMatched := (FFilterValue = '');

  repeat
      if FAscending
         then inc (FCurrent)
         else dec (FCurrent);

      if (FCurrent >= FTotalCount) or (FCurrent < 0) then break;

      if (Current is TSqlRecordEx) and not TSqlRecordEx(Current).Matches (FOwner.FiltersAndStats) then continue;

      if FilterMatched then break;

      if (FCurrent >= FTotalCount) or (FCurrent < 0) then break;

      if FFilterFields <> nil then
        begin
          for var FieldInfo in FFilterFields do
            begin
              //s := FOwner.FieldValueResolver.GetFieldDisplayValue (Current, ff);
              s := FieldInfo.GetDisplayString(Current);
              if FieldInfo.IsTextual then
                begin
                  if FieldInfo.ShouldTranslit
                     then s := TSimpleTranslit.ToLatin (s).ToLower
                     else s := s.ToLower;
                end;
              p := pos (FFilterValue, s);
              if (p > 0) then
                begin
                  FilterMatched := true;
                  break;
                end;
            end;
          if FilterMatched then break;
        end;
    until false;

    // todo add option to filter all fields if ffilfield is blank
  if FAscending
     then result := FTotalCount > FCurrent
     else result := FCurrent >= 0;
end;

function TSqlRecords.TFilteredEnum.GetEnumerator: TFilteredEnum;
begin
  result := self;
end;

{ TSqlRecords.TSqlRecordsIndex }

function TSqlRecords.TSqlRecordsIndex.Add(const rec: TSqlRecord): integer;
var
  FromIndex, ToIndex: integer;
begin
  FromIndex := low (IndexArray);
  ToIndex := high (IndexArray);

  if FindOrNearby (rec.GetFieldVariantValue(FFieldName), result, FromIndex, ToIndex) <> nil
    then
    else
      begin
        result := min (length(IndexArray) - 1, max (FromIndex, ToIndex));
        if not IsTextField
          then
            begin
              var v := rec.GetFieldVariantValue (FFieldName);

              while (result > 0) and (VarCompareValueForSort (ResolveVarValue(result), v) = TVariantRelationship.vrGreaterThan) do
                dec (result);
              inc (result);
            end
          else
            begin
              var s := rec.GetFieldStringValue (FFieldName);

              while (result > 0) and (WideCompareText (ResolveVarValue(result), s)>0) do
                dec (result);
              inc (result);
            end;
      end;

  var int := length(FOwner.Records) - 1;
  DynArray.Insert(result, int);
  RecordIDs.Add(rec.IDValue, result);
  if length (IndexArray) = 1 then Init;
end;

function TSqlRecords.TSqlRecordsIndex.Delete(ID: TID): boolean;
begin
  result := DynArray.Delete(RecordIDs [ID]);
  RecordIDs.Remove(ID);
end;

destructor TSqlRecords.TSqlRecordsIndex.Destroy;
begin
  RecordIDs.Free;
  inherited;
end;

constructor TSqlRecords.TSqlRecordsIndex.Create(const AOwner: TSqlRecords; const AFieldName: string);
begin
  inherited Create;
  RecordIDs := TIDIntegerDictionary.Create;
  FOwner := AOwner;
  FFieldName := AFieldName;
  Init;
  QuickSort;
  DynArray.Init(TypeInfo(TIntegerDynArray), IndexArray);
end;

function TSqlRecords.TSqlRecordsIndex.FFind(const Value: variant; var FromIndex, ToIndex: integer): integer;
var
  guess: integer;
begin
  if Count = 0 then exit (-1);

  var MaxIndex := length(IndexArray);
  repeat
    guess := (FromIndex + ToIndex) div 2;
    case VarCompareValueForSort (ResolveVarValue(guess), Value) of
      TVariantRelationship.vrEqual: exit (guess);
      TVariantRelationship.vrLessThan:    FromIndex := guess+1;
      TVariantRelationship.vrGreaterThan: ToIndex := guess-1;
      TVariantRelationship.vrNotEqual: raise Exception.Create('Unable to compare values of field ' + FFieldName);
    end;
  until (FromIndex<0) or (ToIndex>MaxIndex) or (FromIndex > ToIndex);
  result := -1;
end;

function TSqlRecords.TSqlRecordsIndex.FFindText(const Value: variant; var FromIndex, ToIndex: integer): integer;
var
  guess, cmp: integer;
begin
  if Count = 0 then exit (-1);

  var MaxIndex := length(IndexArray);
  repeat
    begin
      guess := (FromIndex + ToIndex) div 2;
      cmp := WideCompareText (Value, ResolveStringValue(guess));
      if cmp = 0
        then exit (guess)
        else if cmp > 0
          then FromIndex := guess+1 // value is > guess, start over with next one as "from"
          else ToIndex := guess-1;  // value is < guess, start over with previous one as "to"
    end;
  until (FromIndex<0) or (ToIndex>MaxIndex) or (FromIndex > ToIndex);
  result := -1;
end;

procedure TSqlRecords.TSqlRecordsIndex.FillRecordIDs;
begin
  RecordIDs.Clear;
  var id: TID;

  for var i := Low (IndexArray) to high (IndexArray) do
    begin
      id := Records[i, true].IDValue;
      if RecordIDs.ContainsKey (id)
         then begin                    // duplicate or NULL IDs
                RecordIDs.Clear;
                break;
              end
         else RecordIDs.Add (Records[i, true].IDValue, i);
    end;
end;

function TSqlRecords.TSqlRecordsIndex.Find(const Value: variant): TSqlRecord;
var
  Index, FromIndex, ToIndex: integer;
begin
  FromIndex := low (IndexArray);
  ToIndex := high (IndexArray);
  result := FindOrNearby (Value, index, FromIndex, ToIndex);
end;

function TSqlRecords.TSqlRecordsIndex.Find(const Value: variant; var Index: integer): TSqlRecord;
var
  FromIndex, ToIndex: integer;
begin
  FromIndex := low (IndexArray);
  ToIndex := high (IndexArray);
  result := FindOrNearby (Value, index, FromIndex, ToIndex);
end;

function TSqlRecords.TSqlRecordsIndex.IndexOf (const Value: variant): integer;
var
  Index, FromIndex, ToIndex: integer;
begin
  FromIndex := low (IndexArray);
  ToIndex := high (IndexArray);
  if FindOrNearby (Value, Index, FromIndex, ToIndex)<>nil
    then result := Index
    else result := -1;
end;

function TSqlRecords.TSqlRecordsIndex.IndexOfOriginal(const Value: variant): integer; // index of value in owner records array
begin
  result := IndexOf (Value);
  if result <> -1
     then result := IndexArray[result];
end;

function TSqlRecords.TSqlRecordsIndex.IndexOfRecordID(const ID: TID; const Ascending: boolean): integer;
begin
  if RecordIDs.TryGetValue (ID, result)
    then
      if Ascending
         then
         else result := FOwner.Count - 1 - result
    else result := -1
end;

function TSqlRecords.TSqlRecordsIndex.RecordWithID(const ID: TID): TSqlRecord;
begin
  var index := IndexOfRecordID (ID, true);
  if index = -1
     then result := nil
     else result := Records[index, true];
end;


function TSqlRecords.TSqlRecordsIndex.FindOrNearby (const Value: variant; var Index, FromIndex, ToIndex: integer): TSqlRecord;
begin
  if IsTextField
    then index := FFindText (Value, FromIndex, ToIndex)
    else index := FFind (Value, FromIndex, ToIndex);

  if index <> -1
     then begin
            //Index := IndexArray[index];
            result := FOwner.Records[IndexArray[index]];
          end
     else result := nil;
end;

function TSqlRecords.TSqlRecordsIndex.GetCount: integer;
begin
  result := length (IndexArray);
end;

function TSqlRecords.TSqlRecordsIndex.GetRecords(index: integer; Ascending: boolean = true): TSqlRecord;
begin
  if not Ascending
    then index := FOwner.Count - index - 1;
  result := FOwner.Records[IndexArray[index]];
end;

procedure TSqlRecords.TSqlRecordsIndex.Init;
begin
//  LookupClass := FOwner.GetLookupClass (FFieldName);
  if FFieldName[1] = '!' then
    begin
      UseLookup := false;
      FFieldName := copy (FFieldName, 2);
    end
    else
      UseLookup := (FOwner <> nil) and (FOwner.GetLookupClass (FFieldName) <> nil);
  if UseLookup
    then IsTextField := true
    else
      if Length(FOwner.Records) > 0
        then
          begin
            var vt := VarType (FOwner.Records[0].GetFieldVariantValue(FFieldName));
            IsTextField := vt = varString;
          end
        else
            IsTextField := false;
end;

function TSqlRecords.TSqlRecordsIndex.ResolveStringValue(const index: integer): string;
begin
  var SqlRecord := FOwner.Records[IndexArray[index]];
  var ID := SqlRecord.GetFieldVariantValue(FFieldName);
  if UseLookup
    then result := FOwner.FieldValueResolver.GetFieldDisplayValue (SqlRecord, FFieldName)//  ResolveLookupAsString(LookupClass, ID)
    else result := SqlRecord.GetFieldStringValue(FFieldName);
end;

function TSqlRecords.TSqlRecordsIndex.ResolveVarValue(const index: integer): variant;
begin
  var SqlRecord := FOwner.Records[IndexArray[index]];
  result := SqlRecord.GetFieldVariantValue(FFieldName);
  if UseLookup
    then
      //result := FOwner.ResolveLookupAsVariant(LookupClass, result);
      FOwner.FieldValueResolver.GetFieldDisplayValue(SqlRecord, FFieldName);
end;

procedure TSqlRecords.TSqlRecordsIndex.QuickSort(const Ascending: boolean = true);
begin
  SetLength(IndexArray, Length(FOwner.Records));
  for var i := low (IndexArray) to high (IndexArray) do
    IndexArray[i] := i;

  if Length(IndexArray) > 0 then
    begin
      if IsTextField
       then SortText(0, Length(IndexArray) - 1, Ascending)
       else Sort(0, Length(IndexArray) - 1, Ascending);
    end;

  FillRecordIDs;
end;

procedure TSqlRecords.TSqlRecordsIndex.Sort(L, R: Integer; const Ascending: boolean);
var
  I, J, P: Integer;
  v1, v2: variant;
begin
  repeat
    I := L;
    J := R;
    P := (L+R) div 2;
    repeat
      var vr: TVariantRelationship;
      while true do
        begin
          v1 := FOwner.Records[IndexArray[I]].GetFieldVariantValue(FFieldName);
          v2 := FOwner.Records[IndexArray[P]].GetFieldVariantValue(FFieldName);
          vr := VarCompareValueForSort (v1, v2);

          if (vr = TVariantRelationship.vrEqual) and (FOwner.SSFName <> '')
             then vr := FOwner.CompareSSFAsVariants (IndexArray[I], IndexArray[P]);

          if (Ascending and (vr = TVariantRelationship.vrLessThan) )
             or
             (Not Ascending and (vr = TVariantRelationship.vrGreaterThan) )
            then inc (I)
            else break;
        end;

      if vr = TVariantRelationship.vrNotEqual
         then raise Exception.Create('Unable to sort by ' + FFieldName);

      while true do
        begin
          v1 := FOwner.Records[IndexArray[J]].GetFieldVariantValue(FFieldName);
          v2 := FOwner.Records[IndexArray[P]].GetFieldVariantValue(FFieldName);
          vr := VarCompareValueForSort (v1, v2);
          if (vr = TVariantRelationship.vrEqual) and (FOwner.SSFName <> '')
             then vr := FOwner.CompareSSFAsVariants (IndexArray[J], IndexArray[P]);

          if (Ascending and (vr = TVariantRelationship.vrGreaterThan) )
             or
             (Not Ascending and (vr = TVariantRelationship.vrLessThan) )
            then dec (J)
            else break;
        end;

      if vr = TVariantRelationship.vrNotEqual
         then raise Exception.Create('Unable to sort by ' + FFieldName);

//        while Compare(I, P)<0 do inc(I);
//        while Compare(J, P)>0 do dec(J);
      if I<=J then
      begin
        if I<>J then
        begin
          var ExJ := IndexArray[J];
          IndexArray[J] := IndexArray[I];
          IndexArray[I] := ExJ;
          //Exchange(I, J);
          //may have moved the pivot so we must remember which element it is
          if P = I then
            P := J
          else if P=J then
            P := I;
        end;
        inc(I);
        dec(J);
      end;
    until I>J;
    if L<J then
      Sort(L, J, Ascending);
    L := I;
  until I>=R;
end;

function TSqlRecords.GetFieldDisplayValue (index: integer; const FieldName: string): variant;
begin
  result := FieldValueResolver.GetFieldDisplayValue(Records[index], FieldName);
end;

procedure TSqlRecords.TSqlRecordsIndex.SortText(L, R: Integer; const Ascending: boolean);
var
  AscFactor, I, J, P, CompResult: Integer;
//  v1, v2: variant;
  PValue: string;
begin
  repeat
    I := L;
    J := R;
    P := (L+R) div 2;
                                  //todo: optimize and get the lookup object (if there is one) to access it directly
    if Ascending then AscFactor := 1 else AscFactor := -1;
    repeat
      while true do
        begin
          //PValue := FOwner.Records[IndexArray[P]].GetFieldStringValue(FFieldName);
          PValue := FOwner.GetFieldDisplayValue(IndexArray[P], FFieldName);
          CompResult := AscFactor*CompareText (FOwner.GetFieldDisplayValue(IndexArray[I], FFieldName), PValue);
          if (CompResult = 0) and (FOwner.SSFName <> '')
             then CompResult := AscFactor*FOwner.CompareSSF (IndexArray[I], IndexArray[P]);

          if CompResult < 0
            then inc (I)
            else break;
        end;

      while true do
        begin
          CompResult := AscFactor*WideCompareText (FOwner.GetFieldDisplayValue(IndexArray[J], FFieldName), PValue);
          if (CompResult = 0) and (FOwner.SSFName <> '')
             then CompResult := AscFactor*FOwner.CompareSSF (J, P);

          if CompResult  > 0
            then dec (J)
            else break;
        end;

      if I<=J then
      begin
        if I<>J then
        begin
          var ExJ := IndexArray[J];
          IndexArray[J] := IndexArray[I];
          IndexArray[I] := ExJ;
          //Exchange(I, J);
          //may have moved the pivot so we must remember which element it is
          if P = I then
            P := J
          else if P=J then
            P := I;
        end;
        inc(I);
        dec(J);
      end;
    until I>J;
    if L<J then
      SortText(L, J, Ascending);
    L := I;
  until I>=R;
end;

{ TFunLookup }

procedure TLookup.Add(const AIDs: TIDDynArray; const AValues: TRawUTF8DynArray);
begin
  for var i := low (AIDs) to high (AIDs) do
    begin
      Items.Add (AIDs[i], AValues[i]);
      if Reverse.ContainsKey (AValues[i])
        then Reverse.Add(AValues[i] + '(' + StringToUTF8 (AIDs[i].ToString) + ')', AIDs[i])
        else Reverse.Add(AValues[i], AIDs[i]);

      SortedValues.Add(AValues[i].AsString);
      IDIndexes.Add (AIDs[i], i);
    end;
  IDs := AIDs;
  Values := AValues;
  //SortedValues := AValues;
  //QuickSortRawUTF8 (SortedValues, Items.Count);
end;

procedure TLookup.AfterConstruction;
begin
  inherited;
  IDIndexes := TIDIntegerDictionary.Create;
  Items := TDictionary <TID, RawUTF8>.Create;
  Reverse := TDictionary <RawUTF8, TID>.Create;
  CreationTime := now;
  SortedValues := TStringList.Create;
  SortedValues.Sorted := true;
end;

function TLookup.Count: integer;
begin
  if self = nil
    then exit (0)
    else result := Items.Count;
end;

destructor TLookup.Destroy;
begin
  IDIndexes.Free;
  Items.Free;
  Reverse.Free;
  SortedValues.Free;
  inherited;
end;

function TLookup.IndexOf(const ID: TID): integer;
begin
  if self = nil then exit (-1);
  if not IDIndexes.TryGetValue(ID, result) then result := -1;

//  var u: RawUTF8;
//  if Items.TryGetValue (ID, u)
//    then result := SortedValues.IndexOf(u.AsString)
//         // result := FastFindPUTF8CharSorted (@SortedValues, Items.Count-1, @u)
//    else result := -1;
end;

{ TLookups }

constructor TLookups.Create (const ATables, AAlternatives: TSQLRecordClassArray);
begin
  inherited Create;
  Dictionary := TObjectDictionary<TSQLRecordClass, TLookup>.Create ([doOwnsValues]);
  Tables := ATables;
  Alternatives := AAlternatives;
end;

destructor TLookups.Destroy;
begin
  Dictionary.Free;
  inherited;
end;

function TLookups.GetLookup(cl: TSQLRecordClass): TLookup;
begin
  Dictionary.TryGetValue(cl, result);
end;

function TLookups.HasValidLookup(cl: TSQLRecordClass): boolean;
var
  lk: TLookup;
begin
  result := Dictionary.TryGetValue(cl, lk) and (MinutesSince(lk.CreationTime) < Lookup_Timeout_Minutes);
end;

function TLookups.ResolveAsString (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: string): boolean;
var
  LU: TLookup;
  var utf8: RawUTF8;
begin
  result := Dictionary.TryGetValue(ASqlRecordClass, LU);
  if not result then
    for var i := low (Tables) to high (Tables) do
      if Tables[i] = ASqlRecordClass then
        begin
          result := Dictionary.TryGetValue(Alternatives[i], LU);
          if result then break;
        end;
//      if and (ASqlRecordClass = TSQLFunUser) then
//    result := Dictionary.TryGetValue(TSqlPublicUser, LU);

  if LU <> nil
     then
       begin
         result := LU.Items.TryGetValue(ASqlRecordID, utf8);
         if utf8 <> '' then Value := utf8.AsString;
       end;
end;

{function TLookups.ResolveAsVariant (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: variant): variant;
var
  s: string;
begin
  s := value;
  result := ResolveAsString (ASqlRecordClass, ASqlRecordID, s);
end;
}

{ TSqlRecords<T> }


{ TSqlRecords<T> }

constructor TSqlRecords<T>.Create;
begin
  inherited Create(T);
end;

constructor TSqlRecords<T>.CreateFrom(Records: TSqlRecords);
begin
  inherited Create (T);
  Init(nil);
  BeginUpdate;
  for var r in Records.Records do
    Add(r.CreateCopy);
  EndUpdate;
end;

function TSqlRecords<T>.GetItems(index: integer): T;
begin
  result := T (Records[index]);
end;

end.
