﻿unit LocalSettings;

interface

uses
  System.Classes, System.SysUtils, System.IOUtils;

type

  TLocalSettings = class(TPersistent)
  private
    FFilename : string;
    function GetFilename: string;
    function GetDefaultFilename : string;
    procedure Setfilename(const Value: string);
  public
    property Filename : string read GetFilename write Setfilename;
    procedure SaveSettings;
    procedure LoadSettings; virtual;
  end;

  TMormotClientSettings = class(TLocalSettings)
  private
    FServerAddress : string;
    FServerPort : string;
    FLastLogged : string;
    FStoreLastLogged : boolean;
    FLastStyle : integer;
    FFormsStayOntop: boolean;
    FStyle: integer;
    FFontSizeOffset: integer;
  public
    procedure LoadSettings; override;
  published
    property ServerAddress : string read FServerAddress write FServerAddress;
    property ServerPort : string read FServerPort write FServerPort;
    property LastLogged : string read FLastLogged write FLastLogged;
    property StoreLastLogged : boolean read FStoreLastLogged write FStoreLastLogged;
    property LastStyle : integer read FLastStyle write FLastStyle;
    property FormsStayOntop: boolean read FFormsStayOntop write FFormsStayOntop;
    property Style: integer read FStyle write FStyle;
    property FontSizeOffset: integer read FFontSizeOffset write FFontSizeOffset;
  end;


implementation

uses
  mORMot;

{ TLocalSettings }
function TLocalSettings.GetDefaultFilename: string;
begin
  var dp := System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetHomePath, System.IOUtils.TPath.GetFileNameWithoutExtension(ParamStr(0)));
  if not DirectoryExists(dp) then
    TDirectory.CreateDirectory(dp);
  Result := System.IOUtils.TPath.Combine(dp, 'settings.json');
end;

function TLocalSettings.GetFilename: string;
begin
  if FFilename <> '' then
    result := FFilename
  else
    result := GetDefaultFilename;
end;

procedure TLocalSettings.LoadSettings;
begin
  JSONFileToObject(Filename, self);
end;

procedure TLocalSettings.SaveSettings;
begin
  ObjectToJSONFile(self, Filename);
end;

procedure TLocalSettings.Setfilename(const Value: string);
begin
  FFilename := Value;
end;

{ TMormotClientSettings }

procedure TMormotClientSettings.LoadSettings;
begin
  inherited;
  if ServerAddress = ''
    then ServerAddress := 'localhost';
end;

end.
