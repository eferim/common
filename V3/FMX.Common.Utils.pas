unit FMX.Common.Utils;

interface

uses
  System.Types, System.UITypes, System.SysUtils, System.Generics.Collections, System.Generics.Defaults, math, Classes,
  FMX.Types, FMX.Controls, FMX.Dialogs, FMX.DialogService.Sync, FMX.Forms, FMX.Grid, FMX.DateTimeCtrls,
  FMX.Objects, FMX.ListBox, FMX.Layouts, RTTI, System.TypInfo, FMX.StdCtrls, messages, FMX.Memo, WinApi.Windows,
  FMX.TabControl,  FMX.Graphics, FMX.Platform, IOUtils,
  Common.Utils, MormotTypes;

type
  {$IFNDEF FIREMONKEY}
  dont use from server
  {$ENDIF}

  IFormStyler = interface
  ['{5BB78A64-2539-4A87-A729-9F1182757726}']
    procedure ApplyStyle (Form: TCommonCustomForm);
  end;

  TCustomButtonHelper = class helper for TCustomButton
  public
    procedure SimulateClick;
  end;

  TCustomMemoHelper = class helper for TCustomMemo
  public
    procedure AutoHeight;
    function GetAutoHeight: integer;
  end;

  //{$IF CompilerVersion < 34.0}
  TComboBoxHelper = class helper for TCustomComboBox
  private
    function GetText: string;
    procedure SetText(const Value: string);
    function GetSelectedObject: TObject;
    procedure SetSelectedObject(const Value: TObject);
  public
    property Text: string read GetText write SetText;
    property SelectedObject: TObject read GetSelectedObject write SetSelectedObject;
  end;
  //{$ENDIF}

  TControlHelper = class helper for TControl
  public
    function FindChildrenByClass<T: class>: TList<T>;
    function VisibleIncludingParents: boolean;
  end;

  TDateEditHelper = class helper for TDateEdit
    procedure SetSmartDate (d: TDate);
    function GetSmartDate: TDate;
  end;

  TTimeEditHelper = class helper for TTimeEdit
    procedure SetSmartTime (t: TTime);
    function GetSmartTime: TTime;
  end;

  TStringGridHelper = class helper for TStringGrid
  private
    type
    TColumnClass = class of TColumn;
    function GetSelectedRowCells(index: integer): string;
    procedure SetSelectedRowCells(index: integer; const Value: string);
  public
    function AddColumn (ColumnClass: TColumnClass; const Header: string; const TagString: string = ''; Width: integer = 0): TColumn; overload;
    function AddColumn <T: TColumn> (const Header: string; const TagString: string = ''; Width: integer = 0): T; overload;
    function CopyColumn (ACol : TColumn) : TColumn;
    function FindColumn (const Header: string): TColumn;
    function FindColumnIndex (const Header: string): integer;
    procedure AutoColumnWidths (AMinimum: integer = 50; AMaximum: integer = 300);
    function GoToNextRow: boolean;
    procedure LoadFromTSV (const FileName: TFileName);
    procedure SaveToTSV (const FileName: TFileName; const AColumns: TArray<integer>);
    property SelectedRowCells[index: integer] : string read GetSelectedRowCells write SetSelectedRowCells;

  end;

  TFlowLayoutlHelper = class helper for TFlowlayout
  public
    function ChildrenWidth: Single;
    function ChildrenHeight: Single;
  end;

  TRestrictedSizeForm = class (TForm)
//    procedure Resize; override;
//    procedure WMGetMinMaxInfo(var Message: TWMGetMinMaxInfo); message WM_GETMINMAXINFO;
  private
    FIsModal: boolean;
    function GetControlBottomRelativeToForm(ctrl: TControl): Single;
    procedure SetMinHeight(const Value: single);
    procedure SetMinWidth(const Value: single);
    function GetMinHeight: single;
    function GetMinWidth: single;
    {$IF CompilerVersion < 34.0}
  protected
    FMinWidth, FMinHeight: single;
    procedure Resize; override;
  public
    IgnoreResize: boolean;
    {$ENDIF}
    procedure AfterConstruction; override;
    function ShowModal: TModalResult; reintroduce;
  public
    procedure AutoHeight; virtual;
    property MinWidth: single read GetMinWidth write SetMinWidth;
    property MinHeight: single read GetMinHeight write SetMinHeight;
    property IsModal : boolean read FIsModal;
  end;

  TTimerHelper = class helper for TTimer
  public
    procedure Restart;
  end;

type
  TListBoxHelper = class helper for TListBox
  public
    function CheckedCount: Integer;
    function CheckedIndexes: TArray<Integer>;
    function SelectText (const s: string): boolean;
  end;

  TApplicationHelper = class helper for TApplication
  public
    function ExeName: TFileName;
    function ExeFolder: TFileName;
  end;

  TOutcomeHelper = record helper for TOutcome
  public
    procedure ShowError (const AdditionalContent: string = '');
  end;


  function FocusControlOrChild (AControl: TControl): TControl; overload;
  procedure FocusControlOrChild (Form: TCustomForm); overload;

  procedure FocusNextControl (c: TControl);
  procedure ShowErrorAndAbort (const Error: string);
  procedure ShowInformationAndAbort (const Information: string);
  procedure ShowErrorDialogue (const Error: string);
  function ConfirmationDialog(const Text: string): boolean;

  procedure SetupModalAutoDimmer;

  function GetParentForm(Control: TFmxObject): TCommonCustomForm;
  function DontStayOnTop: integer;
  function FindForm (const Name: string): TCommonCustomForm;

  type

//  TCustomAttributeClass = class of TCustomAttribute;

  TLabelHelper = class helper for TLabel
    procedure SetWarningState (Warning: boolean);
  end;

  function HasTextProperty(obj: TObject): Boolean;
  procedure SetTextProperty(obj: TObject; const value: string);

  //procedure AutoTabOrder (obj: TFmxObject); overload;
  procedure AutoTabOrder (frm: TCustomForm); overload;
  procedure AutoTabOrder (ParentControl: TControl); overload;
  function GetGridAutoHeight (Grid: TCustomGrid): single;
  procedure AutoHeightControls(Parent: TControl);
  procedure ToggleChildCallout (Sender: TObject);
  function IsLeftMouseButtonDown: Boolean;
  function RelevantParentTabOrder (Control: TControl): integer;

  procedure DumpControlStructure (c: TControl);
  procedure DumpFormStructure (form: TCommonCustomForm);

type
  TDefaultFont = class (TInterfacedObject, IFMXSystemFontService)
  protected
    OriginalSize: Single;
    class var intfSystemFontService : IFMXSystemFontService;
  public
    FontFamilyName: string;
    SizeOffset: integer;
    class var Instance: TDefaultFont;
    function GetDefaultFontFamilyName: string;
    function GetDefaultFontSize: Single;
    constructor Create;
    class procedure Setup;
  end;

var
  intfFormStyler: IFormStyler;

implementation

type
  TAutoFormDimmer = class
  protected
    Forms: TList <TCommonCustomForm>;
    Dimmers: TDictionary <TCommonCustomForm, TRectangle>;
    procedure OnIdle (Sender: TObject; var Done: boolean);
  public
    OnlyForModal: boolean;
    constructor Create;
    destructor Destroy; override;
  end;

var
  AutoFormDimmer: TAutoFormDimmer;

{ TComboBoxHelper }

//{$IF CompilerVersion < 34.0}
function TComboBoxHelper.GetSelectedObject: TObject;
begin
  if ItemIndex = -1 then result := nil else result := Items.Objects[ItemIndex];
end;

function TComboBoxHelper.GetText: string;
begin
  if ItemIndex = -1 then result := '' else result := Items[ItemIndex];
end;

procedure TComboBoxHelper.SetSelectedObject(const Value: TObject);
begin
  ItemIndex := Items.IndexOfObject (Value);
end;

procedure TComboBoxHelper.SetText(const Value: string);
begin
  ItemIndex := Items.IndexOf (Value);
end;
//{$ENDIF}

procedure AutoHeightControls(Parent: TControl);
var
  i: Integer;
  Ctrl: TControl;
begin
  for i := 0 to Parent.ControlsCount - 1 do
  begin
    Ctrl := Parent.Controls[i];
    if Ctrl is TCustomMemo then TCustomMemo(Ctrl).AutoHeight;

    if Ctrl is TCustomGrid
      then TCustomGrid(Ctrl).Height := GetGridAutoHeight(TCustomGrid(Ctrl));

    AutoHeightControls(Ctrl);
  end;
end;

procedure SetupModalAutoDimmer;
begin
  AutoFormDimmer := TAutoFormDimmer.Create;
end;

constructor TAutoFormDimmer.Create;
begin
  inherited;
  Forms := TList <TCommonCustomForm>.Create;
  Dimmers := TDictionary <TCommonCustomForm, TRectangle>.Create;
  Application.OnIdle := OnIdle;
end;

destructor TAutoFormDimmer.Destroy;
begin
  Forms.Free;
  Dimmers.Free;
  inherited;
end;

procedure TAutoFormDimmer.OnIdle (Sender: TObject; var Done: boolean);
var
  i : integer;
  ModalForm: TCommonCustomForm;
begin
  ModalForm := Screen.ActiveForm;

  if OnlyForModal and (ModalForm <> nil) and not (TFmxFormState.Modal in ModalForm.FormState)
    then ModalForm := nil;

  for i := 0 to Screen.FormCount - 1 do
    begin
      var f := Screen.Forms[i];

      if f.Visible and Forms.Contains(f) and ( (ModalForm = nil) or (ModalForm = f) ) then
        begin
          Forms.Remove (f);
          Dimmers[f].Visible := false;
        end;

      if f.Visible and not Forms.Contains(f) and (ModalForm <> nil) and (ModalForm <> f) then
        begin
          if not Dimmers.ContainsKey (f)
            then begin
                   var fdim := TRectangle.Create (f);
                   fdim.Opacity := 0.15;
                   fdim.Parent := f;
                   Dimmers.Add(f, fdim);
                 end;
          Dimmers[f].SetBounds(0, 0, f.Width, f.Height);
          Dimmers[f].BringToFront;
          Dimmers[f].Visible := true;

          Forms.Add (f);
        end;
    end;
end;

function GetParentForm(Control: TFmxObject): TCommonCustomForm;
begin
  if (Control.Root <> nil) and (Control.Root.GetObject is TCommonCustomForm) then
    Result := TCommonCustomForm(Control.Root.GetObject)
  else
    Result := nil;
end;

function ConfirmationDialog(const Text: string): boolean;
begin
  result := TDialogServiceSync.MessageDialog(Text, TMsgDlgType.mtConfirmation,
      [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], TMsgDlgBtn.mbNo, 0) = mrYes;
end;

{TControlHelper}

function TFLowLayoutlHelper.ChildrenWidth: Single;
var
  VIndex: Integer;
  VControl: TControl;
  VRect: System.Types.TRectF;
begin
  VRect := TRectF.Empty;
  Result := VRect.Width;
  if not ( ClipChildren or SmallSizeControl ) and ( Controls <> nil ) then
  begin
    for VIndex := GetFirstVisibleObjectIndex to GetLastVisibleObjectIndex - 1 do
    begin
      VControl := Controls[ VIndex ];
      if VControl.Visible then
        VRect := System.Types.UnionRect( VRect, VControl.Margins.MarginRect( TRectF.Create( VControl.Position.Point, VControl.Width, VControl.Height ) ) );
    end;
    Result := VRect.Width + Padding.Right;
  end;

end;

function TFLowLayoutlHelper.ChildrenHeight: Single;
var
  VIndex: Integer;
  VControl: TControl;
  VRect: System.Types.TRectF;
begin
  VRect := TRectF.Empty;
  Result := VRect.Height;
  if not ( ClipChildren or SmallSizeControl ) and ( Controls <> nil ) then
  begin
    for VIndex := GetFirstVisibleObjectIndex to GetLastVisibleObjectIndex - 1 do
    begin
      VControl := Controls[ VIndex ];
      if VControl.Visible then
        VRect := System.Types.UnionRect( VRect, VControl.Margins.MarginRect( TRectF.Create( VControl.Position.Point, VControl.Width, VControl.Height ) ) );
    end;
    Result := VRect.Height + Padding.Bottom;
  end;
end;

procedure FocusControlOrChild (Form: TCustomForm);
begin
  for var iCom := 0 to Form.ComponentCount - 1 do
    if (Form.Components[iCom] is TControl) and (TControl(Form.Components[iCom]).TabOrder = 0)
       and (TControl(Form.Components[iCom]).TabStop) and not (Form.Components[iCom] is TLabel)
       then
      begin
        FocusControlOrChild(Form.Components[iCom] as TControl);
        break;
      end;
end;

function ControlsSortedByTabOrder (Parent: TControl): TList<TControl>;
var
  sc: TControl;
begin
  result := TList<TControl>.Create;

  for sc in Parent.Controls do
    result.Add(sc);

  result.Sort(
      TComparer<TControl>.Construct(
        function(const Left, Right: TControl): Integer
        begin
          Result := Left.TabOrder - Right.TabOrder;
        end));
end;

function FocusControlOrChild (AControl: TControl): TControl;
var
  list: TList<TControl>;
  con: TControl;
begin
  if AControl.CanFocus and AControl.Enabled and AControl.Visible and AControl.TabStop then
    begin
      AControl.SetFocus;
      exit (AControl);
    end;

  result := nil;
  list := ControlsSortedByTabOrder (AControl);
  try
    for con in list do
      begin
        result := FocusControlOrChild (con);
        if result <> nil then exit;
      end;
  finally
    list.Free;
  end;
end;

{    begin
      var l := TLayout (c);
      for var sc in l.Controls do
        if (sc.TabOrder = 0) then
          if sc.CanFocus
            then
              begin
                sc.SetFocus;
                exit (sc);
              end
            else if sc is TLayout then
              begin
                result := FocusControlOrChild (sc);
                if result <> nil
                   then exit;
              end;
    end}

procedure FocusNextControl (c: TControl);
begin
  for var sc in c.ParentControl.Controls do
    if (sc.TabOrder = c.TabOrder + 1) then
      begin
        if sc.CanFocus
          then
            begin
              sc.SetFocus;
              exit;
            end
          else if sc is TLayout then
            begin
              FocusControlOrChild (sc);
              exit;
            end
          else FocusNextControl (sc);
        break;
      end;
end;

{ TControlHelper }

function TControlHelper.FindChildrenByClass<T>: TList<T>;
var
  Stack: TStack<TControl>;
  Current: TControl;
  Child: TControl;
begin
  result := TList<T>.Create;
  Stack := TStack<TControl>.Create;
  try
    // Push the parent onto the stack
    Stack.Push(self);

    // Process the stack until it's empty
    while Stack.Count > 0 do
    begin
      Current := Stack.Pop;

      // If the current control is of type T, add it to the result list
      if Current is T then
        result.Add(T(Current));

      // Push all children of the current control onto the stack
      for Child in Current.Controls do
        Stack.Push(Child);
    end;

  finally
    Stack.Free;
  end;
end;

function TControlHelper.VisibleIncludingParents: boolean;
var
  p: TControl;
begin
  if not Visible then exit (false);
  p := ParentControl;
  // ... if no parent then assume its on form I guess?
  while p.IsAssigned do
    begin
      if not p.Visible then exit (false);
      p := p.ParentControl;
    end;
  result := true;
end;

{ TRestrictedSizeForm }

function TRestrictedSizeForm.GetControlBottomRelativeToForm(ctrl: TControl): Single;
var
  Pos: TPointF;
begin
  // Get absolute position of the control
  Pos := ctrl.LocalToAbsolute(TPointF.Create(0, 0));

  // Convert the position relative to the form
//  Pos := Self.AbsoluteToLocal(Pos);

  // Calculate the bottom by adding the control's height to its Y position
  Result := Pos.Y + ctrl.Height;
end;

procedure TRestrictedSizeForm.AutoHeight;
var
  I: Integer;
  bot, MaxBottom: Single;
  Control: TControl;
begin
  MaxBottom := 0;

  // Iterate through all children of the form
  for I := 0 to Self.ComponentCount - 1 do
  begin
    if Self.Components[I] is TControl then
    begin
      Control := TControl(Self.Components[I]);

      // Find the bottom of the control (position + height)
      //if Control.Visible and (Control.Position.Y + Control.Height > MaxBottom) then
      if Control.Visible then
        begin
          bot := GetControlBottomRelativeToForm(Control);
          if (bot < Screen.Height) and (bot > MaxBottom) then
            begin
              //MaxBottom := Control.Position.Y + Control.Height;
              MaxBottom := bot
            end;
        end;
    end;
  end;

  // Set the form's height to ensure all controls are visible
  Self.Height := round (min (Screen.Height - 50, MaxBottom + Self.Padding.Bottom + Self.Padding.Top));
  Self.MinHeight := Self.Height;
end;

function IsLeftMouseButtonDown: Boolean;
begin
  Result := (GetAsyncKeyState(VK_LBUTTON) and $8000) <> 0;
end;

function RelevantParentTabOrder (Control: TControl): integer;
var
  parent: TFmxObject;
begin
  parent := Control.Parent;
  while true do
    begin
      if (parent is TTabItem) or not parent.Parent.IsAssigned then break;
      if (parent.Parent.ChildrenCount > 1) and not (parent.parent is TTabItem) then break; // TabItem ima 2 dece, content i jos nesto...
      parent := parent.Parent;
    end;

  if parent is TTabItem
     then result := TTabItem(parent).Index
     else if parent is TControl
          then result := TControl(parent).TabOrder
          else result := 0; //form?
end;

procedure TRestrictedSizeForm.AfterConstruction;
//var
//  MaxLeft, MaxTop: single;
begin
  inherited;

  {$IF CompilerVersion < 34.0}
  MinWidth := ClientWidth;
  MinHeight := ClientHeight;
  {$ENDIF}

  if intfFormStyler <> nil
     then intfFormStyler.ApplyStyle(self);

//  MaxLeft := 0;
//  MaxTop := 0;
//  for var i := 0 to ChildrenCount - 1 do
//    begin
//      var Child := Children[I];
//      if Child is TControl then
//        begin
//          var ctrl := TControl(Child);
//          if (MinWidth  < ctrl.Position.X + ctrl.Width) and not (ctrl.Align in [TAlignLayout.Client, TAlignLayout.Top, TAlignLayout.Bottom])
//             then MinWidth := ctrl.Position.X + ctrl.Width;
//          if (MinHeight < ctrl.Position.Y + ctrl.Height) and not (ctrl.Align in [TAlignLayout.Client, TAlignLayout.Right, TAlignLayout.Left])
//             then MinHeight := ctrl.Position.Y + ctrl.Height;
//          if MaxLeft   < ctrl.Position.X + ctrl.Margins.Left then MaxLeft  := ctrl.Position.X + ctrl.Margins.Left;
//          if MaxTop    < ctrl.Position.Y + ctrl.Margins.Top  then MaxTop   := ctrl.Position.Y + ctrl.Margins.Top;
//        end;
//    end;
//  MinWidth := MinWidth + MaxLeft;
//  MinHeight := MinHeight + MaxTop;
end;

{$IF CompilerVersion < 34.0}
procedure TRestrictedSizeForm.Resize;
var
  LInput: TInput;
begin
  inherited;

  if IgnoreResize then
    begin

      exit;
    end;

//  if Top < 0 then
//    begin
//      Top := 0;
//      exit;
//    end;

  IgnoreResize := true;

  if ClientHeight < MinHeight then
    begin

      ClientHeight := round(MinHeight);

      case Cursor of crSizeNESW, crSize, crSizeNS, crSizeNWSE, crSizeWE :
  //    if (GetAsyncKeyState(VK_LBUTTON) and $8000) <> 0 then
        begin
          FillMemory(@LInput, SizeOf(LInput), 0);
          LInput.Itype := INPUT_MOUSE;
          LInput.mi.dwFlags := MOUSEEVENTF_ABSOLUTE or MOUSEEVENTF_LEFTUP;
          SendInput(1, LInput, SizeOf(LInput));
        end;
        end;//case
    end;

  if ClientWidth < MinWidth then
    begin
      ClientWidth := round(MinWidth);
      case Cursor of crSizeNESW, crSize, crSizeNS, crSizeNWSE, crSizeWE :
  //    if (GetAsyncKeyState(VK_LBUTTON) and $8000) <> 0 then
        begin
          FillMemory(@LInput, SizeOf(LInput), 0);
          LInput.Itype := INPUT_MOUSE;
          LInput.mi.dwFlags := MOUSEEVENTF_ABSOLUTE or MOUSEEVENTF_LEFTUP;
          SendInput(1, LInput, SizeOf(LInput));
        end;
        end;//case
    end;

  IgnoreResize := false;
end;

function TRestrictedSizeForm.GetMinHeight: single;
begin
  result := FMinHeight;
end;

function TRestrictedSizeForm.GetMinWidth: single;
begin
  result := FMinWidth;
end;

procedure TRestrictedSizeForm.SetMinHeight(const Value: single);
begin
  FMinHeight := Value;
end;

procedure TRestrictedSizeForm.SetMinWidth(const Value: single);
begin
  FMinWidth := Value;
end;

{$ELSE}
function TRestrictedSizeForm.GetMinHeight: single;
begin
  result := Constraints.MinHeight;
end;

function TRestrictedSizeForm.GetMinWidth: single;
begin
  result := Constraints.MinWidth;
end;

procedure TRestrictedSizeForm.SetMinHeight(const Value: single);
begin
  Constraints.MinHeight := Value;
end;

procedure TRestrictedSizeForm.SetMinWidth(const Value: single);
begin
  Constraints.MinWidth := Value;
end;
{$ENDIF}

function TRestrictedSizeForm.ShowModal: TModalResult;
begin
  FIsModal := true;
  result := inherited ShowModal;
  FIsModal := false;
end;


procedure ShowErrorDialogue (const Error: string);
begin
  TDialogServiceSync.MessageDialog(Error, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], TMsgDlgBtn.mbOK, 0);
end;

procedure ShowErrorAndAbort (const Error: string);
begin
  ShowErrorDialogue (Error);
  abort;
end;

procedure ShowInformationAndAbort (const Information: string);
begin
  TDialogServiceSync.MessageDialog(Information, TMsgDlgType.mtInformation, [TMsgDlgBtn.mbOK], TMsgDlgBtn.mbOK, 0);
  abort;
end;


{ TStringGridHelper }

function TStringGridHelper.AddColumn (ColumnClass: TColumnClass; const Header: string; const TagString: string = ''; Width: integer = 0): TColumn;
begin
  result := ColumnClass.Create(self);
  result.Header := Header;
  result.TagString := TagString;
  if Width = 0
     then Width := 100;
  result.Width := max (Width, result.Width);
  result.Name := Name + 'Col' + inttostr (ColumnCount);
  AddObject(result);
end;

function TStringGridHelper.AddColumn<T>(const Header, TagString: string; Width: integer): T;
begin
  result := T (AddColumn (T, Header, TagString, Width));
end;

procedure TStringGridHelper.AutoColumnWidths (AMinimum: integer = 50; AMaximum: integer = 300);
var
  Column: TColumn;
  Bitmap : TBitmap;
  s: string;
  iRow, iCol, slen: Integer;
  TotalAvailable, Available, TotalWidth: single;
  TextWidths: TArray<single>;
begin
  // Create a temporary bitmap to measure text width
  BeginUpdate;
  TotalWidth := 0;
  SetLength (TextWidths, ColumnCount);
  Bitmap := TBitmap.Create;
  try
    Bitmap.Canvas.Font.Assign(TextSettings.Font);
    // Iterate through all columns
    for iCol := 0 to ColumnCount - 1 do
    begin
      Column := Columns[iCol];

      // Measure the width of the header text
      s := Column.Header;
      slen := s.Length;
      for iRow := 0 to min (100, RowCount - 1) do
        if Cells[iCol, iRow].Length > slen then
          begin
            s := Cells[iCol, iRow];
            slen := s.Length;
          end;

      // Set the column width to the text width plus some padding
      TextWidths[iCol] := Bitmap.Canvas.TextWidth(s) + 20;

      Column.Width := TextWidths[iCol];
      if Column.Width > AMaximum
         then Column.Width := AMaximum;
      TotalWidth := TotalWidth + Column.Width;
    end;

    TotalAvailable := Width - 50;

    // if there is extra width, try to satisfy the large columns first
    if TotalWidth < Width then
      for iCol := 0 to ColumnCount - 1 do
        begin
          Column := Columns[iCol];
          Available := TotalAvailable- TotalWidth;
          if (TextWidths[iCol] > AMaximum) then
            begin
              TotalWidth := TotalWidth - Column.Width;
              Column.Width := min (TextWidths[iCol], Column.Width + Available);
              TotalWidth := TotalWidth + Column.Width;
            end;
        end;

    // distribute remainder space equally, if there is remaining space
    if (TotalWidth > 0) and (TotalAvailable - TotalWidth > 0) then
      for iCol := 0 to ColumnCount - 1 do
        begin
          Column := Columns[iCol];
          Column.Width := Column.Width + round (TotalAvailable - TotalWidth) div ColumnCount;
        end;
  finally
    Bitmap.Free;
  end;
  EndUpdate;
  Width := Width + 1;
  Width := Width - 1;
  Repaint;
end;

function TStringGridHelper.CopyColumn(ACol: TColumn) : TColumn;
begin
  var col := TColumnClass(ACol.ClassType).Create(self);
  col.Header := ACol.Header;
  col.Tag := ACol.Tag;
  col.TagString := ACol.TagString;
  col.Width := ACol.Width;
  AddObject(col);
  Result := col;
end;

function TStringGridHelper.FindColumn(const Header: string): TColumn;
var
  i : integer;
begin
  for i := 0 to ColumnCount - 1 do
    if Columns[i].Header = Header then exit (Columns[i]);

  result := nil;
end;

function TStringGridHelper.FindColumnIndex(const Header: string): integer;
var
  i : integer;
begin
  for i := 0 to ColumnCount - 1 do
    if Columns[i].Header = Header then exit (i);

  result := -1;
end;

function TStringGridHelper.GetSelectedRowCells(index: integer): string;
begin
  result := Cells[index, row];
end;

function TStringGridHelper.GoToNextRow: boolean;
begin
  result := Row < RowCount - 1;
  if result
     then Row := Row + 1;
end;

procedure TStringGridHelper.LoadFromTSV(const FileName: TFileName);
var
  fields: TArray<string>;
  sl: TStringList;
  r, c: integer;
begin
  sl := TStringList.Create;
  sl.LoadFromFile(FileName, TEncoding.UTF8);
  RowCount := sl.Count;
  for r := 0 to sl.Count - 1 do
    begin
      fields := sl[r].Split([#9]);
      for c := low (fields) to high (fields) do Cells[c, r] := fields[c].Trim;
    end;

  sl.Free;
end;

procedure TStringGridHelper.SaveToTSV (const FileName: TFileName; const AColumns: TArray<integer>);
var
  sl: TStringList;
  s: string;
  c, r: integer;
begin
  sl := TStringList.Create;
  for r := 0 to RowCount - 1 do
    begin
      s := Cells[AColumns[0], r];
      for c := low (AColumns) + 1 to high (AColumns) do
        s := s + #9 + Cells[AColumns[c], r];
      sl.Add(s);
    end;
  sl.SaveToFile(FileName, TEncoding.UTF8);
  sl.Free;
end;

procedure TStringGridHelper.SetSelectedRowCells(index: integer; const Value: string);
begin
  Cells[index, row] := Value;
end;

{ TDateEditHelper }

function TDateEditHelper.GetSmartDate: TDate;
begin
  if IsEmpty then result := 0 else result := self.Date;
end;

procedure TDateEditHelper.SetSmartDate(d: TDate);
begin
  if d = 0
    then
      begin
        Date := now;
        IsEmpty := true;
      end
    else
      begin
        Date := d;
        IsEmpty := false;
      end;
end;

{ TTimerHelper }

procedure TTimerHelper.Restart;
begin
  if Enabled
     then OnTimer := OnTimer
     else Enabled := true;
end;

function HasTextProperty(obj: TObject): Boolean;
var
  ctx: TRttiContext;
  objType: TRttiType;
  prop: TRttiProperty;
begin
  ctx := TRttiContext.Create;
  objType := ctx.GetType(obj.ClassType);
  prop := objType.GetProperty('Text');
  Result := Assigned(prop) and (prop.Visibility > mvProtected) and (prop.PropertyType.TypeKind = tkString);
  ctx.Free;
end;

procedure SetTextProperty(obj: TObject; const value: string);
var
  ctx: TRttiContext;
  objType: TRttiType;
  prop: TRttiProperty;
begin
  ctx := TRttiContext.Create;
  objType := ctx.GetType(obj.ClassType);
  prop := objType.GetProperty('Text');
  if Assigned(prop) and (prop.PropertyType.TypeKind = tkString) then
    prop.SetValue(obj, value);
  ctx.Free;
end;

{procedure AutoTabOrder (obj: TFmxObject);
var
  control: TControl;
begin
  var lst := TList<TControl>.Create;
  for var iCom := 0 to obj.ComponentCount - 1 do
    if (obj.Components[iCom] is TControl) and not (obj.Components[iCom] is TLabel) and TControl (obj.Components[iCom]).TabStop
       then begin
              var index := 0;
              var c := TControl (obj.Components[iCom]);
              while (index<lst.Count) and
                    (
                      (lst[index].Position.Y < c.Position.Y)
                      or
                      ( (lst[index].Position.Y = c.Position.Y) and ((lst[index].Position.X < c.Position.X)) )
                    )
                do inc (index);
              lst.Insert (index, c);
              AutoTabOrder (c);
            end;

  for var i := 0 to lst.Count - 1 do
    begin
      control := lst[i];
      lst[i].TabOrder := i;
    end;
  lst.Free;
end;
}
procedure AutoTabOrder (frm: TCustomForm);
var
  control: TControl;
  iCom, i, index: integer;
begin
  var lst := TList<TControl>.Create;
  for iCom := 0 to frm.ComponentCount - 1 do
    if (frm.Components[iCom] is TControl) and not (frm.Components[iCom] is TLabel) and TControl (frm.Components[iCom]).TabStop
       then begin
              index := 0;
              control := TControl (frm.Components[iCom]);
              if control.Parent = frm
                then
                  begin
                    while (index<lst.Count) and
                          (
                            (lst[index].Position.Y < control.Position.Y)
                            or
                            ( (lst[index].Position.Y = control.Position.Y) and ((lst[index].Position.X < control.Position.X)) )
                          )
                      do inc (index);
                    lst.Insert (index, control);
                    AutoTabOrder (control);
                  end;
            end;

  for i := 0 to lst.Count - 1 do
    begin
      control := lst[i];
      control.TabOrder := i;
    end;
  lst.Free;
end;

procedure AutoTabOrder (ParentControl: TControl);
var
  control: TControl;
begin
  var lst := TList<TControl>.Create;
  for control in ParentControl.Controls do
    if not (control is TLabel) and control.TabStop
       then begin
              var index := 0;

              while (index<lst.Count) and
                    (
                      (lst[index].Position.Y < control.Position.Y)
                      or
                      ( (lst[index].Position.Y = control.Position.Y) and ((lst[index].Position.X < control.Position.X)) )
                    )
                do inc (index);
              lst.Insert (index, control);
              AutoTabOrder (control);
            end;

  for var i := 0 to lst.Count - 1 do
    begin
      control := lst[i];
      control.TabOrder := i;
    end;
  lst.Free;
end;


{ TTimeEditHelper }

function TTimeEditHelper.GetSmartTime: TTime;
begin
  if IsEmpty
     then result := 0
     else result := self.Time;
end;

procedure TTimeEditHelper.SetSmartTime(t: TTime);
begin
  if frac(t) = 0
    then
      begin
        Time := 0;
        IsEmpty := true;
      end
    else
      begin
        Time := t;
        IsEmpty := false;
      end;
end;

{ TLabelHelper }

procedure TLabelHelper.SetWarningState(Warning: boolean);
begin
  if Warning then
  begin
    StyledSettings := StyledSettings - [TStyledSetting.FontColor];
    TextSettings.FontColor := TAlphaColorRec.Red;
  end
  else
    StyledSettings := StyledSettings + [TStyledSetting.FontColor];
end;

{ TApplicationHelper }

function TApplicationHelper.ExeFolder: TFileName;
begin
  result := ExtractFilePath (ExeName);
end;

function TApplicationHelper.ExeName: TFileName;
begin
  result := ParamStr(0);
end;

function TListBoxHelper.CheckedCount: Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    if ListItems[i].IsChecked then
      Inc(Result);
end;

function TListBoxHelper.CheckedIndexes: TArray<Integer>;
var
  i, iOut: Integer;
begin
  SetLength(Result, CheckedCount);
  iOut := 0;
  for i := 0 to Count - 1 do
    if ListItems[i].IsChecked then
    begin
      Result[iOut] := i;
      Inc(iOut);
    end;
end;

function TListBoxHelper.SelectText(const s: string): boolean;
begin
  ItemIndex := items.IndexOf (s);
  result := ItemIndex <> -1;
end;

function FindForm (const Name: string): TCommonCustomForm;
var
  i: integer;
begin
  for i := 0 to Screen.FormCount - 1 do
    if Screen.Forms[i].Name = Name
       then exit (Screen.Forms[i]);
  result := nil;
end;

function DontStayOnTop: integer;
begin


  for var i := 0 to Screen.FormCount - 1 do


    begin
      var f := Screen.Forms[i];
      f.formstyle := TFormStyle.Normal;


    end;
  result := 0;
end;

procedure TOutcomeHelper.ShowError (const AdditionalContent: string = '');
begin
  if ErrorHandled then exit;

  ErrorHandled := true;

  if IsCriticalError
    then // supress critical errors, should be handled in re-login dialogue
    else
      begin
        var s: string := Description;
        if s = ''
           then s := format ('Unknown error %d', [ErrorCode]);
        if ErrorTableName <> ''
           then s := s + #13#10'Tabela: ' + ErrorTableName;
        if ErrorFieldName <>  ''
           then s := s + #13#10'Polje: ' + ErrorFieldName;

        if AdditionalContent <> ''
           then s := s + #13#10 + AdditionalContent;
        ShowErrorDialogue (s)
      end;
end;

function GetGridAutoHeight (Grid: TCustomGrid): single;
var
  RowHeight: single;
  TextRect: TRectF;
begin
  if Grid.RowHeight = 0
     then begin
            TextRect := TRectF.Create(0, 0, 1000, 1000);
            Grid.Canvas.MeasureText(TextRect, '�Qg', True, [], TTextAlign.Leading, TTextAlign.Leading);
            RowHeight := TextRect.Height + 6;
          end
     else RowHeight := Grid.RowHeight;
  result := max (3, Grid.RowCount + 2) * RowHeight + 12; {+2 rows to include horz scrollbar and status bar}
  if TGridOption.Header in Grid.Options
    then result := result + RowHeight;
end;

procedure ToggleChildCallout (Sender: TObject);
var
  spb : TSpeedButton;
  cap : TCalloutPanel;
begin;
  cap := nil;
  if not (sender is TSpeedButton) then exit;
  spb := (sender as TSpeedButton);
  for var p in spb.Children do
    if p is TCalloutPanel then
    begin
      cap := (p as TCalloutPanel);
      cap.Visible := not cap.Visible;
    end;
  if Assigned(cap) and cap.Visible then
    spb.StyleLookup := 'spintopbutton'
  else
    spb.StyleLookup := 'spinbottombutton'
end;

{ TCustomMemoHelper }

procedure TCustomMemoHelper.AutoHeight;
begin
  Height := GetAutoHeight;
end;

function TCustomMemoHelper.GetAutoHeight: integer;
var
  TextRect: TRectF;
  AText: string;
begin
  TextRect := TRectF.Create(0, 0, Width, 10000);
  if (Lines.Count > 1)
    then AText := Text + #13'�Qg'
    else AText := copy (Text, 3) + '�Qg�Qg�Qg�Qg�Qg �Qg�Qg�Qg�Qg�Qg';

  Canvas.MeasureText(TextRect, AText, True, [], TTextAlign.Leading, TTextAlign.Leading);
  result := ceil ((TextRect.Height + Padding.Top + Padding.Bottom + 12)); // malo promasuje...
end;

{ TCustomButtonHelper }

procedure TCustomButtonHelper.SimulateClick;
begin
  if Enabled then Click; //and Assigned (OnClick) then OnClick(self);
end;

procedure DumpFormStructure (form: TCommonCustomForm);
var
  s: string;
  level: integer;
  procedure Dump (c: TControl);
  var
    line: string;
  begin
    inc (level);
    line := '';
    line := line.PadLeft(level, '-');
    s := s + line + format ('%s [%s]'#13#10, [c.Name, c.ClassName]);
    for var child in c.controls do Dump (child);
    dec (level);
  end;

begin
  s := '';
  level := -1;
  for var c in form.Children do
    if c is TControl then Dump (TControl(c));
  var fn := Application.ExeName + '.' + form.Name + '.ctrl.dump';
  TFile.WriteAllText(fn, s);
end;

procedure DumpControlStructure (c: TControl);
var
  s: string;
  level: integer;
  procedure Dump (c: TControl);
  var
    line: string;
  begin
    inc (level);
    line := '';
    line := line.PadLeft(level, '-');
    s := s + line + format ('%s [%s]'#13#10, [c.Name, c.ClassName]);
    for var child in c.controls do Dump (child);
    dec (level);
  end;

begin
  s := '';
  level := -1;
  Dump (c);
  var fn := Application.ExeName + '.' + c.Name + '.ctrl.dump';
  TFile.WriteAllText(fn, s);
end;

{ TDefaultFont }

class procedure TDefaultFont.Setup;
begin
  Instance := TDefaultFont.Create;
  intfSystemFontService := Instance;
end;

constructor TDefaultFont.Create;
var
  intf: IFMXSystemFontService;
begin
  inherited;
  if Supports (TPlatformServices.Current, IFMXSystemFontService, intf)
    then begin
           OriginalSize := intf.GetDefaultFontSize + 2;
           FontFamilyName := intf.GetDefaultFontFamilyName;
         end
    else begin
           OriginalSize := 12;
           FontFamilyName := 'Tahoma';
         end;
  TPlatformServices.Current.RemovePlatformService(IFMXSystemFontService);
  TPlatformServices.Current.AddPlatformService(IFMXSystemFontService, self);
end;

function TDefaultFont.GetDefaultFontFamilyName: string;
begin
  Result := FontFamilyName;
end;

function TDefaultFont.GetDefaultFontSize: Single;
begin
  result := OriginalSize + SizeOffset;
end;


initialization

finalization
  AutoFormDimmer.Free;

end.
