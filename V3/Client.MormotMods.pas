unit Client.MormotMods;

interface

uses SynCommons, Mormot, MormotMods, SysUtils, Classes;

type

  TSQLRecordHelperClient = class helper (TSQLRecordHelper) for TSQLRecord
  private
    const
      CLIPBOARD_FORMAT_NAME = 'SqlRecord.ClipboardFormat';
    procedure AppendTabSeparatedValues (Builder: TStringBuilder; Fields: TStringList = nil);
    procedure AppendTabSeparatedFieldNames (AModel: TSQLModelEx; Builder: TStringBuilder; Fields: TStringList = nil);
  public
    procedure CopyToClipboard (AModel: TSQLModelEx; IncludeFieldNames: boolean; Fields: TStringList = nil);
    function PasteFromClipboard (const PreserveID: boolean): boolean;
  end;

implementation

uses
   System.JSON, FMX.Clipboard.Win, MormotTypes;

{ TSQLRecordHelper }

procedure TSQLRecordHelperClient.AppendTabSeparatedValues (Builder: TStringBuilder; Fields: TStringList = nil);
var
  FieldInfo: TFieldInfo;
  NeedsTab: boolean;
begin
  NeedsTab := false;
  for FieldInfo in FieldInfoList.Displayable do
    if (Fields = nil) or (Fields.IndexOf(FieldInfo.Name) > -1) then
    begin
      if NeedsTab
         then Builder.Append (#9);
      Builder.Append(FieldInfo.GetDisplayString(self));
      NeedsTab := true;
    end;
end;

procedure TSQLRecordHelperClient.AppendTabSeparatedFieldNames (AModel: TSQLModelEx; Builder: TStringBuilder; Fields: TStringList = nil);
var
  FieldInfo: TFieldInfo;
  NeedsTab: boolean;
begin
  NeedsTab := false;
  for FieldInfo in FieldInfoList.Displayable do
    if (Fields = nil) or (Fields.IndexOf(FieldInfo.Name) > -1) then
    begin
      if NeedsTab
         then Builder.Append (#9);
      Builder.Append(AModel.GetFieldDisplayName (RecordClass, FieldInfo.DisplayName));
      NeedsTab := true;
    end;
  Builder.Append (#13);
end;


procedure TSQLRecordHelperClient.CopyToClipboard (AModel: TSQLModelEx; IncludeFieldNames: boolean; Fields: TStringList = nil);
var
  InternalData: string;
  Builder: TStringBuilder;
begin
  if Fields = nil
     then InternalData := GetJSONValues (true, IDValue.IsNotNULL, RecordProps.CopiableFieldsBits {GetNonVoidFields}, []).AsString
          {ObjectToJSON (self).AsString}
     else InternalData := GetJSONValues (true, IDValue.IsNotNULL, StringToUTF8 (Fields.CommaText), []).AsString;

  InternalData := Format('%s'#9'%s', [SQLTableName, InternalData]);

  Builder := TStringBuilder.Create;

  try
    if IncludeFieldNames
       then AppendTabSeparatedFieldNames (AModel, Builder, Fields);
    AppendTabSeparatedValues(Builder, Fields);
    WindowsClipboard.AddCustomFormat (CLIPBOARD_FORMAT_NAME, InternalData, true);
    WindowsClipboard.AddText (Builder.ToString, false);
  finally
    Builder.Free;
  end;
end;

function TSQLRecordHelperClient.PasteFromClipboard (const PreserveID: boolean): boolean;
var
  InternalData: string;
  TableName, json: RawUTF8;
  posTab: integer;
  TempID: TID;
begin
  if PreserveID
     then TempID := IDValue
     else TempID := NULL_ID;

  InternalData := WindowsClipboard.GetCustomFormat(CLIPBOARD_FORMAT_NAME);
  posTab := pos (#9, InternalData);
  if posTab = 0 then exit (false);

  TableName.AsString := copy (InternalData, 1, posTab - 1);
  json.AsString := copy (InternalData, posTab + 1);

  if TableName <> SQLTableName then exit (false);

  FillFrom(json);
  if PreserveID
     then IDValue := TempID;

  result := true;
end;

end.
