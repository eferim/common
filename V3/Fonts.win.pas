﻿unit Fonts.Win;

interface

uses
  System.Classes;

type

  TFonts = class
  public
    class procedure GetSystemFonts(FontList: TStrings);
    class function FontSupportsUnicode(const FontName: string): Boolean;
  end;

implementation

uses
  system.SysUtils,
  Winapi.Windows;

{ TFonts }


function EnumFontsList(var LogFont: TLogFont; var TextMetric: TTextMetric;
FontType: Integer; Data: Pointer): Integer; stdcall;
var
  List: TStrings;
  fName: string;
begin
  List := TStrings(Data);
  fName := LogFont.lfFaceName;
  if (List.Count = 0) or (AnsiCompareText(List[List.Count-1], fName) <> 0) then
//    if TFonts.FontSupportsUnicode(fName) then
      List.Add(fName);
  Result := 1;
end;

class procedure TFonts.GetSystemFonts(FontList: TStrings);
var
  dContext: HDC;
  LFont: TLogFont;
begin
  dContext := GetDC(0);
  FillChar(LFont, sizeof(LFont), 0);
  LFont.lfCharset := DEFAULT_CHARSET;
  EnumFontFamiliesEx(dContext, LFont, @EnumFontsList, Winapi.Windows.LPARAM(FontList), 0);
  ReleaseDC(0, dContext);
end;

class function TFonts.FontSupportsUnicode(const FontName: string): Boolean;
var
  FontHandle: HFONT;
begin
  FontHandle := CreateFont(0, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
    CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH or FF_DONTCARE, PChar(FontName));

  try
    Result := (GetFontUnicodeRanges(FontHandle, nil) <> 0);
  finally
    DeleteObject(FontHandle);
  end;
end;

end.
