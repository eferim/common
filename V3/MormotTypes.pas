﻿unit MormotTypes;

interface

uses System.SysUtils, StrUtils, System.Generics.Collections, Variants, Classes, System.TypInfo,
     Mormot, Syncommons;

const
  NULL_ID = 0;
  AUTO_ID = -1; // for Update when ID needs to be looked up
  INVALID_ID = -2;

  {$IFDEF FIREBIRD_COLLATION} // TODO this is a temporary fix, databases could (?) be created or altered with
                              // ALTER CHARACTER SET utf8 set default collation unicode_ci_ai
  Filter_Collation: RawUTF8 = 'collate unicode_ci_ai';
  {$ELSE}
  Filter_Collation_CIS = '';// 'COLLATE "und-ci"';
  Filter_Collation_CS = '';//'COLLATE "und_cs"';
  {$ENDIF}

type
  {$SCOPEDENUMS ON}
  TAuthGroupID = TID;
  TFieldAlignment = (LeftMost, Left, None, Right, Rightmost);
  TCRUD = (Unknown, Create, Read, ReadMany, Update, UpdateMissingFields, Delete, ReadLock, Unlock);
  TBasicCRUD = (Create, Read, Update, Delete);
  TGeneralFilterFields = TList<RawUTF8>;

  TReferentialAction = (Restrict, Cascade, SetNull, SetDefault, NoAction);

var
  ReferentialActionStrings: array [TReferentialAction] of string = ('Restrict', 'Cascade', 'Set Null', 'Set Default', 'No Action');

type

  TRecordWithDetailsTableInfo = record
    TableName, Fields, ForeignKeyName: RawUTF8;
    Level, Count: integer;
  end;

  TRecordWithDetails = record
  private
    function GetTableCount: integer;
    function GetRecordCount: integer;
    procedure SetRecordCount(const Value: integer);
    procedure SetTableCount(const Value: integer);
  public
    TableName, Fields: RawUTF8;
    jsonRecord: RawUTF8;
    Tables: TArray<TRecordWithDetailsTableInfo>;
    Records: TArray<RawUTF8>;
//    procedure Add (rec: TRecordWithDetails); overload;
    procedure Add (const Level: integer; rec: TSqlRecord; const ForeignKeyName: RawUTF8 = ''; const Fields: RawUTF8 = '');
    procedure AddTable (const Level: integer; const ATable: TSQLRecordClass;
                        const AForeignKeyName: RawUTF8 = ''; const AFields: RawUTF8 = '');
    function LastTableName: RawUTF8;
    function LastTableLevel: integer;
    function LastTableIndex: integer;
    property RecordCount: integer read GetRecordCount write SetRecordCount;
    property TableCount: integer read GetTableCount write SetTableCount;
  end;

{  TRecordWithDetails = record
  private
    function GetCount: integer;
    procedure SetCount(const Value: integer);
  public
    TableName, Fields: RawUTF8;
    jsonRecord: RawUTF8;
    Details: TArray<TRecordWithDetails>;
    procedure Add (rec: TRecordWithDetails); overload;
    procedure Add (rec: TSqlRecord; Fields: RawUTF8 = ''); overload;
    property Count: integer read GetCount write SetCount;
  end;}

  TArrayOfString = array of string;
  PArrayOfString = ^TArrayOfString;

  TRawUTF8Helper = record helper for RawUTF8
  private
    const Max_SQL_Line_Width = 80;
    function GetAsString: string; inline;
    procedure SetAsString(const Value: string); inline;
    function GetAsAnsiString: AnsiString;
    function GetAsBoolean: boolean;
    procedure SetAsBoolean(const Value: boolean);
    function GetToUpper: RawUTF8;
    function GetIsEmpty: boolean;
    function GetIsNotEmpty: boolean;
    function Bracketed: RawUTF8;
  public
    procedure AppendSQLComma;
    procedure AppendSQL (const Value: RawUTF8); overload;
    procedure AppendSQL (const Value: string); overload;
    function AsFieldListToOrderBy: RawUTF8; // build "order by ..." string if not empty
    function Contains (const Value: RawUTF8): boolean;
    function SubString (const StartDelimiter, EndDelimiter: AnsiChar): RawUTF8; overload;
    function SubString (const StartDelimiter, EndDelimiter: RawUTF8): RawUTF8; overload;
    function ExtractSubString (const StartDelimiter, EndDelimiter: AnsiChar): RawUTF8;

    property AsString: string read GetAsString write SetAsString;
    property ToUpper: RawUTF8 read GetToUpper;
    property AsAnsiString: AnsiString read GetAsAnsiString;
    property AsBoolean: boolean read GetAsBoolean write SetAsBoolean;
    property IsEmpty: boolean read GetIsEmpty;
    property IsNotEmpty: boolean read GetIsNotEmpty;
  end;

  TOutcome = record
  public
    ID: int64;
    Success, ErrorHandled: boolean;
    ErrorCode: integer;
    RecordCount, WarningCode: integer;
    Description, ErrorTableName, ErrorFieldName: string;
    function SetError (AErrorCode: integer; ADescription: string = ''): TOutcome;
    function SetSuccess: TOutcome; inline;
    function SetDefaultSuccess: TOutcome; inline;
    function SetID (AID: int64): TOutcome; // sets ID, null is interpreted as failure
    function ToString: string;
    function IsCriticalError: boolean; // TODO nesto pametnije
    function DescriptionInSingleLine: string;
  end;

  TQueryPage = record
    Offset, Limit: uint64;
  end;

  TFilterOperator = (Less, LessOrEqual, Equal, Like, GreaterOrEqual, Greater, NotEqual, InValues, NotInValues, IsNull, IsNotNull);
  TFilterOperators = set of TFilterOperator;
  TFilterOperatorDynArray = TArray <TFilterOperator>;
//  TFilterCaseSensitivityArray = TArray <boolean>;
//  TFilterValueIsFieldNameArray = TArray <boolean>;

  TSqlSegment = (Where, WhereCondition, OrderBy, Limit);

  TSqlSegments = set of TSqlSegment;

  //TQueryFilterFlags
const
  qff_ValueIsSQL = 1;
  qff_CaseSensitive = 2;

type
  TQueryFilter = record
  private
    function GetCount: integer;
    procedure SetCount(const Value: integer);
  public
    //Mandatories: boolean;
    FieldNames: TRawUTF8DynArray;
    Operators: TFilterOperatorDynArray;
    Values: TVariantDynArray; // varobject so that we can set it to another field ie OnStock < Quantity
    Flags: TArray<cardinal>;
    Any: boolean; // i.e. Any=true means SQL OR, Any = false means SQL AND
    procedure Clear;

    function Add (const FieldName: RawUTF8; const FilterOperator: TFilterOperator; const AValue: variant; AFlags: cardinal = 0): integer; overload;
    function Add (const FieldName: string;  const FilterOperator: TFilterOperator; const AValue: variant; AFlags: cardinal = 0): integer; overload;
    function Add (const FieldName: string;  const FilterOperator: TFilterOperator; const AValues: TArray<integer>; AFlags: cardinal = 0): integer; overload;

    function Add (const FieldName: RawUTF8; const AValue: string): integer; overload;
    function AddForIDValues (const FieldName: RawUTF8; const FilterOperator: TFilterOperator; const AValues: TIDDynArray): integer;

    function AddOrSet(const FieldName: RawUTF8; const FilterOperator: TFilterOperator; const AValue: variant; AFlags: cardinal = 0): integer overload;
    function AddOrSet(const FieldName: string; const FilterOperator: TFilterOperator; const AValue: variant; AFlags: cardinal = 0): integer overload;
    function AddIfValid (const FieldName: RawUTF8; const FilterOperator: TFilterOperator; AValue: variant): integer;

    procedure AddFrom (Filter: TQueryFilter);

    function IndexOfField (const FieldName: RawUTF8; const StartAt: integer = 0): integer; overload;
    function IndexOfField (const FieldName: string; const StartAt: integer = 0): integer; overload;
    function ContainsField (const FieldName: RawUTF8): boolean; overload;
    function ContainsField (const FieldName: string): boolean; overload;
    function Fieldvalue (const FieldName: RawUTF8): variant; overload;
    function Fieldvalue (const FieldName: string): variant; overload;
    function ReplaceField (const FromField, ToField: RawUTF8): integer;
    function RemoveField (const FieldName: RawUTF8): integer; overload;
    function RemoveField (const FieldName: string): integer; overload;
    function CheckFlag (const index: integer; const flag: cardinal): boolean;

    function SQL: RawUTF8;
    function Check (rec: TSqlRecord): boolean;
    property Count: integer read GetCount write SetCount;
  end;

  TQueryParameters   = record
    Operation: TCRUD;
    Page: TQueryPage;
    Filter: TQueryFilter;
    Fields, Where, OrderBy,
    MasterTableName, // used for lookups when we only want lookup info for things that are referenced as FK
    GeneralFilter, // filter that should be applied as "like %" to all applicable fields, with OR
    IDSourceFieldName: RawUTF8; // e.g. foreign key field name that held the ID value for lookup
    ID, MasterID: TID;
    GUID: TGUID;
    // just the where condition, without "WHERE"
    function WhereConditionSql: RawUTF8;
    // full ORDER by statement including ORDER BY
    function OrderByStatementSql: RawUTF8;
    // full WHERE + ORDER BY statement
    function FullStatementSql: RawUTF8;
    // what mormot wants, where without "where" but with order by if this has order by
    function MormotSql: RawUTF8;

    function LimitSql: RawUTF8;

    function BuildSql (Segments: TSqlSegments): RawUTF8;

    procedure AddMandatoryCondition (const ACondition: RawUTF8);
    procedure AddOptionalCondition (const ACondition: RawUTF8);
    procedure ApplyGeneralFilter (Fields: TGeneralFilterFields; AOperator: RawUTF8);
    //todomaybe this causes weirdness, args come up messed up
    //procedure AddMandatoryFormat (const AFormat: RawUTF8; const Args: array of const);
    procedure AddOrderIfBlank (const OrderFields: RawUTF8);
    class procedure AddCondition(var CurrentFilter: RawUTF8; const NewCondition: RawUTF8; const Mandatory: boolean); static;
    class procedure SimpleAddCondition(var CurrentFilter: RawUTF8; const NewCondition: RawUTF8; const Mandatory: boolean); static;
    constructor Create (AWhere, AOrderBy: RawUTF8); overload;
    constructor Create (const AMasterTableName: RawUTF8; const AMasterID : TID); overload;
    constructor Create (const AMasterID : TID); overload;
    constructor Create (AOperation: TCRUD; AID: TID = NULL_ID); overload;
    function ToString: string;
    function Limited: boolean;
  end;

  TSqlRecordDetailIDs = record
  private
  public
    AggregateTableName, DetailTableName, MasterIDField, DetailIDField: RawUTF8;
    IDs: TIDDynArray;
    procedure Setup(const AAggregateTableName, ADetailTableName, AMasterIDField, ADetailIDField: RawUTF8); overload;
    procedure Setup(AggregateTable, DetailTable: TSqlRecordClass; const AMasterIDField, ADetailIDField: RawUTF8); overload;
  end;
//  pSqlRecordDetailIDs = ^TSqlRecordDetailIDs;

  TRequiredAuthLevels = array [TBasicCrud] of TAuthGroupID;
  TRequiredAuthLevelsArray = TArray<TRequiredAuthLevels>;

  TVarVarDictionary = class (TDictionary<variant, variant>)
    public
      function ToVariant: variant;
      constructor CreateFromVariant (const v: variant);
  end;

  function CompareValue (Value1, Value2: variant; AOperator: TFilterOperator): boolean;

var
  // PG has ILIKE but mormot cant seem to handle it i.e. internal sqlite is interfering?
  FilterOperatorAsSQL : array [TFilterOperator] of RawUTF8 = (
                                                            '<', '<=', '=', {$IFDEF FIREBIRD}'LIKE'{$ELSE}'LIKE'{$ENDIF}, '>=', '>', '<>', 'IN', 'NOT IN', 'IS NULL', 'IS NOT NULL');
  FilterOperatorAsText : array [TFilterOperator] of string = ('less', 'less or equal', 'equal', 'like', 'greater or equal', 'greater', 'different', 'in', 'not in', 'is null', 'is not null');
  FilterOperatorAsSymbol : array [TFilterOperator] of RawUTF8 = (
                                                            '<', '<=', '=', '~', '>=', '>', '<>', #$2208, #$2209, #2205, '≠ '#2205);

  CRUDToString: array [TCRUD] of string = ('Unknown', 'Create', 'Read', 'ReadMany', 'Update', 'Update missing fields', 'Delete', 'ReadLock', 'Unlock');
  BasicCrudToCrud: array [TBasicCrud] of TCrud = (TCrud.Create, TCrud.Read, TCrud.Update, TCrud.Delete);

implementation

uses Common.Utils, Transliteration, MormotMods;

{ TOutcome }

function TOutcome.DescriptionInSingleLine: string;
begin
  result := StringReplace(Description, #13, ' ', [rfReplaceAll]);
  result := StringReplace(result, #10, ' ', [rfReplaceAll]);
end;

function TOutcome.IsCriticalError: boolean;
begin
  result := ErrorCode < 0;
end;

function TOutcome.SetDefaultSuccess: TOutcome;
begin
  self := default (TOutcome);
  result := SetSuccess;
end;

function TOutcome.SetError(AErrorCode: integer; ADescription: string): TOutcome;
begin
  ErrorCode := AErrorCode;
  Description := ADescription;
  Success := false;
  result := self;
end;

function TOutcome.SetID(AID: int64): TOutcome;
begin
  ID := AID;
  Success := ID <> 0;
  result := self;
end;

function TOutcome.SetSuccess: TOutcome;
begin
  Success := true;
  ErrorCode := 0;
  Description := '';
  result := self;
end;

function TOutcome.ToString: string;
begin
  var sID    := IfThen (ID <> 0, format (', ID = %d', [ID]), '');
  var sSucc  := IfThen (Success, 'OK', 'FAIL');
  var sCount := IfThen (Success and (RecordCount > 0), format (', # = %d', [RecordCount]), '');

  if Success and (Description = '')
    then
      result := format ('%s%s%s', [sSucc, sID, sCount])
    else result := format ('%s%s, ErrorCode = %d, Description = %s',
            [sSucc, sID, ErrorCode, Description]);
end;

function TQueryFilter.SQL: RawUTF8;
var
  i, j: integer;
  value : variant;
  Collation : RawUTF8;
begin
  result := '';
  for i := 0 to Count - 1 do
    begin
      if i > 0 then if Any
         then result := result + ' OR '
         else result := result + ' AND ';
      value := Values[i];
      if VarType (value) = varBoolean
        then value := integer (value);

      var OperatorAsSql := FilterOperatorAsSQL[Operators[i]];

      case Operators[i] of
        TFilterOperator.Like:
          begin
            if VarIsStr(Values[i])
               then begin
                      var u, fn: RawUTF8;
                      if CheckFlag (i, qff_CaseSensitive)
                         then begin
                                u.AsString := Values[i];
                                fn := FieldNames[i];
                                Collation := Filter_Collation_CS;
                              end
                         else begin
                                u.AsString := AnsiLowerCase(Values[i]);
                                fn := 'lower(' + FieldNames[i] + ')';
                                Collation := Filter_Collation_CIS;
                              end;
                      result := result + FormatUTF8 ('(% % % ''%%%''', [fn, Collation, OperatorAsSql, '%', u, '%'])
                    end
               else result := result + FormatUTF8 ('(% % % ''%''', [FieldNames[i], '', OperatorAsSql, Values[i]]);
//            {$IFDEF FIREBIRD}
//            result := result + ' COLLATE UNICODE_CI';
//            {$ENDIF}
            result := result + ')'
          end;
        TFilterOperator.InValues, TFilterOperator.NotInValues:
          begin
            if CheckFlag (i, qff_ValueIsSQL) and VarIsStr(Values[i])
               then
                 begin
                    result := result + FormatUTF8 ('(% % (%))', [FieldNames[i], OperatorAsSql, Values[i]]);
                 end
               else
                 begin
                    result := result + FormatUTF8 ('(% % (', [FieldNames[i], OperatorAsSql]);
                    for j := 0 to TDocVariantData (Values[i]).Count-1 do
                      begin
                        if j > 0 then result := result + ', ';
                        result := result + StringToUTF8 (TDocVariantData (Values[i])[j]);
                      end;
                    result := result + ') )';
                 end;
          end;
        TFilterOperator.IsNull, TFilterOperator.IsNotNull:
          result := result + FormatUTF8 ('(% %)', [FieldNames[i], OperatorAsSql]);
        else
          if not CheckFlag (i, qff_ValueIsSQL) and VarIsStr(Values[i])
              then result := result + FormatUTF8 ('(% % %)', [FieldNames[i], OperatorAsSql, QuotedStr(Values[i])])
              else result := result + FormatUTF8 ('(% % %)', [FieldNames[i], OperatorAsSql, Values[i]]);
      end;
    end;
end;

// operator is part of value, if ommited default is equal
function TQueryFilter.Add (const FieldName: RawUTF8; const AValue: string): integer;
var
  op: TFilterOperator;
begin
  var temp := AValue.Trim;
  var NoOperator := false;
  case AValue[1] of
    '>' : if AValue[2] = '='
            then op := TFilterOperator.GreaterOrEqual
            else op := TFilterOperator.Greater;
    '<' : if AValue[2] = '='
            then op := TFilterOperator.LessOrEqual
            else op := TFilterOperator.Less;
    '=' : op := TFilterOperator.Equal;
    else begin
           NoOperator := true;
           op := TFilterOperator.Equal;
         end;
  end;

  if NoOperator then
    else if temp[2] = '='
      then temp := copy (temp, 3)
      else temp := copy (temp, 2);

  result := Add (FieldName, op, temp);
end;

function TQueryFilter.AddForIDValues (const FieldName: RawUTF8; const FilterOperator: TFilterOperator; const AValues: TIDDynArray): integer;
var
  v: variant;
begin
  result := Count;
  Count := Count + 1;
  FieldNames[result] := FieldName;
  Operators[result]  := FilterOperator;
  varClear (v);
  for var i := low (AValues) to High (AValues) do
    TDocVariantData (v).AddItem(AValues[i]);
  Values[result] := v;
end;

procedure TQueryFilter.AddFrom(Filter: TQueryFilter);
begin
  for var i := 0 to Filter.Count - 1 do
    Add (Filter.FieldNames[i], Filter.Operators[i], Filter.Values[i], Filter.Flags[i]);
end;

function TQueryFilter.Add (const FieldName: string; const FilterOperator: TFilterOperator; const AValue: variant;
                           AFlags: cardinal = 0): integer;
begin
  result := Add (StringToUTF8(FieldName), FilterOperator, AValue, AFlags);
end;

function TQueryFilter.Add (const FieldName: string; const FilterOperator: TFilterOperator; const AValues: TArray<integer>;
              AFlags: cardinal = 0): integer;
begin
  var Values := VarArrayCreate([low (AValues), high (AValues)], varInteger);

  for var i := low (AValues) to high (AValues) do
    Values[i] := AValues[i];

  result := Add (FieldName, FilterOperator, Values, AFlags);
end;

function TQueryFilter.Add (const FieldName: RawUTF8; const FilterOperator: TFilterOperator; const AValue: variant;
                           AFlags: cardinal = 0): integer;
var
  v: variant;
  i: integer;
begin
  if ((AFlags and qff_ValueIsSQL) > 0)
     and (FilterOperator in [TFilterOperator.Like, TFilterOperator.IsNull, TFilterOperator.IsNotNull])
         then raise Exception.Create('Filter value cant be SQL for this operator');

  result := Count;
  Count := Count + 1;
  FieldNames[result] := FieldName;
  Operators[result]  := FilterOperator;
  Flags[result] := AFlags;
  if (FilterOperator in [TFilterOperator.InValues, TFilterOperator.NotInValues]) and (AFlags and qff_ValueIsSQL = 0)
    then begin
           varClear (v);
           if DocVariantType.IsOfType(AValue)
              then
                v := AValue
              else
               for i := 0 to VarArrayHighBound(AValue, 1) do
                 TDocVariantData (v).AddItem(AValue[i]);
           Values[result] := v;
         end
    else Values[result]     := AValue;
end;

function TQueryFilter.AddIfValid(const FieldName: RawUTF8; const FilterOperator: TFilterOperator; AValue: variant): integer;
begin
  if (FilterOperator in [TFilterOperator.Like]) and
     ( VarIsNull(AValue) or (AValue = '') )
     then exit (-1);

  if (FilterOperator in [TFilterOperator.IsNull])
     then AValue := varNull;

  result := Add (FieldName, FilterOperator, AValue);
end;

function TQueryFilter.AddOrSet(const FieldName: string; const FilterOperator: TFilterOperator; const AValue: variant;
         AFlags: cardinal = 0): integer;
begin
  result := AddOrSet (StringToUTF8(FieldName), FilterOperator, AValue, AFlags);
end;

function TQueryFilter.AddOrSet(const FieldName: RawUTF8; const FilterOperator: TFilterOperator;
         const AValue: variant; AFlags: cardinal = 0): integer;
begin
  if ContainsField (FieldName)
     then RemoveField(FieldName);
  result := Add(FieldName, FilterOperator, AValue, AFlags);
end;

function TQueryFilter.Check(rec: TSqlRecord): boolean;
var
  v: variant;
begin
  result := false;
  for var i := 0 to Count - 1 do
    begin
      v := rec.GetFieldVariant (FieldNames[i].AsString);

      case Operators[i] of
        TFilterOperator.Like : result := UpperCaseUnicode(VariantToUTF8(v)).ToUpper.Contains(UpperCaseUnicode(VariantToUTF8(Values[i])));
//        Less,
//        LessOrEqual,
        TFilterOperator.Equal: result := v = Values[i];
//        GreaterOrEqual,
//        Greater,
//        NotEqual,
//        InValues,
//        IsNull,
//        IsNotNull;
          else raise Exception.Create('Operator not supported');
      end;
      if Any and result then exit (true);
      if not Any and not result then exit (false);
    end;
end;

function TQueryFilter.CheckFlag(const index: integer; const flag: cardinal): boolean;
begin
  result := (Flags[index] and flag) > 0
end;

procedure TQueryFilter.Clear;
begin
  SetCount (0);
end;

function TQueryFilter.ContainsField(const FieldName: string): boolean;
begin
  result := ContainsField (StringToUTF8(FieldName));
end;

function TQueryFilter.Fieldvalue(const FieldName: string): variant;
begin
  result := FieldValue (StringToUTF8(FieldName));
end;

function TQueryFilter.ContainsField(const FieldName: RawUTF8): boolean;
begin
  result := IndexOfField(FieldName) > -1;
end;

function TQueryFilter.Fieldvalue(const FieldName: RawUTF8): variant;
begin
  var index := IndexOfField(FieldName);
  if index = -1
    then VarClear(result)
    else result := Values[index];
end;

function TQueryFilter.IndexOfField (const FieldName: RawUTF8; const StartAt: integer = 0): integer;
var
  i : integer;
begin
  var ff := lowercase (FieldName);
  for i := StartAt to Count - 1 do
    if lowercase (FieldNames[i]) = ff then
        exit (i);
  result := -1;
end;

function TQueryFilter.GetCount: integer;
begin
  result := Length(FieldNames);
end;

function TQueryFilter.IndexOfField(const FieldName: string; const StartAt: integer): integer;
begin
  result := IndexOfField(StringToUTF8 (FieldName), StartAt);
end;

function TQueryFilter.RemoveField(const FieldName: RawUTF8): integer;
var
  index: integer;
begin
  result := -1;
  while true do
    begin
      index := IndexOfField (FieldName);
      if index > -1
        then
          begin
            result := index;
            FieldNames [index] := FieldNames [Count-1];
            Operators [index] := Operators [Count-1];
            Values [index] := Values [Count-1];
            Flags [index] := Flags [Count-1];
            Count := Count - 1;
          end
        else break;
    end;
end;

function TQueryFilter.RemoveField(const FieldName: string): integer;
begin
  result := RemoveField(StringToUTF8(FieldName));
end;

function TQueryFilter.ReplaceField(const FromField, ToField: RawUTF8): integer;
var
  i : integer;
begin
  var ff := lowercase (FromField);
  result := 0;
  for i := 0 to Count - 1 do
    if lowercase (FieldNames[i]) = ff then
      begin
        FieldNames[i] := ToField;
        inc (result);
      end;
end;

procedure TQueryFilter.SetCount(const Value: integer);
begin
  SetLength(FieldNames, Value);
  SetLength(Operators, Value);
  SetLength(Values, Value);
  SetLength(Flags, Value);
end;

procedure TQueryParameters.AddMandatoryCondition(const ACondition: RawUTF8);
begin
  AddCondition(Where, ACondition, true);
end;

procedure TQueryParameters.AddOptionalCondition (const ACondition: RawUTF8);
begin
  AddCondition(Where, ACondition, false);
end;

//procedure TQueryParameters.AddMandatoryFormat (const AFormat: RawUTF8; const Args: array of const);
//begin
//  AddMandatoryCondition(Where, FormatUTF8(AFormat, Args));
//end;

procedure TQueryParameters.AddOrderIfBlank(const OrderFields: RawUTF8);
begin
  if OrderBy = '' then OrderBy := OrderFields;
end;

procedure TQueryParameters.ApplyGeneralFilter(Fields: TGeneralFilterFields; AOperator: RawUTF8);
var
  Filters: TList<RawUTF8>;
  filter, Left, Right, gf, EscapedFilterSegment, EscapedFilterSegmentTL: RawUTF8;
begin
  Filters := TList<RawUTF8>.Create;
//  gf := LowerCaseU (GeneralFilter);// AnsiUpperCase(GeneralFilter);
  gf.AsString := AnsiLowerCase (GeneralFilter.AsString);

//  UpperCaseU

  {$IFDEF FIREBIRD}
  if AOperator = 'LIKE' then
     AOperator := Filter_Collation + ' ' + AOperator;
  {$ENDIF}

  while gf<>'' do
    begin
      filter := '';
      Split(gf, ' ', Left, Right);
      gf := trim (Right);
      EscapedFilterSegment := QuotedStr ('%' + left + '%');
      EscapedFilterSegmentTL := '';
      for var FieldName in Fields do
        if FieldName.AsString.EndsWith ('_TL')
          then
            begin
              if EscapedFilterSegmentTL = ''
                 then EscapedFilterSegmentTL.AsString := TSimpleTranslit.ToLatin (EscapedFilterSegment.AsString);
              TQueryParameters.SimpleAddCondition(filter, FormatUTF8('LOWER' + '(%::text) % %',
                                                          [FieldName, AOperator, EscapedFilterSegmentTL]), false);
            end
          else TQueryParameters.SimpleAddCondition(filter, FormatUTF8('LOWER' + '(%::text) % %',
                                                           [FieldName, AOperator, EscapedFilterSegment]), false);
      Filters.Add (filter);
    end;

  filter := '';
  for var f in Filters do
    AddCondition(filter, f, true);

  AddMandatoryCondition(filter);
  Filters.Free;
end;

function TQueryParameters.BuildSql(Segments: TSqlSegments): RawUTF8;
begin
  result := '';

  if (TSqlSegment.WhereCondition in Segments) then
    begin
      var w := WhereConditionSql;
      if (w <> '') then
        begin
        if TSqlSegment.Where in Segments
          then result := 'WHERE ' + w + #13
          else result := w + #13
        end;
    end;

  if (TSqlSegment.OrderBy in Segments) and (OrderBy <> '')
     then result := result + #13 + OrderBy.AsFieldListToOrderBy;
  if (TSqlSegment.Limit in Segments) and Limited
     then result := result + #13 + LimitSql;
end;

constructor TQueryParameters.Create(AOperation: TCRUD; AID: TID);
begin
  self := default (TQueryParameters);
  inherited;
  Operation := AOperation;
  ID := AID;
end;

constructor TQueryParameters.Create(const AMasterTableName: RawUTF8; const AMasterID: TID);
begin
  self := default (TQueryParameters);
  inherited;
  MasterID := AMasterID;
  MasterTableName := AMasterTableName;
end;

constructor TQueryParameters.Create(const AMasterID: TID);
begin
  self := default (TQueryParameters);
  inherited;
  MasterID := AMasterID;
end;

constructor TQueryParameters.Create(AWhere, AOrderBy: RawUTF8);
begin
  self := default (TQueryParameters);
  inherited;
  Where := AWhere;
  OrderBy := AOrderBy;
end;

const
  AndOr: array [false..true] of RawUTF8 = (' OR ', ' AND ');

class procedure TQueryParameters.AddCondition(var CurrentFilter: RawUTF8; const NewCondition: RawUTF8; const Mandatory: boolean);
begin
  if (CurrentFilter <> '')
    then CurrentFilter := CurrentFilter.Bracketed + AndOr[Mandatory] + NewCondition.Bracketed
    else CurrentFilter := NewCondition;
end;

class procedure TQueryParameters.SimpleAddCondition(var CurrentFilter: RawUTF8; const NewCondition: RawUTF8; const Mandatory: boolean);
begin
  if (CurrentFilter <> '')
    then CurrentFilter := CurrentFilter + AndOr[Mandatory] + NewCondition.Bracketed
    else CurrentFilter := NewCondition.Bracketed;
end;

function TQueryParameters.WhereConditionSql: RawUTF8;
var
  tempWhere, FilterSQL: RawUTF8;
begin
  tempWhere := Where;
  FilterSQL := Filter.SQL;
  if FilterSql<> ''
     then AddCondition(tempWhere, FilterSQL, true);
//  if MasterTableName <> ''
//     then AddCondition(tempWhere, MasterTableName + ' = ' + MasterID.ToRawUTF8, true);

  if tempWhere <> '' then result := tempWhere + sLineBreak + sLineBreak else result := '';
end;

function TQueryParameters.OrderByStatementSql: RawUTF8;
begin
  if OrderBy <> ''
    then result := 'ORDER BY ' + OrderBy
    else result := '';
end;

function TQueryParameters.ToString: string;
begin
//  result := GetEnumName(GetTypeData(TypeInfo (TCRUD), Operation);
  if Operation <> TCRUD.Unknown
     then result := 'CRUD: ' + System.TypInfo.GetEnumName(System.TypeInfo(TCRUD), integer(Operation))
     else result := '';
  if ID <> NULL_ID
     then result := result + sLineBreak + 'ID = ' + inttostr (ID) + sLineBreak;
  if MasterTableName <> ''
     then result := result + sLineBreak + 'MasterTableName = ' + MasterTableName.AsString + sLineBreak;
  if MasterID <> NULL_ID
     then result := result + sLineBreak + 'MasterID = ' + inttostr (MasterID) + sLineBreak;
  if GeneralFilter <> ''
     then result := result + sLineBreak + 'Gen.Filter = ' + GeneralFilter.AsString;

  result := result + sLineBreak + FullStatementSql.AsString;
end;

function TQueryParameters.FullStatementSql: RawUTF8;
begin
  var tempWhere := WhereConditionSql;
  if tempWhere <> ''
     then result := 'WHERE ' + tempWhere + sLineBreak + sLineBreak else result := '';
  result := result + sLineBreak + OrderByStatementSql + sLineBreak;
end;

function TQueryParameters.Limited: boolean;
begin
  result := Page.Limit + Page.Offset > 0;
end;

function TQueryParameters.LimitSql: RawUTF8;
begin
  if Limited
    then result := FormatUTF8('LIMIT % OFFSET %' + sLineBreak, [Page.Limit, Page.Offset])
    else result := '';
end;

function TQueryParameters.MormotSql: RawUTF8;
begin
  result := trim (WhereConditionSql + sLineBreak + OrderByStatementSql);
  if result <> '' then result := result + sLineBreak;
end;

{ TRawUTF8Helper }

procedure TRawUTF8Helper.AppendSQL(const Value: string);
begin
  AppendSQL (StringToUTF8(Value));
end;

procedure TRawUTF8Helper.AppendSQLComma;
begin
  self := self + ', ';
end;

procedure TRawUTF8Helper.AppendSQL(const Value: RawUTF8);
begin
  if length(self) - string (self).LastIndexOf(#13) > Max_SQL_Line_Width
     then self := self + #13#10;
  self := self + Value;
end;

function TRawUTF8Helper.GetIsEmpty: boolean;
begin
  result := self = '';
end;

function TRawUTF8Helper.GetIsNotEmpty: boolean;
begin
  result := self <> '';
end;

function TRawUTF8Helper.AsFieldListToOrderBy: RawUTF8;
begin
  if self = ''
     then result := ''
     else result := ' ORDER BY ' + self;
end;

function TRawUTF8Helper.Bracketed: RawUTF8;
begin
  if (self = '')
     then Exit (self);

  var ln := length(self);

  if (self[1] = '(') and (self[ln]=')')
    then
      begin
        var i := 2;
        var OpenCount := 1;

        while i < ln - 1 do // go until the penultimate char
          begin
            if self[ln] = '('
               then inc (OpenCount)
            else if self[ln] = ')'
               then dec (OpenCount);
            if OpenCount = 0 then break;
          end;

        if OpenCount = 1
          then exit (self)
          else exit ('('+self+')');
      end
    else
        exit ('('+self+')');
end;

function TRawUTF8Helper.Contains(const Value: RawUTF8): boolean;
begin
  result := Pos(Value, self) > -1;
end;

function TRawUTF8Helper.SubString (const StartDelimiter, EndDelimiter: AnsiChar): RawUTF8;
var
  p1, p2: integer;
begin
  p1 := pos (StartDelimiter, self);
  p2 := pos (EndDelimiter, self);
  if (p1 <> 0) and (p2 <> 0)
    then result := copy (self, p1, p2-p1)
    else result := '';
end;

function TRawUTF8Helper.SubString (const StartDelimiter, EndDelimiter: RawUTF8): RawUTF8;
var
  p1, p2: integer;
begin
  p1 := pos (StartDelimiter, self);
  p2 := pos (EndDelimiter, self);
  if (p1 <> 0) and (p2 <> 0)
     then result := copy (self, p1 + length (StartDelimiter), p2 - (length (StartDelimiter)) - 1)
     else result := '';
end;

function TRawUTF8Helper.ExtractSubString (const StartDelimiter, EndDelimiter: AnsiChar): RawUTF8;
var
  p1, p2: integer;
begin
  p1 := pos (StartDelimiter, self);
  p2 := pos (EndDelimiter, self);
  if (p1 <> 0) and (p2 <> 0)
    then
      begin
        result := copy (self, p1+1, p2-p1-1);
        self := copy (self, p2+1);
      end
    else result := '';
end;

function TRawUTF8Helper.GetAsAnsiString: AnsiString;
begin
  result := AnsiString (UTF8ToString(self));
end;

function TRawUTF8Helper.GetAsBoolean: boolean;
begin
  result := self = '1';
end;

function TRawUTF8Helper.GetAsString: string;
begin
  result := UTF8ToString(self)
end;

function TRawUTF8Helper.GetToUpper: RawUTF8;
begin
  result := UpperCaseU (self);
end;

procedure TRawUTF8Helper.SetAsBoolean(const Value: boolean);
begin
  if Value
     then self := '1'
     else self := '0';
end;

procedure TRawUTF8Helper.SetAsString(const Value: string);
begin
  self := StringToUTF8 (Value);
end;

{ TSqlRecordDetailIDs }

procedure TSqlRecordDetailIDs.Setup(const AAggregateTableName, ADetailTableName, AMasterIDField, ADetailIDField: RawUTF8);
begin
  AggregateTableName := AAggregateTableName;
  DetailTableName    := ADetailTableName;
  MasterIDField      := AMasterIDField;
  DetailIDField      := ADetailIDField;
end;

procedure TSqlRecordDetailIDs.Setup(AggregateTable, DetailTable: TSqlRecordClass; const AMasterIDField, ADetailIDField: RawUTF8);
begin
  Setup (AggregateTable.SQLTableName, DetailTable.SQLTableName, AMasterIDField, ADetailIDField);
end;

{ TVarVarDictionary }

constructor TVarVarDictionary.CreateFromVariant(const v: variant);
var
  i: integer;
  key, value, varpair: variant;
begin
  inherited Create;

  for i := 0 to TDocVariantData (v).Count - 1 do
    begin
      varpair := TDocVariantData (v).Values[i];

//      key := TDocVariantData(v).Values[i]; // Get key

      key := TDocVariantData (varpair).Names[0];
      value := TDocVariantData (varpair).Values[0];
      Add (key, value);
    end;
end;

function TVarVarDictionary.ToVariant: variant;
var
  varpair: variant;
begin
  varClear (result);

  for var pair in self do
      begin
        varpair := TDocVariant.NewObject([pair.Key, pair.Value]);
        TDocVariantData (result).AddItem(varpair);
      end;
end;

//var vv1, vv2: TVarVarDictionary;

{ TRecordWithDetails }

procedure TRecordWithDetails.Add (const Level: integer; rec: TSqlRecord; const ForeignKeyName: RawUTF8 = ''; const Fields: RawUTF8 = '');
begin
  if (LastTableName <> rec.crudSQLTableName) or (LastTableLevel <> Level)
    then raise Exception.Create('Add Table before adding records!');
    // force this^ otherwise server doesnt know to delete from tables when no records are added to them
      //AddTable (Level, rec.crudSQLTableName, ForeignKeyName, Fields);

  Tables[LastTableIndex].Count := Tables[LastTableIndex].Count + 1;

  RecordCount := RecordCount + 1;
  if Fields <> ''
    then Records[RecordCount - 1] := rec.GetJSONValues (true, rec.IDValue.IsNotNULL, Fields, [])
    else Records[RecordCount - 1] := rec.GetJSONValues (true, rec.IDValue.IsNotNULL, rec.RecordProps.CopiableFieldsBits
                                     {rec.GetNonVoidFields}, []);
end;

procedure TRecordWithDetails.AddTable (const Level: integer; const ATable: TSQLRecordClass;
                                       const AForeignKeyName: RawUTF8 = ''; const AFields: RawUTF8 = '');
begin
  TableCount := TableCount + 1;
  Tables[LastTableIndex].TableName := ATable.crudSQLTableName;
  Tables[LastTableIndex].ForeignKeyName := AForeignKeyName;
  Tables[LastTableIndex].Fields := AFields;
  Tables[LastTableIndex].Level := Level;
  Tables[LastTableIndex].Count := 0;
end;

function TRecordWithDetails.GetRecordCount: integer;
begin
  result := length(Records);
end;

function TRecordWithDetails.GetTableCount: integer;
begin
  result := length(Tables);
end;

function TRecordWithDetails.LastTableIndex: integer;
begin
  result := TableCount - 1;
end;

function TRecordWithDetails.LastTableLevel: integer;
begin
  if TableCount = 0
    then result := 0
    else result := Tables[TableCount - 1].Level;
end;

function TRecordWithDetails.LastTableName: RawUTF8;
begin
  if TableCount = 0
    then result := ''
    else result := Tables[TableCount - 1].TableName;
end;

procedure TRecordWithDetails.SetRecordCount(const Value: integer);
begin
  SetLength(Records, Value);
end;

procedure TRecordWithDetails.SetTableCount(const Value: integer);
begin
  SetLength(Tables, Value);
end;

function CompareValue (Value1, Value2: variant; AOperator: TFilterOperator): boolean;
begin
  case AOperator of
    TFilterOperator.Less: result := Value1 < Value2;
    TFilterOperator.LessOrEqual: result := Value1 <= Value2;
    TFilterOperator.Equal: result := Value1 = Value2;
    TFilterOperator.Like: raise Exception.Create('Operator not supported');
    TFilterOperator.GreaterOrEqual: result := Value1 >= Value2;
    TFilterOperator.Greater: result := Value1 > Value2;
    TFilterOperator.NotEqual: result := Value1 <> Value2;
    TFilterOperator.InValues:    begin
                                    if VarIsArray(Value2)
                                      then
                                        begin
                                          for var i := VarArrayLowBound(Value2, 1) to VarArrayHighBound(Value2, 1) do
                                            if Value1 = Value2[i]
                                              then Exit(True)
                                        end
                                      else if VarType(Value2) = DocVariantType.VarType then
                                          begin
                                            var p := _Safe(Value2)^; // _Safe is a helper to work with TDocVariant
                                              for var i := 0 to p.Count - 1 do
                                                if Value1 = p.Values[i] then
                                                  Exit(True);
                                          end;
                                    Result := False;
                                 end;
    TFilterOperator.NotInValues: begin
                                    if VarIsArray(Value2)
                                      then
                                        begin
                                          for var i := VarArrayLowBound(Value2, 1) to VarArrayHighBound(Value2, 1) do
                                            if Value1 = Value2[i] then
                                              Exit(false);
                                          Exit(true);
                                        end
                                      else if VarType(Value2) = DocVariantType.VarType then
                                          begin
                                            var p := _Safe(Value2)^; // _Safe is a helper to work with TDocVariant
                                              for var i := 0 to p.Count - 1 do
                                                if Value1 = p.Values[i] then
                                                  Exit(false);

                                            Exit(true);
                                          end;
                                     result := false; // unknown types, fail
                                 end;
    TFilterOperator.IsNull: result := VarIsEmptyOrNull(Value1) or (Value1 = 0); //hmm
    TFilterOperator.IsNotNull: result := not VarIsEmptyOrNull(Value1) and (Value1 <> 0); //hmm
    else result := false;
  end;
end;
initialization

//  vv1 := TVarVarDictionary.Create;
//  vv1.add (2024, 1000);
//  vv1.add (2025, 2000);
//  vv2 := TVarVarDictionary.CreateFromVariant (vv1.ToVariant);
//  vv2.ToVariant;
end.
