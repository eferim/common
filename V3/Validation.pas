﻿unit Validation;

interface

uses System.RTTI, SysUtils, Variants, Generics.Collections, Translator2Classes, Common.Utils,
     System.RegularExpressions, Classes;

const
  {$IFDEF ENGLISH}
  tcFieldXMustHaveValue = 'Field must have a value';
  tcFieldXTooLong       = 'Field value is too long';
  tcFieldXTooShort      = 'Field value is too short.';
  tcFieldXWrongLength   = 'Field value length is incorrect.';
  tcMinLength           = 'Min length is %d';
  tcMaxLength           = 'Max length is %d';
  tcExactLength         = 'Length must be %d';
  tcFieldMustHaveUniqueValue = 'Field must have an unique value';
  tcWrongFieldFormat    = 'Field "%s" has invalid formatting';
  tcDateCantBeLowerThan = 'Minimum date is "%s."';
  tcFieldValidationError = 'Field "%s" is not valid';
  {$ELSE}
  tcFieldXMustHaveValue = 'Polje mora imati vrednost';
  tcFieldXTooLong =       'Vrednost polja je predugačka.';
  tcFieldXTooShort =      'Vrednost polja je prekratka.';
  tcFieldXWrongLength   = 'Dužina polja nije ispravna.';
  tcMaxLength           = 'Maksimalna dužina je %d';
  tcMinLength           = 'Minimalna dužina je %d';
  tcExactLength         = 'Dužina mora biti %d';
  tcFieldMustHaveUniqueValue = 'Polje mora imati jedinstvenu vrednost';
  tcFieldValidationError = 'Polje "%s" nije validno. %s';
  tcWrongFieldFormat    = 'Polje "%s" nema odgovarajući format';
  tcDateCantBeLowerThan = 'Najmanji dozvoljeni datum je "%s."';
  {$ENDIF}

  vs_None = 0; // validation state

type

  IgnoreForImport = class(TCustomAttribute)
  end;

  TUINotNullInfo = record
    RequiredStates, ForbiddenStates: word;
    function Active (const States: word): boolean;
  end;

  UINotNull = class(TCustomAttribute)
  public
    Info: TUINotNullInfo;
    constructor Create (const ARequiredStates: word; AForbiddenStates: word = vs_None);
  end;

  ValidatorAttribute = class(TCustomAttribute)
  public
    Level: integer;
    IgnoreForImport: boolean;
    constructor Create (ALevel: integer = 0); virtual;
    function Validate(Target: TObject; p : TRttiProperty) : string; overload; virtual;
    function ValidateToBoolean(Target: TObject; p : TRttiProperty) : boolean; virtual;
  end;

  NotNullAttribute = class(ValidatorAttribute)
  public
    constructor Create (ALevel: integer = 0); overload; override;
    function Validate(Target: TObject; p : TRttiProperty) : string; override;
    function ValidateToBoolean(Target: TObject; p : TRttiProperty) : boolean; override;
  end;

  TrimmedNotNullAttribute = class(NotNullAttribute)
  public
    function ValidateToBoolean(Target: TObject; p : TRttiProperty) : boolean; override;
  end;

  TRegexEmailValidator = class
    private
      class var
      RegEx : TRegEx;
    public
      class constructor Create;
      class function Validate(const email : string) : boolean;
    end;

  EmailAddressAttribute = class(ValidatorAttribute)
    function Validate(Target: TObject; p : TRttiProperty) : string; override;
  end;

  MinDateAttribute = class(ValidatorAttribute)
  protected
    FMinimum: variant;
  public
    constructor Create (AMinimum: TDateTime = 0); reintroduce;
    function Validate(Target: TObject; p : TRttiProperty) : string; override;
  end;



//  ForeignKeyAttribute = class(ValidatorAttribute)
//  end;
//
//  NotNullForeignKeyAttribute = class(ForeignKeyAttribute)
//  public
//    function Validate(p : TRttiProperty; Target: TObject) : string; override;
//  end;

  MaxLengthAttribute = class(ValidatorAttribute)
  protected
    FLimit: uint64;
  public
    constructor Create (Limit: uint64; ALevel: integer = 0); reintroduce;
    function Validate(Target: TObject; p : TRttiProperty) : string; override;
  end;

  MinLengthAttribute = class(ValidatorAttribute)
  protected
    FLimit: uint64;
  public
    constructor Create (Limit: uint64; ALevel: integer = 0); reintroduce;
    function Validate(Target: TObject; p : TRttiProperty) : string; override;
  end;

  ExactLengthAttribute = class(ValidatorAttribute)
  protected
    FLimit: integer;
  public
    constructor Create (Limit: integer; ALevel: integer = 0); reintroduce;
    function Validate(Target: TObject; p : TRttiProperty) : string; override;
  end;

  function ValidateWithAttributes (Target : TObject; Level: integer; ForImport: boolean) : string; overload;
  function ValidateWithAttributes (Target : TObject; Level: integer; ForImport: boolean;
                                   out FirstFailedAttribute: TCustomAttribute; out FailedPropertyName, Error: string;
                                   const Fields: string = '') : boolean; overload;
  function ValidateWithAttributes (Target : TObject; prop: TRttiProperty; Level: integer; ForImport: boolean;
                                   out FirstFailedAttribute: TCustomAttribute; out FailedPropertyName, Error: string) : boolean; overload;
  function ValidateWithAttributes (Target : TObject; PropertyName: string) : boolean; overload;

  //function ValidationFailedText (const FieldName, Fmt: string; Target: TObject): string;
  function ValidateIsNotNull(Target: TObject; p : TRttiProperty) : boolean; overload;
  function ValidateIsNotNull(Target: TObject; const PropertyName: string) : boolean; overload;

implementation

uses
  System.TypInfo,
  mormotMods;

function ValidateWithAttributes (Target : TObject; PropertyName: string) : boolean;
var
  ctx : TRttiContext;
  t : TRttiType;
  p : TRttiProperty;
  FirstFailedAttribute: TCustomAttribute;
  FailedPropertyName, Error: string;
begin
  ctx := TRttiContext.Create;
  try
    t := ctx.GetType(Target.ClassType);
    p := t.GetProperty(PropertyName);
    result := ValidateWithAttributes(Target, p, 0, false, FirstFailedAttribute, FailedPropertyName, Error);
  finally
    ctx.Free;
  end;
end;

function ValidateWithAttributes(Target : TObject; prop: TRttiProperty; Level: integer; ForImport: boolean;
         out FirstFailedAttribute: TCustomAttribute; out FailedPropertyName, Error: string) : boolean;
var
  a : TCustomAttribute;
begin
  result := true;
  for a in prop.GetAttributes do
    begin
      if not (a is ValidatorAttribute) then continue;
      var va := ValidatorAttribute(a);
      if va.Level > Level then continue;
      if ForImport and va.IgnoreForImport then continue;
      Error := va.Validate(Target, prop);
      if Error <> '' then
        begin
          FirstFailedAttribute := va;
          FailedPropertyName := prop.Name;
          exit (false);
        end;
    end
end;

function ValidateWithAttributes(Target : TObject; Level: integer; ForImport: boolean;
                                out FirstFailedAttribute: TCustomAttribute; out FailedPropertyName, Error: string;
                                const Fields: string = '') : boolean;
var
  ctx : TRttiContext;
  t : TRttiType;
  p : TRttiProperty;
  m : TRttiMethod;
  slFields: TStringList;
begin
  if not Assigned(Target) then
    raise Exception.Create('Can''t validate nil object');

  Result := true;
  FirstFailedAttribute := nil;
  FailedPropertyName := '';

  if Fields.IsEmpty
    then slFields := nil
    else begin
           slFields := TStringList.Create;
           slFields.CommaText := Fields;
         end;

  ctx := TRttiContext.Create;
  try
    t := ctx.GetType(Target.ClassType);

    for m in t.GetMethods do // find functions that are decorated as validators
                             // and perform custom class-specific validation
      if MethodHasAttribute (m, ValidatorAttribute) then
        begin
          Error := m.Invoke (Target, []).ToString;
          if Error <> '' then exit;
        end;

    for p in t.GetProperties do
      begin
        if slFields.IsAssigned and not slFields.Contains (p.Name) then continue;
        result := ValidateWithAttributes (Target, p, Level, ForImport, FirstFailedAttribute, FailedPropertyName, Error);
        if not result then break;
      end;
  finally
    ctx.Free;
    slFields.Free;
  end;
end;

function ValidateWithAttributes(Target : TObject; Level: integer; ForImport: boolean) : string;
var
  ctx : TRttiContext;
  t : TRttiType;
  p : TRttiProperty;
  a : TCustomAttribute;
  m : TRttiMethod;
begin
  Result := '';

  if not Assigned(Target) then
    raise Exception.Create('Can''t validate nil object');

  ctx := TRttiContext.Create;
  try
    t := ctx.GetType(Target.ClassType);

    for m in t.GetMethods do // find functions that are decorated as validators
                             // and perform custom class-specific validation
      if MethodHasAttribute (m, ValidatorAttribute) then
        begin
          result := m.Invoke (Target, []).ToString;
          if result <> '' then exit;
        end;

    for p in t.GetProperties do
      begin
        for a in p.GetAttributes do
          begin
            if not (a is ValidatorAttribute) then continue;
            var va := ValidatorAttribute(a);
            if va.Level > Level then continue;
            if ForImport and va.IgnoreForImport then continue;
            result := va.Validate(Target, p);
            if result <> ''
               then exit;
          end
      end;
  finally
    ctx.Free;
  end;
end;

{ MandatoryForeignKeyAttribute }
{
function ValidationFailedText (const Fmt: string; Target: TObject): string;
begin
  result := Translator.TranslateDBField (Target.ClassName, FieldName);

//  if dicFieldNameTranslations = nil then exit (format (Fmt, [FieldName]));
//
//  var n: string := Target.ClassName + '.' + FieldName;
//  if dicFieldNameTranslations.ContainsKey (n)
//    then result := dicFieldNameTranslations[n]
//    else if dicFieldNameTranslations.ContainsKey (FieldName)
//      then result := dicFieldNameTranslations[FieldName]
//      else result := FieldName;
//  if result = '' then result := FieldName;
  result := format (Fmt, [result]);
end;
}
//function NotNullForeignKeyAttribute.Validate(p : TRttiProperty; Target: TObject; dicFieldNameTranslations: TDictionary<string, string>) : string;
//begin
//  if p.GetValue(Target).AsInt64 <= NULL_ID
//     then result := ValidationFailedText (p.Name, tcFieldXMustHaveValue, Target, dicFieldNameTranslations)
//     else result := '';
//end;

{ NotNullAttribute }

constructor NotNullAttribute.Create(ALevel: integer);
begin
  inherited;
  IgnoreForImport := true;
end;

function ValidateIsNotNull(Target: TObject; const PropertyName: string) : boolean; overload;
var
  ctx : TRttiContext;
  prop: TRttiProperty;
  rtype: TRttiType;
begin
  ctx := TRttiContext.Create;
  try
    rtype := ctx.GetType(Target.ClassType);
    prop := rtype.GetProperty(PropertyName);
    result := ValidateIsNotNull(Target, prop);
  finally
    ctx.Free;
  end;
end;

// return true if its "not null"
function ValidateIsNotNull(Target: TObject; p : TRttiProperty) : boolean;
var
  val: TValue;
begin
  val := p.GetValue(Target);
  case p.PropertyType.TypeKind of
    tkInt64, tkInteger: result := val.AsInt64 <> 0;
    tkFloat: result := val.AsExtended <> 0;
    tkChar, tkString, tkLString: result := val.AsString <> '';
    tkVariant:                   result := not val.IsEmpty;// not VarIsNull();aa
    tkEnumeration:               result := val.AsVariant > 0;
    tkClass:                     begin
                                   try
                                     // ugly workaround for sqlrecord foreign keys of class type
                                     // we only want to know that its not null
                                     // but if its a bad pointer this will crash
                                     // if it crashes, its not null
                                     //val := p.GetValue(Target);
                                     result := not val.IsEmpty;
                                   except
                                     result := true;
                                   end;
                                 end;
    else raise Exception.Create('Invalid property for not null checking, prop.name is ' + p.Name);
  end;
end;

function NotNullAttribute.ValidateToBoolean(Target: TObject; p : TRttiProperty) : boolean;
begin
    result := ValidateIsNotNull (Target, p);
end;

function NotNullAttribute.Validate(Target: TObject; p : TRttiProperty) : string;
begin
  if ValidateToBoolean (Target, p)
     then result := ''
     else result := tcFieldXMustHaveValue;
end;

{ MaxLengthAttribute }

constructor MaxLengthAttribute.Create(Limit: uint64; ALevel: integer = 0);
begin
  inherited Create (ALevel);
  FLimit := Limit;
  IgnoreForImport := true;
end;

function MaxLengthAttribute.Validate(Target: TObject; p : TRttiProperty) : string;
var
  ok : boolean;
begin
  // allow empty value, because that should be checked by NotNull attribute instead
  //  if not NotNullAttribute(nil).ValidateToBoolean (p, Target) then exit (''); <- crashes because it doesnt like cast hmmmmmm
  result := inherited; //Validate;
  if result <> '' then exit;// ValidateToBoolean (Target, p) then exit ('');

  case p.PropertyType.TypeKind of
    tkChar, tkString, tkLString:  ok := length (p.GetValue(Target).AsString) <= FLimit;
    else raise Exception.Create('Invalid property for MaxLengthAttribute');
  end;

  if ok
     then result := ''
     else result := tcFieldXTooLong + sLineBreak + format (tcMaxLength, [FLimit] );
end;

{ MinLengthAttribute }

constructor MinLengthAttribute.Create(Limit: uint64; ALevel: integer = 0);
begin
  inherited Create (ALevel);
  FLimit := Limit;
  IgnoreForImport := true;
end;

function MinLengthAttribute.Validate(Target: TObject; p: TRttiProperty): string;
var
  ok : boolean;
begin
  // allow empty value, because that should be checked by NotNull attribute instead
  if not NotNullAttribute(nil).ValidateToBoolean (Target, p) then exit ('');

  case p.PropertyType.TypeKind of
    tkChar, tkString, tkLString:  ok := length (p.GetValue(Target).AsString) >= FLimit;
    else raise Exception.Create('Invalid property for MaxLengthAttribute');
  end;

  if ok
     then result := ''
     else result := tcFieldXTooShort + sLineBreak + format (tcMinLength, [FLimit]);
end;

{ ExactLengthAttribute }

constructor ExactLengthAttribute.Create(Limit: integer; ALevel: integer = 0);
begin
  inherited Create (ALevel);
  FLimit := Limit;
  IgnoreForImport := true;
end;

function ExactLengthAttribute.Validate(Target: TObject; p: TRttiProperty): string;
var
  ok : boolean;
begin
  // allow empty value, because that should be checked by NotNull attribute instead
  case p.PropertyType.TypeKind of
    tkChar, tkString, tkLString:  ok := length (p.GetValue(Target).AsString) in [FLimit, 0];
    else raise Exception.Create('Invalid property for ExactLengthAttribute');
  end;

  if ok
     then result := ''
     else result := tcFieldXWrongLength + sLineBreak + format (tcExactLength, [FLimit]);
end;

{ ValidatorAttribute }

constructor ValidatorAttribute.Create(ALevel: integer);
begin
  Level := ALevel;
end;

function ValidatorAttribute.Validate(Target: TObject; p: TRttiProperty): string;
begin
  result := '';
end;

function ValidatorAttribute.ValidateToBoolean(Target: TObject; p: TRttiProperty): boolean;
begin
  result := Validate(Target, p) = '';
end;

{ TrimmedNotNullAttribute }

function TrimmedNotNullAttribute.ValidateToBoolean(Target: TObject; p: TRttiProperty): boolean;
begin
  result := not p.GetValue(Target).AsString.Trim.IsEmpty;
end;

{ UINotNull }

constructor UINotNull.Create (const ARequiredStates: word; AForbiddenStates: word = vs_None);
begin
  inherited Create;
  Info.RequiredStates := ARequiredStates;
  Info.ForbiddenStates := AForbiddenStates;
end;

{ TUINotNullInfo }

function TUINotNullInfo.Active(const States: word): boolean;
begin
  result := (States and ForbiddenStates = 0) and (States and RequiredStates = RequiredStates);
end;

{ EmailAddressAttribute }

function EmailAddressAttribute.Validate(Target: TObject; p : TRttiProperty): string;
begin
  var StringValue := p.GetValue(Target).AsString;
  if StringValue.IsEmpty then exit ('');

  if not TRegexEmailValidator.Validate (StringValue)
     then result := format (tcWrongFieldFormat, [p.Name])
     else result := '';
end;

class constructor TRegexEmailValidator.Create;
begin
//  RegEx := TRegEx.Create('^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
  RegEx := TRegEx.Create('^[\w\p{L}\p{M}-\.]+@([\w\p{L}\p{M}-]+\.)+[\w\p{L}\p{M}]{2,4}$');
end;

class function TRegexEmailValidator.Validate(const email: string): boolean;
begin
  Result := RegEx.IsMatch(email);
end;

constructor MinDateAttribute.Create (AMinimum: TDateTime = 0);
begin
  inherited Create;
  FMinimum := AMinimum;
end;

function MinDateAttribute.Validate(Target: TObject; p : TRttiProperty) : string;
begin
  if p.GetValue(Target).AsExtended < FMinimum
     then result := format (tcDateCantBeLowerThan, [FMinimum])
     else result := '';
end;

end.

