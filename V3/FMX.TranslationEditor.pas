﻿unit FMX.TranslationEditor;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Menus, System.Rtti, FMX.Grid.Style, FMX.Grid,
  FMX.Controls.Presentation, FMX.ScrollBox, Translator2Classes, FMX.StdCtrls, StrUtils, FMX.Edit, FMX.ListBox,
  FMX.DialogService.Sync, FMX.Layouts, FMX.Memo, FMX.TabControl, FMX.Common.Utils;

type
//  TStringGrid = class (FMX.Grid.TStringGrid)
//    property OnMouseDown;
//  end;

  TfrmTranslationEditor = class(TForm)
    MenuBar1: TMenuBar;
    MenuItem5: TMenuItem;
    mniOpen: TMenuItem;
    mniSave: TMenuItem;
    mniClose: TMenuItem;
    opd: TOpenDialog;
    svd: TSaveDialog;
    sgrObjects: TStringGrid;
    stcParent: TStringColumn;
    stcClass: TStringColumn;
    mniAddAllForms: TMenuItem;
    mniAddDBData: TMenuItem;
    grbFilter: TGroupBox;
    edtFilter: TEdit;
    timFilter: TTimer;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    cmbFilter: TComboBox;
    Label2: TLabel;
    tac: TTabControl;
    taiConfiguration: TTabItem;
    taiTranslations: TTabItem;
    grbLanguages: TGroupBox;
    GroupBox1: TGroupBox;
    memExcluded: TMemo;
    lsbLanguages: TListBox;
    btnAddLanguage: TButton;
    btnRemoveLanguage: TButton;
    btnApplyTranslation: TButton;
    btnDefaultExcluded: TButton;
    btnApplyExcluded: TButton;
    sgrStrings: TStringGrid;
    StringColumn1: TStringColumn;
    Panel1: TPanel;
    Label3: TLabel;
    Panel2: TPanel;
    Label4: TLabel;
    pmObjects: TPopupMenu;
    mniExcludeParent: TMenuItem;
    mniExcludeObject: TMenuItem;
    mniExcludePath: TMenuItem;
    mniExcludeProperty: TMenuItem;
    stcInstance: TStringColumn;
    stcProperty: TStringColumn;
    mniExcludePropertyOfClass: TMenuItem;
    MenuItem1: TMenuItem;
    mniClear: TMenuItem;
    mniTest: TMenuItem;
    Splitter1: TSplitter;
    mniExclude: TMenuItem;
    mniUseThisTranslation: TMenuItem;
    mniUseForAll: TMenuItem;
    SpeedButton2: TSpeedButton;
    procedure mniOpenClick(Sender: TObject);
    procedure mniSaveClick(Sender: TObject);
    procedure mniAddAllFormsClick(Sender: TObject);
    procedure sgrObjectsEditingDone(Sender: TObject; const ACol, ARow: Integer);
    procedure edtFilterTyping(Sender: TObject);
    procedure timFilterTimer(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnAddLanguageClick(Sender: TObject);
    procedure btnRemoveLanguageClick(Sender: TObject);
    procedure btnDefaultExcludedClick(Sender: TObject);
    procedure btnApplyTranslationClick(Sender: TObject);
    procedure btnApplyExcludedClick(Sender: TObject);
    procedure lsbLanguagesChange(Sender: TObject);
    procedure cmbFilterChange(Sender: TObject);
    procedure sgrStringsEditingDone(Sender: TObject; const ACol, ARow: Integer);
    procedure mniExcludeParentClick(Sender: TObject);
    procedure mniExcludeObjectClick(Sender: TObject);
    procedure mniExcludePathClick(Sender: TObject);
    procedure mniExcludePropertyClick(Sender: TObject);
    procedure mniExcludePropertyOfClassClick(Sender: TObject);
    procedure mniClearClick(Sender: TObject);
    procedure mniTestClick(Sender: TObject);
    procedure lsbLanguagesDblClick(Sender: TObject);
    procedure mniUseForAllClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FileName: TFileName;
    procedure StartTimer;
    function SelectedParent: string;
    function SelectedInstance: string;
    function SelectedProperty: string;
    function SelectedLanguage: string;
    function SelectedTranslation: string;
    function SelectedClass: string;
    function SelectedPath: string;
//    function IsRowFilteredGeneral (const sgr: TStringGrid; const Row: integer): boolean;
    procedure GridMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure GridMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure UpdatePopupMenu;
    procedure ApplyTranslation;
  public
    procedure AfterConstruction; override;
    procedure LoadFile (const FileName: TFileName);
    procedure SaveFile (const AFileName: TFileName = '');
    procedure UpdateUI;
    procedure UpdateLanguagesUI;
  end;

var
  frmTranslationEditor: TfrmTranslationEditor;

implementation

{$R *.fmx}

procedure TfrmTranslationEditor.AfterConstruction;
begin
  inherited;
  UpdateUI;
  UpdateLanguagesUI;
  sgrObjects.OnMouseDown := GridMouseDown;
  sgrObjects.OnMouseUp := GridMouseUp;
end;

procedure TfrmTranslationEditor.btnAddLanguageClick(Sender: TObject);
var
  langs: array [0..0] of string;
begin
  if TDialogServiceSync.InputQuery(Application.Title, ['Enter new language code'], langs)
//  if InputQuery (Application.Title, ['Enter new language code'], langs)
     then begin
            Translator.AddLanguage (langs[0]);
            UpdateUI;
            UpdateLanguagesUI;
          end;
end;

procedure TfrmTranslationEditor.btnApplyTranslationClick(Sender: TObject);
begin
  ApplyTranslation;
end;

procedure TfrmTranslationEditor.ApplyTranslation;
begin
  if lsbLanguages.Selected <> nil
     then Translator.ActiveLanguage := lsbLanguages.Selected.Text;
end;

procedure TfrmTranslationEditor.btnDefaultExcludedClick(Sender: TObject);
begin
  Translator.SetDefaultExcludedProperties;
  UpdateUI;
end;

procedure TfrmTranslationEditor.btnRemoveLanguageClick(Sender: TObject);
begin
  if lsbLanguages.Selected <> nil then
    if TDialogServiceSync.MessageDialog
                ('Are you sure you want to delete selected language: '
                + lsbLanguages.Selected.Text,
                TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], TMsgDlgBtn.mbNo, 0) = mrYes
        then
          begin
            Translator.RemoveLanguage(lsbLanguages.Selected.Text);
            UpdateUI;
            UpdateLanguagesUI;
          end;
end;

procedure TfrmTranslationEditor.cmbFilterChange(Sender: TObject);
begin
  UpdateUI;
end;

procedure TfrmTranslationEditor.btnApplyExcludedClick(Sender: TObject);
begin
  Translator.Excluded := memExcluded.Text;
  UpdateUI;
end;

procedure TfrmTranslationEditor.edtFilterTyping(Sender: TObject);
begin
  StartTimer;
end;

procedure TfrmTranslationEditor.FormShow(Sender: TObject);
begin
  if tac.ActiveTab = taiTranslations then edtFilter.SetFocus;
end;

procedure TfrmTranslationEditor.GridMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var
  Row, Col: integer;
begin
  var found := sgrObjects.CellByPoint(X, Y, Col, Row);

  if (Button = TMouseButton.mbRight) and found
    then sgrObjects.SelectCell(Col, Row);

  if (Button = TMouseButton.mbLeft) and not found
    then sgrObjects.Row := -1;
end;

procedure TfrmTranslationEditor.GridMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var
  Row, Col: integer;
begin
  if Button <> TMouseButton.mbRight then exit;

  if sgrObjects.CellByPoint(X, Y, Col, Row) then
    begin
      UpdatePopupMenu;
      var p := sgrObjects.LocalToAbsolute(PointF(X, Y));
      p.x := p.x + Left;
      p.y := p.y + Top;
      pmObjects.Popup(p.x, p.y);
    end;
end;

procedure TfrmTranslationEditor.StartTimer;
begin
  timFilter.Interval := 250;
  timFilter.Enabled := true;
end;

procedure TfrmTranslationEditor.LoadFile(const FileName: TFileName);
begin
  Translator.LoadFromFile(FileName);
  UpdateUI;
  UpdateLanguagesUI;
end;

procedure TfrmTranslationEditor.lsbLanguagesChange(Sender: TObject);
begin
  btnApplyTranslation.Enabled := lsbLanguages.Selected <> nil;
end;

procedure TfrmTranslationEditor.lsbLanguagesDblClick(Sender: TObject);
begin
  if btnApplyTranslation.Enabled
     then ApplyTranslation;
end;

procedure TfrmTranslationEditor.mniClearClick(Sender: TObject);
begin
  Translator.Clear;
  UpdateUI;
end;

procedure TfrmTranslationEditor.mniOpenClick(Sender: TObject);
begin
  if opd.Execute then LoadFile (opd.FileName);
end;

procedure TfrmTranslationEditor.mniExcludeObjectClick(Sender: TObject);
begin
  Translator.ExcludeInstance (TMenuItem(Sender).TagString);
  UpdateUI;
end;

procedure TfrmTranslationEditor.mniExcludeParentClick(Sender: TObject);
begin
  Translator.ExcludeParent (TMenuItem(Sender).TagString);
  UpdateUI;
end;

procedure TfrmTranslationEditor.mniExcludePathClick(Sender: TObject);
begin
  Translator.Exclude (TMenuItem(Sender).TagString);
  UpdateUI;
end;

procedure TfrmTranslationEditor.mniExcludePropertyClick(Sender: TObject);
begin
  Translator.ExcludeProperty (TMenuItem(Sender).TagString);
  UpdateUI;
end;

procedure TfrmTranslationEditor.mniExcludePropertyOfClassClick(Sender: TObject);
begin
  Translator.Exclude (TMenuItem(Sender).TagString);
  UpdateUI;
end;

procedure TfrmTranslationEditor.mniSaveClick(Sender: TObject);
begin
//  if svd.Execute then SaveFile (svd.FileName);
  SaveFile;
end;

procedure TfrmTranslationEditor.mniTestClick(Sender: TObject);
begin
  Translator.Test;
  UpdateUI;
end;

procedure TfrmTranslationEditor.mniUseForAllClick(Sender: TObject);
begin
  Translator.AddForAllWithThisName (SelectedInstance, SelectedLanguage, SelectedTranslation, true);
  UpdateUI;
end;

procedure TfrmTranslationEditor.mniAddAllFormsClick(Sender: TObject);
begin
  FMXTranslator.AddAllForms;
  UpdateUI;
end;

procedure TfrmTranslationEditor.SaveFile(const AFileName: TFileName = '');
begin
  if AFileName <> ''
     then FileName := AFileName;
  Translator.SaveToFile(FileName);
end;

procedure TfrmTranslationEditor.sgrObjectsEditingDone(Sender: TObject; const ACol, ARow: Integer);
var
  count: integer;
begin
  Translator.Add(SelectedParent, SelectedInstance, SelectedClass, SelectedProperty, SelectedTranslation, SelectedLanguage);

  count := Translator.CountAllWithThisNameWithoutTranslation (SelectedInstance, SelectedLanguage);
  if (count > 0) and (TDialogServiceSync.MessageDialog
                (format ('Found %d elements with same name without translation, set this value for all of them?', [count]),
                TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], TMsgDlgBtn.mbNo, 0) = mrYes)
                then
                begin
                  Translator.AddForAllWithThisName (SelectedInstance, SelectedLanguage, SelectedTranslation, true);
                  UpdateUI;
                end;
end;

function TfrmTranslationEditor.SelectedParent: string;
begin
  result := sgrObjects.Cells[stcParent.Index, sgrObjects.Row];
end;

function TfrmTranslationEditor.SelectedClass: string;
begin
  result := sgrObjects.Cells[stcClass.Index, sgrObjects.Row];
end;

function TfrmTranslationEditor.SelectedInstance: string;
begin
  result := sgrObjects.Cells[stcInstance.Index, sgrObjects.Row];
end;

function TfrmTranslationEditor.SelectedProperty: string;
begin
  result := sgrObjects.Cells[stcProperty.Index, sgrObjects.Row];
  if result = Default_Property_Name then result := '';
end;

function TfrmTranslationEditor.SelectedTranslation: string;
begin
  result := sgrObjects.Cells[sgrObjects.Col, sgrObjects.Row];
end;

function TfrmTranslationEditor.SelectedLanguage: string;
begin
  result := sgrObjects.Columns[sgrObjects.Col].Header;
end;

function TfrmTranslationEditor.SelectedPath: string;
begin
  result := TTranslationElement.ComputeCustomPath ([SelectedParent, SelectedInstance]);
end;

procedure TfrmTranslationEditor.UpdatePopupMenu;
begin
  mniExcludeParent.Text := 'Exclude parent: ' + SelectedParent;
  mniExcludeParent.TagString := SelectedParent;
  mniExcludeParent.Enabled := (SelectedParent <> '') and not Translator.ParentIsExcluded (SelectedParent);

  mniExcludeObject.Text := 'Exclude all elements named: ' + SelectedInstance;
  mniExcludeObject.TagString := SelectedInstance;
  mniExcludeObject.Enabled := (SelectedInstance <> '') and not Translator.InstanceIsExcluded (SelectedInstance);

  mniExcludePath.Text := 'Exclude this element: ' + SelectedPath;
  mniExcludePath.TagString := SelectedPath;
  mniExcludePath.Enabled := (SelectedPath <> '') and not Translator.SubPathIsExcluded (SelectedPath);

  mniExcludeProperty.Text := 'Exclude property: ' + SelectedProperty;
  mniExcludeProperty.TagString := SelectedProperty;
  mniExcludeProperty.Enabled := (SelectedProperty <> '') and not Translator.PropertyIsExcluded (SelectedProperty);

  mniExcludePropertyOfClass.TagString := TTranslationElement.ComputeCustomPath ([SelectedClass, SelectedProperty]);
  mniExcludePropertyOfClass.Text := 'Exclude property of this class: ' + mniExcludePropertyOfClass.TagString;
  mniExcludePropertyOfClass.Enabled := (SelectedClass <> '') and (SelectedProperty <> '')
                            and not Translator.PropertyOfClassOrInstanceIsExcluded(SelectedProperty, SelectedClass);

  mniUseForAll.Text := 'Apply to all objects named ' + SelectedInstance;
end;

procedure TfrmTranslationEditor.sgrStringsEditingDone(Sender: TObject; const ACol, ARow: Integer);
begin
  Translator.AddStringTranslation (sgrStrings.Cells[0, ARow], sgrStrings.Cells[1, ARow],
                                   sgrStrings.Columns[ACol].Header, sgrStrings.Cells[ACol, ARow]);
end;

procedure TfrmTranslationEditor.SpeedButton1Click(Sender: TObject);
begin
  edtFilter.Text := '';
  StartTimer;
end;

procedure TfrmTranslationEditor.SpeedButton2Click(Sender: TObject);
begin
  cmbFilter.ItemIndex := 0;
  UpdateUI;
end;

procedure TfrmTranslationEditor.timFilterTimer(Sender: TObject);
begin
  UpdateUI;
  timFilter.Enabled := false;
end;

procedure TfrmTranslationEditor.UpdateLanguagesUI;
var
  iLang : integer;
begin
  cmbFilter.BeginUpdate;
  for iLang := cmbFilter.Items.Count - 1 downto 2 do
      cmbFilter.Items.Delete(iLang);

  for iLang := 0 to lsbLanguages.Count - 1 do
      cmbFilter.Items.Add('Missing: ' + lsbLanguages.Items[iLang]);

  cmbFilter.EndUpdate;
end;

//function TfrmTranslationEditor.IsRowFilteredGeneral (const sgr: TStringGrid; const Row: integer): boolean;
//begin
//  if edtFilter.Text.IsEmpty
//   then result := false
//   else
//      begin
//        var FilterText := edtFilter.Text.ToLower;
//        for var iCol := 0 to sgr.ColumnCount - 1 do
//          if lowercase(sgr.Cells[iCol, row]).Contains (FilterText)
//             then exit (false);
//
//        exit (true);
//      end;
//end;

procedure TfrmTranslationEditor.UpdateUI;
var
  //iCol,
  iLang, row : integer;
  col: TStringColumn;
  FilterText{, path}: string;
//  valid, found: boolean;
//  si: TStringsInfo;
//  FixedElementColumns: integer;

function RowFiltered2(sgr: TStringGrid): boolean;
var
  valid: boolean;
begin
  if cmbFilter.ItemIndex > 1 then
    begin
       valid := true;
       for var iLang := 0 to Translator.LanguageCount - 1 do
         begin
           if (cmbFilter.ItemIndex = 2 + iLang)
              then if not sgr.Cells[iLang + 1, row].IsEmpty then
                begin
                  valid := false;
                  break;
                end;
         end;
    end
  else if cmbFilter.ItemIndex = 1 then
    begin
       valid := false;
       for var iLang := 0 to Translator.LanguageCount - 1 do
         begin
           if sgr.Cells[iLang + 1, row].IsEmpty then
                begin
                  valid := true;
                  break;
                end;
         end;
    end
  else valid := true;

  result := not valid;
end;

procedure AddToGrid (const Element: TTranslationElement);
var
  iLang : integer;
begin
  sgrObjects.Cells[0, row] := Element.Parent;
  sgrObjects.Cells[1, row] := Element.ClassName;
  sgrObjects.Cells[2, row] := Element.InstanceName;
  if Element.PropertyName.IsEmpty
     then sgrObjects.Cells[3, row] := Default_Property_Name
     else sgrObjects.Cells[3, row] := Element.PropertyName;

  for iLang := 0 to Translator.LanguageCount - 1 do
      sgrObjects.Cells[iLang + 4, row] := Element.Translations[iLang];

//  if not FilterText.IsEmpty then
//     begin
//       found := path.ToLower.Contains (FilterText);
//       if not found then
//         for iLang := 0 to Translator.Languages.Count - 1 do
//           begin
//             found := sgrObjects.Cells[iLang + 1, row].ToLower.Contains (edtFilter.Text);
//             if found then break;
//           end;
//       if not found then
//          exit;
//     end;
//
//  valid := true;
//

  inc (row);
end;

//var
//  arrSplit: TArray<string>;

//procedure AddString;
//begin
//  arrSplit := path.split ([Translator_Path_Delimiter]);
//
//
//  if Translator.dicStringArrays.TryGetValue(arrSplit[0], si) then
//    for var iLang := 0 to Translator.LanguageCount - 1 do
//      sgrStrings.Cells[iLang + 1, row] := Translator.FindStringTranslation(arrSplit[0], arrSplit[1], Translator.dicLanguages.Reverse[iLang]);
//
////  if not RowFiltered1 (sgrStrings) and not RowFiltered2 (sgrStrings) then inc(row);
//end;

begin
  FilterText := edtFilter.Text.ToLower;
  sgrObjects.BeginUpdate;
  sgrStrings.BeginUpdate;

  while sgrObjects.ColumnCount > 4 do
    sgrObjects.Columns[sgrObjects.ColumnCount - 1].Free;
  while sgrStrings.ColumnCount > 1 do
    sgrStrings.Columns[sgrStrings.ColumnCount - 1].Free;

  lsbLanguages.Items.Text := Translator.dicLanguages.ToString;
  memExcluded.Lines.Text := Translator.Excluded;

  for iLang := 0 to Translator.LanguageCount - 1 do
    begin
      col := TStringColumn.Create(sgrObjects);
      col.Header := Translator.dicLanguages.Reverse[iLang];
      col.Index := sgrObjects.ColumnCount - 2;
      col.ReadOnly := iLang = 0;
      sgrObjects.AddObject(col);

      col := TStringColumn.Create(sgrStrings);
      col.Header := Translator.dicLanguages.Reverse[iLang];
      col.Index := sgrStrings.ColumnCount - 2;
      col.Width := 3 * col.Width;
      col.ReadOnly := iLang = 0;
      sgrStrings.AddObject(col);
    end;

  row := 0;
  sgrObjects.RowCount := Translator.lstElements.Count;

  var GeneralFilter := edtFilter.Text.ToLower;

  for var Element in Translator.lstElements do
    if (GeneralFilter.IsEmpty or Element.GeneralFilterMatch (GeneralFilter))
       and
       ((cmbFilter.ItemIndex < 1) or Element.FilterMatch(cmbFilter.ItemIndex))
       then AddToGrid (Element);

  sgrObjects.RowCount := row;
  sgrObjects.AutoColumnWidths;
  sgrObjects.EndUpdate;

  row := 0;

  var pair: TStringAndStringsInfoPair;
//  var isd: TIntStrDictionary;
  var DefaultString, TranslatedString: string;
  var iDefaultString: integer;

  for var iArray := 0 to Translator.dicStringArrays.Count - 1 do
    begin
      pair := Translator.dicStringArrays.SortedByKey[iArray];

      for var iString := Low (pair.Value.DefaultValues) to High (pair.Value.DefaultValues) do
          begin
            sgrStrings.Cells[0, row] := pair.Key;
            DefaultString := pair.Value.DefaultValues[iString];
            iDefaultString := pair.Value.dicDefaultStringToIndex[DefaultString];
            sgrStrings.Cells[1, row] := DefaultString;
            var Translation: string;
            var isd: TIntStrDictionary;
            for iLang := 1 to Translator.LanguageCount - 1 do
              begin
                if pair.Value.dicTranslations.TryGetValue (Translator.dicLanguages.Reverse[iLang], isd)
                  then if isd.TryGetValue (iDefaultString, Translation)
                    then sgrStrings.Cells[1 + iLang, row] := Translation
                    else sgrStrings.Cells[1 + iLang, row] := '';
              end;
            inc (row);
          end;
    end;

  sgrStrings.RowCount := row;
  sgrStrings.EndUpdate;
  sgrStrings.AutoColumnWidths;
end;

end.
