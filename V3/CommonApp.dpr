program CommonApp;

uses
  System.StartUpCopy,
  FMX.Forms,
  CommonAppMain in 'CommonAppMain.pas' {Form1},
  FMX.TranslationEditor in 'FMX.TranslationEditor.pas',
  FMX.GridModel in 'UI\FMX.GridModel.pas',
  FMX.Models in 'UI\FMX.Models.pas',
  FMX.GridLookupEdit in 'UI\FMX.GridLookupEdit.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
