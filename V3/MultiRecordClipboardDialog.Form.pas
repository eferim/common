unit MultiRecordClipboardDialog.Form;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts,
  FMX.ListBox, Generics.Collections, Mormot, MormotMods, ObservableController, FMX.GridModel, Client.MormotMods;

type

  TfrmMultiRecordClipboardDialog = class(TForm)
    rbCopySelectedRecord: TRadioButton;
    rbCopyAllRecords: TRadioButton;
    lsbFields: TListBox;
    labColumns: TLabel;
    gplPostCancel: TGridPanelLayout;
    btnOK: TButton;
    btnCancel: TButton;
    GridPanelLayout1: TGridPanelLayout;
    btnAll: TButton;
    btnNone: TButton;
    chbColumnHeaders: TCheckBox;
    cbxFormat: TComboBox;
    labFormat: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAllClick(Sender: TObject);
    procedure btnNoneClick(Sender: TObject);
  private
    procedure Setup (AModel: TMultipleSqlRecordsModel);
  protected
    slFields, slSelectedFields: TStringList;
    function ShowFor (AModel: TMultipleSqlRecordsModel): boolean;
  public
    class var dicRecordClassToInstance: TDictionary<TSqlRecordClass, TfrmMultiRecordClipboardDialog>;
//    class function ShowInstanceFor (AController: TObservableController;
//                                    ASqlRecordClass: TSqlRecordClass): TfrmMultiRecordClipboardDialog;
    class function ShowInstanceFor (AModel: TMultipleSqlRecordsModel): TfrmMultiRecordClipboardDialog;
    class constructor Create;
    class Destructor Destroy;
  end;

implementation

{$R *.fmx}

{ TfrmMultiRecordClipboardDialog }

procedure TfrmMultiRecordClipboardDialog.btnAllClick(Sender: TObject);
begin
  for var i := 0 to lsbFields.Count - 1 do lsbFields.ListItems[i].IsChecked := true;
end;

procedure TfrmMultiRecordClipboardDialog.btnNoneClick(Sender: TObject);
begin
  for var i := 0 to lsbFields.Count - 1 do lsbFields.ListItems[i].IsChecked := false;
end;

class constructor TfrmMultiRecordClipboardDialog.Create;
begin
  inherited;
  dicRecordClassToInstance := TDictionary<TSqlRecordClass, TfrmMultiRecordClipboardDialog>.Create;
end;

class destructor TfrmMultiRecordClipboardDialog.Destroy;
begin
  dicRecordClassToInstance.Free;
  inherited;
end;

procedure TfrmMultiRecordClipboardDialog.FormCreate(Sender: TObject);
begin
  slFields := TStringList.Create;
  slSelectedFields := TStringList.Create;
end;

procedure TfrmMultiRecordClipboardDialog.FormDestroy(Sender: TObject);
begin
  slFields.Free;
  slSelectedFields.Free;
end;

procedure TfrmMultiRecordClipboardDialog.Setup (AModel: TMultipleSqlRecordsModel);
var
  FieldInfo: TFieldInfo;
  DisplayName: string;
begin
  for FieldInfo in AModel.SqlRecordClass.FieldInfoList.Displayable do
    begin
      DisplayName := AModel.Controller.Model.GetFieldDisplayName (AModel.SqlRecordClass, FieldInfo.DisplayName);
      lsbFields.Items.Add(DisplayName);
      slFields.Add (FieldInfo.Name);
      lsbFields.ListItems[lsbFields.Count-1].IsChecked := true;
    end;

  {$IFNDEF DEBUG}
  labFormat.Visible := false;
  cbxFormat.Visible := false;
  {$ENDIF}
end;

function TfrmMultiRecordClipboardDialog.ShowFor(AModel: TMultipleSqlRecordsModel): boolean;
var
  i: integer;
begin
  rbCopySelectedRecord.Enabled := AModel.SelectedRecord <> nil;
  if AModel.SelectedRecord = nil
     then rbCopyAllRecords.IsChecked := true;

  result := ShowModal = mrOK;
  if result then
    begin
      slSelectedFields.Clear;
      for i := 0 to slFields.Count - 1 do
        if lsbFields.ListItems[i].IsChecked
          then slSelectedFields.Add(slFields[i]);

      if rbCopySelectedRecord.IsChecked
       then AModel.SelectedRecord.CopyToClipboard (AModel.Controller.Model, chbColumnHeaders.IsChecked, slSelectedFields)
       else case cbxFormat.ItemIndex of
            0: AModel.CopyToClipboardAsText (true, chbColumnHeaders.IsChecked, slSelectedFields);
            2: AModel.CopyToClipboardAsJSON (true, nil{slSelectedFields});
       end;
    end;
end;

class function TfrmMultiRecordClipboardDialog.ShowInstanceFor (AModel: TMultipleSqlRecordsModel): TfrmMultiRecordClipboardDialog;
begin
  if not dicRecordClassToInstance.TryGetValue (AModel.SqlRecordClass, result) then
    begin
      Application.CreateForm(TfrmMultiRecordClipboardDialog, result);
      result.Setup (AModel);
      dicRecordClassToInstance.Add (AModel.SqlRecordClass, result);
    end;

  result.ShowFor (AModel);
end;


end.
