unit CSVToArray;

interface

uses
  Classes, SysUtils;

type
 TCSVData = class
  private
    FHeaders: TArray<string>;
    FData: TArray<TArray<string>>;
    ParsedLinesCount: integer;
    TitleRow: integer;
    FTitle: string;
    procedure ParseCSVLine(const Line: string; var LineValues: TArray<string>);
    function GetCount: integer;
  public
    constructor Create(const FileName: string; ATitleRow: integer = -1);
    procedure   Load(const FileName: string; ATitleRow: integer = -1);

    property Headers: TArray<string> read FHeaders;
    property Data: TArray<TArray<string>> read FData;
    property Count: integer read GetCount;
    property Title: string read FTitle;
  end;

implementation

constructor TCSVData.Create(const FileName: string; ATitleRow: integer = -1);
begin
  inherited Create;
  TitleRow := ATitleRow;
  Load (FileName, TitleRow);
end;

function TCSVData.GetCount: integer;
begin
  result := length (FData);
end;

procedure TCSVData.ParseCSVLine(const Line: string; var LineValues: TArray<string>);
var
  InsideQuotes: Boolean;
  CurrentValue: string;
  CharIndex, HeaderIndex, DataIndex: Integer;

  procedure Add;
  begin
    SetLength(LineValues, Length(LineValues) + 1);
    LineValues[High(LineValues)] := CurrentValue;
  end;

begin
  inc (ParsedLinesCount);

  InsideQuotes := False;
  CurrentValue := '';
  SetLength(LineValues, 0);

  for CharIndex := 1 to Length(Line) do
  begin
    if Line[CharIndex] = '"' then
    begin
      InsideQuotes := not InsideQuotes;
    end
    else if Line[CharIndex] = ',' then
    begin
      if InsideQuotes then
      begin
        CurrentValue := CurrentValue + Line[CharIndex];
      end
      else
      begin
        Add;
        CurrentValue := '';
      end;
    end
    else
    begin
      CurrentValue := CurrentValue + Line[CharIndex];
    end;
  end;

  // Add the last value
  Add;

  if ParsedLinesCount - 1 = TitleRow then
    begin
      for var iCol := low (LineValues) to high (Linevalues) do
        if LineValues[iCol] <> '' then
          begin
            FTitle := LineValues[iCol];
            exit;
          end;
    end;

  if length (FHeaders) = 0
      then FHeaders := LineValues
      else begin
             var LineIndex := length(FData);
             SetLength (FData, length(FData) + 1);
             SetLength (FData[LineIndex], length(FHeaders));
             if Length(LineValues) <= Length(FHeaders)
               then
                 for HeaderIndex := Low(LineValues) to High(LineValues) do
                   begin
                     DataIndex := HeaderIndex; // Adjust index if necessary
                     FData[LineIndex, DataIndex] := StringReplace(LineValues[HeaderIndex], #160, ' ', [rfReplaceAll]); // convert stupid NBSP
                   end
               else
                 raise Exception.CreateFmt('Error: Number of values in line %d does not match number of headers.', [LineIndex]);
           end;
end;

procedure TCSVData.Load(const FileName: string; ATitleRow: integer = -1);
var
  FileStream: TFileStream;
  Reader: TStreamReader;
  Buffer: string;
  InQuotes: Boolean;
  Ch: Char;
  CharBuffer: TCharArray;
  LineValues: TArray<string>;
begin
  TitleRow := ATitleRow;
  ParsedLinesCount := 0;
  FileStream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  SetLength (CharBuffer, 1);
  SetLength(FHeaders, 0);
  try
    Reader := TStreamReader.Create(FileStream, TEncoding.UTF8);
    try
      Buffer := '';
      InQuotes := False;

      while not Reader.EndOfStream do
      begin
        Reader.Read(CharBuffer, 0, 1); // Read one character at a time
        Ch := CharBuffer[0];

        // Handle quotes and toggling in/out of quoted sections
        if Ch = '"' then
        begin
          InQuotes := not InQuotes;
          Buffer := Buffer + Ch;
          Continue;
        end;

        // Handle line breaks outside of quotes (indicating end of a row)
        if (Ch = #10) or (Ch = #13) then
        begin
          if not InQuotes then
          begin
            if (Buffer <> '') and (Ch = #10) then // Skip adding an empty line
            begin
              ParseCSVLine (Buffer, LineValues);
              Buffer := '';
            end;
            Continue;
          end;
        end;

        // Append character to buffer
        Buffer := Buffer + Ch;
      end;

      // Handle any remaining data in buffer after EOFaa
      if Buffer <> '' then
        ParseCSVLine (Buffer, LineValues);

    finally
      Reader.Free;
    end;
  finally
    FileStream.Free;
  end;
end;

(*
procedure TCSVData.Load(const FileName: string);
var
  CSVFile: TStringList;
  LineValues: TArray<string>;
  LineIndex, HeaderIndex, DataIndex: Integer;
begin
  CSVFile := TStringList.Create;
  try
    CSVFile.LoadFromFile(FileName, TEncoding.UTF8);

    if CSVFile.Count > 0 then
    begin
      SetLength(FHeaders, 0);
      ParseCSVLine(CSVFile[0], FHeaders);
      SetLength(FData, CSVFile.Count - 1, Length(FHeaders));

      for LineIndex := 1 to CSVFile.Count - 1 do
      begin
        ParseCSVLine(CSVFile[LineIndex], LineValues);

        if Length(LineValues) = Length(FHeaders) then
        begin
          for HeaderIndex := Low(FHeaders) to High(FHeaders) do
          begin
            DataIndex := HeaderIndex; // Adjust index if necessary
            FData[LineIndex - 1, DataIndex] := StringReplace(LineValues[HeaderIndex], #160, ' ', [rfReplaceAll]); // convert stupid NBSP
          end;
        end
        else
        begin
          raise Exception.CreateFmt('Error: Number of values in line %d does not match number of headers.', [LineIndex]);
        end;
      end;
    end
    else
    begin
      raise Exception.Create('Error: CSV file is empty.');
    end;
  finally
    CSVFile.Free;
  end;
end;
*)

{
// Example usage:
var
  FileName: string;
  CSVData: TCSVData;
  Row, Col: Integer;
begin
  FileName := 'example.csv'; // Specify the path to your CSV file
  try
    CSVData := TCSVData.Create(FileName);

    // Output headers
    WriteLn('Headers:');
    for Col := Low(CSVData.Headers) to High(CSVData.Headers) do
      WriteLn(CSVData.Headers[Col]);

    // Output data
    WriteLn('Data:');
    for Row := Low(CSVData.Data) to High(CSVData.Data) do
    begin
      for Col := Low(CSVData.Data[Row]) to High(CSVData.Data[Row]) do
        Write(CSVData.Data[Row, Col] + ' ');
      WriteLn;
    end;
  except
    on E: Exception do
      WriteLn('Error: ' + E.Message);
  end;
}

end.
