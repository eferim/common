﻿unit Common.Utils;

interface

uses SysUtils, DateUtils, StrUtils, Threading, Classes, RTTI, Math, System.Types, TypInfo, Variants, IOUtils
     {$IFDEF MSWINDOWS}, ShellAPI, System.Win.Registry, Generics.Collections, SyncObjs, Winapi.Winsock,
     Winapi.Windows, Winapi.AccCtrl, Winapi.AclApi, Winapi.ShlObj
     {$ENDIF}
     ;

//var
//  chrSortAscending, chrSortDescending : char;

const
  chrSortAscending = #$25B2;
  chrSortDescending = #$25BC;
  chrWhiteRightPointingTriangle = #$25B7;
  chrBlackRightPointingTriangle = #$25B6;
  chrFilterActive = '🔍'; // Magnifying Glass (U+1F50D)

  DelphiHour = 1/24;
  DelphiMinute = DelphiHour/60;
  DelphiSecond = DelphiMinute/60;
  DelphiMilliSecond = DelphiSecond/1000;

type
  INamed = interface
//  ['{6A4FABF9-8192-4A83-8598-5360FDC6F19F}']
    function GetName: string;
    property Name: string read GetName;
  end;

  TNonARCInterfacedObject = class
  public
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

  TStringsHelper = class helper for TStrings
  public
    function Contains (const AValue: string): boolean;
    function ContainsName (const AValue: string): boolean;
  end;

  TPointerHelper = record helper for pointer
    function IsAssigned: boolean; inline;
    function IsNotAssigned: boolean; inline;
  end;

  TObjectHelper = class helper for TObject
    function IsAssigned: boolean; inline;
    function IsNotAssigned: boolean; inline;
    function HasProperty(const PropName: string; ATypeInfo: PTypeInfo) : boolean;
    procedure SetProperty(const PropName: string; PropValue: TValue);
    function GetProperty(const PropName: string) : TValue;
  end;

  TListHelper = class helper for TList
  public
    function Contains (const AObject: TObject): boolean;
  end;

  function MinutesSince (const dt: TDateTime): double;
  function SecondsSince (const AThen: TDateTime): double;
  function MillisecondsSince (const dt: TDateTime): double;
  function StripThousandsSeparator (const s: string; separator: char = #0): string;
  function GetResourceString (const Identifier: string): string;

  procedure Postpone(const AProc: TThreadProcedure; const ADelayMS: Cardinal = 0); overload;
  procedure Postpone(const AProc: TThreadMethod; const ADelayMS: Cardinal = 0); overload;
  procedure PostponeOnce(const AProc: TThreadMethod; const ADelayMS: Cardinal = 0); overload;
  function CancelPostponedCall (AProc: TThreadMethod): boolean;
  procedure CancelPostponedCalls (AObject: TObject);

  procedure TypeKey(const Key: Cardinal; const Ctrl: boolean = false);
  procedure TypeString(const Str: String);

  function ExtractQuotedString (var s: string): string;
  function IsNumerical (const s: string): boolean;
  function GetFileSize (const FileName: TFileName): Longint;

type
  TCustomAttributeClass = class of TCustomAttribute;

  function FieldGetAttribute(AField: TRttiField; AAttributeClass: TCustomAttributeClass): TCustomAttribute;

  function MethodHasAttribute(AMethod: TRttiMethod;  AttribClass: TCustomAttributeClass): Boolean;

  procedure SetPropertyOfObject(Obj: TObject; const PropertyName: string; const Value: TValue);


  // this should work as long as the caller doesnt free the rtti context, I think
  function PropertyGetAttribute(AProperty: TRttiProperty; AAttributeClass: TCustomAttributeClass): TCustomAttribute;
  function PropertyHasAttribute(AProperty: TRttiProperty; AAttributeClass: TCustomAttributeClass): boolean; overload;
  function PropertyHasAttribute(AClass: TClass; const PropertyName: string; AAttributeClass: TCustomAttributeClass): boolean; overload;

  function ClassGetAttribute(AClass: TRttiType; AAttributeClass: TCustomAttributeClass): TCustomAttribute; overload;

  // ne moze jer rtti context unistava sve atribute koje sam preko njega dohvatio...
  //function ClassGetAttribute(AClass: TClass;    AAttributeClass: TCustomAttributeClass): TCustomAttribute; overload;

  function ClassHasAttribute(AClass: TRttiType; AAttributeClass: TCustomAttributeClass): boolean; overload;
  function ClassHasAttribute(AClass: TClass;    AAttributeClass: TCustomAttributeClass): boolean; overload;

  function ComponentHasProperty(AComponent: TComponent; const AName: string; APropertyType: PTypeInfo): boolean;
  procedure ComponentSetProperty(AComponent: TComponent; const AName: string; AProperty: TComponent);
  function FindClassByName(const Name: string): TClass;

  function ConvertRomanNumber (Roman: string; RequireUppercase: boolean = true): cardinal;
  function ApplicationExeFolder: string;
  function ApplicationExeName: string;
  function ReadConsoleCharacter: char;
  function RandomString (size: integer): string;
  function CheckHasConsole: Boolean;
  function RunApplication(const AExecutableFile: string; const AParameters: string = ''; const AShowOption: Integer = SW_SHOWNORMAL): TShellExecuteInfo;

{$IFDEF MSWINDOWS}
  procedure GetBuildInfo(var V1, V2, V3, V4: word);
  function GetBuildInfoAsString: string;
  procedure ListRegisteredFonts(out list : TStringList);
{$ENDIF}

  // ensure DLL bitness is same as application bitness
  procedure AssertDllBitness(const DllPath: string);

  function ADLSWriteToRegistry(const Key, ValueName, ValueData: string): boolean;
  function ReadFromRegistry(const Key, ValueName: string): string;
  function GetPrivilegeLevel: Integer;
  function IsExecutableInPath(const ExeName: string): Boolean;
  function PosAny(const Substrings: array of string; const Source: string; var sep: string): Integer;
  function TryCurrencyStrToFloat (CurrencyString: string; var Value: double): boolean;
  function CurrencyStrToFloat (CurrencyString: string; FormatSettings: TFormatSettings): double; overload;
  function CurrencyStrToFloat (CurrencyString: string): double; overload;
  function VarToFormattedStr(const V: Variant): string;
  function strRemoveCharacters(const s: string; const Characters: TArray<Char>): string;
  function NormalizeWhitespace(const S: string): string;
  function LevenshteinDistance(const s1, s2: string): Integer;
  function SqlString (const s: string): string;

type

  TRttiPropertyHelper = class helper for TRttiProperty
  public
    function GetAttribute(AAttributeClass: TCustomAttributeClass): TCustomAttribute; overload;
    function GetAttribute <T: TCustomAttribute>: T; overload;
    function HasAttribute (T: TCustomAttributeClass): boolean;
  end;

  TRttiFieldHelper = class helper for TRttiField
    function HasAttribute (AClass: TCustomAttributeClass): boolean;
    function FindAttribute (AClass: TCustomAttributeClass): TCustomAttribute;
    function GetAttribute<T: TCustomAttribute>: T;
  end;

  TRttiClassHelper = class helper for TRttiType
  public
    function GetAttribute <T: TCustomAttribute>: T;
  end;

  TRttiContextHelper = record helper for TRttiContext
  public
    function GetAttribute <T: TCustomAttribute>(AClass: TClass): T;
  end;

  function GetExeFileName: string;
  function FindProjectFileFolder: string;
  function FindProjectFilePath: string;
  function GetIPAddress: string;
  procedure SetupReportMemoryLeaksOnShutdown;
  function FrontEllipsisString (const s: string; MaxLength: integer): string;
  function EndEllipsisString (const s: string; MaxLength: integer): string;
  procedure ElevateSelf;
  function StringCompareIgnoringCRLF(const S1, S2: string): Boolean;

var
  DebugString: string;
  HasConsole: boolean;

implementation

uses Fonts.Win;

var
  lstPostponedObjects: TList<TObject>;

function StripThousandsSeparator (const s: string; separator: char = #0): string;
begin
  if separator = #0 then separator := FormatSettings.ThousandSeparator;
  result := s.replace (separator, '');
end;

{TNonARCInterfacedObject}

function TNonARCInterfacedObject.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TNonARCInterfacedObject._AddRef: Integer;
begin
  Result := -1;
end;

function TNonARCInterfacedObject._Release: Integer;
begin
  Result := -1;
end;

function MinutesSince (const dt: TDateTime): double;
begin
  result := (now - dt) * MinsPerDay;
end;

function MillisecondsSince (const dt: TDateTime): double;
begin
  result := (now - dt) * MSecsPerDay;
end;

function SecondsSince (const AThen: TDateTime): double;
begin
  Result := Abs(DateTimeToMilliseconds(Now) - DateTimeToMilliseconds(AThen))/1000;
end;

procedure Postpone(const AProc: TThreadProcedure; const ADelayMS: Cardinal = 0); overload;
begin
  TTask.Run(
    procedure
    begin
      if ADelayMS > 0 then begin
        TThread.Sleep(ADelayMS);
      end;
      TThread.Queue(nil, AProc);
    end);
end;

procedure Postpone(const AProc: TThreadMethod; const ADelayMS: Cardinal = 0); overload;
begin
  TTask.Run(
    procedure
    begin
      if ADelayMS > 0 then begin
        TThread.Sleep(ADelayMS);
      end;
      TThread.Queue(nil, AProc);
    end);
end;

type
  TPostponeOnceThread = class (TThread)
    procedure Execute; override;
  end;

var
  csPostponeOnce: TCriticalSection;
  lstPostponeOnceMethods: TList<TThreadMethod>;
  lstPostponeOnceTimes: TList<TDateTime>;
  lstReadyPostponedMethods: TList<TThreadMethod>;
  PostponeOnceThread: TPostponeOnceThread;

procedure CallPostponed; // running in main thread
begin
  csPostponeOnce.Acquire;
  try
    while lstReadyPostponedMethods.Count > 0 do
      begin
        var AMethod := lstReadyPostponedMethods[0];
        var obj := TMethod (AMethod).Data;
        try
          if lstPostponedObjects.Contains(obj)
            then AMethod()
            else if now = 0 then halt;
        finally
          lstReadyPostponedMethods.Delete(0);
        end;
      end;
  finally
    csPostponeOnce.Release;
  end;
end;

procedure TPostponeOnceThread.Execute;
var
  count, index: integer;
begin
  while not Terminated do
    begin
      csPostponeOnce.Acquire;
      try
        count := 0;
        for index := lstPostponeOnceMethods.Count - 1 downto 0 do
          begin
            if now > lstPostponeOnceTimes[index] then
              begin
                lstReadyPostponedMethods.Add (lstPostponeOnceMethods[index]);
                lstPostponeOnceTimes.Delete(index);
                lstPostponeOnceMethods.Delete(index);
                inc (count);
              end;

{              begin
                Method := lstPostponeOnceMethods[index];
                TThread.Queue (nil, procedure begin DoPostpone (Method); end);
                //TThread.Queue (nil, Method);
                lstPostponeOnceTimes.Delete(index);
                lstPostponeOnceMethods.Delete(index);
              end;}
          end;
      finally
        csPostponeOnce.Release;
      end;
      if count > 0
         then TThread.Queue (nil, CallPostponed);
      sleep (10);
    end;
end;

procedure CancelPostponedCalls (AObject: TObject);
begin
  if lstPostponedObjects.IsAssigned
    then lstPostponedObjects.Remove(AObject);
end;

procedure SetupPostponedCalls;
begin
  lstPostponeOnceMethods := TList<TThreadMethod>.Create;
  lstReadyPostponedMethods := TList<TThreadMethod>.Create;
  lstPostponeOnceTimes := TList<TDateTime>.Create;
  csPostponeOnce := TCriticalSection.Create;
  PostponeOnceThread := TPostponeOnceThread.Create;
  lstPostponedObjects := TList<TObject>.Create;
end;

function CancelPostponedCall (AProc: TThreadMethod): boolean;
begin
  if csPostponeOnce = nil then SetupPostponedCalls;
  csPostponeOnce.Acquire;
  try
    result := lstPostponeOnceMethods.Remove(AProc) <> -1;
  finally
    csPostponeOnce.Release;
  end;
end;

procedure PostponeOnce(const AProc: TThreadMethod; const ADelayMS: Cardinal = 0); overload;
var
  index: integer;
begin
  if csPostponeOnce = nil then SetupPostponedCalls;

  var obj := TMethod (AProc).Data;
  if not lstPostponedObjects.Contains(obj)
     then lstPostponedObjects.Add(obj);

  csPostponeOnce.Acquire;
  try
    index := lstPostponeOnceMethods.IndexOf (AProc);
    if index > -1
      then
         lstPostponeOnceTimes[index] := now + ADelayMS * DelphiMilliSecond
      else
        begin
          lstPostponeOnceMethods.Add(AProc);
          lstPostponeOnceTimes.Add(now + ADelayMS * DelphiMilliSecond);
        end;
  finally
    csPostponeOnce.Release;
  end;
end;


{$IFDEF MSWINDOWS}
procedure GetBuildInfo(var V1, V2, V3, V4: word);
var
  VerInfoSize, VerValueSize, Dummy: DWORD;
  VerInfo: Pointer;
  VerValue: PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ApplicationExeName), Dummy);
  if VerInfoSize > 0
    then
      begin
          GetMem(VerInfo, VerInfoSize);
          try
            if GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo) then
            begin
              VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
              with VerValue^ do
              begin
                V1 := dwFileVersionMS shr 16;
                V2 := dwFileVersionMS and $FFFF;
                V3 := dwFileVersionLS shr 16;
                V4 := dwFileVersionLS and $FFFF;
              end;
            end;
          finally
            FreeMem(VerInfo, VerInfoSize);
          end;
      end
    else
      begin
        V1 := 0;
        V2 := 0;
        V3 := 0;
        V4 := 0;
      end;
end;

function GetBuildInfoAsString: string;
var
  V1, V2, V3, V4: word;
begin
  GetBuildInfo(V1, V2, V3, V4);
  Result := format('%d.%d.%d.%d', [V1, V2, V3, V4]);
//  IntToStr(V1) + '.' + IntToStr(V2) + '.' +
//    IntToStr(V3) + '.' + IntToStr(V4);
end;

{$ENDIF}


procedure TypeKey(const Key: Cardinal; const Ctrl: boolean = false);
const KEYEVENTF_KEYDOWN = 0;
      KEYEVENTF_UNICODE = 4;
var rec: TInput;
    rLen: Integer;
    shift: Boolean;
begin
  rLen:=SizeOf(TInput);

  shift:=(Key shr 8)=1;

  if shift then begin
    rec.Itype:=INPUT_KEYBOARD;
    rec.ki.wVk:=VK_SHIFT;
    rec.ki.dwFlags:=KEYEVENTF_KEYDOWN; // or KEYEVENTF_UNICODE;
    SendInput(1,rec,rLen);
  end;

  if Ctrl then begin
    rec.Itype:=INPUT_KEYBOARD;
    rec.ki.wVk:=VK_LCONTROL;
    rec.ki.dwFlags:=KEYEVENTF_KEYDOWN; // or KEYEVENTF_UNICODE;
    SendInput(1,rec,rLen);
  end;


  rec.Itype:=INPUT_KEYBOARD;
  rec.ki.wVk:=Key;
  rec.ki.dwFlags:=KEYEVENTF_KEYDOWN; // or KEYEVENTF_UNICODE;
  SendInput(1,rec,rLen);

  rec.Itype:=INPUT_KEYBOARD;
  rec.ki.wVk:=Key;
  rec.ki.dwFlags:=KEYEVENTF_KEYUP; // or KEYEVENTF_UNICODE;
  SendInput(1,rec,rLen);

  if Ctrl then begin
    rec.Itype:=INPUT_KEYBOARD;
    rec.ki.wVk:=VK_LCONTROL;
    rec.ki.dwFlags:=KEYEVENTF_KEYUP; // or KEYEVENTF_UNICODE;
    SendInput(1,rec,rLen);
  end;

  if shift then begin
    rec.Itype:=INPUT_KEYBOARD;
    rec.ki.wVk:=VK_SHIFT;
    rec.ki.dwFlags:=KEYEVENTF_KEYUP; // or KEYEVENTF_UNICODE;
    SendInput(1,rec,rLen);
  end;
end;

procedure TypeString(const Str: String);
var
  i, sLen: Integer;
begin
  sLen:=Length(Str);
  if sLen>0 then for i:=1 to sLen do TypeKey(vkKeyScan(Str[i]));
end;

{ TPointerHelper }

function TPointerHelper.IsAssigned: boolean;
begin
  result := Assigned (self);
end;

function TPointerHelper.IsNotAssigned: boolean;
begin
  result := not Assigned (self);
end;

{ TObjectHelper }

function TObjectHelper.IsAssigned: boolean;
begin
  result := Assigned (self);
end;

function TObjectHelper.IsNotAssigned: boolean;
begin
  result := not Assigned (self);
end;

function TObjectHelper.HasProperty(const PropName: string; ATypeInfo: PTypeInfo): boolean;
var
  ctx: TRttiContext;
begin
  Result := False;
  var p := ctx.GetType(Self.ClassType).GetProperty(PropName);
  if Assigned(p) then
  begin
    var PropTypeInfo := p.GetValue(self).TypeInfo;

    result := (PropTypeInfo = ATypeInfo)
              or ((PropTypeInfo^.Kind = tkClass) and (GetTypeData(PropTypeInfo)^.ClassType.InheritsFrom(GetTypeData(ATypeInfo)^.ClassType)))
              or ((PropTypeInfo^.Kind in [tkUString, tkLString, tkWString, tkString]) and (ATypeInfo^.Kind in [tkUString, tkLString, tkWString, tkString]));
  end;
end;

function TObjectHelper.GetProperty(const PropName: string): TValue;
var
  ctx : TRttiContext;
begin
  result := nil;
var p := ctx.GetType(Self.ClassType).GetProperty(PropName);
  if Assigned(p) then
    Result := p.GetValue(self);
end;

procedure TObjectHelper.SetProperty(const PropName: string; PropValue: TValue);
var
  ctx : TRttiContext;
begin
  if HasProperty(PropName, PropValue.TypeInfo) then
  begin
    var p := ctx.GetType(Self.ClassType).GetProperty(PropName);
    if p.IsWritable then
      p.SetValue(self, propvalue)
    else
      raise Exception.Create(format('Property "%s" is read only in class %s', [PropName, Self.ClassName]))
  end
  else
    raise Exception.Create(format('Thera are no "%s" proprty in class %s', [PropName, Self.ClassName]));
end;

function GetResourceString (const Identifier: string): string;
var
  Stream: TResourceStream;
  sl: TStringList;
begin
  result := '';
  Stream := TResourceStream.Create(HInstance, Identifier, RT_RCDATA);
  sl := TStringList.Create;
  try
    sl.LoadFromStream(Stream, TEncoding.UTF8);
    result := sl.Text;
  finally
    sl.Free;
    Stream.Free;
  end;
end;

function IsNumerical (const s: string): boolean;
var
  e: extended;
begin
  result := TryStrToFloat (s, e);
end;

function ExtractQuotedString (var s: string): string;
var
  index: integer;
begin
  index := 2;
  while true do
    begin
      if (s [index] = '"') and
         ( (index = length (s)) or (s[index+1] <> '"') ) then break; // found quote, and its either last char or next one isnt a quote
      if (s [index] = '"') then inc (index); // double quote, skip an extra index
      inc (index);
    end;
  result := copy (s, 2, index-2);
  s := copy (s, index+1);
end;

function GetFileSize (const FileName: TFileName): Longint;
var s : TSearchRec;
begin
  if FindFirst (FileName, faAnyFile, s) = 0 then
    begin
      result := s.Size;
      Sysutils.FindClose(s);
    end
  else result := -1;
end;

function MethodHasAttribute(AMethod: TRttiMethod;  AttribClass: TCustomAttributeClass): Boolean;
var
  Attributes: TArray<TCustomAttribute>;
  Attrib: TCustomAttribute;
begin
  Result := False;
  Attributes := AMethod.GetAttributes;

  for Attrib in Attributes do
    if Attrib.InheritsFrom(AttribClass) then
      Exit(True);
end;

procedure SetPropertyOfObject(Obj: TObject; const PropertyName: string; const Value: TValue);
var
  RttiContext: TRttiContext;
  RttiType: TRttiType;
  RttiProperty: TRttiProperty;
begin
  RttiType := RttiContext.GetType(Obj.ClassType);
  RttiProperty := RttiType.GetProperty(PropertyName);

  if RttiProperty.IsAssigned and RttiProperty.IsWritable then
  begin
    // Check if the property type is compatible with the value
    if Value.IsType(RttiProperty.PropertyType.Handle) then
    begin
      // Set the property value
      RttiProperty.SetValue(Obj, Value);
    end
    else
      raise Exception.CreateFmt('Value type mismatch for property "%s".', [PropertyName]);
  end
  else
    raise Exception.CreateFmt('Property "%s" not found or not writable.', [PropertyName]);
end;


function PropertyHasAttribute(AProperty: TRttiProperty; AAttributeClass: TCustomAttributeClass): boolean;
begin
  result := PropertyGetAttribute(AProperty, AAttributeClass) <> nil;
end;

function PropertyHasAttribute(AClass: TClass; const PropertyName: string; AAttributeClass: TCustomAttributeClass): boolean; overload;
var
  ctx : TRttiContext;
  t : TRttiType;
begin
  ctx := TRttiContext.Create;
  try
    t := ctx.GetType(AClass);
    var p := t.GetProperty(PropertyName);
    result := p.HasAttribute(AAttributeClass);
  finally
    ctx.Free;
  end;
end;

function PropertyGetAttribute(AProperty: TRttiProperty; AAttributeClass: TCustomAttributeClass): TCustomAttribute;
var
  Attributes: TArray<TCustomAttribute>;
  Attrib: TCustomAttribute;
begin
  Attributes := AProperty.GetAttributes;

  for Attrib in Attributes do
    if Attrib.InheritsFrom(AAttributeClass) then
      Exit(Attrib);
  result := nil;
end;

function FieldGetAttribute(AField: TRttiField; AAttributeClass: TCustomAttributeClass): TCustomAttribute;
var
  Attributes: TArray<TCustomAttribute>;
  Attrib: TCustomAttribute;
begin
  Attributes := AField.GetAttributes;

  for Attrib in Attributes do
    if Attrib.InheritsFrom(AAttributeClass) then
      Exit(Attrib);
  result := nil;
end;

function ClassGetAttribute(AClass: TRttiType; AAttributeClass: TCustomAttributeClass): TCustomAttribute;
var
  Attributes: TArray<TCustomAttribute>;
  Attrib: TCustomAttribute;
begin
  Attributes := AClass.GetAttributes;

  for Attrib in Attributes do
    if Attrib.InheritsFrom(AAttributeClass) then
      Exit(Attrib);
  result := nil;
end;

function ClassHasAttribute(AClass: TRttiType; AAttributeClass: TCustomAttributeClass): boolean;
begin
  result := ClassGetAttribute(AClass, AAttributeClass) <> nil;
end;

function ClassHasAttribute(AClass: TClass; AAttributeClass: TCustomAttributeClass): boolean;
var
  ctx : TRttiContext;
  t : TRttiType;
begin
  ctx := TRttiContext.Create;
  try
    t := ctx.GetType(AClass);
    result := ClassHasAttribute (t, AAttributeClass);
  finally
    ctx.Free;
  end;
end;

function ComponentHasProperty(AComponent: TComponent; const AName: string; APropertyType: PTypeInfo): boolean;
var
  Ctx: TRttiContext;
  Prop: TRttiProperty;
begin
  Result := False;

  Prop := Ctx.GetType(AComponent.ClassType).GetProperty(AName);
  if Assigned(Prop) then
     Result := Prop.PropertyType.Handle = APropertyType;
end;

procedure ComponentSetProperty(AComponent: TComponent; const AName: string; AProperty: TComponent);
begin
  if IsPublishedProp (AComponent, AName) then
  begin
    SetObjectProp (AComponent,  AName, AProperty);
  end;
end;

function FindClassByName(const Name: string): TClass;
var
  ctx: TRttiContext;
  typ: TRttiType;
  list: TArray<TRttiType>;
begin
  Result := nil;
  ctx := TRttiContext.Create;
  list := ctx.GetTypes;
  for typ in list do
    begin
      if typ.IsInstance and
         ( (typ.Name = Name) or typ.Name.EndsWith ('.' + Name) ) then
        begin
          Result := typ.AsInstance.MetaClassType;
          break;
        end;
    end;
  ctx.Free;
end;

function ApplicationExeFolder: string;
begin
  result := ExtractFilePath (ParamStr(0));
end;

function ApplicationExeName: string;
begin
  result := ParamStr(0);
end;

function ReadConsoleCharacter: char;
var
    Event      : TInputrecord;
    EventsRead : DWORD;
begin
    while true do
      begin
        ReadConsoleInput(GetStdhandle(STD_INPUT_HANDLE), Event, 1, EventsRead);
        if (Event.Eventtype = key_Event) and Event.Event.KeyEvent.bKeyDown
           and (Event.Event.KeyEvent.AsciiChar <> #0)
           then exit (Event.Event.KeyEvent.UnicodeChar);
      end;
end;

function RandomString (size: integer): string;
var i : integer;
begin
  result := '';
  for i := 1 to size do
      result := result + chr (RandomRange (ord('a'), ord('z')));
end;

function ConvertRomanNumber (Roman: string; RequireUppercase: boolean = true): cardinal;

const
  RomanNumbers :      array [0..6] of char     = ('I', 'V', 'X', 'L',  'C',  'D',   'M');
  RomanNumberValues : array [0..6] of cardinal = ( 1,   5,  10,  50,  100,  500,  1000);

var
  SameCount, ind, indNumber, OriginalLength: integer;
  Character, PreviousCharacter: char;
  Indexes : array of cardinal;

begin
   if not RequireUppercase
      then Roman := Uppercase (Roman);

   OriginalLength := Length (Roman);

   SetLength (Indexes, OriginalLength);

   PreviousCharacter := ' ';
   SameCount := 0;

   for ind := 1 to OriginalLength do
     begin
       indNumber := Low (RomanNumbers);
       Character := Roman[ind];
       while indNumber <= High (RomanNumbers) do
         if Character = RomanNumbers[indNumber]
           then
             begin
               Indexes [ind - 1] := indNumber;
               break;
             end
           else inc (indNumber);

       if indNumber > High (RomanNumbers) then exit (0); // character is not roman numeral

       if PreviousCharacter = Character
          then begin
                 if SameCount = 3
                   then exit (0) // already 3 consecutive same characters, 4th is not allowed
                                 // (V L D cant repeat even twice but that is checked later)
                   else inc (SameCount);
               end
          else begin
                 PreviousCharacter := Character;
                 SameCount := 1;
               end;
     end;

   for ind := 0 to OriginalLength - 2 do
     begin
       if integer(Indexes [ind + 1]) - integer(Indexes [ind]) > 2
         then exit (0); // Allow IV, IX but not IL, IC etc, the difference must be at most 2 levels

       if (Indexes [ind + 1] >= Indexes [ind]) and ( (Indexes [ind] mod 2) = 1)
         then exit (0); // V, L, D cant appear in front of larger number, and they cant repeat either
     end;

   result := 100;

   for ind := 0 to OriginalLength - 1 do
     begin
       if (ind < OriginalLength - 1) and (Indexes[ind + 1] > Indexes[ind])
          then result := result - RomanNumberValues[Indexes[ind]]
          else result := result + RomanNumberValues[Indexes[ind]];
     end;
   Result := Result - 100;
end;

function CheckHasConsole: Boolean;
var
  Stdout: THandle;
begin
  Stdout := GetStdHandle(Std_Output_Handle);
  Win32Check(Stdout <> Invalid_Handle_Value);
  Result := Stdout <> 0;
end;

function RunApplication(const AExecutableFile: string; const AParameters: string = ''; const AShowOption: Integer = SW_SHOWNORMAL): TShellExecuteInfo;
begin
  FillChar(result, SizeOf(result), 0);

  if not FileExists(AExecutableFile) then
    Exit;

  result.cbSize := SizeOf(TShellExecuteInfo);
  result.fMask := SEE_MASK_NOCLOSEPROCESS;
  // result.Wnd := Application.Handle;
  result.lpFile := PChar(AExecutableFile);
  result.lpParameters := PChar(AParameters);
  result.nShow := AShowOption;
  ShellExecuteEx(@result);
end;

function IsWow64: Boolean;
type
  TIsWow64Process = function(hProcess: THandle; var Wow64Process: BOOL): BOOL; stdcall;
var
  IsWow64Process: TIsWow64Process;
  Wow64Process: BOOL;
begin
  IsWow64Process := GetProcAddress(GetModuleHandle(kernel32), 'IsWow64Process');
  if Assigned(IsWow64Process) then
  begin
    if not IsWow64Process(GetCurrentProcess, Wow64Process) then
      RaiseLastOSError;
    Result := Wow64Process;
  end
  else
    Result := False;
end;

function Isx64(const DllPath: string): Boolean;
const
  IMAGE_FILE_MACHINE_I386     = $014c; // Intel x86
  IMAGE_FILE_MACHINE_IA64     = $0200; // Intel Itanium Processor Family (IPF)
  IMAGE_FILE_MACHINE_AMD64    = $8664; // x64 (AMD64 or EM64T)
  // You'll unlikely encounter the things below:
  IMAGE_FILE_MACHINE_R3000_BE = $160;  // MIPS big-endian
  IMAGE_FILE_MACHINE_R3000    = $162;  // MIPS little-endian, 0x160 big-endian
  IMAGE_FILE_MACHINE_R4000    = $166;  // MIPS little-endian
  IMAGE_FILE_MACHINE_R10000   = $168;  // MIPS little-endian
  IMAGE_FILE_MACHINE_ALPHA    = $184;  // Alpha_AXP }
  IMAGE_FILE_MACHINE_POWERPC  = $1F0;  // IBM PowerPC Little-Endian
var
  Header: TImageDosHeader;
  ImageNtHeaders: TImageNtHeaders;
begin
  if not FileExists(DllPath)
    then raise Exception.Create('File does not exist: ' + DllPath);

  var Strm := TFileStream.Create(DllPath, fmOpenRead or fmShareDenyWrite);
  try
    Strm.ReadBuffer(Header, SizeOf(Header));
    if (Header.e_magic <> IMAGE_DOS_SIGNATURE) or
       (Header._lfanew = 0) then
      raise Exception.Create('Invalid executable ' + DllPath);
    Strm.Position := Header._lfanew;

    Strm.ReadBuffer(ImageNtHeaders, SizeOf(ImageNtHeaders));
    if ImageNtHeaders.Signature <> IMAGE_NT_SIGNATURE then
      raise Exception.Create('Invalid executable ' + DllPath);

    Result := ImageNtHeaders.FileHeader.Machine <> IMAGE_FILE_MACHINE_I386;
  finally
    Strm.Free;
  end;
end;


procedure AssertDllBitness(const DllPath: string);
var
  DllBitness: Word;
  AppBitness: Word;
begin
  if IsWow64 then
    AppBitness := 32
  else
    {$IFDEF WIN64}
    AppBitness := 64;
    {$ELSE}
    AppBitness := 32;
    {$ENDIF}

  var Is64 := Isx64 (DllPath);
  if Is64 then DllBitness := 64 else DllBitness := 32;
  if DllBitness <> AppBitness then
    raise Exception.Create(format ('Application is %dbit but %s is %dbit', [AppBitness, DllPath, DllBitness]));
end;

function ReadFromRegistry(const Key, ValueName: string): string;
var
  Reg: TRegistry;
begin
  result := '';
  Reg := TRegistry.Create(KEY_READ);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.KeyExists(Key) then
    begin
      // Key already exists, open it to set the value
      if Reg.OpenKey(Key, False) then
      begin
        result := Reg.ReadString(ValueName);
        Reg.CloseKey;
      end;
      Exit;
    end;
  finally
    Reg.Free;
  end;
end;

function CheckTokenMembership(TokenHandle: THandle; SidToCheck: PSID; var IsMember: BOOL): BOOL; stdcall; external advapi32 name 'CheckTokenMembership';

function GetPrivilegeLevel: Integer;
const
  SECURITY_NT_AUTHORITY: TSIDIdentifierAuthority = (Value: (0, 0, 0, 0, 0, 5));
  SECURITY_BUILTIN_DOMAIN_RID = $00000020;
  DOMAIN_ALIAS_RID_ADMINS = $00000220;
  SECURITY_LOCAL_SYSTEM_RID = $00000012;
var
  Token: THandle;
  Elevation: TOKEN_ELEVATION;
  Size: DWORD;
  AdminSID, SystemSID: PSID;
  IsMember: BOOL;
begin
  Result := 0; // Default to non-elevated

  // Open the access token associated with the current process.
  if not OpenProcessToken(GetCurrentProcess, TOKEN_QUERY, Token) then
    RaiseLastOSError;

  try
    // Check if the token has elevated privileges.
    if GetTokenInformation(Token, TokenElevation, @Elevation, SizeOf(Elevation), Size) then
      if Elevation.TokenIsElevated <> 0 then
        Result := 1; // Elevated

    // Additionally check if the token has admin privileges.
    if Result = 1 then
    begin
      AllocateAndInitializeSid(SECURITY_NT_AUTHORITY, 2,
        SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, AdminSID);
      try
        if CheckTokenMembership(Token, AdminSID, IsMember) then
          if not IsMember then
            Result := 0; // Not elevated if not a member of Admins
      finally
        FreeSid(AdminSID);
      end;
    end;

    // Check if the token belongs to the LocalSystem account.
    AllocateAndInitializeSid(SECURITY_NT_AUTHORITY, 1, SECURITY_LOCAL_SYSTEM_RID, 0, 0, 0, 0, 0, 0, 0, SystemSID);
    try
      if CheckTokenMembership(Token, SystemSID, IsMember) then
        if IsMember then
          Result := 2; // System service
    finally
      FreeSid(SystemSID);
    end;
  finally
    CloseHandle(Token);
  end;
end;

// write to registry as admin or local system account (for services)
function ADLSWriteToRegistry(const Key, ValueName, ValueData: string): boolean;
var
  Reg: TRegistry;
  SA: SECURITY_ATTRIBUTES;
  SD: SECURITY_DESCRIPTOR;
  KeyHandle: HKEY;
  Disposition: DWORD;
  AdminSID, SystemSID: PSID;
  ACL: PACL;
  SIDAuth: SID_IDENTIFIER_AUTHORITY;
  AccessDescriptor: EXPLICIT_ACCESS;

const
  SECURITY_NT_AUTHORITY: TSidIdentifierAuthority = (Value: (0, 0, 0, 0, 0, 5));
  SECURITY_BUILTIN_DOMAIN_RID = $00000020;
  DOMAIN_ALIAS_RID_ADMINS = $00000220;
  SECURITY_LOCAL_SYSTEM_RID = $00000012;

begin
  Result := False;
  ACL := nil;
  AdminSID := nil;
  SystemSID := nil;

  // Check if the key already exists
  Reg := TRegistry.Create(KEY_READ);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.KeyExists(Key) then
    begin
      // Key already exists, open it to set the value
      if Reg.OpenKey(Key, False) then
      begin
        Reg.WriteString(ValueName, ValueData);
        Reg.CloseKey;
        Result := True;
      end;
      Exit;
    end;
  finally
    Reg.Free;
  end;

  // Initialize Security Attributes
  if not InitializeSecurityDescriptor(@SD, SECURITY_DESCRIPTOR_REVISION) then Exit;
  if not SetSecurityDescriptorDacl(@SD, True, nil, False) then Exit;
  SA.nLength := SizeOf(SECURITY_ATTRIBUTES);
  SA.lpSecurityDescriptor := @SD;
  SA.bInheritHandle := False;

  // Create the key with security attributes
  if RegCreateKeyEx (HKEY_LOCAL_MACHINE, PChar(Key), 0, nil, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, @SA,
                     KeyHandle, @Disposition) <> ERROR_SUCCESS then Exit;

  // Initialize SID for Admins
  SIDAuth := SECURITY_NT_AUTHORITY;
  if not AllocateAndInitializeSid(SIDAuth, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, AdminSID) then Exit;

  // Initialize SID for LocalSystem
  if not AllocateAndInitializeSid(SIDAuth, 1, SECURITY_LOCAL_SYSTEM_RID, 0, 0, 0, 0, 0, 0, 0, SystemSID) then
  begin
    FreeSid(AdminSID);
    Exit;
  end;

  // Create an ACL
  ZeroMemory(@AccessDescriptor, SizeOf(EXPLICIT_ACCESS));
  AccessDescriptor.grfAccessPermissions := GENERIC_ALL;
  AccessDescriptor.grfAccessMode := SET_ACCESS;
  AccessDescriptor.grfInheritance := SUB_CONTAINERS_AND_OBJECTS_INHERIT;
  AccessDescriptor.Trustee.TrusteeForm := TRUSTEE_IS_SID;
  AccessDescriptor.Trustee.TrusteeType := TRUSTEE_IS_GROUP;
  AccessDescriptor.Trustee.ptstrName := PChar(AdminSID);

  try
    if SetEntriesInAcl(1, @AccessDescriptor, nil, ACL) <> ERROR_SUCCESS then exit;

    // Add LocalSystem to ACL
    AccessDescriptor.Trustee.ptstrName := PChar(SystemSID);
    if SetEntriesInAcl(1, @AccessDescriptor, ACL, ACL) <> ERROR_SUCCESS then Exit;

    // Set the new DACL in the security descriptor
    if not SetSecurityDescriptorDacl(@SD, True, ACL, False) then Exit;

    // Apply the security descriptor to the key
    if RegSetKeySecurity(KeyHandle, DACL_SECURITY_INFORMATION, @SD) <> ERROR_SUCCESS then Exit;

  finally
    if AdminSID  <> nil then FreeSid(AdminSID);
    if SystemSID <> nil then FreeSid(SystemSID);
    if ACL       <> nil then LocalFree(HLOCAL(ACL));
  end;

  // Set the value in the key
  Reg := TRegistry.Create(KEY_ALL_ACCESS);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey(Key, False) then
    begin
      Reg.WriteString(ValueName, ValueData);
      Reg.CloseKey;
      Result := True;
    end;
  finally
    Reg.Free;
  end;
end;

function IsExecutableInPath(const ExeName: string): Boolean;
var
  PathEnvVar: string;
  PathList: TStringList;
  I: Integer;
begin
  Result := False;
  PathEnvVar := GetEnvironmentVariable('PATH');
  PathList := TStringList.Create;
  try
    PathList.Delimiter := ';';
    PathList.DelimitedText := PathEnvVar;
    for I := 0 to PathList.Count - 1 do
      if FileExists(IncludeTrailingPathDelimiter(PathList[I]) + ExeName)
        then exit(true);
  finally
    PathList.Free;
  end;
end;

function PosAny(const Substrings: array of string; const Source: string; var sep: string): Integer;
var
  I, PosIndex: Integer;
begin
  Result := 0;
  sep := '';
  for I := Low(Substrings) to High(Substrings) do
  begin
    PosIndex := Pos(Substrings[I], Source);
    if (PosIndex > 0) and ((Result = 0) or (PosIndex < Result)) then
    begin
      Result := PosIndex;
      sep := Substrings[I];
    end;
  end;
end;

procedure ListRegisteredFonts(out list : TStringList);
begin
  TFonts.GetSystemFonts(list);
  list.Sort;
end;

function TryCurrencyStrToFloat (CurrencyString: string; var Value: double): boolean;
begin
  CurrencyString := StripThousandsSeparator(CurrencyString);
  result := TryStrToFloat(CurrencyString, Value);
end;

function CurrencyStrToFloat (CurrencyString: string): double;
begin
  CurrencyString := StripThousandsSeparator(CurrencyString);
  result := StrToFloat(CurrencyString);
end;

function CurrencyStrToFloat (CurrencyString: string; FormatSettings: TFormatSettings): double;
begin
  CurrencyString := StripThousandsSeparator(CurrencyString, FormatSettings.ThousandSeparator);
  result := StrToFloat(CurrencyString, FormatSettings);
end;

function VarToFormattedStr(const V: Variant): string;
begin
  case VarType(V) of
    varCurrency: Result := CurrToStrF(V, ffNumber, 0); // Currency formatting
    varDate:     Result := DateTimeToStr(V);             // Date formatting
  else
    Result := VarToStr(V);                               // Default conversion
  end;
end;

function strRemoveCharacters(const s: string; const Characters: TArray<Char>): string;
var
  i, j: Integer;
  IsCharToRemove: Boolean;
begin
  SetLength(Result, Length(s));
  var ResIndex := 0;

  for i := 1 to Length(s) do
  begin
    IsCharToRemove := False;
    for j := Low(Characters) to High(Characters) do
    begin
      if s[i] = Characters[j] then
      begin
        IsCharToRemove := True;
        Break;
      end;
    end;

    if not IsCharToRemove then
    begin
      Inc(ResIndex);
      Result[ResIndex] := s[i];
    end;
  end;

  SetLength(Result, ResIndex);
end;

{ TRttiPropertyHelper }

function TRttiPropertyHelper.GetAttribute(AAttributeClass: TCustomAttributeClass): TCustomAttribute;
var
  Attributes: TArray<TCustomAttribute>;
  Attrib: TCustomAttribute;
begin
  Attributes := GetAttributes;

  for Attrib in Attributes do
    if Attrib.InheritsFrom(AAttributeClass) then
      Exit(Attrib);
  result := nil;
end;

function TRttiPropertyHelper.GetAttribute<T>: T;
begin
  result := T (GetAttribute(T));
end;

function TRttiPropertyHelper.HasAttribute(T: TCustomAttributeClass): boolean;
begin
  result := GetAttribute(T) <> nil;
end;

{ TRttiPropertyHelper }

//function TRttiPropertyHelper.FindAttribute(AClass: TCustomAttributeClass): TCustomAttribute;
//begin
//  for var Attribute in GetAttributes do
//    if Attribute is AClass then exit (Attribute);
//  result := nil;
//end;
//
//function TRttiPropertyHelper.HasAttribute(AClass: TCustomAttributeClass): boolean;
//begin
//  result := FindAttribute(AClass) <> nil;
//end;

{ TRttiFieldHelper }

function TRttiFieldHelper.FindAttribute(AClass: TCustomAttributeClass): TCustomAttribute;
begin
  for var Attribute in GetAttributes do
    if Attribute is AClass then exit (Attribute);
  result := nil;
end;

function TRttiFieldHelper.GetAttribute<T>: T;
begin
  result := T (FindAttribute(T));
end;

function TRttiFieldHelper.HasAttribute(AClass: TCustomAttributeClass): boolean;
begin
  result := FindAttribute(AClass) <> nil;
end;

function NormalizeWhitespace(const S: string): string;
var
  i, len: Integer;
  InSpace: Boolean;
begin
  Result := '';
  len := Length(S);
  InSpace := False;

  for i := 1 to len do
  begin
    // Check for whitespace characters
    if CharInSet (S[i], [#9, #10, #13, ' ']) then
    begin
      if not InSpace then
      begin
        Result := Result + ' ';
        InSpace := True;
      end;
    end
    else
    begin
      Result := Result + S[i];
      InSpace := False;
    end;
  end;

  Result := Trim(Result); // Trim any leading/trailing spaces
end;

{ TRttiClassHelper }

function TRttiClassHelper.GetAttribute<T>: T;
var
  Attributes: TArray<TCustomAttribute>;
  Attrib: TCustomAttribute;
begin
  Attributes := GetAttributes;

  for Attrib in Attributes do
    if Attrib.InheritsFrom(T) then
      Exit(T(Attrib));
  result := nil;
end;

{ TClassHelper }

{ TRttiContextHelper }

function TRttiContextHelper.GetAttribute<T>(AClass: TClass): T;
begin
  var TableType := GetType(AClass);
  result := T (ClassGetAttribute(TableType, T));
end;

function SqlString (const s: string): string;
begin
  result := '''' + s.Replace('''', '''''') + '''';
end;

function LevenshteinDistance(const s1, s2: string): Integer;
var
  lenS1, lenS2, i, j: Integer;
  cost, above, left, diag: Integer;
  d: TArray<TArray<Integer>>;
begin
  lenS1 := Length(s1);
  lenS2 := Length(s2);

  SetLength(d, lenS1 + 1, lenS2 + 1);

  for i := 0 to lenS1 do
    d[i, 0] := i;
  for j := 0 to lenS2 do
    d[0, j] := j;

  for j := 1 to lenS2 do
  begin
    for i := 1 to lenS1 do
    begin
      if s1[i] = s2[j] then
        cost := 0
      else
        cost := 1;

      above := d[i - 1, j] + 1;
      left := d[i, j - 1] + 1;
      diag := d[i - 1, j - 1] + cost;

      d[i, j] := Min(Min(above, left), diag);
    end;
  end;

  Result := d[lenS1, lenS2];
end;

function GetExeFileName: string;
begin
  result := ExtractFileName(ParamStr(0));
end;

function FindProjectFileFolder: string;
var
  ProjectFileName, CurrentDir: string;
begin
  var level := 0;
  ProjectFileName := ChangeFileExt(GetExeFileName, '.dpr');
  CurrentDir := ExtractFilePath(ParamStr(0));

  // Search for the project file in the current directory and its parent directories
  while (level < 3) and not TFile.Exists(TPath.Combine(CurrentDir, ProjectFileName)) do
  begin
    CurrentDir := ExtractFilePath(ExcludeTrailingPathDelimiter(CurrentDir));
    inc (level);
    // Raise an exception if we've reached the root directory or an invalid path
    if CurrentDir = '' then
      raise Exception.Create('Project file not found.');
  end;

  result := CurrentDir;
end;

function FindProjectFilePath: string;
begin
  Result := TPath.Combine(FindProjectFileFolder, ChangeFileExt(GetExeFileName, '.dpr'));
end;

function GetIPAddress: string;
var
  WSAData: TWSAData;
  HostName: array[0..255] of AnsiChar;
  HostEnt: PHostEnt;
  Addr: PAnsiChar;
begin
  if WSAStartup($0202, WSAData) <> 0 then
  begin
    Writeln('WSAStartup failed.');
    Exit;
  end;

  try
    if gethostname(HostName, SizeOf(HostName)) = 0 then
    begin
      HostEnt := gethostbyname(HostName);
      if Assigned(HostEnt) then
      begin
        Addr := inet_ntoa(PInAddr(HostEnt^.h_addr_list^)^);
        result := string(Addr);
      end
      else
        result := 'Failed to retrieve host information.';
    end
    else
      result := 'Failed to get host name.';
  finally
    WSACleanup;
  end;
end;

procedure SetupReportMemoryLeaksOnShutdown;
begin
  {$WARN SYMBOL_PLATFORM OFF}
  {$IFDEF MSWINDOWS} {$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  {$ENDIF}  {$ENDIF}
  {$WARN SYMBOL_PLATFORM ON}
end;

function FrontEllipsisString (const s: string; MaxLength: integer): string;
begin
  if length(s) > MaxLength
     then result := '...'+copy (s, length(s)-MaxLength+4, MaxLength)
     else result := s;
end;

function EndEllipsisString (const s: string; MaxLength: integer): string;
begin
  if length(s) > MaxLength
     then result := copy (s, 1, MaxLength-3)+'...'
     else result := s;
end;


{ TListHelper }

function TListHelper.Contains(const AObject: TObject): boolean;
begin
  result := IndexOf(AObject) > -1;
end;

{ TStringsHelper }

function TStringsHelper.Contains(const AValue: string): boolean;
begin
  result := IndexOf (AValue) > -1;
end;

function TStringsHelper.ContainsName(const AValue: string): boolean;
begin
  result := IndexOfName (AValue) > -1;
end;

procedure ElevateSelf;
var
  ExePath, Params: string;
  I: Integer;
begin
  ExePath := ParamStr(0); // Get the current executable path
  Params := '';

  // Reconstruct command-line parameters
  for I := 1 to ParamCount do
    Params := Params + ' ' + ParamStr(I);

  ShellExecute(0, 'runas', PChar(ExePath), PChar(Trim(Params)), nil, SW_SHOWNORMAL);
  Halt; // Exit current non-elevated instance
end;

function StringCompareIgnoringCRLF(const S1, S2: string): Boolean;
begin
  Result := StringReplace(StringReplace(S1, #13, '', [rfReplaceAll]), #10, '', [rfReplaceAll]) =
            StringReplace(StringReplace(S2, #13, '', [rfReplaceAll]), #10, '', [rfReplaceAll]);
end;


initialization
  HasConsole := CheckHasConsole;

finalization
  csPostponeOnce.Free;
  lstPostponeOnceMethods.Free;
  lstPostponeOnceTimes.Free;
  PostponeOnceThread.Free;
  lstPostponedObjects.Free;
  lstReadyPostponedMethods.Free;
end.
