﻿unit ReportTemplate;

interface

uses
  System.Classes,
  SynCommons, mormot,
  MormotTypes, Mormotmods, MormotAttributes, Validation, SqlRecords;

type

  TVariantArray = array of variant;

  TsqlReportTemplateDetailRecords = class;

  [CrudPermissions (1, 1, 1, 1)]
  TSqlReportTemplate = class(TSqlRecordEx)
  private
    FDisplayName: RawUTF8;
    FTemplateFilename: RawUTF8;
    FTemplateTablename: RawUTF8;
    FNotes: RawUTF8;
    FShortDisplayName: RawUTF8;
    FGenderFieldName: RawUTF8;
    FQualifierFields: TRawUTF8DynArray;
    FQualifierValues: TVariantDynArray;
    FQualifierOperators: TFilterOperatorDynArray;
    function GetTemplateSqlRecordClass: TSqlRecordClass;
    function GetQualifierFilter: TQueryFilter;
    procedure SetQualifierFilter(const Value: TQueryFilter);
  public
      TemplateData : TMemoryStream;
      TemplateFields : TStringList;
      Details: TsqlReportTemplateDetailRecords;
    class var
      Model : TSqlModelEx;
    procedure GetFieldNames(out fields: TStringList);
    constructor Create; override;
    destructor Destroy; override;
//    procedure AddCondition (FieldName: RawUTF8; Value: variant);
//    procedure ClearConditions;

    property TemplateSqlRecordClass : TSqlRecordClass read GetTemplateSqlRecordClass;
    // cant use "Filter" - it is a member of something inherited in mormot
    property QualifierFilter: TQueryFilter read GetQualifierFilter write SetQualifierFilter;
    function QualifierFlags: TArray<cardinal>;
  published
    [Displayable, GeneralFilter, NotNull]
    property DisplayName : RawUTF8 read FDisplayName write FDisplayName;

    [Displayable, GeneralFilter] // if empty, use DisplayName
    property ShortDisplayName : RawUTF8 read FShortDisplayName write FShortDisplayName;

    [Displayable, NotNull]
    property TemplateFilename : RawUTF8 read FTemplateFilename write FTemplateFilename;

    [Displayable, NotNull, FieldInfoTableName]
    property TemplateTableName : RawUTF8 read FTemplateTablename write FTemplateTablename;

    [Displayable]
    property GenderFieldName : RawUTF8 read FGenderFieldName write FGenderFieldName;

    // LegalForm=1, ForVehicle=true
//    property ConditionFields: TRawUTF8DynArray read FConditionFields write FConditionFields;

//    property ConditionValues: TVariantArray read FConditionValues write FConditionValues;

    property QualifierFields: TRawUTF8DynArray read FQualifierFields write FQualifierFields;
    property QualifierOperators: TFilterOperatorDynArray read FQualifierOperators write FQualifierOperators;
    property QualifierValues: TVariantDynArray read FQualifierValues write FQualifierValues;

    property Notes : RawUTF8 read FNotes write FNotes;
  end;

  [CrudPermissions (1, 1, 1, 1)]
  TSqlReportTemplateDetail = class(TSqlRecordEx)
  private
    FTagKeyFieldName: RawUTF8;
    FTableName: RawUTF8;
    FMasterIDFieldName: RawUTF8;
    FReportTemplateID: TID;
    FNotes: RawUTF8;
    FForeignKeyFieldName: RawUTF8;
    FTagKeyDomainTableName: RawUTF8;
  public
    ValidKeyValues : TStringList;

    constructor Create; override;
    destructor Destroy; override;
  published
    [FKRestricted (TSqlReportTemplate), NotNull]
    property ReportTemplateID: TID read FReportTemplateID write FReportTemplateID;

    [Displayable, NotNull] // (Detail)TableName.ForeignKeyFieldName = (Master)ReportTemplate.MasterIDFieldName
    property MasterIDFieldName : RawUTF8 read FMasterIDFieldName write FMasterIDFieldName;

    [GeneralFilter, Displayable, NotNull, FieldInfoTableName] // (Detail)TableName
    property TableName : RawUTF8 read FTableName write FTableName;

    [Displayable, NotNull] //(Detail)TableName.ForeignKeyFieldName
    property ForeignKeyFieldName : RawUTF8 read FForeignKeyFieldName write FForeignKeyFieldName;

    [Displayable, NotNull]
    property TagKeyFieldName : RawUTF8 read FTagKeyFieldName write FTagKeyFieldName;

    [Displayable, NotNull, FieldInfoTableName]
    property TagKeyDomainTableName : RawUTF8 read FTagKeyDomainTableName write FTagKeyDomainTableName;

    property Notes : RawUTF8 read FNotes write FNotes;
  end;

  TsqlReportTemplateDetailRecords = class (TSqlRecords<TSqlReportTemplateDetail>);

implementation

uses
  System.Rtti, System.TypInfo, System.SysUtils,
  Common.Utils;

{ TSqlReportTemplate }

constructor TSqlReportTemplate.Create; //(const AFileName: string; const ADisplayName : string; AReportTemplateSqlRecordClass : TClass);
begin
  inherited Create;
  TemplateFields := TStringList.Create;

//  FDisplayName := ADisplayName;
//  ReportTemplateSqlRecordClass := AReportTemplateSqlRecordClass;

//  GetFieldNames(TemplateFields);
//  TemplateFields
end;

destructor TSqlReportTemplate.Destroy;
begin
  TemplateFields.Free;
  FreeAndNil(TemplateData);
  FreeAndNil(Details);
  inherited;
end;

procedure TSqlReportTemplate.GetFieldNames(out fields: TStringList);
begin
  var reccl := TemplateSqlRecordClass;
  if not assigned(reccl) then exit;
  var ctx := TRttiContext.Create;
  try
//    for var p in ctx.GetType(reccl).GetProperties do
//      if (p.Visibility in [mvPublished]) and
//         p.HasAttribute(DisplayableAttribute)
//         then
//        fields.Append(p.Name);
    for var fi in TemplateSqlRecordClass.FieldInfoList do
      if not fi.IsForeignKey and (fi.Name <> 'ID') then
        fields.Add(fi.Name);
  finally
    ctx.Free;
  end;
end;

function TSqlReportTemplate.GetQualifierFilter: TQueryFilter;
begin
  result := default (TQueryFilter);
  result.FieldNames := QualifierFields;
  result.Operators  := QualifierOperators;
  result.Values     := QualifierValues;
  result.Flags      := QualifierFlags;
end;

function TSqlReportTemplate.GetTemplateSqlRecordClass: TSqlRecordClass;
begin
  Result := Model.Table[TemplateTablename];
end;

function TSqlReportTemplate.QualifierFlags: TArray<cardinal>;
begin
  SetLength (result, length (QualifierValues)); //dummy
end;

procedure TSqlReportTemplate.SetQualifierFilter(const Value: TQueryFilter);
begin
  QualifierFields := Value.FieldNames;
  QualifierOperators := Value.Operators;
  QualifierValues := Value.Values;
end;

{ TSqlReportTemplateDetail }

constructor TSqlReportTemplateDetail.Create;
begin
  inherited;
  ValidKeyValues := TStringList.Create;
end;

destructor TSqlReportTemplateDetail.Destroy;
begin
  ValidKeyValues.Free;
  inherited;
end;


end.
