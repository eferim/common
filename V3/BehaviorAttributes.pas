﻿unit BehaviorAttributes;

interface

uses
  System.Classes,
  System.Generics.Collections,
  System.SysUtils,
  System.Messaging,
  System.Rtti,
  FMX.Forms,
  FMX.Controls,
  FMX.Types,
  FMX.Menus,
  Validation,
  Common.Utils
  ;

type


  BehaviorAttribute<TEventType> = class(TCustomAttribute)
  private
    FEventName: string;
    class var
      FSpecialEvent: TEventType; {e.g. OnClick : TNotifyEvent}
  protected
    class var
      OriginalEvents : TDictionary<TControl, TEventType>;
      dummy : TEventType;
      IgnoreSelfCall : boolean;
    property EventName : string read FEventName write FEventName;
    class property SpecialEvent : TEventType read FSpecialEvent;
    class function GetPropertyValue(obj : TObject; const PropertyName : string) : TValue;
    class procedure SetPropertyValue(obj : TObject; const PropertyName : string; Avalue : TValue);
    class procedure InvokeMethod(obj : TObject; const MethodName : string; Args : array of TValue);
    class constructor Create;
    class destructor Destroy;
    class procedure FormBeforeShownHandler(const Sender: TObject; const M: TMessage); virtual;
    class procedure SetSpecificPropertyOrBehavior(ctrl : TControl); virtual;


  end;

  TRemoveBlanks = (Leading, Trailing, All);

  RemoveBlanksAttribute = class(BehaviorAttribute<TNotifyEvent>)
  private
    class procedure DoSpecialEvent(sender : TObject);
  protected
    FRemoveBlanks : TRemoveBlanks;
    class constructor Create;
  public
    constructor Create(AEventName : string; ARemove : TRemoveBlanks);
  end;

  MultiLinePasteAttribute = class(BehaviorAttribute<TKeyEvent>)
  private
  class var
    PopupMenu : TPopupMenu;
//   OnKeyDownEvents : TDictionary<TControl, TKeyEvent>;

    class procedure CutClick(sender : TObject);
    class procedure CopyClick(sender : TObject);
    class procedure PasteClick(sender : TObject);
    class procedure DeleteClick(sender : TObject);
    class Procedure SelectAllClick(sender : TObject);
    class function SpecialPaste : string;
//    class procedure SpecialKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
//    class function HasTextProperty(obj: TObject): Boolean;
//    class procedure SetTextProperty(obj: TObject; const value: string);
//    class function GetTextProperty(obj: TObject) : string;
//    class function GetSelectedText(obj : TObject) : string;
//    class function GetSelStart(obj : TObject) : integer;
//    class function GetSelLength(obj : TObject) : integer;
//    class function GetCaretPos(obj : TObject) : integer;
//    class procedure SetCaretPos(obj : TObject; pos: integer);
    class procedure SetClipboard(const buffer : string);
    class function GetClipboard : string;
//    class procedure DeleteSelection(obj : TObject);

  protected

//    class function GetIntegerProperty(obj : TObject; const PropertyName : string) : integer;
    class constructor Create;
    class destructor Destroy;
  private
    class var
      CutItem, CopyItem, PasteItem, DeleteItem, SelAllItem : TMenuItem;
    class procedure DoSpecialEvent(Sender: TObject; var Key: Word; var KeyChar: WideChar; Shift: TShiftState);
  protected
    class procedure FormBeforeShownHandler(const Sender: TObject; const M: TMessage); override;
    class procedure SetSpecificPropertyOrBehavior(ctrl : TControl); override;
  public
    constructor Create;


  end;


  DummyAttribute = class(BehaviorAttribute<TKeyEvent>)
  protected
    class constructor Create;
  end;

implementation

uses
  System.UITypes,
  System.TypInfo,
  FMX.Platform;

{ BehaviorAttribute<T> }

class constructor BehaviorAttribute<TEventType>.Create;
begin
  OriginalEvents := TDictionary<TControl, TEventType>.Create;
//  TMessageManager.DefaultManager.SubscribeToMessage(TFormBeforeShownMessage, FormBeforeShownHandler)
end;

class destructor BehaviorAttribute<TEventType>.Destroy;
begin
  OriginalEvents.Free;
end;

class procedure BehaviorAttribute<TEventType>.FormBeforeShownHandler(const Sender: TObject; const M: TMessage);
var
  dummy : TEventType;
  EventPropertyValue : TValue;
begin
  if not (M is TFormBeforeShownMessage) then exit;
  var ctx := TRttiContext.Create;
  var ShowingForm := TFormBeforeShownMessage(M).Value;
  if ShowingForm is TForm then
  begin

    var typ := ctx.GetType(ShowingForm.ClassType);
    for var f in typ.GetFields do
    begin
      var A : BehaviorAttribute<TEventType> := f.FindAttribute(self) as self;
      if assigned(A) then
      begin
        var c := f.GetValue(ShowingForm).AsObject;
        if c is TControl then
        begin
          var ctrl := c as TControl;
          var EventProperty := ctx.GetType(ctrl.ClassType).GetProperty(A.EventName);
          if assigned(EventProperty) then
            EventPropertyValue :=  EventProperty.GetValue(ctrl)
          else
            raise Exception.Create(A.EventName + ' not found! in ' + ctrl.Name);
          if not OriginalEvents.TryGetValue(ctrl, dummy) then
          begin
            OriginalEvents.AddOrSetValue(ctrl, EventPropertyValue.AsType<TEventType>);
            EventProperty.SetValue(ctrl, TValue.From<TEventType>(SpecialEvent));
            // set additional property
            SetSpecificPropertyOrBehavior(ctrl);
          end
          else
            exit;
        end;
      end;
    end;
  end;
end;


class function BehaviorAttribute<TEventType>.GetPropertyValue(obj: TObject; const PropertyName: string): TValue;
var
  ctx : TRttiContext;
  typ : TRttiType;
  prop : TRttiProperty;
begin
  ctx := TRttiContext.Create;
  try
    typ := ctx.GetType(obj.ClassType);
    prop := typ.GetProperty(PropertyName);
    if Assigned(prop) and (prop.Visibility > mvProtected) then
      Result := prop.GetValue(obj)
    else
      Result := TValue.Empty;
  finally
    ctx.Free;
  end;

end;

class procedure BehaviorAttribute<TEventType>.InvokeMethod(obj: TObject; const MethodName: string;
  Args: array of TValue);
var
  ctx : TRttiContext;
//  typ : TRttiType;
  meth : TRttiMethod;
begin
  ctx := TRttiContext.Create;
  try
    meth := ctx.GetType(obj.ClassType).GetMethod(MethodName);
    if Assigned(meth) and (meth.Visibility > mvProtected) and (meth.MethodKind in [mkProcedure, mkClassProcedure, mkFunction, mkClassFunction]) then
      meth.Invoke(obj, args);
  finally
    ctx.Free;
  end;

end;

class procedure BehaviorAttribute<TEventType>.SetPropertyValue(obj: TObject; const PropertyName: string;
  Avalue: TValue);
begin
  var ctx := TRttiContext.Create;
  try
    ctx.GetType(obj.ClassType).GetProperty(PropertyName).SetValue(obj, AValue);
  finally
    ctx.Free;
  end;

end;

class procedure BehaviorAttribute<TEventType>.SetSpecificPropertyOrBehavior(ctrl: TControl);
begin
  // abstract
end;

{ RemoveBlanksAttribute }

constructor RemoveBlanksAttribute.Create(AEventName: string; ARemove: TRemoveBlanks);
begin
  inherited Create;
  FEventName := AEventName;
  FRemoveBlanks := ARemove;
end;

class constructor RemoveBlanksAttribute.Create;
begin
  inherited;
  TMessageManager.DefaultManager.SubscribeToMessage(TFormBeforeShownMessage, FormBeforeShownHandler);
  FSpecialEvent := DoSpecialEvent;
end;

class procedure RemoveBlanksAttribute.DoSpecialEvent(sender: TObject);
var
  s : string;
  orig : TNotifyEvent;

  function BlankCountBeforeCaret(CaretPos : integer) : integer;
  var
    bc : integer;
  begin
    BC := 0;
    for var i := 0 to CaretPos - 1 do
      if s[i + 1] = ' ' then
        inc(bc);
    Result := bc;
  end;
begin
  if IgnoreSelfCall then exit;

  var ctrl := sender as TControl;
  var ctx := TRttiContext.Create;
  var prop := ctx.GetType(sender.ClassType).GetProperty('Text');
  var field := ctx.GetType(ctrl.Owner.ClassType).GetField(ctrl.Name);
  var attr := FieldGetAttribute(field, RemoveBlanksAttribute);
  if Assigned(prop) then
  begin
    IgnoreSelfCall := true;
    s := prop.GetValue(sender).AsString;
    var cp := GetPropertyValue(sender, 'CaretPosition').AsInteger;
    case (attr as RemoveBlanksAttribute).FRemoveBlanks of
      Leading:  prop.SetValue(sender, TValue.From(TrimLeft(s)));
      Trailing: prop.SetValue(sender, TValue.From(TrimRight(s)));
      All:      prop.SetValue(sender, TValue.From(Trim(s)));
    end;
    SetPropertyValue(sender, 'CaretPosition', cp - BlankCountBeforeCaret(cp));
    IgnoreSelfCall := False;
  end;
  ctx.Free;
  if OriginalEvents.TryGetValue(ctrl, Orig) then
    if assigned(Orig) then
      Orig(sender);
end;

{ DummyAttribute }

class constructor DummyAttribute.Create;
begin
//  FSpecialEvent := DoSpecialEvent;
end;


{ MultiLinePasteAttribute }

class constructor MultiLinePasteAttribute.Create;
begin
  inherited;
  TMessageManager.DefaultManager.SubscribeToMessage(TFormBeforeShownMessage, FormBeforeShownHandler);
  FSpecialEvent := DoSpecialEvent;

  PopupMenu := TPopupMenu.Create(nil);
  var FPopupMenuItem := TMenuItem.Create(PopupMenu);
  FPopupMenuItem.Text := 'Iseci';
  PopupMenu.AddObject(FPopupMenuItem);
  FPopupMenuItem.OnClick := CutClick;
  CutItem := FPopupMenuItem;

  FPopupMenuItem := TMenuItem.Create(PopupMenu);
  FPopupMenuItem.Text := 'Kopiraj';
  PopupMenu.AddObject(FPopupMenuItem);
  FPopupMenuItem.OnClick := CopyClick;
  CopyItem := FPopupMenuItem;

  FPopupMenuItem := TMenuItem.Create(PopupMenu);
  FPopupMenuItem.Text := 'Nalepi';
  PopupMenu.AddObject(FPopupMenuItem);
  FPopupMenuItem.OnClick := PasteClick;
  PasteItem := FPopupMenuItem;

  FPopupMenuItem := TMenuItem.Create(PopupMenu);
  FPopupMenuItem.Text := 'Obriši';
  PopupMenu.AddObject(FPopupMenuItem);
  FPopupMenuItem.OnClick := DeleteClick;
  DeleteItem := FPopupMenuItem;

  FPopupMenuItem := TMenuItem.Create(PopupMenu);
  FPopupMenuItem.Text := '-';
  PopupMenu.AddObject(FPopupMenuItem);

  FPopupMenuItem := TMenuItem.Create(PopupMenu);
  FPopupMenuItem.Text := 'Označi sve';
  PopupMenu.AddObject(FPopupMenuItem);
  FPopupMenuItem.OnClick := SelectAllClick;
  SelAllItem := FPopupMenuItem;
end;

class procedure MultiLinePasteAttribute.CopyClick(sender: TObject);
begin
  var obj := Screen.ActiveForm.Focused.GetObject;
  SetClipboard(GetPropertyValue(obj, 'SelText').AsString);
end;

constructor MultiLinePasteAttribute.Create;
begin
  inherited Create;
  FEventName := 'OnKeyDown';
end;

class procedure MultiLinePasteAttribute.CutClick(sender: TObject);
begin
  var obj := Screen.ActiveForm.Focused.GetObject;
  var b := GetPropertyValue(obj, 'SelText');
  SetClipboard(b.AsString);
  InvokeMethod(obj, 'DeleteSelection', []);
//  DeleteSelection(obj);
end;

class procedure MultiLinePasteAttribute.DeleteClick(sender: TObject);
begin
  var obj := Screen.ActiveForm.Focused.GetObject;
  InvokeMethod(obj, 'DeleteSelection', []);
end;

class destructor MultiLinePasteAttribute.Destroy;
begin
  FreeAndNil(PopupMenu);
  inherited;
end;

class procedure MultiLinePasteAttribute.DoSpecialEvent(Sender: TObject; var Key: Word; var KeyChar: WideChar; Shift: TShiftState);
var
  v : TKeyEvent;
begin
  if sender is TControl then
  begin
    if ((Key = ord('V')) and (ssCtrl in Shift)) or ((key = vkInsert) and (ssShift in Shift)) then
    begin
      PasteClick(sender);
      key := 0;
    end
    else
      if OriginalEvents.TryGetValue(sender as TControl, v) then
        if Assigned(v) then
          v(Sender, key, KeyChar, Shift);
  end;

end;

class procedure MultiLinePasteAttribute.FormBeforeShownHandler(const Sender: TObject; const M: TMessage);
begin
  if not (M is TFormBeforeShownMessage) then exit;
  inherited;
  var frm := TFormBeforeShownMessage(M).Value;
  if frm is TCustomPopupForm then
    if Assigned(Screen.ActiveForm) then
    begin
      var obj := Screen.ActiveForm.Focused.GetObject as TControl;
      if OriginalEvents.TryGetValue(obj, dummy) then
      begin
        var t := GetPropertyValue(obj, 'Text').AsString;
        var en := GetPropertyValue(obj, 'SelText').AsString <> '';
        CutItem.Enabled := en;
        CopyItem.Enabled := en;
        PasteItem.Enabled := GetClipboard <> '';
        DeleteItem.Enabled := en;
        SelAllItem.Enabled := t <> '';
      end;
    end;
end;

class procedure MultiLinePasteAttribute.PasteClick(sender: TObject);
var
  txt, lead, trail : string;
begin
  var obj := Screen.ActiveForm.Focused.GetObject;
  var P := GetPropertyValue(obj, 'Text');
  if not P.IsEmpty then
  begin
    txt := p.AsString;
    if GetPropertyValue(obj, 'SelText').AsString = '' then
    begin
      var cp := GetPropertyValue(obj, 'CaretPosition').AsInteger;
      lead := copy(txt, 1, cp);
      trail := copy(txt, cp + 1, Length(txt) - cp);
      txt := format('%s%s%s', [lead, SpecialPaste, trail]);
    end
    else
    begin
      var sp := GetPropertyValue(obj, 'SelStart').AsInteger;
      var l := GetPropertyValue(obj, 'SelLength').AsInteger;
      lead := copy(txt, 1, sp);
      trail := copy(txt, sp + l + 1, length(txt) - sp - l );
      txt := format('%s%s%s', [lead, SpecialPaste, trail]);
    end;
    SetPropertyValue(obj, 'Text', txt);
    SetPropertyValue(obj, 'CaretPosition', Length(txt) - length(trail));
  end;
end;

class function MultiLinePasteAttribute.GetClipboard: string;
var
  ClipBoard : IFMXClipboardService;
begin
  Result := '';
  if TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, ClipBoard) then
  begin
    var V := ClipBoard.GetClipboard;
    if not V.IsEmpty and V.TryAsType(Result) then
      Result := V.AsString;
  end;
end;

class procedure MultiLinePasteAttribute.SelectAllClick(sender: TObject);
begin
  var obj := Screen.ActiveForm.Focused.GetObject;
  InvokeMethod(obj, 'SelectAll', []);
end;

class procedure MultiLinePasteAttribute.SetClipboard(const buffer: string);
var
  ClipBoard : IFMXClipboardService;
begin
  if TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, ClipBoard) then
    ClipBoard.SetClipboard(buffer);

end;

class procedure MultiLinePasteAttribute.SetSpecificPropertyOrBehavior(ctrl: TControl);
begin
  ctrl.PopupMenu := PopupMenu;
end;

class function MultiLinePasteAttribute.SpecialPaste: string;
begin
  Result := StringReplace(GetClipboard, #13#10, ' ', [rfReplaceAll]);
end;

end.
