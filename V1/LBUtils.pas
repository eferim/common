unit LBUtils;

interface

uses SysUtils, DateUtils, StrUtils, Threading, Classes, System.Rtti;

var
  chrSortAscending, chrSortDescending : char;

type
  TCRUD = (Unknown, Create, Read, ReadMany, Update, UpdateMissingFields, Delete, ReadLock, Unlock);

  INamed = interface
//  ['{6A4FABF9-8192-4A83-8598-5360FDC6F19F}']
    function GetName: string;
    property Name: string read GetName;
  end;

  TOutcome = record
  public
    ID: int64;
    Success, ErrorHandled: boolean;
    RecordCount, WarningCode, ErrorCode: integer;
    Description: string;
    function SetError (AErrorCode: integer; ADescription: string = ''): TOutcome;
    function SetSuccess: TOutcome; inline;
    function SetID (AID: int64): TOutcome; // sets ID, null is interpreted as failure
    function ToString: string;
    function IsCriticalError: boolean; // TODO nesto pametnije
    function DescriptionInSingleLine: string;
  end;

  TNonARCInterfacedObject = class
  public
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

  TPointerHelper = record helper for pointer
    function IsAssigned: boolean; inline;
    function IsNotAssigned: boolean; inline;
  end;

  TObjectHelper = class helper for TObject
    function IsAssigned: boolean; inline;
    function IsNotAssigned: boolean; inline;
  end;

  TRTTITypeHelper = class helper for TRTTIType
    function HasProperty(typ : TRttiType; PropertyName : string) : boolean;
  end;

  function MinutesSince (const dt: TDateTime): double;
  function MillisecondsSince (const dt: TDateTime): double;
  function StripThousandsSeparator (const s: string; separator: char): string;
  function SecondsSince (const AThen: TDateTime): double;
  function GetResourceString (const Identifier: string): string;
  procedure Postpone(const AProc: TThreadProcedure; const ADelayMS: Cardinal = 0); overload;
  procedure Postpone(const AProc: TThreadMethod; const ADelayMS: Cardinal = 0); overload;

  procedure TypeKey(const Key: Cardinal; const Ctrl: boolean = false);
  procedure TypeString(const Str: String);

  function ExtractQuotedString (var s: string): string;
  function GetFileSize (const FileName: TFileName): Longint;

var
  CRUDToString: array [TCRUD] of string = ('Unknown', 'Create', 'Read', 'ReadMany', 'Update', 'Update missing fields', 'Delete', 'ReadLock', 'Unlock');

{$IFDEF MSWINDOWS}
  procedure GetBuildInfo(var V1, V2, V3, V4: word);
  function GetBuildInfoAsString: string;
{$ENDIF}
implementation

uses
  System.Types
  {$IFDEF MSWINDOWS}, Windows{$ENDIF}
  ;

{ TOutcome }

function StripThousandsSeparator (const s: string; separator: char): string;
begin
  result := s.replace (separator, '');
end;

function TOutcome.DescriptionInSingleLine: string;
begin
  result := StringReplace(Description, #13, ' ', [rfReplaceAll]);
  result := StringReplace(result, #10, ' ', [rfReplaceAll]);
end;

function TOutcome.IsCriticalError: boolean;
begin
  result := ErrorCode < 0;
end;

function TOutcome.SetError(AErrorCode: integer; ADescription: string): TOutcome;
begin
  ErrorCode := AErrorCode;
  Description := ADescription;
  Success := false;
end;

function TOutcome.SetID(AID: int64): TOutcome;
begin
  ID := AID;
  Success := ID <> 0;
end;

function TOutcome.SetSuccess: TOutcome;
begin
  Success := true;
  ErrorCode := 0;
  Description := '';
  result := self;
end;

function TOutcome.ToString: string;
begin
  var sID   := IfThen (ID <> 0, format (', ID = %d', [ID]), '');
  var sSucc := IfThen (Success, 'OK', 'FAIL');

  if Success and (Description = '')
    then
      result := format ('%s%s', [sSucc, sID])
    else result := format ('%s%s, ErrorCode = %d, Description = %s',
            [sSucc, sID, ErrorCode, Description]);
end;

{TNonARCInterfacedObject}

function TNonARCInterfacedObject.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TNonARCInterfacedObject._AddRef: Integer;
begin
  Result := -1;
end;

function TNonARCInterfacedObject._Release: Integer;
begin
  Result := -1;
end;

function MinutesSince (const dt: TDateTime): double;
begin
  result := (now - dt) * MinsPerDay;
end;

function MillisecondsSince (const dt: TDateTime): double;
begin
  result := (now - dt) * MSecsPerDay;
end;

function SecondsSince (const AThen: TDateTime): double;
begin
  Result := Abs(DateTimeToMilliseconds(Now) - DateTimeToMilliseconds(AThen))/1000;
end;

procedure Postpone(const AProc: TThreadProcedure; const ADelayMS: Cardinal = 0); overload;
begin
  TTask.Run(
    procedure
    begin
      if ADelayMS > 0 then begin
        TThread.Sleep(ADelayMS);
      end;
      TThread.Queue(nil, AProc);
    end);
end;

procedure Postpone(const AProc: TThreadMethod; const ADelayMS: Cardinal = 0); overload;
begin
  TTask.Run(
    procedure
    begin
      if ADelayMS > 0 then begin
        TThread.Sleep(ADelayMS);
      end;
      TThread.Queue(nil, AProc);
    end);
end;

{$IFDEF MSWINDOWS}
procedure GetBuildInfo(var V1, V2, V3, V4: word);
var
  VerInfoSize, VerValueSize, Dummy: DWORD;
  VerInfo: Pointer;
  VerValue: PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  if VerInfoSize > 0 then
  begin
      GetMem(VerInfo, VerInfoSize);
      try
        if GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo) then
        begin
          VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
          with VerValue^ do
          begin
            V1 := dwFileVersionMS shr 16;
            V2 := dwFileVersionMS and $FFFF;
            V3 := dwFileVersionLS shr 16;
            V4 := dwFileVersionLS and $FFFF;
          end;
        end;
      finally
        FreeMem(VerInfo, VerInfoSize);
      end;
  end;
end;

function GetBuildInfoAsString: string;
var
  V1, V2, V3, V4: word;
begin
  GetBuildInfo(V1, V2, V3, V4);
  Result := format('%d.%d.%d.%d', [V1, V2, V3, V4]);
//  IntToStr(V1) + '.' + IntToStr(V2) + '.' +
//    IntToStr(V3) + '.' + IntToStr(V4);
end;

{$ENDIF}


procedure TypeKey(const Key: Cardinal; const Ctrl: boolean = false);
const KEYEVENTF_KEYDOWN = 0;
      KEYEVENTF_UNICODE = 4;
var rec: TInput;
    rLen: Integer;
    shift: Boolean;
begin
  rLen:=SizeOf(TInput);

  shift:=(Key shr 8)=1;

  if shift then begin
    rec.Itype:=INPUT_KEYBOARD;
    rec.ki.wVk:=VK_SHIFT;
    rec.ki.dwFlags:=KEYEVENTF_KEYDOWN; // or KEYEVENTF_UNICODE;
    SendInput(1,rec,rLen);
  end;

  if Ctrl then begin
    rec.Itype:=INPUT_KEYBOARD;
    rec.ki.wVk:=VK_LCONTROL;
    rec.ki.dwFlags:=KEYEVENTF_KEYDOWN; // or KEYEVENTF_UNICODE;
    SendInput(1,rec,rLen);
  end;


  rec.Itype:=INPUT_KEYBOARD;
  rec.ki.wVk:=Key;
  rec.ki.dwFlags:=KEYEVENTF_KEYDOWN; // or KEYEVENTF_UNICODE;
  SendInput(1,rec,rLen);

  rec.Itype:=INPUT_KEYBOARD;
  rec.ki.wVk:=Key;
  rec.ki.dwFlags:=KEYEVENTF_KEYUP; // or KEYEVENTF_UNICODE;
  SendInput(1,rec,rLen);

  if Ctrl then begin
    rec.Itype:=INPUT_KEYBOARD;
    rec.ki.wVk:=VK_LCONTROL;
    rec.ki.dwFlags:=KEYEVENTF_KEYUP; // or KEYEVENTF_UNICODE;
    SendInput(1,rec,rLen);
  end;

  if shift then begin
    rec.Itype:=INPUT_KEYBOARD;
    rec.ki.wVk:=VK_SHIFT;
    rec.ki.dwFlags:=KEYEVENTF_KEYUP; // or KEYEVENTF_UNICODE;
    SendInput(1,rec,rLen);
  end;
end;

procedure TypeString(const Str: String);
var
  i, sLen: Integer;
begin
  sLen:=Length(Str);
  if sLen>0 then for i:=1 to sLen do TypeKey(vkKeyScan(Str[i]));
end;

{ TPointerHelper }

function TPointerHelper.IsAssigned: boolean;
begin
  result := Assigned (self);
end;

function TPointerHelper.IsNotAssigned: boolean;
begin
  result := not Assigned (self);
end;

{ TObjectHelper }

function TObjectHelper.IsAssigned: boolean;
begin
  result := Assigned (self);
end;

function TObjectHelper.IsNotAssigned: boolean;
begin
  result := not Assigned (self);
end;

function GetResourceString (const Identifier: string): string;
var
  Stream: TResourceStream;
  //ss: TStringStream;
  sl: TStringList;
begin
  result := '';
  Stream := TResourceStream.Create(HInstance, Identifier, RT_RCDATA);
  sl := TStringList.Create;
  //ss := TStringStream.Create (;
  try
    //ss.CopyFrom(Stream, Stream.Size);
    //result := ss.DataString;
    sl.LoadFromStream(Stream, TEncoding.UTF8);
    result := sl.Text;
  finally
    //ss.Free;
    sl.Free;
    Stream.Free;
  end;
end;

function ExtractQuotedString (var s: string): string;
var
  index: integer;
begin
  index := 2;
  while true do
    begin
      if (s [index] = '"') and
         ( (index = length (s)) or (s[index+1] <> '"') ) then break; // found quote, and its either last char or next one isnt a quote
      if (s [index] = '"') then inc (index); // double quote, skip an extra index
      inc (index);
    end;
  result := copy (s, 2, index-2);
  s := copy (s, index+1);
end;

function GetFileSize (const FileName: TFileName): Longint;
var s : TSearchRec;
begin
  if FindFirst (FileName, faAnyFile, s) = 0 then
    begin
      result := s.Size;
      Sysutils.FindClose(s);
    end
  else result := -1;
end;

{ TRTTITypeHelper }

function TRTTITypeHelper.HasProperty(typ: TRttiType; PropertyName: string): boolean;
begin
  if typ.GetProperty(PropertyName) <> nil then
    Result := true
  else
    result := false;
end;

initialization
  if Win32MajorVersion >= 6
    then // Windows Vista or higher
      begin
        chrSortAscending := #$25B2;
        chrSortDescending := #$25BC;
      end
    else
      begin
        chrSortAscending  := '>'; //'�';
        chrSortDescending := '<'; //'�';
      end;

end.
