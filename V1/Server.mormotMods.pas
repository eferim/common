﻿unit Server.mormotMods;

{
Info: Encryption happens in TWebSocketProcess.SendFrame
actual bytes sent done in TCrtSocket.TrySndLow
}

interface

uses
  SysUtils, Contnrs, System.Generics.Collections, Classes, System.Types, System.Generics.Defaults,
  Variants, StrUtils, math, DateUtils, System.TypInfo, RTTI, MormotMods, mORMotSQLite3, SyncObjs,
  SqlRecords, Validation,
  {$ifdef MSWINDOWS}
  Windows, //for inline of synlocker...
  {$endif}
  //todo rework:
  SynDBZeos, SynCommons, SynDB, mormot, SynTable, LBUtils;

const
  NULL_ID = 0;

type
  TIDGenerator = class
  protected
    CriticalSection: TCriticalSection;
    FCurrentID: TID;
  public
    constructor Create (Server: TSQLRestServerDB; SqlRecordClass: TSQLRecordClass);
    function NextID: TID;
  end;

  TServerSettings = class (TPersistent)
  strict private
    FServer: TSQLDBDefinition;
    FServerName, FDBPath, FUsername, FPassword: RawUTF8;
    FLibraryLocation: TFileName;
    FConsoleLog, FAppendExePath: boolean;
    FPort: RawUTF8;
    FCallbacks: boolean;
  published
    property Server: TSQLDBDefinition read FServer write FServer;
    property ServerName: RawUTF8 read FServerName write FServerName;
    property LibraryLocation: TFileName read FLibraryLocation write FLibraryLocation;
    property AppendExePath: boolean read FAppendExePath write FAppendExePath;
    property DBPath: RawUTF8 read FDBPath write FDBPath;
    property Username: RawUTF8 read FUsername write FUsername;
    property Password: RawUTF8 read FPassword write FPassword;
    property ConsoleLog: boolean read FConsoleLog write FConsoleLog;
    property Port: RawUTF8 read FPort write FPort;
    property Callbacks: boolean read FCallbacks write FCallbacks;
  end;

  TSQLDBZEOSConnectionPropertiesEx = class (TSQLDBZEOSConnectionProperties) //TODO make helper?
  private
    function IndexExists(const aTableName: RawUTF8;
      const aFieldNames: array of RawUTF8;
      const aIndexName: RawUTF8): boolean;
  public
    function SQLAddIndex (const aTableName: RawUTF8; const aFieldNames: array of RawUTF8; aUnique: boolean;
                          aDescending: boolean=false; const aIndexName: RawUTF8=''): RawUTF8; override;
  end;

  TSQLRestHelper = class helper for TSQLRest
  public
    function Delete(rec: TSQLRecord): boolean; overload;
    function TableHasRowsWhere(Table: TSQLRecordClass; Where: RawUTF8): boolean;
  end;

  TFunRecordLocker = class
    type
      TLockedRecord = record
        TimeStamp: TDateTime;
        Session: cardinal;
        SqlRecordClass: TSQLRecordClass;
        ID: TID;
        procedure Renew;
        constructor Create (ASqlRecordClass: TSQLRecordClass; AID: TID; UnknownSession: boolean = false);
      end;
  private
    function DoLock(cl: TSQLRecordClass; ID: TID; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean;
  protected
    SynLock: TSynLocker;
    Records: TList<TLockedRecord>;
    function FFind (cl: TSQLRecordClass; ID: TID): integer;
  public
    constructor Create;
    destructor Destroy; override;

    function IsLocked(rec: TSQLRecord): boolean; overload;
    function IsLocked(cl: TSQLRecordClass; ID: TID): boolean; overload;

    function Lock(rec: TSQLRecord; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean; overload;
    function Lock(cl: TSQLRecordClass; ID: TID; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean; overload;

    function Unlock(cl: TSQLRecordClass; ID: TID): boolean; overload;
    function Unlock(rec: TSQLRecord): boolean; overload;

    function RenewLocks: integer;
  end;

  TDBChange = record
    SqlRecordClass: TSQLRecordClass;
    ID: TID;
    Operation: TCRUD;
    Session: cardinal;
    TimeStamp: TDateTime;
  end;

  TSQLRestServerDBEx = class (TSQLRestServerDB)
  private
  protected
    function InternalAdd(Value: TSQLRecord; SendData: boolean; CustomFields: PSQLFieldBits; ForceID,
                                            DoNotAutoComputeFields: boolean): TID; override;
  public
    ImagePath: TFileName;
    RecordLocker: TFunRecordLocker;
    DBConnectionProperties: TSQLDBConnectionPropertiesThreadSafe;
    IDGenerators: TDictionary <TSQLRecordClass, TIDGenerator>;
    RestrictedCascades: TRestrictedCascades;
    DBChanges: TList<TDBChange>;
    function RestrictIfTableHasForeignKey(Table: TSQLRecordClass; const FieldName: RawUTF8; FKID: TID; var Outcome: TOutcome): boolean;
    function TableHasForeignKey(Table: TSQLRecordClass; const FieldName: RawUtf8; FKID: TID): boolean;
    function BatchSend(Batch: TSQLRestBatch; var Outcome: TOutcome): integer; overload;
    function ReadLock(rec: TSQLRecord; var Outcome: TOutcome): boolean;
    function UnLock(Table: TSQLRecordClass; aID: TID): boolean; overload; override;
    function UnLock(rec: TSQLRecord): boolean; overload;

    function Retrieve(aID: TID; Value: TSQLRecord; var Outcome: TOutcome; ForUpdate: boolean=false): boolean; reintroduce; overload;
    function Retrieve(Value: TSQLRecord; var Outcome: TOutcome; Lock: boolean = false): boolean; overload;
    function UpdateAndUnlock(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''; DoNotAutoComputeFields: boolean=false): TOutcome;
    function Kick(AUserID: TID): boolean;
    function KickOtherSessions(AUserID: TID; Session: cardinal): boolean;
//    constructor Create(AModel: TSQLModel; AHandleUserAuthentication: boolean=false; AOwnDB: boolean=false); reintroduce;
    constructor Create(AModel: TSQLModel; const aDBFileName: TFileName; ADBConnectionProperties: TSQLDBConnectionPropertiesThreadSafe; aHandleUserAuthentication: boolean=false; AOwnDB: boolean=false); virtual;
//    constructor Create(AModel: TSQLModel; ADB: TSQLDataBase; AZeosDB: TSQLDBZEOSConnectionPropertiesEx; aHandleUserAuthentication: boolean=false; AOwnDB: boolean=false); reintroduce; overload;

    destructor Destroy; override;

    function RetrieveSqlRecords<T: TSQLRecord>(const FormatSQLWhere: RawUTF8; const BoundsSQLWhere: array of const;
                                                     const Fields: RawUTF8 = ''): TSQLRecords<T>;
    function Validate (Value: TSQLRecord): TOutcome;
    function CheckUniqueFields(rec: TSqlRecord): TOutcome;
    function Add(Value: TSQLRecord; SendData: boolean = true; ForceID: boolean=false; DoNotAutoComputeFields: boolean=false): TOutcome; reintroduce; overload;
    function Add(Value: TSQLRecord; Batch: TSQLRestBatch): TOutcome; reintroduce; overload;
    function AddMany (Field: TSQLRecordMany; SourceID: TID; IDs: TIDDynArray; Batch: TSQLRestBatch = nil): TOutcome;
    function ReplaceMany (Field: TSQLRecordMany; SourceID: TID; IDs: TIDDynArray): TOutcome;
    function Update(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''; DoNotAutoComputeFields: boolean=false): TOutcome;
    function UpdateWithoutValidation(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''; DoNotAutoComputeFields: boolean = false): TOutcome;
    function Delete(Table: TSQLRecordClass; ID: TID): TOutcome; reintroduce; overload;
    function Delete(rec: TSQLRecord): TOutcome; reintroduce; overload;

    function AddIDGenerator (Table: TSQLRecordClass): TIDGenerator;
    function RetrieveListObjArrayEx (var ObjArray; Table: TSQLRecordClass; Parameters: TQueryParameters; const aCustomFieldsCSV: RawUTF8=''): boolean;

    //procedure AddFunction (Name: RawUTF8);
    // transaction control
    procedure LockAndStartTransaction;
    procedure CommitAndUnlock;
    procedure RollbackAndUnlock;
    procedure UnLockAndEndTransaction (Success: boolean);
    procedure RegisterDBChange (const SqlRecordClass: TSqlRecordClass; const ID: TID; const Operation: TCRUD);
    function CreateBatch: TSQLRestBatch;
    procedure GenerateID (rec: TSqlRecord);
    procedure CreateIndexes; virtual;
  published
    procedure Sum(Ctxt: TSQLRestServerURIContext);
    procedure Image(Ctxt: TSQLRestServerURIContext);
  end;

var
  SqlRecordLockTimeout: UInt16 = 30; //seconds

implementation

{ TSQLDBZEOSConnectionPropertiesEx }

function TSQLDBZEOSConnectionPropertiesEx.IndexExists(const aTableName: RawUTF8;
  const aFieldNames: array of RawUTF8;
  const aIndexName: RawUTF8): boolean;
var
  IndexName,FieldsCSV, Owner, Table: RawUTF8;
  rows: ISQLDBRows;
begin
  if (self=nil) or (aTableName='') or (high(aFieldNames)<0) then
    exit (false);

  if aIndexName=''
    then
      begin
        SQLSplitTableName(aTableName,Owner,Table);
        if (Owner<>'') and
           not (fDBMS in [dMSSQL,dPostgreSQL,dMySQL,dFirebird,dDB2,dInformix]) then
          // some DB engines do not expect any schema in the index name
          IndexName := Owner+'.';
        FieldsCSV := RawUTF8ArrayToCSV(aFieldNames,'');
        if length(FieldsCSV)+length(Table)>27 then
          // sounds like if some DB limit the identifier length to 32 chars
          IndexName := IndexName+'INDEX'+
            crc32cUTF8ToHex(Table)+crc32cUTF8ToHex(FieldsCSV) else
          IndexName := IndexName+'NDX'+Table+FieldsCSV;
        end
      else
          IndexName := aIndexName;
  case fDBMS of
    dFirebird: begin
                 rows := Execute (FormatUTF8 ('SELECT RDB$INDEX_ID FROM RDB$INDICES WHERE RDB$INDEX_NAME = %',
                         [uppercase(QuotedStr (IndexName))]), []);
                 result := rows.Step();
                 rows.ReleaseRows;
               end
    else       result := false;
    end;

end;

function TSQLDBZEOSConnectionPropertiesEx.SQLAddIndex(const aTableName: RawUTF8; const aFieldNames: array of RawUTF8; aUnique,
                                                            aDescending: boolean; const aIndexName: RawUTF8): RawUTF8;
begin
  case fDBMS of
    dFirebird : if not IndexExists (aTableName, aFieldNames, aIndexName)
                then result := inherited
                else result := ''
     else       result := inherited
  end;
end;

{ TSQLRestHelper }

function TSQLRestHelper.Delete(rec: TSQLRecord): boolean;
begin
  result := Delete (rec.RecordClass, rec.ID);
end;

function SortDynArraySQLRecordID(const A,B): integer;
begin
  result := TSQLRecord(A).IDValue - TSQLRecord(B).IDValue;
end;

function TSQLRestHelper.TableHasRowsWhere(Table: TSQLRecordClass; Where: RawUTF8): boolean;
var
  rec: TSQLRecord;
begin
  rec := Table.Create;
  Retrieve (Where, rec);
  result := rec.ID <> 0;
  rec.Free;
end;

{ TFunRecordLocker }

constructor TFunRecordLocker.Create;
begin
  inherited Create;
  Records := TList<TLockedRecord>.Create;
  SynLock.Init;
end;

destructor TFunRecordLocker.destroy;
begin
  Records.Free;
  SynLock.Done;
  inherited;
end;

function TFunRecordLocker.FFind(cl: TSQLRecordClass; ID: TID): integer;
var
  i : integer;
begin
  for i := 0 to Records.Count-1 do
    if (Records[i].ID = ID) and (Records[i].SqlRecordClass = cl) then exit (i);
  result := -1;
end;

function TFunRecordLocker.IsLocked(cl: TSQLRecordClass; ID: TID): boolean;
begin
  SynLock.Lock;
  try
    result := FFind(cl, ID) <> -1;
  finally
    SynLock.UnLock;
  end;
end;

function TFunRecordLocker.IsLocked(rec: TSQLRecord): boolean;
begin
  result := IsLocked(rec.RecordClass, rec.IDValue);
end;

function TFunRecordLocker.Lock(rec: TSQLRecord; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean;
begin
  result := Lock (rec.RecordClass, rec.IDValue, UnknownSession);
end;

function TFunRecordLocker.DoLock(cl: TSQLRecordClass; ID: TID; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean;
begin
  SynLock.Lock;
  try
    var index := FFind (cl, ID);
    AlreadyLocked := index > -1;
    if AlreadyLocked
      then
        if SecondsSince(Records[index].TimeStamp) > SqlRecordLockTimeout
          then Records.Delete(index)
          else if Records[index].Session = ServiceContext.Request.Session
            then exit (true) // allow re-lock by same session
            else exit (false);

    Records.Add(TLockedRecord.Create (cl, ID, UnknownSession));
    result := true;
  finally
    SynLock.UnLock;
  end;
end;

function TFunRecordLocker.Lock(cl: TSQLRecordClass; ID: TID; var AlreadyLocked: boolean; UnknownSession: boolean = false): boolean;
begin
  for var i := 1 to 3 do
    begin
      result := DoLock (cl, ID, AlreadyLocked, UnknownSession);
      if result then exit;
      sleep (100);
    end;
end;

function TFunRecordLocker.RenewLocks: integer;
var
  i : integer;
begin
  result := 0;
  SynLock.Lock;
  try
    for i := 0 to Records.Count-1 do
     if Records[i].Session = ServiceContext.Request.Session
       then begin
              Records[i].Renew;
              inc (result);
            end;
  finally
    SynLock.UnLock;
  end;
end;

function TFunRecordLocker.Unlock(rec: TSQLRecord): boolean;
begin
  result := Unlock (rec.RecordClass, rec.ID);
end;

function TFunRecordLocker.Unlock(cl: TSQLRecordClass; ID: TID): boolean;
begin
  SynLock.Lock;
  try
    var index := FFind (cl, ID);
    if index = -1 then
      exit (false);

    result := Records[index].Session = ServiceContext.Request.Session;
    if result
      then Records.Delete(index);
  finally
    SynLock.UnLock;
  end;
end;

{ TFunRecordLocker.TLockedRecord }

constructor TFunRecordLocker.TLockedRecord.Create(ASqlRecordClass: TSQLRecordClass; AID: TID; UnknownSession: boolean = false);
begin
  TimeStamp := now;
  if UnknownSession
     then Session := NULL_ID
     else Session := ServiceContext.Request.Session;
  SqlRecordClass := ASqlRecordClass;
  ID := AID;
end;

procedure TFunRecordLocker.TLockedRecord.Renew;
begin
  TimeStamp := now;
end;

{ TIDGenerator }

constructor TIDGenerator.Create(Server: TSQLRestServerDB; SqlRecordClass: TSQLRecordClass);
begin
  inherited Create;
  FCurrentID := Server.TableMaxID (SqlRecordClass);
end;

function TIDGenerator.NextID: TID;
begin
  result := AtomicIncrement(FCurrentID);
end;

function TSQLRestServerDBEx.InternalAdd(Value: TSQLRecord; SendData: boolean; CustomFields: PSQLFieldBits;
         ForceID, DoNotAutoComputeFields: boolean): TID;
var
  Generator: TIDGenerator;
begin
  if not ForceID then
    if IDGenerators.TryGetValue (Value.RecordClass, Generator) then
      begin
        ForceID := true;
        Value.IDValue := Generator.NextID;
      end;
  result := inherited InternalAdd(Value, SendData, CustomFields, ForceID, DoNotAutoComputeFields);
end;

function TSQLRestServerDBEx.Validate (Value: TSQLRecord): TOutcome;
begin
  var err := Value.Validate;
  if err <>'' then exit (ServerErrorOutcome(seValidationFailed, err));

  result := CheckUniqueFields(Value);
end;

function TSQLRestServerDBEx.Add(Value: TSQLRecord; Batch: TSQLRestBatch): TOutcome;
var
  Generator: TIDGenerator;
begin
  result := Validate (Value);
  if not result.Success then exit;

  var ForceID := false;
  if IDGenerators.TryGetValue (Value.RecordClass, Generator) then
    begin
      ForceID := true;
      Value.IDValue := Generator.NextID;
    end;
  if Batch.Add (Value, true, ForceID) = -1
     then result := ServerErrorOutcome(seBatchFailed)
     else result.SetSuccess;
end;

function TSQLRestServerDBEx.Add(Value: TSQLRecord; SendData: boolean = true; ForceID: boolean=false; DoNotAutoComputeFields: boolean=false): TOutcome;
begin
  result := Validate (Value);
  if not result.Success then exit;

  try
    result.SetID (inherited Add (Value, SendData, ForceID, DoNotAutoComputeFields));
    if result.ID <> NULL_ID
       then Value.IDValue := result.ID
       else exit (ServerErrorOutcome(seAddFailed));
//  except on e: EZSQLException do
  except on e: exception do
    begin
      if pos ('duplicate key', e.Message) > 0
        then exit (ServerErrorOutcome(seDuplicateKey))
        else exit (ServerErrorOutcome(seAddFailed, e.Message));
    end
    else exit (ServerErrorOutcome(seAddFailed, 'WTF'));
  end;
end;

//procedure TSQLRestServerDBEx.AddFunction(Name: RawUTF8);
//var
//  fcode: RawUTF8;
//begin
//  DB.Execute('drop function ' + Name, []);
//  fcode.AsString := TFile.ReadAllText(TPath.Combine(RootPath, Name.AsString + '.sql'));
//  funServerMain.DB.Execute(fcode, []);
//end;

function TSQLRestServerDBEx.Kick(AUserID: TID): boolean;
var
  i: integer;
  sess: TAuthSession;
begin
  result := false;
  fSessions.Safe.Lock;
  try
    for i := fSessions.Count-1 downto 0 do
      begin
        sess := TAuthSession(fSessions.List[i]);
        if (sess.User <> nil) and (sess.User.IDValue = AUserID) then
          SessionDelete(i, ServiceContext.Request);
        result := true;
      end;
  finally
    fSessions.Safe.UnLock;
  end;
end;

function TSQLRestServerDBEx.KickOtherSessions(AUserID: TID; Session: cardinal): boolean;
var
  i: integer;
  sess: TAuthSession;
begin
  result := false;
  fSessions.Safe.Lock;
  try
    for i := fSessions.Count-1 downto 0 do
      begin
        sess := TAuthSession(fSessions.List[i]);
        if (sess.User <> nil) and (sess.User.IDValue = AUserID) and (sess.IDCardinal <> Session) then
          SessionDelete(i, ServiceContext.Request);
        result := true;
      end;
  finally
    fSessions.Safe.UnLock;
  end;
end;

function TSQLRestServerDBEx.AddIDGenerator(Table: TSQLRecordClass): TIDGenerator;
begin
  result := TIDGenerator.Create(self, Table);
  IDGenerators.Add(Table, result);
end;

function TSQLRestServerDBEx.AddMany (Field: TSQLRecordMany; SourceID: TID; IDs: TIDDynArray; Batch: TSQLRestBatch = nil): TOutcome;
begin
  for var DestID in IDs do
    if not Field.ManyAdd(self, SourceID, DestID, false, Batch)
      then exit (ServerErrorOutcome(seDetailTableOperationFailed));
end;

function TSQLRestServerDBEx.ReplaceMany (Field: TSQLRecordMany; SourceID: TID; IDs: TIDDynArray): TOutcome;
var
  DestIDs: TIDDynArray;
begin
  Field.DestGet(self, SourceID, DestIDs);
  for var DestID in DestIDs do
    Field.ManyDelete(self, SourceID, DestID);
  AddMany (Field, SourceID, IDs);
end;

function TSQLRestServerDBEx.BatchSend(Batch: TSQLRestBatch; var Outcome: TOutcome): integer;
begin
  result := inherited BatchSend (Batch);
  if result <> 200
     then Outcome := ServerErrorOutcome(seBatchFailed, result.ToString)
     else Outcome.SetSuccess;
end;

//constructor TSQLRestServerDBEx.Create(AModel: TSQLModel; ADB: TSQLDataBase; AZeosDB: TSQLDBZEOSConnectionPropertiesEx; aHandleUserAuthentication: boolean=false; AOwnDB: boolean=false);
constructor TSQLRestServerDBEx.Create(AModel: TSQLModel; const aDBFileName: TFileName; ADBConnectionProperties: TSQLDBConnectionPropertiesThreadSafe; aHandleUserAuthentication: boolean=false; AOwnDB: boolean=false);
begin
  inherited Create (AModel, aDBFileName, AHandleUserAuthentication);
  DBConnectionProperties := ADBConnectionProperties;
  RecordLocker := TFunRecordLocker.Create;
  IDGenerators := TDictionary <TSQLRecordClass, TIDGenerator>.Create;
  DBChanges := TList<TDBChange>.Create;
end;

destructor TSQLRestServerDBEx.Destroy;
begin
  RecordLocker.Free;
  for var Pair in IDGenerators do
      Pair.Value.Free;
  IDGenerators.Free;
  DBChanges.Free;
  inherited;
end;

function TSQLRestServerDBEx.CreateBatch: TSQLRestBatch;
begin
  result := TSQLRestBatch.Create(self, nil, maxInt, [boRollbackOnError]);
end;

function TSQLRestServerDBEx.Delete(Table: TSQLRecordClass; ID: TID): TOutcome;
var
  lst: TRestrictedCascadeList;
begin
  if Assigned (RestrictedCascades) and RestrictedCascades.TryGetValue(Table, lst) then
    for var rule in lst do
        if RestrictIfTableHasForeignKey(rule.TableClass, rule.ForeignKeyName, ID, result)
           then exit;

  result.Success := inherited Delete (Table, ID);
end;

function TSQLRestServerDBEx.Delete(rec: TSQLRecord): TOutcome;
begin
  result := Delete (rec.RecordClass, rec.IDValue);
end;

procedure TSQLRestServerDBEx.GenerateID(rec: TSqlRecord);
var
  gen: TIDGenerator;
begin
  if IDGenerators.TryGetValue (rec.RecordClass, gen)
    then rec.IDValue := gen.NextID
    else raise Exception.Create('No generator for ' + rec.SQLTableName.AsString);
end;

procedure TSQLRestServerDBEx.CreateIndexes;
begin

end;

function TSQLRestServerDBEx.TableHasForeignKey(Table: TSQLRecordClass; const FieldName: RawUtf8; FKID: TID): boolean;
begin
  var WhereClause := FieldName + ' = ' + Int64ToUtf8 (FKID);
  var id := OneFieldValue(Table, 'ID', WhereClause);
  result := id <> '';
end;

function TSQLRestServerDBEx.Retrieve(Value: TSQLRecord; var Outcome: TOutcome; Lock: boolean = false): boolean;
begin
  if Lock then exit (ReadLock(Value, Outcome));

  if not inherited Retrieve (Value.IDValue, Value)
     then Outcome := ServerErrorOutcome(seRecordNotFound)
     else Outcome.SetID (Value.IDValue);
  result := Outcome.Success;
end;

function TSQLRestServerDBEx.RetrieveListObjArrayEx (var ObjArray; Table: TSQLRecordClass; Parameters: TQueryParameters; const aCustomFieldsCSV: RawUTF8=''): boolean;
begin
  if Parameters.OrderBy = ''
    then Parameters.OrderBy := Table.DefaultSortField;

  result := inherited RetrieveListObjArray (ObjArray, Table, Parameters.MormotSql, []);
end;

function TSQLRestServerDBEx.RetrieveSqlRecords<T>(const FormatSQLWhere: RawUTF8;
  const BoundsSQLWhere: array of const; const Fields: RawUTF8): TSQLRecords<T>;
begin
  result := TSQLRecords<T>.Create;
  inherited RetrieveListObjArray(result.Records, T, FormatSQLWhere, BoundsSQLWhere, Fields);
  result.Init (nil);
end;

function TSQLRestServerDBEx.Retrieve(aID: TID; Value: TSQLRecord; var Outcome: TOutcome; ForUpdate: boolean=false): boolean;
begin
  Value.IDValue := aID;
  exit (Retrieve (Value, Outcome, ForUpdate));
end;


function TSQLRestServerDBEx.ReadLock(rec: TSQLRecord; var Outcome: TOutcome): boolean;
var
  AlreadyLocked: boolean;
  //         Locker.Locked    Mormot.Locked
  // case A: not locked       false            we lock and exit
  // case B: locked by this   true             we just read and exit
  // case C: locked by other  true             we just read and exit

  // case B: locked by same user, locked by mormot, Lock returns true, alreadylocked=true, we just read and exit
  // case C: locked by unknown, locked by mormot, Lock returns true, alreadylocked=true, we just read and exit
begin
  var ID := rec.IDValue;

  if not RecordLocker.Lock (rec, AlreadyLocked) // true if not locked, or locked by same user, or locked but timed-out
     then begin
            Outcome := ServerErrorOutcome(seRecordAlreadyLocked);
            exit (false);
          end;

  // if we get this far, locking was allowed but record could have been already previously locked on mormot level by same user
  // retrive record with mormot-lock only if not alrady locked and exit if successful
  if inherited Retrieve (ID, rec, not AlreadyLocked)
     then begin
            Outcome.SetSuccess;
            exit (true);
          end;

  // we are here because mormot has it already locked, unlock our lock
  RecordLocker.Unlock (rec.RecordClass, ID);

  // failed, so unlock record on mormot level if we are the ones who locked it
  if not AlreadyLocked
     then inherited UnLock(rec.RecordClass, ID);

  //try to see why it failed
  if inherited Retrieve (ID, rec, false)
     then begin
            RecordLocker.Lock (rec.RecordClass, ID, AlreadyLocked, true); // add a lock by unknown so that it can time out in 30 sec
            Outcome := ServerErrorOutcome(seRecordAlreadyLockedByMormot);
            exit (false);
          end
     else begin
            Outcome := ServerErrorOutcome(seRecordNotFound);
            exit (false);
          end;
end;

procedure TSQLRestServerDBEx.RegisterDBChange(const SqlRecordClass: TSqlRecordClass; const ID: TID; const Operation: TCRUD);
var
  dbc: TDBChange;
begin
  exit;

  dbc.SqlRecordClass := SqlRecordClass;
  dbc.ID := ID;
  dbc.Operation := Operation;
  if ServiceContext.Request.IsAssigned
     then dbc.Session := ServiceContext.Request.Session
     else dbc.Session := 0;
  dbc.TimeStamp := now;
  DBChanges.Add(dbc);
  while (DBChanges.Count>0) and (SecondsSince (DBChanges[0].TimeStamp) > 30) do
        DBChanges.Delete(0);
end;

function TSQLRestServerDBEx.RestrictIfTableHasForeignKey(Table: TSQLRecordClass; const FieldName: RawUTF8; FKID: TID; var Outcome: TOutcome): boolean;
begin
  result := TableHasForeignKey (Table, FieldName, FKID);
  if result then Outcome := ServerErrorOutcome(seCascadeRestrict);
end;

procedure TSQLRestServerDBEx.LockAndStartTransaction;
begin
  try
    WriteLn ('LockAndStartTransaction');
    fAcquireExecution[execORMWrite].Safe.Lock;
    if not DBConnectionProperties.MainConnection.Connected
       then DBConnectionProperties.MainConnection.Connect;
    DBConnectionProperties.MainConnection.StartTransaction;
  except
    halt; // if this fails it is too risky to do anything else imo
  end;
end;

// look for unique fields, see if there is already a different record with that value to avoid duplicate key errors
// return first field name that has a value match
function TSQLRestServerDBEx.CheckUniqueFields(rec: TSqlRecord): TOutcome;
var
  ID: TID;
  value: RawUTF8;
  i: integer;
  field: TSQLPropInfo;
begin
  result.SetSuccess;

  for i := 0 to rec.RecordProps.Fields.Count-1 do
  begin
    field := rec.RecordProps.Fields.Items[i];

    if aIsUnique in field.Attributes then
      begin
        value := field.GetValue(rec, true);
        if field.SQLFieldType in STRING_FIELDS then value := '''' + value + '''';

        ID := OneFieldValueInt64(rec.RecordClass, 'rowID',
                                 FormatUTF8 ('(% = %)', [field.Name, value], []), 0);
        if (ID <> NULL_ID) and (ID <> rec.IDValue)
           then exit (ServerErrorOutcome(seDuplicateKey, ValidationFailedText (field.Name.AsString, 'Polje %s mora biti jedinstveno.', rec)));
      end;
  end;
end;


procedure TSQLRestServerDBEx.CommitAndUnlock;
begin
  try
    WriteLn ('CommitAndUnlock');
    DBConnectionProperties.MainConnection.Commit;
    fAcquireExecution[execORMWrite].Safe.UnLock;
  except
    RollbackAndUnlock;
  end;
end;

procedure TSQLRestServerDBEx.RollbackAndUnlock;
begin
  try
    WriteLn ('RollbackAndUnlock');
    DBConnectionProperties.MainConnection.Rollback;
    fAcquireExecution[execORMWrite].Safe.UnLock;
  except
    halt; // if this fails it is too risky to do anything else imo
  end;
end;

procedure TSQLRestServerDBEx.UnLockAndEndTransaction(Success: boolean);
begin
  if Success
    then CommitAndUnlock
    else RollbackAndUnlock;
end;


procedure TSQLRestServerDBEx.Sum(Ctxt: TSQLRestServerURIContext);
begin
  // a test published method
  with Ctxt do
    Results([InputDouble['a']+InputDouble['b']]);
end;

function TSQLRestServerDBEx.UnLock(Table: TSQLRecordClass; aID: TID): boolean;
begin
  result := RecordLocker.UnLock (Table, aID) and
            inherited Unlock (Table, aID);
end;

function TSQLRestServerDBEx.UnLock(rec: TSQLRecord): boolean;
begin
  if rec = nil then exit (true);

  result := RecordLocker.UnLock (rec) and
            inherited Unlock (rec.RecordClass, rec.IDValue);
end;

function TSQLRestServerDBEx.Update(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''; DoNotAutoComputeFields: boolean = false): TOutcome;
begin
  result := Validate (Value);
  if not result.Success then exit;

  result.Success := inherited Update (Value, CustomCSVFields, DoNotAutoComputeFields);

  if Result.Success
     then RegisterDBChange (Value.RecordClass, Value.ID, TCRUD.Update);
end;

function TSQLRestServerDBEx.UpdateWithoutValidation(Value: TSQLRecord; const CustomCSVFields: RawUTF8 = ''; DoNotAutoComputeFields: boolean = false): TOutcome;
begin
  result.Success := inherited Update (Value, CustomCSVFields, DoNotAutoComputeFields);

  if Result.Success
     then RegisterDBChange (Value.RecordClass, Value.ID, TCRUD.Update);
end;


function TSQLRestServerDBEx.UpdateAndUnlock(Value: TSQLRecord; const CustomCSVFields: RawUTF8; DoNotAutoComputeFields: boolean): TOutcome;
begin
  if not RecordLocker.IsLocked (Value)
    then exit (ServerErrorOutcome(seRecordNotLocked));

  result := Update (Value, CustomCSVFields, DoNotAutoComputeFields);

  if result.Success then UnLock(Value);
end;

procedure TSQLRestServerDBEx.Image(Ctxt: TSQLRestServerURIContext);
begin
(*
idk how to use this on client side, replaced with interface which is better anyway

if ImagePath = '' then ImagePath := TPath.Combine (ExtractFilePath (ParamStr(0)), 'Images');

  if ContextUserInfo = nil then
    begin
      Ctxt.Results([], HTTP_FORBIDDEN);
      exit;
    end;

  var FileName := Ctxt.InputString['Name'];
  if FileName.IndexOfAny (['\', '/'{, '.'}]) > 0 then
    begin
      Ctxt.Results([], HTTP_FORBIDDEN);
      exit;
    end;

  FileName := TPath.Combine (ImagePath, FileName);

  if FileExists (FileName)
     then begin
            var blob : TSQLRawBlob;
            var fs := TFileStream.Create(FileName, fmOpenRead);
            blob := StreamToRawByteString(fs);
            fs.Free;
            Ctxt.ReturnFile(FileName);
            //Ctxt.Results ([blob]);
          end
     else Ctxt.Results ([], HTTP_NOTFOUND);
*)
end;

end.
