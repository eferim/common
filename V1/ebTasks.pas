unit ebTasks;

// General Purpose Event Bus Tasks
// Tasks that have virtual Execute methods called within a TTask thread environment
// Upon completion, event is posted on EventBus

interface

uses
  System.Classes, System.Threading, SysUtils, SyncObjs, System.Generics.Collections,
  EventBus.Helpers, LBUtils,
  {$IFDEF FIREMONKEY}FMX.LBUtils{$ELSE}VCL.LBUtils{$ENDIF}
  ;

const
  ecPreviousTaskFailed = -1;
  ecException          = -2;

type

  IebTask = interface
  ['{F231E844-E300-4BF4-92CA-870F1F6A8781}']
    function Outcome: TOutcome;
    function GetOperation: TCRUD;
    function GetContext: string;
    function GetInitiator: TGUID;
    function GetNamedInitiator: INamed;

    procedure SetException (e: exception);
    function AsString: string;
    function WithoutInitiator: boolean;
    function TaskID: cardinal;

    property Operation: TCRUD read GetOperation;
    property Context: string read GetContext;
    property Initiator : TGUID read GetInitiator;
    property NamedInitiator : INamed read GetNamedInitiator;
  end;

  IebBooleanTask  = interface (IebTask) // a boolean event, would require context to determine which one
  ['{0EA03B0A-B27F-4B9D-A71C-1BDA06643AC6}']
    function Value: boolean;
  end;

  TebTask = class (TInterfacedObject, IebTask)
  private
    FInitiator: TGUID;
    FStage: string;
    FSemaphore: TSemaphore;
//    function _AddRef: Integer; stdcall;
    procedure CaptureInterface;
    procedure ReleaseInterface;
    procedure SetInitiator(const Value: TGUID);
    function GetInitiator: TGUID;
    procedure SetStage(const Value: string);
    function GetNamedInitiator: INamed;
    procedure SetNamedInitiator(const Value: INamed);
    function GetNameAndContext: string;
    procedure DoExecute;
  protected
    FOutcome: TOutcome;
//    FOperation: TCRUD;
    FContext: string;
    FTaskID: cardinal;
    Priority: boolean;
    IsCritical: boolean;
    FNamedInitiator: INamed;
    procedure FBeforeExecute; virtual;
    procedure FPrepare; virtual;
    procedure FExecute; virtual;
    procedure FFinish; virtual;
    procedure FRetry; virtual;
    function GetOperation: TCRUD; virtual;
    function GetContext: string;
    function ShouldRetry: boolean; virtual;
    procedure Log (const Action: string; Content: string = ''); virtual;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure Execute;
    procedure Post; virtual;

    {IebTask}
    function TaskID: cardinal;
    function WithoutInitiator: boolean;
    function Outcome: TOutcome;
    function AsString: string; virtual;
    property Initiator : TGUID read GetInitiator write SetInitiator;
    property NamedInitiator : INamed read GetNamedInitiator write SetNamedInitiator;
    procedure SetException (e: exception);
    procedure SetFail (const ErrorCode: integer; const Description: string);
    property Operation: TCRUD read GetOperation;
    property Context: string read GetContext;
    property Stage: string read FStage write SetStage;
    property NameAndContext: string read GetNameAndContext;
    property Semaphore: TSemaphore read FSemaphore;
  end;

  TebTaskClass = class of TebTask;

  IActivityEvent = interface
  ['{071F3A4C-4697-4CED-A950-D8B52F598950}']
    function GetIdle: boolean;
    property Idle: boolean read GetIdle;
  end;

  TActivityEvent = class (TInterfacedObject, IActivityEvent)
  private
    FIdle: boolean;
    function GetIdle: boolean;
  public
    procedure Execute;
  end;

  ITaskFailedEvent = interface
  ['{09B5A68D-986D-4119-BAD9-FBEE7AEB56A9}']
  function TaskClass: TebTaskClass;
  function Outcome: TOutcome;
  end;

  TTaskFailedEvent = class (TInterfacedObject, ITaskFailedEvent)
  protected
    FTaskClass: TebTaskClass;
    FOutcome: TOutcome;
  public
    constructor Create (const ATaskClass: TebTaskClass; const AOutcome: TOutcome); reintroduce;
    function TaskClass: TebTaskClass;
    function Outcome: TOutcome;
  end;

  TTaskSynchronizer = class
  private
    AbortTasksToID, TaskID, LastStartedTaskID, LastCompletedTaskID: cardinal;
    PriorityInProgress: boolean;
    csID: TCriticalSection;
    FIdle: boolean;
    FPendingTasks: TList <integer>;
    FEndingTasks: cardinal;
    FReadyTasks: TDictionary <integer, TebTask>;
//    procedure TaskStarted(const Task: TebTask);
    procedure SetIdle(const Value: boolean);
  public
    PostEvents: boolean;
    function NextTaskID: cardinal;
    function ActiveTasks: boolean;
    function PendingTasks: integer;
    procedure TaskReady(const Task: TebTask);
    procedure TaskCompleted (const Task: TebTask; Priority, Success: boolean);
    procedure TaskRemoved (const ID: cardinal);
    procedure AbortAll;
    procedure ResetID;
    constructor Create;
    destructor Destroy; override; //todo friend class?
    property Idle: boolean read FIdle write SetIdle;
  end;

  TTaskThread = class (TThread)
    Tasks: TList<TebTask>;
    cs: TCriticalSection;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Execute; override;
    procedure Queue (Task: TebTask);
  end;


implementation

uses
  EventBus;

var
  TaskThread : TTaskThread;

{ TEvent }

procedure TebTask.CaptureInterface;
begin
  _AddRef;
end;

constructor TebTask.Create;
begin
  inherited;
  FOutcome.Success := true;
  IsCritical := true;
  FSemaphore := TSemaphore.Create;
end;

destructor TebTask.Destroy;
begin
  Stage := 'Destroy';
  FSemaphore.Free;
  inherited;
end;

procedure TebTask.DoExecute;
begin
  try
    FPrepare;
    if FOutcome.Success
      then FExecute
      else if ShouldRetry
        then FRetry;
    {if not FOutcome.Success
      then FRetry;}
  except on e: exception do
    try
      SetException (e);
      FRetry;
    except
      SetException (e);
    end;
  end;

  FFinish;

  Post;
  ReleaseInterface;
end;

procedure TebTask.Execute;
begin
  CaptureInterface;
  FBeforeExecute;

  if TaskThread.IsAssigned
     then TaskThread.Queue (self)
     else TTask.Create(DoExecute).Start;
end;

procedure TebTask.FBeforeExecute;
begin

end;

procedure TebTask.FExecute;
begin
  Stage := 'Execute';
  sleep (20);
end;

procedure TebTask.FFinish;
begin
  Stage := 'Finish';
end;

procedure TebTask.FPrepare;
begin
  Stage := 'Prepare';
end;

procedure TebTask.FRetry;
begin
end;

function TebTask.GetContext: string;
begin
  result := FContext;
end;

function TebTask.GetInitiator: TGUID;
begin
  result := FInitiator;
end;

function TebTask.GetNameAndContext: string;
begin
  result := copy (ClassName, 2);
  var p := pos ('task', lowercase(result));
  if p = length(result) - 3 then result := copy (result, 1, length(result) - 4);

  if Context <> '' then result := result + '.' + Context;
end;

function TebTask.GetNamedInitiator: INamed;
begin
  result := FNamedInitiator;
end;

function TebTask.GetOperation: TCRUD;
begin
  result := TCRUD.Unknown;
end;

function TebTask.TaskID: cardinal;
begin
  result := FTaskID;
end;

{function TebTask.GetOperation: TCRUD;
begin
  result := FOperation;
end;
}
procedure TebTask.Log (const Action: string; Content: string = '');
begin

end;

function TebTask.Outcome: TOutcome;
begin
  result := FOutcome;
end;

procedure TebTask.Post;
begin
end;

procedure TebTask.ReleaseInterface;
begin
  _Release;
end;

procedure TebTask.SetException(e: exception);
begin
  Log ('SetException', e.Message);
  SetFail(ecException, e.Message);
end;

procedure TebTask.SetFail(const ErrorCode: integer; const Description: string);
begin
  Log ('SetFail', FOutcome.ToString);
  FOutcome.ErrorCode := ErrorCode;
  FOutcome.Success := false;
  FOutcome.Description := Description;
end;

procedure TebTask.SetInitiator(const Value: TGUID);
begin
  FInitiator := Value;
end;


procedure TebTask.SetNamedInitiator(const Value: INamed);
begin
  FNamedInitiator := Value;
end;

procedure TebTask.SetStage(const Value: string);
begin
  FStage := Value;
  Log (Stage);
end;

function TebTask.ShouldRetry: boolean;
begin
  result := false;
end;

function TebTask.WithoutInitiator: boolean;
begin
  result := (NamedInitiator = nil) and (Initiator = TGuid.Empty);
end;

//function TebTask._AddRef: Integer;
//begin
//  result := inherited;
//end;

function TebTask.AsString: string;
begin
  result := Outcome.ToString;
  if Operation <> TCRUD.Unknown
     then result := 'Operation: ' + CRUDToString[Operation] + ' ' + result;
  result := '[' + ClassName + '] ' + result;
end;

{ TTaskSynchronizer }

function TTaskSynchronizer.NextTaskID: cardinal;
begin
  csID.Acquire;
  inc (TaskID);
  result := TaskID;
  FPendingTasks.Add (result);
  csID.Release;
end;

procedure TTaskSynchronizer.SetIdle(const Value: boolean);
begin
  if FIdle = Value then exit;

  FIdle := Value;

  if PostEvents then
    begin
      var AE := TActivityEvent.Create;
      AE.FIdle := FIdle;
      AE.Execute;
    end;
end;


procedure TTaskSynchronizer.TaskReady(const Task: TebTask);
begin
  Task.Semaphore.Acquire;

  csID.Acquire;
  try
    if AbortTasksToID >= Task.TaskID
       then Task.SetFail(ecPreviousTaskFailed, 'Previous critical task failed.');

    if Task.Priority
      then
        begin // priority task has to run only after all previous tasks finished, priority means forward only
          if not PriorityInProgress and (LastCompletedTaskID = Task.TaskID - 1) then
          begin
            PriorityInProgress := true;
            LastStartedTaskID := Task.TaskID;
            Task.Semaphore.Release; // task can run
          end
        end
      else
        if not PriorityInProgress and (LastCompletedTaskID = Task.TaskID - 1)
           //and (FEndingTasks < 10)
           then
          begin
            LastStartedTaskID := Task.TaskID;
            Task.Semaphore.Release; // task can run
          end
      // if we are here, task has to wait
      else
            FReadyTasks.Add (Task.TaskID, Task);
  finally
    Idle := False;
    csID.Release;
  end;

  Task.Semaphore.Acquire;
end;

{
procedure TTaskSynchronizer.TaskCompleted(const Task: TebTask; Priority, Success: boolean);
begin
  while true do
    begin
      try
        csID.Acquire;
        if not Success and Task.IsCritical
          then AbortTasksToID := Task.TaskID;

        if LastCompletedTaskID = Task.TaskID - 1 then // delay completion if its not the expected task ID to enforce sequence
          begin
            if Priority then PriorityInProgress := false;
            LastCompletedTaskID := Task.TaskID;
            Idle := LastCompletedTaskID = LastStartedTaskID;
            inc (FEndingTasks);
            exit;
          end;

      finally
        csID.Release;
      end;

      sleep (1); // todo rework
    end;
end;
}

procedure TTaskSynchronizer.TaskCompleted(const Task: TebTask; Priority, Success: boolean);
var
  sem: TSemaphore;
  NextTask: TebTask;
begin
  try
    csID.Acquire;
    if not Success and Task.IsCritical
      then AbortTasksToID := Task.TaskID;

    LastCompletedTaskID := Task.TaskID;
    if Priority then PriorityInProgress := false; // todo multiple priority tasks?

    sem := nil;

//    while (FSemaphores.Count > 0) and (LastCompletedTaskID <= TaskID)  do // green light for next task
//      begin
        // try to find next task, it may exist but not be registered to wait yet in which case we are idle
        if FReadyTasks.TryGetValue(LastCompletedTaskID + 1, NextTask)
          then begin
                 if AbortTasksToID >= Task.TaskID
                    then NextTask.SetFail(ecPreviousTaskFailed, 'Previous critical task failed.');

                 NextTask.Semaphore.Release;
                 FReadyTasks.Remove (LastCompletedTaskID + 1);
//                 break;
               end;
//          else inc (LastCompletedTaskID);

//        if (LastCompletedTaskID >= TaskID) then
//          if now=0 then exit;

//      end;
    Idle := sem = nil;
    inc (FEndingTasks);
  finally
    csID.Release;
  end;
end;

procedure TTaskSynchronizer.TaskRemoved(const ID: cardinal);
begin
  csID.Acquire;
  FPendingTasks.Remove (ID);
  dec (FEndingTasks);
  csID.Release;
end;

procedure TTaskSynchronizer.AbortAll;
begin
  csID.Acquire;
  AbortTasksToID := TaskID;
  csID.Release;
end;

function TTaskSynchronizer.ActiveTasks: boolean;
begin
  csID.Acquire;
  result := LastStartedTaskID <> LastCompletedTaskID;
  csID.Release;
end;

function TTaskSynchronizer.PendingTasks: integer;
begin
  csID.Acquire;
  result := FPendingTasks.Count;
  csID.Release;
end;

procedure TTaskSynchronizer.ResetID;
begin
  if PendingTasks > 0
     then ShowErrorAndAbort ('Pending tasks exist');

  csID.Acquire;
  TaskID := 0;
  LastStartedTaskID := 0;
  LastCompletedTaskID := 0;
  AbortTasksToID := 0;
  csID.Release;
end;

constructor TTaskSynchronizer.Create;
begin
  inherited;
  FPendingTasks := TList<integer>.Create;
  FReadyTasks := TDictionary <integer, TebTask>.Create;
  csID := TCriticalSection.Create;
  PostEvents := true;
end;

destructor TTaskSynchronizer.Destroy;
begin
  csID.Free;
  FPendingTasks.Free;
  FReadyTasks.Free;
  inherited;
end;

{ TActivityEvent }

procedure TActivityEvent.Execute;
begin
  TTask.Create(
    procedure
    begin
      GlobalEventBus.Post(self as IACtivityEvent);
    end).Start;
end;

function TActivityEvent.GetIdle: boolean;
begin
  result := FIdle;
end;

{ TTaskeFailedEvent }

constructor TTaskFailedEvent.Create(const ATaskClass: TebTaskClass; const AOutcome: TOutcome);
begin
  inherited Create;
  FTaskClass := ATaskClass;
  FOutcome := AOutcome;
  GlobalEventBus.Post(self as ITaskFailedEvent);
end;

function TTaskFailedEvent.Outcome: TOutcome;
begin
  result := FOutcome;
end;

function TTaskFailedEvent.TaskClass: TebTaskClass;
begin
  result := FTaskClass;
end;

{ TTaskThread }

constructor TTaskThread.Create;
begin
  inherited;
  Tasks := TList<TebTask>.Create;
  cs := TCriticalSection.Create;
end;

destructor TTaskThread.Destroy;
begin
  Tasks.Free;
  cs.Free;

  inherited;
end;

procedure TTaskThread.Execute;
var
  t: TebTask;
begin
  inherited;
  t := nil;

  repeat
    begin
      cs.Acquire;
      if Tasks.Count > 0
        then
          begin
            t := Tasks[0];
            Tasks.Delete (0);
          end;
      cs.Release;
      if t.IsAssigned then
        begin
          t.DoExecute;
          t := nil;
        end
        else sleep (1);
    end;
  until Terminated;
end;

procedure TTaskThread.Queue(Task: TebTask);
begin
  cs.Acquire;
  Tasks.Add(Task);
  cs.Release;
end;

initialization
  if FindCmdLineSwitch ('tt')
     then TaskThread := TTaskThread.Create;

finalization
  TaskThread.Free;

end.
