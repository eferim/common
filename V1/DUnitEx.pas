unit DUnitEx;

interface

uses
  SysUtils, Classes, winapi.windows,
  EventBus, ebTasks;

type

  TDUnitTestEx = class(TObject)
  protected
    WaitCount, RemainingCount, SuccessCount: integer;
    procedure Start (Count: integer = 1);
    procedure Wait;
    function FAllSuccess: boolean;
    function FAllFail: boolean;
    procedure EventReceived (AEvent: IebTask);
  public
    LastError: string;
    property AllSuccess: boolean read FAllSuccess;
    property AllFail: boolean read FAllFail;
  end;


implementation

function TDUnitTestEx.FAllFail: boolean;
begin
  result := SuccessCount = 0;
end;

function TDUnitTestEx.FAllSuccess: boolean;
begin
  result := SuccessCount = WaitCount;
end;

procedure TDUnitTestEx.EventReceived (AEvent: IebTask);
begin
  if AEvent.Outcome.Success then
     inc (SuccessCount);
  dec (RemainingCount);
  LastError := AEvent.Outcome.Description;

  if LastError <> '' then
    begin
      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED or FOREGROUND_INTENSITY);
      WriteLn (LastError);
      LastError := '';
    end;

//  if RemainingCount = 0 then
  //  Assert.IsTrue (AllSuccess); would be nice but what if we want to fail...
end;

procedure TDUnitTestEx.Start(Count: integer = 1);
begin
  WaitCount := Count;
  RemainingCount := WaitCount;
  SuccessCount := 0;
end;

procedure TDUnitTestEx.Wait;
begin
  while RemainingCount > 0 do
    begin
      sleep (1);
      CheckSynchronize;
    end;
end;

end.
