﻿unit mormotMods;

{
Info: Encryption happens in TWebSocketProcess.SendFrame
actual bytes sent done in TCrtSocket.TrySndLow
}

interface

uses
  SysUtils, Contnrs, System.Generics.Collections, Classes, System.Types, System.Generics.Defaults,
  Variants, StrUtils, math, DateUtils, System.TypInfo, RTTI, mORMotHttpClient, SynCrtSock,
  {$ifdef MSWINDOWS}
  Windows, //for inline of synlocker...
  {$endif}

  SynDBZeos, SynCommons, SynDB, mormot, SynTable, LBUtils;

const
  NULL_ID = 0;

type
  TServerError = integer;

  TServerWarning = (MoreRecordsAvailable);

  TIDHelper = record helper for TID
    function ToRawUTF8: RawUTF8; inline;
    function ToString: string; inline;
    function ToPointer: pointer; inline;
    function IsNULL: boolean; inline;
    function IsNotNULL: boolean; inline; // cap?
    procedure Clear; inline;
//    procedure SetNull;??
  end;

  TQueryPage = record
    Offset, Limit: uint64;
  end;

  FKAttribute = class (TCustomAttribute)
    SqlRecordClass: TSQLRecordClass;
    constructor Create (ASqlRecordClass: TSQLRecordClass);
  end;

  FKRestrictedAttribute = class(FKAttribute)
  end;

  GeneralFilterAttribute = class(TCustomAttribute)
  end;

  DefaultSortFieldAttribute = class(TCustomAttribute)
  end;

  UniqueIfNotNullAttribute = class(TCustomAttribute)
  end;

  DisplayableAttribute = class(TCustomAttribute)
  public
    DisplayName: string;
    constructor Create (const ADisplayName: string = '');
  end;

  TFilterOperator = (Like, Less, LessOrEqual, Equal, GreaterOrEqual, Greater, NotEqual, InValues, IsNull, IsNotNull);

  TFilterOperatorDynArray = TArray <TFilterOperator>;

  TQueryFilter = record
  private
    function GetCount: integer;
    procedure SetCount(const Value: integer);
  public
    //Mandatories: boolean;
    FieldNames: TRawUTF8DynArray;
    Operators: TFilterOperatorDynArray;
    Values: TVariantDynArray; // varobject so that we can set it to another field ie OnStock < Quantity
    function ReplaceField (const FromField, ToField: RawUTF8): integer;
    function IndexOfField (const FieldName: RawUTF8): integer;
    function ContainsField (const FieldName: RawUTF8): boolean;
    function RemoveField (const FieldName: RawUTF8): integer;
    function Fieldvalue (const FieldName: RawUTF8): variant;
    function SQL: RawUTF8;
    procedure AddFrom (Filter: TQueryFilter);
    procedure Add (const FieldName: RawUTF8; const FilterOperator: TFilterOperator; const AValue: variant); overload;
    procedure Add(const FieldName: RawUTF8; const FilterOperator: TFilterOperator; const AValues: TIDDynArray); overload;
    procedure AddIfValid (const FieldName: RawUTF8; const FilterOperator: TFilterOperator; AValue: variant);
    procedure Clear;
    property Count: integer read GetCount write SetCount;
  end;

  TQueryParameters   = record
    Operation: TCRUD;
    Page: TQueryPage;
    Filter: TQueryFilter;
    Where, OrderBy,
    MasterTableName, // used for lookups when we only want lookup info for things that are referenced as FK
    GeneralFilter, // filter that should be applied as "like %" to all applicable fields, with OR
    IDSourceFieldName: RawUTF8; // e.g. foreign key field name that held the ID value for lookup
    ID, MasterID: TID;
    GUID: TGUID;
    // just the where condition, without "WHERE"
    function WhereConditionSql: RawUTF8;
    // full ORDER by statement including ORDER BY
    function OrderByStatementSql: RawUTF8;
    // full WHERE + ORDER BY statement
    function FullStatementSql: RawUTF8;
    // what mormot wants, where without "where" but with order by if this has order by
    function MormotSql: RawUTF8;

    function LimitSql: RawUTF8;

    procedure AddMandatoryCondition (const ACondition: RawUTF8);
    procedure AddOptionalCondition (const ACondition: RawUTF8);
    procedure ApplyGeneralFilter (FieldNames: array of RawUTF8; AOperator: RawUTF8);
    //todomaybe this causes weirdness, args come up messed up
    //procedure AddMandatoryFormat (const AFormat: RawUTF8; const Args: array of const);
    procedure AddOrderIfBlank (const OrderFields: RawUTF8);
    class procedure AddCondition(var CurrentFilter: RawUTF8; const NewCondition: RawUTF8; const Mandatory: boolean); static;
    constructor Create (AWhere, AOrderBy: RawUTF8); overload;
    constructor Create (const AMasterTableName: RawUTF8; const AMasterID : TID); overload;
    constructor Create (const AMasterID : TID); overload;
    constructor Create (AOperation: TCRUD; AID: TID = NULL_ID); overload;
    function ToString: string;
  end;

  // Meant to be dictionary containing arrays of all foreign key IDs of x-to-many relationship
  // e.g. User record has IndexDictionary['Warehouses'] = [ID1, ID2, ID3]
  TIDDictionary = class (TDictionary <string, TIDDynArray>)
    public
      function CreateCopy: TIDDictionary;
  end;

  IMormotInterface = IINvokable;

  TSQLRecordList = class;

  TSQLRecordLists = class (TObjectList<TSQLRecordList>)
    function FindListForTable (Table: TSQLRecordClass): TSQLRecordList; overload;
    function FindListForTable (Table: string): TSQLRecordList; overload;
  end;

  TSQLRecordListFields = array of RawUTF8;

  // Za laksi rad sa listom recorda, verovatno sluzi samo za ORM access pa mozda nece biti preterano korisna
  TSQLRecordList = class
  private
    function GetItems(index: integer): TSQLRecord;
  public
    ObjectList: TObjectList{<TSQLRecord>};
    Fields: TSQLRecordListFields;
    Table: TSQLRecordClass;
    constructor Create (ATable: TSQLRecordClass; const AFields: TSQLRecordListFields);
    destructor Destroy; override;
    function IndexOfID (ID: TID): integer;
    function ItemWithID (ID: TID): TSQLRecord;
    property Items [index: integer]: TSQLRecord read GetItems;
  end;

  TSQLRecordHelper = class helper for TSQLRecord
  private
    function GetAsTID: TID;
  public
    function GetFieldValue        (const PropName: RawUTF8): RawUTF8; reintroduce;
    function GetFieldStringValue  (const PropName: RawUTF8): string; overload;
    function GetFieldStringValue  (const PropName: string): string;  overload;
    function GetFieldVariantValue (const PropName: string): variant;  overload;
    function GetLookupClass       (const AFieldName: string; AuthUserClass: TSQLRecordClass): TSQLRecordClass;

    class function DisplayName: string;
    class function FieldDisplayName (const FieldName: string): string;
    class function DefaultSortField: RawUTF8;
    class function GeneralFilterFields: TArray<RawUTF8>;

    function FillPrepareWithOrder(aClient: TSQLRest; const aIDs: array of Int64; const OrderBy: RawUTF8 = ''; const aCustomFieldsCSV: RawUTF8=''): boolean;
    procedure LoadFromISQLDBRows (isql: ISQLDBRows; const Prefix: RawUTF8);

    procedure PlaceCopyInto (var p: pointer);
    function Validate: string;
    function ContainsUpdatedFields (Original: TSqlRecord): boolean;

    property AsTID: TID read GetAsTID;
//    function ToVariant: variant;
  end;

  TRawUTF8Helper = record helper for RawUTF8
  private
    function GetAsString: string;
    procedure SetAsString(const Value: string);
    function GetAsAnsiString: AnsiString;
    function GetAsBoolean: boolean;
    procedure SetAsBoolean(const Value: boolean);
  public
    property AsString: string read GetAsString write SetAsString;
    property AsAnsiString: AnsiString read GetAsAnsiString;
    property AsBoolean: boolean read GetAsBoolean write SetAsBoolean;
  end;

  TSQLAuthUserHelper = class helper for TSQLAuthUser
  private
    function GetGroupRightsID: TID;
    procedure SetGroupRightsID(const Value: TID);
  public
    property GroupRightsID: TID read GetGroupRightsID write SetGroupRightsID;
  end;

  TSqlRecordArray = array of TSqlRecord;

  TServiceFactoryHelper = class helper for TServiceFactory
  public
    procedure GetOrCrash (out Obj);
  end;

  TServiceContainerHelper = class helper for TServiceContainer
  private
    function HelperGetService(const aURI: RawUTF8): TServiceFactory;
  public
    property Services[const aURI: RawUTF8]: TServiceFactory read HelperGetService; default;
  end;

  TDynArrayHelper = record helper for TDynArray
    function Contains (const Elem): boolean;
  end;

  TRestrictedCascade = record
    TableClass: TSQLRecordClass;
    ForeignKeyName: RawUTF8;
  end;

  TRestrictedCascadeList = TList<TRestrictedCascade>;

  TRestrictedCascades = class (TObjectDictionary <TSqlRecordClass, TRestrictedCascadeList>)
  public
    procedure Add (MasterClass, DetailClass: TSqlRecordClass; ForeignKeyName: RawUTF8 = '');
  end;

  TSQLModelEx = class (TSqlModel)
  private
    function GetTitle(cl: TSqlRecordClass): string;
  protected
    FTableTitles: TDictionary <TSqlRecordClass, string>;
  public
    RestrictedCascades : TRestrictedCascades;
    constructor Create(const Tables: array of TSQLRecordClass; const aRoot: RawUTF8='root');
    destructor Destroy; override;
    property TableTitles [cl: TSqlRecordClass]: string read GetTitle;
  end;

  TSQLRecordsRecord = class (TSQLRecord)
    Records: TObject;
    destructor Destroy; override;
  end;

  // Wrapper for mormot client class
  TSQLHttpClientWebsocketsEx = class (TSQLHttpClientWebsockets)  // cant use default TSQLHttpClient(http.sys) because callbacks are not supported
  private
    procedure SetReceiveTimeout(const Value: cardinal);
  protected
    FUpgradedToSockets: boolean;
    FWebSocketsEncryptionKey, FUsername, FPassword: string;
    LastRelogin: TDateTime;
  public
    Model: TSQLModel; // TODO there is already a model in inherited but unsure if it can be accessed safely in constructor
    UseCallbacks: boolean;
    constructor Create (AModel: TSQLModel; const Address, Port: string; AWebSocketsEncryptionKey: string); virtual;

    procedure RegisterServices; virtual;

    function ServiceMustRegister(const aInterfaces: array of PTypeInfo;
                                       aInstanceCreation: TServiceInstanceImplementation=sicSingle;
                                       const aContractExpected: RawUTF8=''): boolean;
    procedure UpgradeToSockets;
    function SetUser(const AUserName, APassword: string; aHashedPassword: Boolean=false): boolean; reintroduce;
    function ReLogin: boolean; virtual;
  public
    property UpgradedToSockets: boolean read FUpgradedToSockets;
    property ReceiveTimeout: cardinal read FReceiveTimeout write SetReceiveTimeout;
  end;

  procedure RegisterJSONSerializerClasses (const Classes: array of TClass);

  function ServerErrorOutcome(const error: TServerError; const FurtherDescription: string = ''): TOutcome;
  function RegisterServerError (sMessage: string): TServerError;

var
  FilterOperatorAsSQL : array [TFilterOperator] of string = ({$IFDEF FIREBIRD}'LIKE'{$ELSE}'ILIKE'{$ENDIF},
                                                            '<', '<=', '=', '>=', '>', '<>', 'IN', 'IS NULL', 'IS NOT NULL');

  slServerErrorMessages: TStringList;
  seNoError,
  seRecordAlreadyLocked, seCascadeRestrict, seRecordNotFound, seUnknownTable,
  seNoRecordAccess, seBatchFailed,
  seRecordNotLocked, seRecordAlreadyLockedByMormot, seRecordChangeNotAllowed,
  seNotImplemented, seDetailTableOperationFailed, seException, seWrongCRUD, seNotRecordOwner, seDetailsMissing,
  seValidationFailed, seAddFailed, seInvalidParameters, seRecordValueNotFound,
  seFileNotFound, seDuplicateKey, seDBFunctionFailed: TServerError;

implementation

uses Validation;

var
  SQLRecordLists: TSQLRecordLists;
  FServerError: TServerError;
  dicDefaultSortFields: TDictionary <TSQLRecordClass, RawUTF8>;
  dicGeneralFilters : TDictionary <TSQLRecordClass, TArray<RawUTF8>>;

function EnsureBracketed (const s: RawUTF8): RawUTF8;
begin
  if (s = '')
     then Exit (s);

  var ln := length(s);

  if (s[1] = '(') and (s[ln]=')')
    then
      begin
        var i := 2;
        var OpenCount := 1;

        while i < ln - 1 do // go until the penultimate char
          begin
            if s[ln] = '('
               then inc (OpenCount)
            else if s[ln] = ')'
               then dec (OpenCount);
            if OpenCount = 0 then break;
          end;

        if OpenCount = 1
          then exit (s)
          else exit ('('+s+')');
      end
    else
        exit ('('+s+')');
end;

function EncodeForLike(const Value: string): string;
const
  EscChar = '\';
begin
  // Replace backslash first to avoid double-escaping
  Result := StringReplace(Value, EscChar, EscChar + EscChar, [rfReplaceAll]);
  // Replace percent and underscore
  Result := StringReplace(Result, '%', EscChar + '%', [rfReplaceAll]);
  Result := StringReplace(Result, '_', EscChar + '_', [rfReplaceAll]);
end;

function TQueryFilter.SQL: RawUTF8;
var
  i, j: integer;
  value : variant;
begin
  result := '';
  for i := 0 to Count - 1 do
    begin
      if i > 0 then result := result + ' AND ';
      value := Values[i];
      if VarType (value) = varBoolean
        then value := integer (value);

      case Operators[i] of
        TFilterOperator.Like:
          begin
            if VarIsStr(Values[i])
               then begin
                      var tempValue : string := Values[i];
                      //tempValue := EncodeForLike (tempValue);
                      // NB ^ ovo bi "pokvarilo" trenutni rad
                      // mozda bi trebalo napraviti easy like i strict like pa u strict da korisnik kuca wildcards obavezno a u easy nikad
                      result := result + FormatUTF8 ('(% % ''%%%''', [FieldNames[i], FilterOperatorAsSQL[Operators[i]], '%', tempValue, '%'])
                    end
               else result := result + FormatUTF8 ('(% % ''%''', [FieldNames[i], FilterOperatorAsSQL[Operators[i]], Values[i]]);
            {$IFDEF FIREBIRD}
            result := result + ' COLLATE UNICODE_CI';
            {$ENDIF}

            result := result + ')'
          end;
        TFilterOperator.InValues:
          begin
            result := result + FormatUTF8 ('(% IN (', [FieldNames[i]]);
            for j := 0 to TDocVariantData (Values[i]).Count-1 do
              begin
                if j > 0 then result := result + ', ';
                result := result + StringToUTF8 (TDocVariantData (Values[i])[j]);
              end;
            result := result + ') )';
          end;
        TFilterOperator.IsNull, TFilterOperator.IsNotNull:
          result := result + FormatUTF8 ('(% %)', [FieldNames[i], FilterOperatorAsSQL[Operators[i]]]);
        else
          if VarIsStr(Values[i])
            then result := result + FormatUTF8 ('(% % %)', [FieldNames[i], FilterOperatorAsSQL[Operators[i]], QuotedStr(Values[i])])
            else result := result + FormatUTF8 ('(% % %)', [FieldNames[i], FilterOperatorAsSQL[Operators[i]], Values[i]]);
      end;
    end;
end;

function OrderByClause (OrderBy: RawUTF8): RawUTF8;
begin
  if OrderBy = ''
     then result := ''
     else result := ' ORDER BY ' + OrderBy;
end;

{ TSQLRecordList }

constructor TSQLRecordList.Create (ATable: TSQLRecordClass; const AFields: TSQLRecordListFields);
begin
  inherited Create;
  SQLRecordLists.Add (self);
  Table := ATable;
  Fields := AFields;
end;

destructor TSQLRecordList.Destroy;
begin
  ObjectList.Free;
  SQLRecordLists.Remove(self);
  inherited;
end;

function TSQLRecordList.GetItems(index: integer): TSQLRecord;
begin
  result := TSQLRecord(ObjectList[index]);
end;

function TSQLRecordList.IndexOfID(ID: TID): integer;
var
  i : integer;
begin
  if ObjectList = nil then exit (-1);

  for i := 0 to ObjectList.Count-1 do
    if TSQLRecord(ObjectList[i]).ID = ID then exit (i);
  result := -1;
end;

function TSQLRecordList.ItemWithID(ID: TID): TSQLRecord;
begin
  if ObjectList = nil then exit (nil);
  for result in ObjectList do
    if TSQLRecord(result).ID = ID then exit;
  result := nil;
end;

function TSQLRecordHelper.GetFieldStringValue(const PropName: RawUTF8): string;
begin
  result := UTF8ToString (GetFieldValue (PropName));
end;

class function TSQLRecordHelper.DefaultSortField: RawUTF8;
begin
  if not dicDefaultSortFields.TryGetValue(self, result) then
    begin
      var ctx := TRttiContext.Create;
      try
        var TableType := ctx.GetType(self);
        for var prop in TableType.GetProperties do
          for var Attribute in prop.GetAttributes do
            if Attribute is DefaultSortFieldAttribute then
            begin
              dicDefaultSortFields.Add(self, StringToUTF8 (prop.Name));
              exit (StringToUTF8 (prop.Name))
            end;
        dicDefaultSortFields.Add(self, '');
        result := '';
      finally
        ctx.Free;
      end;
    end;
end;

class function TSQLRecordHelper.DisplayName: string;
begin

  var ctx := TRttiContext.Create;
  try
    var TableType := ctx.GetType(self);
    for var Attribute in TableType.GetAttributes do
        if Attribute is DisplayableAttribute then
           exit ((Attribute as DisplayableAttribute).DisplayName)
  finally
    ctx.Free;
  end;

  result := SQLTableName.AsString;
end;

class function TSQLRecordHelper.FieldDisplayName(const FieldName: string): string;
begin
  result := {self.SQLTableName + '.' + }FieldName;
end;

const INLINED_MAX = 10;

function TSQLRecordHelper.FillPrepareWithOrder(aClient: TSQLRest; const aIDs: array of Int64; const OrderBy, aCustomFieldsCSV: RawUTF8): boolean;
begin
  if high(aIDs)<0 then
    result := false else
    result := FillPrepare(aClient,SelectInClause('id',aIDs,'',INLINED_MAX) + OrderByClause (OrderBy),aCustomFieldsCSV);
end;

class function TSQLRecordHelper.GeneralFilterFields: TArray<RawUTF8>;
begin
  if not dicGeneralFilters.TryGetValue(self, result) then
    begin
      var ctx := TRttiContext.Create;
      try
        var TableType := ctx.GetType(self);
        for var prop in TableType.GetProperties do
          for var Attribute in prop.GetAttributes do
            if Attribute is GeneralFilterAttribute then
            begin
              setlength  (result, length(result) + 1);
              result [length(result) - 1].AsString := prop.Name;
            end;

        dicGeneralFilters.Add(self, result);
      finally
        ctx.Free;
      end;
    end;
end;

function TSQLRecordHelper.GetAsTID: TID;
begin
  result := TID(self);
end;

function TSQLRecordHelper.GetFieldStringValue(const PropName: string): string;
begin
  result := UTF8ToString (GetFieldValue (StringToUTF8(PropName)));
end;

function TSQLRecordHelper.GetFieldVariantValue(const PropName: string): variant;
begin
//  if PropName.ToUpper = 'LETTERDATE'
//    then if now=0 then exit;

  if PropName.ToUpper = 'ID'
     then result := ID
     else
    begin
      var P := RecordProps.Fields.ByRawUTF8Name(StringToUTF8(PropName));
      if P=nil
        then result := self.GetFieldVariant (PropName)// VarClear(result)
        else begin

               case P.SQLFieldType of
                 sftDateTime{, sftDate}:
                               begin
                                 P.GetVariant(self,result);

                                if result = 0
                                  then varClear (result);
                               end;
                 sftCreateTime,
                 sftModTime:    begin
                                  P.GetVariant(self,result);
                                  if result = 0
                                    then varClear (result)
                                    else result := TimeLogToDateTime (result);
                                end;
                 sftUTF8Text:   begin
                                  var r: RawUTF8;
                                  P.GetValueVar(self,true, r, nil);

                                  RawUTF8ToVariant(r, TVarData(result), varUString);
                                  //VarType()
                                  //result := r;
                                end;
                 else P.GetVariant(self,result);
               end;
             end;
    end;
end;

function TSQLRecordHelper.GetLookupClass(const AFieldName: string; AuthUserClass: TSQLRecordClass): TSQLRecordClass;
begin
  var P := RecordProps.Fields.ByRawUTF8Name(StringToUTF8(AFieldName));
  if P = nil then exit (nil);

  case P.SQLFieldType of
    sftSessionUserID: result := AuthUserClass;
    sftTID: result := TSQLPropInfoRTTITID (P).RecordClass;
    else result := nil;
  end;
end;

procedure TSQLRecordHelper.LoadFromISQLDBRows(isql: ISQLDBRows; const Prefix: RawUTF8);
var
  iField, iColumn : integer;
  FieldName: RawUTF8;
begin
  for iField := 0 to RecordProps.Fields.Count - 1 do
    begin
      var Field := RecordProps.Fields.Items[iField];
      FieldName := Field.Name;
      iColumn := isql.ColumnIndex (Prefix + FieldName);
      if iColumn <> - 1
         then begin
                case Field.SQLFieldType of
                  sftBlob, sftBlobDynArray, sftBlobCustom :
                       begin
//                         var u := isql.ColumnUTF8 (iColumn);
  //                       Field.SetValue (self, @u, true);
                         //Field.SetBinary(self, @u, pointer (cardinal (@u) + length (u) - 1) );

                         var v := isql.ColumnVariant(iColumn);
                         Field.SetVariant(self, v);
                       end;
                  sftUTF8Text:
                    begin
                      var v := isql.ColumnVariant(iColumn);
                      if VarIsNull(v)
                        then v := '';
                      Field.SetVariant(self, v);
                    end
                  else begin
                         var v := isql.ColumnVariant(iColumn);
                         Field.SetVariant(self, v);
                       end;
                end;

              end;
    end;
end;

function TSQLRecordHelper.ContainsUpdatedFields (Original: TSqlRecord): boolean;
var
  iField : integer;
  FieldName: RawUTF8;
//  ft : TSQLFieldType;
  Field: TSQLPropInfo;
begin
  for iField := 0 to RecordProps.Fields.Count - 1 do
    begin
      Field := RecordProps.Fields.Items[iField];
      //var OriginalField := Original.RecordProps.Fields.Items[iField];
      if Field.GetValue (self, false) <> Field.GetValue (Original, false) then
        begin
          FieldName := Field.Name;
          //ft := Field.SQLFieldType;
          case Field.SQLFieldType of
            TSQLFieldType.sftTID, TSQLFieldType.sftID:
              if Original.GetFieldVariantValue(FieldName.AsString) <> NULL_ID
                then exit (true);
            TSQLFieldType.sftUTF8Text:
              if Field.GetValue(Original, false) <> ''
                then exit (true);
            else exit (true);
           end; //case
        end;
    end;
  result := false;
end;

procedure TSQLRecordHelper.PlaceCopyInto(var p: pointer);
begin
  p := self.CreateCopy;
end;

function TSQLRecordHelper.Validate: string;
begin
  result := ValidateWithAttributes (self);
end;

function TSQLRecordHelper.GetFieldValue(const PropName: RawUTF8): RawUTF8;
var
  left, right: RawUTF8;
  PropInfo: TSQLPropInfo;
  list: TSQLRecordList;
  t: string;
begin
  var p := pos ('.', PropName.AsString);
  if p > 0
    then
      begin
        Split (PropName, '.', left, right);
        PropInfo := RecordProps.Fields.ByName(pointer(left));
        if PropInfo<>nil then
          begin
            t := PropInfo.SQLFieldRTTITypeName.AsString;
            var tbdpos := pos ('ToBeDeletedID', t);
            if tbdpos > 0 then
              t := t.Remove(tbdpos - 1);
            list := SQLRecordLists.FindListForTable(t);
            if list = nil
               then result.AsString := t
               else begin
                      var rec := list.ItemWithID(GetFieldVariant(left.AsString));
                      if rec = nil
                         then result := ''
                         else result := rec.GetFieldValue(right);
                    end;
          end;
      end
    else
      if PropName = 'ID'
        then result := StringToUTF8 (IntToStr(ID))
        else result := inherited GetFieldValue(PropName);
end;

{ TSQLRecordLists }

function TSQLRecordLists.FindListForTable(Table: TSQLRecordClass): TSQLRecordList;
begin
  for result in self do
    if result.Table = Table then exit;
  result := nil;
end;

function TSQLRecordLists.FindListForTable(Table: string): TSQLRecordList;
begin
  for result in self do
    if result.Table.ClassName = Table then exit;
  result := nil;
end;

{ TRawUTF8Helper }

function TRawUTF8Helper.GetAsAnsiString: AnsiString;
begin
  result := AnsiString (UTF8ToString(self));
end;

function TRawUTF8Helper.GetAsBoolean: boolean;
begin
  result := self = '1';
end;

function TRawUTF8Helper.GetAsString: string;
begin
  result := UTF8ToString(self)
end;

procedure TRawUTF8Helper.SetAsBoolean(const Value: boolean);
begin
  if Value
     then self := '1'
     else self := '0';
end;

procedure TRawUTF8Helper.SetAsString(const Value: string);
begin
  self := StringToUTF8 (Value);
end;

{ TSQLAuthUserHelper }

function TSQLAuthUserHelper.GetGroupRightsID: TID;
begin
  result := TID (GroupRights);
end;

procedure TSQLAuthUserHelper.SetGroupRightsID(const Value: TID);
begin
  GroupRights := TSQLAuthGroup (Value);
end;

procedure RegisterJSONSerializerClasses (const Classes: array of TClass);
begin
  for var ClassItem in Classes do
    TJSONSerializer.RegisterClassForJSON(ClassItem);
end;

{ TServiceFactoryHelper }

procedure TServiceFactoryHelper.GetOrCrash(out Obj);
begin
  if not Get (Obj)
     then raise Exception.Create('Service not defined');
end;

{ TServiceContainerHelper }

function TServiceContainerHelper.HelperGetService(const aURI: RawUTF8): TServiceFactory;
begin
  result := GetService(aURI);
  if result = nil
    then raise Exception.Create('Service ' + aURI.AsString + ' not registered');
end;

{ TQueryDetails }

procedure TQueryParameters.AddMandatoryCondition(const ACondition: RawUTF8);
begin
  AddCondition(Where, ACondition, true);
end;

procedure TQueryParameters.AddOptionalCondition (const ACondition: RawUTF8);
begin
  AddCondition(Where, ACondition, false);
end;

//procedure TQueryParameters.AddMandatoryFormat (const AFormat: RawUTF8; const Args: array of const);
//begin
//  AddMandatoryCondition(Where, FormatUTF8(AFormat, Args));
//end;

procedure TQueryParameters.AddOrderIfBlank(const OrderFields: RawUTF8);
begin
  if OrderBy = '' then OrderBy := OrderFields;
end;

procedure TQueryParameters.ApplyGeneralFilter(FieldNames: array of RawUTF8; AOperator: RawUTF8);
var
  Filters: TList<RawUTF8>;
  filter, Left, Right, gf, EscapedGF: RawUTF8;
begin
  Filters := TList<RawUTF8>.Create;
  gf := GeneralFilter;

  while gf<>'' do
    begin
      filter := '';
      Split(gf, ' ', Left, Right);
      gf := trim (Right);
      EscapedGF := QuotedStr ('%' + left + '%');
      for var fn in FieldNames do
        TQueryParameters.AddCondition(filter, FormatUTF8('% % %', [fn, AOperator, EscapedGF]), false);
      Filters.Add (filter);
    end;

  filter := '';
  for var f in Filters do
    AddCondition(filter, f, true);

  AddMandatoryCondition(filter);
  Filters.Free;
end;

constructor TQueryParameters.Create(AOperation: TCRUD; AID: TID);
begin
  self := default (TQueryParameters);
  inherited;
  Operation := AOperation;
  ID := AID;
end;

constructor TQueryParameters.Create(const AMasterTableName: RawUTF8; const AMasterID: TID);
begin
  self := default (TQueryParameters);
  inherited;
  MasterID := AMasterID;
  MasterTableName := AMasterTableName;
end;

constructor TQueryParameters.Create(const AMasterID: TID);
begin
  self := default (TQueryParameters);
  inherited;
  MasterID := AMasterID;
end;

constructor TQueryParameters.Create(AWhere, AOrderBy: RawUTF8);
begin
  self := default (TQueryParameters);
  inherited;
  Where := AWhere;
  OrderBy := AOrderBy;
end;

class procedure TQueryParameters.AddCondition(var CurrentFilter: RawUTF8; const NewCondition: RawUTF8; const Mandatory: boolean);
const
  AndOr: array [false..true] of RawUTF8 = (' OR ', ' AND ');
begin
  if (CurrentFilter <> '')
    then CurrentFilter := EnsureBracketed (CurrentFilter) + AndOr[Mandatory] + EnsureBracketed (NewCondition)
    else CurrentFilter := NewCondition;
end;

function TQueryParameters.WhereConditionSql: RawUTF8;
begin
  var tempWhere := Where;
  var FilterSQL := Filter.SQL;
  if FilterSql<> ''
    then AddCondition(tempWhere, FilterSQL, true);
  if tempWhere <> '' then result := tempWhere + sLineBreak + sLineBreak else result := '';
end;

function TQueryParameters.OrderByStatementSql: RawUTF8;
begin
  if OrderBy <> ''
    then result := ' ORDER BY ' + OrderBy
    else result := '';
end;

function TQueryParameters.ToString: string;
begin
//  result := GetEnumName(GetTypeData(TypeInfo (TCRUD), Operation);
  if Operation <> TCRUD.Unknown
     then result := 'CRUD: ' + System.TypInfo.GetEnumName(System.TypeInfo(TCRUD), integer(Operation))
     else result := '';
  if ID <> NULL_ID
     then result := result + sLineBreak + 'ID = ' + inttostr (ID) + sLineBreak;
  if MasterTableName <> ''
     then result := result + sLineBreak + 'MasterTableName = ' + MasterTableName.AsString + sLineBreak;
  if MasterID <> NULL_ID
     then result := result + sLineBreak + 'MasterID = ' + inttostr (MasterID) + sLineBreak;
  if GeneralFilter <> ''
     then result := result + sLineBreak + 'Gen.Filter = ' + GeneralFilter.AsString;

  result := result + sLineBreak + FullStatementSql.AsString;
end;

function TQueryParameters.FullStatementSql: RawUTF8;
begin
  var tempWhere := WhereConditionSql;
  if tempWhere <> ''
     then result := 'WHERE ' + tempWhere + sLineBreak + sLineBreak else result := '';
  result := result + sLineBreak + OrderByStatementSql + sLineBreak;
end;

function TQueryParameters.LimitSql: RawUTF8;
begin
  if Page.Limit + Page.Offset > 0
    then result := FormatUTF8('LIMIT % OFFSET %' + sLineBreak, [Page.Limit, Page.Offset])
    else result := '';
end;

function TQueryParameters.MormotSql: RawUTF8;
begin
  result := trim (WhereConditionSql + sLineBreak + OrderByStatementSql);
  if result <> '' then result := result + sLineBreak;
end;

{ TIDDictionary }

function TIDDictionary.CreateCopy: TIDDictionary;
begin
  if self = nil then exit (nil);

  result := TIDDictionary.Create;
  for var key in self.Keys do
    result.Add(key, self[key]);
end;

{ TDynArrayHelper }

function TDynArrayHelper.Contains(const Elem): boolean;
begin
  result := IndexOf (Elem) > -1;
end;

{ TIDHelper }

procedure TIDHelper.Clear;
begin
  self := NULL_ID;
end;

function TIDHelper.IsNotNull: boolean;
begin
  result := self <> NULL_ID;
end;

function TIDHelper.IsNull: boolean;
begin
  result := self = NULL_ID;
end;

function TIDHelper.ToPointer: pointer;
begin
  result := pointer (self);
end;

function TIDHelper.ToRawUTF8: RawUTF8;
begin
  result := Int64ToUtf8(self);
end;

function TIDHelper.ToString: string;
begin
  result := IntToString(self);
end;

{ TQueryFilter }

procedure TQueryFilter.Add(const FieldName: RawUTF8; const FilterOperator: TFilterOperator; const AValues: TIDDynArray);
var
  v: variant;
begin
  Count := Count + 1;
  FieldNames[Count-1] := FieldName;
  Operators[Count-1]  := FilterOperator;
  varClear (v);
  for var i := low (AValues) to High (AValues) do
    TDocVariantData (v).AddItem(AValues[i]);
  Values[Count-1] := v;
end;

procedure TQueryFilter.AddFrom(Filter: TQueryFilter);
begin
  for var i := 0 to Filter.Count - 1 do
    Add (Filter.FieldNames[i], Filter.Operators[i], Filter.Values[i]);
end;

procedure TQueryFilter.Add(const FieldName: RawUTF8; const FilterOperator: TFilterOperator; const AValue: variant);
var
  v: variant;
begin
  Count := Count + 1;
  FieldNames[Count-1] := FieldName;
  Operators[Count-1]  := FilterOperator;
  if FilterOperator = TFilterOperator.InValues
    then begin
           varClear (v);
           for var i := 0 to VarArrayHighBound(AValue, 0) do
             TDocVariantData (v).AddItem(AValue[i]);
           Values[Count-1] := v;
         end
    else Values[Count-1]     := AValue;
end;

procedure TQueryFilter.AddIfValid(const FieldName: RawUTF8; const FilterOperator: TFilterOperator; AValue: variant);
begin
  if (FilterOperator in [TFilterOperator.Like]) and
     ( VarIsNull(AValue) or (AValue = '') )
     then exit;

  if (FilterOperator in [TFilterOperator.IsNull])
     then AValue := varNull;

  Add (FieldName, FilterOperator, AValue);
end;

procedure TQueryFilter.Clear;
begin
  SetCount (0);
end;

function TQueryFilter.ContainsField(const FieldName: RawUTF8): boolean;
begin
  result := IndexOfField(FieldName) > -1;
end;

function TQueryFilter.Fieldvalue(const FieldName: RawUTF8): variant;
begin
  var index := IndexOfField(FieldName);
  if index = -1
    then VarClear(result)
    else result := Values[index];
end;

function TQueryFilter.IndexOfField(const FieldName: RawUTF8): integer;
var
  i : integer;
begin
  var ff := lowercase (FieldName);
  for i := 0 to Count - 1 do
    if lowercase (FieldNames[i]) = ff then
        exit (i);
  result := -1;
end;

function TQueryFilter.GetCount: integer;
begin
  result := Length(FieldNames);
end;

function TQueryFilter.RemoveField(const FieldName: RawUTF8): integer;
begin
  result := IndexOfField (FieldName);
  if result > -1 then
    begin
      FieldNames [result] := FieldNames [Count-1];
      Operators [result] := Operators [Count-1];
      Values [result] := Values [Count-1];
      Count := Count - 1;
    end;
end;

function TQueryFilter.ReplaceField(const FromField, ToField: RawUTF8): integer;
var
  i : integer;
begin
  var ff := lowercase (FromField);
  result := 0;
  for i := 0 to Count - 1 do
    if lowercase (FieldNames[i]) = ff then
      begin
        FieldNames[i] := ToField;
        inc (result);
      end;
end;

procedure TQueryFilter.SetCount(const Value: integer);
begin
  SetLength(FieldNames, Value);
  SetLength(Operators, Value);
  SetLength(Values, Value);
end;

{ TRestrictedCascades }

procedure TRestrictedCascades.Add(MasterClass, DetailClass: TSqlRecordClass; ForeignKeyName: RawUTF8 = '');
var
  lst : TList<TRestrictedCascade>;
  rc: TRestrictedCascade;
begin
  if ForeignKeyName = '' then
    if DetailClass.RecordProps.Fields.ByRawUTF8Name (MasterClass.SQLTableName + 'ID') <> nil
       then ForeignKeyName := MasterClass.SQLTableName + 'ID'
       else if DetailClass.RecordProps.Fields.ByRawUTF8Name (MasterClass.SQLTableName) <> nil
       then ForeignKeyName := MasterClass.SQLTableName
       else raise Exception.Create('Cant find foreign key for ' + DetailClass.SQLTableName.AsString + ' -> ' + MasterClass.SQLTableName.AsString);

  if not TryGetValue(MasterClass, lst) then
     begin
       lst := TList<TRestrictedCascade>.Create;
       inherited Add (MasterClass, lst);
     end;

  rc.TableClass := DetailClass;
  rc.ForeignKeyName := ForeignKeyName;

  lst.Add(rc);
end;

function ServerErrorOutcome(const error: TServerError; const FurtherDescription: string= ''): TOutcome;
begin
  if error = seRecordChangeNotAllowed then
    if HInstance = 1 then Exit; //todo wtf is this...
  if error = seRecordNotFound then
    if HInstance = 1 then Exit;

  result.Success := false;
  result.ErrorCode := integer(error);
  result.Description := slServerErrorMessages [error];
  if FurtherDescription <> '' then
    result.Description := result.Description + #13 + FurtherDescription;
end;

function RegisterServerError (sMessage: string): TServerError;
begin
  slServerErrorMessages.Add(sMessage);
  result := FServerError;
  inc (FServerError);
end;

{ TSQLRecordsRecord }

destructor TSQLRecordsRecord.Destroy;
begin
  if Records <> nil then
    raise Exception.Create('This record is still in a SqlRecords array!');
  inherited;
end;

{ TFunSQLModel }

constructor TSQLModelEx.Create(const Tables: array of TSQLRecordClass; const aRoot: RawUTF8);
begin
  inherited;
  RestrictedCascades := TRestrictedCascades.Create ([doOwnsValues]);
  FTableTitles := TDictionary<TSqlRecordClass, string>.Create;

  var ctx := TRttiContext.Create;
  try
    for var Table in Tables do
      begin
        var TableType := ctx.GetType(Table);
        for var prop in TableType.GetProperties do
          for var Attribute in prop.GetAttributes do
            if Attribute is FKRestrictedAttribute then
        begin
          var DetailClassName := copy (prop.PropertyType.Name, 1, length(prop.PropertyType.Name) - 2);
          RestrictedCascades.Add(Table, self.Table[StringToUTF8 (DetailClassName)], StringToUTF8 (prop.Name));
        end
        //else
      end;
  finally
    ctx.Free;
  end;


end;

destructor TSQLModelEx.Destroy;
begin
  FreeAndNil(RestrictedCascades);
  FreeAndNil(FTableTitles);
  inherited;
end;

function TSQLModelEx.GetTitle(cl: TSqlRecordClass): string;
begin
  if not FTableTitles.TryGetValue(cl, result)
    then result := cl.SQLTableName.AsString;
end;

constructor TSQLHttpClientWebsocketsEx.Create (AModel: TSQLModel; const Address, Port: string; AWebSocketsEncryptionKey: string);
begin
  inherited Create (ansistring(Address), ansistring(Port),
                    AModel, false {todo check https, currently}, '', '', 5000, 5000, 5000);
  Model := AModel;
  FWebSocketsEncryptionKey := AWebSocketsEncryptionKey;
  RetryOnceOnTimeout := false;
  Model.Owner := self;
  UseCallbacks := FindCmdLineSwitch('callbacks');
end;

procedure TSQLHttpClientWebsocketsEx.RegisterServices;
begin
  ServerTimestampSynchronize;
end;

function TSQLHttpClientWebsocketsEx.ServiceMustRegister(const aInterfaces: array of PTypeInfo; aInstanceCreation: TServiceInstanceImplementation; const aContractExpected: RawUTF8): boolean;
begin
  result := ServiceRegister(aInterfaces, aInstanceCreation, aContractExpected);
  if not result
    then raise Exception.Create('Unable to register service ' + string (aInterfaces[0].Name));
end;

procedure TSQLHttpClientWebsocketsEx.UpgradeToSockets;
begin
  FUpgradedToSockets := true;
  if not ServerTimestampSynchronize
    then raise Exception.Create('Unable to connect ' + self.LastErrorMessage.AsString);

  if WebSocketsUpgrade(StringToUTF8 (FWebSocketsEncryptionKey)) <> ''
     then raise Exception.Create('Unable to upgrade connection ' + self.LastErrorMessage.AsString);
end;

procedure TSQLHttpClientWebsocketsEx.SetReceiveTimeout(const Value: cardinal);
begin
  FReceiveTimeout := Value;
end;

function TSQLHttpClientWebsocketsEx.SetUser(const AUserName, APassword: string; aHashedPassword: Boolean): boolean;
begin
  result := inherited SetUser (StringToUTF8 (aUserName), StringToUTF8 (aPassword), aHashedPassword);
  TMonitor.Enter(self);
  FUserName := AUserName;
  FPassword := APassword;
  TMonitor.Exit(self);
end;

function TSQLHttpClientWebsocketsEx.ReLogin: boolean;
begin
  TMonitor.Enter(self);
  var U := FUserName;
  var P := FPassword;
  if SecondsSince (LastRelogin) < 5 then
    begin
      TMonitor.Exit(self);
      sleep (2000);
      exit (true);
    end;

  LastRelogin := now;
  TMonitor.Exit(self);

  self.InternalClose;
  result := SetUser (U, P);
  if result then RegisterServices;
end;

{ DefaultDisplayAttribute }

constructor DisplayableAttribute.Create(const ADisplayName: string = '');
begin
  DisplayName := ADisplayName;
end;

{ FKAttribute }

constructor FKAttribute.Create(ASqlRecordClass: TSQLRecordClass);
begin
  inherited Create;
  SqlRecordClass := ASQLRecordClass;
end;

initialization
  slServerErrorMessages := TStringList.Create;

  SQLRecordLists := TSQLRecordLists.Create;
  SQLRecordLists.OwnsObjects := false;

  seNoError := RegisterServerError ('');
  seRecordAlreadyLocked := RegisterServerError ('Record already locked');
  seCascadeRestrict := RegisterServerError ('Cascade restricted');
  seRecordNotFound := RegisterServerError ('Record not found');
  seUnknownTable := RegisterServerError ('Unknown table');
  seNoRecordAccess := RegisterServerError ('No record access');
  seBatchFailed := RegisterServerError ('Batch failed');
  seRecordNotLocked := RegisterServerError ('Record not locked');
  seRecordAlreadyLockedByMormot := RegisterServerError ('Record already locked by Mormot');
  seRecordChangeNotAllowed := RegisterServerError ('Record change not allowed');
  seNotImplemented := RegisterServerError ('Not implemented');
  seDetailTableOperationFailed := RegisterServerError ('Detail Table Operation Failed');
  seException := RegisterServerError ('Exception');
  seWrongCRUD := RegisterServerError ('Wrong CRUD');
  seNotRecordOwner := RegisterServerError ('Not Record Owner');
  seDetailsMissing := RegisterServerError ('Details Missing');
  seValidationFailed := RegisterServerError ('Validation Failed');
  seAddFailed := RegisterServerError ('Add Failed');
  seInvalidParameters := RegisterServerError ('Invalid Parameters');
  seRecordValueNotFound := RegisterServerError ('Record Value Not Found');
  seFileNotFound := RegisterServerError ('File Not Found');
  seDuplicateKey := RegisterServerError ('Duplicate Key');
  seDBFunctionFailed := RegisterServerError ('DB Function Failed');

  SocketNeverRetries := true;

  dicDefaultSortFields := TDictionary <TSQLRecordClass, RawUTF8>.Create;
  dicGeneralFilters := TDictionary <TSQLRecordClass, TArray<RawUTF8>>.Create;
finalization
  SQLRecordLists.Free;
  slServerErrorMessages.Free;
  dicDefaultSortFields.Free;
  dicGeneralFilters.Free;

end.
