unit VCL.LBUtils;

interface

uses
  System.Types,
  Controls;

  procedure FocusControlOrChild (c: TControl);

implementation


procedure FocusControlOrChild (c: TControl);
begin
  if (c is TWinControl) then
    begin
      var l := TWinControl (c);
      for var i := 0 to l.ControlCount do
        if l.Controls[i] is TWinControl then
          begin
            var wc := l.Controls[i] as TWinControl;
            if (wc.TabOrder = 0) and wc.CanFocus then
              begin
                wc.SetFocus;
                exit;
              end;
          end;
      l.SetFocus;
    end;
end;


end.
