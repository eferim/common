unit SqlRecords;

interface

uses
  SysUtils, Contnrs, System.Generics.Collections, Classes, System.Types, System.Generics.Defaults,
  Variants, StrUtils, math, DateUtils, LBUtils,
  {$ifdef MSWINDOWS}
  Windows, //for inline of synlocker...
  {$endif}
  MormotMods,
  SynDBZeos, SynCommons, SynDB, mormot, SynTable;

const
  Lookup_Timeout_Minutes = 0.5;

type

  TLookup = class
  protected
  public
    CreationTime: TDateTime;
    IDIndexes: TDictionary <TID, integer>;
    Items: TDictionary <TID, RawUTF8>;
    Reverse: TDictionary <RawUTF8, TID>;
    IDs: TIDDynArray;
    Values: TRawUTF8DynArray;
    //SortedValues: TRawUTF8DynArray;
    SortedValues : TStringList;
    MasterID: TID;
    procedure AfterConstruction; override;
    procedure Add (const AIDs: TIDDynArray; const AValues: TRawUTF8DynArray); reintroduce;
    function IndexOf (const ID: TID): integer;
    function Count: integer;
    destructor Destroy; override;
  end;

{  ILookupSource = interface
    function ResolveAsString (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: string): boolean;
    //function ResolveAsVariant (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID): variant;
  end;
}

  TLookups = class (TNonARCInterfacedObject{, ILookupSource})
  private
    Tables, Alternatives: TArray <TSQLRecordClass>;
    function GetLookup(cl: TSQLRecordClass): TLookup;
  public
    Dictionary: TObjectDictionary <TSQLRecordClass, TLookup>;
    function ResolveAsString (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: string): boolean;
//    function ResolveAsVariant (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: variant): boolean;

    constructor Create (const ATables, AAlternatives: TArray <TSQLRecordClass>);
    destructor Destroy; override;
    function HasValidLookup (cl: TSQLRecordClass): boolean;
    property Lookup [cl: TSQLRecordClass]: TLookup read GetLookup; default;
  end;

  IFieldValueResolver = interface
    function GetFieldDisplayValue (const SqlRecord: TSQLRecord; const FieldName: string): variant;
    function AuthUserClass: TSQLRecordClass;
  end;

  TSqlRecords <T: TSQLRecord> = class// (TInterfacedObject)
  private
    class var AllSqlRecords: TList<TObject>;
    class var NextID: integer;
    class constructor Create;
    class destructor Destroy;
    function GetFieldDisplayValue(index: integer;
      const FieldName: string): variant;
  var
    FInitTime: TDateTime;
    FCopyNo: integer;
    FID: integer;
    function GetCount: integer;
    type
      TArrayType = array of T;

      TSqlRecordsIndex = class
      private
        UseLookup, IsTextField: boolean;
        //LookupClass: TSQLRecordClass;
        function GetRecords(index: integer; Ascending: boolean = true): T;
        procedure FillRecordIDs;
        function FFind(const Value: variant; var FromIndex, ToIndex: integer): integer;
        function FFindText(const Value: variant; var FromIndex, ToIndex: integer): integer;
        function FindOrNearby(const Value: variant; var Index, FromIndex, ToIndex: integer): T; overload;
        function GetCount: integer;
      public
        FFieldName: string;
        IndexArray: TIntegerDynArray;
        RecordIDs: TDictionary <TID, integer>; // maps record IDs to index in IndexArray
                                               // to find a record by ID in this index
        DynArray: TDynArray;
        FOwner: TSqlRecords<T>;
        constructor Create (const AOwner: TSqlRecords<T>; const AFieldName: string);
        destructor Destroy; override;
        procedure Init;
        procedure Sort(L, R: Integer; const Ascending: boolean = true);
        procedure SortText(L, R: Integer; const Ascending: boolean = true);

        procedure QuickSort(const Ascending: boolean = true);

        function Find(const Value: variant): T; overload;
        function Find(const Value: variant; var Index: integer): T; overload;

        function IndexOf(const Value: variant): integer;         // index of value (used in sorting) in this "DB"index...
        function IndexOfOriginal(const Value: variant): integer; // index of value (used in sorting) in owner records array
        function IndexOfRecordID(const ID: TID; const Ascending: boolean): integer; // returns index of record in this index's array
        function RecordWithID (const ID: TID): T; // find a record with this ID
        function Add (const rec: T): integer;
        function Delete (ID: TID): integer;
        // for given index in the index's array, look for the corresponding owner's record and get the field value
        function ResolveVarValue(const index: integer): variant; inline;
        function ResolveStringValue(const index: integer): string; inline;
        property Records [index: integer; Ascending: boolean = true]: T read GetRecords;
        property Count: integer read GetCount;
      end;

      TFilteredEnum = class
      private
        FTotalCount, FCurrent: integer;
        FOwner: TSqlRecords<T>;
        FIndex: TSqlRecordsIndex;
        FAscending: boolean;
        FFilterFields: TStringList;
        FFilterValue: string;
        //FLookupClass: TSQLRecordClass;
      public
        constructor Create (const AOwner: TSqlRecords<T>; const AIndex: TSqlRecordsIndex;
                            const AAscending: boolean = true; const AFilterFields: string = ''; const AFilterValue: string = '');
        destructor Destroy; override;
        function GetEnumerator: TFilteredEnum;
        function MoveNext:Boolean;
        function GetCurrent: T;
        property Current: T read GetCurrent;
      end;

    function GetIndexes(Name: string): TSqlRecordsIndex;

    var
      FIndexes: TDictionary <string, TSqlRecordsIndex>; // field name, index
  protected
    //ExternalLookupSource: ILookupSource;
  public
    Records: TArrayType; // tsql record objects go only into this array
    DynArray: TDynArray;
    FieldValueResolver: IFieldValueResolver;

    constructor Create;
    destructor Destroy; override;
    procedure Init (const AFieldValueResolver: IFieldValueResolver);
    function CreateCopy: TSqlRecords <T>;
    function Find(const FieldName: string; const Value: variant; StartFrom: integer = 0): T; overload;
//    function Find(const FieldName: string; const Value: variant; var Index: integer; StartFrom: integer = 0): T; overload;
    function Lookup(const FindFieldName: string; const FindValue: variant; const LookupFieldName: string): variant;
    function EnumByField (const FieldName: string; const Ascending: boolean = true;
                          const FilterField: string = ''; const FilterValue: string = ''): TFilteredEnum;
    function GetLookupClass (AFieldName: string): TSQLRecordClass;
    //function ResolveLookupAsVariant(const LookupClass: TSQLRecordClass; const LookupID: TID): variant;
    //function ResolveLookupAsString(const LookupClass: TSQLRecordClass; const LookupID: TID): string;
    procedure Add (ARecord: T);
    procedure Delete (ID: TID);
    procedure Update (ARecord: T);
    function IndexOf (ARecord: TSqlRecord): integer; overload;
    function IndexOf (ID: TID): integer; overload;
    function Contains(ID: TID): boolean;
    function Sum (FieldName: string): double;

    property Indexes [Name: string]: TSqlRecordsIndex read GetIndexes;
    property InitTime: TDateTime read FInitTime;
    property Count: integer read GetCount;
    property CopyNo: integer read FCopyNo;
    property ID: integer read FID;
  end;

  function VarCompareValueForSort(const A, B: Variant): TVariantRelationship;

implementation


function VarCompareValueForSort(const A, B: Variant): TVariantRelationship;
begin
  // to treat empty variants as lowest value
  if VarIsEmpty (A)
    then if VarIsEmpty (B)
      then result := TVariantRelationship.vrEqual
      else result := TVariantRelationship.vrLessThan
    else if VarIsEmpty (B)
      then result := TVariantRelationship.vrGreaterThan
    else if VarType (A) = varUString
      then begin
             var c := CompareText (A, B);
             if c < 0
                then result := TVariantRelationship.vrLessThan
                else if c > 0
                then result := TVariantRelationship.vrGreaterThan
                else result := TVariantRelationship.vrEqual
           end
      else result := System.Variants.VarCompareValue(A, B);
end;

procedure TSqlRecords<T>.Add(ARecord: T);
var
  pair: TPair <string, TSqlRecordsIndex>;
begin
  DynArray.Add(ARecord);
  for pair in FIndexes do
    pair.Value.Add (ARecord);
end;

procedure TSqlRecords<T>.Delete (ID: TID);
var
  i : integer;
begin
  i := IndexOf (ID);
  DynArray.Delete(i);
  for var pair in FIndexes do
    pair.Value.Delete (ID);
end;

class destructor TSqlRecords<T>.Destroy;
begin
  inherited;
  AllSqlRecords.Free;
end;

procedure TSqlRecords<T>.Update (ARecord: T);
begin
  Delete (ARecord.IDValue);
  Add (ARecord);
end;

class constructor TSqlRecords<T>.Create;
begin
  inherited;
  AllSqlRecords := TList<TObject>.Create;
end;

function TSqlRecords<T>.Contains(ID: TID): boolean;
begin
  result := IndexOf (ID) > -1;
end;

constructor TSqlRecords<T>.Create;
begin
  inherited Create;
  FIndexes := TDictionary<string, TSqlRecordsIndex>.Create;
  AllSqlRecords.Add (self);
  inc (NextID);
  FID := NextID;
end;

function TSqlRecords<T>.CreateCopy: TSqlRecords<T>;
var
  rec: TSqlRecord;
  i: integer;
begin
  result := TSqlRecords<T>.Create;
  SetLength (result.Records, Count);
  for i := Low (result.Records) to High (result.Records) do
    result.Records[i] := T(Records[i].CreateCopy);
  result.Init (FieldValueResolver);
  result.FCopyNo := CopyNo + 1;
  result.FID := ID;
end;

function TSqlRecords<T>.GetCount: integer;
begin
  if self = nil
     then result := 0
     else result := length (Records);
end;

function TSqlRecords<T>.GetIndexes(Name: string): TSqlRecordsIndex;
begin
  if Name = ''
     then raise Exception.Create('Index field name not specified');

  if not FIndexes.TryGetValue (Name.ToLower, result)
    then begin
           result := TSqlRecordsIndex.Create(self, Name.ToLower);
           FIndexes.Add(Name.ToLower, result);
         end;
end;

function TSqlRecords<T>.GetLookupClass(AFieldName: string): TSQLRecordClass;
begin
  if (Count = 0) or (FieldValueResolver = nil) then exit (nil);

  var SqlRecord := records[0] as TSqlRecord;
  result := SqlRecord.GetLookupClass(AFieldName, FieldValueResolver.AuthUserClass);
end;

{
function TSqlRecords<T>.ResolveLookupAsVariant(const LookupClass: TSQLRecordClass; const LookupID: TID): variant;
begin
  var s := '';
  if FieldValueResolver. ResolveAsString(LookupClass, LookupID, s) //was AsVariant
    then result := s
    else VarClear(result);
end;


function TSqlRecords<T>.ResolveLookupAsString(const LookupClass: TSQLRecordClass; const LookupID: TID): string;
begin
  if not ExternalLookupSource.ResolveAsString(LookupClass, LookupID, result) then result := '';
end;
}

destructor TSqlRecords<T>.Destroy;
begin
  if AllSqlRecords.IndexOf (self) = -1 then
    raise Exception.Create('Invalid TSqlRecords pointer');

  if (Count > 0) and (Records[0] is TSQLRecordsRecord) then
    for var i := 0 to Count-1 do
        TSQLRecordsRecord (Records[i]).Records := nil;

  for var rec in Records do
      rec.Free;
  if FIndexes <> nil then
    for var Index in FIndexes.Values do
      Index.Free;
  FIndexes.Free;
  AllSqlRecords.Remove (self);

  FieldValueResolver := nil;
  inherited;
end;

function TSqlRecords<T>.EnumByField (const FieldName: string; const Ascending: boolean = true;
                          const FilterField: string = ''; const FilterValue: string = ''): TFilteredEnum;
begin
  if FieldName = '' then raise Exception.Create('Field name not specified.'); //todo should check if field exists...
  var index := Indexes[FieldName];
  result := TFilteredEnum.Create (self, index, Ascending, FilterField, FilterValue);
end;

function TSqlRecords<T>.Find(const FieldName: string; const Value: variant; StartFrom: integer): T;
var
  Index, FromIndex, ToIndex: integer;
begin
  if length(Records) = 0 then exit (nil);

  var pIndex := Indexes[FieldName];
  FromIndex := StartFrom;
  ToIndex := high (Records);
  result := pIndex.FindOrNearby (Value, Index, FromIndex, ToIndex);
end;

function TSqlRecords<T>.IndexOf (ARecord: TSqlRecord): integer;
begin
  result := IndexOf (ARecord.IDValue);
end;

function TSqlRecords<T>.IndexOf (ID: TID): integer;
begin // records      [a f b d]
      // index [name] [1 3 4 2]
      //               0 1 2 3
  var IDIndex := Indexes['ID'];
  result := IDIndex.IndexOfOriginal(ID);
//  result := IDIndex.IndexArray[result];
end;

procedure TSqlRecords<T>.Init (const AFieldValueResolver: IFieldValueResolver);
begin
  FieldValueResolver := AFieldValueResolver;
  DynArray.Init(TypeInfo(TArrayType), Records);
  FIndexes.Clear;
  FInitTime := now;
  if (Count > 0) and (Records[0] is TSQLRecordsRecord) then
    for var i := 0 to Count-1 do
      if TSQLRecordsRecord (Records[i]).Records <> nil
         then raise Exception.Create('Record already belongs to a different TSqlRecords!')
         else TSQLRecordsRecord (Records[i]).Records := self;
end;

function TSqlRecords<T>.Lookup(const FindFieldName: string; const FindValue: variant; const LookupFieldName: string): variant;
begin
  var rec := Find (FindFieldName, FindValue);
  if rec <> nil
     then result := rec.GetFieldVariantValue (LookupFieldName)
     else result := varNull;
end;

function TSqlRecords<T>.Sum(FieldName: string): double;
var
  rec: T;
  temp: variant;
begin
  result := 0;

  var P := T.RecordProps.Fields.ByRawUTF8Name(StringToUTF8(FieldName));

  for rec in self.Records do
    begin
      P.GetVariant(rec, temp);
      result := result + temp;
    end;
end;

// ============

constructor TSqlRecords<T>.TFilteredEnum.Create(const AOwner: TSqlRecords<T>; const AIndex: TSqlRecordsIndex;
                                                const AAscending: boolean = true; const AFilterFields: string = ''; const AFilterValue: string = '');
begin
  inherited Create;
  FFilterFields := TStringList.Create;
  FFilterFields.CommaText := AFilterFields;
  FOwner := AOwner;
  FIndex := AIndex;
  FAscending := AAscending;
  FFilterValue := AFilterValue.ToLower;
  FTotalCount := Length(FIndex.IndexArray);
  if FAscending
     then FCurrent := -1
     else FCurrent := FTotalCount;
end;

destructor TSqlRecords<T>.TFilteredEnum.Destroy;
begin
  FFilterFields.Free;
  inherited;
end;

function TSqlRecords<T>.TFilteredEnum.GetCurrent: T;
begin
//  if fcurrent = 0 then
//    if now=0 then exit;

  result := FOwner.Records[FIndex.IndexArray[FCurrent]];
end;

function TSqlRecords<T>.TFilteredEnum.MoveNext: Boolean;
var
  s: string;
  p: integer;
  FilterMatched: boolean;
begin
  FilterMatched := (FFilterValue = '');

  repeat
      if FAscending
         then inc (FCurrent)
         else dec (FCurrent);

      if FilterMatched then break;

      if (FCurrent >= FTotalCount) or (FCurrent < 0) then break;

      for var ff in FFilterFields do
        begin
          s := FOwner.FieldValueResolver.GetFieldDisplayValue (Current, ff);
          s := s.ToLower;
          p := pos (FFilterValue, s);
          if (p > 0) then
            begin
              FilterMatched := true;
              break;
            end;
        end;
    until FilterMatched;

    // todo add option to filter all fields if ffilfield is blank
  if FAscending
     then result := FTotalCount > FCurrent
     else result := FCurrent >= 0;
end;

function TSqlRecords<T>.TFilteredEnum.GetEnumerator: TFilteredEnum;
begin
  result := self;
end;

{ TSqlRecords<T>.TSqlRecordsIndex }

function TSqlRecords<T>.TSqlRecordsIndex.Add(const rec: T): integer;
var
  FromIndex, ToIndex, index: integer;
begin
  FromIndex := low (IndexArray);
  ToIndex := high (IndexArray);

  if FindOrNearby (rec.GetFieldVariantValue(FFieldName), index, FromIndex, ToIndex) <> nil
    then
    else
      begin
        index := min (length(IndexArray) - 1, max (FromIndex, ToIndex));
        if not IsTextField
          then
            begin
              var v := rec.GetFieldVariantValue (FFieldName);

              while (index > 0) and (VarCompareValueForSort (ResolveVarValue(index), v) = TVariantRelationship.vrGreaterThan) do
                dec (index);
              inc (index);
            end
          else
            begin
              var s := rec.GetFieldStringValue (FFieldName);

              while (index > 0) and (CompareText (ResolveVarValue(index), s)>0) do
                dec (index);
              inc (index);
            end;
      end;

  var int := length(FOwner.Records) - 1;
  DynArray.Insert(index, int);
  RecordIDs.Add(rec.IDValue, index);
  if length (IndexArray) = 1 then Init;
  result := index;
end;

function TSqlRecords<T>.TSqlRecordsIndex.Delete(ID: TID): integer;
begin
  result := RecordIDs [ID];
  DynArray.Delete(RecordIDs [ID]);
  RecordIDs.Remove(ID);
end;

destructor TSqlRecords<T>.TSqlRecordsIndex.Destroy;
begin
  RecordIDs.Free;
  inherited;
end;

constructor TSqlRecords<T>.TSqlRecordsIndex.Create(const AOwner: TSqlRecords<T>; const AFieldName: string);
begin
  inherited Create;
  RecordIDs := TDictionary <TID, integer>.Create;
  FOwner := AOwner;
  FFieldName := AFieldName;
  Init;
  QuickSort;
  DynArray.Init(TypeInfo(TIntegerDynArray), IndexArray);
end;

function TSqlRecords<T>.TSqlRecordsIndex.FFind(const Value: variant; var FromIndex, ToIndex: integer): integer;
var
  guess: integer;
begin
  if Count = 0 then exit (-1);

  var MaxIndex := length(IndexArray);
  repeat
    guess := (FromIndex + ToIndex) div 2;
    case VarCompareValueForSort (ResolveVarValue(guess), Value) of
      TVariantRelationship.vrEqual: exit (guess);
      TVariantRelationship.vrLessThan:    FromIndex := guess+1;
      TVariantRelationship.vrGreaterThan: ToIndex := guess-1;
      TVariantRelationship.vrNotEqual: raise Exception.Create('Unable to compare values of field ' + FFieldName);
    end;
  until (FromIndex<0) or (ToIndex>MaxIndex) or (FromIndex > ToIndex);
  result := -1;
end;

function TSqlRecords<T>.TSqlRecordsIndex.FFindText(const Value: variant; var FromIndex, ToIndex: integer): integer;
var
  guess, cmp: integer;
begin
  if Count = 0 then exit (-1);

  var MaxIndex := length(IndexArray);
  repeat
    begin
      guess := (FromIndex + ToIndex) div 2;
      cmp := CompareText (Value, ResolveStringValue(guess));
      if cmp = 0
        then exit (guess)
        else if cmp > 0
          then FromIndex := guess+1 // value is > guess, start over with next one as "from"
          else ToIndex := guess-1;  // value is < guess, start over with previous one as "to"
    end;
  until (FromIndex<0) or (ToIndex>MaxIndex) or (FromIndex > ToIndex);
  result := -1;
end;

procedure TSqlRecords<T>.TSqlRecordsIndex.FillRecordIDs;
begin
  RecordIDs.Clear;
  var id: TID;

  for var i := Low (IndexArray) to high (IndexArray) do
    begin
      id := Records[i, true].IDValue;
      if RecordIDs.ContainsKey (id)
         then begin                    // duplicate or NULL IDs
                RecordIDs.Clear;
                break;
              end
         else RecordIDs.Add (Records[i, true].IDValue, i);
    end;
end;

function TSqlRecords<T>.TSqlRecordsIndex.Find(const Value: variant): T;
var
  Index, FromIndex, ToIndex: integer;
begin
  FromIndex := low (IndexArray);
  ToIndex := high (IndexArray);
  result := FindOrNearby (Value, index, FromIndex, ToIndex);
end;

function TSqlRecords<T>.TSqlRecordsIndex.Find(const Value: variant; var Index: integer): T;
var
  FromIndex, ToIndex: integer;
begin
  FromIndex := low (IndexArray);
  ToIndex := high (IndexArray);
  result := FindOrNearby (Value, index, FromIndex, ToIndex);
end;

function TSqlRecords<T>.TSqlRecordsIndex.IndexOf (const Value: variant): integer;
var
  Index, FromIndex, ToIndex: integer;
begin
  FromIndex := low (IndexArray);
  ToIndex := high (IndexArray);
  if FindOrNearby (Value, Index, FromIndex, ToIndex)<>nil
    then result := Index
    else result := -1;
end;

function TSqlRecords<T>.TSqlRecordsIndex.IndexOfOriginal(const Value: variant): integer; // index of value in owner records array
begin
  result := IndexOf (Value);
  if result <> -1
     then result := IndexArray[result];
end;

function TSqlRecords<T>.TSqlRecordsIndex.IndexOfRecordID(const ID: TID; const Ascending: boolean): integer;
begin
  if RecordIDs.TryGetValue (ID, result)
    then
      if Ascending
         then
         else result := FOwner.Count - 1 - result
    else result := -1
end;

function TSqlRecords<T>.TSqlRecordsIndex.RecordWithID(const ID: TID): T;
begin
  var index := IndexOfRecordID (ID, true);
  if index = -1
     then result := nil
     else result := Records[index, true];
end;


function TSqlRecords<T>.TSqlRecordsIndex.FindOrNearby (const Value: variant; var Index, FromIndex, ToIndex: integer): T;
begin
  if IsTextField
    then index := FFindText (Value, FromIndex, ToIndex)
    else index := FFind (Value, FromIndex, ToIndex);

  if index <> -1
     then begin
            //Index := IndexArray[index];
            result := FOwner.Records[IndexArray[index]];
          end
     else result := nil;
end;

function TSqlRecords<T>.TSqlRecordsIndex.GetCount: integer;
begin
  result := length (IndexArray);
end;

function TSqlRecords<T>.TSqlRecordsIndex.GetRecords(index: integer; Ascending: boolean = true): T;
begin
  if not Ascending
    then index := FOwner.Count - index - 1;
  result := FOwner.Records[IndexArray[index]];
end;

procedure TSqlRecords<T>.TSqlRecordsIndex.Init;
begin
//  LookupClass := FOwner.GetLookupClass (FFieldName);
  if FFieldName[1] = '!' then
    begin
      UseLookup := false;
      FFieldName := copy (FFieldName, 2);
    end
    else
      UseLookup := (FOwner <> nil) and (FOwner.GetLookupClass (FFieldName) <> nil);
  if UseLookup
    then IsTextField := true
    else
      if Length(FOwner.Records) > 0
        then
          begin
            var vt := VarType (FOwner.Records[0].GetFieldVariantValue(FFieldName));
            IsTextField := vt = varString;
          end
        else
            IsTextField := false;
end;

function TSqlRecords<T>.TSqlRecordsIndex.ResolveStringValue(const index: integer): string;
begin
  var SqlRecord := FOwner.Records[IndexArray[index]];
  var ID := SqlRecord.GetFieldVariantValue(FFieldName);
  if UseLookup
    then result := FOwner.FieldValueResolver.GetFieldDisplayValue (SqlRecord, FFieldName)//  ResolveLookupAsString(LookupClass, ID)
    else result := SqlRecord.GetFieldStringValue(FFieldName);
end;

function TSqlRecords<T>.TSqlRecordsIndex.ResolveVarValue(const index: integer): variant;
begin
  var SqlRecord := FOwner.Records[IndexArray[index]];
  result := SqlRecord.GetFieldVariantValue(FFieldName);
  if UseLookup
    then
      //result := FOwner.ResolveLookupAsVariant(LookupClass, result);
      FOwner.FieldValueResolver.GetFieldDisplayValue(SqlRecord, FFieldName);
end;

procedure TSqlRecords<T>.TSqlRecordsIndex.QuickSort(const Ascending: boolean = true);
begin
  SetLength(IndexArray, Length(FOwner.Records));
  for var i := low (IndexArray) to high (IndexArray) do
    IndexArray[i] := i;

  if Length(IndexArray) > 0 then
    begin
      if IsTextField
       then SortText(0, Length(IndexArray) - 1, Ascending)
       else Sort(0, Length(IndexArray) - 1, Ascending);
    end;

  FillRecordIDs;
end;

procedure TSqlRecords<T>.TSqlRecordsIndex.Sort(L, R: Integer; const Ascending: boolean);
var
  I, J, P: Integer;
  v1, v2: variant;
begin
  repeat
    I := L;
    J := R;
    P := (L+R) div 2;
    repeat
      var vr: TVariantRelationship;
      while true do
        begin
          v1 := FOwner.Records[IndexArray[I]].GetFieldVariantValue(FFieldName);
          v2 := FOwner.Records[IndexArray[P]].GetFieldVariantValue(FFieldName);
          vr := VarCompareValueForSort (v1, v2);
          if (Ascending and (vr = TVariantRelationship.vrLessThan) )
             or
             (Not Ascending and (vr = TVariantRelationship.vrGreaterThan) )
            then inc (I)
            else break;
        end;

      if vr = TVariantRelationship.vrNotEqual
         then raise Exception.Create('Unable to sort by ' + FFieldName);

      while true do
        begin
          v1 := FOwner.Records[IndexArray[J]].GetFieldVariantValue(FFieldName);
          v2 := FOwner.Records[IndexArray[P]].GetFieldVariantValue(FFieldName);
          vr := VarCompareValueForSort (v1, v2);
          if (Ascending and (vr = TVariantRelationship.vrGreaterThan) )
             or
             (Not Ascending and (vr = TVariantRelationship.vrLessThan) )
            then dec (J)
            else break;
        end;

      if vr = TVariantRelationship.vrNotEqual
         then raise Exception.Create('Unable to sort by ' + FFieldName);

//        while Compare(I, P)<0 do inc(I);
//        while Compare(J, P)>0 do dec(J);
      if I<=J then
      begin
        if I<>J then
        begin
          var ExJ := IndexArray[J];
          IndexArray[J] := IndexArray[I];
          IndexArray[I] := ExJ;
          //Exchange(I, J);
          //may have moved the pivot so we must remember which element it is
          if P = I then
            P := J
          else if P=J then
            P := I;
        end;
        inc(I);
        dec(J);
      end;
    until I>J;
    if L<J then
      Sort(L, J, Ascending);
    L := I;
  until I>=R;
end;

function TSqlRecords<T>.GetFieldDisplayValue (index: integer; const FieldName: string): variant;
begin
  result := FieldValueResolver.GetFieldDisplayValue(Records[index], FieldName);
end;

procedure TSqlRecords<T>.TSqlRecordsIndex.SortText(L, R: Integer; const Ascending: boolean);
var
  AscFactor, I, J, P: Integer;
  v1, v2: variant;
  PValue: string;
begin
  repeat
    I := L;
    J := R;
    P := (L+R) div 2;
                                  //todo: optimize and get the lookup object (if there is one) to access it directly
    if Ascending then AscFactor := 1 else AscFactor := -1;
    repeat
      while true do
        begin
          //PValue := FOwner.Records[IndexArray[P]].GetFieldStringValue(FFieldName);
          PValue := FOwner.GetFieldDisplayValue(IndexArray[P], FFieldName);

          //if AscFactor*CompareText (FOwner.Records[IndexArray[I]].GetFieldStringValue(FFieldName), PValue) < 0
          if AscFactor*CompareText (FOwner.GetFieldDisplayValue(IndexArray[I], FFieldName), PValue) < 0
            then inc (I)
            else break;
        end;

      while true do
        begin
          //if AscFactor*CompareText (FOwner.Records[IndexArray[J]].GetFieldStringValue(FFieldName), PValue) > 0
          if AscFactor*CompareText (FOwner.GetFieldDisplayValue(IndexArray[J], FFieldName), PValue) > 0
            then dec (J)
            else break;
        end;

      if I<=J then
      begin
        if I<>J then
        begin
          var ExJ := IndexArray[J];
          IndexArray[J] := IndexArray[I];
          IndexArray[I] := ExJ;
          //Exchange(I, J);
          //may have moved the pivot so we must remember which element it is
          if P = I then
            P := J
          else if P=J then
            P := I;
        end;
        inc(I);
        dec(J);
      end;
    until I>J;
    if L<J then
      SortText(L, J, Ascending);
    L := I;
  until I>=R;
end;

{ TFunLookup }

procedure TLookup.Add(const AIDs: TIDDynArray; const AValues: TRawUTF8DynArray);
begin
  for var i := low (AIDs) to high (AIDs) do
    begin
      Items.Add (AIDs[i], AValues[i]);
      if Reverse.ContainsKey (AValues[i])
        then Reverse.Add(AValues[i] + '(' + StringToUTF8 (AIDs[i].ToString) + ')', AIDs[i])
        else Reverse.Add(AValues[i], AIDs[i]);

      SortedValues.Add(AValues[i].AsString);
      IDIndexes.Add (AIDs[i], i);
    end;
  IDs := AIDs;
  Values := AValues;
  //SortedValues := AValues;
  //QuickSortRawUTF8 (SortedValues, Items.Count);
end;

procedure TLookup.AfterConstruction;
begin
  inherited;
  IDIndexes := TDictionary <TID, integer>.Create;
  Items := TDictionary <TID, RawUTF8>.Create;
  Reverse := TDictionary <RawUTF8, TID>.Create;
  CreationTime := now;
  SortedValues := TStringList.Create;
  SortedValues.Sorted := true;
end;

function TLookup.Count: integer;
begin
  if self = nil
    then exit (0)
    else result := Items.Count;
end;

destructor TLookup.Destroy;
begin
  IDIndexes.Free;
  Items.Free;
  Reverse.Free;
  SortedValues.Free;
  inherited;
end;

function TLookup.IndexOf(const ID: TID): integer;
begin
  if self = nil then exit (-1);
  if not IDIndexes.TryGetValue(ID, result) then result := -1;

//  var u: RawUTF8;
//  if Items.TryGetValue (ID, u)
//    then result := SortedValues.IndexOf(u.AsString)
//         // result := FastFindPUTF8CharSorted (@SortedValues, Items.Count-1, @u)
//    else result := -1;
end;

{ TLookups }

constructor TLookups.Create (const ATables, AAlternatives: TArray <TSQLRecordClass>);
begin
  inherited Create;
  Dictionary := TObjectDictionary<TSQLRecordClass, TLookup>.Create ([doOwnsValues]);
  Tables := ATables;
  Alternatives := AAlternatives;
end;

destructor TLookups.Destroy;
begin
  Dictionary.Free;
  inherited;
end;

function TLookups.GetLookup(cl: TSQLRecordClass): TLookup;
begin
  Dictionary.TryGetValue(cl, result);
end;

function TLookups.HasValidLookup(cl: TSQLRecordClass): boolean;
var
  lk: TLookup;
begin
  result := Dictionary.TryGetValue(cl, lk) and (MinutesSince(lk.CreationTime) < Lookup_Timeout_Minutes);
end;

function TLookups.ResolveAsString (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: string): boolean;
var
  LU: TLookup;
  var utf8: RawUTF8;
begin
  result := Dictionary.TryGetValue(ASqlRecordClass, LU);
  if not result then
    for var i := low (Tables) to high (Tables) do
      if Tables[i] = ASqlRecordClass then
        begin
          result := Dictionary.TryGetValue(Alternatives[i], LU);
          if result then break;
        end;
//      if and (ASqlRecordClass = TSQLFunUser) then
//    result := Dictionary.TryGetValue(TSqlPublicUser, LU);

  if LU <> nil
     then
       begin
         result := LU.Items.TryGetValue(ASqlRecordID, utf8);
         if utf8 <> '' then Value := utf8.AsString;
       end;
end;

{function TLookups.ResolveAsVariant (const ASqlRecordClass: TSQLRecordClass; const ASqlRecordID: TID; var Value: variant): variant;
var
  s: string;
begin
  s := value;
  result := ResolveAsString (ASqlRecordClass, ASqlRecordID, s);
end;
}

end.
