﻿unit Validation;

interface

uses System.RTTI, SysUtils, Variants, Generics.Collections;

const
  {$IFDEF ENGLISH}
  tcFieldXMustHaveValue = 'Field "%s" must have a value';
  tcFieldXTooLong       = 'Field "%s" value is too long';
  tcFieldXTooShort      = 'Field "%s" value is too short.';
  tcFieldXWrongLength   = 'Field "%s" value length is incorrect.';
  tcMinLength           = 'Min length is %d';
  tcMaxLength           = 'Max length is %d';
  tcExactLength         = 'Length must be %d';
  tcFieldMustHaveUniqueValue = 'Field "%s" must have an unique value';
  {$ELSE}
  tcFieldXMustHaveValue = 'Polje "%s" mora imati vrednost';
  tcFieldXTooLong = 'Vrednost polja "%s" je predugačka.';
  tcFieldXTooShort = 'Vrednost polja "%s" je prekratka.';
  tcFieldXWrongLength   = 'Dužina polja "%s" nije ispravna.';
  tcMaxLength     = 'Maksimalna dužina je %d';
  tcMinLength     = 'Minimalna dužina je %d';
  tcExactLength   = 'Dužina mora biti %d';
  tcFieldMustHaveUniqueValue = 'Polje "%s" mora imati jedinstvenu vrednost';
  {$ENDIF}

type

  ValidatorAttribute = class(TCustomAttribute);

  NotNullAttribute = class(TCustomAttribute)
  public
    function Validate(p : TRttiProperty; Target: TObject) : string;
  end;

  ForeignKeyAttribute = class(TCustomAttribute)
  end;

  NotNullForeignKeyAttribute = class(ForeignKeyAttribute)
  public
    function Validate(p : TRttiProperty; Target: TObject) : string;
  end;

  MaxLengthAttribute = class(TCustomAttribute)
  protected
    FLimit: uint64;
  public
    constructor Create (Limit: uint64);
    function Validate(p : TRttiProperty; Target: TObject) : string;
  end;

  MinLengthAttribute = class(TCustomAttribute)
  protected
    FLimit: uint64;
  public
    constructor Create (Limit: uint64);
    function Validate(p : TRttiProperty; Target: TObject) : string;
  end;

  ExactLengthAttribute = class(TCustomAttribute)
  protected
    FLimit: uint64;
  public
    constructor Create (Limit: uint64);
    function Validate(p : TRttiProperty; Target: TObject) : string;
  end;

  function ValidateWithAttributes(Target : TObject) : string;
  function ValidationFailedText (const FieldName, Fmt: string; Target: TObject): string;

var
  ValidationDictionary: TDictionary<string, string>;

implementation

uses
  mormotMods;

type
  TCustomAttributeClass = class of TCustomAttribute;

function HasAttribute(AMethod: TRttiMethod;  AttribClass: TCustomAttributeClass): Boolean;
var
  Attributes: TArray<TCustomAttribute>;
  Attrib: TCustomAttribute;
begin
  Result := False;
  Attributes := AMethod.GetAttributes;

  for Attrib in Attributes do
    if Attrib.InheritsFrom(AttribClass) then
      Exit(True);
end;

function ValidateWithAttributes(Target : TObject) : string;
var
  ctx : TRttiContext;
  t : TRttiType;
  p : TRttiProperty;
  a : TCustomAttribute;
  m : TRttiMethod;
begin
  Result := '';

  if not Assigned(Target) then
    raise Exception.Create('Can''t validate nil object');

  ctx := TRttiContext.Create;
  try
    t := ctx.GetType(Target.ClassType);

    for m in t.GetMethods do
      if HasAttribute (m, ValidatorAttribute) then
        begin
          result := m.Invoke (Target, []).ToString;
          if result <> '' then exit;
        end;

    for p in t.GetProperties do
      begin
        for a in p.GetAttributes do
          if a is NotNullForeignKeyAttribute then
          begin
            result := NotNullForeignKeyAttribute(a).Validate(p, Target);
            if result <> '' then exit;
          end
          else if a is NotNullAttribute then
          begin
            result := NotNullAttribute(a).Validate(p, Target);
            if result <> '' then exit;
          end
          {
          else if a is BaseStringValidationAttribute then
          begin
            if not BaseStringValidationAttribute(a).Validate(p.GetValue(Target).AsString) then
              ErrorList.Add(BaseValidationAttribute(a).FailureMessage);
          end}
      end;
  finally
    ctx.Free;
  end;
end;

{ MandatoryForeignKeyAttribute }

function ValidationFailedText (const FieldName, Fmt: string; Target: TObject): string;
begin
  var n: string := Target.ClassName + '.' + FieldName;
  if ValidationDictionary.ContainsKey (n)
    then result := ValidationDictionary[n]
    else if ValidationDictionary.ContainsKey (FieldName)
      then result := ValidationDictionary[FieldName]
      else result := FieldName;
  result := format (Fmt, [result]);
end;

function NotNullForeignKeyAttribute.Validate(p: TRttiProperty; Target: TObject): string;
begin
  if p.GetValue(Target).AsInt64 <= NULL_ID
     then result := ValidationFailedText (p.Name, tcFieldXMustHaveValue, Target)
     else result := '';
end;

{ NotNullAttribute }

function NotNullAttribute.Validate(p: TRttiProperty; Target: TObject): string;
var
  ok : boolean;
  val: TValue;
begin
    case p.PropertyType.TypeKind of
      tkInt64, tkInteger: ok := p.GetValue(Target).AsInt64 <> 0;
      tkFloat: ok := p.GetValue(Target).AsExtended <> 0;
      tkChar, tkString, tkLString:            ok := p.GetValue(Target).AsString <> '';
      tkVariant:                   ok := not VarIsNull(p.GetValue(Target).AsVariant);
      tkClass:                     begin
                                     try
                                       // ugly workaround for sqlrecord foreign keys of class type
                                       // we only want to know that its not null
                                       // but if its a bad pointer this will crash
                                       // if it crashes, its not null
                                       val := p.GetValue(Target);
                                       ok := not val.IsEmpty;
                                     except
                                       ok := true;
                                     end;
                                   end;

      else raise Exception.Create('Invalid property for NotNullAttribute');
  //    tkSet, tkClass, tkMethod, tkWChar, tkLString, tkWString, tkArray, tkRecord, tkInterface,
  //    tkDynArray, tkUString, tkClassRef, tkPointer, tkProcedure, tkMRecord
    end;

  if ok
     then result := ''
     else result := ValidationFailedText (p.Name, tcFieldXMustHaveValue, Target);
end;

{ MaxLengthAttribute }

constructor MaxLengthAttribute.Create(Limit: uint64);
begin
  inherited Create;
  FLimit := Limit;
end;

function MaxLengthAttribute.Validate(p: TRttiProperty; Target: TObject): string;
var
  ok : boolean;
begin
    case p.PropertyType.TypeKind of
      tkChar, tkString, tkLString:  ok := length (p.GetValue(Target).AsString) <= FLimit;
      else raise Exception.Create('Invalid property for MaxLengthAttribute');
    end;

  if ok
     then result := ''
     else result := ValidationFailedText (p.Name, tcFieldXTooLong, Target) + sLineBreak + format (tcMaxLength, [FLimit]);
end;

{ MinLengthAttribute }

constructor MinLengthAttribute.Create(Limit: uint64);
begin
  inherited Create;
  FLimit := Limit;
end;

function MinLengthAttribute.Validate(p: TRttiProperty; Target: TObject): string;
var
  ok : boolean;
begin
    case p.PropertyType.TypeKind of
      tkChar, tkString, tkLString:  ok := length (p.GetValue(Target).AsString) >= FLimit;
      else raise Exception.Create('Invalid property for MaxLengthAttribute');
    end;

  if ok
     then result := ''
     else result := ValidationFailedText (p.Name, tcFieldXTooShort, Target) + sLineBreak + format (tcMinLength, [FLimit]);
end;

{ ExactLengthAttribute }

constructor ExactLengthAttribute.Create(Limit: uint64);
begin
  inherited Create;
  FLimit := Limit;
end;

function ExactLengthAttribute.Validate(p: TRttiProperty; Target: TObject): string;
var
  ok : boolean;
begin
    case p.PropertyType.TypeKind of
      tkChar, tkString, tkLString:  ok := length (p.GetValue(Target).AsString) = FLimit;
      else raise Exception.Create('Invalid property for ExactLengthAttribute');
    end;

  if ok
     then result := ''
     else result := ValidationFailedText (p.Name, tcFieldXWrongLength, Target) + sLineBreak + format (tcExactLength, [FLimit]);
end;

initialization
  ValidationDictionary := TDictionary<string, string>.Create;

finalization
  FreeAndNil (ValidationDictionary);

end.

