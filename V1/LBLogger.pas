unit LBLogger;

interface

uses Classes, SysUtils, SyncObjs, IOUtils, math;

const
  Logger_buffer_size = 1000;

type

  TStreamEx = class helper for TStream
    procedure writeString(const data: string);
  end;

  TLogEntry = record
    dt: TDateTime;
    s: string;
    end;

  ILogObserver = interface
    procedure OnLogWrite (dt: TDateTime; s: string);
  end;

  TLogFile = class (TThread)
  private
    FObserver: ILogObserver;
    ReadCursor, WriteCursor: integer;
    Semaphore: TSemaphore;
    procedure OpenLogFile;
    procedure SetObserver(const Value: ILogObserver);
    procedure CheckLogSize;
    procedure AcquireSemaphore;
  public
    fsLog: TFileStream;
    MinLogSize, MaxLogSize: cardinal;
    FileName, TempFileName: TFileName;
    Buffer: array [0 .. Logger_buffer_size - 1] of TLogEntry;
    FailedLogs: integer;
//    Mutex: TMutex;
    DateTimeFormat: string;
    CriticalFail, ClearFlag, ConsoleOutput: boolean;
    procedure Execute; override;
    procedure Write (const s: string);
    procedure Clear;
    function GetLastNLines (count: integer): TStringList;
    constructor Create (AFileName: TFileName; ADateTimeFormat: string; AMinLogSize, AMaxLogSize: cardinal);
    destructor Destroy; override;

    property Observer: ILogObserver read FObserver write SetObserver;
  end;

implementation

{ TLogFile }

function CreateWriteFileStream (fn : TFileName; Mode : word = fmCreate; Share : word = fmShareDenyWrite): TFileStream;
begin
  try
    if (Mode = fmCreate) and ((Share and fmShareExclusive) = 0)
       then begin
              TFile.WriteAllText(fn, '');
              mode := fmOpenWrite or Share;
            end
       else if (Mode <> fmCreate)
            then mode := mode or Share;
    result := TFileStream.Create (fn, mode);
  except on e: exception do
    raise Exception.Create ('Unable to create file ' + fn + #13#13 + e.Message);
  end;
end;

function CreateReadFileStream (fn : TFileName; Mode : word = fmOpenRead or fmShareDenyNone): TFileStream;
begin
  try
    result := TFileStream.Create (fn, mode);
  except on e: exception do
    raise Exception.Create ('Unable to open file '+fn+#13#13+e.Message);
  end;
end;

procedure TLogFile.AcquireSemaphore;
var
  wr: TWaitResult;
begin
  wr := Semaphore.WaitFor (5000);
  if wr = TWaitResult.wrTimeout then
    begin
      CriticalFail := true;
      raise Exception.Create('Log semaphore acquire timeout, giving up');
    end;
end;

procedure TLogFile.Clear;
begin
  AcquireSemaphore;

  WriteCursor := ReadCursor;
  ClearFlag := true;

  Semaphore.Release;
end;

constructor TLogFile.Create(AFileName: TFileName; ADateTimeFormat: string; AMinLogSize, AMaxLogSize: cardinal);
begin
  inherited Create (false);
  Semaphore := TSemaphore.Create;
  MinLogSize := AMinLogSize;
  MaxLogSize := AMaxLogSize;
  FileName := AFileName;
  TempFileName := AFileName + '.tmp';
//  Mutex := TMutex.Create;
  DateTimeFormat := ADateTimeFormat;
  FreeOnTerminate := false;
end;

procedure TLogFile.OpenLogFile;
begin
  try
    try
      if not FileExists(FileName)
         then fsLog := CreateWriteFileStream(FileName, fmCreate, fmShareDenyWrite)
         else fsLog := CreateWriteFileStream(FileName, fmOpenReadWrite, fmShareDenyWrite);
    except
      sleep (2000);
      if not FileExists(FileName)
         then fsLog := CreateWriteFileStream(FileName, fmCreate, fmShareDenyWrite)
         else fsLog := CreateWriteFileStream(FileName, fmOpenReadWrite, fmShareDenyWrite);
    end;
    if fsLog <> nil
       then fsLog.Position := fsLog.Size;
  except on e: exception do
    begin
      fsLog := nil;
    end;
  end;
end;

destructor TLogFile.Destroy;
begin
//  Mutex.Free;
  inherited;
end;

procedure TLogFile.Write(const s: string);
var
  NextWriteCursor: integer;
  dt: TDateTime;
begin
  dt := now;

  if CriticalFail then exit;

  AcquireSemaphore;

  // calculate next write cursor
  NextWriteCursor := WriteCursor + 1;
  if NextWriteCursor = Logger_buffer_size then NextWriteCursor := 0;

  // read and write cursors cant become identical after writing as that would mean buffer is full
  while (ReadCursor = NextWriteCursor) do
    begin // wait for buffer to be emptied
      Semaphore.Release;
      TThread.Yield;//sleep (1);

      AcquireSemaphore;

      if FailedLogs < 100000
         then inc (FailedLogs)
         else begin
                CriticalFail := true;
                raise Exception.Create('Log fail counter triggered');
                exit;
              end;
    end;

  Buffer[WriteCursor].dt := dt;
  Buffer[WriteCursor].s := s;
  WriteCursor := NextWriteCursor;
  Semaphore.Release;

  if ConsoleOutput
     then WriteLn (s);
end;

procedure TLogFile.SetObserver(const Value: ILogObserver);
begin
  FObserver := Value;
end;

procedure TLogFile.CheckLogSize;
var
  fsTemp: TFileStream;
begin
  if (fsLog <> nil) and (fsLog.Size > MaxLogSize) then
      begin
        FreeAndNil (fsLog);
        if FileExists (TempFileName)
           then TFile.Delete (TempFileName);
        if not FileExists (FileName)
           then if now = 0 then exit;

        TFile.Move (FileName, TempFileName);
        OpenLogFile;
        try
          fsTemp := CreateReadFileStream(TempFileName, fmShareDenyWrite);
          fsTemp.Position :=  fsTemp.Size - MinLogSize - 1;
          fsLog.CopyFrom(fsTemp, MinLogSize);
          fsTemp.Free;
          TFile.Delete (TempFileName);
        except on e: exception do
          Write (e.message);
        end;
      end;
end;

procedure TLogFile.Execute;
var
  s: string;
  finished: boolean;
  dt: TDateTime;
  SleepTime: integer;
begin
  inherited;

  ForceDirectories (TPath.GetDirectoryName (FileName));
  OpenLogFile;
  SleepTime := 10;

  while true do
    begin
      while true do
        begin
          AcquireSemaphore;

          if ClearFlag then
            begin
              FreeAndNil (fsLog);
              TFile.Delete (FileName);
              OpenLogFile;
              ClearFlag := false;
            end;

          Finished := ReadCursor = WriteCursor;
          if finished
            then
              begin
                Semaphore.Release;
                SleepTime := min (1000, SleepTime + 10);
                break;
              end
            else
              begin
                SleepTime := 10;
                {$IFDEF DEBUGLOGGER}
                fsLog.writeString(format ('Logger RC %d WC %d'+CRLF, [ReadCursor, WriteCursor]));
                {$ENDIF}

                s :=  trim (Buffer[ReadCursor].s);
                dt := Buffer[ReadCursor].dt;
                inc (ReadCursor);
                if ReadCursor = Logger_buffer_size
                   then ReadCursor := 0;
                Semaphore.Release;

                s := FormatDateTime (DateTimeFormat, dt) + ' ' + s + #13#10;
                fsLog.writeString(s);
                //sleep (1);
                if Observer <> nil
                   then Queue (nil, procedure begin Observer.OnLogWrite(dt, s); end);
              end;
        end;

      CheckLogSize;

      if Terminated then break;// this way it will flush the buffer even if terminated, rather than do "while not terminated"
      sleep (SleepTime);
    end;

  FreeAndNil (fsLog);
  FreeAndNil (Semaphore);

end;

function TLogFile.GetLastNLines(count: integer): TStringList;
var
  i: integer;
begin
  result := TStringList.Create;
  if count > Logger_buffer_size then count := Logger_buffer_size;

  AcquireSemaphore;
  i := WriteCursor-1;

  while result.Count < count do
    begin
      if i < 0 then i := Logger_buffer_size - 1;
      if Buffer[i].dt = 0 then break;

      result.Insert (0, FormatDateTime (DateTimeFormat, Buffer[i].dt) + ' ' + Buffer[i].s);
      dec (i);
    end;
  Semaphore.Release;
end;

{ TStreamEx }

procedure TStreamEx.writeString(const data: string);
var
   len: cardinal;
   oString: UTF8String;
begin
   oString := UTF8String(data);
   len := length(oString);
   //self.WriteBuffer(len, 4);
   if len > 0 then
      self.WriteBuffer(oString[1], len);
end;

{
Initial condition read and write cursors are zero

writing moves WC

keep reading from buffer until cursors are identical

when writing to log, write cursor cant become read cursor as that would mean there is nothing to write

}

end.
