unit DumpGrid;

interface

uses Types, Graphics, Classes, SysUtils, math, vcl.Grids, System.Generics.Collections, System.TypInfo, RTTI,
     windows,
     LBUtils, mORMot, MormotMods, EventBus, ebTasks;

type

  TDumpGridEventInfo = record
    Row, Column: integer;
    Operation: TCRUD;
  end;

  IDumpGridEvent = interface (IInvokable)
  ['{F7212A76-D4A7-4F00-85E7-08C04DF2B07F}']
    function GetInfo: TDumpGridEventInfo;
    procedure SetInfo (AInfo: TDumpGridEventInfo);
    property Info: TDumpGridEventInfo read GetInfo write SetInfo;
  end;

  TDumpGridEvent = class (TInterfacedObject, IDumpGridEvent)
  private
    FInfo: TDumpGridEventInfo;
  public
    function GetInfo: TDumpGridEventInfo;
    procedure SetInfo (AInfo: TDumpGridEventInfo);
    property Info: TDumpGridEventInfo read GetInfo write SetInfo;
  end;

  TItemKind = (None, TableName, FieldName, Value, New);

  TSection = class
    Name: string;
    Color: TColor;
    Row, Col, LastRow, LastCol, Count: integer;
    Fields: array of string;
    Records: TSQLRecordObjArray;
    procedure GetDisplay (ARow, ACol: integer; var Text: string; var AColor: TColor; var ItemKind: TItemKind);
    function FindRecord (ARow, ACol: integer): TSQLRecord;
    destructor Destroy; override;
  end;

  TSections = class (TObjectList <TSection>)
    Map: TDictionary <cardinal, byte>;
    function Find (ARow, ACol: integer): TSection;
    function FindRecord (ARow, ACol: integer): TSQLRecord;
    procedure GetDisplay (ARow, ACol: integer; var Text: string; var AColor: TColor; var ItemKind: TItemKind);
    function Add(MaxRow, MaxCol: integer; Name: string; Color: TColor; const Records: TSQLRecordObjArray; const Fields: array of string): TSection;
    procedure Clear;
    constructor Create;
    destructor Destroy; override;

  end;

  TDumpGrid = class (TDrawGrid)
  private
  protected
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
  public
    Sections: TSections;
    MaxRows, MaxCols: integer;
    constructor Create (AOwner: TComponent); override;
    destructor Destroy; override;
    procedure KeyUp(var Key: Word; Shift: TShiftState);  override;
    procedure Clear;
    procedure Add(Name: string; Color: TColor; Records: TSQLRecordObjArray; const Fields: array of string);
  end;


implementation

const
  clSelectedRowBorder = $008CFF;

{ TSection }

destructor TSection.Destroy;
begin
{  for var rec in Records do
    rec.Free;}

  inherited;
end;

function TSection.FindRecord(ARow, ACol: integer): TSQLRecord;
begin
  var LocalRow := ARow - Row;

  if (LocalRow > 1) and (LocalRow < LastRow)
     then result := Records[LocalRow-2]
     else result := nil;
end;

procedure TSection.GetDisplay(ARow, ACol: integer; var Text: string; var AColor: TColor; var ItemKind: TItemKind);
begin
  var LocalRow := ARow - Row;
  var LocalCol := ACol - Col;

  case LocalRow of
    0 :   begin
            ItemKind := TItemKind.TableName;
            AColor := Color - $202020;
            if LocalCol = 0 then Text := Name else Text := '';
          end;
    1 :   begin
            ItemKind := TItemKind.FieldName;
            AColor := Color - $101010;
            Text := Fields[LocalCol];
          end
    else  begin
            if LocalRow-2 = length(Records)
              then
                begin
                  ItemKind := TItemKind.New;
                  AColor := Color + $202020;
                  if LocalCol = 0
                     then Text := '[ + ]'
                     else Text := '';
                end
              else
                begin
                  ItemKind := TItemKind.Value;
                  if ARow mod 2 = 0
                     then AColor := Color
                     else AColor := Color + $101010;
                  Text := Records[LocalRow-2].GetFieldStringValue(Fields[LocalCol]);
                end
          end;
  end;
end;

{ TSections }

function TSections.Add(MaxRow, MaxCol: integer; Name: string; Color: TColor; const Records: TSQLRecordObjArray; const Fields: array of string): TSection;
var
  TempRowCount, ColCount, row, BestRow, BestCol: integer;
begin
  result := TSection.Create;
  ColCount := length (Fields);

  row := 0;
  BestRow := 0;
  BestCol := 0;
  while row < MaxRow do
    begin
      if not Map.ContainsKey (row) then
        begin
          BestRow := row;
          BestCol := 0;
          break;
        end;

      if (Map[row] + ColCount < MaxCol)
        then
          begin
            TempRowCount := length (Records) + 2;
            while (TempRowCount > 0) and
                  ( Map.ContainsKey (row) and (Map[row] + ColCount < MaxCol) ) do
              begin
                BestCol := max (BestCol, Map[row]);
                inc(row);
                dec (TempRowCount);
              end;
          if TempRowCount = 0 then
            begin
              BestRow := row - length (Records) - 2;
              break;
            end;
          end;
        // else
          inc (row);
    end;

  result.Row := BestRow;
  result.Col := BestCol;

  for row := BestRow to BestRow + 2 + length(Records) + 1 {for separation} do
    Map.AddOrSetValue (row, BestCol + ColCount + 1);

  result.Records := Records;
  setlength (result.Fields, length (Fields));
  for var i := low (Fields) to High (Fields) do result.Fields[i] := Fields[i];

  result.Name := name;
  result.Color := Color;
  result.LastRow := result.Row + 2 + length(Records);
  result.LastCol := result.Col + length(Fields) - 1;
  inherited Add (result);
end;

procedure TSections.Clear;
begin
  inherited;
  Map.Clear;
end;

constructor TSections.Create;
begin
  inherited Create;
  Map := TDictionary<cardinal, byte>.Create;
end;

destructor TSections.Destroy;
begin
  Map.Free;
  inherited;
end;

function TSections.Find(ARow, ACol: integer): TSection;
begin
  for result in self do
    if (ARow >= result.Row) and (ACol >= result.Col)
       and (ARow <= result.LastRow) and (ACol <= result.LastCol)
         then exit;
  result := nil;
end;

function TSections.FindRecord(ARow, ACol: integer): TSQLRecord;
begin
  var Section := Find (ARow, ACol);
  if Section <> nil
     then result := Section.FindRecord (ARow, ACol)
     else result := nil;
end;

procedure TSections.GetDisplay(ARow, ACol: integer; var Text: string; var AColor: TColor; var ItemKind: TItemKind);
begin
  var section := Find (ARow, ACol);
  if section <> nil
    then section.GetDisplay(Arow, ACol, Text, AColor, ItemKind)
    else ItemKind := TItemKind.None;
end;

procedure TDumpGrid.Clear;
begin
  Sections.Clear;
  RowCount := 2;
  invalidate;
end;

procedure TDumpGrid.Add(Name: string; Color: TColor; Records: TSQLRecordObjArray; const Fields: array of string);
begin
  var section := Sections.Add(MaxRows, MaxCols, Name, Color, Records, Fields);
  RowCount := max (RowCount, section.LastRow + 1);
  ColCount := max (ColCount, section.LastCol + 1);
  invalidate;
end;

constructor TDumpGrid.Create(AOwner: TComponent);
begin
  inherited;
  Sections := TSections.Create;
  FixedCols := 0;
  RowCount := 2;
  ColCount := 4;
  MaxCols := 20;
  MaxRows := 200;

//  DefaultDrawing := false;
end;

destructor TDumpGrid.Destroy;
begin
  Clear;
  FreeAndNil (Sections);
  inherited;
end;

procedure TDumpGrid.DrawCell(ACol, ARow: Integer; ARect: TRect; AState: TGridDrawState);
var
  Text: string;
  ItemKind: TItemKind;
  CellColor: TColor;

  procedure TextOut (const s: string);
  var
    Size: TSize;
  begin
    Size := Canvas.TextExtent (s);
    Size.cx := min (ClientWidth div 2, Size.cx);
    if Size.cx + 16 > ColWidths [ACol] then
       ColWidths [ACol] := Size.cx + 20;
    if Size.cy + 6 > RowHeights [ARow] then
       RowHeights [ACol] := Size.cy + 6;
    InflateRect(ARect, -5, -2);
    //Canvas.TextOut (ARect.Left + 3, ARect.Top + 5, s  aaa);
    DrawText(Canvas.Handle, PChar(Text), Length(Text), ARect, DT_VCENTER or DT_SINGLELINE or DT_NOPREFIX or DT_NOCLIP);
  end;

begin
  inherited;

  Sections.GetDisplay(ARow, ACol, Text, CellColor, ItemKind);

  if ItemKind = TItemKind.None
     then Canvas.Brush.Color := Color
     else Canvas.Brush.Color := CellColor;

  Canvas.Pen.Color := clGray;
  InflateRect(ARect, 1, 1);
  if ItemKind <> TItemKind.TableName then
    begin
      Canvas.Rectangle (ARect);
      InflateRect(ARect, -1, -1);
    end;
  Canvas.FillRect(ARect);

  if ItemKind = TItemKind.None then exit;

  case ItemKind of
    TItemKind.TableName :
      begin
        Canvas.Font.Size := Font.Size + 2;
        Canvas.Font.Style := [fsBold, fsUnderline];
      end;
    TItemKind.FieldName :
      begin
        Canvas.Font.Size := Font.Size + 1;
        Canvas.Font.Style := [fsBold];
      end;
    else
      begin
        Canvas.Font.Size := Font.Size;
        Canvas.Font.Style := [];
      end;
    end;

  TextOut(Text);

{  if ARow = Row then
    begin
      Canvas.Pen.Color := clSelectedRowBorder;
      Canvas.MoveTo(ARect.Left, ARect.Top+1);
      Canvas.LineTo(ARect.Right-1, ARect.Top+1);
      Canvas.MoveTo(ARect.Left, ARect.Bottom-1);
      Canvas.LineTo(ARect.Right-1, ARect.Bottom-1);
    end;
}
end;

var ev : TDumpGridEvent;

procedure TDumpGrid.KeyUp(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_DELETE) and (Sections.FindRecord (Row, Col) <> nil) then
    begin
      //var
      ev := TDumpGridEvent.Create;
      var info: TDumpGridEventInfo;
      info.Row := Row;
      info.Column := Col;
      info.Operation := TCRUD.Delete;
      ev.Info := info;
      GlobalEventBus.Post (ev as IDumpGridEvent);
    end;
end;

{ TDumpGridEvent }

{ TDumpGridEvent }

function TDumpGridEvent.GetInfo: TDumpGridEventInfo;
begin
  result := FInfo;
end;

procedure TDumpGridEvent.SetInfo(AInfo: TDumpGridEventInfo);
begin
  FInfo := AInfo;
end;

end.
