unit FMX.ExifOrientation;

interface
uses
  System.Classes,
  System.SysUtils,
  System.Generics.Collections,
  FMX.Graphics,
  CCR.Exif;

type

  TExifRotateFlip = class
  private
    FFileName : string;
    FBitmap : TBitmap;
    FExifData: TExifData;
    FRotationList, FVerticalFlipList, FHorizontalFlipList : TList<TTiffOrientation>;
    procedure Rotate(AntiClockwise : boolean = true);
  public
    class procedure LoadExifImage(Bitmap : TBitmap; FileName : string);
    class procedure SaveExifImage(Bitmap : TBitmap; FileName : string; ExifData: TExifData = nil); overload;
    class procedure OrientExifImage(Bitmap : TBitmap; ExifData: TExifData);
    class function OrientationDescription(Value: TTiffOrientation): string; overload;
    class function OrientationToStr(Value: TTiffOrientation): string; overload;
    class function StrToOrientation(OrientationName: string) : TTiffOrientation; overload;

    function OrientationDescription: string; overload;
    function OrientationToStr: string; overload;
    function StrToOrientation : TTiffOrientation; overload;

    procedure ImportExifImage(Filename : TFilename); overload;
    procedure ImportExifImage(Stream : TStream); overload;
    procedure DrawRawfImage(Destination : TBitmap);
    procedure DrawExifImage(Destination : TBitmap);

    procedure ExportExifImage(Filename : TFilename); overload;
    procedure ExportExifImage(Stream : TStream); overload;
    procedure SaveExifImage; overload;
    procedure RotateClockwise;
    procedure RotateAnticlockwise;
    procedure FlipVertical;
    procedure FlipHorizontal;

    property ExifData : TExifData read FExifData;

    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  System.Rtti;

class procedure TExifRotateFlip.OrientExifImage(Bitmap : TBitmap; ExifData: TExifData);
begin
  if not (Assigned(ExifData) and Assigned(Bitmap)) then Exit;

  case ExifData.Orientation of
    toUndefined, toTopLeft:;
    toTopRight: Bitmap.FlipHorizontal;
    toBottomRight: Bitmap.Rotate(180);
    toBottomLeft: Bitmap.FlipVertical;
    toLeftTop: begin
      Bitmap.FlipHorizontal;
      Bitmap.Rotate(270);
    end;
    toRightTop: Bitmap.Rotate(90);
    toRightBottom: begin
      Bitmap.FlipHorizontal;
      Bitmap.Rotate(90);
    end;
    toLeftBottom: Bitmap.Rotate(270);
  end;
end;

procedure TExifRotateFlip.Rotate(AntiClockwise: boolean);
begin
// if not Assigned(FExifData) then
//    FExifData := TExifData.Create;
  var OI := FRotationList.IndexOf(ExifData.Orientation);
  if OI = -1 then
    OI := 0;
  var ni := (oi div 4) * 4 + (OI + (1 + ord(AntiClockwise) * 2)) mod 4;
  ExifData.Orientation := FRotationList[ni];
end;

procedure TExifRotateFlip.RotateAnticlockwise;
begin
  Rotate(true);
end;

procedure TExifRotateFlip.RotateClockwise;
begin
  Rotate(false);
end;

class procedure TExifRotateFlip.LoadExifImage(Bitmap : TBitmap; FileName : string);
var
  ExifData: TExifData;
begin
  ExifData := TExifData.Create;
    try
      ExifData.EnsureEnumsInRange := False;
      ExifData.LoadFromGraphic(FileName);
      Bitmap.LoadFromFile(FileName);
      OrientExifImage(Bitmap, ExifData);
    finally
      ExifData.Free;
    end;
end;

class procedure TExifRotateFlip.SaveExifImage(Bitmap : TBitmap; FileName : string; ExifData: TExifData);
begin
  Bitmap.SaveToFile(FileName);
  if Assigned(ExifData) then
    ExifData.SaveToGraphic(FileName);
end;

procedure TExifRotateFlip.SaveExifImage;
begin
  SaveExifImage(FBitmap, FFileName, FExifData);
end;

function TExifRotateFlip.StrToOrientation: TTiffOrientation;
begin
  Result := StrToOrientation()
end;

class function TExifRotateFlip.OrientationDescription(Value: TTiffOrientation): string;
begin
  case Value of
    toUndefined: Result := 'Undefined';                           //0
    toTopLeft: Result := 'Normal';                                //1
    toTopRight: Result := 'Mirror horizontal';                    //2
    toBottomRight: Result := 'Rotate 180�';                       //3
    toBottomLeft: Result := 'Mirror vertical';                    //4
    toLeftTop: Result := 'Mirrow horizontal and rotate 270�';     //5
    toRightTop: Result := 'Rotate 90�';                           //6
    toRightBottom: Result := 'Mirror hotizontal and rotate 90�';  //7
    toLeftBottom: Result := 'Rotate 270�';                        //8
  else FmtStr(Result, '[Unrecognised value (%d)]', [Ord(Value)]);
  end;
end;

class function TExifRotateFlip.OrientationToStr(Value: TTiffOrientation): string;
begin
  Result := TRttiEnumerationType.GetName(Value);
end;

class function TExifRotateFlip.StrToOrientation(OrientationName: string): TTiffOrientation;
begin
  Result := TRttiEnumerationType.GetValue<TTiffOrientation>(OrientationName);
end;

function TExifRotateFlip.OrientationDescription: string;
begin
  Result := OrientationDescription(FExifData.Orientation);
end;

function TExifRotateFlip.OrientationToStr: string;
begin
  Result := OrientationToStr(FExifData.Orientation);
end;

constructor TExifRotateFlip.Create;
begin
  inherited;
  FExifData := TExifData.Create(nil);
  FRotationList := TList<TTiffOrientation>.Create;
  FRotationList.AddRange([toTopLeft, toRightTop, toBottomRight, toLeftBottom,
    toTopRight, toBottomLeft, toRightBottom, toLeftTop]);

  FVerticalFlipList := TList<TTiffOrientation>.Create;
  FVerticalFlipList.AddRange([toBottomLeft, toBottomLeft, toBottomRight, toTopRight, toTopLeft,
    toLeftBottom, toRightBottom, toRightTop, toLeftTop ]);

  FHorizontalFlipList := TList<TTiffOrientation>.Create;
  FHorizontalFlipList.AddRange([toTopRight, toTopRight, toTopLeft, toBottomLeft, toBottomRight,
    toRightTop, toLeftTop, toLeftBottom, toRightBottom ]);
end;

destructor TExifRotateFlip.Destroy;
begin
  FreeAndNil(FBitmap);
  FreeAndNil(FExifData);
  FreeAndNil(FRotationList);
  FreeAndNil(FVerticalFlipList);
  FreeAndNil(FHorizontalFlipList);
  inherited;
end;

procedure TExifRotateFlip.DrawExifImage(Destination: TBitmap);
begin
  DrawRawfImage(Destination);
  OrientExifImage(Destination, FExifData);
end;

procedure TExifRotateFlip.DrawRawfImage(Destination: TBitmap);
begin
  Destination.Size := FBitmap.Size;
  Destination.CopyFromBitmap(Fbitmap);
end;

procedure TExifRotateFlip.ExportExifImage(Stream: TStream);
begin
  FBitmap.SaveToStream(Stream);
  FExifData.SaveToStream(Stream);
end;

procedure TExifRotateFlip.FlipHorizontal;
begin
  FExifData.Orientation := FHorizontalFlipList[ord(FExifData.Orientation)];
end;

procedure TExifRotateFlip.FlipVertical;
begin
  FExifData.Orientation := FVerticalFlipList[ord(FExifData.Orientation)];
end;

procedure TExifRotateFlip.ExportExifImage(Filename: TFilename);
begin
  SaveExifImage(FBitmap, Filename, FExifData);
end;

procedure TExifRotateFlip.ImportExifImage(Stream: TStream);
begin
  FBitmap.Free;
  FBitmap := TBitmap.Create;
  Stream.Position := 0;
  FBitmap.LoadFromStream(Stream);
  Stream.Position := 0;
  FExifData.LoadFromGraphic(Stream);
  Stream.Position := 0;
end;

procedure TExifRotateFlip.ImportExifImage(Filename: TFilename);
begin
  FBitmap.Free;
  FBitmap := TBitmap.Create;
  FBitmap.LoadFromFile(filename);
  FFileName := Filename;
end;
end.
