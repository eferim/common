unit MormotCallbacks;

// Implementation of callback mechanism

interface

uses
  Classes, System.Generics.Collections, System.SyncObjs, Contnrs, math, SysUtils,
  mORMot, SynCommons,
  mormotMods;

type

  TChatInfo = record
    UserID: TID;
    Content: string;
  end;

  TLoginAction = (Login, Logout);

  ICallback = interface(IInvokable)
    ['{D5B663B5-4B47-4C28-AF90-E6F2C2EA8661}']
    procedure UserLoginAction  (UserID: TID; Action: TLoginAction);
    procedure Chat (User: TID; Text: RawUTF8);
  end;

  ICallbackServices = interface(IServiceWithCallbackReleased)
    ['{47BD23D2-3892-4A54-BDDE-C32974F9765F}']
    function RegisterCallback   (const callback: ICallback): boolean;
    procedure UnRegisterCallback (const callback: ICallback);
    procedure Chat (Text: RawUTF8);
  end;


  TCallbackUserPair = TPair<ICallback, TID>;

  TCallbackUsers = TDictionary<ICallback, TID>;

  TCallbackActionKind = (Register, Chat, Unregister);

  TCallbackAction = record
    Kind: TCallbackActionKind;
    User: TID;
    Callback: ICallback;
    Content: RawUTF8;
  end;

  TCallbackServiceThread = class (TThread)
    private
      FActionStack:     TList<TCallbackAction>;
      FCallbackUsers:   TCallbackUsers;
      FUsers:           TList<TID>;
      FCriticalSection: TCriticalSection;
    protected
      FRestServer     : TSQLRestServer;

      // direct calls to ICallback methods, i.e. notifications
      procedure FUserLoggedIn  (User: TID);
      procedure FUserLoggedOut (User: TID); virtual;
      procedure FChat(User: TID; Text: RawUTF8);
      // ===

      procedure FCallbackCrashed(Callback: ICallback);
    public
      constructor Create (ARestServer: TSQLRestServer);
      destructor Destroy; override;
      procedure Execute; override;

      procedure RegisterCallback(const Callback: ICallback; const User: TID);
      procedure UnregisterCallback(const Callback: ICallback);
      procedure Chat (UserID: TID; Text: RawUTF8);
      function  GetUserList: TIDDynArray;
    end;

  TCallbackService = class(TInterfacedObject, ICallbackServices)
  private
    function Thread: TCallbackServiceThread;
  protected
    function CreateThread: TCallbackServiceThread; virtual; abstract;
  public
    constructor Create;
    destructor Destroy; override;

    function RegisterCallback   (const callback: ICallback): boolean; virtual;
    procedure UnRegisterCallback (const callback: ICallback);
    procedure CallbackReleased   (const callback: IInvokable; const interfaceName: RawUTF8);
    procedure Chat (Text: RawUTF8);
  end;

implementation

var
  CallbackServiceThread : TCallbackServiceThread;

{ TCallbackServiceThread }

constructor TCallbackServiceThread.Create (ARestServer: TSQLRestServer);
begin
  inherited Create;
  FRestServer := ARestServer;
  FCriticalSection := TCriticalSection.Create;
  FCallbackUsers := TCallbackUsers.Create;
  FUsers         := TList<TID>.Create;
  fActionStack := TList<TCallbackAction>.Create;
end;

destructor TCallbackServiceThread.Destroy;
begin
  FCriticalSection.Free;
  FCallbackUsers.Free;
  FUsers.Free;
  fActionStack.Free;
  inherited;
end;

procedure TCallbackServiceThread.RegisterCallback(const Callback: ICallback; const User: TID);
var
  cba: TCallbackAction;
begin
  cba.Kind := TCallbackActionKind.Register;
  cba.Callback := Callback;
  cba.User := User;

  FCriticalSection.Acquire;
  try
    fActionStack.Add(cba);
  finally
    FCriticalSection.Release;
  end;
end;

procedure TCallbackServiceThread.UnregisterCallback (const callback: ICallback);
var
  cba: TCallbackAction;
begin
  cba.Kind := TCallbackActionKind.UnRegister;
  cba.Callback := Callback;
  FCriticalSection.Acquire;
  try
    fActionStack.Insert (0, cba); // prioritise removal
  finally
    FCriticalSection.Release;
  end;
end;

procedure TCallbackServiceThread.Chat (UserID: TID; Text: RawUTF8);
var
  cba: TCallbackAction;
begin
  cba.Kind := TCallbackActionKind.Chat;
  cba.Content := Text;
  cba.User := UserID;
  FCriticalSection.Acquire;
  try
    fActionStack.Add(cba);
  finally
    FCriticalSection.Release;
  end;
end;

procedure TCallbackServiceThread.Execute;
var
  User: TID;
  Action: TCallbackAction;
  LocalStack, NewStack: TList<TCallbackAction>;
  WorkDone: boolean;
begin
  inherited;
  LocalStack := TList<TCallbackAction>.Create;

  while not Terminated do
    begin
      User := NULL_ID;

      // swap stack
      FCriticalSection.Acquire;
      NewStack     := FActionStack;  // this will be local stack in a moment
      FActionStack := LocalStack;    // the empty local stack becomes the stack to receive new logins
      LocalStack   := NewStack;      // local stack is the previous stack with logins
      FCriticalSection.Release;
      WorkDone := LocalStack.Count > 0;

      while LocalStack.Count > 0 do
        try
          Action := LocalStack.ExtractAt (0);
          case Action.Kind of
            TCallbackActionKind.Register   : begin
                                               FCallbackUsers.Add(Action.Callback, Action.User);
                                               FUsers.Add (Action.User);
                                               FUserLoggedIn (Action.User);
                                             end;
            TCallbackActionKind.Unregister : begin
                                               if FCallbackUsers.TryGetValue (Action.Callback, User) then
                                                 begin
                                                   FCallbackUsers.Remove(Action.Callback);
                                                   FUsers.Remove (User);

                                                   if not FUsers.Contains(User) then // last login removed
                                                      FUserLoggedOut(User);
                                                 end;
                                             end;
            TCallbackActionKind.Chat       : FChat (Action.User, Action.Content);
          end;

        finally

        end;

      if not WorkDone and not Terminated
         then sleep (250); // there was no work
    end;

  LocalStack.Free;
end;

procedure TCallbackServiceThread.FCallbackCrashed (Callback: ICallback);
begin
  UnregisterCallback (Callback);
end;

procedure TCallbackServiceThread.FChat(User: TID; Text: RawUTF8);
var
  pair: TCallbackUserPair;
begin // works with copy of Users list
  for pair in FCallbackUsers do
    try
      pair.Key.Chat (User, Text);
    except
      FCallbackCrashed (pair.Key);
    end;
end;

procedure TCallbackServiceThread.FUserLoggedIn(User: TID);
var
  pair: TCallbackUserPair;
begin
  for pair in FCallbackUsers do
    try
      pair.Key.UserLoginAction (User, TLoginAction.Login);
    except
      FCallbackCrashed (pair.Key);
    end;
end;

procedure TCallbackServiceThread.FUserLoggedOut(User: TID);
var
  pair: TCallbackUserPair;
begin
  for pair in FCallbackUsers do
    try
      pair.Key.UserLoginAction (User, TLoginAction.Logout);
    except
      FCallbackCrashed (pair.Key);
    end;
end;

function TCallbackServiceThread.GetUserList: TIDDynArray;
begin
  result := nil;
end;

{ TCallbackService }

procedure TCallbackService.Chat(Text: RawUTF8);
begin
  Thread.Chat(ServiceContext.Request.SessionUser, Text);
end;

constructor TCallbackService.Create;
begin
  inherited; //TODO NB this is never called, part of reason of thread existance
end;

destructor TCallbackService.Destroy;
begin
  inherited;
end;

procedure TCallbackService.CallbackReleased(const callback: IInvokable; const interfaceName: RawUTF8);
begin
  if interfaceName='ICallback'
    then
      begin
        WriteLn ('Callback released', uint64(pointer(callback)));
        Thread.UnregisterCallback (callback as ICallback);
      end;
end;

function TCallbackService.RegisterCallback (const callback: ICallback): boolean;
begin
  WriteLn ('Registering callback ', uint64(pointer(callback)));
  Thread.RegisterCallback(callback, ServiceContext.Request.SessionUser);
  result := true;
  //TODO logged on status je u stvari registered callback status jer osim toga nema trajnih konekcija
end;

function TCallbackService.Thread: TCallbackServiceThread;
begin
  if CallbackServiceThread = nil
    then CallbackServiceThread := CreateThread;

  result := CallbackServiceThread;
end;

procedure TCallbackService.UnRegisterCallback(const callback: ICallback);
begin
  Thread.UnregisterCallback (callback);
end;

initialization


finalization
CallbackServiceThread.Free;


end.
