unit FMX.LBUtils;

interface

uses
  System.Types,
  System.UITypes,
  System.SysUtils,
  System.Generics.Collections,
  math,
  LBUtils,
  FMX.Controls,
  FMX.Dialogs,
  FMX.DialogService.Sync,
  FMX.Forms,
  FMX.Grid,
  FMX.DateTimeCtrls,
  FMX.Objects,
  FMX.Types,
  FMX.Platform,
  FMX.Layouts;



type

  TApplicationHelper = class helper for TApplication
  public
    function ExeName: String;
  end;


  TDateEditHelper = class helper for TDateEdit
    procedure SetSmartDate (d: TDate);
  end;

  TStringGridHelper = class helper for TStringGrid
  type
    TColumnClass = class of TColumn;
    function AddColumn (ColumnClass: TColumnClass; const Header: string; const TagString: string = ''; Width: integer = 0): TColumn;
    function CopyColumn (ACol : TColumn) : TColumn;
  end;

  TFLowLayoutlHelper = class helper for TFlowlayout
  public
    function ChildrenWidth: Single;
    function ChildrenHeight: Single;
  end;

  TRestrictedSizeForm = class (TForm)
    procedure Resize; override;
  public
    MinWidth, MinHeight: integer;
    procedure AfterConstruction; override;
  end;

  procedure FocusControlOrChild (c: TControl);
  procedure ShowErrorAndAbort (const Error: string);
  procedure ShowError (const Error: string);
  function ConfirmationDialog(const Text: string): boolean;

  procedure SetupModalAutoDimmer;

  procedure OpenURL(URL : string);

implementation

uses
{$IFDEF MSWINDOWS}
  Winapi.ShellAPI, Winapi.Windows;
{$ENDIF MSWINDOWS}
{$IFDEF ANDROID}
    Androidapi.JNI.GraphicsContentViewText,
    Androidapi.Helpers,
    Androidapi.JNI.Net;
{$ENDIF ANDROID}

type

  TAutoModalDimmer = class (TNonARCInterfacedObject, IFreeNotification)
  private
    Forms: TList <TCommonCustomForm>;
    Dimmers: TDictionary <TCommonCustomForm, TRectangle>;
    procedure FreeNotification(AObject: TObject);
    procedure OnIdle (Sender: TObject; var Done: boolean);
  public
    constructor Create;
    destructor Destroy; override;
  end;

var
  AutoModalDimmer: TAutoModalDimmer;

procedure OpenURL(URL : string);
begin
{$IFDEF MSWINDOWS}
  ShellExecute(0, 'OPEN', PChar(URL), '', '', SW_SHOWNORMAL);
{$ENDIF MSWINDOWS}
{$IFDEF ANDROID}
  var Intent := TJIntent.JavaClass.init(TJIntent.JavaClass.ACTION_VIEW,
      TJnet_Uri.JavaClass.parse(StringToJString(URL)));
  SharedActivity.startActivity(Intent);
{$ENDIF ANDROID}
end;

procedure SetupModalAutoDimmer;
begin
  AutoModalDimmer := TAutoModalDimmer.Create;
end;

constructor TAutoModalDimmer.Create;
begin
  inherited;
  Forms := TList <TCommonCustomForm>.Create;
  Dimmers := TDictionary <TCommonCustomForm, TRectangle>.Create;
  Application.OnIdle := OnIdle;
end;

destructor TAutoModalDimmer.Destroy;
begin
  Forms.Free;
  Dimmers.Free;
  inherited;
end;

procedure TAutoModalDimmer.FreeNotification(AObject: TObject);
begin
  Forms.Remove (TCommonCustomForm(AObject));
  Dimmers.Remove(TCommonCustomForm(AObject));
end;

procedure TAutoModalDimmer.OnIdle (Sender: TObject; var Done: boolean);
var
  i : integer;
  ModalForm: TCommonCustomForm;
begin
  ModalForm := Screen.ActiveForm;
  if (ModalForm <> nil) and not (TFmxFormState.Modal in ModalForm.FormState)
    then ModalForm := nil;

  for i := 0 to Screen.FormCount - 1 do
    begin
      var f := Screen.Forms[i];

      if f.Visible and Forms.Contains(f) and ( (ModalForm = nil) or (ModalForm = f) ) then
        begin
          Forms.Remove (f);
          Dimmers[f].Visible := false;
        end;

      if f.Visible and not Forms.Contains(f) and (ModalForm <> nil) and (ModalForm <> f) then
        begin
          if not Dimmers.ContainsKey (f)
            then begin
                   var fdim := TRectangle.Create (f);
                   fdim.Opacity := 0.5;
                   fdim.Parent := f;
                   Dimmers.Add(f, fdim);
                   f.AddFreeNotify(self);
                 end;
          Dimmers[f].SetBounds(0, 0, f.Width, f.Height);
          Dimmers[f].BringToFront;
          Dimmers[f].Visible := true;

          Forms.Add (f);
        end;
    end;
end;


function ConfirmationDialog(const Text: string): boolean;
begin
  result := TDialogServiceSync.MessageDialog(Text, TMsgDlgType.mtConfirmation,
      [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], TMsgDlgBtn.mbCancel, 0) = mrYes;
end;

{TControlHelper}

function TFLowLayoutlHelper.ChildrenWidth: Single;
var
  VIndex: Integer;
  VControl: TControl;
  VRect: TRectF;
begin
  VRect := TRectF.Empty;
  Result := VRect.Width;
  if not ( ClipChildren or SmallSizeControl ) and ( Controls <> nil ) then
  begin
    for VIndex := GetFirstVisibleObjectIndex to GetLastVisibleObjectIndex - 1 do
    begin
      VControl := Controls[ VIndex ];
      if VControl.Visible then
        VRect := System.Types.UnionRect( VRect, VControl.Margins.MarginRect( TRectF.Create( VControl.Position.Point, VControl.Width, VControl.Height ) ) );
    end;
    Result := VRect.Width + Padding.Right;
  end;

end;

function TFLowLayoutlHelper.ChildrenHeight: Single;
var
  VIndex: Integer;
  VControl: TControl;
  VRect: TRectF;
begin
  VRect := TRectF.Empty;
  Result := VRect.Height;
  if not ( ClipChildren or SmallSizeControl ) and ( Controls <> nil ) then
  begin
    for VIndex := GetFirstVisibleObjectIndex to GetLastVisibleObjectIndex - 1 do
    begin
      VControl := Controls[ VIndex ];
      if VControl.Visible then
        VRect := System.Types.UnionRect( VRect,
                 VControl.Margins.MarginRect(
                   TRectF.Create( VControl.Position.Point, VControl.Width, VControl.Height )
                   ) );
    end;
    Result := VRect.Height + Padding.Bottom + Padding.Top;
  end;
end;

procedure FocusControlOrChild (c: TControl);
begin
  if (c is TLayout) then
    begin
      var l := TLayout (c);
      for var sc in l.Controls do
        if (sc.TabOrder = 0) then
          if sc.CanFocus
            then
              begin
                sc.SetFocus;
                exit;
              end
            else if sc is TLayout then
              begin
                FocusControlOrChild (sc);
                exit;
              end;
    end
  else c.SetFocus;
end;

{ TRestrictedSizeForm }

procedure TRestrictedSizeForm.AfterConstruction;
begin
  inherited;
  MinWidth := 400;
  MinHeight := 400;
end;

procedure TRestrictedSizeForm.Resize;
begin
  inherited;

  if width < MinWidth then
    width := MinWidth;

  if Height < MinHeight then
    Height := MinHeight;

  if WindowState <> TWindowState.wsNormal then exit;

  if Top < 0 then Top := 0;
  if Left < 0 then Left := 0;

  var ScreenHeight := Screen.WorkAreaHeight;
  var ScreenWidth := Screen.WorkAreaWidth;

  if (Top + Height > ScreenHeight) and (Top > 0)
     then Top := 0;
  if Top + Height > ScreenHeight
     then Height := ScreenHeight;

  if (Left + Width > ScreenWidth) and (Left > 0)
     then Left := 0;
  if Left + Width > ScreenWidth
     then Width := ScreenWidth;
end;

procedure ShowError (const Error: string);
begin
  TDialogServiceSync.MessageDialog(Error, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], TMsgDlgBtn.mbOK, 0);
end;

procedure ShowErrorAndAbort (const Error: string);
begin
  ShowError (Error);
  abort;
end;

{ TStringGridHelper }

function TStringGridHelper.AddColumn (ColumnClass: TColumnClass; const Header: string; const TagString: string = ''; Width: integer = 0): TColumn;
begin
  result := ColumnClass.Create(self);
  result.Header := Header;
  result.TagString := TagString;
  if Width = 0
     then Width := 100;
  result.Width := max (Width, result.Width);
  result.Name := Name + 'Col' + inttostr (ColumnCount);
  AddObject(result);
end;

function TStringGridHelper.CopyColumn(ACol: TColumn) : TColumn;
begin
  var col := TColumnClass(ACol.ClassType).Create(self);
  col.Header := ACol.Header;
  col.Tag := ACol.Tag;
  col.TagString := ACol.TagString;
  col.Width := ACol.Width;
  AddObject(col);
  Result := col;
end;

{ TDateEditHelper }

procedure TDateEditHelper.SetSmartDate(d: TDate);
begin
  if d = 0
    then
      begin
        Date := now;
        IsEmpty := true;
      end
    else
      begin
        Date := d;
        IsEmpty := false;
      end;

end;

{ TApplicationHelper }

function TApplicationHelper.ExeName: String;
begin
   // ParamStr(0);

{$IF defined(ANDROID)}
  if (Result.IsEmpty) then
    Result := JStringToString(TAndroidHelper.Context.getPackageCodePath);
{$ELSE}
  Result :=  ParamStr(0);;
{$IFEND}
end;

initialization

finalization
  AutoModalDimmer.Free;

end.
